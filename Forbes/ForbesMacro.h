
//
//  ForbesMacro.h
//  Forbes
//
//  Created by 周灿华 on 2019/7/20.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#ifndef ForbesMacro_h
#define ForbesMacro_h

/****************** extern 定义，方便都出变量 ******************/
#ifdef __cplusplus
#define CC_EXTERN        extern "C" __attribute__((visibility ("default")))
#else
#define CC_EXTERN        extern __attribute__((visibility ("default")))
#endif

#define CC_STATIC_INLINE    static inline

/****************** 打印日志 ******************/
//替换系统默认的NSLog, 使用FBSLog 功能更为强大
#ifdef DEBUG
#define NSLog(format, ...) printf("\n[%s] %s [第%d行] %s\n", __TIME__, __FUNCTION__, __LINE__, [[NSString stringWithFormat:format, ## __VA_ARGS__] UTF8String]);
#else
#define NSLog(format, ...)
#endif

#define WEAKSELF __weak typeof(self) weakSelf = self;



/**********************沙盒路径*****************************/
#define kDocumentPath [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject]
#define kDocumentFilePath(fileName) [kDocumentPath stringByAppendingPathComponent:fileName]

#define kTmpPath NSTemporaryDirectory()
#define kTmpFilePath(fileName) [kTmpPath stringByAppendingPathComponent:fileName]

#define kCachePath [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) firstObject]
#define kCacheFilePath(fileName) [kCachePath stringByAppendingPathComponent:fileName]

//app bundleid
#define kBundleIdentifier  @"com.coderchou.Forbes"



////获取图片资源
#define kImageWithName(imageName) [UIImage imageNamed:[NSString stringWithFormat:@"%@",imageName]]

/**********************颜色相关*****************************/

#define Hexcolor(value) [UIColor colorWithRed:((float)((value & 0xFF0000) >> 16))/255.0 \
green:((float)((value & 0xFF00) >> 8))/255.0 \
blue:((float)(value & 0xFF))/255.0 alpha:1.0]

#define RGB(r,g,b) [UIColor colorWithRed:(r)/255.0f green:(g)/255.0f blue:(b)/255.0f alpha:1.0]
#define RGBA(r,g,b,a) [UIColor colorWithRed:(r)/255.0f green:(g)/255.0f blue:(b)/255.0f alpha:(a)]
#define RandomColor             RGB(arc4random_uniform(255),arc4random_uniform(255),arc4random_uniform(255))


#define COLOR_BackgroundGray Hexcolor(0xf5f5f5)    //灰色背景色
#define COLOR_DarkRed Hexcolor(0xcd484d)           //深红色
#define COLOR_TitleBlack Hexcolor(0x323232)        //字体黑色
#define COLOR_TitleGray  Hexcolor(0x888888)        //字体灰色
#define COLOR_DDDDDD  Hexcolor(0xdddddd)           //浅灰色
#define COLOR_EEEEEE Hexcolor(0xeeeeee)
#define COLOR_CommomBG Hexcolor(0xF4F4F4)          //通用背景色
#define MainThemeColor Hexcolor(0x8C734B)

/**********************系统版本相关*****************************/
#define SystemVersion [[[UIDevice currentDevice] systemVersion] floatValue]

#define IOS12_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 12.0)
#define IOS11_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 11.0)
#define IOS10_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 10.0)
#define IOS9_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 9.0)
#define IOS8_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
#define IOS7_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)


/**********************屏幕适配相关*****************************/

/** 主窗口 */
#define KEYWINDOW [UIApplication sharedApplication].keyWindow

// 是否iPhone X
#define IPHONE_X \
({BOOL isPhoneX = NO;\
if (@available(iOS 11.0, *)) {\
isPhoneX = [[UIApplication sharedApplication] delegate].window.safeAreaInsets.bottom > 0.0;\
}\
(isPhoneX);})



#define OnePixel       1.0 / [UIScreen mainScreen].scale

/** 屏幕尺寸 */
#define kScreenWidth    [UIScreen mainScreen].bounds.size.width
#define kScreenHeight   [UIScreen mainScreen].bounds.size.height
#define kScreenScale    ([UIScreen mainScreen].scale)
#define kScreenBounds   ([[UIScreen mainScreen] bounds])


#define kBaseTabBarH 49.0
// 当前的tabbar高度
#define kTabBarH (IPHONE_X ? 83.0 : 49.0)
// tabbar高度差
#define kDiffTabBarH (kTabBarH - 49.0)
// 当前的statusBar高度
#define kStatusBarH (IPHONE_X ? 44.0 : 20.0)
// statusBar高度差
#define kDiffStatusBarH (kStatusBarH - 20.0)
// navBar高度
#define kNavBarH 44.0
// statusBar + navBar高度（以前的64，现在88）
#define kNavTopMargin (kStatusBarH + kNavBarH)


#define kWidthRate ([UIScreen mainScreen].bounds.size.width >= 375.0 ? 1.0 : [UIScreen mainScreen].bounds.size.width/375.0) //屏幕宽度大于375不进行拉伸
//#define kWidthRate 1.0   //直接把比列设置为 1

//  计算在当前屏幕实际的宽度，用于‘屏幕适配’
#define WScale(v) ((v)*kWidthRate)


/**********************方便字体统一管理*****************************/
//字体大小调节
#define kLabelFontRate (1.0)
//#define kLabelFontRate ([[[NSUserDefaults standardUserDefaults] objectForKey:@"KEY_fontsize"] doubleValue])

#define kLabelSquareFont   [UIFont systemFontOfSize:13*kWidthRate*kLabelFontRate]
#define kLabelFontSize18_5 [UIFont systemFontOfSize:18.5*kWidthRate*kLabelFontRate]
#define kLabelFontSize17_5 [UIFont systemFontOfSize:17.5*kWidthRate*kLabelFontRate]
#define kLabelFontSize16_5 [UIFont systemFontOfSize:16.5*kWidthRate*kLabelFontRate]
#define kLabelFontSize15_5 [UIFont systemFontOfSize:15.5*kWidthRate*kLabelFontRate]
#define kLabelFontSize14_5 [UIFont systemFontOfSize:14.5*kWidthRate*kLabelFontRate]
#define kLabelFontSize13_5 [UIFont systemFontOfSize:13.5*kWidthRate*kLabelFontRate]
#define kLabelFontSize12_5 [UIFont systemFontOfSize:12.5*kWidthRate*kLabelFontRate]
#define kLabelFontSize45 [UIFont systemFontOfSize:45*kWidthRate*kLabelFontRate]
#define kLabelFontSize30 [UIFont systemFontOfSize:30*kWidthRate*kLabelFontRate]
#define kLabelFontSize24 [UIFont systemFontOfSize:24*kWidthRate*kLabelFontRate]
#define kLabelFontSize21 [UIFont systemFontOfSize:21*kWidthRate*kLabelFontRate]
#define kLabelFontSize20 [UIFont systemFontOfSize:20*kWidthRate*kLabelFontRate]
#define kLabelFontSize19 [UIFont systemFontOfSize:19*kWidthRate*kLabelFontRate]
#define kLabelFontSize18 [UIFont systemFontOfSize:18*kWidthRate*kLabelFontRate]
#define kLabelFontSize17 [UIFont systemFontOfSize:17*kWidthRate*kLabelFontRate]
#define kLabelFontSize16 [UIFont systemFontOfSize:16*kWidthRate*kLabelFontRate]
#define kLabelFontSize15 [UIFont systemFontOfSize:15*kWidthRate*kLabelFontRate]
#define kLabelFontSize14 [UIFont systemFontOfSize:14*kWidthRate*kLabelFontRate]
#define kLabelFontSize13 [UIFont systemFontOfSize:13*kWidthRate*kLabelFontRate]
#define kLabelFontSize12 [UIFont systemFontOfSize:12*kWidthRate*kLabelFontRate]
#define kLabelFontSize11 [UIFont systemFontOfSize:11*kWidthRate*kLabelFontRate]
#define kLabelFontSize10 [UIFont systemFontOfSize:10*kWidthRate*kLabelFontRate]
#define kLabelFontSize9 [UIFont systemFontOfSize:9*kWidthRate*kLabelFontRate]
#define kLabelFontSize8 [UIFont systemFontOfSize:8*kWidthRate*kLabelFontRate]

// 自定义字体大小
#define kLabelFontSize(size) [UIFont systemFontOfSize:(size)*kWidthRate*kLabelFontRate]
#define kLabelBoldFontSize(size) [UIFont boldSystemFontOfSize:(size)*kWidthRate*kLabelFontRate]

#define SCREENWIDTH  ([[UIScreen mainScreen]bounds].size.width)
#define WS(weakSelf)  __weak __typeof(&*self)weakSelf = self;
#define RGBA_COLOR(R, G, B, A) [UIColor colorWithRed:((R) / 255.0f) green:((G) / 255.0f) blue:((B) / 255.0f) alpha:A]
#define ImgHeader @"http://static.soperson.com"

#define Main_Screen_Height     [[UIScreen mainScreen] bounds].size.height

#define Main_Screen_Width       [[UIScreen mainScreen] bounds].size.width


// View 圆角和加边框
#define ViewBorderRadius(View, Radius, Width, Color)\
                                \
                                [View.layer setCornerRadius:(Radius)];\
                                [View.layer setMasksToBounds:YES];\
                                [View.layer setBorderWidth:(Width)];\
                                [View.layer setBorderColor:[Color CGColor]]



// View 圆角
#define ViewRadius(View, Radius)\
                                \
                                [View.layer setCornerRadius:(Radius)];\
                                [View.layer setMasksToBounds:YES]


#endif /* ForbesMacro_h */
