//
//  DGNetworking.m
//  DGDemo
//
//  Created by michan on 2020/4/23.
//  Copyright © 2020 michan. All rights reserved.
//





#import "MCNetworking.h"

#import <AFNetworking/AFNetworking.h>
#import <AFNetworkActivityIndicatorManager.h>
#include <CommonCrypto/CommonCrypto.h>
#import <YYCache.h>
#import <YYDiskCache.h>

@implementation MCResultModel



@end



@implementation MCNetworking

static NSMutableArray *_allSessionTask;

static AFHTTPSessionManager *_sessionManager;



static YYCache *_dataCache;
static NSString *const NetworkResponseCache = @"MCNetworkResponseCache";

#pragma mark -- 初始化相关属性
+ (void)initialize{
    _sessionManager = [AFHTTPSessionManager manager];
    //设置请求超时时间
    _sessionManager.requestSerializer.timeoutInterval = 30.f;
    //设置服务器返回结果的类型:JSON(AFJSONResponseSerializer,AFHTTPResponseSerializer)
    _sessionManager.responseSerializer = [AFHTTPResponseSerializer serializer];

    _sessionManager.requestSerializer = [AFHTTPRequestSerializer serializer];
    
    _sessionManager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/html", @"text/json", @"text/plain", @"text/javascript", @"text/xml", @"image/*",@"multipart/form-data",@"application/x-www-form-urlencoded", nil];
    
    //开始监测网络状态
//    [[AFNetworkReachabilityManager sharedManager] startMonitoring];

    //打开状态栏菊花
    [AFNetworkActivityIndicatorManager sharedManager].enabled = YES;
    _dataCache = [YYCache cacheWithName:NetworkResponseCache];

    
}

#pragma mark -- POST请求
/**
 POST请求
 
 @param DGAPI 请求api
 @param parameters 请求参数
 @param cachePolicy 是否缓存(是否先读取缓存)
 @param ResponseType    DG相对应的服务器
 @param isNeedLogin    是否需要登录才能方法该接口
 @param success 请求成功回调
 @param failure 请求失败回调
 @param requestEnd 请求结束（没实际用，只是告知请求者已请求结束方便处理其他事情，实际数据在success返回）
 */

+ (void)ResponseNetworkPOST_API:(NSString *)DGAPI parameters:(NSDictionary *)parameters  TheServer:(NSInteger)Server  cachePolicy:(BOOL)cachePolicy ISNeedLogin:(BOOL)isNeedLogin  RequestEnd:(MCHttpEnd)requestEnd MCHttpCacheData:(MCHttpCacheData)CacheObject success:(MCHttpSuccess)success failure:(MCHttpFail)failure{

    [self HTTPWithMethod_API:DGAPI IsPOST:YES parameters:parameters TheServer:Server cachePolicy:cachePolicy ISNeedLogin:isNeedLogin RequestEnd:requestEnd MCHttpCacheData:CacheObject success:success failure:failure];
    
}
+ (void)ResponseNetworkGET_API:(NSString *)DGAPI parameters:(NSDictionary *)parameters  TheServer:(NSInteger)Server  cachePolicy:(BOOL)cachePolicy ISNeedLogin:(BOOL)isNeedLogin  RequestEnd:(MCHttpEnd)requestEnd MCHttpCacheData:(MCHttpCacheData)CacheObject success:(MCHttpSuccess)success failure:(MCHttpFail)failure{
    [self HTTPWithMethod_API:DGAPI IsPOST:NO parameters:parameters TheServer:Server cachePolicy:cachePolicy ISNeedLogin:isNeedLogin RequestEnd:requestEnd MCHttpCacheData:CacheObject success:success failure:failure];

}

+ (void)HTTPWithMethod_API:(NSString *)API IsPOST:(BOOL)isPOST parameters:(NSDictionary *)parameters TheServer:(NSInteger)Server  cachePolicy:(BOOL)cachePolicy ISNeedLogin:(BOOL)isNeedLogin RequestEnd:(MCHttpEnd)requestEnd MCHttpCacheData:(MCHttpCacheData)CacheObject success:(MCHttpSuccess)MCsuccess failure:(MCHttpFail)failure{
    //各接口对应的服务器
    NSString *url = [NSString stringWithFormat:@"%@%@",FBSBaseUrl,API];
    if (Server == 1) {//微博直播
        url = [NSString stringWithFormat:@"%@%@",FBSWeiboUrl,API];
    }
    
    if (cachePolicy) {
        [self httpCacheForURL:API parameters:parameters withBlock:^(id responseObject) {
            if (responseObject) {
                CacheObject?CacheObject(responseObject):nil;
            }
        }];
    }
    
    __weak typeof(self) weakSlef = self;
    
    
    
    [self dataTaskWithHTTPMethodUrl:url parameters:parameters IsPOST:isPOST ISNeedLogin:isNeedLogin success:^(NSURLSessionDataTask * _Nullable task, id _Nullable responseObject) {
        [[weakSlef allSessionTask] removeObject:task];
        requestEnd?requestEnd(@""):@"";

       
        
        NSString * dataStr =[[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        
        
        
//     //请求成功后数据处理
        NSDictionary * resultDic= [weakSlef DataProcessing:dataStr];
        
        NSLog(@"resultDic ==%@",resultDic);

//
        MCResultModel * Rmodel =[MCResultModel mj_objectWithKeyValues:resultDic];
//
//
        NSString *success = Rmodel.success;
        NSString *code = Rmodel.code;
        

         if ([code isEqualToString:@"200"]&&resultDic) {
             if (cachePolicy) {
                //缓存
                [weakSlef setHttpCache:resultDic url:API parameters:parameters];
            }
        
             MCsuccess ? MCsuccess(resultDic) : nil;

         }

        else
        {
            
            failure ? failure(task,nil,resultDic,@"网络繁忙，请稍后重试") : nil;
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [[weakSlef allSessionTask] removeObject:task];
        requestEnd?requestEnd(@""):@"";
        failure ? failure(task,error,nil,@"网络繁忙，请稍后重试") : nil;

    }];
    
    
}





#pragma mark -- 网络请求处理
+ (void)dataTaskWithHTTPMethodUrl:(NSString *)url
parameters:(nullable id )parameters IsPOST:(BOOL)isPOST ISNeedLogin:(BOOL)isNeedLogin
   success:(void (^)(NSURLSessionDataTask * _Nullable, id _Nullable))success
   failure:(void (^)(NSURLSessionDataTask * _Nullable, NSError * _Nonnull))failure {

    NSURLSessionTask *sessionTask;
    if (isPOST) {
        [_sessionManager POST:url parameters:parameters progress:nil success:success failure:failure];

    }
    else
        [_sessionManager GET:url parameters:parameters progress:nil success:success failure:failure];

        
//    添加最新的sessionTask到数组
    sessionTask ? [[self allSessionTask] addObject:sessionTask] : nil;
    
}

#pragma mark-数据处理
+(NSDictionary*)DataProcessing:(id)response{
    
    NSString *responseString1 =  [response stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    NSString *responseString2 = [responseString1 stringByReplacingOccurrencesOfString:@"\r" withString:@""];
    NSString *responseString3 = [responseString2 stringByReplacingOccurrencesOfString:@"\t" withString:@""];
    
    NSData *jsonData = [responseString3 dataUsingEncoding:NSUTF8StringEncoding];
    if (!jsonData) {
        return response;
    }
    NSError *err;
    
    NSDictionary *  resultDic = [NSJSONSerialization JSONObjectWithData:jsonData
                                                                options:NSJSONReadingMutableContainers
                                                                  error:&err];
    return resultDic;
    
}



/*所有的请求task数组*/
+ (NSMutableArray *)allSessionTask{
    if (!_allSessionTask) {
        _allSessionTask = [NSMutableArray array];
    }
    return _allSessionTask;
}

//商城加密算法
+ (NSString *)LJHMACMD5:(NSString *)data key:(NSString *)key {
    
    NSData *datas = [data dataUsingEncoding:NSUTF8StringEncoding];
    size_t dataLength = datas.length;
    NSData *keys = [key dataUsingEncoding:NSUTF8StringEncoding];
    size_t keyLength = keys.length;
    unsigned char result[CC_MD5_DIGEST_LENGTH];
    CCHmac(kCCHmacAlgMD5, [keys bytes], keyLength, [datas bytes], dataLength, result);
    for (int i = 0; i < CC_MD5_DIGEST_LENGTH; i ++) {
        printf("%d ",result[i]);
    }
    printf("\n-------%s-------\n",result);
    
    NSData *HMAC = [[NSData alloc] initWithBytes:result length:sizeof(result)];
    NSString *hash = [HMAC base64Encoding];
    return hash;
}

#pragma mark -- 网络缓存
#pragma mark -- 网络缓存
+ (YYCache *)getYYCache
{
    return _dataCache;
}
/**
*  异步缓存网络数据,根据请求的 URL与parameters
*  做Key存储数据, 这样就能缓存多级页面的数据
*
*  @param httpData   服务器返回的数据
*  @param url        请求的URL地址
*  @param parameters 请求的参数
*/

+ (void)setHttpCache:(id)httpData url:(NSString *)url parameters:(NSDictionary *)parameters{
    if (httpData) {
        NSString *cacheKey = [self cacheKeyWithURL:url parameters:parameters];
        //        NSString *cacheTime = NSStringFormat(@"%@Time",cacheKey);//缓存时间
        //        [_dataCache setObject:[NSDate date] forKey:cacheTime];
        [_dataCache setObject:httpData forKey:cacheKey withBlock:nil];
    }
}
+ (NSString *)cacheKeyWithURL:(NSString *)url parameters:(NSDictionary *)parameters
{
    if(!parameters){return url;};
        
    /// 将URL与转换好的参数字符串拼接在一起,成为最终存储的KEY值
    NSString *cacheKey = [NSString stringWithFormat:@"%@%@",url,[parameters mj_JSONString]];
    
    return cacheKey;
}
/**
*  根据请求的 URL与parameters 异步取出缓存数据
*
*  @param url        请求的URL
*  @param parameters 请求的参数
*  @param block      异步回调缓存的数据
*
*/
+ (void)httpCacheForURL:(NSString *)url parameters:(NSDictionary *)parameters withBlock:(void(^)(id responseObject))block
{
    NSString *cacheKey = [self cacheKeyWithURL:url parameters:parameters];
    // NSString *cacheTime = NSStringFormat(@"%@Time",cacheKey);//缓存时间
    
    [_dataCache objectForKey:cacheKey withBlock:^(NSString * _Nonnull key, id<NSCoding>  _Nonnull object) {
        dispatch_async(dispatch_get_main_queue(), ^{
            block(object);
        });
    }];
    
}
+ (void)getAllHttpCacheSizeBlock:(void(^)(NSInteger totalCount))block{
    return [_dataCache.diskCache totalCountWithBlock:block];
}
+ (void)removeAllHttpCacheBlock:(void(^)(int removedCount, int totalCount))progress
                       endBlock:(void(^)(BOOL error))end{
    
    [_dataCache.diskCache removeAllObjectsWithProgressBlock:progress endBlock:end];
}

/// 取消指定URL的HTTP请求
+ (void)cancelRequestWithURL:(NSString *)URL{
    if (!URL) { return; }
    
    @synchronized (self) {
        [[self allSessionTask] enumerateObjectsUsingBlock:^(NSURLSessionTask  *_Nonnull task, NSUInteger idx, BOOL * _Nonnull stop) {
            if ([task.currentRequest.URL.absoluteString hasPrefix:URL]) {
                [task cancel];
                [[self allSessionTask] removeObject:task];
                *stop = YES;
            }
        }];
    }
}

/// 取消所有HTTP请求
+ (void)cancelAllRequest{
    @synchronized (self) {
        [[self allSessionTask] enumerateObjectsUsingBlock:^(NSURLSessionTask  *_Nonnull task, NSUInteger idx, BOOL * _Nonnull stop) {
            [task cancel];
        }];
        [[self allSessionTask] removeAllObjects];
    }
}
@end
