//
//  DGNetworking.h
//  DGDemo
//
//  Created by michan on 2020/4/23.
//  Copyright © 2020 michan. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN



/**缓存数据Block*/
typedef void(^MCHttpCacheData)(id CacheObject);

/**请求的成功Block*/
typedef void(^MCHttpSuccess)(id responseObject);

/**请求的失败Block*/
typedef void(^MCHttpFail)(NSURLSessionDataTask *operation,NSError *error,id responseObject,NSString *description);
/**请求结束Block（EndObject：值暂没有实际意义）*/
typedef void(^MCHttpEnd)(id EndObject);


@interface MCResultModel : NSObject
@property(nonatomic,copy)NSString * success;
@property(nonatomic,copy)NSString * code;
@property(nonatomic,strong)NSDictionary * data;

@end





@interface MCNetworking : NSObject



#pragma mark -- POST请求
/**
 POST请求
 
 @param DGAPI 请求api
 @param parameters 请求参数
 @param cachePolicy 是否缓存(是否先读取缓存)
 @param ResponseType    DG相对应的服务器
 @param isNeedLogin    是否需要登录才能访问该接口
 @param CacheObject  缓存数据（cachePolicy YES 时才有效：先读取缓存、请求成功后也会回调success）
 @param success 请求成功回调
 @param failure 请求失败回调
 @param requestEnd 请求结束（没实际用，只是告知请求者已请求结束方便处理其他事情，实际数据在success返回）

 */
+ (void)ResponseNetworkPOST_API:(NSString *)DGAPI parameters:(NSDictionary *)parameters  TheServer:(NSInteger)Server  cachePolicy:(BOOL)cachePolicy ISNeedLogin:(BOOL)isNeedLogin  RequestEnd:(MCHttpEnd)requestEnd MCHttpCacheData:(MCHttpCacheData)CacheObject success:(MCHttpSuccess)success failure:(MCHttpFail)failure;


+ (void)ResponseNetworkGET_API:(NSString *)DGAPI parameters:(NSDictionary *)parameters  TheServer:(NSInteger)Server  cachePolicy:(BOOL)cachePolicy ISNeedLogin:(BOOL)isNeedLogin  RequestEnd:(MCHttpEnd)requestEnd MCHttpCacheData:(MCHttpCacheData)CacheObject success:(MCHttpSuccess)success failure:(MCHttpFail)failure;



/**
 *  获取网络缓存的总大小 bytes(字节)
 *  推荐使用该方法 不会阻塞主线程，通过block返回
 */
+ (void)getAllHttpCacheSizeBlock:(void(^)(NSInteger totalCount))block;


/**
 *  删除所有网络缓存
 *  推荐使用该方法 不会阻塞主线程，同时返回Progress
 */
+ (void)removeAllHttpCacheBlock:(void(^)(int removedCount, int totalCount))progress
                       endBlock:(void(^)(BOOL error))end;


/// 取消指定URL的HTTP请求
+ (void)cancelRequestWithURL:(NSString *)URL;

/// 取消所有HTTP请求
+ (void)cancelAllRequest;



@end

NS_ASSUME_NONNULL_END
