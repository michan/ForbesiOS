//
//  FBSAdvertListApi.m
//  Forbes
//
//  Created by 赵志辉 on 2019/9/18.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import "FBSAdvertListApi.h"

@implementation FBSAdvertListApi

- (NSString *)requestURI {
    return @"ads/position";
}

@end
