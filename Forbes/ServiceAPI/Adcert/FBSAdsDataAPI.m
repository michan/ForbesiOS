//
//  FBSAdsDataAPI.m
//  Forbes
//
//  Created by 赵志辉 on 2019/9/20.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import "FBSAdsDataAPI.h"

@implementation FBSAdsDataAPI

- (NSString *)requestURI {
    return @"ads/list";
}


- (NSDictionary *)requestParameter {
    return @{
             @"position_id" : self.position_id ?: @"",
             };
}

@end
