//
//  FBSAdsDataAPI.h
//  Forbes
//
//  Created by 赵志辉 on 2019/9/20.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import "FBSGetRequest.h"

NS_ASSUME_NONNULL_BEGIN

@interface FBSAdsDataAPI : FBSGetRequest

@property(nonatomic ,strong)NSString *position_id;

@end

NS_ASSUME_NONNULL_END
