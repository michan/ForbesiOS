//
//  FBSMineInfoAPI.h
//  Forbes
//
//  Created by 周灿华 on 2019/9/8.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSGetRequest.h"

NS_ASSUME_NONNULL_BEGIN

@interface FBSMineInfoAPI : FBSGetRequest

@end

NS_ASSUME_NONNULL_END
