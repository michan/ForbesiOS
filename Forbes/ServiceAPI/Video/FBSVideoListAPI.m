//
//  FBSVideoListAPI.m
//  Forbes
//
//  Created by 周灿华 on 2019/9/7.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSVideoListAPI.h"

@implementation FBSVideoListAPI

- (NSString *)requestURI {
    return @"tv/list";
}


- (NSDictionary *)requestParameter {
    return @{
             @"limit" : @(self.limit),
             @"page" : @(self.page),
             @"category_id" : self.category_id.length ? self.category_id : @""
             };
}

@end
