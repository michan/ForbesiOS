//
//  FBSVideoListAPI.h
//  Forbes
//
//  Created by 周灿华 on 2019/9/7.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSGetRequest.h"

@interface FBSVideoListAPI : FBSGetRequest

@property (nonatomic, assign) NSInteger limit;

@property (nonatomic, assign) NSInteger page;
@property (nonatomic, strong)NSString  *category_id;
@end

