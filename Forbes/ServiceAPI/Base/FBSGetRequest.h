//
//  FBSGetRequest.h
//  Forbes
//
//  Created by 周灿华 on 2019/9/7.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "YBBaseRequest.h"

NS_ASSUME_NONNULL_BEGIN

@interface FBSGetRequest : YBBaseRequest

@end

NS_ASSUME_NONNULL_END
