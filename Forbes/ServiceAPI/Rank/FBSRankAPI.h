//
//  FBSRankAPI.h
//  Forbes
//
//  Created by 周灿华 on 2019/9/24.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSGetRequest.h"

///榜单推荐
@interface FBSRankRecommendAPI : FBSGetRequest

@end


///榜单列表
//http://www.forbeschina.com/v1/list?group_id=2&limit=20&page=1&keyword=2017
@interface FBSRankAPI : FBSGetRequest
@property (nonatomic, copy) NSString *group_id;  //用来标记过滤条件
@property (nonatomic, copy) NSString *category_id;  //用来标记过滤条件
@end




///榜单详情
@interface FBSRankDetailAPI : FBSGetRequest

@end

