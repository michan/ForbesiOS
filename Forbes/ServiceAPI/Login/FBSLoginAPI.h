//
//  FBSLoginAPI.h
//  Forbes
//
//  Created by 周灿华 on 2019/9/8.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 账号登录
 -- 组合登录一：account + password
 -- 组合登录二：email + password
 */
@interface FBSAccountLoginAPI : FBSPostRequest
@property (nonatomic, copy) NSString *account;
@property (nonatomic, copy) NSString *email;
@property (nonatomic, copy) NSString *password;
@end


/**
 获取手机号验证码
 */
@interface FBSPhoneCodeAPI : FBSPostRequest
@property (nonatomic, copy) NSString *tel;
@end


/**
 手机号登录
 */
@interface FBSPhoneLoginAPI : FBSPostRequest
@property (nonatomic, copy) NSString *tel;
@property (nonatomic, copy) NSString *code;
@end




///注册验证码接口
@interface FBSSignupcodeAPI : FBSPostRequest
@property (nonatomic, copy) NSString *tel;
@property (nonatomic, copy) NSString *token;
@end


///注册接口
@interface FBSSignupAPI : FBSPostRequest
@property (nonatomic, copy) NSString *tel; //手机号
@property (nonatomic, copy) NSString *account; //账号
@property (nonatomic, copy) NSString *password;  //密码
@property (nonatomic, copy) NSString *nickname;  //昵称
@property (nonatomic, copy) NSString *email;     //邮箱
@property (nonatomic, copy) NSString *code;     //短信验证码
@end




