//
//  FBSLoginAPI.m
//  Forbes
//
//  Created by 周灿华 on 2019/9/8.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSLoginAPI.h"

@implementation FBSAccountLoginAPI

- (NSString *)requestURI {
    return @"login";
}

- (NSDictionary *)requestParameter {
    if (self.account.length) {
        return @{
                 @"account" : self.account ?:@"",
                 @"password" : self.password ?:@""
                 };
    } else {
        return @{
                 @"email" : self.email ?:@"",
                 @"password" : self.password ?:@""
                 };
    }
}

@end




@implementation FBSPhoneCodeAPI

- (NSString *)requestURI {
    return @"login/mobile";
}

- (NSDictionary *)requestParameter {
    return @{
             @"tel" : self.tel ?:@"",
             };
}

@end




@implementation FBSPhoneLoginAPI

- (NSString *)requestURI {
    return @"login/mobile/verify";
}

- (NSDictionary *)requestParameter {
    return @{
             @"tel" : self.tel ?:@"",
             @"code" : self.code ?:@""
             };
}

@end














@implementation FBSSignupcodeAPI

- (NSString *)requestURI {
    return @"login/signupcode";
}


- (NSDictionary *)requestParameter {
    return @{
             @"tel" : self.tel ?:@"",
             @"token" : self.token ?:@""
             };
}

@end



@implementation FBSSignupAPI

- (NSString *)requestURI {
    return @"login/signup";
}

- (NSDictionary *)requestParameter {
    return @{
             @"tel" : self.tel ?:@"",
             @"account" : self.account ?:@"",
             @"password" : self.password ?:@"",
             @"nickname" : self.nickname ?:@"",
             @"email" : self.email ?:@"",
             @"code" : self.code ?:@"",
             };
}

@end
