//
//  FBSHomeAPI.h
//  Forbes
//
//  Created by 周灿华 on 2019/9/8.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSGetRequest.h"

//要闻频道所有推荐内容
@interface FBSAllRecommendAPI : FBSGetRequest

@end


//要闻频道置顶新闻
@interface FBSTopNewsAPI : FBSGetRequest

@end

//要闻频道精选数据
@interface FBSKeyNewsAPI : FBSGetRequest

@end


//要闻频道专题数据
@interface FBSTopicAPI : FBSGetRequest

@end


//要闻频道所有文章列表
@interface FBSAllArticlesAPI : FBSGetRequest
///一次取多少条记录，默认10
@property (nonatomic, assign) NSInteger limit;
///页码，默认1
@property (nonatomic, assign) NSInteger page;
@end



//要闻频道所有推荐内容
@interface FBSSearchAPI : FBSGetRequest
@property (nonatomic, strong) NSString *keyword; //搜索关键字
@property (nonatomic, assign) NSInteger limit;  //一次取多少条记录，默认20
@property (nonatomic, assign) NSInteger page;   //页码，默认1
@end








