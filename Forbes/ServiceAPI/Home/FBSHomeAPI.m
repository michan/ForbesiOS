//
//  FBSHomeAPI.m
//  Forbes
//
//  Created by 周灿华 on 2019/9/8.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSHomeAPI.h"

@implementation FBSAllRecommendAPI

- (NSString *)requestURI {
    return @"focus/channels";
}

@end


@implementation FBSTopNewsAPI

- (NSString *)requestURI {
    return @"articles/top";
}

@end



@implementation FBSKeyNewsAPI

- (NSString *)requestURI {
    return @"focus/news";
}

@end



@implementation FBSTopicAPI

- (NSString *)requestURI {
    return @"topic";
}

@end



@implementation FBSAllArticlesAPI

- (NSString *)requestURI {
    return @"articles/all";
}


- (NSDictionary *)requestParameter {
    return @{
             @"limit" : [@(self.limit) stringValue],
             @"page"  : [@(self.page) stringValue]
             };
}

@end



@implementation FBSSearchAPI

- (NSString *)requestURI {
    return @"search";
}


- (NSDictionary *)requestParameter {
    return @{
             @"keyword"  : self.keyword,
             @"limit" : [@(self.limit) stringValue],
             @"page"  : [@(self.page) stringValue]
             };
}

@end
