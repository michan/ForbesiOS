//
//  ForbesConfig.h
//  Forbes
//
//  Created by 周灿华 on 2019/7/20.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#ifndef ForbesConfig_h
#define ForbesConfig_h

//服务器基地址配置: baseurl要以"/"结尾  relativeUrl不能以"/"开始
#define FBSBaseUrl @"http://www.forbeschina.com/v1/"
#define FBSWeiboUrl @"https://api.weibo.com/2/"


#define kAppKey           @"1480309785"
#define kRedirectURI    @"http://open.weibo.com/apps/1480309785/privilege/oauth"


#define PeopleGroupId  @"2"  //榜单人
#define ZoneGroupId    @"3"  //榜单地方
#define CompanyGroupId @"4"  //榜单公司

typedef NS_ENUM(NSUInteger, DetailPageType) {
    DetailPageTypeArticle,   //文章
    DetailPageTypeRank,      //榜单
    DetailPageTypeActivity,  //活动
    DetailPageTypeVideo,     //视频
    DetailPageTypeTopic      //专题
};


#endif /* ForbesConfig_h */
