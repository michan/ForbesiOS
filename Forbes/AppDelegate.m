//
//  AppDelegate.m
//  Forbes
//
//  Created by 周灿华 on 2019/7/20.
//  Copyright © 2019年 周灿华. All rights reserved.
//
#define kAlertMessage(aMessage) dispatch_async(dispatch_get_main_queue(), ^{\
UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示信息" message:aMessage delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];\
[alert show];\
});

#import "AppDelegate.h"
#import "LBLaunchImageAdView.h"
#import<CoreTelephony/CTCellularData.h>
#import<SystemConfiguration/CaptiveNetwork.h>
#import "AFNetworkActivityIndicatorManager.h"
#import "AFNetworking.h"

#import "YBBaseRequest.h"
#import "JPUSHService.h"
/// iOS10注册APNs所需头文件
#ifdef NSFoundationVersionNumber_iOS_9_x_Max
#import <UserNotifications/UserNotifications.h>
#endif

@interface AppDelegate ()<JPUSHRegisterDelegate>
@property(nonatomic,copy)NSString * registrationID;

@end

@implementation AppDelegate
@synthesize allowRotation;
@synthesize netWorkStatesCode;

- (void)networkStatus:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    //2.根据权限执行相应的交互
    CTCellularData *cellularData = [[CTCellularData alloc] init];
    /*
     此函数会在网络权限改变时再次调用
     */
    CTCellularDataRestrictedState *c = cellularData.restrictedState;
    
    cellularData.cellularDataRestrictionDidUpdateNotifier = ^(CTCellularDataRestrictedState state) {
        switch (state) {
            case kCTCellularDataRestricted:
            {
                NSLog(@"Restricted");
                //2.1权限关闭的情况下 再次请求网络数据会弹出设置网络提示
                NSString *url = @"www.baidu.com";
                YBBaseRequest *request = [YBBaseRequest new];
                request.requestURI = url;
                [request startWithSuccess:^(YBNetworkResponse * _Nonnull response) {
                    
                } failure:^(YBNetworkResponse * _Nonnull response) {
                    
                }];
            }
                break;
            case kCTCellularDataNotRestricted:
                
                NSLog(@"NotRestricted");
                //2.2已经开启网络权限 监听网络状态
                [self addReachabilityManager:application didFinishLaunchingWithOptions:launchOptions];
                break;
            case kCTCellularDataRestrictedStateUnknown:
                
                NSLog(@"Unknown");
                //2.3未知情况 （还没有遇到推测是有网络但是连接不正常的情况下）
                break;
            default:
                break;
        }
    };
}
/**
 实时检查当前网络状态
 */
- (void)addReachabilityManager:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    AFNetworkReachabilityManager *afNetworkReachabilityManager = [AFNetworkReachabilityManager sharedManager];
    
    //这个可以放在需要侦听的页面
    //    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(afNetworkStatusChanged:) name:AFNetworkingReachabilityDidChangeNotification object:nil];
    [afNetworkReachabilityManager setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        self.netWorkStatesCode = status;
        [[NSNotificationCenter defaultCenter]postNotificationName:@"netWorkChangeEventNotification" object:@(status)];

        switch (status) {
            case AFNetworkReachabilityStatusNotReachable:{
                NSLog(@"网络不通：%@",@(status) );
                break;
            }
            case AFNetworkReachabilityStatusReachableViaWiFi:{
                NSLog(@"网络通过WIFI连接：%@",@(status));
                [self getInfo_application:application didFinishLaunchingWithOptions:launchOptions];
                break;
            }
            case AFNetworkReachabilityStatusReachableViaWWAN:{
                [self getInfo_application:application didFinishLaunchingWithOptions:launchOptions];
                break;
            }
            default:
                break;
        }
    }];
    
    [afNetworkReachabilityManager startMonitoring];  //开启网络监视器；
}
- (void)getInfo_application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    //获取最新用户数据
    [[FBSUserData sharedData] fetchNewestUserData];
    [[HLWJShareSDKManager sharedInstance] didFinishLaunchingWithOptions:launchOptions];
    //初始化分享
    
    //加载启动页广告
    WEAKSELF
    [LBLaunchImageAdView beginLaunch:LogoAdType adTime:3 clickBlock:^(clickType type) {
        switch (type) {
                
            case clickAdType:
                NSLog(@"点击广告回调");

                break;
            case skipAdType:
                NSLog(@"点击跳过回调");
                break;
            case overtimeAdType:
                NSLog(@"倒计时完成后的回调");
                break;
            default:
                break;
        }
        self.mainVC = [[FBSTabBarController alloc] init];
        weakSelf.window.rootViewController = weakSelf.mainVC;
    }];
}
- (NSDictionary *)fetchSSIDInfo {
    
    NSArray *ifs = (__bridge_transfer NSArray *)CNCopySupportedInterfaces();
    
    if (!ifs) {
        
        return nil;
        
    }
    
    NSDictionary *info = nil;
    
    for (NSString *ifnam in ifs) {
        
        info = (__bridge_transfer NSDictionary *)CNCopyCurrentNetworkInfo((__bridge CFStringRef)ifnam);
        
        if (info && [info count]) { break; }
        
    }
    
    return info;
    
}
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {

    [FBSLogManager setup:YES asl:NO file:YES];
    
    [FBSNavigationBar setDefaultBarBackgroundColor:[UIColor whiteColor]];
    [FBSNavigationBar setDefaultTitleAttributes:@{NSForegroundColorAttributeName : Hexcolor(0x333333),NSFontAttributeName : [UIFont systemFontOfSize:18]}];
    [FBSNavigationBar setDefaultBackButtonImage:kImageWithName(@"navback_gray")];
    
    //[IQKeyboardManager sharedManager].shouldResignOnTouchOutside = YES;
    [self prepareJPUSHServiceWithOptions:application didFinishLaunchingWithOptions:launchOptions];

    self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.window.backgroundColor = [UIColor whiteColor];
    self.window.rootViewController = self.mainVC;
    [self.window makeKeyAndVisible];
    [[HLWJShareSDKManager sharedInstance] didFinishLaunchingWithOptions:launchOptions];
    
    if ([self fetchSSIDInfo] == nil) {
        //1.获取网络权限 根绝权限进行人机交互
        if (NSFoundationVersionNumber > NSFoundationVersionNumber_iOS_9_x_Max) {
            [self networkStatus:application didFinishLaunchingWithOptions:launchOptions];
        }
        else {
            //2.2已经开启网络权限 监听网络状态
            [self addReachabilityManager:application didFinishLaunchingWithOptions:launchOptions];
        }
    }else{
        [[FBSUserData sharedData] fetchNewestUserData];
        //加载启动页广告
        WEAKSELF
        [LBLaunchImageAdView beginLaunch:LogoAdType adTime:3 clickBlock:^(clickType type) {
            switch (type) {
                    
                    
                case clickAdType:
                    NSLog(@"点击广告回调");
                    break;
                case skipAdType:
                    NSLog(@"点击跳过回调");
                    break;
                case overtimeAdType:
                    NSLog(@"倒计时完成后的回调");
                    break;
                default:
                    break;
            }
            self.mainVC = [[FBSTabBarController alloc] init];
            weakSelf.window.rootViewController = weakSelf.mainVC;
        }];
        [[HLWJShareSDKManager sharedInstance] didFinishLaunchingWithOptions:launchOptions];
    }


//    [self netWorkChangeEvent];

    
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    
    
   

    //设置显示数字
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    //设置显示数字
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];

    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


- (FBSTabBarController *)mainVC {
    if (!_mainVC) {
        _mainVC = [[FBSTabBarController alloc] init];
        _mainVC.view.backgroundColor = [UIColor whiteColor];
    }
    return _mainVC;
}

- (void)prepareJPUSHServiceWithOptions:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions{
   
    [self prepareJPUSHServiceWithOptions:launchOptions];
    
}
-(void)prepareJPUSHServiceWithOptions:(NSDictionary *)launchOptions{
    //Required
    //notice: 3.0.0及以后版本注册可以这样写，也可以继续用之前的注册方式
    JPUSHRegisterEntity * entity = [[JPUSHRegisterEntity alloc] init];
    entity.types = JPAuthorizationOptionAlert|JPAuthorizationOptionBadge|JPAuthorizationOptionSound;
    if ([[UIDevice currentDevice].systemVersion floatValue] >= 8.0) {
        // 可以添加自定义categories
        // NSSet<UNNotificationCategory *> *categories for iOS10 or later
        // NSSet<UIUserNotificationCategory *> *categories for iOS8 and iOS9
    }
    [JPUSHService registerForRemoteNotificationConfig:entity delegate:self];
    
    NSString *appKey = @"ab332c3703f33bc9ea09943b";
    NSString *channel = @"App Store";
    BOOL isProduction = 0;//0 (默认值)表示采用的是开发证书，1 表示采用生产证书发布应用
    
    //如不需要使用IDFA，advertisingIdentifier 可为nil
    [JPUSHService setupWithOption:launchOptions appKey:appKey
                          channel:channel
                 apsForProduction:isProduction
            advertisingIdentifier:nil];
    
    //2.1.9版本新增获取registration id block接口。
    [JPUSHService registrationIDCompletionHandler:^(int resCode, NSString *registrationID) {
        if(resCode == 0){
            NSLog(@"registrationID获取成功：%@",registrationID);
            self.registrationID =registrationID;
            [self prepareTuisong];
        }
        else{
            NSLog(@"registrationID获取失败，code：%d",resCode);
        }
    }];
    
    
    
    
}
- (void)application:(UIApplication *)application
didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    NSLog(@"deviceToken -------%@",deviceToken);
    /// Required - 注册 DeviceToken
    [JPUSHService registerDeviceToken:deviceToken];
}
- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    //Optional
    NSLog(@"did Fail To Register For Remote Notifications With Error: %@", error);
}
#if __IPHONE_OS_VERSION_MAX_ALLOWED > __IPHONE_7_1
- (void)application:(UIApplication *)application
didRegisterUserNotificationSettings:
(UIUserNotificationSettings *)notificationSettings {
    
    NSLog(@"MC推送1");
    
}
// Called when your app has been activated by the user selecting an action from
// a local notification.
// A nil action identifier indicates the default action.
// You should call the completion handler as soon as you've finished handling
// the action.
- (void)application:(UIApplication *)application
handleActionWithIdentifier:(NSString *)identifier
forLocalNotification:(UILocalNotification *)notification
  completionHandler:(void (^)())completionHandler {
    NSLog(@"MC推送2");
    
}

// Called when your app has been activated by the user selecting an action from
// a remote notification.
// A nil action identifier indicates the default action.
// You should call the completion handler as soon as you've finished handling
// the action.
- (void)application:(UIApplication *)application
handleActionWithIdentifier:(NSString *)identifier
forRemoteNotification:(NSDictionary *)userInfo
  completionHandler:(void (^)())completionHandler {
    NSLog(@"MC推送3");
    
}
#endif

- (void)application:(UIApplication *)application
didReceiveRemoteNotification:(NSDictionary *)userInfo {
    [JPUSHService handleRemoteNotification:userInfo];
    NSLog(@"iOS6及以下系统，收到通知");
    NSLog(@"MC推送4");
    
}

- (void)application:(UIApplication *)application
didReceiveRemoteNotification:(NSDictionary *)userInfo
fetchCompletionHandler:
(void (^)(UIBackgroundFetchResult))completionHandler {
    [JPUSHService handleRemoteNotification:userInfo];
    NSLog(@"iOS7及以上系统，收到通知");
    NSLog(@"MC推送5");
    
    if ([[UIDevice currentDevice].systemVersion floatValue]<10.0 || application.applicationState>0) {
        
        
        //        [rootViewController addNotificationCount];
    }
    
    completionHandler(UIBackgroundFetchResultNewData);
}

- (void)application:(UIApplication *)application
didReceiveLocalNotification:(UILocalNotification *)notification {
//    NSDictionary * userInfo = notification;

    NSLog(@"MC推送6");
    
    [JPUSHService showLocalNotificationAtFront:notification identifierKey:nil];
}
#ifdef NSFoundationVersionNumber_iOS_9_x_Max
#pragma mark- JPUSHRegisterDelegate
- (void)jpushNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(NSInteger))completionHandler {
     NSDictionary * userInfo = notification.request.content.userInfo;

    NSLog(@"MC推送7 == %@",userInfo);
//    NSString * srt = [NSString stringWithFormat:@"7===%@",userInfo];
//    kAlertMessage(srt);

    
}

- (void)jpushNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void (^)())completionHandler {
    
     NSDictionary * userInfo = response.notification.request.content.userInfo;
    NSLog(@"MC推送8 ==%@",userInfo);
    NSString * srt = [NSString stringWithFormat:@"8===%@",userInfo];

//    userInfo=@{
//        @"id":@"50094"
//    };
    
//    BOOL isLaunch = NO;
//    CGFloat time =1.5;
//    if ([[self getCurrentVC] isKindOfClass:[LaunchViewController class]]) {
//        isLaunch = YES;
//        time = 5;
//    }
//
//    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(8 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//        kAlertMessage(srt);
//
//
//    });
//    [FBSUserData sharedData].alertID = @"50413";
   
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            if (userInfo[@"id"]) {
               
                NSString * idStr= [NSString stringWithFormat:@"%ld",[userInfo[@"id"] integerValue]];
        //        idStr = @"50094";
                
                BOOL isLaunch = NO;
                CGFloat time =.5;
                if ([[self getCurrentVC] isKindOfClass:[LaunchViewController class]]) {
                    isLaunch = YES;
                    [FBSUserData sharedData].alertID = idStr;
                        return;
//                    time = 7;
        //            [[NSUserDefaults standardUserDefaults] setObject:idStr?:@"" forKey:@"alertID"];
        //
        //            [[NSUserDefaults standardUserDefaults] synchronize];
        //            return;
                }
        //        [[NSUserDefaults standardUserDefaults] setObject:idStr?:@"" forKey:@"alertID"];
        //
        //        [[NSUserDefaults standardUserDefaults] synchronize];

                
                
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(time * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    NSLog(@"getCurrentVC ===%@",[self getCurrentVC]);
                    if (![[self getCurrentVC] isKindOfClass:[LaunchViewController class]]) {
                        DetailViewController *detail = [[DetailViewController alloc] initWithArticleId:idStr htmlAddressL:@"article/" cellAddress:@"interest/"];

                        FBSTabBarController * TabBar2 =(FBSTabBarController*)[self getCurrentVC];
                        
                        UIViewController * vv =[self topViewControllerWithRootViewController:TabBar2];

                        [vv.navigationController pushViewController:detail animated:YES];

                    }


                    
                });
                
                
            }
            


    });

    
    


    

    
    
    
    

    
}
- (UIViewController*)topViewControllerWithRootViewController:(UIViewController*)rootViewController {
    if ([rootViewController isKindOfClass:[UITabBarController class]]) {
        UITabBarController* tabBarController = (UITabBarController*)rootViewController;
        return [self topViewControllerWithRootViewController:tabBarController.selectedViewController];
    } else if ([rootViewController isKindOfClass:[UINavigationController class]]) {
        UINavigationController* navigationController = (UINavigationController*)rootViewController;
        return [self topViewControllerWithRootViewController:navigationController.visibleViewController];
    } else if (rootViewController.presentedViewController) {
        UIViewController* presentedViewController = rootViewController.presentedViewController;
        return [self topViewControllerWithRootViewController:presentedViewController];
    } else {
        return rootViewController;
    }
}

#pragma mark - 获取当前屏幕显示的viewcontroller
- (UIViewController *)getCurrentVC
{
    UIViewController *result = nil;
    
    UIWindow * window = [[UIApplication sharedApplication] keyWindow];
    if (window.windowLevel != UIWindowLevelNormal)
    {
        NSArray *windows = [[UIApplication sharedApplication] windows];
        for(UIWindow * tmpWin in windows)
        {
            if (tmpWin.windowLevel == UIWindowLevelNormal)
            {
                window = tmpWin;
                break;
            }
        }
    }
    
    UIView *frontView = [[window subviews] objectAtIndex:0];
    id nextResponder = [frontView nextResponder];
    
    if ([nextResponder isKindOfClass:[UIViewController class]])
        result = nextResponder;
    else
        result = window.rootViewController;
    
    return result;
}



#endif

#pragma mark-推送
-(void)prepareTuisong{
    NSNotificationCenter *defaultCenter = [NSNotificationCenter defaultCenter];
    [defaultCenter addObserver:self
                      selector:@selector(networkDidSetup:)
                          name:kJPFNetworkDidSetupNotification
                        object:nil];
    [defaultCenter addObserver:self
                      selector:@selector(networkDidClose:)
                          name:kJPFNetworkDidCloseNotification
                        object:nil];
    [defaultCenter addObserver:self
                      selector:@selector(networkDidRegister:)
                          name:kJPFNetworkDidRegisterNotification
                        object:nil];
    [defaultCenter addObserver:self
                      selector:@selector(networkDidLogin:)
                          name:kJPFNetworkDidLoginNotification
                        object:nil];
    [defaultCenter addObserver:self
                      selector:@selector(networkDidReceiveMessage:)
                          name:kJPFNetworkDidReceiveMessageNotification
                        object:nil];
    [defaultCenter addObserver:self
                      selector:@selector(serviceError:)
                          name:kJPFServiceErrorNotification
                        object:nil];
    
    
    
}
- (void)networkDidSetup:(NSNotification *)notification {
    NSLog(@"已连接");
    
}

- (void)networkDidClose:(NSNotification *)notification {
    NSLog(@"未连接");
}

- (void)networkDidRegister:(NSNotification *)notification {
    NSLog(@"%@", [notification userInfo]);
    
    NSLog(@"已注册");
}

- (void)networkDidLogin:(NSNotification *)notification {
    
    NSLog(@"已登录");
    NSString * ss = self.registrationID?:@"";
    if (ss.length) {
        
        [JPUSHService setAlias:ss completion:^(NSInteger iResCode, NSString *iAlias, NSInteger seq) {
            
            NSLog(@"iResCode ===%zd",iResCode);
            NSLog(@"alias回调:%@", iAlias);

            
            
        } seq:nil];
//
//        [JPUSHService setAlias:ss
//              callbackSelector:@selector(tagsAliasCallback:tags:alias:)
//                        object:self];
//
        
    }
    
    if ([JPUSHService registrationID]) {
        
        
        NSLog(@"get RegistrationID");
    }
}

- (void)networkDidReceiveMessage:(NSNotification *)notification {
    
    
}

// log NSSet with UTF8
// if not ,log will be \Uxxx
- (NSString *)logDic:(NSDictionary *)dic {
    if (![dic count]) {
        return nil;
    }
    NSString *tempStr1 =
    [[dic description] stringByReplacingOccurrencesOfString:@"\\u"
                                                 withString:@"\\U"];
    NSString *tempStr2 =
    [tempStr1 stringByReplacingOccurrencesOfString:@"\"" withString:@"\\\""];
    NSString *tempStr3 =
    [[@"\"" stringByAppendingString:tempStr2] stringByAppendingString:@"\""];
    NSData *tempData = [tempStr3 dataUsingEncoding:NSUTF8StringEncoding];
    NSString *str =
    [NSPropertyListSerialization propertyListFromData:tempData
                                     mutabilityOption:NSPropertyListImmutable
                                               format:NULL
                                     errorDescription:NULL];
    return str;
}

- (void)serviceError:(NSNotification *)notification {
    NSDictionary *userInfo = [notification userInfo];
    NSString *error = [userInfo valueForKey:@"error"];
    NSLog(@"%@", error);
}
- (void)tagsAliasCallback:(int)iResCode
                     tags:(NSSet *)tags
                    alias:(NSString *)alias {
    
    
    NSLog(@"iResCode ===%zd",iResCode);
    NSLog(@"alias回调:%@", alias);
}


#pragma mark - 屏幕旋转相关设置
-(UIInterfaceOrientationMask)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window
{
    if (self.allowRotation) {
        return UIInterfaceOrientationMaskAllButUpsideDown;
    }
    return UIInterfaceOrientationMaskPortrait;
}

#pragma mark - 检测网络状态变化
-(void)netWorkChangeEvent
{
    [[AFNetworkActivityIndicatorManager sharedManager] setEnabled:YES];
    NSURL *url = [NSURL URLWithString:@"http://baidu.com"];
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:url];
    [manager.reachabilityManager setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        self.netWorkStatesCode = status;
        switch (status) {
            case AFNetworkReachabilityStatusReachableViaWWAN:
                NSLog(@"当前使用的是流量模式");
                break;
            case AFNetworkReachabilityStatusReachableViaWiFi:
                NSLog(@"当前使用的是wifi模式");
                break;
            case AFNetworkReachabilityStatusNotReachable:
                NSLog(@"断网了");
                break;
            case AFNetworkReachabilityStatusUnknown:
                NSLog(@"变成了未知网络状态");
                break;
                
            default:
                break;
        }
        [[NSNotificationCenter defaultCenter]postNotificationName:@"netWorkChangeEventNotification" object:@(status)];
    }];
    [manager.reachabilityManager startMonitoring];
}


@end
