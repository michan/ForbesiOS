//
//  FBSInvoiceDescCell.h
//  Forbes
//
//  Created by 周灿华 on 2019/8/18.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSCell.h"

@interface FBSInvoiceDescItem : FBSItem
@property (nonatomic, strong) NSString *content;
@end



@interface FBSInvoiceDescCell : FBSCell

@end

