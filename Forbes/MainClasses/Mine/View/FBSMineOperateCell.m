//
//  FBSMineOperateCell.m
//  Forbes
//
//  Created by 周灿华 on 2019/7/27.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSMineOperateCell.h"

@implementation FBSMineOperateItem

@end



@interface FBSMineOperateView : UIControl
@property (nonatomic, strong) UIImageView *iconV;
@property (nonatomic, strong) UILabel *nameL;
@end


@implementation FBSMineOperateView

- (instancetype)initWithIcon:(NSString *)icon name:(NSString *)name {
    self = [super init];
    if (self) {
        [self addSubview:self.iconV];
        [self addSubview:self.nameL];
        
        self.iconV.image = kImageWithName(icon);
        self.nameL.text = name;
        
        [self.iconV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(WScale(20));
            make.centerX.mas_equalTo(0);
            make.width.height.mas_equalTo(WScale(17));
        }];
        
        [self.nameL mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.iconV.mas_bottom).offset(WScale(7));
            make.centerX.mas_equalTo(self.iconV );
            make.height.mas_equalTo(WScale(12));
        }];

    }
    return self;
}


- (UIImageView *)iconV {
    if (!_iconV) {
        _iconV  = [[UIImageView alloc] init];
    }
    return _iconV;
}



- (UILabel *)nameL {
    if (!_nameL) {
        _nameL  = [[UILabel alloc] init];
        _nameL.textColor = Hexcolor(0x999999);
        _nameL.font = kLabelFontSize12;
    }
    return _nameL;
}


@end


@interface FBSMineOperateCell ()
@property (nonatomic, strong) FBSMineOperateView *collectionV;
@property (nonatomic, strong) FBSMineOperateView *commentView;
@property (nonatomic, strong) FBSMineOperateView *praiseView;
@property (nonatomic, strong) FBSMineOperateView *historyView;
@end

@implementation FBSMineOperateCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self.contentView addSubview:self.collectionV];
        [self.contentView addSubview:self.commentView];
        [self.contentView addSubview:self.praiseView];
        [self.contentView addSubview:self.historyView];
        
        CGFloat viewW = kScreenWidth / 4.0;
        
        [self.commentView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.left.bottom.mas_equalTo(0);
            make.width.mas_equalTo(viewW);
        }];
        
       
        [self.collectionV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.bottom.mas_equalTo(0);
            make.left.mas_equalTo(self.commentView.mas_right);
            make.width.mas_equalTo(viewW);
        }];

        
        [self.praiseView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.bottom.mas_equalTo(0);
            make.left.mas_equalTo(self.collectionV.mas_right);
            make.width.mas_equalTo(viewW);
        }];

        
        [self.historyView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.bottom.mas_equalTo(0);
            make.left.mas_equalTo(self.praiseView.mas_right);
            make.width.mas_equalTo(viewW);
        }];

        
    }
    
    return self;
}



- (void)setItem:(FBSMineOperateItem *)item {
    if (![item isKindOfClass:[FBSMineOperateItem class]]) {
        return ;
    }
    
    [super setItem:item];

}


#pragma mark - property

- (FBSMineOperateView *)collectionV {
    if (!_collectionV) {
        _collectionV  = [[FBSMineOperateView alloc] initWithIcon:@"mine_collection" name:@"收藏"];
        WEAKSELF
        [_collectionV addBlockForControlEvents:UIControlEventTouchUpInside block:^(id  _Nonnull sender) {
            if (self.delegate && [self.delegate respondsToSelector:@selector(fbsCell:didClickButtonAtIndex:)]) {
                [weakSelf.delegate fbsCell:weakSelf didClickButtonAtIndex:1];
            }
        }];
        
    }
    return _collectionV;
}


- (FBSMineOperateView *)commentView {
    if (!_commentView) {
        _commentView  = [[FBSMineOperateView alloc] initWithIcon:@"mine_comment" name:@"评论"];
        WEAKSELF
        [_commentView addBlockForControlEvents:UIControlEventTouchUpInside block:^(id  _Nonnull sender) {
            if (self.delegate && [self.delegate respondsToSelector:@selector(fbsCell:didClickButtonAtIndex:)]) {
                [weakSelf.delegate fbsCell:weakSelf didClickButtonAtIndex:0];
            }
        }];
    }
    return _commentView;
}



- (FBSMineOperateView *)praiseView {
    if (!_praiseView) {
        _praiseView  = [[FBSMineOperateView alloc] initWithIcon:@"mine_praise" name:@"点赞"];
        WEAKSELF
        [_praiseView addBlockForControlEvents:UIControlEventTouchUpInside block:^(id  _Nonnull sender) {
            if (self.delegate && [self.delegate respondsToSelector:@selector(fbsCell:didClickButtonAtIndex:)]) {
                [weakSelf.delegate fbsCell:weakSelf didClickButtonAtIndex:2];
            }
        }];
    }
    return _praiseView;
}



- (FBSMineOperateView *)historyView {
    if (!_historyView) {
        _historyView  = [[FBSMineOperateView alloc] initWithIcon:@"mine_history" name:@"历史"];
        WEAKSELF
        [_historyView addBlockForControlEvents:UIControlEventTouchUpInside block:^(id  _Nonnull sender) {
            if (self.delegate && [self.delegate respondsToSelector:@selector(fbsCell:didClickButtonAtIndex:)]) {
                [weakSelf.delegate fbsCell:weakSelf didClickButtonAtIndex:3];
            }
        }];
    }
    return _historyView;
}


@end
