
//
//  FBSMineHeadCell.m
//  Forbes
//
//  Created by 周灿华 on 2019/7/27.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSMineHeadCell.h"


@implementation FBSMineHeadItem

@end



@interface FBSMineHeadCell ()
@property (nonatomic, strong) UIImageView *bgV;
@property (nonatomic, strong) UIImageView *avatarV;
@property (nonatomic, strong) UILabel *nameL;
@end

@implementation FBSMineHeadCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self.contentView addSubview:self.bgV];
        [self.bgV addSubview:self.avatarV];
        [self.bgV addSubview:self.nameL];
        
        [self.bgV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.mas_equalTo(0);
        }];
        
        self.avatarV.layer.masksToBounds = YES;
        self.avatarV.layer.cornerRadius = WScale(85) / 2;
        self.avatarV.clipsToBounds = YES;
        self.avatarV.contentMode = UIViewContentModeScaleAspectFill;
       
        [self.avatarV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(0);
            make.width.height.mas_equalTo(WScale(85));
        }];
        
        [self.nameL mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.avatarV.mas_bottom).offset(WScale(10));
            make.bottom.mas_equalTo(-WScale(8));
            make.centerX.mas_equalTo(0);
            make.height.mas_equalTo(WScale(21));
        }];
        
    }
    
    return self;
}



- (void)setItem:(FBSMineHeadItem *)item {
    if (![item isKindOfClass:[FBSMineHeadItem class]]) {
        return ;
    }
    
    [super setItem:item];
    self.nameL.text = item.name;
    NSString *header = [[NSUserDefaults standardUserDefaults] objectForKey:@"headerImage"];
    [self.avatarV sd_setImageWithURL:[NSURL URLWithString:header] placeholderImage:kImageWithName(@"default_avatar")];
}

#pragma mark - property

- (UIImageView *)bgV {
    if (!_bgV) {
        _bgV  = [[UIImageView alloc] init];
        _bgV.backgroundColor = MainThemeColor;
    }
    return _bgV;
}


- (UIImageView *)avatarV {
    if (!_avatarV) {
        _avatarV  = [[UIImageView alloc] init];
        _avatarV.backgroundColor = [UIColor clearColor];
//        _avatarV.layer.cornerRadius = WScale(85.0 / 2.0);
    }
    return _avatarV;
}



- (UILabel *)nameL {
    if (!_nameL) {
        _nameL  = [[UILabel alloc] init];
        _nameL.textColor = [UIColor whiteColor];
        _nameL.font = kLabelFontSize15;
    }
    return _nameL;
}


@end
