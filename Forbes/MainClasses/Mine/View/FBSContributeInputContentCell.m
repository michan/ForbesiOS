//
//  FBSContributeInputContentCell.m
//  Forbes
//
//  Created by 周灿华 on 2019/8/14.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSContributeInputContentCell.h"
#import "FBSTextView.h"
#import "CustomYYImage.h"
#import "ImageModel.h"

@implementation FBSContributeInputContentItem

- (instancetype)init {
    self = [super init];
    if (self) {
        self.canEdit = YES;
    }
    return self;
}


@end

@interface FBSContributeInputContentCell ()<FBSTextViewDelegate,YYTextViewDelegate>
{
    FBSContributeInputContentItem *_currentItem;
}
/** 输入框 */
@property (nonatomic, strong) YYTextView *textView;
/** value 属性 */
@property (nonatomic, strong) NSDictionary *valueAttributes;
/** placeholder 属性 */
@property (nonatomic, strong) NSMutableDictionary *placeholderAttributes;
@property (nonatomic, strong) UIButton *addImageBtn;

@end

@implementation FBSContributeInputContentCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self.contentView addSubview:self.textView];
        [self.contentView addSubview:self.addImageBtn];
        
        [self.textView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(WScale(15));
            make.top.mas_equalTo(0);
            make.right.mas_equalTo(5 - WScale(13));  //textView 的 inseet.right == 5
            make.bottom.mas_equalTo(self.addImageBtn.mas_top).offset(-WScale(10));
        }];
        
        
        [self.addImageBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(WScale(20));
            make.width.mas_equalTo(WScale(65));
            make.height.mas_equalTo(WScale(65));
            make.bottom.mas_equalTo(-WScale(15));
        }];

    }
    
    return self;
}



- (void)setItem:(FBSContributeInputContentItem *)item {
    if (![item isKindOfClass:[FBSContributeInputContentItem class]]) {
        return ;
    }
    
    [super setItem:item];
    
    
    _currentItem = item;
    
    self.textView.userInteractionEnabled = item.canEdit;
    
    if (item.placeholderColor) {
        self.placeholderAttributes[NSForegroundColorAttributeName] = item.placeholderColor;
    }
    
    
    NSAttributedString *placeholderAttStr = [[NSAttributedString alloc] initWithString:item.placeholder attributes:self.placeholderAttributes];
    self.textView.placeholderAttributedText = placeholderAttStr;
    
    if (item.AttributedString.length) {
        self.textView.attributedText = item.AttributedString;
    } else {
        self.textView.text = nil;
    }
}
- (void)textViewDidChange:(YYTextView *)textView{
    if ([self.item isKindOfClass:[FBSContributeInputContentItem class]]) {
        FBSContributeInputContentItem *itemInput = (FBSContributeInputContentItem *)self.item;
        itemInput.AttributedString = [[NSMutableAttributedString alloc] initWithAttributedString:textView.attributedText];
    }
}

- (void)addImageAction {
    if (self.delegate && [self.delegate respondsToSelector:@selector(fbsCell:didClickButtonAtIndex:)]) {
        [self.delegate fbsCell:self didClickButtonAtIndex:0];
    }
}

#pragma mark - FBSTextViewDelegate

-(BOOL)limitedTextViewShouldReturn:(UITextView *)textView{
    NSLog(@"点击了return");
    [textView resignFirstResponder];
    return NO;
}

- (void)limitedTextViewDidChange:(UITextView *)textView{
    NSLog(@"文字改变了 -- %@",textView.text);
    _currentItem.content = textView.text;
    
    UITextRange *selectedRange = [textView markedTextRange];
    UITextPosition *pos = [textView positionFromPosition:selectedRange.start offset:0];
    if (selectedRange && pos) {
        return;
    }
    textView.attributedText = [[NSAttributedString alloc] initWithString:textView.text attributes:self.valueAttributes];
    
    //    self.countLb.text = [NSString stringWithFormat:@"%zi/%zi",self.textView.inputLength,self.textView.maxLength];
}


-(void)limitedTextViewDidEndEditing:(UITextView *)textView{
    NSLog(@"结束编辑 - text:%@",textView.text);
}

- (void)create:(NSArray *)photos{
    _textViewRange = _textView.selectedRange;
    NSMutableAttributedString *contentText = [_textView.attributedText mutableCopy];
    for (NSInteger i = photos.count - 1; i >= 0; i--) {
        
        UIImage * image = photos[i];
        
        //此处省略了调用七牛上传图片，拼接得到图片的url
        
        NSString*altStr = [NSString stringWithFormat:@"http://7xojj3.com1.z0.glb.clouddn.com/image/sharepic/%@",@"imageName"];
        CustomYYImage *imageView = [[CustomYYImage alloc] initWithDataSourece:self];
        
        
        imageView.imageW = image.size.width;
        imageView.imageH = image.size.height;
        
        float scale = (kScreenWidth-30)/image.size.width;
        image = [self imageByScalingAndCroppingForSize:CGSizeMake(kScreenWidth-30, image.size.height*scale) forImage:image];
        imageView.image = image;
        imageView.urlStr = altStr;
        imageView.location = _textViewRange.location + i * 2 + 1;
        contentText = [[self p_textViewAttributedText:imageView contentText:contentText index:_textViewRange.location originPoint:[_textView caretRectForPosition:_textView.selectedTextRange.start].origin isData:NO] mutableCopy];
        
        [contentText insertAttributedString:[[NSAttributedString alloc] initWithString:@"\n"] atIndex:_textViewRange.location + 1 * 2];
    }
    //    [contentText insertAttributedString:[[NSAttributedString alloc] initWithString:@"\n"] atIndex:_textViewRange.location + photos.count * 2];
    [contentText setFont:[UIFont systemFontOfSize:16]];
    contentText.lineSpacing = 15;
    _textView.attributedText = contentText;
    if ([self.item isKindOfClass:[FBSContributeInputContentItem class]]) {
        FBSContributeInputContentItem *itemInput = (FBSContributeInputContentItem *)self.item;
        itemInput.AttributedString = contentText;
    }
    self.textView.selectedRange = NSMakeRange(contentText.length, 0);
}
#pragma mark - YYTextView Get Attributed String
- (NSAttributedString *)p_textViewAttributedText:(id)attribute contentText:(NSAttributedString *)attributeString index:(NSInteger)index originPoint:(CGPoint)originPoint isData:(BOOL)isData {
    NSMutableAttributedString *contentText = [attributeString mutableCopy];
    NSAttributedString *textAttachmentString = [[NSAttributedString alloc] initWithString:@"\n"];
    if ([attribute isKindOfClass:[CustomYYImage class]]) {
        CustomYYImage *imageView = (CustomYYImage *)attribute;
        CGFloat imageViewHeight = ![imageView.title isEqualToString:@""] ? kScreenWidth + 30.0 : kScreenWidth;
        float scale = (kScreenWidth-30)/imageView.imageW;
        imageView.frame = CGRectMake(originPoint.x, originPoint.y, kScreenWidth-30, imageView.imageH*scale);
        //        imageView.frame = CGRectMake(originPoint.x, originPoint.y, SIZE.width-30, 200);
        
        NSMutableAttributedString *attachText = [NSMutableAttributedString attachmentStringWithContent:imageView contentMode:UIViewContentModeScaleAspectFit attachmentSize:imageView.frame.size alignToFont:_textView.font alignment:YYTextVerticalAlignmentCenter];
        if (!isData) [contentText insertAttributedString:textAttachmentString atIndex:index++];
        [contentText insertAttributedString:attachText atIndex:index++];
        
        
    }
    return contentText;
}
- (NSArray *)p_trimIsMove:(BOOL)isMove {
    NSInteger currentIndex = 0;
    NSString *dataString = _textView.attributedText.string;
    NSMutableArray *data = [[dataString componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]] mutableCopy];
    NSMutableArray *result = [NSMutableArray new];
    for (int i = 0; i < data.count; i++) {
        BOOL isChangedIndex = NO;
        int attachmentIndex = 0;
        for (int j = attachmentIndex; j < _textView.textLayout.attachmentRanges.count; j++) {
            if ([_textView.textLayout.attachmentRanges[j] rangeValue].location == currentIndex) {
                if ([_textView.textLayout.attachments[j].content isKindOfClass:[CustomYYImage class]])
                {
                    CustomYYImage *imageView = _textView.textLayout.attachments[j].content;
                    if (!isChangedIndex) {
                        if (isMove) {
                            [result addObject:imageView];
                        } else {
                            ImageModel *model = [ImageModel new];
                            model.image = imageView.image;
                            model.point = imageView.origin;
                            model.abbrUrl = imageView.urlStr;
                            if (imageView.title) model.title = imageView.title;
                            [result addObject:model];
                        }
                        currentIndex += 2;
                        isChangedIndex = YES;
                        attachmentIndex ++;
                    }
                }
            }
        }
        if (!isChangedIndex) {
            NSString *string = data[i];
            currentIndex += string.length + 1;
            if (![string isEqualToString:@""]) {
                [result addObject:string];
            }
        }
    }
    return result;
}
//图片伸缩到指定大小
- (UIImage*)imageByScalingAndCroppingForSize:(CGSize)targetSize forImage:(UIImage *)originImage
{
    UIImage *sourceImage = originImage;// 原图
    UIImage *newImage = nil;// 新图
    // 原图尺寸
    CGSize imageSize = sourceImage.size;
    CGFloat width = imageSize.width;
    CGFloat height = imageSize.height;
    CGFloat targetWidth = targetSize.width;// 目标宽度
    CGFloat targetHeight = targetSize.height;// 目标高度
    // 伸缩参数初始化
    CGFloat scaleFactor = 0.0;
    CGFloat scaledWidth = targetWidth;
    CGFloat scaledHeight = targetHeight;
    CGPoint thumbnailPoint = CGPointMake(0.0,0.0);
    
    if (CGSizeEqualToSize(imageSize, targetSize) == NO)
    {// 如果原尺寸与目标尺寸不同才执行
        CGFloat widthFactor = targetWidth / width;
        CGFloat heightFactor = targetHeight / height;
        
        if (widthFactor > heightFactor)
            scaleFactor = widthFactor; // 根据宽度伸缩
        else
            scaleFactor = heightFactor; // 根据高度伸缩
        scaledWidth= width * scaleFactor;
        scaledHeight = height * scaleFactor;
        
        // 定位图片的中心点
        if (widthFactor > heightFactor)
        {
            thumbnailPoint.y = (targetHeight - scaledHeight) * 0.5;
        }
        else if (widthFactor < heightFactor)
        {
            thumbnailPoint.x = (targetWidth - scaledWidth) * 0.5;
        }
    }
    
    // 创建基于位图的上下文
    UIGraphicsBeginImageContext(targetSize);
    
    // 目标尺寸
    CGRect thumbnailRect = CGRectZero;
    thumbnailRect.origin = thumbnailPoint;
    thumbnailRect.size.width= scaledWidth;
    thumbnailRect.size.height = scaledHeight;
    
    [sourceImage drawInRect:thumbnailRect];
    
    // 新图片
    newImage = UIGraphicsGetImageFromCurrentImageContext();
    if(newImage == nil)
        NSLog(@"could not scale image");
    
    // 退出位图上下文
    UIGraphicsEndImageContext();
    return newImage;
}

#pragma mark - property


- (YYTextView *)textView {
    if (!_textView) {
        _textView = [[YYTextView alloc] init];
        _textView.delegate = self;
        _textView.returnKeyType = UIReturnKeyDone;
        _textView.font = [UIFont systemFontOfSize:16];
        _textView.textColor = RGB(50, 50, 50);
        _textView.textAlignment = NSTextAlignmentLeft;
        _textView.userInteractionEnabled = YES;
        _textView.textVerticalAlignment = YYTextVerticalAlignmentTop;
        [self.contentView addSubview:_textView];
    }
    return _textView;
}


- (UIButton *)addImageBtn {
    if (!_addImageBtn) {
        _addImageBtn  = [UIButton buttonWithType:UIButtonTypeCustom];
        [_addImageBtn setImage:kImageWithName(@"contribute_addPhoto") forState:UIControlStateNormal];
        [_addImageBtn addTarget:self action:@selector(addImageAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _addImageBtn;
}


- (NSDictionary *)valueAttributes {
    if (!_valueAttributes) {
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        paragraphStyle.lineSpacing = 5;//字体的行间距
        _valueAttributes = @{
                             NSFontAttributeName:kLabelFontSize15,
                             NSForegroundColorAttributeName : RGB(50,50,50),
                             NSParagraphStyleAttributeName:paragraphStyle
                             };
        
    }
    return _valueAttributes;
}

- (NSDictionary *)placeholderAttributes {
    if (!_placeholderAttributes) {
        NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc] init];
        style.lineSpacing = 5;
        
        NSDictionary *dict = @{
                               NSFontAttributeName : kLabelFontSize15,
                               NSForegroundColorAttributeName : Hexcolor(0x999999),
                               NSParagraphStyleAttributeName : style
                               };
        _placeholderAttributes = [dict mutableCopy];
    }
    return _placeholderAttributes;
}

@end


