//
//  FBSMineInfoCell.h
//  Forbes
//
//  Created by 周灿华 on 2019/8/8.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSCell.h"

@interface FBSMineInfoItem : FBSItem
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *detail;
@property (nonatomic, copy) NSString *imageUrl;
@end


@interface FBSMineInfoCell : FBSCell

@end
