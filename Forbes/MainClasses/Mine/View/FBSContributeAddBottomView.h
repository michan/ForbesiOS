//
//  FBSContributeAddBottomView.h
//  Forbes
//
//  Created by 周灿华 on 2019/8/11.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import <UIKit/UIKit.h>

#define BottomViewH (WScale(25) + WScale(50))

@interface FBSContributeAddBottomView : UIView

@property (nonatomic, strong) void (^addBlock)(void);

@end


