//
//  FBSMineInfoCell.m
//  Forbes
//
//  Created by 周灿华 on 2019/8/8.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSMineInfoCell.h"

@implementation FBSMineInfoItem

@end

@interface FBSMineInfoCell ()
@property (nonatomic, strong) UILabel *titleL;
@property (nonatomic, strong) UILabel *detailL;
@property (nonatomic, strong) UIImageView *iconV;
@end

@implementation FBSMineInfoCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self.contentView addSubview:self.titleL];
        [self.contentView addSubview:self.detailL];
        [self.contentView addSubview:self.iconV];
        
        [self.titleL mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(WScale(15));
            make.centerY.mas_equalTo(0);
        }];
        
        [self.iconV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(0);
            make.right.mas_equalTo(-WScale(30));
            make.width.mas_equalTo(WScale(28));
            make.height.mas_equalTo(WScale(28));
        }];
        
        
        [self.detailL mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(self.iconV);
            make.centerY.mas_equalTo(0);
        }];

    }
    
    return self;
}



- (void)setItem:(FBSMineInfoItem *)item {
    if (![item isKindOfClass:[FBSMineInfoItem class]]) {
        return ;
    }
    
    [super setItem:item];
    
    self.titleL.text = item.title;
    self.detailL.text = item.detail;
    
    self.detailL.hidden = !item.detail.length;
    self.iconV.hidden = !item.imageUrl.length;
    
    if (item.imageUrl.length) {
        [_iconV sd_setImageWithURL:[NSURL URLWithString:item.imageUrl] placeholderImage:kImageWithName(item.imageUrl)];
    }
}

#pragma mark - property

- (UIImageView *)iconV {
    if (!_iconV) {
        _iconV  = [[UIImageView alloc] init];
        _iconV.image = kImageWithName(@"default_avatar");
        _iconV.layer.cornerRadius = WScale(14);
    }
    return _iconV;
}



- (UILabel *)titleL {
    if (!_titleL) {
        _titleL  = [[UILabel alloc] init];
        _titleL.textColor = Hexcolor(0x333333);
        _titleL.font = kLabelFontSize14;
    }
    return _titleL;
}




- (UILabel *)detailL {
    if (!_detailL) {
        _detailL  = [[UILabel alloc] init];
        _detailL.textColor = Hexcolor(0x333333);
        _detailL.font = kLabelFontSize14;
    }
    return _detailL;
}


@end
