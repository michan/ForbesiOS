//
//  FBSContributeListCell.h
//  Forbes
//
//  Created by 周灿华 on 2019/8/10.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSCell.h"

@interface FBSContributeListItem : FBSItem
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *time;
@property (nonatomic, copy) NSString *imgUrl;
@end



@interface FBSContributeListCell : FBSCell

@end
