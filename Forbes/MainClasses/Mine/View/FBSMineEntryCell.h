//
//  FBSMineEntryCell.h
//  Forbes
//
//  Created by 周灿华 on 2019/7/27.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSCell.h"

@interface FBSMineEntryItem : FBSItem
@property (nonatomic, copy) NSString *title;
@end


@interface FBSMineEntryCell : FBSCell

@end

