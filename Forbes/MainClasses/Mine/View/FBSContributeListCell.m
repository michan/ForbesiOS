
//
//  FBSContributeListCell.m
//  Forbes
//
//  Created by 周灿华 on 2019/8/10.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSContributeListCell.h"

#define EdgeMargin WScale(15)
#define ImageH     WScale(200)

@implementation FBSContributeListItem

- (CGFloat)calculateCellHeight {
    CGFloat cellHeight = EdgeMargin;
    
    NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc] init];
    style.lineSpacing = 5;
    
    NSDictionary *dict = @{
                           NSFontAttributeName : kLabelFontSize18,
                           NSParagraphStyleAttributeName : style,
                           };
    
    
    if (self.title.length) {
        CGFloat textH = [self.title boundingRectWithSize:CGSizeMake(kScreenWidth  - 2 * EdgeMargin, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:dict context:NULL].size.height;
        if (textH > kLabelFontSize18.lineHeight * 2 + 5) {
            textH = kLabelFontSize18.lineHeight * 2 + 5;
        }
        cellHeight += textH;
    }
    
    if (self.imgUrl.length) {
        cellHeight += WScale(10) + ImageH;
    }
    
    cellHeight += WScale(10) + WScale(11) +  WScale(10);
    
    return cellHeight;
    
}


@end


@interface FBSContributeListCell ()
@property (nonatomic, strong) UILabel *titleL;
@property (nonatomic, strong) UIImageView *imageV;
@property (nonatomic, strong) UILabel *descL;
@property (nonatomic, strong) NSDictionary *attributeDic;
@end

@implementation FBSContributeListCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self.contentView addSubview:self.titleL];
        [self.contentView addSubview:self.imageV];
        [self.contentView addSubview:self.descL];
        
        [self.titleL mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(EdgeMargin);
            make.left.mas_equalTo(EdgeMargin);
            make.width.mas_equalTo(kScreenWidth - 2*EdgeMargin);
        }];
        
        
        [self.imageV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.titleL.mas_bottom).offset(WScale(10));
            make.left.mas_equalTo(EdgeMargin);
            make.right.mas_equalTo(-EdgeMargin);
            make.height.mas_equalTo(ImageH);
        }];
        
        
        [self.descL mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(EdgeMargin);
            make.height.mas_equalTo(WScale(11));
            make.bottom.mas_equalTo(-WScale(10));
        }];
        
        
    }
    
    return self;
}



- (void)setItem:(FBSContributeListItem *)item {
    if (![item isKindOfClass:[FBSContributeListItem class]]) {
        return ;
    }
    
    [super setItem:item];
    self.titleL.attributedText = [[NSAttributedString alloc] initWithString:item.title attributes:self.attributeDic];
    self.titleL.lineBreakMode = NSLineBreakByTruncatingTail;
    self.descL.text = [NSString stringWithFormat:@"最近更新时间 %@",item.time];
    self.imageV.hidden = !item.imgUrl.length;
}

#pragma mark - property

- (UILabel *)titleL {
    if (!_titleL) {
        _titleL  = [[UILabel alloc] init];
        _titleL.textColor = Hexcolor(0x333333);
        _titleL.font = kLabelFontSize18;
        _titleL.numberOfLines = 2;
    }
    return _titleL;
}

- (UILabel *)descL {
    if (!_descL) {
        _descL  = [[UILabel alloc] init];
        _descL.textColor = Hexcolor(0x666666);
        _descL.font = kLabelFontSize11;
    }
    return _descL;
}

- (UIImageView *)imageV {
    if (!_imageV) {
        _imageV  = [[UIImageView alloc] init];
        _imageV.backgroundColor = RandomColor;
    }
    return _imageV;
}

- (NSDictionary *)attributeDic {
    if (!_attributeDic) {
        NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc] init];
        style.lineSpacing = 5;
        
        _attributeDic  = @{
                           NSForegroundColorAttributeName : Hexcolor(0x333333),
                           NSFontAttributeName : kLabelFontSize18,
                           NSParagraphStyleAttributeName : style
                           };
    }
    return _attributeDic;
}


@end
