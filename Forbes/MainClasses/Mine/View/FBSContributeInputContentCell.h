//
//  FBSContributeInputContentCell.h
//  Forbes
//
//  Created by 周灿华 on 2019/8/14.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSCell.h"

@interface FBSContributeInputContentItem : FBSItem
/** 值 */
@property (nonatomic, strong) NSString *content;
/** 输入框占位文字 */
@property (nonatomic, strong) NSString *placeholder;
/** 输入框占位文字颜色 */
@property (nonatomic, strong) UIColor *placeholderColor;
/** 是否能输入,默认为yes */
@property (nonatomic, assign) BOOL canEdit;

/** 是否能输入,默认为yes */
@property (nonatomic, strong) NSMutableAttributedString *AttributedString;

@end


@interface FBSContributeInputContentCell : FBSCell
@property (nonatomic, assign) NSRange textViewRange;
- (void)create:(NSArray *)photos;
- (NSArray *)p_trimIsMove:(BOOL)isMove;
@end

