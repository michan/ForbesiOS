
//
//  FBSInvoiceListCell.m
//  Forbes
//
//  Created by 周灿华 on 2019/8/17.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSInvoiceListCell.h"

#define NormalMargin WScale(18)
#define EditOffset   WScale(28)

@implementation FBSInvoiceListItem

@end



@interface FBSInvoiceListCell ()
@property (nonatomic, strong) UIImageView *bgV;
@property (nonatomic, strong) UILabel *orderL;
@property (nonatomic, strong) UILabel *titleL;
@property (nonatomic, strong) UILabel *timeL;
@property (nonatomic, strong) UIButton *editBtn;
@property (nonatomic, strong) UIButton *operateBtn;
@end

@implementation FBSInvoiceListCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self.contentView addSubview:self.editBtn];
        [self.contentView addSubview:self.bgV];
        [self.bgV addSubview:self.orderL];
        [self.bgV addSubview:self.titleL];
        [self.bgV addSubview:self.timeL];
        [self.bgV addSubview:self.operateBtn];
        
        
        [self.editBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(WScale(13));
            make.top.mas_equalTo(20);
            make.width.height.mas_equalTo(WScale(13));
        }];

        
        
        [self.bgV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(10);
            make.left.mas_equalTo(NormalMargin);
            make.height.mas_equalTo(WScale(150));
            make.right.mas_equalTo(-NormalMargin);
        }];
        

        [self.orderL mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(WScale(10));
            make.left.mas_equalTo(WScale(10));
            make.height.mas_equalTo(WScale(11));
        }];
        
        
        [self.titleL mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(WScale(40));
            make.left.mas_equalTo(WScale(10));
            make.right.mas_equalTo(-WScale(10));
        }];

        
        
        [self.timeL mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.mas_equalTo(-WScale(40));
            make.left.mas_equalTo(WScale(10));
            make.height.mas_equalTo(WScale(11));
        }];
        
        
        
        [self.operateBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.mas_equalTo(-WScale(4));
            make.right.mas_equalTo(-WScale(10));
            make.width.mas_equalTo(WScale(92));
            make.height.mas_equalTo(WScale(22));
        }];
    }
    
    return self;
}



- (void)setItem:(FBSInvoiceListItem *)item {
    if (![item isKindOfClass:[FBSInvoiceListItem class]]) {
        return ;
    }
    
    [super setItem:item];
    
    self.editBtn.hidden = !item.isEdit;
    self.editBtn.selected = item.isSelected;
    self.operateBtn.selected = item.isDetail;
    self.orderL.text = [NSString stringWithFormat:@"订单号：%@",item.order];
    self.titleL.text = item.title;
    self.timeL.text = [NSString stringWithFormat:@"活动时间：%@",item.order];
    
    if (item.isEdit) {
        [self.bgV mas_updateConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(NormalMargin + EditOffset);
            make.right.mas_equalTo(-NormalMargin + EditOffset);
        }];

    } else {
        [self.bgV mas_updateConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(NormalMargin);
            make.right.mas_equalTo(-NormalMargin);
        }];

    }

}

#pragma mark - property

- (UIImageView *)bgV {
    if (!_bgV) {
        _bgV  = [[UIImageView alloc] init];
        _bgV.image = kImageWithName(@"invoice_listborder");
    }
    return _bgV;
}


- (UILabel *)orderL {
    if (!_orderL) {
        _orderL  = [[UILabel alloc] init];
        _orderL.textColor = Hexcolor(0x999999);
        _orderL.font = kLabelFontSize11;
    }
    return _orderL;
}


- (UILabel *)titleL {
    if (!_titleL) {
        _titleL  = [[UILabel alloc] init];
        _titleL.textColor = RGB(74,74,74);
        _titleL.font = kLabelFontSize17;
        _titleL.numberOfLines = 2;
    }
    return _titleL;
}



- (UILabel *)timeL {
    if (!_timeL) {
        _timeL  = [[UILabel alloc] init];
        _timeL.textColor = Hexcolor(0x999999);
        _timeL.font = kLabelFontSize11;
    }
    return _timeL;
}


- (UIButton *)editBtn {
    if (!_editBtn) {
        _editBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_editBtn setBackgroundImage:kImageWithName(@"invoice_editunsel") forState:UIControlStateNormal];
        [_editBtn setBackgroundImage:kImageWithName(@"invoice_editsel") forState:UIControlStateSelected];
    }
    return _editBtn;
}


- (UIButton *)operateBtn {
    if (!_operateBtn) {
        _operateBtn  = [UIButton buttonWithType:UIButtonTypeCustom];
        [_operateBtn setBackgroundImage:kImageWithName(@"invoice_apply") forState:UIControlStateNormal];
        [_operateBtn setBackgroundImage:kImageWithName(@"invoice_detail") forState:UIControlStateSelected];
    }
    return _operateBtn;
}


@end
