//
//  FBSMyActivityListItem.h
//  Forbes
//
//  Created by 赵志辉 on 2019/9/29.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import "FBSItem.h"

NS_ASSUME_NONNULL_BEGIN

@interface FBSMyActivityListItem : FBSItem
@property (nonatomic , copy) NSString              * id;
@property (nonatomic , copy) NSString              * activity_ticket_code;
@property (nonatomic , copy) NSString              * status;
@property (nonatomic , copy) NSString              * is_ok;
@property (nonatomic , copy) NSString              * created_at;
@property (nonatomic , copy) NSString              * type;
@property (nonatomic , copy) NSString              * remarks;
@property (nonatomic , copy) NSString              * ticket_title;
@property (nonatomic , copy) NSString              * title;
@end

NS_ASSUME_NONNULL_END
