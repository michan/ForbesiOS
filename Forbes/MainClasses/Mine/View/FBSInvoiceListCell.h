//
//  FBSInvoiceListCell.h
//  Forbes
//
//  Created by 周灿华 on 2019/8/17.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSCell.h"

@interface FBSInvoiceListItem : FBSItem
@property (nonatomic, assign) BOOL isEdit;
@property (nonatomic, assign) BOOL isSelected;
@property (nonatomic, assign) BOOL isDetail;
@property (nonatomic, copy) NSString *order;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *time;
@end

@interface FBSInvoiceListCell : FBSCell

@end


