//
//  FBSOnlyOneInputItem.h
//  Forbes
//
//  Created by 赵志辉 on 2019/10/9.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import "FBSItem.h"

NS_ASSUME_NONNULL_BEGIN

@interface FBSOnlyOneInputItem : FBSItem

@property(nonatomic, strong)NSString *text;
@property(nonatomic, strong)NSString *placeholder;
@property(nonatomic, strong)NSString *placeholderColor;



@end

NS_ASSUME_NONNULL_END
