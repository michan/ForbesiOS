//
//  FBSContributeAddContentCell.h
//  Forbes
//
//  Created by 周灿华 on 2019/8/15.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSCell.h"

@interface FBSContributeAddContentItem : FBSItem
@property (nonatomic, strong) NSString *content;
@property (nonatomic, assign) BOOL isSelecte;

@end


@interface FBSContributeAddContentCell : FBSCell

@end
