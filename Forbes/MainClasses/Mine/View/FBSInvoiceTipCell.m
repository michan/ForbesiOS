//
//  FBSInvoiceTipCell.m
//  Forbes
//
//  Created by 周灿华 on 2019/8/18.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSInvoiceTipCell.h"

@implementation FBSInvoiceTipItem

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.bottomLineHidden = YES;
    }
    return self;
}

@end


@interface FBSInvoiceTipCell ()
@property (nonatomic, strong) UIView *bgV;
@property (nonatomic, strong) UILabel *tipL;

@end

@implementation FBSInvoiceTipCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self.contentView addSubview:self.bgV];
        [self.bgV addSubview:self.tipL];
        
        [self.bgV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(WScale(15));
            make.right.mas_equalTo(-WScale(15));
            make.height.mas_equalTo(WScale(40));
            make.top.mas_equalTo(0);
        }];
        
        [self.tipL mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(WScale(8));
            make.left.mas_equalTo(WScale(15));
            make.height.mas_equalTo(WScale(12));
        }];
    }
    
    return self;
}



- (void)setItem:(FBSInvoiceTipItem *)item {
    if (![item isKindOfClass:[FBSInvoiceTipItem class]]) {
        return ;
    }
    
    [super setItem:item];
    
 
}


#pragma mark - property

- (UIView *)bgV {
    if (!_bgV) {
        _bgV  = [[UIView alloc] init];
        _bgV.backgroundColor = RGB(246,247,249);
    }
    return _bgV;
}


- (UILabel *)tipL {
    if (!_tipL) {
        _tipL  = [[UILabel alloc] init];
        _tipL.font  = kLabelFontSize12;
        _tipL.textColor = RGB(153, 153, 153);
        _tipL.text = @"提交发票抬头讲不可修改";
    }
    return _tipL;
}


@end
