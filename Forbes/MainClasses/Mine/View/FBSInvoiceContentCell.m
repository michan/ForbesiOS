//
//  FBSInvoiceContentCell.m
//  Forbes
//
//  Created by 周灿华 on 2019/8/18.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSInvoiceContentCell.h"


@implementation FBSInvoiceContentItem

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.keyboardType = UIKeyboardTypeDefault;
        self.canEdit = YES;
        self.showArrow = NO;
        self.bottomLineInsets = UIEdgeInsetsMake(0, WScale(15), 0, WScale(15));
        self.cellHeight = WScale(45);
    }
    return self;
}

@end



@interface FBSInvoiceContentCell ()<UITextFieldDelegate>
{
    FBSInvoiceContentItem *_curentItem;
}
@property (nonatomic, strong) UILabel *titleL;
@property (nonatomic, strong) UITextField *inputTF;
@property (nonatomic, strong) UIImageView *arrowV;
@end

@implementation FBSInvoiceContentCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self.contentView addSubview:self.titleL];
        [self.contentView addSubview:self.inputTF];
        [self.contentView addSubview:self.arrowV];
        
        [self.titleL mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(WScale(15));
            make.centerY.mas_equalTo(0);
            make.width.mas_equalTo(WScale(120 - 5));
        }];
        
        
        
        [self.inputTF mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(WScale(135));
            make.height.mas_equalTo(self.contentView);
            make.right.mas_equalTo(-WScale(20));
            make.centerY.mas_equalTo(0);
        }];
        
        
        [self.arrowV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(-WScale(20));
            make.width.mas_equalTo(WScale(7));
            make.height.mas_equalTo(WScale(12));
            make.centerY.mas_equalTo(0);
        }];
        
   
    }
    
    return self;
}



- (void)setItem:(FBSInvoiceContentItem *)item {
    if (![item isKindOfClass:[FBSInvoiceContentItem class]]) {
        return ;
    }
    
    [super setItem:item];
    _curentItem = item;
    
    self.titleL.text = item.title;
    self.inputTF.userInteractionEnabled = item.canEdit;
    self.inputTF.placeholder = item.placeholder;
    self.inputTF.text = item.content;
    self.inputTF.keyboardType = item.keyboardType;
    self.arrowV.hidden = !item.showArrow;
}

#pragma mark - UITextFieldDelegete

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}


- (void)textFieldDidEndEditing:(UITextField *)textField {
    _curentItem.content = textField.text;
}


#pragma mark - property

- (UILabel *)titleL {
    if (!_titleL) {
        _titleL  = [[UILabel alloc] init];
        _titleL.textColor = RGB(51,51,51);
        _titleL.font = kLabelFontSize16;
        _titleL.adjustsFontSizeToFitWidth = YES;
    }
    return _titleL;
}


- (UITextField *)inputTF {
    if (!_inputTF) {
        _inputTF  = [[UITextField alloc] init];
        _inputTF.font = kLabelFontSize14;
        _inputTF.textColor = Hexcolor(0x333333);
        _inputTF.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"" attributes:@{NSFontAttributeName:kLabelFontSize14,NSForegroundColorAttributeName:RGB(51, 51, 51)}];
        _inputTF.returnKeyType = UIReturnKeyDone;
        _inputTF.clearButtonMode = UITextFieldViewModeWhileEditing;
        _inputTF.delegate = self;
    }
    return _inputTF;
}


- (UIImageView *)arrowV {
    if (!_arrowV) {
        _arrowV  = [[UIImageView alloc] init];
        _arrowV.image = kImageWithName(@"invoice_arrow");
    }
    return _arrowV;
}


@end
