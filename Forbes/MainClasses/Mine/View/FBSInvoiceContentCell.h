//
//  FBSInvoiceContentCell.h
//  Forbes
//
//  Created by 周灿华 on 2019/8/18.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSCell.h"


@interface FBSInvoiceContentItem : FBSItem
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *content;
@property (nonatomic, copy) NSString *placeholder;
@property (nonatomic, assign) BOOL canEdit;
@property (nonatomic, assign) BOOL showArrow;
@property (nonatomic, assign) UIKeyboardType keyboardType;
@end

@interface FBSInvoiceContentCell : FBSCell

@end
