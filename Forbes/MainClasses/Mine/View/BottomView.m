//
//  BottomView.m
//  pod_test
//
//  Created by YY_ZYQ on 2017/6/23.
//  Copyright © 2017年 YY_ZYQ. All rights reserved.
//

#import "BottomView.h"

@implementation BottomView

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = Hexcolor(0xF3F7F8);
        [self addSubview:self.allBtn];
        [self addSubview:self.deleteBtn];
    }
    return self;
}

- (UIButton *)allBtn{
    if (!_allBtn) {
        self.allBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _allBtn.frame = CGRectMake(0, WScale(5), self.bounds.size.width/2 - 1, self.bounds.size.height);
        _allBtn.titleLabel.font = kLabelFontSize14;
        _allBtn.backgroundColor = Hexcolor(0xFFFFFF);
        [_allBtn setTitle:@"一件清空" forState:UIControlStateNormal];
        [_allBtn setTitleColor:Hexcolor(0x4A4A4A) forState:UIControlStateNormal];
    }
    return _allBtn;
}


- (UIButton *)deleteBtn{
    if (!_deleteBtn) {
        self.deleteBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _deleteBtn.frame = CGRectMake(self.bounds.size.width/2+2, WScale(5), self.bounds.size.width/2 -1, self.bounds.size.height);
        _deleteBtn.titleLabel.font = kLabelFontSize14;
        _deleteBtn.backgroundColor = Hexcolor(0xFFFFFF);
        [_deleteBtn setTitle:@"删除" forState:UIControlStateNormal];
        [_deleteBtn setTitleColor:Hexcolor(0x999999) forState:UIControlStateNormal];
    }
    return _deleteBtn;
}

@end
