
//
//  FBSContributeAddBottomView.m
//  Forbes
//
//  Created by 周灿华 on 2019/8/11.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSContributeAddBottomView.h"

@interface FBSContributeAddBottomView ()
@property (nonatomic, strong) UIButton *button;

@end

@implementation FBSContributeAddBottomView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        
        [self addSubview:self.button];
        [self.button mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(WScale(15));
            make.right.mas_equalTo(-WScale(15));
            make.height.mas_equalTo(WScale(40));
            make.bottom.mas_equalTo(-WScale(25));
        }];
    }
    return self;
}


#pragma mark - action

- (void)clickButton {
    if (self.addBlock) {
        self.addBlock();
    }
}


#pragma mark - property

- (UIButton *)button {
    if (!_button) {
        _button  = [UIButton buttonWithType:UIButtonTypeCustom];
        _button.backgroundColor = MainThemeColor;
        _button.titleLabel.font = kLabelFontSize15;
        [_button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_button setTitle:@"新建一篇文章" forState:UIControlStateNormal];
        [_button setImage:kImageWithName(@"contribute_add") forState:UIControlStateNormal];
        [_button setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 5)];
        [_button setTitleEdgeInsets:UIEdgeInsetsMake(0, 5, 0, 0)];
        [_button addTarget:self action:@selector(clickButton) forControlEvents:UIControlEventTouchUpInside];
    }
    return _button;
    
}


@end
