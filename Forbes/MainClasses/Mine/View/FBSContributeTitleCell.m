
//
//  FBSContributeTitleCell.m
//  Forbes
//
//  Created by 周灿华 on 2019/8/14.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSContributeTitleCell.h"

@implementation FBSContributeTitleItem

@end

@interface FBSContributeTitleCell ()
@property (nonatomic, strong) UILabel *titleL;
@property (nonatomic, strong) UIButton *arrowBtn;
@end

@implementation FBSContributeTitleCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self.contentView addSubview:self.titleL];
        [self.contentView addSubview:self.arrowBtn];
        
        [self.titleL mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(WScale(15));
            make.centerY.mas_equalTo(0);
        }];
        
        [self.arrowBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(0);
            make.right.mas_equalTo(-WScale(15));
        }];
        
    }
    
    return self;
}



- (void)setItem:(FBSContributeTitleItem *)item {
    if (![item isKindOfClass:[FBSContributeTitleItem class]]) {
        return ;
    }
    
    [super setItem:item];
    
    self.titleL.text = item.title;
    self.arrowBtn.hidden = !item.canExpand;
    self.arrowBtn.selected = item.isExpand;
}

#pragma mark - property


- (UILabel *)titleL {
    if (!_titleL) {
        _titleL  = [[UILabel alloc] init];
        _titleL.textColor = Hexcolor(0x333333);
        _titleL.font = kLabelFontSize16;
    }
    return _titleL;
}


- (UIButton *)arrowBtn {
    if (!_arrowBtn) {
        _arrowBtn  = [UIButton buttonWithType:UIButtonTypeCustom];
        [_arrowBtn setBackgroundImage:kImageWithName(@"contribute_arrowup") forState:UIControlStateNormal];
        [_arrowBtn setBackgroundImage:kImageWithName(@"contribute_arrowdown") forState:UIControlStateSelected];
        
    }
    return _arrowBtn;
}


@end
