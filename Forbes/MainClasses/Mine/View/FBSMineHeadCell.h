//
//  FBSMineHeadCell.h
//  Forbes
//
//  Created by 周灿华 on 2019/7/27.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSCell.h"

@interface FBSMineHeadItem : FBSItem
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *avatar;
@property (nonatomic, assign) BOOL isLogin;
@end

@interface FBSMineHeadCell : FBSCell

@end


