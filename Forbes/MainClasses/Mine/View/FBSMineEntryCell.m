//
//  FBSMineEntryCell.m
//  Forbes
//
//  Created by 周灿华 on 2019/7/27.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSMineEntryCell.h"

@implementation FBSMineEntryItem

@end



@interface FBSMineEntryCell ()
@property (nonatomic, strong) UILabel *titleL;
@property (nonatomic, strong) UIImageView *arrowV;
@end

@implementation FBSMineEntryCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self.contentView addSubview:self.titleL];
        [self.contentView addSubview:self.arrowV];
        
        [self.titleL mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(WScale(15));
            make.centerY.mas_equalTo(0);
        }];
        
        [self.arrowV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(0);
            make.right.mas_equalTo(-WScale(15));
            make.width.mas_equalTo(WScale(7));
            make.height.mas_equalTo(WScale(12));
        }];
    }
    
    return self;
}



- (void)setItem:(FBSMineEntryItem *)item {
    if (![item isKindOfClass:[FBSMineEntryItem class]]) {
        return ;
    }
    
    [super setItem:item];
    
    self.titleL.text = item.title;
}

#pragma mark - property

- (UIImageView *)arrowV {
    if (!_arrowV) {
        _arrowV  = [[UIImageView alloc] init];
        _arrowV.image = kImageWithName(@"mine_rightarrow");
    }
    return _arrowV;
}



- (UILabel *)titleL {
    if (!_titleL) {
        _titleL  = [[UILabel alloc] init];
        _titleL.textColor = Hexcolor(0x333333);
        _titleL.font = kLabelFontSize14;
    }
    return _titleL;
}


@end
