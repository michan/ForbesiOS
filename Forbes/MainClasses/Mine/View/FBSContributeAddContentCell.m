//
//  FBSContributeAddContentCell.m
//  Forbes
//
//  Created by 周灿华 on 2019/8/15.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSContributeAddContentCell.h"


@implementation FBSContributeAddContentItem

@end



@interface FBSContributeAddContentCell ()
@property (nonatomic, strong) UILabel *contentL;
@end




@implementation FBSContributeAddContentCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self.contentView addSubview:self.contentL];
        
        [self.contentL mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(WScale(15));
            make.centerY.mas_equalTo(0);
        }];
    }
    
    return self;
}



- (void)setItem:(FBSContributeAddContentItem *)item {
    if (![item isKindOfClass:[FBSContributeAddContentItem class]]) {
        return ;
    }
    
    [super setItem:item];
    
    self.contentL.text = item.content;
}

#pragma mark - property

- (UILabel *)contentL {
    if (!_contentL) {
        _contentL  = [[UILabel alloc] init];
        _contentL.textColor = Hexcolor(0x333333);
        _contentL.font = kLabelFontSize14;
    }
    return _contentL;
}

@end
