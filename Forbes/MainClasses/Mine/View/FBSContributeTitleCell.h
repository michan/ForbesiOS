//
//  FBSContributeTitleCell.h
//  Forbes
//
//  Created by 周灿华 on 2019/8/14.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSCell.h"

@interface FBSContributeTitleItem : FBSItem
@property (nonatomic, copy) NSString *title;
@property (nonatomic, assign) BOOL canExpand; //是否能展开
@property (nonatomic, assign) BOOL isExpand;  //时候展开
@end


@interface FBSContributeTitleCell : FBSCell

@end
