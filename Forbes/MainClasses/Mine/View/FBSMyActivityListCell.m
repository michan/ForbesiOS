//
//  FBSActivityPayDetailCell.m
//  Forbes
//
//  Created by 赵志辉 on 2019/9/26.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import "FBSMyActivityListCell.h"
#import "FBSActivityPayDetailItem.h"
#import "UIView+FBSLayer.h"
#import "NSMutableAttributedString+FBSAttributedString.h"
#import "FBSMyActivityListItem.h"

@interface FBSMyActivityListCell()

@property (nonatomic, strong) UIView *backView;
@property (nonatomic, strong) UIView *lineViewTop;
@property (nonatomic, strong) UIView *lineViewBottom;
@property (nonatomic, strong) YYLabel *titledetail;
@property (nonatomic, strong) UILabel *title;

@property (nonatomic, strong) UILabel *ticketType;


@property (nonatomic, strong) UILabel *examineVerify;



@property (nonatomic, strong) UIButton *numberButton;




@end
@implementation FBSMyActivityListCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleDefault;
        self.backgroundColor = Hexcolor(0xFFFFFF);
        [self.contentView addSubview:self.backView];
        [self.backView addSubview:self.titledetail];
        [self.backView addSubview:self.title];
        [self.backView addSubview:self.lineViewTop];
        [self.backView addSubview:self.lineViewBottom];
        
        [self.backView addSubview:self.ticketType];
     
        
        [self.backView addSubview:self.examineVerify];
        
        
        
        [self.contentView addSubview:self.numberButton];
        
        
        [self.backView setContentHuggingPriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisHorizontal];
        [self.backView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.contentView).offset(WScale(17));
            make.top.mas_equalTo(self.contentView).offset(WScale(21));
            make.width.mas_equalTo(kScreenWidth - WScale(34));
            make.height.mas_equalTo(WScale(150));
        }];
        
        [self.titledetail mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.backView).offset(WScale(8));
            make.top.mas_equalTo(self.backView).offset(WScale(10));
            make.right.mas_equalTo(self.backView).offset(WScale(-11));
            make.height.mas_equalTo(WScale(15));
        }];
        [self.lineViewTop mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.backView);
            make.top.mas_equalTo(self.titledetail.mas_bottom).offset(WScale(4));
            make.right.mas_equalTo(self.backView);
            make.height.mas_equalTo(WScale(2));
        }];
        
        self.title.numberOfLines = 0;
        self.title.preferredMaxLayoutWidth = (kScreenWidth - WScale(20));
        [self.title setContentHuggingPriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisHorizontal];
        
        
        [self.title mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.backView).offset(WScale(10));
            make.top.mas_equalTo(self.lineViewTop.mas_bottom).offset(WScale(4));
            make.right.mas_equalTo(self.backView).offset(WScale(-10));
            make.height.mas_equalTo(WScale(52));
        }];
        
        [self.lineViewBottom mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.backView);
            make.top.mas_equalTo(self.title.mas_bottom).offset(WScale(2));
            make.right.mas_equalTo(self.backView);
            make.height.mas_equalTo(WScale(2));
        }];
        
        [self.ticketType mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.backView).offset(WScale(10));
            make.top.mas_equalTo(self.lineViewBottom.mas_bottom).offset(WScale(9));
            make.right.mas_equalTo(self.backView).offset(WScale(-11));
            make.height.mas_equalTo(WScale(17));
        }];
        
        [self.examineVerify mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.backView).offset(WScale(10));
            make.top.mas_equalTo(self.ticketType.mas_bottom).offset(WScale(3));
            make.right.mas_equalTo(self.backView).offset(WScale(-11));
            make.bottom.mas_equalTo(self.backView.mas_bottom).offset(WScale(-15));
        }];
        [self.numberButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.mas_equalTo(WScale(92));
            make.top.mas_equalTo(self.lineViewBottom).offset(WScale(17));
            make.right.mas_equalTo(self.backView).offset(WScale(-11));
            make.height.mas_equalTo(WScale(22));
        }];
        
        
        UIView *backGroundView = [[UIView alloc]init];
        backGroundView.backgroundColor = [UIColor clearColor];
        self.selectedBackgroundView = backGroundView;

        
        
    }
    
    return self;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    if (!self.editing) {
        return;
    }
    [super setSelected:selected animated:animated];
    
    if (self.editing) {
        self.contentView.backgroundColor = [UIColor clearColor];
        self.backgroundColor = [UIColor clearColor];
        self.backView.backgroundColor = [UIColor whiteColor];
        self.titledetail.backgroundColor = [UIColor whiteColor];
        self.title.backgroundColor = [UIColor whiteColor];
        self.ticketType.backgroundColor = [UIColor whiteColor];
        self.examineVerify.backgroundColor = [UIColor whiteColor];
        self.lineViewTop.backgroundColor = [UIColor whiteColor];
        self.lineViewBottom.backgroundColor = [UIColor whiteColor];
    }
}

- (void)setItem:(FBSMyActivityListItem *)item{
    [super setItem:item];

    self.title.text = item.ticket_title;
    self.titledetail.text = item.activity_ticket_code;
    self.ticketType.attributedText =[self setTextAttributedString:[NSString stringWithFormat:@"票种%@",item.ticket_title]];
    self.examineVerify.attributedText = [self setTextAttributedString:[NSString stringWithFormat:@"审核%@",item.status]];
    [self layoutIfNeeded];
    [self drawLineOfDashByCAShapeLayer:self.lineViewTop lineLength:1 lineSpacing:1 lineColor:Hexcolor(0xD1D1D1) lineDirection:YES];
    [self drawLineOfDashByCAShapeLayer:self.lineViewBottom lineLength:1 lineSpacing:1 lineColor:Hexcolor(0xD1D1D1) lineDirection:YES];
}
- (void)clickTo{
    if ([self.delegate respondsToSelector:@selector(fbsCell:didClickButtonAtIndex:)]) {
        [self.delegate fbsCell:self didClickButtonAtIndex:0];
    }
}
- (UIButton *)numberButton
{
    if (!_numberButton) {
        _numberButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _numberButton.titleLabel.textColor = Hexcolor(0xFFFFFF);
        _numberButton.titleLabel.font = kLabelFontSize13;
        _numberButton.backgroundColor = Hexcolor(0x8C734B);
        _numberButton.layer.cornerRadius = WScale(11);
        [_numberButton setTitle:@"订单详情" forState:UIControlStateNormal];
        [_numberButton addTarget:self action:@selector(clickTo) forControlEvents:UIControlEventTouchUpInside];
    }
    return _numberButton;
}
- (NSAttributedString *)setTextAttributedString:(NSString *)actionStr{
    //设置整段字符串的颜色
    NSDictionary *attributes = @{NSFontAttributeName:kLabelFontSize12, NSForegroundColorAttributeName:Hexcolor(0xD1D1D1)};
    NSRange range = NSMakeRange(1, 1);
    NSMutableAttributedString *text = [NSMutableAttributedString setTextAttributedString:actionStr attributes:attributes space:WScale(17) range:range];
    return [text copy];
}




- (YYLabel *)titledetail {
    if (!_titledetail) {
        _titledetail  = [[YYLabel alloc] init];
        _titledetail.textColor = Hexcolor(0x999999);
        _titledetail.font = kLabelFontSize11;
//        _titledetail.textVerticalAlignment = YYTextVerticalAlignmentTop;
    }
    return _titledetail;
}
- (UILabel *)title {
    if (!_title) {
        _title = [[UILabel alloc] init];
        _title.textColor = Hexcolor(0x4A4A4A);
        _title.font = kLabelFontSize18;
    }
    return _title;
}
- (UIView *)lineViewTop{
    if (!_lineViewTop) {
        _lineViewTop = [[UIView alloc] init];
        _lineViewTop.backgroundColor = Hexcolor(0xFFFFFF);
    }
    return _lineViewTop;
}
- (UIView *)lineViewBottom{
    if (!_lineViewBottom) {
        _lineViewBottom = [[UIView alloc] init];
        _lineViewBottom.backgroundColor = Hexcolor(0xFFFFFF);
    }
    return _lineViewBottom;
}
- (UILabel *)ticketType{
    if (!_ticketType) {
        _ticketType = [[UILabel alloc] init];
        _ticketType.textColor = Hexcolor(0x999999);
        _ticketType.font = kLabelFontSize12;
    }
    return _ticketType;
}

- (UILabel *)examineVerify{
    if (!_examineVerify) {
        _examineVerify = [[UILabel alloc] init];
        _examineVerify.textColor = Hexcolor(0x999999);
        _examineVerify.font = kLabelFontSize12;
    }
    return _examineVerify;
}

- (UIView *)backView {
    if (!_backView) {
        _backView  = [[UILabel alloc] init];
        _backView.backgroundColor = Hexcolor(0xFFFFFF);
        _backView.layer.borderColor = Hexcolor(0xD8D8D8).CGColor;
        _backView.layer.borderWidth = WScale(1);
    }
    return _backView;
}

-(void)layoutSubviews
{
    UIImageView *img;
    for (UIControl *control in self.subviews){
        if ([control isMemberOfClass:NSClassFromString(@"UITableViewCellEditControl")]){
            for (UIView *v in control.subviews)
            {
                if ([v isKindOfClass: [UIImageView class]]) {
                    img=(UIImageView *)v;
                    if (self.selected) {
                        img.image=[UIImage imageNamed:@"sel"];
                    }else
                    {
                        img.image=[UIImage imageNamed:@"unsel"];
                    }
                    img.frame = CGRectMake(img.frame.origin.x, 20, img.frame.size.width, img.frame.size.height);
                    img.center = CGPointMake(img.center.x, img.frame.size.height+5);
                }
            }
        }
    }
    [super layoutSubviews];
    for (UIControl *control in self.subviews){
        if ([control isMemberOfClass:NSClassFromString(@"UITableViewCellEditControl")]){
            control.frame = CGRectMake(0, 0, WScale(38), WScale(56));
        }
    }
}

@end
