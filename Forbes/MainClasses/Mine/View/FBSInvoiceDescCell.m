//
//  FBSInvoiceDescCell.m
//  Forbes
//
//  Created by 周灿华 on 2019/8/18.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSInvoiceDescCell.h"
#import "FBSTextView.h"

@implementation FBSInvoiceDescItem

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.bottomLineHidden = YES;
    }
    return self;
}

@end


@interface FBSInvoiceDescCell ()<FBSTextViewDelegate>
{
    FBSInvoiceDescItem *_currentItem;
}
@property (nonatomic, strong) UILabel *titleL;
@property (nonatomic, strong) FBSTextView *textView;
@property (nonatomic, strong) NSDictionary *valueAttributes;
@property (nonatomic, strong) NSMutableDictionary *placeholderAttributes;

@end

@implementation FBSInvoiceDescCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self.contentView addSubview:self.titleL];
        [self.contentView addSubview:self.textView];
        
        [self.titleL mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.left.mas_equalTo(WScale(15));
            make.height.mas_equalTo(WScale(16));
        }];

        
        [self.textView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(WScale(15));
            make.top.mas_equalTo(WScale(45));
            make.right.mas_equalTo(5 - WScale(15));  //textView 的 inseet.right == 5
            make.height.mas_equalTo(WScale(70));
        }];
       
    }
    
    return self;
}



- (void)setItem:(FBSInvoiceDescItem *)item {
    if (![item isKindOfClass:[FBSInvoiceDescItem class]]) {
        return ;
    }
    
    [super setItem:item];
    _currentItem = item;
    
    
    NSAttributedString *placeholderAttStr = [[NSAttributedString alloc] initWithString:@"发票内容" attributes:self.placeholderAttributes];
    self.textView.attributedPlaceholder = placeholderAttStr;
    
    if (item.content.length) {
        NSAttributedString *contentAttStr = [[NSAttributedString alloc] initWithString:item.content attributes:self.valueAttributes];
        self.textView.attributedText = contentAttStr;
    } else {
        self.textView.text = nil;
    }
    
}


#pragma mark - FBSTextViewDelegate

-(BOOL)limitedTextViewShouldReturn:(UITextView *)textView{
    NSLog(@"点击了return");
    [textView resignFirstResponder];
    return NO;
}

- (void)limitedTextViewDidChange:(UITextView *)textView{
    NSLog(@"文字改变了 -- %@",textView.text);
    _currentItem.content = textView.text;
    
    UITextRange *selectedRange = [textView markedTextRange];
    UITextPosition *pos = [textView positionFromPosition:selectedRange.start offset:0];
    if (selectedRange && pos) {
        return;
    }
    textView.attributedText = [[NSAttributedString alloc] initWithString:textView.text attributes:self.valueAttributes];
}


-(void)limitedTextViewDidEndEditing:(UITextView *)textView{
    NSLog(@"结束编辑 - text:%@",textView.text);
}




#pragma mark - property


- (FBSTextView *)textView {
    if (!_textView) {
        _textView = [[FBSTextView alloc] init];
        _textView.realDelegate = self;
        _textView.returnKeyType = UIReturnKeyDone;
        _textView.font = kLabelFontSize15;
        _textView.textColor = RGB(50, 50, 50);
        _textView.textAlignment = NSTextAlignmentLeft;
        _textView.maxLength = 0;
        _textView.layer.borderColor = Hexcolor(0xdddddd).CGColor;
        _textView.layer.borderWidth = 1;
        
        [self.contentView addSubview:_textView];
    }
    return _textView;
}


- (UILabel *)titleL {
    if (!_titleL) {
        _titleL  = [[UILabel alloc] init];
        _titleL.textColor = RGB(51,51,51);
        _titleL.font = kLabelFontSize16;
        _titleL.text = @"收票内容";
        _titleL.adjustsFontSizeToFitWidth = YES;
    }
    return _titleL;
}




- (NSDictionary *)valueAttributes {
    if (!_valueAttributes) {
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        paragraphStyle.lineSpacing = 5;//字体的行间距
        _valueAttributes = @{
                             NSFontAttributeName:kLabelFontSize15,
                             NSForegroundColorAttributeName : RGB(50,50,50),
                             NSParagraphStyleAttributeName:paragraphStyle
                             };
        
    }
    return _valueAttributes;
}

- (NSDictionary *)placeholderAttributes {
    if (!_placeholderAttributes) {
        NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc] init];
        style.lineSpacing = 5;
        
        NSDictionary *dict = @{
                               NSFontAttributeName : kLabelFontSize15,
                               NSForegroundColorAttributeName : Hexcolor(0x999999),
                               NSParagraphStyleAttributeName : style
                               };
        _placeholderAttributes = [dict mutableCopy];
    }
    return _placeholderAttributes;
}

@end
