//
//  FBSMineEditViewController.m
//  Forbes
//
//  Created by 周灿华 on 2019/8/10.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSMineEditViewController.h"

@interface FBSMineEditViewController ()<UITextFieldDelegate>
@property (nonatomic, strong) FBSMineEditViewModel *viewModel;
@property (nonatomic, assign) MineEditType editType;

@property (nonatomic, strong) UIScrollView *bgScrollView;
@property (nonatomic, strong) UIView *contentView;
@property (nonatomic, strong) UIButton *commitButton;

//修改昵称
@property (nonatomic, strong) UITextField *nickNameTF;
//修改密码
@property (nonatomic, strong) UITextField *pwdTF;
//绑定手机号码
@property (nonatomic, strong) UITextField *phoneTF;
@property (nonatomic, strong) UITextField *codeTF;

@property (nonatomic, strong) UIButton *sendCodeBtn;

//分割线
@property (nonatomic, strong) UIView *topLine;
@property (nonatomic, strong) UIView *middleLine;
@property (nonatomic, strong) UIView *bottomLine;

@end

@implementation FBSMineEditViewController
@synthesize viewModel = _viewModel;

#pragma mark - life cycle

- (instancetype)initWithEditType:(MineEditType)editType {
    self = [super init];
    if (self) {
        self.editType = editType;
        self.viewModel = [[FBSMineEditViewModel alloc] initWithEditType:editType];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    [self UIInit];
}

- (void)UIInit {
    self.view.backgroundColor = [UIColor whiteColor];
    
    switch (self.editType) {
        case MineEditTypeNickName:
        {
            self.title = @"修改昵称";
            [self nickNameUIInit];
        }
            
            break;
            
        case MineEditTypePhone:
        {
            self.title = @"更换手机号码";
            [self changePhoneUIInit];
        }
            break;
            
        case MineEditTypePassword:
        {
            self.title = @"修改密码";
            [self changePasswordUIInit];
        }
            break;
            
        default:
            break;
    }

    
    
    [self.view addSubview:self.bgScrollView];
    [self.bgScrollView addSubview:self.contentView];
    [self.contentView addSubview:self.commitButton];
    
    
    [self.bgScrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(kNavTopMargin);
        make.left.right.mas_equalTo(self.view);
        make.height.mas_equalTo(self.view).offset(-kNavTopMargin);
    }];
    
    
    [self.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.mas_equalTo(0);
        make.width.mas_equalTo(self.bgScrollView);
        make.height.mas_equalTo(self.bgScrollView);
        make.bottom.mas_equalTo(0);
    }];
    
    
    [self.commitButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(WScale(15));
        make.right.mas_equalTo(-WScale(15));
        make.height.mas_equalTo(WScale(40));
        make.bottom.mas_equalTo(-WScale(30) - kDiffTabBarH);
    }];
}

- (void)nickNameUIInit  {
    
    UIView *line = [[UIView alloc] init];
    line.backgroundColor = Hexcolor(0xD1D1D1);
    
    UILabel *tipLabel = [[UILabel alloc] init];
    tipLabel.font = kLabelFontSize14;
    tipLabel.text = @"3-99个字符，仅支持汉字/数字/英文";
    tipLabel.textColor = Hexcolor(0x999999);
    
    [self.contentView addSubview:self.nickNameTF];
    [self.contentView addSubview:line];
    [self.contentView addSubview:tipLabel];
    
    [self.nickNameTF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(0);
        make.left.mas_equalTo(WScale(33));
        make.right.mas_equalTo(-WScale(33));
        make.height.mas_equalTo(WScale(50));
    }];
    
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.nickNameTF.mas_bottom);
        make.left.mas_equalTo(WScale(33));
        make.right.mas_equalTo(-WScale(33));
        make.height.mas_equalTo(1);
    }];

    [tipLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(line.mas_bottom);
        make.left.mas_equalTo(WScale(33));
        make.height.mas_equalTo(WScale(40));
    }];
}


- (void)changePhoneUIInit  {
    UILabel *tipLabel = [[UILabel alloc] init];
    tipLabel.font = kLabelFontSize15;
    tipLabel.text = @"更换已经绑定的手机号码";
    tipLabel.textColor = Hexcolor(0x333333);
    
    [self.contentView addSubview:tipLabel];
    [self.contentView addSubview:self.phoneTF];
    [self.contentView addSubview:self.codeTF];
    [self.contentView addSubview:self.topLine];
    [self.contentView addSubview:self.middleLine];
    [self.contentView addSubview:self.bottomLine];
    [self.contentView addSubview:self.sendCodeBtn];
    
    [tipLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(0);
        make.centerX.mas_equalTo(0);
        make.height.mas_equalTo(WScale(55));
    }];
    
    
    [self.topLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(tipLabel.mas_bottom);
        make.left.right.mas_equalTo(0);
        make.height.mas_equalTo(1);
    }];
    
    
    [self.phoneTF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.topLine.mas_bottom);
        make.left.mas_equalTo(WScale(30));
        make.width.mas_equalTo(WScale(200));
        make.height.mas_equalTo(WScale(44));
    }];
    
    
    [self.middleLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.phoneTF.mas_bottom);
        make.left.right.mas_equalTo(0);
        make.height.mas_equalTo(1);
    }];

    
    [self.codeTF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.middleLine.mas_bottom);
        make.left.mas_equalTo(WScale(30));
        make.width.mas_equalTo(WScale(200));
        make.height.mas_equalTo(WScale(44));
    }];
    
    [self.bottomLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.codeTF.mas_bottom);
        make.left.right.mas_equalTo(0);
        make.height.mas_equalTo(1);
    }];

    
    
    [self.sendCodeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(WScale(70));
        make.height.mas_equalTo(WScale(25));
        make.centerY.mas_equalTo(self.codeTF);
        make.right.mas_equalTo(-WScale(34));
    }];

}




- (void)changePasswordUIInit  {
    [self.contentView addSubview:self.pwdTF];
    [self.contentView addSubview:self.bottomLine];
    
    [self.pwdTF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(0);
        make.left.mas_equalTo(WScale(30));
        make.width.mas_equalTo(WScale(200));
        make.height.mas_equalTo(WScale(44));
    }];
    
    [self.bottomLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.pwdTF.mas_bottom);
        make.left.right.mas_equalTo(0);
        make.height.mas_equalTo(1);
    }];
}



#pragma mark - action

- (void)clickButton {
    [self.view endEditing:YES];
    
    WEAKSELF
    switch (self.editType) {
        case MineEditTypeNickName:
        {
            if (!self.nickNameTF.text.length) {
                [self makeToast:@"请输入昵称"];
                return ;
            }
            
            [self showHUD];
            [self.viewModel rqUpdateNickname:self.nickNameTF.text complete:^(BOOL success, NSString *msg) {
                [weakSelf update:success];
                if (success) {
                    NSDictionary *infoDic = @{
                                              @"content" : weakSelf.nickNameTF.text,
                                              @"type" : @"1"
                                              };
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:FBSEditUserInfoNotication object:nil userInfo:infoDic];
                } else {
                    [weakSelf makeToast:msg];
                }
            }];
        }
            break;
            
        case MineEditTypePhone:
        {
            if (self.phoneTF.text.length == 0) {
                [self makeToast:@"请输入手机号码"];
                return ;
            }
            
            if (![NSString validateContactNumber:self.phoneTF.text]) {
                [self makeToast:@"请输入正确的手机号码"];
                return ;
            }
            
            if (self.codeTF.text.length == 0) {
                [self makeToast:@"请输入验证码"];
                return ;
            }
            
            [self showHUD];
            [self.viewModel rqUpdateTel:self.phoneTF.text code:self.codeTF.text complete:^(BOOL success, NSString *msg) {
                
                [weakSelf update:success];
                if (success) {
                    NSDictionary *infoDic = @{
                                              @"content" : weakSelf.phoneTF.text,
                                              @"type" : @"2"
                                              };
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:FBSEditUserInfoNotication object:nil userInfo:infoDic];
                    
                } else {
                    [weakSelf makeToast:msg];
                }
            }];



        }
            break;

        case MineEditTypePassword:
        {
            if (self.pwdTF.text.length == 0) {
                [self makeToast:@"请输入新密码"];
                return ;
            }
            
            [self showHUD];
            [self.viewModel rqUpdatePassword:self.pwdTF.text complete:^(BOOL success, NSString *msg) {
                
                [weakSelf update:success];
                if (success) {
                    
                } else {
                    [weakSelf makeToast:msg];
                }
            }];

        }
            break;

            
        default:
            break;
    }
}



- (void)update:(BOOL)success {
    [self hideHUD];

    if (success) {
        [self makeToast:@"修改成功"];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self.navigationController popViewControllerAnimated:YES];
        });
        
        //更新用户数据
        [[FBSUserData sharedData] fetchNewestUserData];
    }

}


#pragma mark - 发送验证码

- (void)sentCodeMethod {
    [self.view endEditing:YES];
    
    if (self.phoneTF.text.length == 0) {
        [self makeToast:@"请输入手机号码"];
        return ;
    }
    
    if (![NSString validateContactNumber:self.phoneTF.text]) {
        [self makeToast:@"请输入正确的手机号码"];
        return ;
    }

    
    NSLog(@"发送验证码。。");
    
    WEAKSELF
    [self showHUD];
    [self.viewModel rqSendSMSCode:self.phoneTF.text complete:^(BOOL success) {
        [weakSelf hideHUD];
        if (success) {
            [weakSelf makeToast:@"验证码已发送"];
            [weakSelf sentPhoneCodeTimeMethod];
        } else {
            [weakSelf makeToast:@"获取验证码失败"];
        }
    }];
    
}


//计时器发送验证码
- (void)sentPhoneCodeTimeMethod{
    //倒计时时间 - 60秒
    __block NSInteger timeOut = 59;
    //执行队列
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    //计时器 -》dispatch_source_set_timer自动生成
    dispatch_source_t timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, queue);
    dispatch_source_set_timer(timer, DISPATCH_TIME_NOW, 1.0 * NSEC_PER_SEC, 0 * NSEC_PER_SEC);
    dispatch_source_set_event_handler(timer, ^{
        if (timeOut <= 0) {
            dispatch_source_cancel(timer);
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.sendCodeBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
                [self.sendCodeBtn setUserInteractionEnabled:YES];
            });
        }else{
            //开始计时
            //剩余秒数 seconds
            NSInteger seconds = timeOut % 60;
            NSString *strTime = [NSString stringWithFormat:@"%.1ld",seconds];
            //主线程设置按钮样式
            dispatch_async(dispatch_get_main_queue(), ^{
                [UIView beginAnimations:nil context:nil];
                [UIView setAnimationDuration:1.0];
                [self.sendCodeBtn setTitle:[NSString stringWithFormat:@"%@S",strTime] forState:UIControlStateNormal];
                [UIView commitAnimations];
                //计时器件不允许点击
                [self.sendCodeBtn setUserInteractionEnabled:NO];
            });
            timeOut--;
        }
    });
    dispatch_resume(timer);
}


#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}


#pragma mark - property

- (UIScrollView *)bgScrollView {
    if (!_bgScrollView) {
        _bgScrollView  = [[UIScrollView alloc] init];
        _bgScrollView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
        _bgScrollView.alwaysBounceVertical = YES;
        if (@available(iOS 11.0, *)) {
            _bgScrollView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        }
    }
    return _bgScrollView;
}


- (UIView *)contentView {
    if (!_contentView) {
        _contentView  = [[UIView alloc] init];
        _contentView.backgroundColor = [UIColor clearColor];
    }
    return _contentView;
}


- (UIButton *)commitButton {
    if (!_commitButton) {
        _commitButton  = [UIButton buttonWithType:UIButtonTypeCustom];
        _commitButton.backgroundColor = MainThemeColor;
        _commitButton.titleLabel.font = kLabelFontSize15;
        [_commitButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_commitButton setTitle:@"完成" forState:UIControlStateNormal];
        [_commitButton addTarget:self action:@selector(clickButton) forControlEvents:UIControlEventTouchUpInside];
    }
    return _commitButton;
    
}


- (UITextField *)nickNameTF {
    if (!_nickNameTF) {
        _nickNameTF  = [[UITextField alloc] init];
        _nickNameTF.font = kLabelFontSize14;
        _nickNameTF.textColor = Hexcolor(0x333333);
        _nickNameTF.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"输入新昵称" attributes:@{NSFontAttributeName:kLabelFontSize14,NSForegroundColorAttributeName:Hexcolor(0xCBCCCC)}];
        _nickNameTF.returnKeyType = UIReturnKeyDone;
        _nickNameTF.clearButtonMode = UITextFieldViewModeWhileEditing;
        _nickNameTF.delegate = self;
    }
    return _nickNameTF;
}


- (UITextField *)phoneTF {
    if (!_phoneTF) {
        _phoneTF  = [[UITextField alloc] init];
        _phoneTF.font = kLabelFontSize14;
        _phoneTF.textColor = Hexcolor(0x333333);
        _phoneTF.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"输入手机号码" attributes:@{NSFontAttributeName:kLabelFontSize14,NSForegroundColorAttributeName:Hexcolor(0xCBCCCC)}];
        
        _phoneTF.returnKeyType = UIReturnKeyDone;
        _phoneTF.keyboardType = UIKeyboardTypeNumberPad;
        _phoneTF.clearButtonMode = UITextFieldViewModeWhileEditing;
        _phoneTF.delegate = self;
    }
    return _phoneTF;
}


- (UITextField *)codeTF {
    if (!_codeTF) {
        _codeTF  = [[UITextField alloc] init];
        _codeTF.font = kLabelFontSize14;
        _codeTF.textColor = Hexcolor(0x333333);
        _codeTF.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"请输入验证码" attributes:@{NSFontAttributeName:kLabelFontSize14,NSForegroundColorAttributeName:Hexcolor(0xCBCCCC)}];
        _codeTF.returnKeyType = UIReturnKeyDone;
        _codeTF.keyboardType = UIKeyboardTypeNumberPad;
        _codeTF.clearButtonMode = UITextFieldViewModeWhileEditing;
        _codeTF.delegate = self;
    }
    return _codeTF;
}



- (UITextField *)pwdTF {
    if (!_pwdTF) {
        _pwdTF  = [[UITextField alloc] init];
        _pwdTF.font = kLabelFontSize14;
        _pwdTF.textColor = Hexcolor(0x333333);
        _pwdTF.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"请输入新密码" attributes:@{NSFontAttributeName:kLabelFontSize14,NSForegroundColorAttributeName:Hexcolor(0xCBCCCC)}];
        _pwdTF.returnKeyType = UIReturnKeyDone;
        _pwdTF.clearButtonMode = UITextFieldViewModeWhileEditing;
        _pwdTF.delegate = self;
    }
    return _pwdTF;
}



- (UIButton *)sendCodeBtn {
    if (!_sendCodeBtn) {
        _sendCodeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_sendCodeBtn setBackgroundColor:[UIColor whiteColor]];
        _sendCodeBtn.titleLabel.font = kLabelFontSize12;
        _sendCodeBtn.titleLabel.adjustsFontSizeToFitWidth = YES;
        [_sendCodeBtn setTitleColor:Hexcolor(0xBB833E) forState:UIControlStateNormal];
        [_sendCodeBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
        
        [_sendCodeBtn.layer setBorderWidth:1.0];
        [_sendCodeBtn.layer setBorderColor:Hexcolor(0xBB833E).CGColor];
        
        [_sendCodeBtn addTarget:self action:@selector(sentCodeMethod) forControlEvents:UIControlEventTouchUpInside];
        
    }
    return _sendCodeBtn;
}



- (UIView *)topLine {
    if (!_topLine) {
        _topLine  = [[UIView alloc] init];
        _topLine.backgroundColor = RGB(232, 232, 232);
    }
    return _topLine;
}


- (UIView *)middleLine {
    if (!_middleLine) {
        _middleLine  = [[UIView alloc] init];
        _middleLine.backgroundColor = RGB(232, 232, 232);
    }
    return _middleLine;
}


- (UIView *)bottomLine {
    if (!_bottomLine) {
        _bottomLine  = [[UIView alloc] init];
        _bottomLine.backgroundColor = RGB(232, 232, 232);
    }
    return _bottomLine;
}


@end
