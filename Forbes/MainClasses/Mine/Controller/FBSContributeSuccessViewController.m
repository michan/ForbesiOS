

//
//  FBSContributeSuccessViewController.m
//  Forbes
//
//  Created by 周灿华 on 2019/8/11.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSContributeSuccessViewController.h"
#import "FBSContributeSuccessViewModel.h"


@interface FBSContributeSuccessViewController ()

@property (nonatomic ,copy)void (^block)(void);
@property (nonatomic, strong) FBSContributeSuccessViewModel *viewModel;
@end

@implementation FBSContributeSuccessViewController
@synthesize viewModel = _viewModel;

- (instancetype)initWithTitle:(NSString *)title image:(NSString *)imageURL success:(NSString *)successsStr content:(NSString *)content button:(NSString *)buttonStr block:(void (^)(void))block{
    if (self = [super init]) {
        self.viewModel = [[FBSContributeSuccessViewModel alloc] initWithimage:imageURL success:successsStr content:content button:buttonStr];
        self.title = title;
        self.block = block;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.registerDictionary = @{
                                [FBSSuccessItem reuseIdentifier] : [FBSSuccessCell class],
                                [FBSButtonItem reuseIdentifier] : [FBSButtonCell class]
                                };
}


#pragma mark - cell代理

- (void)fbsCell:(FBSCell *)cell didClickButtonAtIndex:(NSInteger)index {
    if ([cell isKindOfClass:[FBSButtonCell class]]) {
        if (self.block) {
            self.block();
        }
    }
}

#pragma mark - property

- (FBSContributeSuccessViewModel *)viewModel {
    if (!_viewModel) {
        _viewModel  = [[FBSContributeSuccessViewModel alloc] init];
    }
    return _viewModel;
}

@end
