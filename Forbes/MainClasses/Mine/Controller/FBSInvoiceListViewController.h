//
//  FBSInvoiceListViewController.h
//  Forbes
//
//  Created by 周灿华 on 2019/8/17.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSTableViewController.h"
#import "FBSInvoiceListViewViewModel.h"
#import "FBSEditView.h"

@interface FBSInvoiceListViewController : FBSTableViewController
@property (nonatomic, strong) FBSInvoiceListViewViewModel *viewModel;
@property (nonatomic, assign) BOOL isEdit;
@property (nonatomic, weak) FBSEditView *editView;


- (instancetype)initWithType:(NSInteger)type;

@end

