//
//  FBSContributeSuccessViewController.h
//  Forbes
//
//  Created by 周灿华 on 2019/8/11.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FBSContributeSuccessViewController : FBSTableViewController

- (instancetype)initWithTitle:(NSString *)title image:(NSString *)imageURL success:(NSString *)successsStr content:(NSString *)content button:(NSString *)buttonStr block:(void (^)(void))block;

@end

NS_ASSUME_NONNULL_END
