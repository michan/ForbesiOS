//
//  FBSMyActivityDetailViewController.m
//  Forbes
//
//  Created by 赵志辉 on 2019/9/29.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import "FBSMyActivityDetailViewController.h"
#import "FBSButtonCell.h"
#import "FBSPersonDetailCell.h"
#import "FBSPersonDetailItem.h"
#import "FBSActivityPayDetailCell.h"
#import "FBSActivityPayDetailItem.h"
#import "FBSActivityPayTypeCell.h"
#import "FBSActivityPayTypeItem.h"
#import "FBSMyActivityDetailViewModel.h"

@interface FBSMyActivityDetailViewController ()

@property(nonatomic, strong)FBSMyActivityDetailViewModel *viewModel;

@end

@implementation FBSMyActivityDetailViewController
@synthesize viewModel = _viewModel;

- (instancetype)initWithactivity_ticket_code:(NSString *)activity_ticket_code{
    if (self = [super init]) {
        self.viewModel.activity_ticket_code = activity_ticket_code;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"订单详情";
    self.registerDictionary = @{
                                
                                [FBSButtonItem reuseIdentifier] : [FBSButtonCell class],
                                [FBSPersonDetailItem reuseIdentifier] : [FBSPersonDetailCell class],
                                [FBSActivityPayDetailItem reuseIdentifier] : [FBSActivityPayDetailCell class],
                                [FBSActivityPayTypeItem reuseIdentifier] : [FBSActivityPayTypeCell class]
                                };
    
    
    WEAKSELF
    [self.viewModel requestActivities:^(BOOL success) {
        [weakSelf.tableView reloadData];
    }];
    // 自动布局
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 44;
}
#pragma mark - Table View Delegate
- (void)fbsCell:(FBSCell *)cell didClickButtonAtIndex:(NSInteger)index{
    if ([cell isKindOfClass:[FBSButtonCell class]]){
       
        FBSLog(@"%@",@"ok");
    }
}
- (FBSMyActivityDetailViewModel *)viewModel {
    if (!_viewModel) {
        _viewModel  = [[FBSMyActivityDetailViewModel alloc] init];
    }
    return _viewModel;
}
@end
