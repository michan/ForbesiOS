//
//  FBSContributeAddViewController.h
//  Forbes
//
//  Created by 周灿华 on 2019/8/11.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSTableViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface FBSContributeAddViewController : FBSTableViewController

@property(nonatomic, copy)void (^blockRefresh)(BOOL status);

@end

NS_ASSUME_NONNULL_END
