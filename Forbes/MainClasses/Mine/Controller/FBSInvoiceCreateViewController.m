//
//  FBSInvoiceCreateViewController.m
//  Forbes
//
//  Created by 周灿华 on 2019/8/18.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSInvoiceCreateViewController.h"
#import "FBSInvoiceInputViewController.h"

#import <WMPageController.h>
#import "FBSInvoiceCreateViewModel.h"

@interface FBSInvoiceCreateViewController ()<WMPageControllerDelegate,WMPageControllerDataSource>
@property (nonatomic, strong) WMPageController *pageController;
@property (nonatomic, strong) NSArray *tabTitles;
@property (nonatomic, strong) UIView *pageContainerView;
@property (nonatomic, strong) FBSInvoiceCreateViewModel *viewModel;
@end

@implementation FBSInvoiceCreateViewController
@synthesize viewModel = _viewModel;

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"发票申请";
    
    [self.navigationBar.contentView setBackgroundColor:[UIColor whiteColor]];
    
    [self.pageContainerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.navigationBar.mas_bottom);
        make.bottom.left.right.mas_equalTo(0);
    }];
    
    
    [self.pageController.view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.mas_equalTo(0);
        make.width.mas_equalTo(kScreenWidth);
        make.height.mas_equalTo(self.pageContainerView);
    }];
    
}


#pragma mark - WMPageControllerDataSource

- (NSInteger)numbersOfChildControllersInPageController:(WMPageController *)pageController {
    return self.tabTitles.count;
}

- (NSString *)pageController:(WMPageController *)pageController titleAtIndex:(NSInteger)index {
    return [self.tabTitles objectAtIndex:index];
}

- (UIViewController *)pageController:(WMPageController *)pageController viewControllerAtIndex:(NSInteger)index {
    
    FBSInvoiceInputViewController *listvc = [[FBSInvoiceInputViewController alloc] initWithType:[@(index) stringValue]];
    return listvc;
}

- (CGRect)pageController:(WMPageController *)pageController preferredFrameForMenuView:(WMMenuView *)menuView {
    CGFloat leftMargin = 0;
    CGFloat rightMargin = 0;
    CGFloat originY = 0;
    return CGRectMake(leftMargin, originY, pageController.view.frame.size.width - leftMargin - rightMargin, 44);
}

- (CGRect)pageController:(WMPageController *)pageController preferredFrameForContentView:(WMScrollView *)contentView {
    CGFloat originY = CGRectGetMaxY([self pageController:pageController preferredFrameForMenuView:self.pageController.menuView]);
    return CGRectMake(0, originY + 1, pageController.view.frame.size.width, pageController.view.frame.size.height - originY - 1);
}


#pragma mark - property

- (FBSInvoiceCreateViewModel *)viewModel {
    if (!_viewModel) {
        _viewModel  = [[FBSInvoiceCreateViewModel alloc] init];
    }
    return _viewModel;
}



- (UIView *)pageContainerView {
    if (!_pageContainerView) {
        _pageContainerView = [[UIView alloc] init];
        _pageContainerView.backgroundColor = [UIColor yellowColor];
        [self.view addSubview:_pageContainerView];
    }
    return _pageContainerView;
}

- (WMPageController *)pageController {
    if (!_pageController) {
        _pageController = [[WMPageController alloc] init];
        _pageController.dataSource = self;
        _pageController.delegate = self;
        _pageController.selectIndex = 0;
        _pageController.menuViewStyle = WMMenuViewStyleLine;
        _pageController.progressViewIsNaughty = YES;
        _pageController.progressWidth = 60;
        _pageController.automaticallyCalculatesItemWidths = YES;
        _pageController.titleColorNormal = Hexcolor(0x4A4A4A);
        _pageController.titleColorSelected = MainThemeColor;
        _pageController.titleSizeNormal = WScale(15);
        _pageController.titleSizeSelected = WScale(17);
        _pageController.view.backgroundColor = RGB(242,242,242);
        _pageController.menuView.backgroundColor = [UIColor whiteColor];
        _pageController.itemMargin = 10;
        [self addChildViewController:_pageController];
        [self.pageContainerView addSubview:_pageController.view];
    }
    return _pageController;
}



- (NSArray *)tabTitles {
    if (!_tabTitles) {
        _tabTitles = @[
                       @"增值税普通发票",
                       @"增值税专用发票"
                       ];
    }
    return _tabTitles;
}


@end
