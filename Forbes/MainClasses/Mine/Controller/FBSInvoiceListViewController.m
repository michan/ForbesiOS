
//
//  FBSInvoiceListViewController.m
//  Forbes
//
//  Created by 周灿华 on 2019/8/17.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSInvoiceListViewController.h"
#import "FBSInvoiceCreateViewController.h"
#import "FBSInvoiceDetailViewController.h"


@interface FBSInvoiceListViewController ()

@end

@implementation FBSInvoiceListViewController
@synthesize viewModel = _viewModel;

- (instancetype)initWithType:(NSInteger)type {
    self = [super init];
    if (self) {
        self.viewModel = [[FBSInvoiceListViewViewModel alloc] initWithType:type];
    }
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.isEdit = NO;
    self.navigationBarHidden = YES;

    self.registerDictionary = @{
                                [FBSInvoiceListItem reuseIdentifier] : [FBSInvoiceListCell class]
                                };
    
    [self.tableView reloadData];
}

#pragma mark - LgCLBaseCellDelegate

- (void)fbsCell:(FBSCell *)cell didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([cell isKindOfClass:[FBSInvoiceListCell class]]) {
        if (self.isEdit) {
            FBSInvoiceListItem *currentItem = (FBSInvoiceListItem *)cell.item;
            if ([self.viewModel.selectedItems containsObject:currentItem]) {
                currentItem.isSelected = NO;
                [self.viewModel.selectedItems removeObject:currentItem];
                
            } else {
                currentItem.isSelected = YES;
                [self.viewModel.selectedItems addObject:currentItem];
            }
            
            
            BOOL isSelectAll = self.viewModel.selectedItems.count == self.viewModel.items.count;
            [self.editView setAllButonStatus:isSelectAll];
            
            [self.tableView reloadData];
        
        } else {
            if (self.viewModel.type == 0) { //发票申请页面
                FBSInvoiceCreateViewController *createVC = [[FBSInvoiceCreateViewController alloc] init];
                [self.navigationController pushViewController:createVC animated:YES];
                
                
            } else { //发票详情
                
                FBSInvoiceDetailViewController *detailVC = [[FBSInvoiceDetailViewController alloc] init];
                [self.navigationController pushViewController:detailVC animated:YES];

            }
            

        }
        
    }
    
}


@end
