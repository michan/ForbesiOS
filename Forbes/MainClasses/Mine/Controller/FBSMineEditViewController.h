//
//  FBSMineEditViewController.h
//  Forbes
//
//  Created by 周灿华 on 2019/8/10.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSViewController.h"
#import "FBSMineEditViewModel.h"

@interface FBSMineEditViewController : FBSViewController

- (instancetype)initWithEditType:(MineEditType)editType;

@end

