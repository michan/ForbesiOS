//
//  FBSInvoiceInputViewController.h
//  Forbes
//
//  Created by 周灿华 on 2019/8/18.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSTableViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface FBSInvoiceInputViewController : FBSTableViewController

@property (nonatomic, strong) NSString *type;

- (instancetype)initWithType:(NSString *)type;

@end

NS_ASSUME_NONNULL_END
