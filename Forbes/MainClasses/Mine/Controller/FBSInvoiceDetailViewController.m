//
//  FBSInvoiceDetailViewController.m
//  Forbes
//
//  Created by 周灿华 on 2019/8/18.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSInvoiceDetailViewController.h"

@interface FBSInvoiceDetailViewController ()
@property (nonatomic, strong) UIImageView *imgView;
@end

@implementation FBSInvoiceDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"发票详情";
    
    [self.view addSubview:self.imgView];
    [self.imgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.navigationBar.mas_bottom).offset(WScale(15));
        make.left.mas_equalTo(WScale(15));
        make.right.mas_equalTo(-WScale(15));
        make.bottom.mas_equalTo(-WScale(78));
    }];
}

- (UIImageView *)imgView {
    if (!_imgView) {
        _imgView  = [[UIImageView alloc] init];
        _imgView.backgroundColor = [UIColor lightGrayColor];
    }
    return _imgView;
}


@end
