//
//  FBSContributeViewController.m
//  Forbes
//
//  Created by 周灿华 on 2019/8/10.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSContributeViewController.h"
#import "FBSContributeListViewController.h"

#import <WMPageController.h>
#import "FBSContributeViewModel.h"

@interface FBSContributeViewController ()<WMPageControllerDelegate,WMPageControllerDataSource>
@property (nonatomic, strong) WMPageController *pageController;
@property (nonatomic, strong) NSArray *tabTitles;
@property (nonatomic, strong) UIView *pageContainerView;
@property (nonatomic, strong) FBSContributeViewModel *viewModel;
@end

@implementation FBSContributeViewController
@synthesize viewModel = _viewModel;

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"投稿管理";
    
    [self.navigationBar.contentView setBackgroundColor:[UIColor whiteColor]];
    
    [self.pageContainerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.navigationBar.mas_bottom);
        make.bottom.left.right.mas_equalTo(0);
    }];
    
    
    [self.pageController.view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.mas_equalTo(0);
        make.width.mas_equalTo(kScreenWidth);
        make.height.mas_equalTo(self.pageContainerView);
    }];
    
}


#pragma mark - WMPageControllerDataSource

- (NSInteger)numbersOfChildControllersInPageController:(WMPageController *)pageController {
    return self.tabTitles.count;
}

- (NSString *)pageController:(WMPageController *)pageController titleAtIndex:(NSInteger)index {
    return [self.tabTitles objectAtIndex:index];
}

- (UIViewController *)pageController:(WMPageController *)pageController viewControllerAtIndex:(NSInteger)index {
    
    FBSContributeListViewController *listvc = [[FBSContributeListViewController alloc] init];
    if (index == 0) {
        listvc.type = @"1";
    }else if (index == 1){
        listvc.type = @"3";
    }else if (index == 2){
        listvc.type = @"2";
    }
    
    return listvc;
}

- (CGRect)pageController:(WMPageController *)pageController preferredFrameForMenuView:(WMMenuView *)menuView {
    CGFloat leftMargin = 0;
    CGFloat rightMargin = 0;
    CGFloat originY = 0;
    return CGRectMake(leftMargin, originY, pageController.view.frame.size.width - leftMargin - rightMargin, 44);
}

- (CGRect)pageController:(WMPageController *)pageController preferredFrameForContentView:(WMScrollView *)contentView {
    CGFloat originY = CGRectGetMaxY([self pageController:pageController preferredFrameForMenuView:self.pageController.menuView]);
    return CGRectMake(0, originY + 1, pageController.view.frame.size.width, pageController.view.frame.size.height - originY - 1);
}


#pragma mark - property

- (FBSContributeViewModel *)viewModel {
    if (!_viewModel) {
        _viewModel  = [[FBSContributeViewModel alloc] init];
    }
    return _viewModel;
}



- (UIView *)pageContainerView {
    if (!_pageContainerView) {
        _pageContainerView = [[UIView alloc] init];
        _pageContainerView.backgroundColor = [UIColor yellowColor];
        [self.view addSubview:_pageContainerView];
    }
    return _pageContainerView;
}

- (WMPageController *)pageController {
    if (!_pageController) {
        _pageController = [[WMPageController alloc] init];
        _pageController.dataSource = self;
        _pageController.delegate = self;
        _pageController.selectIndex = 0;
        _pageController.menuViewStyle = WMMenuViewStyleLine;
        _pageController.progressViewIsNaughty = YES;
        _pageController.progressWidth = 30;
        _pageController.automaticallyCalculatesItemWidths = YES;
        _pageController.titleColorNormal = Hexcolor(0x4A4A4A);
        _pageController.titleColorSelected = MainThemeColor;
        _pageController.titleSizeNormal = WScale(15);
        _pageController.titleSizeSelected = WScale(17);
        _pageController.view.backgroundColor = RGB(242,242,242);
        _pageController.menuView.backgroundColor = [UIColor whiteColor];
        _pageController.itemMargin = WScale(10);
        [self addChildViewController:_pageController];
        [self.pageContainerView addSubview:_pageController.view];
    }
    return _pageController;
}



- (NSArray *)tabTitles {
    if (!_tabTitles) {
        _tabTitles = @[
                       @"已发布",
                       @"待审核",
                       @"草稿箱",
                       ];
    }
    return _tabTitles;
}


@end
