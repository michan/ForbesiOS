//
//  FBSInvoiceViewController.m
//  Forbes
//
//  Created by 周灿华 on 2019/8/17.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSInvoiceViewController.h"
#import "FBSInvoiceViewModel.h"
#import "FBSInvoiceListViewController.h"

#import "FBSEditView.h"
#import <WMPageController.h>

@interface FBSInvoiceViewController ()<WMPageControllerDelegate,WMPageControllerDataSource>
@property (nonatomic, strong) WMPageController *pageController;
@property (nonatomic, strong) NSArray *tabTitles;
@property (nonatomic, strong) UIView *pageContainerView;
@property (nonatomic, strong) FBSInvoiceViewModel *viewModel;
@property (nonatomic, assign) BOOL isEdit;
@property (nonatomic, strong) FBSEditView *editView;
@property (nonatomic, strong) FBSInvoiceListViewController *applyVC;
@property (nonatomic, strong) FBSInvoiceListViewController *checkVC;
@property (nonatomic, weak)   FBSInvoiceListViewController *currentVC;

@end

@implementation FBSInvoiceViewController
@synthesize viewModel = _viewModel;

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"发票申请";
    self.isEdit = NO;
    
    [self.navigationBar.contentView setBackgroundColor:[UIColor whiteColor]];
    
    [self.pageContainerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.navigationBar.mas_bottom);
        make.bottom.left.right.mas_equalTo(0);
    }];
    
    
    [self.pageController.view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.mas_equalTo(0);
        make.width.mas_equalTo(kScreenWidth);
        make.height.mas_equalTo(self.pageContainerView);
    }];
    
    
    
    FBSNavigationItem *rightItem = [[FBSNavigationItem alloc] init];
    rightItem.title = @"编辑";
    rightItem.titleAttributes = @{
                                  NSFontAttributeName : [UIFont systemFontOfSize:17],
                                  NSForegroundColorAttributeName : Hexcolor(0x4A4A4A)
                                  };
    WEAKSELF
    rightItem.action = ^(FBSNavigationItem *item) {
        [weakSelf editAction];
    };
    
    
    [self.navigationBar setRightBarButtonItems:@[rightItem]];
    
    [self.editView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.view);
        make.height.bottom.mas_equalTo(EditViewH);
    }];
    
}

#pragma mark - private method

- (void)editAction {
    FBSLog(@"点击了编辑");
    self.isEdit = !self.isEdit;
    self.interactivePopDisabled = self.isEdit;
    self.navigationBar.backButton.enabled = !self.isEdit;
    self.pageController.menuView.userInteractionEnabled = !self.isEdit;
    self.pageController.scrollEnable = !self.isEdit;

    self.applyVC.isEdit = self.isEdit;
    self.checkVC.isEdit = self.isEdit;
    
    if (!self.isEdit) {
        [UIView animateWithDuration:0.25 animations:^{
            [self.editView mas_updateConstraints:^(MASConstraintMaker *make) {
                make.bottom.mas_equalTo(EditViewH);
            }];
            [self.editView.superview layoutIfNeeded];
            
        }];
        
        self.currentVC.tableView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
    } else {
        
        [UIView animateWithDuration:0.25 animations:^{
            [self.editView mas_updateConstraints:^(MASConstraintMaker *make) {
                make.bottom.mas_equalTo(0);
            }];
            [self.editView.superview layoutIfNeeded];
            
        }];
        
        self.currentVC.tableView.contentInset = UIEdgeInsetsMake(0, 0, EditViewH, 0);
    }
    
    //取消选中的项目
    [self selectAllAction:NO isEdit:self.isEdit];
}


- (void)selectAllAction:(BOOL)isSelectAll isEdit:(BOOL)isEdit{
    FBSLog(@"点击了%@",isSelectAll ? @"全选" : @"取消全选") ;
    [self.currentVC.viewModel.selectedItems removeAllObjects];
    
    for (FBSInvoiceListItem *item  in self.currentVC.viewModel.items) {
        if ([item isKindOfClass:[FBSInvoiceListItem class]]) {
            item.isEdit = isEdit;
            item.isSelected = isSelectAll;
        }
    }
    
    if (isSelectAll) {
        self.currentVC.viewModel.selectedItems = [self.currentVC.viewModel.items mutableCopy];
    }
    
    [self.editView setAllButonStatus:isSelectAll];
    
    [self.currentVC.tableView reloadData];
}




- (void)deleteAction {
    
    if (self.currentVC.viewModel.selectedItems.count) {
        FBSLog(@"点击了删除");
        [self.currentVC.viewModel.items removeObjectsInArray:self.currentVC.viewModel.selectedItems];
        [self.currentVC.viewModel.selectedItems removeAllObjects];
        [self.currentVC.tableView reloadData];
    } else {
        FBSLog(@"请选择要删除的项目");
    }
}


#pragma mark - WMPageControllerDataSource


- (NSInteger)numbersOfChildControllersInPageController:(WMPageController *)pageController {
    return self.tabTitles.count;
}

- (NSString *)pageController:(WMPageController *)pageController titleAtIndex:(NSInteger)index {
    return [self.tabTitles objectAtIndex:index];
}

- (UIViewController *)pageController:(WMPageController *)pageController viewControllerAtIndex:(NSInteger)index {
    if (index == 0) {
        return self.applyVC;
    } else if(index == 1) {
        return self.checkVC;
    }
    return nil;
}

- (CGRect)pageController:(WMPageController *)pageController preferredFrameForMenuView:(WMMenuView *)menuView {
    CGFloat leftMargin = 0;
    CGFloat rightMargin = 0;
    CGFloat originY = 0;
    return CGRectMake(leftMargin, originY, pageController.view.frame.size.width - leftMargin - rightMargin, 44);
}

- (CGRect)pageController:(WMPageController *)pageController preferredFrameForContentView:(WMScrollView *)contentView {
    CGFloat originY = CGRectGetMaxY([self pageController:pageController preferredFrameForMenuView:self.pageController.menuView]);
    return CGRectMake(0, originY + 1, pageController.view.frame.size.width, pageController.view.frame.size.height - originY - 1);
}


#pragma mark - property

- (FBSInvoiceViewModel *)viewModel {
    if (!_viewModel) {
        _viewModel  = [[FBSInvoiceViewModel alloc] init];
    }
    return _viewModel;
}


- (UIView *)pageContainerView {
    if (!_pageContainerView) {
        _pageContainerView = [[UIView alloc] init];
        _pageContainerView.backgroundColor = [UIColor yellowColor];
        [self.view addSubview:_pageContainerView];
    }
    return _pageContainerView;
}

- (WMPageController *)pageController {
    if (!_pageController) {
        _pageController = [[WMPageController alloc] init];
        _pageController.dataSource = self;
        _pageController.delegate = self;
        _pageController.selectIndex = 0;
        _pageController.menuViewStyle = WMMenuViewStyleLine;
        _pageController.progressViewIsNaughty = YES;
        _pageController.progressWidth = 30;
        _pageController.automaticallyCalculatesItemWidths = YES;
        _pageController.titleColorNormal = Hexcolor(0x4A4A4A);
        _pageController.titleColorSelected = MainThemeColor;
        _pageController.titleSizeNormal = WScale(15);
        _pageController.titleSizeSelected = WScale(17);
        _pageController.view.backgroundColor = RGB(242,242,242);
        _pageController.menuView.backgroundColor = [UIColor whiteColor];
        _pageController.itemMargin = WScale(10);
        [self addChildViewController:_pageController];
        [self.pageContainerView addSubview:_pageController.view];
    }
    return _pageController;
}



- (NSArray *)tabTitles {
    if (!_tabTitles) {
        _tabTitles = @[
                       @"所有",
                       @"申请成功"
                       ];
    }
    return _tabTitles;
}



- (FBSEditView *)editView {
    if (!_editView) {
        _editView = [[FBSEditView alloc] init];
        _editView.backgroundColor = [UIColor whiteColor];
        
        WEAKSELF
        _editView.selectBlock = ^(BOOL isSelectAll) {
            [weakSelf selectAllAction:isSelectAll isEdit:YES];
        };
        
        
        _editView.deleteBlock = ^{
            [weakSelf deleteAction];
        };
        _editView.layer.shadowColor = [UIColor lightGrayColor].CGColor;
        _editView.layer.shadowOpacity = 0.9;
        _editView.layer.shadowRadius = 5.0;
        _editView.layer.shadowOffset = CGSizeMake(0,0);
        [self.view addSubview:_editView];
    }
    return _editView;
}


- (FBSInvoiceListViewController *)applyVC {
    if (!_applyVC) {
         _applyVC = [[FBSInvoiceListViewController alloc] initWithType:0];
    }
    return _applyVC;
}



- (FBSInvoiceListViewController *)checkVC {
    if (!_checkVC) {
         _checkVC = [[FBSInvoiceListViewController alloc] initWithType:1];
    }
    return _checkVC;
}



- (FBSInvoiceListViewController *)currentVC {
    return self.pageController.selectIndex == 0 ? self.applyVC : self.checkVC;
}


@end
