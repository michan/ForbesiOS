//
//  FBSContributeViewController.h
//  Forbes
//
//  Created by 周灿华 on 2019/8/10.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSViewController.h"

NS_ASSUME_NONNULL_BEGIN

///投稿列表
@interface FBSContributeViewController : FBSViewController

@end

NS_ASSUME_NONNULL_END
