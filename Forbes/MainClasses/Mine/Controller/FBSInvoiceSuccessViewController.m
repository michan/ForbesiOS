//
//  FBSInvoiceSuccessViewController.m
//  Forbes
//
//  Created by 周灿华 on 2019/8/18.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSInvoiceSuccessViewController.h"
#import "FBSInvoiceSuccessViewModel.h"


@interface FBSInvoiceSuccessViewController ()
@property (nonatomic, strong) FBSInvoiceSuccessViewModel *viewModel;
@end

@implementation FBSInvoiceSuccessViewController
@synthesize viewModel = _viewModel;

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"发票申请";
    
    self.registerDictionary = @{
                                [FBSSuccessItem reuseIdentifier] : [FBSSuccessCell class],
                                [FBSButtonItem reuseIdentifier] : [FBSButtonCell class]
                                };
}


#pragma mark - cell代理

- (void)fbsCell:(FBSCell *)cell didClickButtonAtIndex:(NSInteger)index {
    if ([cell isKindOfClass:[FBSButtonCell class]]) {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

#pragma mark - property

- (FBSInvoiceSuccessViewModel *)viewModel {
    if (!_viewModel) {
        _viewModel  = [[FBSInvoiceSuccessViewModel alloc] init];
    }
    return _viewModel;
}

@end
