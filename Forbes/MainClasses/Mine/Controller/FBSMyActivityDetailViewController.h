//
//  FBSMyActivityDetailViewController.h
//  Forbes
//
//  Created by 赵志辉 on 2019/9/29.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import "FBSTableViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface FBSMyActivityDetailViewController : FBSTableViewController

- (instancetype)initWithactivity_ticket_code:(NSString *)activity_ticket_code;
@end

NS_ASSUME_NONNULL_END
