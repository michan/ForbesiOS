//
//  FBSContributeListViewController.m
//  Forbes
//
//  Created by 周灿华 on 2019/8/10.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSContributeListViewController.h"
#import "FBSContributeListViewModel.h"
#import "FBSContributeSuccessViewController.h"
#import "FBSContributeAddViewController.h"
#import "FBSContributeAddBottomView.h"

@interface FBSContributeListViewController ()
@property (nonatomic, strong) FBSContributeListViewModel *viewModel;
@end

@implementation FBSContributeListViewController
@synthesize viewModel = _viewModel;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationBarHidden = YES;
    
    self.registerDictionary = @{
                                [FBSContributeListItem reuseIdentifier] :  [FBSContributeListCell class]
                                };
    [self UIInit];
}

- (void)UIInit {
    FBSContributeAddBottomView *bottomView = [[FBSContributeAddBottomView alloc] init];
    bottomView.backgroundColor = [UIColor whiteColor];
    WEAKSELF
    bottomView.addBlock = ^{
        FBSContributeAddViewController *addVC = [[FBSContributeAddViewController alloc] init];
        addVC.blockRefresh = ^(BOOL status){
            if (status) {
                [self.tableView.mj_header beginRefreshing];
            }
        };
        [weakSelf.navigationController pushViewController:addVC animated:YES];
    };
    [self.view addSubview:bottomView];
    
    [bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.mas_equalTo(0);
        make.height.mas_equalTo(BottomViewH);
    }];
    
    self.tableView.contentInset = UIEdgeInsetsMake(0, 0, BottomViewH, 0);
    
    [self createload];
}
- (void)createload{
    
    self.page = @(1);
    self.pageCount = @(10);
    WEAKSELF
    [self normalHeaderRefreshingActionBlock:^(FooterConfigBlock  _Nonnull footerConfig) {
        [weakSelf.viewModel createArticleListL:self.type isMore:NO complete:^(BOOL success, NSUInteger count) {
            if (success) {
                [weakSelf.tableView reloadData];
                footerConfig(count);
            }else{
                
            }
        }];
    }];
    [self backNormalFooterRefreshingActionBlock:^(FooterConfigBlock  _Nonnull footerConfig) {
        [weakSelf.viewModel createArticleListL:self.type isMore:YES complete:^(BOOL success, NSUInteger count) {
            if (success) {
                [weakSelf.tableView reloadData];
                footerConfig(count);
            }else{
                
            }
        }];
    }];
}
#pragma mark - cell 代理

- (void)fbsCell:(FBSCell *)cell didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([cell isKindOfClass:[FBSContributeListCell class]]) {
        FBSContributeSuccessViewController *successVC = [[FBSContributeSuccessViewController alloc] initWithTitle:@"发布成功" image:@"tab_activityunsel" success:@"发布成功！" content:@"感谢您对福布斯的支持，我们将会尽快审核您的文章。" button:@"完成" block:^{
            
        }];
        [self.navigationController pushViewController:successVC animated:YES];
    }
}

#pragma mark - property

- (FBSContributeListViewModel *)viewModel {
    if (!_viewModel) {
        _viewModel  = [[FBSContributeListViewModel alloc] init];
    }
    return _viewModel;
}

@end
