//
//  FBSMineViewController.h
//  Forbes
//
//  Created by 周灿华 on 2019/7/24.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSTableViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface FBSMineViewController : FBSTableViewController

@end

NS_ASSUME_NONNULL_END
