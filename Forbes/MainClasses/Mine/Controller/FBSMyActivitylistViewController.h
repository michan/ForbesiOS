//
//  FBSMyActivitylistViewController.h
//  Forbes
//
//  Created by 赵志辉 on 2019/9/29.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import "FBSTableViewController.h"
#import "FBSMyActivitylistViewModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface FBSMyActivitylistViewController : FBSTableViewController
- (instancetype)initWith:(NSString *)isok;

- (void)clickRight:(BOOL)status;
@end

NS_ASSUME_NONNULL_END
