//
//  FBSContributeListViewController.h
//  Forbes
//
//  Created by 周灿华 on 2019/8/10.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSTableViewController.h"

NS_ASSUME_NONNULL_BEGIN

///投稿列表
@interface FBSContributeListViewController : FBSTableViewController

@property (nonatomic, copy) NSString *type;

@end

NS_ASSUME_NONNULL_END
