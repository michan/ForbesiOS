//
//  FBSMyActivitylistViewController.m
//  Forbes
//
//  Created by 赵志辉 on 2019/9/29.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import "FBSMyActivitylistViewController.h"
#import "FBSMyActivitylistViewModel.h"
#import "FBSButtonCell.h"
#import "FBSPersonDetailCell.h"
#import "FBSPersonDetailItem.h"
#import "FBSActivityPayDetailCell.h"
#import "FBSActivityPayDetailItem.h"
#import "FBSActivityPayTypeCell.h"
#import "FBSActivityPayTypeItem.h"
#import "FBSMyActivityListItem.h"
#import "FBSMyActivityListCell.h"
#import "FBSMyActivityDetailViewController.h"
#import "BottomView.h"



@interface FBSMyActivitylistViewController ()

@property(nonatomic ,strong)FBSMyActivitylistViewModel *viewModel;

@property (nonatomic, copy) void(^action)(FBSNavigationItem *item);


@property (nonatomic ,strong)NSMutableArray *deleteArray;

@property (nonatomic ,strong) BottomView *bottom_view;//底部视图




@end

@implementation FBSMyActivitylistViewController
@synthesize viewModel = _viewModel;

- (instancetype)initWith:(NSString *)isok{
    if (self == [super init]) {
        self.viewModel.is_ok = isok;
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationBarHidden = YES;
    self.registerDictionary = @{
                                
                                [FBSButtonItem reuseIdentifier] : [FBSButtonCell class],
                                [FBSPersonDetailItem reuseIdentifier] : [FBSPersonDetailCell class],
                                [FBSActivityPayDetailItem reuseIdentifier] : [FBSActivityPayDetailCell class],
                                [FBSActivityPayTypeItem reuseIdentifier] : [FBSActivityPayTypeCell class],
                                [FBSMyActivityListItem reuseIdentifier] : [FBSMyActivityListCell class]
                                };
    
    
   
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 44;
    
    [self createload];
}
- (void)clickRight:(BOOL)status{
    if (status) {
        [self.tableView setEditing:YES animated:YES];
        //如果在全选状态下，点击完成，再次进来的时候需要改变按钮的文字和点击状态
        if (self.bottom_view.allBtn.selected) {
            self.bottom_view.allBtn.selected = !self.bottom_view.allBtn.selected;
            [self.bottom_view.allBtn setTitle:@"全选" forState:UIControlStateNormal];
        }
        //添加底部视图
        CGRect frame = self.bottom_view.frame;
        frame.origin.y -= WScale(50);
        [UIView animateWithDuration:0.5 animations:^{
            self.bottom_view.frame = frame;
            [self.view addSubview:self.bottom_view];
        }];
    }else{
        [self.tableView setEditing:NO animated:YES];
        [UIView animateWithDuration:0.5 animations:^{
            CGPoint point = self.bottom_view.center;
            point.y   += WScale(50);
            self.bottom_view.center   = point;
        } completion:^(BOOL finished) {
            [self.bottom_view removeFromSuperview];
        }];
    }
}

/**
  删除数据方法
  */
- (void)deleteData{
    if (self.deleteArray.count >0) {
        [self showHUD];
        WEAKSELF
        [self.viewModel delegatedelegatArray:self.deleteArray complete:^(BOOL success, int count) {
            [weakSelf hideHUD];
            [weakSelf makeToast:[NSString stringWithFormat:@"成功删除%d条活动",count] duration:2 position:ToastPositionCenter];
            [self.tableView reloadData];
            
        }];
    }
    
}
- (void)tapAllBtn:(UIButton *)btn{
    
    btn.selected = !btn.selected;
    
    if (btn.selected) {
        
        for (int i = 0; i< self.viewModel.items.count; i++) {
            NSIndexPath *indexPath = [NSIndexPath indexPathForItem:i inSection:0];
            //全选实现方法
            [self.tableView selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionTop];
        }
        
        //点击全选的时候需要清除deleteArray里面的数据，防止deleteArray里面的数据和列表数据不一致
        if (self.deleteArray.count >0) {
            [self.deleteArray removeAllObjects];
        }
        [self.deleteArray addObjectsFromArray:self.viewModel.items];
        
        [btn setTitle:@"取消" forState:UIControlStateNormal];
    }else{
        
        //取消选中
        for (int i = 0; i< self.viewModel.items.count; i++) {
            NSIndexPath *indexPath = [NSIndexPath indexPathForItem:i inSection:0];
            [self.tableView deselectRowAtIndexPath:indexPath animated:NO];
        }
        [btn setTitle:@"全选" forState:UIControlStateNormal];
        [self.deleteArray removeAllObjects];
    }
}

-(UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView.editing) {
        return UITableViewCellEditingStyleDelete|UITableViewCellEditingStyleInsert;
    }
    return UITableViewCellEditingStyleNone;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView.isEditing) {
        NSLog(@"选中");
        FBSItem *item = [self.viewModel itemAtRow:indexPath.row inSection:indexPath.section];
        [self.deleteArray addObject:item];
        
    }else{
        NSLog(@"跳转下一页");
    }
}
- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath  {

    if (tableView.isEditing) {
        NSLog(@"撤销");
        FBSItem *item = [self.viewModel itemAtRow:indexPath.row inSection:indexPath.section];
        [self.deleteArray removeObject:item];
    }else{
        NSLog(@"取消跳转");
    }
}
- (void)createload{
    
    self.page = @(1);
    self.pageCount = @(10);
    WEAKSELF
    [self normalHeaderRefreshingActionBlock:^(FooterConfigBlock  _Nonnull footerConfig) {
        [weakSelf.viewModel requestActivitiesIsMore:NO complete:^(BOOL success,NSInteger count) {
            if (success) {
                [weakSelf.tableView reloadData];
                footerConfig(count);
            }else{
                
            }
        }];
    }];
    [self backNormalFooterRefreshingActionBlock:^(FooterConfigBlock  _Nonnull footerConfig) {
        [weakSelf.viewModel requestActivitiesIsMore:YES complete:^(BOOL success,NSInteger count) {
            if (success) {
                [weakSelf.tableView reloadData];
                footerConfig(count);
            }else{
                
            }
        }];
    }];
}

#pragma mark - Table View Delegate
- (void)fbsCell:(FBSCell *)cell didClickButtonAtIndex:(NSInteger)index{
    if ([cell isKindOfClass:[FBSMyActivityListCell class]]){
        FBSMyActivitylistDataItemsItem *item = [self.viewModel.model.data.items firstObject];
        FBSMyActivityDetailViewController *vc = [[FBSMyActivityDetailViewController alloc] initWithactivity_ticket_code:item.activity_ticket_code];
        [self.navigationController pushViewController:vc animated:YES];
        FBSLog(@"%@",@"ok");
    }
}
- (FBSMyActivitylistViewModel *)viewModel {
    if (!_viewModel) {
        _viewModel  = [[FBSMyActivitylistViewModel alloc] init];
    }
    return _viewModel;
}
- (BottomView *)bottom_view{
    if (!_bottom_view) {
        self.bottom_view = [[BottomView alloc] initWithFrame:CGRectMake(0, self.view.bounds.size.height, kScreenWidth, WScale(50))];
        [_bottom_view.deleteBtn addTarget:self action:@selector(deleteData) forControlEvents:UIControlEventTouchUpInside];
        [_bottom_view.allBtn addTarget:self action:@selector(tapAllBtn:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _bottom_view;
}
- (NSMutableArray *)deleteArray{
    if (!_deleteArray) {
        self.deleteArray = [NSMutableArray array];
    }
    return _deleteArray;
}
@end
