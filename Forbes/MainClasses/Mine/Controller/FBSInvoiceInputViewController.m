//
//  FBSInvoiceInputViewController.m
//  Forbes
//
//  Created by 周灿华 on 2019/8/18.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSInvoiceInputViewController.h"
#import "FBSInvoiceInputViewModel.h"

#import "FBSInvoiceSuccessViewController.h"

@interface FBSInvoiceInputViewController ()
@property (nonatomic, strong) FBSInvoiceInputViewModel *viewModel;
@end

@implementation FBSInvoiceInputViewController
@synthesize viewModel = _viewModel;


- (instancetype)initWithType:(NSString *)type
{
    self = [super init];
    if (self) {
        self.type = type;
        self.viewModel = [[FBSInvoiceInputViewModel alloc] initWithType:type];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationBarHidden = YES;
    
    self.registerDictionary = @{
                                [FBSInvoiceContentItem reuseIdentifier] : [FBSInvoiceContentCell class],
                                [FBSInvoiceTipItem reuseIdentifier] :  [FBSInvoiceTipCell class],
                                [FBSButtonItem reuseIdentifier] : [FBSButtonCell class],
                                [FBSSpaceItem reuseIdentifier] : [FBSSpaceCell class],
                                [FBSInvoiceDescItem reuseIdentifier] : [FBSInvoiceDescCell class]
                                };
    [self UIInit];
}

- (void)UIInit {
    
}

#pragma mark - cell 代理

- (void)fbsCell:(FBSCell *)cell didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([cell isKindOfClass:[FBSButtonCell class]]) {
        FBSInvoiceSuccessViewController *successVC = [[FBSInvoiceSuccessViewController alloc] init];
        [self.navigationController pushViewController:successVC animated:YES];
    }
}


#pragma mark - property


@end
