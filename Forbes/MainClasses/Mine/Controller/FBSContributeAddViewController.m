//
//  FBSContributeAddViewController.m
//  Forbes
//
//  Created by 周灿华 on 2019/8/11.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSContributeAddViewController.h"
#import "FBSContributeAddViewModel.h"
#import "TZImagePickerController.h"
#import "ImageModel.h"
#import "FBSOnlyOneInputCell.h"
#import "FBSOnlyOneInputItem.h"

@interface FBSContributeAddViewController ()<TZImagePickerControllerDelegate,ACEExpandableTableViewDelegate>
@property (nonatomic, strong) FBSContributeAddViewModel *viewModel;

@property (nonatomic, strong)FBSContributeInputContentCell *cell;
@end

@implementation FBSContributeAddViewController
@synthesize viewModel = _viewModel;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.registerDictionary = @{
                                [FBSContributeTitleItem reuseIdentifier] : [FBSContributeTitleCell class],
                                [FBSButtonItem reuseIdentifier] : [FBSButtonCell class],
                                [FBSContributeInputContentItem reuseIdentifier] : [FBSContributeInputContentCell class],
                                [FBSContributeAddContentItem reuseIdentifier] : [FBSContributeAddContentCell class],
                                [FBSTagsItem reuseIdentifier] : [FBSTagsCell class],
                                [FBSOnlyOneInputItem reuseIdentifier] : [FBSOnlyOneInputCell class]
                                
                                };
    
    FBSNavigationItem *rightItem = [[FBSNavigationItem alloc] init];
    rightItem.title = @"存草稿";
    rightItem.titleAttributes = @{
                                  NSFontAttributeName : [UIFont systemFontOfSize:17],
                                  NSForegroundColorAttributeName : Hexcolor(0x4A4A4A)
                                  };
    WEAKSELF
    rightItem.action = ^(FBSNavigationItem *item) {
        NSMutableArray *postArray = [[self.cell p_trimIsMove:NO] mutableCopy];
       
        //存储到NSUserDefaults（转NSData存）
        NSData *data = [NSKeyedArchiver archivedDataWithRootObject:postArray];
        NSUserDefaults *de = [NSUserDefaults standardUserDefaults];
        [de setObject:data forKey:@"AttributedString"];
        
        FBSOnlyOneInputItem *InputItem = [self.viewModel.items firstObject];
        if (InputItem.text.length) {
            [de setObject:InputItem.text forKey:@"title"];
        }
        [de synchronize];
        [weakSelf makeToast:@"保存成功" duration:2 position:1];
    };
    
    
    [self.navigationBar setRightBarButtonItems:@[rightItem]];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSAssert(self.viewModel != nil && [self.viewModel isKindOfClass:[FBSTableViewModel class]], @"FBSTableViewController  viewModel属性不能为空，且必须为FBSTableViewModel子类！");
    
    FBSItem *item  = [self.viewModel itemAtRow:indexPath.row inSection:indexPath.section];
    FBSCell *cell;
    if ([[item class] reuseIdentifier].length) {
        cell = [self.tableView dequeueReusableCellWithIdentifier:[[item class] reuseIdentifier] forIndexPath:indexPath];
        cell.item = item;
        cell.delegate = self;
        if ([cell isKindOfClass:[FBSContributeInputContentCell class]]) {
            self.cell = (FBSContributeInputContentCell *)cell;
        }
        if ([cell isKindOfClass:[FBSOnlyOneInputCell class]]) {
            FBSOnlyOneInputCell *cellText = (FBSOnlyOneInputCell *)cell;
            cellText.expandableTableView = tableView;
        }
    }
    return cell;
}

#pragma mark - cell代理

- (void)tableView:(UITableView *)tableView updatedHeight:(CGFloat)height atIndexPath:(NSIndexPath *)indexPath
{
    FBSItem *item  = [self.viewModel itemAtRow:indexPath.row inSection:indexPath.section];
    item.cellHeight = height;
}

- (void)tableView:(UITableView *)tableView updatedText:(NSString *)text atIndexPath:(NSIndexPath *)indexPath
{
    FBSItem *item  = [self.viewModel itemAtRow:indexPath.row inSection:indexPath.section];
    if ([item isKindOfClass:[FBSOnlyOneInputItem class]]) {
        FBSOnlyOneInputItem *itemTEXT = (FBSOnlyOneInputItem *)item;
        itemTEXT.text = text;
    }
}
- (void)fbsCell:(FBSCell *)cell didClickButtonAtIndex:(NSInteger)index {
    if ([cell isKindOfClass:[FBSButtonCell class]]) {
        
        if (self.cell) {
            // 获取内容为字符串和图片的数组
            FBSOnlyOneInputItem *item = [self.viewModel.items firstObject];
            if (!item.text.length) {
                 [self makeToast:@"请填写标题" duration:2 position:1];
            }
            if (!self.viewModel.inputItem.AttributedString.length) {
                [self makeToast:@"请填文章内容" duration:2 position:1];
            }
            NSMutableArray *postArray = [[self.cell p_trimIsMove:NO] mutableCopy];
            [self showHUD];
            [self.viewModel uploadfile:postArray Detail:^(BOOL success, NSString *bodyHTML) {
                [self hideHUD];
                if (success) {
                    if (self.blockRefresh) {
                        self.blockRefresh(YES);
                    }
                    [self.navigationController popViewControllerAnimated:YES];
                }else{
                    [self makeToast:@"保存失败" duration:2 position:1];
                }
            }];
        }
    }else if ([cell isKindOfClass:[FBSContributeInputContentCell class]]){
        self.cell = (FBSContributeInputContentCell *)cell;
        [self imageBtnClick];
    }else if ([cell isKindOfClass:[FBSTagsCell class]]){
        [self.viewModel contentExpand:NO];
        [self.tableView reloadData];
    }
}



- (void)fbsCell:(FBSCell *)cell didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([cell isKindOfClass:[FBSContributeTitleCell class]]) {
        FBSContributeTitleItem *item = (FBSContributeTitleItem *)cell.item;
        if (item.canExpand) {
            item.isExpand = !item.isExpand;
            [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            
            if (item.tag == ContentTag) {
                if (self.viewModel.tagsItem.selectedTags.count > 0) {
                    if (item.isExpand) {
                        [self showHUD];
                        [self.viewModel createChannel_idDetail:^(BOOL success) {
                            [self hideHUD];
                            if (success) {
                                [self.viewModel contentExpand:item.isExpand];
                                [self.tableView reloadData];
                            }else{
                                [self makeToast:@"请求失败请重新再试" duration:2 position:1];
                            }
                        }];
                    }else{
                        [self.viewModel contentExpand:item.isExpand];
                        [self.tableView reloadData];
                    }
                }else{
                    [self makeToast:@"请选择文章类别" duration:2 position:1];
                }
            } else if (item.tag == CategoryTag) {
                if (item.isExpand) {
                    [self showHUD];
                    [self.viewModel createArticleDetail:^(BOOL success) {
                        [self hideHUD];
                        if (success) {
                            [self.viewModel categoryExpand:item.isExpand];
                            [self.tableView reloadData];
                        }else{
                            [self makeToast:@"请求失败请重新再试" duration:2 position:1];
                        }
                    }];
                }else{
                    [self.viewModel categoryExpand:item.isExpand];
                    [self.tableView reloadData];
                }
            }
        }
    }else if ([cell isKindOfClass:[FBSContributeAddContentCell class]]){
        FBSContributeAddContentCell *itemContent = (FBSContributeAddContentCell *)cell.item;
        itemContent.selected = !itemContent.selected;
    }
}
#pragma mark 相册
-(void)imageBtnClick
{
    TZImagePickerController *controller = [[TZImagePickerController alloc] initWithMaxImagesCount:5 delegate:self];
    [self presentViewController:controller animated:YES completion:nil];
}
#pragma mark - TZImage Picker Controller Delegate
- (void)imagePickerController:(TZImagePickerController *)picker didFinishPickingPhotos:(NSArray<UIImage *> *)photos sourceAssets:(NSArray *)assets isSelectOriginalPhoto:(BOOL)isSelectOriginalPhoto {
    [self.cell create:photos];
}

#pragma mark - property

- (FBSContributeAddViewModel *)viewModel {
    if (!_viewModel) {
        _viewModel  = [[FBSContributeAddViewModel alloc] init];
    }
    return _viewModel;
}

@end
