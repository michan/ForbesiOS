//
//  FBSMineViewController.m
//  Forbes
//
//  Created by 周灿华 on 2019/7/24.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSMineViewController.h"
#import "FBSMineViewModel.h"

#import "FBSLoginViewController.h"
#import "FBSContributeViewController.h"
#import "FBSMineInfoViewController.h"
#import "FBSInvoiceViewController.h"
#import "FBSMyActivityManagerViewController.h"
#import "FBSRecordViewController.h"
#import "DemoSettingController.h"
#import "FBSFollowViewController.h"

@interface FBSMineViewController ()
@property (nonatomic, strong) FBSMineViewModel *viewModel;
@end

@implementation FBSMineViewController
@synthesize viewModel = _viewModel;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationBarHidden = YES;
    //添加一个顶部主题色layer
    CALayer *layer = [CALayer layer];
    layer.backgroundColor = MainThemeColor.CGColor;
    layer.frame = CGRectMake(0, -500, kScreenWidth, 500);
    [self.tableView.layer addSublayer:layer];
    
    self.registerDictionary = @{
                                [FBSMineHeadItem reuseIdentifier] : [FBSMineHeadCell class],
                                [FBSMineOperateItem reuseIdentifier] : [FBSMineOperateCell class],
                                [FBSMineEntryItem reuseIdentifier] : [FBSMineEntryCell class],
                                [FBSSpaceItem reuseIdentifier] : [FBSSpaceCell class],
                                [FBSButtonItem reuseIdentifier] : [FBSButtonCell class]
                                };
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(editUserInfo:) name:FBSEditUserInfoNotication object:nil];
}
- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self.tableView reloadData];
}

- (void)editUserInfo:(NSNotification *)notication {
    if (self.isLogin) {
        NSDictionary *dict = notication.userInfo;
        NSString *content = [dict valueForKey:@"content"];
        NSString *type = [dict valueForKey:@"type"];
        
        if ([type isEqualToString:@"1"]) {
            self.viewModel.headItem.name = content;
            [self.tableView reloadData];
        }
    }
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}


#pragma mark - FBSCellDelegate

- (void)fbsCell:(FBSCell *)cell didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([cell isKindOfClass:[FBSMineHeadCell class]]) {
        
        if (self.isLogin) {
            FBSMineInfoViewController *vc = [[FBSMineInfoViewController alloc] init];
            [self.navigationController pushViewController:vc animated:YES];
        } else {
            [self pushToLoginController];
        }
     
    } else if([cell isKindOfClass:[FBSMineEntryCell class]]) {
        
        // 判断是够已经登录
        if (cell.item.tag == FollowEntryTag ||
            cell.item.tag == ActivityEntryTag ||
            cell.item.tag == ContributeEntryTag ||
            cell.item.tag == InvoiceEntryTag) {
            
            if (!self.isLogin) {
                [self pushToLoginController];
                return ;
            }
        }
        
        
        FBSViewController *vc  = nil;
        
        switch (cell.item.tag) {
            case FollowEntryTag:
            {
                FBSFollowViewController *vc= [[FBSFollowViewController alloc] init];
                [self.navigationController pushViewController:vc animated:YES];

                
            }
                break;
            case ActivityEntryTag:
            {
                FBSMyActivityManagerViewController *vc= [[FBSMyActivityManagerViewController alloc] init];
                [self.navigationController pushViewController:vc animated:YES];
            }
                break;
            case ContributeEntryTag:
            {
                [self makeToast:@"暂未开放!"];
                //vc = [[FBSContributeViewController alloc] init];
            }
                break;
            case InvoiceEntryTag:
            {
//                vc = [[FBSInvoiceViewController alloc] init];
                [self makeToast:@"开发中，敬请期待!"];
            }
                break;
            case FeedbackEntryTag:
            {
                [self makeToast:@"开发中，敬请期待!"];
            }
                break;
            case SettingEntryTag:
            {
                vc = [[DemoSettingController alloc] init];
                
            }
                break;
            default:
                break;
        }
        
        
        if (vc) {
            [self.navigationController pushViewController:vc animated:YES];
        }
    }
}

- (void)fbsCell:(FBSCell *)cell didClickButtonAtIndex:(NSInteger)index {
    if ([cell isKindOfClass:[FBSButtonCell class]]) {
        if (self.isLogin) {
            [self showHUD];
            [[FBSUserData sharedData] deleteUserData];
        }
        
    }
    if ([cell isKindOfClass:[FBSMineOperateCell class]]) {
        FBSRecordViewController *vc = [[FBSRecordViewController alloc] initwithSelect:index];
        [self.navigationController pushViewController:vc animated:YES];
    }
}


#pragma mark - 登录状态改变

- (void)loginSuccess {
    [self.viewModel makeItems];
    [self.tableView reloadData];
}

- (void)logoutSuccess {
    [self hideHUD];
    [self.viewModel makeItems];
    [self.tableView reloadData];
}

#pragma mark - property

- (FBSMineViewModel *)viewModel {
    if (!_viewModel) {
        _viewModel  = [[FBSMineViewModel alloc] init];
    }
    return _viewModel;
}


@end


