//
//  FBSMineInfoViewController.m
//  Forbes
//
//  Created by 周灿华 on 2019/8/8.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSMineInfoViewController.h"
#import "FBSMineInfoViewModel.h"
#import "FBSMineEditViewController.h"
#import "TZImagePickerController.h"
@interface FBSMineInfoViewController ()<TZImagePickerControllerDelegate>
@property (nonatomic, strong) FBSMineInfoViewModel *viewModel;
@end

@implementation FBSMineInfoViewController
@synthesize viewModel = _viewModel;

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"个人资料";
    
    [self.navigationBar.contentView setBackgroundColor:[UIColor whiteColor]];
    self.registerDictionary = @{
                                [FBSMineInfoItem reuseIdentifier] : [FBSMineInfoCell class],
                                };
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(editUserInfo:) name:FBSEditUserInfoNotication object:nil];
}

- (void)editUserInfo:(NSNotification *)notication {
    if (self.isLogin) {
        NSDictionary *dict = notication.userInfo;
        NSString *content = [dict valueForKey:@"content"];
        NSString *type = [dict valueForKey:@"type"];
        
        if ([type isEqualToString:@"1"]) {
            self.viewModel.nicknameItem.detail = content;
            
        } else if ([type isEqualToString:@"2"]) {
            self.viewModel.phoneItem.detail = [NSString filterPhone:content];
            
        }
        
        [self.tableView reloadData];
    }
}


#pragma mark - FBSCellDelegate

- (void)fbsCell:(FBSCell *)cell didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([cell isKindOfClass:[FBSMineInfoCell class]]) {
        FBSMineEditViewController *editeVC = nil;
        if (cell.item.tag == AvatarTag) {
            [self imageBtnClick];
        } else if (cell.item.tag == NickNameTag) {
          editeVC = [[FBSMineEditViewController alloc] initWithEditType:MineEditTypeNickName];
        } else if (cell.item.tag == PhoneTag) {
          editeVC = [[FBSMineEditViewController alloc] initWithEditType:MineEditTypePhone];
        } else if (cell.item.tag == PasswordTag) {
          editeVC = [[FBSMineEditViewController alloc] initWithEditType:MineEditTypePassword];
        }
        
        if (editeVC) {
            [self.navigationController pushViewController:editeVC animated:YES];
        }
    }
}
#pragma mark 相册
-(void)imageBtnClick
{
    TZImagePickerController *controller = [[TZImagePickerController alloc] initWithMaxImagesCount:1 delegate:self];
    [self presentViewController:controller animated:YES completion:nil];
}
#pragma mark - TZImage Picker Controller Delegate
- (void)imagePickerController:(TZImagePickerController *)picker didFinishPickingPhotos:(NSArray<UIImage *> *)photos sourceAssets:(NSArray *)assets isSelectOriginalPhoto:(BOOL)isSelectOriginalPhoto {
    if (photos.count == 1) {
        [self showHUD];
        WEAKSELF;
        [self.viewModel uploadHeader:photos Detail:^(BOOL success, NSString *headerURl) {
            [weakSelf hideHUD];
            if (success) {
                [weakSelf makeToast:@"修改成功"];
                [weakSelf.tableView reloadData];
            }else{
                [weakSelf makeToast:@"修改失败"];
            }
        }];
    }
}
- (void)fbsCell:(FBSCell *)cell didClickButtonAtIndex:(NSInteger)index {
    if ([cell isKindOfClass:[FBSButtonCell class]]) {
        FBSLog(@"点击了退出登录");
    }
}



#pragma mark - property

- (FBSMineInfoViewModel *)viewModel {
    if (!_viewModel) {
        _viewModel  = [[FBSMineInfoViewModel alloc] init];
    }
    return _viewModel;
}


@end
