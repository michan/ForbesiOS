//
//  FBSMineEditViewModel.h
//  Forbes
//
//  Created by 周灿华 on 2019/8/10.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSMineViewModel.h"

typedef NS_ENUM(NSUInteger, MineEditType) {
    MineEditTypeNickName,
    MineEditTypePhone,
    MineEditTypePassword
};


@interface FBSMineEditViewModel : FBSViewModel

@property (nonatomic, assign) MineEditType editType;

- (instancetype)initWithEditType:(MineEditType)editType;

- (void)rqUpdateNickname:(NSString *)nickname complete:(void(^)(BOOL success, NSString *msg))complete;

- (void)rqSendSMSCode:(NSString *)tel complete:(void(^)(BOOL success))complete;

- (void)rqUpdateTel:(NSString *)tel code:(NSString *)code complete:(void(^)(BOOL success, NSString *msg))complete;


- (void)rqUpdatePassword:(NSString *)pwd complete:(void(^)(BOOL success, NSString *msg))complete;


@end


