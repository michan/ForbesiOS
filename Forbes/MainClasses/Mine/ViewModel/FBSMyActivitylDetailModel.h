//
//  FBSMyActivitylDetailModel.h
//  Forbes
//
//  Created by 赵志辉 on 2019/9/29.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import "FBSBaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface FBSMyActivitylDetailDataActivity :FBSBaseModel
@property (nonatomic , copy) NSString              * id;
@property (nonatomic , copy) NSString              * user_id;
@property (nonatomic , copy) NSString              * title;
@property (nonatomic , copy) NSString              * file_id;
@property (nonatomic , copy) NSString              * limit_number;
@property (nonatomic , copy) NSString              * start_time;
@property (nonatomic , copy) NSString              * end_time;
@property (nonatomic , copy) NSString              * type;
@property (nonatomic , copy) NSString              * activity_time;
@property (nonatomic , copy) NSString              * activity_address;
@property (nonatomic , copy) NSString              * activity_sponsor;
@property (nonatomic , copy) NSString              * created_at;
@property (nonatomic , copy) NSString              * updated_at;

@end


@interface FBSMyActivitylDetailDataActivity_ticket :FBSBaseModel
@property (nonatomic , copy) NSString              * id;
@property (nonatomic , copy) NSString              * activity_id;
@property (nonatomic , copy) NSString              * title;
@property (nonatomic , copy) NSString              * type;
@property (nonatomic , copy) NSString              * price;
@property (nonatomic , copy) NSString              * remarks;
@property (nonatomic , copy) NSString              * number;
@property (nonatomic , copy) NSString              * receive_number;
@property (nonatomic , copy) NSString              * created_at;
@property (nonatomic , copy) NSString              * updated_at;

@end


@interface FBSMyActivitylDetailDataActivity_date :FBSBaseModel
@property (nonatomic , copy) NSString              * id;
@property (nonatomic , copy) NSString              * activity_id;
@property (nonatomic , copy) NSString              * area_and_date;
@property (nonatomic , copy) NSString              * created_at;
@property (nonatomic , copy) NSString              * updated_at;

@end


@interface FBSMyActivitylDetailDataModel :FBSBaseModel
@property (nonatomic , copy) NSString              * id;
@property (nonatomic , copy) NSString              * activity_id;
@property (nonatomic , copy) NSString              * activity_ticket_id;
@property (nonatomic , copy) NSString              * activity_ticket_code;
@property (nonatomic , copy) NSString              * activity_date_id;
@property (nonatomic , copy) NSString              * user_id;
@property (nonatomic , copy) NSString              * name;
@property (nonatomic , copy) NSString              * ipone;
@property (nonatomic , copy) NSString              * email;
@property (nonatomic , copy) NSString              * sex;
@property (nonatomic , copy) NSString              * is_ok;
@property (nonatomic , copy) NSString              * position;
@property (nonatomic , copy) NSString              * corporate_name;
@property (nonatomic , copy) NSString              * status;
@property (nonatomic , copy) NSString              * created_at;
@property (nonatomic , copy) NSString              * updated_at;
@property (nonatomic , strong) FBSMyActivitylDetailDataActivity              * activity;
@property (nonatomic , strong) FBSMyActivitylDetailDataActivity_ticket              * activity_ticket;
@property (nonatomic , strong) FBSMyActivitylDetailDataActivity_date              * activity_date;

@end


@interface FBSMyActivitylDetailModel :FBSBaseModel
@property (nonatomic , strong) FBSMyActivitylDetailDataModel              * data;
//@property (nonatomic , copy) NSString              success;
//@property (nonatomic , copy) NSString              code;
//@property (nonatomic , copy) NSString              * msg;

@end

NS_ASSUME_NONNULL_END
