//
//  FBSInvoiceSuccessViewModel.h
//  Forbes
//
//  Created by 周灿华 on 2019/8/18.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSTableViewModel.h"
#import "FBSSuccessCell.h"
#import "FBSButtonCell.h"

@interface FBSInvoiceSuccessViewModel : FBSTableViewModel

@end

