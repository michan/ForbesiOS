//
//  FBSContributeAddViewModel.h
//  Forbes
//
//  Created by 周灿华 on 2019/8/11.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSTableViewModel.h"
#import "FBSContributeTitleCell.h"
#import "FBSContributeInputContentCell.h"
#import "FBSContributeAddContentCell.h"
#import "FBSButtonCell.h"
#import "FBSTagsCell.h"
#import "FBSActivityCatageryModel.h"
#import "FBSlingyuModel.h"


#define CategoryTag 1000
#define ContentTag  2000

@interface FBSContributeAddViewModel : FBSTableViewModel
@property (nonatomic, strong) NSMutableArray *contentArr;
@property (nonatomic, strong) FBSTagsItem *tagsItem;
@property (nonatomic, strong) FBSActivityCatageryModel *model;

@property (nonatomic, strong) FBSContributeInputContentItem *inputItem;
@property (nonatomic, strong) FBSlingyuModel *lingyuModel;
- (void)createArticleDetail:(void(^)(BOOL success))complete;
- (void)createChannel_idDetail:(void(^)(BOOL success))complete;
- (void)categoryExpand:(BOOL)expand;

- (void)contentExpand:(BOOL)expand;

- (void)sendArticle:(NSArray *)array Detail:(void(^)(BOOL success,NSString *bodyHTML))complete;
- (void)uploadfile:(NSArray *)array Detail:(void(^)(BOOL success,NSString *bodyHTML))complete;
@end

