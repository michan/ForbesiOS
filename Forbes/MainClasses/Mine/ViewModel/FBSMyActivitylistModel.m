//
//  FBSMyActivitylistModel.m
//  Forbes
//
//  Created by 赵志辉 on 2019/9/29.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import "FBSMyActivitylistModel.h"

@implementation FBSMyActivitylistDataItemsItem
- (id)mj_newValueFromOldValue:(id)oldValue property:(MJProperty *)property
{
    if([property.name isEqualToString:@"status"]){
        NSString *oldValue1 = [(NSNumber *)oldValue stringValue];
        if ([oldValue1 isEqualToString:@"1"]) {
            return @"待付款";
        }else if ([oldValue1 isEqualToString:@"2"]){
            return @"已付款（待分配）";
        }else if ([oldValue1 isEqualToString:@"3"]){
            return @"已取消（已取消付款）";
        }else if ([oldValue1 isEqualToString:@"4"]){
            return @"退票";
        }else if ([oldValue1 isEqualToString:@"5"]){
            return @"关闭";
        }else if ([oldValue1 isEqualToString:@"6"]){
            return @"已使用";
        }else if ([oldValue1 isEqualToString:@"7"]){
            return @"待分配";
        }else if ([oldValue1 isEqualToString:@"8"]){
            return @"待使用";
        }else if ([oldValue1 isEqualToString:@"9"]){
            return @"已使用";
        }else if ([oldValue1 isEqualToString:@"10"]){
            return @"退票申请";
        }
        return oldValue;
    }else if ([property.name isEqualToString:@"is_ok"]){
        NSString *oldValue1 = [(NSNumber *)oldValue stringValue];
        if ([oldValue1 isEqualToString:@"0"]) {
            return @"审核不通过";
        }else if ([oldValue1 isEqualToString:@"1"]){
            return @"审核已通过";
        }else if ([oldValue1 isEqualToString:@"2"]){
            return @"审核未通过";
        }else if ([oldValue1 isEqualToString:@"3"]){
            return @"已取票";
        }
        return oldValue;
    }
    return oldValue;
}
//is_ok：
//0.审核不通过
//1.审核已通过
//2.审核未通过
//3.已取票
@end


@implementation FBSMyActivitylistDataModel

//1.待付款
//2.已付款（待分配）
//3.已取消（已取消付款）
//4.退票
//5.关闭
//6.已使用
//7.待分配
//8.待使用
//9.已使用
//10.退票申请
+ (NSDictionary *)mj_objectClassInArray {
    return @{@"items" : @"FBSMyActivitylistDataItemsItem"};
}
@end


@implementation FBSMyActivitylistModel
@end
