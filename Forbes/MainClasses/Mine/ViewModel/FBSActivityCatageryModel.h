//
//  FBSActivityCatageryModel.h
//  Forbes
//
//  Created by 赵志辉 on 2019/10/8.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import "FBSBaseModel.h"

NS_ASSUME_NONNULL_BEGIN

//"channel_id": 5,
//"title": "活动",
//"channel_catgory_id": 0,
//"category": null
@interface FBSActivityCatageryItemModel : FBSBaseModel
@property (nonatomic , copy) NSString              * channel_id;
@property (nonatomic , copy) NSString              * title;
@property (nonatomic , copy) NSString              * channel_catgory_id;
@property (nonatomic , copy) NSString              * category;
@end


@interface FBSActivityCatageryModel : FBSBaseModel

@property (nonatomic , strong) NSArray  * data;

@end

NS_ASSUME_NONNULL_END
