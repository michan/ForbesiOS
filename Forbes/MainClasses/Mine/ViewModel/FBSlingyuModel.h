//
//  FBSlingyuModel.h
//  Forbes
//
//  Created by 赵志辉 on 2019/10/8.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import "FBSBaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface FBSlingyuItemModel : FBSBaseModel
@property (nonatomic , copy) NSString              * id;
@property (nonatomic , copy) NSString              * title;

@end
@interface FBSlingyuModel : FBSBaseModel

@property (nonatomic , strong) NSArray  * data;

@end

NS_ASSUME_NONNULL_END
