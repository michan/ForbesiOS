//
//  FBSContributeListViewModel.h
//  Forbes
//
//  Created by 周灿华 on 2019/8/10.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSTableViewModel.h"
#import "FBSContributeListCell.h"
#import "FBSMyarticlesListModel.h"

@interface FBSContributeListViewModel : FBSTableViewModel

@property (nonatomic, strong)FBSMyarticlesListModel *model;


- (void)createArticleListL:(NSString *)type isMore:(BOOL)isMore complete:(void(^)(BOOL success ,NSUInteger count))complete;
@end

