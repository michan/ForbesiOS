//
//  FBSMineViewModel.m
//  Forbes
//
//  Created by 周灿华 on 2019/7/27.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSMineViewModel.h"
#import "FBSUserData.h"

@implementation FBSMineViewModel

- (void)makeItems {
    [self.items removeAllObjects];
    
    self.headItem = [FBSMineHeadItem item];
    if ([FBSUserData sharedData].isLogin) {
        self.headItem.name = [FBSUserData sharedData].nickname;
    } else {
        self.headItem.name = @"点击登录";
    }
    self.headItem.cellHeight = WScale(144) + kStatusBarH;
    [self.items addObject:self.headItem];

    FBSMineOperateItem *operationItem = [FBSMineOperateItem item];
    operationItem.bottomLineHidden = YES;
    operationItem.cellHeight = WScale(70);
    [self.items addObject:operationItem];

    [self.items addObject:[FBSSpaceItem item]];
    
    NSArray *titles  = @[
                         @"我的关注",
                         @"我的活动",
//                         @"投稿管理",
//                         @"发票申请",
//                         @"意见反馈",
                         @"设置",
                         ];
    int tags[] = {
                  FollowEntryTag,
                  ActivityEntryTag,
//                  ContributeEntryTag,
//                  InvoiceEntryTag,
//                 FeedbackEntryTag,
                  SettingEntryTag};
    

    for (NSInteger i = 0; i < titles.count; i++) {
        FBSMineEntryItem *entryItem = [FBSMineEntryItem item];
        entryItem.tag = tags[i];
        entryItem.title = [titles objectAtIndex:i];
        entryItem.cellHeight = WScale(50);
        [self.items addObject:entryItem];
        
        if (entryItem.tag == InvoiceEntryTag) {
            [self.items addObject:[FBSSpaceItem item]];
            entryItem.bottomLineHidden = YES;
        }
        
    }
    
    
    if ([FBSUserData sharedData].isLogin) {
        //退出按钮
        FBSButtonItem *logoutItem = [FBSButtonItem item];
        logoutItem.buttonText = @"退出登录";
        logoutItem.buttonTextColor = [UIColor whiteColor];
        logoutItem.buttonBgColor = MainThemeColor;
        logoutItem.bottomLineHidden = YES;
        logoutItem.cellHeight = WScale(270);
        [self.items addObject:logoutItem];

    }
}

@end
