//
//  FBSMyActivitylistModel.h
//  Forbes
//
//  Created by 赵志辉 on 2019/9/29.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import "FBSBaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface FBSMyActivitylistDataItemsItem :NSObject
@property (nonatomic , copy) NSString              * id;
@property (nonatomic , copy) NSString              * activity_ticket_code;
@property (nonatomic , copy) NSString              * status;
@property (nonatomic , copy) NSString              * is_ok;
@property (nonatomic , copy) NSString              * created_at;
@property (nonatomic , copy) NSString              * type;
@property (nonatomic , copy) NSString              * remarks;
@property (nonatomic , copy) NSString              * ticket_title;
@property (nonatomic , copy) NSString              * title;

@end


@interface FBSMyActivitylistDataModel :NSObject
@property (nonatomic , copy) NSString              * page;
@property (nonatomic , copy) NSString              * total;
@property (nonatomic , strong) NSArray <FBSMyActivitylistDataItemsItem *>              * items;

@end


@interface FBSMyActivitylistModel :NSObject
@property (nonatomic , strong) FBSMyActivitylistDataModel              * data;
//@property (nonatomic , copy) NSString              success;
//@property (nonatomic , copy) NSString              code;
//@property (nonatomic , copy) NSString              * msg;

@end

NS_ASSUME_NONNULL_END
