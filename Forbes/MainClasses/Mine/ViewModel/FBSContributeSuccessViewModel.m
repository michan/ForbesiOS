//
//  FBSContributeSuccessViewModel.m
//  Forbes
//
//  Created by 周灿华 on 2019/8/11.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSContributeSuccessViewModel.h"

@implementation FBSContributeSuccessViewModel

- (instancetype)initWithimage:(NSString *)imageURL success:(NSString *)successsStr content:(NSString *)content button:(NSString *)buttonStr
{
    if (self = [super init]) {
        FBSSuccessItem *successItem = [FBSSuccessItem item];
        successItem.title = successsStr;
        successItem.tip = content;
        successItem.cellHeight = WScale(230);
        successItem.icon = imageURL;
        [self.items addObject:successItem];
        
        FBSButtonItem *finishItem = [FBSButtonItem item];
        finishItem.buttonText = buttonStr;
        finishItem.buttonTextColor = [UIColor whiteColor];
        finishItem.buttonBgColor = MainThemeColor;
        finishItem.cellHeight = kScreenHeight - successItem.cellHeight - kNavTopMargin;
        [self.items addObject:finishItem];
    }
    return self;
}
//- (void)makeItems {
//
//    FBSSuccessItem *successItem = [FBSSuccessItem item];
//    successItem.title = @"发布成功！";
//    successItem.tip = @"感谢您对福布斯的支持，我们将会尽快审核您的文章。";
//    successItem.cellHeight = WScale(230);
//    [self.items addObject:successItem];
//
//
//    FBSButtonItem *finishItem = [FBSButtonItem item];
//    finishItem.buttonText = @"完成";
//    finishItem.buttonTextColor = [UIColor whiteColor];
//    finishItem.buttonBgColor = MainThemeColor;
//    finishItem.cellHeight = kScreenHeight - successItem.cellHeight - kNavTopMargin;
//    [self.items addObject:finishItem];
//}
//
@end
