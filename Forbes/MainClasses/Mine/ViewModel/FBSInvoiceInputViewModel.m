//
//  FBSInvoiceInputViewModel.m
//  Forbes
//
//  Created by 周灿华 on 2019/8/18.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSInvoiceInputViewModel.h"


@interface FBSInvoiceInputViewModel ()
@property (nonatomic, strong) FBSInvoiceContentItem *invoiceTitleItem;
@property (nonatomic, strong) FBSInvoiceTipItem *tipItem;
@property (nonatomic, strong) FBSInvoiceContentItem *registrationItem;
@property (nonatomic, strong) FBSInvoiceContentItem *receiverItem;
@property (nonatomic, strong) FBSInvoiceContentItem *phoneItem;
@property (nonatomic, strong) FBSInvoiceContentItem *addressItem;
@property (nonatomic, strong) FBSInvoiceDescItem *descItem;
@property (nonatomic, strong) FBSButtonItem *commitItem;


//special
@property (nonatomic, strong) FBSInvoiceContentItem *bankItem;
@property (nonatomic, strong) FBSInvoiceContentItem *bankAccItem;
@property (nonatomic, strong) FBSInvoiceContentItem *registerPhoneItem;
@property (nonatomic, strong) FBSInvoiceContentItem *registerAddressItem;


@end


@implementation FBSInvoiceInputViewModel


- (instancetype)initWithType:(NSString *)type
{
    self = [super init];
    if (self) {
        self.type = type;
    }
    return self;
}


- (void)makeItems {

    FBSSpaceItem *space0Item = [FBSSpaceItem item];
    space0Item.cellHeight = WScale(5);
    space0Item.backgroundColor = [UIColor whiteColor];
    [self.items addObject:space0Item];
    
    [self.items addObject:self.invoiceTitleItem];
    [self.items addObject:self.tipItem];
    [self.items addObject:self.registrationItem];
    
    
    if ([self.type isEqualToString:@"0"]) {
        [self.items addObject:self.receiverItem];
        [self.items addObject:self.phoneItem];
        
    } else {
        
        [self.items addObject:self.bankItem];
        [self.items addObject:self.bankAccItem];
        [self.items addObject:self.registerPhoneItem];
        [self.items addObject:self.registerAddressItem];

    }
    
    FBSSpaceItem *space1Item = [FBSSpaceItem item];
    space1Item.cellHeight = WScale(5);
    [self.items addObject:space1Item];
    
    [self.items addObject:self.addressItem];
    
    FBSSpaceItem *space2Item = [FBSSpaceItem item];
    space2Item.cellHeight = WScale(5);
    [self.items addObject:space2Item];
    
    [self.items addObject:self.descItem];
    
    FBSSpaceItem *space3Item = [FBSSpaceItem item];
    space3Item.cellHeight = WScale(5);
    [self.items addObject:space3Item];
    
    [self.items addObject:self.commitItem];
}


#pragma mark - property

- (FBSInvoiceContentItem *)invoiceTitleItem {
    if (!_invoiceTitleItem) {
        _invoiceTitleItem  = [[FBSInvoiceContentItem alloc] init];
        _invoiceTitleItem.title = @"发票抬头";
        _invoiceTitleItem.placeholder = @"请填写公司全称或个人姓名";
    }
    return _invoiceTitleItem;
}


- (FBSInvoiceTipItem *)tipItem {
    if (!_tipItem) {
        _tipItem = [FBSInvoiceTipItem item];
        _tipItem.cellHeight = WScale(50);
    }
    return _tipItem;
}



- (FBSInvoiceContentItem *)registrationItem {
    if (!_registrationItem) {
        _registrationItem  = [[FBSInvoiceContentItem alloc] init];
        _registrationItem.title = @"纳税人识别号";
        _registrationItem.placeholder = @"请核准企业纳税人识别号";
    }
    return _registrationItem;
}


- (FBSInvoiceContentItem *)receiverItem {
    if (!_receiverItem) {
        _receiverItem  = [[FBSInvoiceContentItem alloc] init];
        _receiverItem.title = @"普票收票人";
        _receiverItem.placeholder = @"请输入普票收票人";
    }
    return _receiverItem;
}


- (FBSInvoiceContentItem *)phoneItem {
    if (!_phoneItem) {
        _phoneItem  = [[FBSInvoiceContentItem alloc] init];
        _phoneItem.title = @"手机号码";
        _phoneItem.placeholder = @"请输入收票人手机号码";
        _phoneItem.keyboardType = UIKeyboardTypeNumberPad;
    }
    return _phoneItem;
}


- (FBSInvoiceContentItem *)addressItem {
    if (!_addressItem) {
        _addressItem  = [[FBSInvoiceContentItem alloc] init];
        _addressItem.title = @"收票地址";
        _addressItem.placeholder = @"添加收件地址";
        _addressItem.canEdit = NO;
        _addressItem.showArrow = YES;
    }
    return _addressItem;
}



- (FBSInvoiceDescItem *)descItem {
    if (!_descItem) {
        _descItem  = [[FBSInvoiceDescItem alloc] init];
        _descItem.cellHeight = WScale(140);
    }
    return _descItem;
}



- (FBSButtonItem *)commitItem {
    if (!_commitItem) {
        _commitItem  = [FBSButtonItem item];
        _commitItem.buttonBgColor = RGB(140,115,75);
        _commitItem.buttonTextColor = [UIColor whiteColor];
        _commitItem.buttonText = @"提交";
        _commitItem.cellHeight = WScale(80) + kDiffTabBarH;
    }
    return _commitItem;
}



- (FBSInvoiceContentItem *)bankItem {
    if (!_bankItem) {
        _bankItem  = [[FBSInvoiceContentItem alloc] init];
        _bankItem.title = @"开户银行";
        _bankItem.placeholder = @"需明确到支行信息";
        _bankItem.canEdit = NO;
        _bankItem.showArrow = YES;
    }
    return _bankItem;
}


- (FBSInvoiceContentItem *)bankAccItem {
    if (!_bankAccItem) {
        _bankAccItem  = [[FBSInvoiceContentItem alloc] init];
        _bankAccItem.title = @"银行账号";
        _bankAccItem.placeholder = @"请输入银行账号";
        _bankAccItem.canEdit = NO;
        _bankAccItem.showArrow = YES;
    }
    return _bankAccItem;
}


- (FBSInvoiceContentItem *)registerPhoneItem {
    if (!_registerPhoneItem) {
        _registerPhoneItem  = [[FBSInvoiceContentItem alloc] init];
        _registerPhoneItem.title = @"注册电话";
        _registerPhoneItem.placeholder = @"请输入注册电话";
    }
    return _registerPhoneItem;
}



- (FBSInvoiceContentItem *)registerAddressItem {
    if (!_registerAddressItem) {
        _registerAddressItem  = [[FBSInvoiceContentItem alloc] init];
        _registerAddressItem.title = @"注册地址";
        _registerAddressItem.placeholder = @"请填写公司注册地址";
    }
    return _registerAddressItem;
}





@end
