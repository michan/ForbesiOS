//
//  FBSMyActivitylistViewModel.m
//  Forbes
//
//  Created by 赵志辉 on 2019/9/29.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import "FBSMyActivitylistViewModel.h"
#import "FBSMyActivitylistModel.h"
#import "FBSMyActivityListItem.h"
#import "HLWJMD5.h"


@interface FBSMyActivitylistViewModel()

@property(nonatomic, assign)int count;
@end

@implementation FBSMyActivitylistViewModel

- (void)createItem{
    NSMutableArray *array = [NSMutableArray array];
    
    for (FBSMyActivitylistDataModel *model in self.model.data.items) {
        NSDictionary *dic = [model mj_keyValues];
        FBSMyActivityListItem *item = [FBSMyActivityListItem mj_objectWithKeyValues:dic];
        [array addObject:item];
        item.cellHeight = WScale(190);
    }
    self.items = array;
}
- (void)createItemMore{
    NSMutableArray *array = [NSMutableArray array];
    for (FBSMyActivitylistDataModel *model in self.model.data.items) {
        NSDictionary *dic = [model mj_keyValues];
        FBSMyActivityListItem *item = [FBSMyActivityListItem mj_objectWithKeyValues:dic];
        [array addObject:item];
    }
    
    [self.items addObjectsFromArray:array];
}
// 请求活动列表
- (void)requestActivitiesIsMore:(BOOL)isMore complete:(void(^)(BOOL success, NSInteger count))complete{
    FBSGetRequest *request = [[FBSGetRequest alloc] init];
    request.cacheHandler.writeMode = YBNetworkCacheWriteModeMemoryAndDisk;
    request.cacheHandler.readMode = YBNetworkCacheReadModeAlsoNetwork;
    request.requestMethod = YBRequestMethodGET;
    request.requestURI = @"user/activities";
    FBSUserData *user = [FBSUserData sharedData];
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setObject:user.useId forKey:@"user_id"];
    if (self.is_ok.length) {
        [dic setObject:self.is_ok forKey:@"is_ok"];
    }
    [dic setObject:[NSString stringWithFormat:@"%ld",self.page] forKey:@"page"];
    request.requestParameter = dic;
    WEAKSELF;
    [request startWithSuccess:^(YBNetworkResponse * _Nonnull response) {
        weakSelf.model = [FBSMyActivitylistModel mj_objectWithKeyValues:response.responseObject];
        if (!isMore) {
           [self createItem];
        }else{
           [self createItemMore];
        }
        complete(YES,weakSelf.model.data.items.count);
    } failure:^(YBNetworkResponse * _Nonnull response) {
        
    }];
}
// 请求活动列表
- (void)delegatedelegatArray:(NSMutableArray *)arrayItem complete:(void(^)(BOOL success ,int count))complete{
    
    dispatch_group_t group = dispatch_group_create();
    for (FBSMyActivityListItem *item in arrayItem) {
        dispatch_group_enter(group);
        FBSGetRequest *request = [[FBSGetRequest alloc] init];
        request.cacheHandler.writeMode = YBNetworkCacheWriteModeMemoryAndDisk;
        request.cacheHandler.readMode = YBNetworkCacheReadModeAlsoNetwork;
        request.requestMethod = YBRequestMethodPOST;
        request.requestURI = @"user/deleteOrder";
        FBSUserData *user = [FBSUserData sharedData];
        NSMutableDictionary *dic = [NSMutableDictionary dictionary];
        [dic setObject:user.useId forKey:@"user_id"];
        [dic setObject:item.id forKey:@"order_id"];
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        formatter.dateFormat = @"yyyy-MM-dd";
        NSString *plainText = [NSString stringWithFormat:@"%@%@",user.account,[formatter stringFromDate:[NSDate date]]];
        [dic setObject:[HLWJMD5 MD5ForLower32Bate:plainText] forKey:@"token"];
        request.requestParameter = dic;
        WEAKSELF
        [request startWithSuccess:^(YBNetworkResponse * _Nonnull response) {
            dispatch_group_leave(group);
            NSNumber *num = response.responseObject[@"success"];
            if([[num stringValue] isEqualToString:@"1"]){
                [arrayItem removeObject:item];
                [weakSelf.items removeObject:item];
                weakSelf.count ++;
                
            }else{
                
            }
        } failure:^(YBNetworkResponse * _Nonnull response) {
            dispatch_group_leave(group);
            
        }];
    }
    dispatch_group_notify(group, dispatch_get_main_queue(), ^{
        complete(YES,self.count);
    });
    
}
@end
