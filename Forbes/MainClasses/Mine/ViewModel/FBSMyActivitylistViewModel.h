//
//  FBSMyActivitylistViewModel.h
//  Forbes
//
//  Created by 赵志辉 on 2019/9/29.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FBSMyActivitylistModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface FBSMyActivitylistViewModel : FBSTableViewModel
@property(nonatomic ,strong)FBSMyActivitylistModel *model;
@property(nonatomic, strong)NSString *is_ok;

- (void)requestActivitiesIsMore:(BOOL)isMore complete:(void(^)(BOOL success, NSInteger count))complete;
- (void)delegatedelegatArray:(NSMutableArray *)arrayItem complete:(void(^)(BOOL success,int count))complete;
@end

NS_ASSUME_NONNULL_END
