//
//  FBSInvoiceInputViewModel.h
//  Forbes
//
//  Created by 周灿华 on 2019/8/18.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSTableViewModel.h"
#import "FBSInvoiceContentCell.h"
#import "FBSInvoiceTipCell.h"
#import "FBSButtonCell.h"
#import "FBSSpaceCell.h"
#import "FBSInvoiceDescCell.h"

@interface FBSInvoiceInputViewModel : FBSTableViewModel

@property (nonatomic, strong) NSString *type;

- (instancetype)initWithType:(NSString *)type;

@end

