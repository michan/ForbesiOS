
//
//  FBSMineEditViewModel.m
//  Forbes
//
//  Created by 周灿华 on 2019/8/10.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSMineEditViewModel.h"
#import "HLWJMD5.h"

@interface FBSMineEditViewModel ()
@property (nonatomic, strong) FBSPostRequest *updateNicknameAPI;
@property (nonatomic, strong) FBSPostRequest *sendSMSCodeAPI;
@property (nonatomic, strong) FBSPostRequest *updateTelAPI;
@property (nonatomic, strong) FBSPostRequest *updatePasswordAPI;
@end

@implementation FBSMineEditViewModel

- (instancetype)initWithEditType:(MineEditType)editType {
    self = [super init];
    if (self) {
        self.editType = editType;
    }
    return self;
}


#pragma mark - request

- (void)rqUpdateNickname:(NSString *)nickname complete:(void(^)(BOOL success, NSString *msg))complete {
    if (!nickname.length) {
        return ;
    }
    
    self.updateNicknameAPI.requestParameter = @{
                                          @"id" : [FBSUserData sharedData].useId,
                                          @"nickname" : nickname,
                                          @"token" : [FBSUserData sharedData].token,
                                          };
    
    [self.updateNicknameAPI startWithSuccess:^(YBNetworkResponse * _Nonnull response) {
        
        FBSLog(@"修改用户昵称结果 : %@",response.responseObject);
        
        if (complete) {
            FBSBaseModel *model = [FBSBaseModel mj_objectWithKeyValues:response.responseObject];
            complete(model.success, model.msg);
        }
    } failure:^(YBNetworkResponse * _Nonnull response) {
        if (complete) {
            complete(NO, response.error.localizedDescription);
        }
        
    }];
}


- (void)rqSendSMSCode:(NSString *)tel complete:(void(^)(BOOL success))complete {
    if (!tel.length) {
        return ;
    }
    
    self.sendSMSCodeAPI.requestParameter = @{
                                                @"id" : [FBSUserData sharedData].useId,
                                                @"tel" : tel,
                                                @"token" : [FBSUserData sharedData].token,
                                                };
    
    [self.sendSMSCodeAPI startWithSuccess:^(YBNetworkResponse * _Nonnull response) {
        
        FBSLog(@"发送更换手机号码验证码结果 : %@",response.responseObject);
        
        if (complete) {
            FBSBaseModel *model = [FBSBaseModel mj_objectWithKeyValues:response.responseObject];
            complete(model.success);
        }
    } failure:^(YBNetworkResponse * _Nonnull response) {
        if (complete) {
            complete(NO);
        }
        
    }];
}


- (void)rqUpdateTel:(NSString *)tel code:(NSString *)code complete:(void(^)(BOOL success, NSString *msg))complete {
    if (!code.length) {
        return ;
    }
    
    self.updateTelAPI.requestParameter = @{
                                             @"id" : [FBSUserData sharedData].useId,
                                             @"tel" : tel,
                                             @"code" : code,
                                             };
    
    [self.updateTelAPI startWithSuccess:^(YBNetworkResponse * _Nonnull response) {
        
        FBSLog(@"变更手机号码 : %@",response.responseObject);
        
        if (complete) {
            FBSBaseModel *model = [FBSBaseModel mj_objectWithKeyValues:response.responseObject];
            complete(model.success,model.msg);
        }
    } failure:^(YBNetworkResponse * _Nonnull response) {
        if (complete) {
            complete(NO, response.responseObject);
        }
        
    }];
}


- (void)rqUpdatePassword:(NSString *)pwd complete:(void(^)(BOOL success, NSString *msg))complete {
    if (!pwd.length) {
        return ;
    }
    
    self.updatePasswordAPI.requestParameter = @{
                                           @"user_id" : [FBSUserData sharedData].useId,
                                           @"password" : pwd,
                                           @"token" : [FBSUserData sharedData].token,
                                           };
    
    [self.updatePasswordAPI startWithSuccess:^(YBNetworkResponse * _Nonnull response) {
        
        FBSLog(@"变更用户密码 : %@",response.responseObject);
        
        if (complete) {
            FBSBaseModel *model = [FBSBaseModel mj_objectWithKeyValues:response.responseObject];
            complete(model.success,model.msg);
        }
    } failure:^(YBNetworkResponse * _Nonnull response) {
        if (complete) {
            complete(NO, response.error.localizedDescription);
        }
        
    }];
    
}


#pragma mark - property

- (FBSPostRequest *)updateNicknameAPI {
    if (!_updateNicknameAPI) {
        _updateNicknameAPI  = [[FBSPostRequest alloc] init];
        _updateNicknameAPI.requestURI = @"user/updateNickname";
    }
    return _updateNicknameAPI;
}


- (FBSPostRequest *)sendSMSCodeAPI {
    if (!_sendSMSCodeAPI) {
        _sendSMSCodeAPI  = [[FBSPostRequest alloc] init];
        _sendSMSCodeAPI.requestURI = @"user/sendSMSCode";
    }
    return _sendSMSCodeAPI;
}



- (FBSPostRequest *)updateTelAPI {
    if (!_updateTelAPI) {
        _updateTelAPI = [[FBSPostRequest alloc] init];
        _updateTelAPI.requestURI = @"user/updateTel";
    }
    return _updateTelAPI;
}



- (FBSPostRequest *)updatePasswordAPI {
    if (!_updatePasswordAPI) {
        _updatePasswordAPI = [[FBSPostRequest alloc] init];
        _updatePasswordAPI.requestURI = @"user/updatePassword";
    }
    return _updatePasswordAPI;
}





@end
