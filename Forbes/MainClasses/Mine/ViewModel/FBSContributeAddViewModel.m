
//
//  FBSContributeAddViewModel.m
//  Forbes
//
//  Created by 周灿华 on 2019/8/11.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSContributeAddViewModel.h"
#import "HLWJMD5.h"
#import "FBSActivityCatageryModel.h"
#import "ImageModel.h"
#import "CustomYYImage.h"
#import "FBSOnlyOneInputItem.h"


@interface FBSContributeAddViewModel ()<ImageViewDataSource>
@property (nonatomic, strong) FBSContributeTitleItem *categoryTitleItem;
@property (nonatomic, strong) FBSContributeTitleItem *contentTitleItem;
@property (nonatomic, strong) NSArray *tags;
@property (nonatomic, strong) FBSPostRequest *sendPost;


@end

@implementation FBSContributeAddViewModel

- (void)createArticleDetail:(void(^)(BOOL success))complete{
    FBSGetRequest *request = [[FBSGetRequest alloc] init];
     FBSUserData *user = [FBSUserData sharedData];
    request.requestURI = [NSString stringWithFormat:@"user/article/categories/%@",user.useId];
    [request startWithSuccess:^(YBNetworkResponse * _Nonnull response) {
        self.model = [FBSActivityCatageryModel mj_objectWithKeyValues:response.responseObject];
        complete(YES);
    } failure:^(YBNetworkResponse * _Nonnull response) {
        complete(NO);
    }];
    
}
- (void)createChannel_idDetail:(void(^)(BOOL success))complete{
    FBSGetRequest *request = [[FBSGetRequest alloc] init];
    for (NSString *str in self.tagsItem.selectedTags) {
        for (FBSActivityCatageryItemModel *itemData in self.model.data) {
            if ([itemData.title isEqualToString:str]) {
                 request.requestURI = [NSString stringWithFormat:@"user/field/%@",itemData.channel_id];
                break;
            }
        }
    }
    [request startWithSuccess:^(YBNetworkResponse * _Nonnull response) {
        self.lingyuModel = [FBSlingyuModel mj_objectWithKeyValues:response.responseObject];
        complete(YES);
    } failure:^(YBNetworkResponse * _Nonnull response) {
        complete(NO);
    }];
    
}
- (NSMutableAttributedString *)cretae:(UIImage *)image att:(NSMutableAttributedString *)contentText point:(CGPoint)point{

        
        NSString*altStr = [NSString stringWithFormat:@"http://7xojj3.com1.z0.glb.clouddn.com/image/sharepic/%@",@"imageName"];
        CustomYYImage *imageView = [[CustomYYImage alloc] initWithDataSourece:self];
        imageView.imageW = image.size.width;
        imageView.imageH = image.size.height;
        
        float scale = (kScreenWidth-30)/image.size.width;
        image = [self imageByScalingAndCroppingForSize:CGSizeMake(kScreenWidth-30, image.size.height*scale) forImage:image];
        imageView.image = image;
        imageView.urlStr = altStr;
        imageView.location = contentText.length ;
        contentText = [[self p_textViewAttributedText:imageView contentText:contentText index:contentText.length originPoint:point isData:NO] mutableCopy];
        
        [contentText insertAttributedString:[[NSAttributedString alloc] initWithString:@"\n"] atIndex:contentText.length];
    [contentText setFont:[UIFont systemFontOfSize:16]];
    contentText.lineSpacing = 15;
    return [contentText mutableCopy];
}
#pragma mark - YYTextView Get Attributed String
- (NSAttributedString *)p_textViewAttributedText:(id)attribute contentText:(NSAttributedString *)attributeString index:(NSInteger)index originPoint:(CGPoint)originPoint isData:(BOOL)isData {
    NSMutableAttributedString *contentText = [attributeString mutableCopy];
    NSAttributedString *textAttachmentString = [[NSAttributedString alloc] initWithString:@"\n"];
    if ([attribute isKindOfClass:[CustomYYImage class]]) {
        CustomYYImage *imageView = (CustomYYImage *)attribute;
        CGFloat imageViewHeight = ![imageView.title isEqualToString:@""] ? kScreenWidth + 30.0 : kScreenWidth;
        float scale = (kScreenWidth-30)/imageView.imageW;
        imageView.frame = CGRectMake(originPoint.x, originPoint.y, kScreenWidth-30, imageView.imageH*scale);
        //        imageView.frame = CGRectMake(originPoint.x, originPoint.y, SIZE.width-30, 200);
        
        NSMutableAttributedString *attachText = [NSMutableAttributedString attachmentStringWithContent:imageView contentMode:UIViewContentModeScaleAspectFit attachmentSize:imageView.frame.size alignToFont:[UIFont systemFontOfSize:16] alignment:YYTextVerticalAlignmentCenter];
        if (!isData) [contentText insertAttributedString:textAttachmentString atIndex:index++];
        [contentText insertAttributedString:attachText atIndex:index++];
        
        
    }
    return contentText;
}

//图片伸缩到指定大小
- (UIImage*)imageByScalingAndCroppingForSize:(CGSize)targetSize forImage:(UIImage *)originImage
{
    UIImage *sourceImage = originImage;// 原图
    UIImage *newImage = nil;// 新图
    // 原图尺寸
    CGSize imageSize = sourceImage.size;
    CGFloat width = imageSize.width;
    CGFloat height = imageSize.height;
    CGFloat targetWidth = targetSize.width;// 目标宽度
    CGFloat targetHeight = targetSize.height;// 目标高度
    // 伸缩参数初始化
    CGFloat scaleFactor = 0.0;
    CGFloat scaledWidth = targetWidth;
    CGFloat scaledHeight = targetHeight;
    CGPoint thumbnailPoint = CGPointMake(0.0,0.0);
    
    if (CGSizeEqualToSize(imageSize, targetSize) == NO)
    {// 如果原尺寸与目标尺寸不同才执行
        CGFloat widthFactor = targetWidth / width;
        CGFloat heightFactor = targetHeight / height;
        
        if (widthFactor > heightFactor)
            scaleFactor = widthFactor; // 根据宽度伸缩
        else
            scaleFactor = heightFactor; // 根据高度伸缩
        scaledWidth= width * scaleFactor;
        scaledHeight = height * scaleFactor;
        
        // 定位图片的中心点
        if (widthFactor > heightFactor)
        {
            thumbnailPoint.y = (targetHeight - scaledHeight) * 0.5;
        }
        else if (widthFactor < heightFactor)
        {
            thumbnailPoint.x = (targetWidth - scaledWidth) * 0.5;
        }
    }
    
    // 创建基于位图的上下文
    UIGraphicsBeginImageContext(targetSize);
    
    // 目标尺寸
    CGRect thumbnailRect = CGRectZero;
    thumbnailRect.origin = thumbnailPoint;
    thumbnailRect.size.width= scaledWidth;
    thumbnailRect.size.height = scaledHeight;
    
    [sourceImage drawInRect:thumbnailRect];
    
    // 新图片
    newImage = UIGraphicsGetImageFromCurrentImageContext();
    if(newImage == nil)
        NSLog(@"could not scale image");
    
    // 退出位图上下文
    UIGraphicsEndImageContext();
    return newImage;
}

- (void)makeItems {
    NSUserDefaults *user = [NSUserDefaults standardUserDefaults];
    
    //取出
    NSString *text = [user objectForKey:@"title"];
    FBSOnlyOneInputItem *title1Item = [FBSOnlyOneInputItem item];
    if (text.length) {
        title1Item.text  = text;
    }
    title1Item.placeholder = @"添加标题";
    title1Item.bottomLineInsets = UIEdgeInsetsMake(0, WScale(15), 0, WScale(15));
    title1Item.cellHeight = WScale(55);
    [self.items addObject:title1Item];
    
    
    
    //取出
    NSData *data = [user objectForKey:@"AttributedString"];
    NSMutableArray *array  = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    NSMutableAttributedString *att = [[NSMutableAttributedString alloc] init];
    for (id item in array) {
        if ([item isKindOfClass:[ImageModel class]]) {
            ImageModel *model = (ImageModel *)item;
            att = [self cretae:model.image att:att point:model.point];
        }else{
            NSAttributedString *a  = [[NSAttributedString alloc] initWithString:(NSString *)item];
            [att appendAttributedString:a];
        }
    }
    FBSContributeInputContentItem *inputItem = [FBSContributeInputContentItem item];
    inputItem.content = @"";
    inputItem.placeholder = @"请输入正文";
    inputItem.canEdit = YES;
    inputItem.cellHeight = WScale(290);
    inputItem.AttributedString = att;
    [self.items addObject:inputItem];
    self.inputItem = inputItem;

    self.categoryTitleItem = [FBSContributeTitleItem item];
    self.categoryTitleItem.tag = CategoryTag;
    self.categoryTitleItem.title = @"选择文章分类";
    self.categoryTitleItem.canExpand = YES;
    self.categoryTitleItem.bottomLineInsets = UIEdgeInsetsMake(0, WScale(15), 0, WScale(15));
    self.categoryTitleItem.cellHeight = WScale(45);
    [self.items addObject:self.categoryTitleItem];
    
    
    self.contentTitleItem = [FBSContributeTitleItem item];
    self.contentTitleItem.tag = ContentTag;
    self.contentTitleItem.title = @"选择所属领域";
    self.contentTitleItem.canExpand = YES;
    self.contentTitleItem.bottomLineInsets = UIEdgeInsetsMake(0, WScale(15), 0, WScale(15));
    self.contentTitleItem.cellHeight = WScale(45);
    [self.items addObject:self.contentTitleItem];


    
    
    FBSButtonItem *finishItem = [FBSButtonItem item];
    finishItem.buttonText = @"完成";
    finishItem.buttonTextColor = [UIColor whiteColor];
    finishItem.buttonBgColor = MainThemeColor;
    finishItem.cellHeight = 90;
    [self.items addObject:finishItem];
    
}


#pragma mark - private method

- (void)categoryExpand:(BOOL)expand {
    if (expand) {
        NSMutableArray *array = [NSMutableArray array];
        for (FBSActivityCatageryItemModel *model in self.model.data) {
            [array addObject:model.title];
        }
        self.tagsItem.tags = array;
        NSInteger index = [self indexPathForItem:self.categoryTitleItem].row;
         [self.items insertObject:self.tagsItem atIndex:index + 1];
    } else {
        [self.items removeObject:self.tagsItem];
    }
}

- (void)contentExpand:(BOOL)expand {
    if (expand) {
        [_contentArr removeAllObjects];
        for (NSInteger i = 0; i < self.lingyuModel.data.count; i++) {
            FBSlingyuItemModel *model = self.lingyuModel.data[i];
            FBSContributeAddContentItem *item = [FBSContributeAddContentItem item];
            item.content = model.title;
            item.isSelecte = NO;
            [_contentArr addObject:item];
        }
        NSInteger index = [self indexPathForItem:self.contentTitleItem].row;
        for (NSInteger i = 0; i < self.contentArr.count; i++) {
            [self.items insertObject:[self.contentArr objectAtIndex:i] atIndex:index + i + 1];
        }
        
    } else {
        if (self.contentTitleItem.isExpand == YES) {
            self.contentTitleItem.isExpand = NO;
            for (NSInteger i = 0; i < self.contentArr.count; i++) {
                [self.items removeObject:[self.contentArr objectAtIndex:i]];
            }
        }
    }
    
}
- (void)uploadfile:(NSArray *)array Detail:(void(^)(BOOL success,NSString *bodyHTML))complete{
    
    dispatch_group_t group = dispatch_group_create();
    //    WEAKSELF
    //    for (FBSKeyNewsItem *news in data.items) {
    //        dispatch_group_enter(group);
    //        FBSGetRequest *request = [FBSGetRequest new];
    //        request.requestURI = info.link;
    //        [request startWithSuccess:^(YBNetworkResponse * _Nonnull response) {
    //            dispatch_group_enter(group);
    //            [weakSelf.dataDic setObject:response forKey:info.link];
    //        } failure:^(YBNetworkResponse * _Nonnull response) {
    //            dispatch_group_leave(group);
    //        }];
    //    }
    //    dispatch_group_notify(group, dispatch_get_main_queue(), ^{
    //        NSLog(@"%@",self.dataDic);
    //    });
    for (int i = 0; i < array.count; i++) {
        if ([array[i] isKindOfClass:[ImageModel class]]) {
            dispatch_group_enter(group);
            NSMutableDictionary *dic = [NSMutableDictionary dictionary];
            FBSUserData *user = [FBSUserData sharedData];
            [dic setObject:user.useId forKey:@"user_id"];
            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
            formatter.dateFormat = @"yyyy-MM-dd";
            NSString *plainText = [NSString stringWithFormat:@"%@%@",user.account,[formatter stringFromDate:[NSDate date]]];
            [dic setObject:[HLWJMD5 MD5ForLower32Bate:plainText] forKey:@"token"];
            
            FBSPostRequest *upload = [[FBSPostRequest alloc] init];
            upload.requestURI = @"user/uploadfile";
            upload.requestParameter = dic;
            ImageModel *model = array[i];
            upload.requestConstructingBody = ^(id<AFMultipartFormData> formData){
                NSDateFormatter *formatter=[[NSDateFormatter alloc]init];
                formatter.dateFormat=@"yyyyMMddHHmmss";
                NSString *str=[formatter stringFromDate:[NSDate date]];
                NSString *fileName=[NSString stringWithFormat:@"%@%d.jpg",str,i+1];
                UIImage *image = model.image;
                NSData *imageData = UIImageJPEGRepresentation(image, 0.28);
                //                [formData appendPartWithFileData:imageData name:[NSString stringWithFormat:@"image%d",i+1] fileName:fileName mimeType:@"image/jpeg"];
                [formData appendPartWithFileData:imageData name:@"image" fileName:fileName mimeType:@"image/jpeg"];
            };
            [upload startWithSuccess:^(YBNetworkResponse * _Nonnull response) {
                model.abbrUrl = response.responseObject[@"data"][@"image_url"];
                dispatch_group_leave(group);
            } failure:^(YBNetworkResponse * _Nonnull response) {
                dispatch_group_leave(group);
            }];
        }
    }
    dispatch_group_notify(group, dispatch_get_main_queue(), ^{
        [self sendArticle:array Detail:^(BOOL success, NSString *bodyHTML) {
             complete(YES, nil);
        }];
    });
}
- (void)sendArticle:(NSArray *)array Detail:(void(^)(BOOL success,NSString *bodyHTML))complete{
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    FBSUserData *user = [FBSUserData sharedData];
    [dic setObject:user.useId forKey:@"user_id"];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"yyyy-MM-dd";
    NSString *plainText = [NSString stringWithFormat:@"%@%@",user.account,[formatter stringFromDate:[NSDate date]]];
    [dic setObject:[HLWJMD5 MD5ForLower32Bate:plainText] forKey:@"token"];
    FBSOnlyOneInputItem *title1Item = [self.items firstObject];
    [dic setObject:title1Item.text forKey:@"title"];
    [dic setObject:@"2" forKey:@"open_comment"];
    //FBSActivityCatageryItemModel *mo =  self.model.data[self.tagsItem.index];
    NSMutableString *contentStr = [[NSMutableString alloc] init];
    for (int i = 0; i < array.count; i++) {
        if ([array[i] isKindOfClass:[ImageModel class]]) {
            ImageModel *model = array[i];
             NSString *imageStr = [NSString stringWithFormat:@"<img width=100%%  src=\"%@\" alt="" />",model.abbrUrl];
            if (i == 0) [contentStr appendString:imageStr];
            else [contentStr appendFormat:@"\n%@", imageStr];
        }else{
            if (i == 0) [contentStr appendString:array[i]];
            else [contentStr appendFormat:@"\n%@", array[i]];
        }
    }
    //NSArray *arrayId= @[mo.channel_id,mo.channel_catgory_id];
    [dic setObject:@"" forKey:@"channel_category"];
    [dic setObject:@"" forKey:@"value_field"];
    [dic setObject:@[contentStr] forKey:@"contents"];
    [dic setObject:@"" forKey:@"thumb"];
    self.sendPost.requestParameter = dic;
    [self.sendPost startWithSuccess:^(YBNetworkResponse * _Nonnull response) {
        
//        self.model = [FBSArticalModel mj_objectWithKeyValues:response.responseObject];
//        NSDictionary *parts = [response.responseObject valueForKeyPath:@"data"];
//        NSString *content = parts[@"describe"];
//        [self createItem];
        complete(YES, nil);
    } failure:^(YBNetworkResponse * _Nonnull response) {
        complete(NO, nil);
    }];
}
- (FBSPostRequest *)sendPost {
    if (!_sendPost) {
        _sendPost = [[FBSPostRequest alloc] init];
        _sendPost.requestURI = @"user/article/create";
    }
    return _sendPost;
}
#pragma mark - property

- (NSMutableArray *)contentArr {
    if (!_contentArr) {
        _contentArr  = [[NSMutableArray alloc] init];
    }
    return _contentArr;
}





- (FBSTagsItem *)tagsItem {
    if (!_tagsItem) {
        _tagsItem = [FBSTagsItem new];
        _tagsItem.cellHeight = [_tagsItem calculateCellHeight];
    }
    return _tagsItem;
}


@end
