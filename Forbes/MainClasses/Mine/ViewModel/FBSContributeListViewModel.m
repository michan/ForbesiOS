//
//  FBSContributeListViewModel.m
//  Forbes
//
//  Created by 周灿华 on 2019/8/10.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSContributeListViewModel.h"

@implementation FBSContributeListViewModel

- (void)createItem {

    NSMutableArray *array = [NSMutableArray array];
    for (NSInteger i = 0; i < self.model.data.items.count; i++) {
         FBSMyarticlesListArrayItemModel *model =  self.model.data.items[i];
        FBSContributeListItem *listItem = [FBSContributeListItem item];
        listItem.time = model.time;
        listItem.title = model.title;
        listItem.imgUrl = model.link;
        listItem.cellHeight = [listItem calculateCellHeight];
        
        [array addObject:listItem];
    }
    self.items = array;
    
    
}
- (void)createItemMore:(FBSMyarticlesListModel *)ListModel{
    
    
    for (NSInteger i = 0; i < ListModel.data.items.count; i++) {
        FBSMyarticlesListArrayItemModel *model =  self.model.data.items[i];
        FBSContributeListItem *listItem = [FBSContributeListItem item];
        listItem.time = model.time;
        listItem.title = model.title;
        listItem.imgUrl = model.link;
        listItem.cellHeight = [listItem calculateCellHeight];
        [self.items addObject:listItem];
    }
    
    
}
- (void)createArticleListL:(NSString *)type isMore:(BOOL)isMore complete:(void(^)(BOOL success ,NSUInteger count))complete{
    FBSGetRequest *request = [[FBSGetRequest alloc] init];
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    FBSUserData *user = [FBSUserData sharedData];
    request.requestURI = @"user/articles";
    [dic setObject:user.useId forKey:@"user_id"];
    [dic setObject:[NSString stringWithFormat:@"%ld",self.page] forKey:@"page"];
    [dic setObject:type forKey:@"status"];
    request.requestParameter = dic;
    [request startWithSuccess:^(YBNetworkResponse * _Nonnull response) {
        FBSMyarticlesListModel *model = [FBSMyarticlesListModel mj_objectWithKeyValues:response.responseObject];
        if (!isMore) {
             self.model = model;
            [self createItem];
        }else{
            [self.model.data.items addObjectsFromArray:model.data.items];
            [self createItemMore:model];
        }
        complete(YES,model.data.items.count);
    } failure:^(YBNetworkResponse * _Nonnull response) {
        complete(YES,self.model.data.items.count);
    }];
    
}
@end
