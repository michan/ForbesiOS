//
//  FBSMyarticlesListModel.h
//  Forbes
//
//  Created by 赵志辉 on 2019/10/8.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import "FBSBaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface FBSMyarticlesListArrayItemModel :NSObject
@property (nonatomic , copy) NSString              * id;
@property (nonatomic , copy) NSString              * title;
@property (nonatomic , copy) NSString              * user_id;
@property (nonatomic , copy) NSString              * select_count;
@property (nonatomic , copy) NSString              * time;
@property (nonatomic , copy) NSString              * status;
@property (nonatomic , copy) NSString              * file_id;
@property (nonatomic , copy) NSString              * Description;
@property (nonatomic , copy) NSString              * path;
@property (nonatomic , copy) NSString              * open_comment;
@property (nonatomic , copy) NSString              * created_at;
@property (nonatomic , copy) NSString              * updated_at;
@property (nonatomic , copy) NSString              * statusStr;
@property (nonatomic , copy) NSString              * link;

@end


@interface FBSMyarticlesListArrayModel :NSObject
@property (nonatomic , strong) NSMutableArray <FBSMyarticlesListArrayItemModel *>              * items;
@property (nonatomic , copy) NSString              * total;
@property (nonatomic , copy) NSString              * page;

@end


@interface FBSMyarticlesListModel :NSObject
@property (nonatomic , strong) FBSMyarticlesListArrayModel              * data;
@property (nonatomic , copy) NSString             *  code;

@end

NS_ASSUME_NONNULL_END
