//
//  FBSMyActivityDetailViewModel.m
//  Forbes
//
//  Created by 赵志辉 on 2019/9/29.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import "FBSMyActivityDetailViewModel.h"
#import "FBSPersonDetailItem.h"
#import "FBSActivityPayDetailItem.h"
#import "FBSMyActivitylDetailModel.h"
#import "FBSButtonCell.h"

@interface FBSMyActivityDetailViewModel()

@property(nonatomic ,strong)FBSMyActivitylDetailModel *model;

@end

@implementation FBSMyActivityDetailViewModel

- (void)createItem{
    
    
    FBSPersonDetailItem *personItem = [[FBSPersonDetailItem alloc] init];
    personItem.title = @"联系方式";
    personItem.detail = @"（用于接受电子票验证码）";
    personItem.cellHeight = WScale(148);

    personItem.name = [NSString stringWithFormat:@"姓名：%@",self.model.data.name];
    personItem.phone = [NSString stringWithFormat:@"手机：%@",self.model.data.ipone];
    personItem.email = [NSString stringWithFormat:@"邮箱：%@",self.model.data.email];
    
    personItem.bottomLineHidden = YES;
    
    [self.items addObject:personItem];
    
    
    FBSActivityPayDetailItem *item = [[FBSActivityPayDetailItem alloc] init];
    FBSMyActivitylDetailDataActivity_ticket *ticketItem = self.model.data.activity_ticket;
    FBSMyActivitylDetailDataActivity *activity = self.model.data.activity;

    item.title = @"交易信息";
    item.ticketPrice = ticketItem.price;
    item.timeAndAddress = [NSString stringWithFormat:@"活动时间：%@\n活动地址：%@",activity.activity_time,activity.activity_address];
    item.ticketType = ticketItem.type;
    item.examineVerify = ticketItem.remarks;
    item.totlePrice = ticketItem.price;
    item.cellHeight = WScale(311);
    item.titleDetail = activity.title;
    
    [self.items addObject:item];
    
    
    FBSButtonItem *itemBt = [[FBSButtonItem alloc] init];
    itemBt.bottomLineHidden = YES;
    itemBt.buttonText = @"下一步";
    itemBt.buttonTextColor =  Hexcolor(0xFFFFFF);
    itemBt.buttonBgColor = Hexcolor(0x8C734B);
    itemBt.cellHeight = WScale(120);
    [self.items addObject:itemBt];
}
- (void)createItem:(void(^)(BOOL success))complete{
    [self requestActivities:^(BOOL success) {
        [self createItem];
        complete(YES);
    }];
}
// 请求活动列表
- (void)requestActivities:(void(^)(BOOL success))complete{
    FBSGetRequest *request = [[FBSGetRequest alloc] init];
    request.cacheHandler.writeMode = YBNetworkCacheWriteModeMemoryAndDisk;
    request.cacheHandler.readMode = YBNetworkCacheReadModeAlsoNetwork;
    request.requestMethod = YBRequestMethodGET;
    request.requestURI = [NSString stringWithFormat:@"user/order/%@",self.activity_ticket_code];

    WEAKSELF;
    [request startWithSuccess:^(YBNetworkResponse * _Nonnull response) {
        weakSelf.model = [FBSMyActivitylDetailModel mj_objectWithKeyValues:response.responseObject];
        [self createItem];
        complete(YES);
    } failure:^(YBNetworkResponse * _Nonnull response) {
        
    }];
}
@end
