
//
//  FBSMineInfoViewModel.m
//  Forbes
//
//  Created by 周灿华 on 2019/8/8.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSMineInfoViewModel.h"

@implementation FBSMineInfoViewModel
- (void)uploadHeader:(NSArray *)array Detail:(void(^)(BOOL success,NSString *headerURl))complete{
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    FBSUserData *user = [FBSUserData sharedData];
    [dic setObject:user.useId forKey:@"user_id"];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"yyyy-MM-dd";
    NSString *plainText = [NSString stringWithFormat:@"%@%@",user.account,[formatter stringFromDate:[NSDate date]]];
    [dic setObject:[HLWJMD5 MD5ForLower32Bate:plainText] forKey:@"token"];
    
    FBSPostRequest *upload = [[FBSPostRequest alloc] init];
    upload.requestURI = @"user/uploadfile";
    upload.requestParameter = dic;
    upload.requestConstructingBody = ^(id<AFMultipartFormData> formData){
        NSDateFormatter *formatter=[[NSDateFormatter alloc]init];
        formatter.dateFormat=@"yyyyMMddHHmmss";
        NSString *str=[formatter stringFromDate:[NSDate date]];
        NSString *fileName=[NSString stringWithFormat:@"%@%d.jpg",str,1];
        UIImage *image = array[0];
        NSData *imageData = UIImageJPEGRepresentation(image, 0.28);
        //                [formData appendPartWithFileData:imageData name:[NSString stringWithFormat:@"image%d",i+1] fileName:fileName mimeType:@"image/jpeg"];
        [formData appendPartWithFileData:imageData name:@"image" fileName:fileName mimeType:@"image/jpeg"];
    };
    WEAKSELF;
    [upload startWithSuccess:^(YBNetworkResponse * _Nonnull response) {
        NSInteger code = [response.responseObject[@"code"] integerValue];
        if (code == 200) {
            NSString *headerImage = response.responseObject[@"data"][@"image_url"];
            weakSelf.avatarItem.imageUrl = headerImage;
            [[NSUserDefaults standardUserDefaults] setObject:headerImage forKey:@"headerImage"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            complete(YES,headerImage);
        }else{
            complete(NO,nil);
        }
    } failure:^(YBNetworkResponse * _Nonnull response) {
        complete(NO,nil);
    }];
}
- (void)makeItems {
    [self.items addObject:self.avatarItem];
    [self.items addObject:self.nicknameItem];
    [self.items addObject:self.phoneItem];
    [self.items addObject:self.passwordItem];

}

#pragma mark - property

- (FBSMineInfoItem *)avatarItem {
    if (!_avatarItem) {
        _avatarItem  = [FBSMineInfoItem item];
        _avatarItem.tag = AvatarTag;
        _avatarItem.title = @"我的头像";
        NSString *header = [[NSUserDefaults standardUserDefaults] objectForKey:@"headerImage"];
        _avatarItem.imageUrl = header.length > 0 ? header : @"default_avatar";
        _avatarItem.cellHeight = WScale(50);
    }
    return _avatarItem;
}

- (FBSMineInfoItem *)nicknameItem {
    if (!_nicknameItem) {
        _nicknameItem  = [FBSMineInfoItem item];
        _nicknameItem.title = @"昵称";
        _nicknameItem.tag = NickNameTag;
        _nicknameItem.detail = [FBSUserData sharedData].nickname;
        _nicknameItem.cellHeight = WScale(50);
    }
    return _nicknameItem;
}



- (FBSMineInfoItem *)phoneItem {
    if (!_phoneItem) {
        _phoneItem  = [FBSMineInfoItem item];
        _phoneItem.title = @"手机号码";
        _phoneItem.tag = PhoneTag;
        _phoneItem.detail = [NSString filterPhone:[FBSUserData sharedData].tel];
        _phoneItem.cellHeight = WScale(50);
    }
    return _phoneItem;
}



- (FBSMineInfoItem *)passwordItem {
    if (!_passwordItem) {
        _passwordItem  = [FBSMineInfoItem item];
        _passwordItem.title = @"修改密码";
        _passwordItem.tag = PasswordTag;
        _passwordItem.detail = @"";
        _passwordItem.cellHeight = WScale(50);
    }
    return _passwordItem;
}


@end
