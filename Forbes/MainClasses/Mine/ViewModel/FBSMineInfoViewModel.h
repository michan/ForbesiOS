//
//  FBSMineInfoViewModel.h
//  Forbes
//
//  Created by 周灿华 on 2019/8/8.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSTableViewModel.h"
#import "FBSMineInfoCell.h"
#import "FBSButtonCell.h"

#define AvatarTag   1001
#define NickNameTag 1002
#define PhoneTag    1003
#define PasswordTag 1004

@interface FBSMineInfoViewModel : FBSTableViewModel
@property (nonatomic, strong) FBSMineInfoItem *avatarItem;
@property (nonatomic, strong) FBSMineInfoItem *nicknameItem;
@property (nonatomic, strong) FBSMineInfoItem *phoneItem;
@property (nonatomic, strong) FBSMineInfoItem *passwordItem;
- (void)uploadHeader:(NSArray *)array Detail:(void(^)(BOOL success,NSString *headerURl))complete;
@end

