//
//  FBSMyActivitylDetailModel.m
//  Forbes
//
//  Created by 赵志辉 on 2019/9/29.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import "FBSMyActivitylDetailModel.h"

@implementation FBSMyActivitylDetailDataActivity

@end


@implementation FBSMyActivitylDetailDataActivity_ticket
@end


@implementation FBSMyActivitylDetailDataActivity_date
@end


@implementation FBSMyActivitylDetailDataModel

+ (NSDictionary *)mj_objectClassInArray {
    return @{@"activity" : @"FBSMyActivitylDetailDataActivity",@"activity_ticket" : @"FBSMyActivitylDetailDataActivity_ticket",@"activity_date" : @"FBSMyActivitylDetailDataActivity_date"};
}
@end


@implementation FBSMyActivitylDetailModel
@end
