//
//  FBSMineViewModel.h
//  Forbes
//
//  Created by 周灿华 on 2019/7/27.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSTableViewModel.h"
#import "FBSMineHeadCell.h"
#import "FBSMineOperateCell.h"
#import "FBSMineEntryCell.h"
#import "FBSSpaceCell.h"
#import "FBSButtonCell.h"


#define FollowEntryTag      1000  //我的关注
#define ActivityEntryTag    1001  //我的活动
#define ContributeEntryTag  1002  //投稿管理
#define InvoiceEntryTag     1003  //发票申请
#define FeedbackEntryTag    1004  //意见反馈
#define SettingEntryTag     1005  //设置


@interface FBSMineViewModel : FBSTableViewModel

@property (nonatomic, strong) FBSMineHeadItem *headItem;

@end

