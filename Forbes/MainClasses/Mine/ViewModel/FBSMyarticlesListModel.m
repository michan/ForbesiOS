//
//  FBSMyarticlesListModel.m
//  Forbes
//
//  Created by 赵志辉 on 2019/10/8.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import "FBSMyarticlesListModel.h"


@implementation FBSMyarticlesListArrayItemModel

+ (NSDictionary *)mj_replacedKeyFromPropertyName {
    return @{@"Description" : @"description"};
}

@end

@implementation FBSMyarticlesListArrayModel

+ (NSDictionary *)mj_objectClassInArray {
    return @{@"items" : @"FBSMyarticlesListArrayItemModel"};
}

@end
@implementation FBSMyarticlesListModel

@end
