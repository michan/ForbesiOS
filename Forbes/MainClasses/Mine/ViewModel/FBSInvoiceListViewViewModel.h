//
//  FBSInvoiceListViewViewModel.h
//  Forbes
//
//  Created by 周灿华 on 2019/8/17.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSTableViewModel.h"
#import "FBSInvoiceListCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface FBSInvoiceListViewViewModel : FBSTableViewModel

@property (nonatomic, strong) NSMutableArray *selectedItems;

@property (nonatomic, assign) NSInteger type;

- (instancetype)initWithType:(NSInteger)type;
@end

NS_ASSUME_NONNULL_END
