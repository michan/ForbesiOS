//
//  FBSInvoiceListViewViewModel.m
//  Forbes
//
//  Created by 周灿华 on 2019/8/17.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSInvoiceListViewViewModel.h"

@implementation FBSInvoiceListViewViewModel

- (instancetype)initWithType:(NSInteger)type {
    self = [super init];
    if (self) {
        self.type = type;
    }
    return self;
}

- (void)makeItems {
    [self.items removeAllObjects];
    [self.selectedItems removeAllObjects];
    
    for (NSInteger i= 0; i< 10; i++) {
        FBSInvoiceListItem *item = [FBSInvoiceListItem item];
        item.isEdit = NO;
        item.isSelected = NO;
        item.isDetail = self.type == 1;
        item.order = @"342433444II4I4I";
        item.title = @"我是一张发票";
        if (i % 2) {
            item.title = @"我是一张很长的发票和名称激昂的发票界面处的发 发发发发快递方式开发撒浪费";
        }
        item.time = @"2019.09.30 13:00";
        item.cellHeight = WScale(160);
        item.bottomLineHidden = YES;
        [self.items addObject:item];
    }
        
}

#pragma mark - property

- (NSMutableArray *)selectedItems {
    if (!_selectedItems) {
        _selectedItems = [[NSMutableArray alloc] init];
    }
    return _selectedItems;
}



@end
