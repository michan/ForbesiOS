//
//  FBSContributeSuccessViewModel.h
//  Forbes
//
//  Created by 周灿华 on 2019/8/11.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FBSSuccessCell.h"
#import "FBSButtonCell.h"

@interface FBSContributeSuccessViewModel : FBSTableViewModel

- (instancetype)initWithimage:(NSString *)imageURL success:(NSString *)successsStr content:(NSString *)content button:(NSString *)buttonStr;
@end

