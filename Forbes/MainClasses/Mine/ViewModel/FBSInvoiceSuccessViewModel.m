//
//  FBSInvoiceSuccessViewModel.m
//  Forbes
//
//  Created by 周灿华 on 2019/8/18.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSInvoiceSuccessViewModel.h"

@implementation FBSInvoiceSuccessViewModel

- (void)makeItems {
    FBSSuccessItem *successItem = [FBSSuccessItem item];
    successItem.icon = @"invoice_success";
    successItem.title = @"提交成功！";
    successItem.tip = @"感谢您对福布斯的支持，我们将会尽快确认您的信息。";
    successItem.cellHeight = WScale(230);
    [self.items addObject:successItem];
    
    
    FBSButtonItem *finishItem = [FBSButtonItem item];
    finishItem.buttonText = @"完成";
    finishItem.buttonTextColor = [UIColor whiteColor];
    finishItem.buttonBgColor = MainThemeColor;
    finishItem.cellHeight = kScreenHeight - successItem.cellHeight - kNavTopMargin;
    [self.items addObject:finishItem];
    
}



@end
