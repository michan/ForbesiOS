//
//  FBSRecordModel.m
//  Forbes
//
//  Created by 赵志辉 on 2019/11/26.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import "FBSRecordModel.h"


@implementation  FBSRecordModelItem

+ (NSDictionary *)mj_replacedKeyFromPropertyName {
    return @{@"Description" : @"description"};
}

@end
@implementation  FBSRecordModelData

+ (NSDictionary *)mj_objectClassInArray {
    return @{@"items" : @"FBSRecordModelItem"};
}

@end

@implementation  FBSRecordModel


@end
