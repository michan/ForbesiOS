//
//  FBSRecordModel.h
//  Forbes
//
//  Created by 赵志辉 on 2019/11/26.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import "FBSBaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface FBSRecordModelItem :NSObject

@property (nonatomic , copy) NSString              * id;
@property (nonatomic , copy) NSString              * object_id;
@property (nonatomic , copy) NSString              * object_type;
@property (nonatomic , copy) NSString              * object_title;
@property (nonatomic , copy) NSString              * like_count;
@property (nonatomic , copy) NSString              * share_count;
@property (nonatomic , copy) NSString              * view_count;
@property (nonatomic , copy) NSString              * created_at;
@property (nonatomic , copy) NSString              * updated_at;
@property (nonatomic , copy) NSString              * title;
@property (nonatomic , copy) NSString              * Description;
@property (nonatomic , copy) NSString              * path;
@property (nonatomic , copy) NSString              * channel_type;
@property (nonatomic , copy) NSString              * file_url;
@property (nonatomic , copy) NSString              * account;
@property (nonatomic , copy) NSString              * nickname;
@property (nonatomic , copy) NSString              * image_url;
@property (nonatomic , copy) NSString              * comment_count;
@property (nonatomic, copy) NSString *has_like;
@property (nonatomic, copy) NSString *has_fav;



@end


@interface FBSRecordModelData :NSObject
@property (nonatomic , copy) NSString              *limit;
@property (nonatomic , copy) NSString              *page;
@property (nonatomic , copy) NSString              *total;
@property (nonatomic , strong) NSArray <FBSRecordModelItem *>              * items;

@end


@interface FBSRecordModel :FBSBaseModel

@property (nonatomic , strong) FBSRecordModelData  * data;
@end

NS_ASSUME_NONNULL_END
