//
//  DynamicsModel.h
//  LooyuEasyBuy
//
//  Created by Andy on 15/11/20.
//  Copyright © 2015年 Doyoo. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "DynamicsModel.h"

@interface DynamicsModel : NSObject

@property (nonatomic , copy) NSString              * id;
@property (nonatomic , copy) NSString              * object_id;
@property (nonatomic , copy) NSString              * object_type;
@property (nonatomic , copy) NSString              * object_title;
@property (nonatomic , copy) NSString              * like_count;
@property (nonatomic , copy) NSString              * share_count;
@property (nonatomic , copy) NSString              * view_count;
@property (nonatomic , copy) NSString              * created_at;
@property (nonatomic , copy) NSString              * updated_at;
@property (nonatomic , copy) NSString              * title;
@property (nonatomic , copy) NSString              * Description;
@property (nonatomic , copy) NSString              * path;
@property (nonatomic , copy) NSString              * channel_type;
@property (nonatomic , copy) NSString              * file_url;
@property (nonatomic , copy) NSString              * account;
@property (nonatomic , copy) NSString              * nickname;
@property (nonatomic , copy) NSString              * image_url;
@property (nonatomic , copy) NSString              * comment_count;
@property (nonatomic , copy) NSArray              * image_urlS;
@property (nonatomic, copy) NSString *has_like;
@property (nonatomic, copy) NSString *has_fav;



@end

