//
//  NewDynamicsLayout.m
//  LooyuEasyBuy
//
//  Created by Andy on 2017/9/27.
//  Copyright © 2017年 Doyoo. All rights reserved.
//

#import "NewDynamicsLayout.h"
#import "SDWeiXinPhotoContainerView.h"
#import <YYKit.h>

@implementation NewDynamicsLayout

- (instancetype)initWithModel:(DynamicsModel *)model type:(CellType)type
{
    self = [super init];
    if (self) {
        _model = model;
        _type = type;
        [self resetLayout];
    }
    return self;
}

- (void)resetLayout
{
    _height = 0;
    _thumbCommentHeight = 0;
    
    [self.commentLayoutArr removeAllObjects];
    
    _height += kDynamicsNormalPadding;
    _height += kDynamicsPortraitWidthAndHeight;
    _height += kDynamicsNameDetailPadding;
    
    [self layoutDetail];
    _height += _detailLayout.textBoundingSize.height;

    if (self.type == cellTypeOneImage) {
        self.type = cellTypeOneImage;
        [self layoutOneImageView];
        _height += kDynamicsNameDetailPadding;
        _height += kDynamicsOneImageHeight;
        _height -= _detailLayout.textBoundingSize.height;
        _height -= kDynamicsNameDetailPadding;
    }else if (self.type == cellTypeImages) {
        self.type = cellTypeImages;
        [self layoutPicture];
        _height += kDynamicsNameDetailPadding;
        _height += _photoContainerSize.height;
    }

    if (self.type == cellTypePinglun) {//头条类型
        [self layoutGrayDetailView];
        _height += kDynamicsNameDetailPadding;
        _height += kDynamicsGrayBgHeight;
        _height += kDynamicsNameDetailPadding;
        _height += kDynamicsNameHeight;
        self.type = cellTypePinglun;
    }
    _height += kDynamicsNameDetailPadding;
    _height += kDynamicsButtonHeight;
    _height += kDynamicsNormalPadding;
}

- (void)layoutDetail
{
    _detailLayout = nil;
    if(_model.Description.length <= 0){
        _model.Description = @"";
    }
    NSMutableAttributedString * text = [[NSMutableAttributedString alloc] initWithString:_model.Description];
    text.font = [UIFont systemFontOfSize:14];
    text.lineSpacing = kDynamicsLineSpacing;
    
    NSDataDetector *detector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingTypePhoneNumber | NSTextCheckingTypeLink error:nil];
    
    WEAKSELF
    [detector enumerateMatchesInString:_model.Description
                               options:kNilOptions
                                 range:text.rangeOfAll
                            usingBlock:^(NSTextCheckingResult *result, NSMatchingFlags flags, BOOL *stop) {
                                
                                if (result.URL) {
                                    YYTextHighlight * highLight = [YYTextHighlight new];
                                    [text setColor:[UIColor colorWithRed:69/255.0 green:88/255.0 blue:133/255.0 alpha:1] range:result.range];
                                    highLight.tapAction = ^(UIView * _Nonnull containerView, NSAttributedString * _Nonnull text, NSRange range, CGRect rect) {
                                        if (weakSelf.clickUrlBlock) {
                                            weakSelf.clickUrlBlock([text.string substringWithRange:range]);
                                        }
                                    };
                                    [text setTextHighlight:highLight range:result.range];
                                }
                                if (result.phoneNumber) {
                                    YYTextHighlight * highLight = [YYTextHighlight new];
                                    [text setColor:[UIColor colorWithRed:69/255.0 green:88/255.0 blue:133/255.0 alpha:1] range:result.range];
                                    highLight.tapAction = ^(UIView * _Nonnull containerView, NSAttributedString * _Nonnull text, NSRange range, CGRect rect) {
                                        if (weakSelf.clickPhoneNumBlock) {
                                            weakSelf.clickPhoneNumBlock([text.string substringWithRange:range]);
                                        }
                                    };
                                    [text setTextHighlight:highLight range:result.range];
                                }
                            }];
    YYTextContainer * container = [YYTextContainer containerWithSize:CGSizeMake(SCREENWIDTH - 2 *kDynamicsNormalPadding, CGFLOAT_MAX)];

    container.truncationType = YYTextTruncationTypeEnd;
    
    _detailLayout = [YYTextLayout layoutWithContainer:container text:text];
    
}
- (void)layoutPicture
{
    self.photoContainerSize = CGSizeZero;
    self.photoContainerSize = [SDWeiXinPhotoContainerView getContainerSizeWithPicPathStringsArray:_model.image_urlS];
}
- (void)layoutOneImageView
{
    if(_model.title.length <= 0){
        _model.title = @"";
    }
    NSMutableAttributedString * text = [[NSMutableAttributedString alloc] initWithString:_model.title];
    text.font = [UIFont systemFontOfSize:14];
    text.lineSpacing = 5;
    YYTextContainer * container = [YYTextContainer containerWithSize:CGSizeMake(SCREENWIDTH - kDynamicsNormalPadding - kDynamicsPortraitNamePadding - kDynamicsGrayPicPadding - kDynamicsOneImageWight - kDynamicsNameDetailPadding * 2 - kDynamicsNormalPadding,kDynamicsGrayBgHeight - kDynamicsGrayPicPadding * 2)];
    container.truncationType = YYTextTruncationTypeEnd;
    
    _oneImageLayout = [YYTextLayout layoutWithContainer:container text:text];
}
- (void)layoutGrayDetailView
{
    if(_model.title.length <= 0){
        _model.title = @"";
    }
    NSMutableAttributedString * text = [[NSMutableAttributedString alloc] initWithString:_model.title];
    text.font = [UIFont systemFontOfSize:14];
    text.lineSpacing = 3;
    
    YYTextContainer * container = [YYTextContainer containerWithSize:CGSizeMake(SCREENWIDTH - kDynamicsNormalPadding - kDynamicsPortraitNamePadding - kDynamicsGrayPicPadding - kDynamicsGrayPicHeight - kDynamicsNameDetailPadding*2 - kDynamicsNormalPadding,kDynamicsGrayBgHeight - kDynamicsGrayPicPadding*2)];
    container.truncationType = YYTextTruncationTypeEnd;
    
    _dspLayout = [YYTextLayout layoutWithContainer:container text:text];
}
@end
