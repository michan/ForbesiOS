//
//  NewDynamicsLayout.h
//  LooyuEasyBuy
//
//  Created by Andy on 2017/9/27.
//  Copyright © 2017年 Doyoo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DynamicsModel.h"

#define kDynamicsNormalPadding 8
#define kDynamicsPortraitWidthAndHeight 45
#define kDynamicsPortraitNamePadding 10
#define kDynamicsNameDetailPadding 8
#define kDynamicsNameHeight 17
#define kDynamicsMoreLessButtonHeight 30
#define kDynamicsSpreadButtonHeight 20
#define kDynamicsGrayBgHeight 66
#define kDynamicsGrayPicHeight 46
#define kDynamicsGrayPicPadding 3
#define kDynamicsThumbTopPadding 10
#define kDynamicsDataAndDetailHeight 15
#define kDynamicsGrayPicPaddingRight 12
#define kDynamicsGrayPicPaddingTop 11
#define kDynamicsButtonHeight 35
#define kDynamicsOneImageHeight 74
#define kDynamicsOneImageWight 117
#define kDynamicsLineSpacing 5

typedef void(^ClickUserBlock)(NSString * userID);
typedef void(^ClickUrlBlock)(NSString * url);
typedef void(^ClickPhoneNumBlock)(NSString * phoneNum);

typedef enum : NSUInteger {
    cellTypePinglun,
    cellTypeOneImage,
    cellTypeNoneImage,
    cellTypeImages
} CellType;

@interface NewDynamicsLayout : FBSItem

@property(nonatomic,assign)CellType type;
@property(nonatomic,strong)DynamicsModel * model;
@property(nonatomic,strong)YYTextLayout * detailLayout;
@property(nonatomic,assign)CGSize photoContainerSize;
@property(nonatomic,strong)YYTextLayout * dspLayout;
@property(nonatomic,strong)YYTextLayout * oneImageLayout;
@property(nonatomic,strong)YYTextLayout * thumbLayout;
@property(nonatomic,strong)NSMutableArray * commentLayoutArr;
@property(nonatomic,assign)CGFloat thumbHeight;
@property(nonatomic,assign)CGFloat commentHeight;
@property(nonatomic,assign)CGFloat thumbCommentHeight;
@property(nonatomic,copy)ClickUserBlock clickUserBlock;
@property(nonatomic,copy)ClickUrlBlock clickUrlBlock;
@property(nonatomic,copy)ClickPhoneNumBlock clickPhoneNumBlock;

@property(nonatomic,assign)CGFloat height;

- (instancetype)initWithModel:(DynamicsModel *)model type:(CellType)type;
- (void)resetLayout;

@end
