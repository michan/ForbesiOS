//
//  NewDynamicsTableViewCell.m
//  LooyuEasyBuy
//
//  Created by Andy on 2017/9/27.
//  Copyright © 2017年 Doyoo. All rights reserved.
//

#import "NewDynamicsTableViewCell.h"

@implementation  NewDynamicsPinglunView

-(instancetype)initWithFrame:(CGRect)frame
{
    if (frame.size.width == 0 && frame.size.height == 0) {
        frame.size.width = SCREENWIDTH - kDynamicsNormalPadding * 2;
        frame.size.height = kDynamicsButtonHeight;
    }
    self = [super initWithFrame:frame];
    if (self) {
        [self setup];
    }
    return self;
}
- (void)setup
{
    [self addSubview:self.likeBt];
    [self addSubview:self.sayBt];
    [self addSubview:self.shareBt];
    
    [self layout];
}
- (void)layout
{
    _likeBt.left = 0;
    _likeBt.top = 0;
    _likeBt.width = (SCREENWIDTH - 2 * kDynamicsNormalPadding) / 3;
    _likeBt.height = kDynamicsButtonHeight;
    [_likeBt setImage:[UIImage imageNamed:@"video_praise1"] forState:UIControlStateNormal];
    [_likeBt setImage:[UIImage imageNamed:@"video_praise2"] forState:UIControlStateSelected];
    [_likeBt setTitleColor:RGBA_COLOR(102, 102, 102, 1) forState:UIControlStateNormal];
    
    _sayBt.left = _likeBt.right;
    _sayBt.top = 0;
    _sayBt.width = (SCREENWIDTH - 2 * kDynamicsNormalPadding) / 3;
    _sayBt.height = kDynamicsButtonHeight;
    [_sayBt setTitleColor:RGBA_COLOR(102, 102, 102, 1) forState:UIControlStateNormal];
    [_sayBt setImage:[UIImage imageNamed:@"mine_comment"] forState:UIControlStateNormal];
    
    
    _shareBt.left = _sayBt.right;
    _shareBt.top = 0;
    _shareBt.width = (SCREENWIDTH - 2 * kDynamicsNormalPadding) / 3;
    _shareBt.height = kDynamicsButtonHeight;
    [_shareBt setTitleColor:RGBA_COLOR(102, 102, 102, 1) forState:UIControlStateNormal];
    [_shareBt setTintColor:RGBA_COLOR(102, 102, 102, 1)];
    [_shareBt setImage:[UIImage imageNamed:@"share"] forState:UIControlStateNormal];

    
    _likeBt.titleLabel.font = [UIFont systemFontOfSize:13];
    _sayBt.titleLabel.font = [UIFont systemFontOfSize:13];
    _shareBt.titleLabel.font = [UIFont systemFontOfSize:13];



}

-(UIButton *)likeBt
{
    if (!_likeBt) {
        _likeBt = [UIButton buttonWithType:UIButtonTypeCustom];
        _likeBt.backgroundColor = [UIColor whiteColor];
        WS(weakSelf);
        [_likeBt bk_addEventHandler:^(id sender) {
            if (weakSelf.cell.delegate != nil && [weakSelf.cell.delegate respondsToSelector:@selector(DidClickBottemViewInDynamicsCell:tag:)]) {
                [weakSelf.cell.delegate DidClickBottemViewInDynamicsCell:weakSelf.cell tag:0];
            }
        } forControlEvents:UIControlEventTouchUpInside];
    }
    return _likeBt;
}
-(UIButton *)sayBt
{
    if (!_sayBt) {
        _sayBt = [UIButton buttonWithType:UIButtonTypeCustom];
        _sayBt.backgroundColor = [UIColor whiteColor];
        WS(weakSelf);
        [_sayBt bk_addEventHandler:^(id sender) {
            if (weakSelf.cell.delegate != nil && [weakSelf.cell.delegate respondsToSelector:@selector(DidClickBottemViewInDynamicsCell:tag:)]) {
                [weakSelf.cell.delegate DidClickBottemViewInDynamicsCell:weakSelf.cell tag:1];
            }
        } forControlEvents:UIControlEventTouchUpInside];
    }
    return _sayBt;
}
-(UIButton *)shareBt
{
    if (!_shareBt) {
        _shareBt = [UIButton buttonWithType:UIButtonTypeCustom];
        _shareBt.backgroundColor = [UIColor whiteColor];
        WS(weakSelf);
        [_shareBt bk_addEventHandler:^(id sender) {
            if (weakSelf.cell.delegate != nil && [weakSelf.cell.delegate respondsToSelector:@selector(DidClickBottemViewInDynamicsCell:tag:)]) {
                [weakSelf.cell.delegate DidClickBottemViewInDynamicsCell:weakSelf.cell tag:2];
            }
        } forControlEvents:UIControlEventTouchUpInside];
    }
    return _shareBt;
}
@end

@implementation NewDynamicsGrayView

-(instancetype)initWithFrame:(CGRect)frame
{
    if (frame.size.width == 0 && frame.size.height == 0) {
        frame.size.width = SCREENWIDTH - kDynamicsNormalPadding * 2;
        frame.size.height = kDynamicsGrayBgHeight;
    }
    self = [super initWithFrame:frame];
    if (self) {
        [self setup];
    }
    return self;
}
- (void)setup
{
    [self addSubview:self.grayBtn];
    [self addSubview:self.thumbImg];
    [self addSubview:self.dspLabel];
    
    [self layout];
}
- (void)layout
{
    _grayBtn.frame = self.frame;
    _thumbImg.left = self.right - kDynamicsGrayPicPaddingRight - kDynamicsGrayPicHeight;
    _thumbImg.top = kDynamicsGrayPicPaddingTop;
    _thumbImg.width = kDynamicsGrayPicHeight;
    _thumbImg.height = kDynamicsGrayPicHeight;
    
    _dspLabel.left = kDynamicsNameDetailPadding;
    _dspLabel.width = self.right - kDynamicsGrayPicPaddingRight - kDynamicsGrayPicHeight - 2 * kDynamicsNameDetailPadding;
}

-(UIButton *)grayBtn
{
    if (!_grayBtn) {
        _grayBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _grayBtn.backgroundColor = RGBA_COLOR(240, 240, 242, 1);
        WS(weakSelf);
        [_grayBtn bk_addEventHandler:^(id sender) {
            if (weakSelf.cell.delegate != nil && [weakSelf.cell.delegate respondsToSelector:@selector(DidClickGrayViewInDynamicsCell:)]) {
                [weakSelf.cell.delegate DidClickGrayViewInDynamicsCell:weakSelf.cell];
            }
        } forControlEvents:UIControlEventTouchUpInside];
    }
    return _grayBtn;
}
-(UIImageView *)thumbImg
{
    if (!_thumbImg) {
        _thumbImg = [UIImageView new];
        _thumbImg.userInteractionEnabled = NO;
        _thumbImg.backgroundColor = [UIColor grayColor];
    }
    return _thumbImg;
}
-(YYLabel *)dspLabel
{
    if (!_dspLabel) {
        _dspLabel = [YYLabel new];
        _dspLabel.userInteractionEnabled = NO;
    }
    return _dspLabel;
}

@end

@implementation NewDynamicsOneImage

-(instancetype)initWithFrame:(CGRect)frame
{
    if (frame.size.width == 0 && frame.size.height == 0) {
        frame.size.width = SCREENWIDTH - kDynamicsNormalPadding * 2;
        frame.size.height = kDynamicsOneImageHeight;
    }
    self = [super initWithFrame:frame];
    if (self) {
        [self setup];
    }
    return self;
}
- (void)setup
{
    [self addSubview:self.grayBtn];
    [self addSubview:self.thumbImg];
    [self addSubview:self.dspLabel];
    
    [self layout];
}
- (void)layout
{
    _grayBtn.frame = self.frame;
    _thumbImg.left = self.right - kDynamicsGrayPicPaddingRight -kDynamicsOneImageWight;
    _thumbImg.top = 0;
    _thumbImg.width = kDynamicsOneImageWight;
    _thumbImg.height = kDynamicsOneImageHeight;
    
    _dspLabel.left = 0;
    _dspLabel.width = self.right - kDynamicsGrayPicPaddingRight - kDynamicsOneImageWight - 2 *kDynamicsNameDetailPadding;
    _dspLabel.top = 0;
    _dspLabel.height = kDynamicsOneImageHeight;
    _dspLabel.numberOfLines= 0;
    _dspLabel.textVerticalAlignment =  YYTextVerticalAlignmentTop;
}

-(UIButton *)grayBtn
{
    if (!_grayBtn) {
        _grayBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        WS(weakSelf);
        [_grayBtn bk_addEventHandler:^(id sender) {
            if (weakSelf.cell.delegate != nil && [weakSelf.cell.delegate respondsToSelector:@selector(DidClickGrayViewInDynamicsCell:)]) {
                [weakSelf.cell.delegate DidClickGrayViewInDynamicsCell:weakSelf.cell];
            }
        } forControlEvents:UIControlEventTouchUpInside];
    }
    return _grayBtn;
}
-(UIImageView *)thumbImg
{
    if (!_thumbImg) {
        _thumbImg = [UIImageView new];
        _thumbImg.userInteractionEnabled = NO;
        _thumbImg.backgroundColor = [UIColor grayColor];
    }
    return _thumbImg;
}
-(YYLabel *)dspLabel
{
    if (!_dspLabel) {
        _dspLabel = [YYLabel new];
        _dspLabel.userInteractionEnabled = NO;
    }
    return _dspLabel;
}

@end


@implementation NewDynamicsTableViewCell

@dynamic delegate;

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setup];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return self;
}
- (void)setup
{
    [self.contentView addSubview:self.portrait];
    [self.contentView addSubview:self.nameLabel];
    [self.contentView addSubview:self.content];
    [self.contentView addSubview:self.detailLabel];
    [self.contentView addSubview:self.yueduLabel];
    [self.contentView addSubview:self.picContainerView];
    [self.contentView addSubview:self.grayView];
    [self.contentView addSubview:self.oneImageView];
    [self.contentView addSubview:self.dateLabel];
    [self.contentView addSubview:self.deleteBtn];
    [self.contentView addSubview:self.menuBtn];
    [self.contentView addSubview:self.dividingLine];
    [self.contentView addSubview:self.likeAndSayAndShare];
    [self.contentView addSubview:self.lineLabel];
    self.lineLabel.backgroundColor = RGBA_COLOR(243,247,248,1);

}
- (void)setItem:(FBSItem *)item
{
    [super setItem:item];
    if ([item isKindOfClass:[NewDynamicsLayout class]]) {
        [self setLayout:(NewDynamicsLayout *)item];
    }
}

-(void)setLayout:(NewDynamicsLayout *)layout
{
    UIView * lastView;
    _layout = layout;
    DynamicsModel * model = layout.model;
    
    //头像
    _portrait.left = kDynamicsNormalPadding;
    _portrait.top = kDynamicsNormalPadding;
    _portrait.size = CGSizeMake(kDynamicsPortraitWidthAndHeight, kDynamicsPortraitWidthAndHeight);
    _portrait.layer.masksToBounds = YES;
    _portrait.layer.cornerRadius = kDynamicsPortraitWidthAndHeight/ 2;
    [_portrait sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",model.image_url]]];
    
    //昵称
    _nameLabel.text = model.nickname;
    _nameLabel.top = kDynamicsNormalPadding;
    _nameLabel.left = _portrait.right + kDynamicsPortraitNamePadding;
    CGSize nameSize = [_nameLabel sizeThatFits:CGSizeZero];
    _nameLabel.width = nameSize.width;
    _nameLabel.height = kDynamicsNameHeight;
    
    // 时间
    _content.text = [NSString getTimestamp:model.updated_at formatter:@"MM-dd"];
    _content.left = _portrait.right + kDynamicsPortraitNamePadding;
    _content.top = _nameLabel.bottom + kDynamicsNameDetailPadding;
    _content.width = SCREENWIDTH - kDynamicsNormalPadding * 2 - kDynamicsPortraitNamePadding - kDynamicsPortraitWidthAndHeight;
    _content.height = kDynamicsDataAndDetailHeight;
    
    // 详情或者标题
    _detailLabel.hidden = NO;
    _detailLabel.left = kDynamicsNormalPadding;
    _detailLabel.top = _portrait.bottom + kDynamicsNameDetailPadding;
    _detailLabel.width = SCREENWIDTH - kDynamicsNormalPadding * 2;
    _detailLabel.height = layout.detailLayout.textBoundingSize.height;
    _detailLabel.textLayout = layout.detailLayout;
    lastView = _detailLabel;
    //图片集
    if (layout.type == cellTypeOneImage) {
        
            lastView = _portrait;
            _detailLabel.hidden = YES;
            _picContainerView.hidden = YES;
            _oneImageView.hidden = NO;
            _oneImageView.left = kDynamicsNormalPadding;
            _oneImageView.top = lastView.bottom + kDynamicsNameDetailPadding;
            _oneImageView.width = SCREENWIDTH - kDynamicsNormalPadding * 2;
            _oneImageView.height = kDynamicsOneImageHeight;
            [_oneImageView.thumbImg sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",model.file_url]]];
            _oneImageView.dspLabel.text = model.title;
            lastView = _oneImageView;
    }else if (layout.type == cellTypeImages) {
            // 详情或者标题
            _detailLabel.left = kDynamicsNormalPadding;
            _detailLabel.top = _portrait.bottom + kDynamicsNameDetailPadding;
            _detailLabel.width = SCREENWIDTH - kDynamicsNormalPadding * 2;
            _detailLabel.height = layout.detailLayout.textBoundingSize.height;
            _detailLabel.textLayout = layout.detailLayout;
            lastView = _detailLabel;
            _picContainerView.hidden = NO;
            _oneImageView.hidden = YES;
            _picContainerView.left = kDynamicsNormalPadding;
            _picContainerView.top = lastView.bottom + kDynamicsNameDetailPadding;
            _picContainerView.width = layout.photoContainerSize.width;
            _picContainerView.height = layout.photoContainerSize.height;
            _picContainerView.picPathStringsArray = model.image_urlS;
            lastView = _picContainerView;
    }else{
        _picContainerView.hidden = YES;
        _oneImageView.hidden = YES;
    }
    //头条
    if (layout.type  == cellTypePinglun) {
        _grayView.hidden = NO;
        _yueduLabel.hidden = NO;
        _grayView.left = kDynamicsNormalPadding;
        _grayView.top = lastView.bottom + kDynamicsNameDetailPadding;
        _grayView.width = SCREENWIDTH - kDynamicsNormalPadding * 2;;
        _grayView.height = kDynamicsGrayBgHeight;
        
        [_grayView.thumbImg sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",model.file_url]]];
        _grayView.dspLabel.height = layout.dspLayout.textBoundingSize.height;
        _grayView.dspLabel.centerY = _grayView.thumbImg.centerY;
        _grayView.dspLabel.textLayout = layout.dspLayout;
        
        lastView = _grayView;
        
        _yueduLabel.text = [NSString stringWithFormat:@"%@阅读",model.comment_count];
        _yueduLabel.left = kDynamicsNormalPadding;
        _yueduLabel.top = lastView.bottom + kDynamicsNameDetailPadding;
        _yueduLabel.width = SCREENWIDTH;
        _yueduLabel.height = kDynamicsNameHeight;
        
        lastView = _yueduLabel;
    }else{
        _grayView.hidden = YES;
        _yueduLabel.hidden = YES;

    }
    _likeAndSayAndShare.left = kDynamicsNormalPadding;
    _likeAndSayAndShare.top = lastView.bottom + kDynamicsNameDetailPadding;
    _likeAndSayAndShare.width = SCREENWIDTH - kDynamicsNormalPadding * 2;;
    _likeAndSayAndShare.height = kDynamicsButtonHeight;
    [_likeAndSayAndShare.likeBt setTitle:model.like_count forState:UIControlStateNormal];
    if ([model.has_like isEqualToString:@"Y"]) {
        _likeAndSayAndShare.likeBt.selected = YES;
    }else{
        _likeAndSayAndShare.likeBt.selected = NO;

    }
    lastView = _likeAndSayAndShare;
    
    _lineLabel.left = 0;
    _lineLabel.top = lastView.bottom;
    _lineLabel.width = SCREENWIDTH;
    _lineLabel.height = kDynamicsNormalPadding;
}
#pragma mark - getter
-(UIImageView *)portrait
{
    if(!_portrait){
        _portrait = [UIImageView new];
        _portrait.userInteractionEnabled = YES;
        _portrait.backgroundColor = [UIColor grayColor];
        WS(weakSelf);
        UITapGestureRecognizer * tapGR = [[UITapGestureRecognizer alloc] bk_initWithHandler:^(UIGestureRecognizer *sender, UIGestureRecognizerState state, CGPoint location) {
            if (weakSelf.delegate && [weakSelf.delegate respondsToSelector:@selector(DynamicsCell:didClickUser:)]) {
                [weakSelf.delegate DynamicsCell:weakSelf didClickUser:weakSelf.layout.model.id];
            }
        }];
        [_portrait addGestureRecognizer:tapGR];
    }
    return _portrait;
}
-(YYLabel *)yueduLabel
{
    if (!_yueduLabel) {
        _yueduLabel = [YYLabel new];
        _yueduLabel.font = [UIFont systemFontOfSize:15];
        _yueduLabel.textColor = [UIColor colorWithRed:74/255.0 green:90/255.0 blue:133/255.0 alpha:1];
        WS(weakSelf);
        UITapGestureRecognizer * tapGR = [[UITapGestureRecognizer alloc] bk_initWithHandler:^(UIGestureRecognizer *sender, UIGestureRecognizerState state, CGPoint location) {
            if (weakSelf.delegate && [weakSelf.delegate respondsToSelector:@selector(DynamicsCell:didClickUser:)]) {
                [weakSelf.delegate DynamicsCell:weakSelf didClickUser:weakSelf.layout.model.id];
            }
        }];
        [_yueduLabel addGestureRecognizer:tapGR];
    }
    return _yueduLabel;
}
-(YYLabel *)lineLabel
{
    if (!_lineLabel) {
        _lineLabel = [YYLabel new];
        _lineLabel.font = [UIFont systemFontOfSize:15];
        _lineLabel.textColor = [UIColor colorWithRed:74/255.0 green:90/255.0 blue:133/255.0 alpha:1];
        WS(weakSelf);
        UITapGestureRecognizer * tapGR = [[UITapGestureRecognizer alloc] bk_initWithHandler:^(UIGestureRecognizer *sender, UIGestureRecognizerState state, CGPoint location) {
            if (weakSelf.delegate && [weakSelf.delegate respondsToSelector:@selector(DynamicsCell:didClickUser:)]) {
                [weakSelf.delegate DynamicsCell:weakSelf didClickUser:weakSelf.layout.model.id];
            }
        }];
        [_lineLabel addGestureRecognizer:tapGR];
    }
    return _lineLabel;
}
-(YYLabel *)nameLabel
{
    if (!_nameLabel) {
        _nameLabel = [YYLabel new];
        _nameLabel.font = [UIFont systemFontOfSize:15];
        _nameLabel.textColor = [UIColor colorWithRed:74/255.0 green:90/255.0 blue:133/255.0 alpha:1];
        WS(weakSelf);
        UITapGestureRecognizer * tapGR = [[UITapGestureRecognizer alloc] bk_initWithHandler:^(UIGestureRecognizer *sender, UIGestureRecognizerState state, CGPoint location) {
            if (weakSelf.delegate && [weakSelf.delegate respondsToSelector:@selector(DynamicsCell:didClickUser:)]) {
                [weakSelf.delegate DynamicsCell:weakSelf didClickUser:weakSelf.layout.model.id];
            }
        }];
        [_nameLabel addGestureRecognizer:tapGR];
    }
    return _nameLabel;
}
-(YYLabel *)content
{
    if (!_content) {
        _content = [YYLabel new];
        _content.font = [UIFont systemFontOfSize:15];
        _content.textColor = [UIColor colorWithRed:74/255.0 green:90/255.0 blue:133/255.0 alpha:1];
        WS(weakSelf);
        UITapGestureRecognizer * tapGR = [[UITapGestureRecognizer alloc] bk_initWithHandler:^(UIGestureRecognizer *sencder, UIGestureRecognizerState state, CGPoint location) {
            if (weakSelf.delegate && [weakSelf.delegate respondsToSelector:@selector(DynamicsCell:didClickUser:)]) {
                [weakSelf.delegate DynamicsCell:weakSelf didClickUser:weakSelf.layout.model.id];
            }
        }];
        [_content addGestureRecognizer:tapGR];
    }
    return _content;
}
-(YYLabel *)detailLabel
{
    if (!_detailLabel) {
        _detailLabel = [YYLabel new];
        _detailLabel.textLongPressAction = ^(UIView * _Nonnull containerView, NSAttributedString * _Nonnull text, NSRange range, CGRect rect) {
//            containerView.backgroundColor = RGBA_COLOR(1, 1, 1, .2);
            [SVProgressHUD showSuccessWithStatus:@"文字复制成功!"];
            UIPasteboard * board = [UIPasteboard generalPasteboard];
            board.string = text.string;
//            [Utils delayTime:.5 TimeOverBlock:^{
//                containerView.backgroundColor = [UIColor clearColor];
//            }];
        };
    }
    return _detailLabel;
}
-(SDWeiXinPhotoContainerView *)picContainerView
{
    if (!_picContainerView) {
        _picContainerView = [SDWeiXinPhotoContainerView new];
        _picContainerView.hidden = YES;
    }
    return _picContainerView;
}
-(NewDynamicsGrayView *)grayView
{
    if (!_grayView) {
        _grayView = [NewDynamicsGrayView new];
        _grayView.cell = self;
    }
    return _grayView;
}
-(NewDynamicsOneImage *)oneImageView
{
    if (!_oneImageView) {
        _oneImageView= [NewDynamicsOneImage new];
        _oneImageView.cell = self;
    }
    return _oneImageView;
}
-(NewDynamicsPinglunView *)likeAndSayAndShare
{
    if (!_likeAndSayAndShare) {
        _likeAndSayAndShare = [NewDynamicsPinglunView new];
        _likeAndSayAndShare.cell = self;
    }
    return _likeAndSayAndShare;
}
-(YYLabel *)dateLabel
{
    if (!_dateLabel) {
        _dateLabel = [YYLabel new];
        _dateLabel.textColor = [UIColor lightGrayColor];
        _dateLabel.font = [UIFont systemFontOfSize:13];
    }
    return _dateLabel;
}
-(UIButton *)deleteBtn
{
    if (!_deleteBtn) {
        _deleteBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_deleteBtn setTitle:@"删除" forState:UIControlStateNormal];
        _deleteBtn.titleLabel.adjustsFontSizeToFitWidth = YES;
        _deleteBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        _deleteBtn.titleLabel.font = [UIFont systemFontOfSize:13];
        [_deleteBtn setTitleColor:[UIColor colorWithRed:74/255.0 green:90/255.0 blue:133/255.0 alpha:1] forState:UIControlStateNormal];
        WS(weakSelf);
        [_deleteBtn bk_addEventHandler:^(id sender) {
            if (weakSelf.delegate != nil && [weakSelf.delegate respondsToSelector:@selector(DidClickDeleteInDynamicsCell:)]) {
                [weakSelf.delegate DidClickDeleteInDynamicsCell:weakSelf];
            }
        } forControlEvents:UIControlEventTouchUpInside];
    }
    return _deleteBtn;
}
-(UIButton *)menuBtn
{
    if (!_menuBtn) {
        _menuBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _menuBtn.contentMode = UIViewContentModeScaleAspectFit;
        [_menuBtn setImage:[UIImage imageNamed:@"AlbumOperateMore"] forState:UIControlStateNormal];
    }
    return _menuBtn;
}
-(UIView *)dividingLine
{
    if (!_dividingLine) {
        _dividingLine = [UIView new];
        _dividingLine.backgroundColor = [UIColor lightGrayColor];
        _dividingLine.alpha = .3;
    }
    return _dividingLine;
}

- (NSString *)formateDate:(NSString *)dateString withFormate:(NSString *) formate
{
    
    //实例化一个NSDateFormatter对象
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:formate];
    
    NSDate * nowDate = [NSDate date];
    
    /////  将需要转换的时间转换成 NSDate 对象
    NSDate * needFormatDate = [dateFormatter dateFromString:dateString];
    /////  取当前时间和转换时间两个日期对象的时间间隔
    /////  这里的NSTimeInterval 并不是对象，是基本型，其实是double类型，是由c定义的:  typedef double NSTimeInterval;
    NSTimeInterval time = [nowDate timeIntervalSinceDate:needFormatDate];
    
    //// 再然后，把间隔的秒数折算成天数和小时数：
    
    NSString *dateStr = @"";
    
    if (time<=60) {  //// 1分钟以内的
        dateStr = @"刚刚";
    }else if(time<=60*60){  ////  一个小时以内的
        
        int mins = time/60;
        dateStr = [NSString stringWithFormat:@"%d分钟前",mins];
        
    }else if(time<=60*60*24){   //// 在两天内的
        
        [dateFormatter setDateFormat:@"YYYY/MM/dd"];
        NSString * need_yMd = [dateFormatter stringFromDate:needFormatDate];
        NSString *now_yMd = [dateFormatter stringFromDate:nowDate];
        
        [dateFormatter setDateFormat:@"HH:mm"];
        if ([need_yMd isEqualToString:now_yMd]) {
            //// 在同一天
            dateStr = [NSString stringWithFormat:@"今天 %@",[dateFormatter stringFromDate:needFormatDate]];
        }else{
            ////  昨天
            dateStr = [NSString stringWithFormat:@"昨天 %@",[dateFormatter stringFromDate:needFormatDate]];
        }
    }else {
        
        [dateFormatter setDateFormat:@"yyyy"];
        NSString * yearStr = [dateFormatter stringFromDate:needFormatDate];
        NSString *nowYear = [dateFormatter stringFromDate:nowDate];
        
        if ([yearStr isEqualToString:nowYear]) {
            ////  在同一年
            [dateFormatter setDateFormat:@"MM月dd日"];
            dateStr = [dateFormatter stringFromDate:needFormatDate];
        }else{
            [dateFormatter setDateFormat:@"yyyy/MM/dd"];
            dateStr = [dateFormatter stringFromDate:needFormatDate];
        }
    }
    
    return dateStr;
    
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
