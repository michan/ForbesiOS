//
//  NewDynamicsTableViewCell.h
//  LooyuEasyBuy
//
//  Created by Andy on 2017/9/27.
//  Copyright © 2017年 Doyoo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NewDynamicsLayout.h"
#import "SDWeiXinPhotoContainerView.h"
#import "JRMenuView.h"
@class NewDynamicsTableViewCell;
@protocol NewDynamicsCellDelegate;

@interface NewDynamicsPinglunView : UIView

@property(nonatomic,strong)UIButton * likeBt;
@property(nonatomic,strong)UIButton * sayBt;
@property(nonatomic,strong)UIButton * shareBt;
@property(nonatomic,strong)NewDynamicsTableViewCell * cell;

@end
@interface NewDynamicsGrayView : UIView

@property(nonatomic,strong)UIButton * grayBtn;
@property(nonatomic,strong)UIImageView * thumbImg;
@property(nonatomic,strong)YYLabel * dspLabel;

@property(nonatomic,strong)NewDynamicsTableViewCell * cell;

@end
@interface NewDynamicsOneImage : UIView

@property(nonatomic,strong)UIButton * grayBtn;
@property(nonatomic,strong)UIImageView * thumbImg;
@property(nonatomic,strong)YYLabel * dspLabel;

@property(nonatomic,strong)NewDynamicsTableViewCell * cell;

@end


@interface NewDynamicsTableViewCell : FBSCell

@property(nonatomic,strong)NewDynamicsLayout * layout;

@property(nonatomic,strong)UIImageView * portrait;
@property(nonatomic,strong)YYLabel * nameLabel;
@property(nonatomic,strong)YYLabel * content;
@property(nonatomic,strong)YYLabel * detailLabel;
@property(nonatomic,strong)YYLabel * titleLabel;
@property(nonatomic,strong)SDWeiXinPhotoContainerView *picContainerView;
@property(nonatomic,strong)NewDynamicsGrayView * grayView;
@property(nonatomic,strong)NewDynamicsOneImage * oneImageView;
@property(nonatomic,strong)YYLabel * yueduLabel;
@property(nonatomic,strong)YYLabel * dateLabel;
@property(nonatomic,strong)YYLabel * lineLabel;
@property(nonatomic,strong)UIButton * deleteBtn;
@property(nonatomic,strong)UIButton * menuBtn;
@property(nonatomic,strong)UIView * dividingLine;

@property(nonatomic,strong)JRMenuView * jrMenuView;

@property(nonatomic, strong)NewDynamicsPinglunView *likeAndSayAndShare;

@property(nonatomic,assign)id<NewDynamicsCellDelegate>delegate;

@end

@protocol NewDynamicsCellDelegate <FBSCellDelegate>
/**
 点击了用户头像或名称

 @param userId 用户ID
 */
- (void)DynamicsCell:(NewDynamicsTableViewCell *)cell didClickUser:(NSString *)userId;

/**
 点击了灰色详情

 */
- (void)DidClickGrayViewInDynamicsCell:(NewDynamicsTableViewCell *)cell;
/**
 点击了底部点赞 评论 分享按钮
 
 */
- (void)DidClickBottemViewInDynamicsCell:(NewDynamicsTableViewCell *)cell tag:(int)tag;
/**
 点赞

 */
- (void)DidClickThunmbInDynamicsCell:(NewDynamicsTableViewCell *)cell;

/**
 取消点赞

 */
- (void)DidClickCancelThunmbInDynamicsCell:(NewDynamicsTableViewCell *)cell;

/**
 评论

 */
- (void)DidClickCommentInDynamicsCell:(NewDynamicsTableViewCell *)cell;

/**
 分享

 */
- (void)DidClickShareInDynamicsCell:(NewDynamicsTableViewCell *)cell;

/**
 删除

 */
- (void)DidClickDeleteInDynamicsCell:(NewDynamicsTableViewCell *)cell;


/**
 点击了网址或电话号码
 @param url 网址链接
 @param phoneNum 电话号
 */
- (void)DynamicsCell:(NewDynamicsTableViewCell *)cell didClickUrl:(NSString *)url PhoneNum:(NSString *)phoneNum;
@end


