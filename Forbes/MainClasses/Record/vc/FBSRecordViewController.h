//
//  FBSRecordViewController.h
//  Forbes
//
//  Created by 赵志辉 on 2019/11/26.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import "FBSViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface FBSRecordViewController : FBSViewController

- (instancetype)initwithSelect:(NSInteger)selectIndex;
@end

NS_ASSUME_NONNULL_END
