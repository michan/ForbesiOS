//
//  FBSRecordLikeViewController.m
//  Forbes
//
//  Created by 赵志辉 on 2019/11/26.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import "FBSRecordLikeViewController.h"
#import "FBSRecordLikeViewModel.h"
#import "NewDynamicsLayout.h"
#import "NewDynamicsTableViewCell.h"
#import "FBSTableViewController+Refresh.h"

@interface FBSRecordLikeViewController ()
@property (nonatomic, strong) FBSRecordLikeViewModel *viewModel;
@end

@implementation FBSRecordLikeViewController
@synthesize viewModel = _viewModel;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationBarHidden = YES;
    
    self.registerDictionary = @{
                                [NewDynamicsLayout reuseIdentifier] : [NewDynamicsTableViewCell class],
                                };    
}

- (void)createload {
    self.page = @(1);
    self.pageCount = @(50);
    WEAKSELF
    [self normalHeaderRefreshingActionBlock:^(FooterConfigBlock  _Nonnull footerConfig) {
        [weakSelf.viewModel rqContent:^(BOOL success, NSInteger count) {
            if (success) {
                [weakSelf.tableView reloadData];
            }
            if(footerConfig){
                footerConfig(count);
            }
        }];
    }];
    
    
    [self backNormalFooterRefreshingActionBlock:^(FooterConfigBlock  _Nonnull footerConfig) {
        [weakSelf.viewModel rqContent:^(BOOL success, NSInteger count)  {
            if (success) {
                [weakSelf.tableView reloadData];
            }
            if(footerConfig){
                footerConfig(count);
            }
        }];
    }];
}

#pragma mark - getters and setters
-(FBSRecordLikeViewModel *)viewModel{
    if (!_viewModel) {
        _viewModel = [FBSRecordLikeViewModel new];
    }
    return _viewModel;
}
#pragma mark - delegate
- (void)fbsCell:(FBSCell *)cell didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    //    if ([cell isKindOfClass:[FBSMainCell class]]) {
    //
    //    }
}
- (void)fbsCell:(FBSCell *)cell didClickButtonAtIndex:(NSInteger)index {
    //    if ([cell isKindOfClass:[FBSBannerCell class]]) {
    //
    //    }
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NewDynamicsLayout * layout = self.viewModel.items[indexPath.row];
    return layout.height;
}
- (void)DidClickGrayViewInDynamicsCell:(NewDynamicsTableViewCell *)cell{
    NewDynamicsLayout *itemLayout = (NewDynamicsLayout *)cell.item;
    if ([itemLayout.model.object_type isEqualToString:@"1"]) {
        DetailViewController *vc = [[DetailViewController alloc] initWithArticleId:itemLayout.model.object_id htmlAddressL:@"article/" cellAddress:@"interest/"];
        [self.navigationController pushViewController:vc animated:YES];
    }else if([itemLayout.model.object_type isEqualToString:@"2"]){
        DetailViewController *vc = [[DetailViewController alloc] initWithArticleId:itemLayout.model.object_id htmlAddressL:@"activity/" cellAddress:@"interest/"];
        vc.position_id = @"36";
        [self.navigationController pushViewController:vc animated:YES];
    }else if([itemLayout.model.object_type isEqualToString:@"3"]){
        DetailViewController *vc = [[DetailViewController alloc] initWithArticleId:itemLayout.model.object_id htmlAddressL:@"article/" cellAddress:@"interest/"];
        [self.navigationController pushViewController:vc animated:YES];
    }
}
- (void)DidClickBottemViewInDynamicsCell:(NewDynamicsTableViewCell *)cell tag:(int)tag
{
    if (tag == 0) {
        // 点赞
        NewDynamicsLayout *itemLayout = (NewDynamicsLayout *)cell.item;
        DynamicsModel *model = itemLayout.model;
        [self showHUD];
        WEAKSELF;
        NSString *islike = @"";
        if ([model.has_like isEqualToString:@"Y"]) {
            islike = @"unlike";
        }else{
            islike = @"like";
        }
        [self.viewModel.PinglunLikeCollectionHositoryModel rqLikeobject_type:model.object_type object_id:model.object_id action:islike object_title:model.title block:^(BOOL success, NSInteger count) {
            if (success) {
                [weakSelf hideHUD];
                [weakSelf makeToast:@"成功"];
                if ([model.has_like isEqualToString:@"Y"]) {
                    model.has_like = @"N";
                    model.like_count = [NSString stringWithFormat:@"%d",[model.like_count intValue] - 1];
                }else{
                    model.has_like = @"Y";
                    model.like_count = [NSString stringWithFormat:@"%d",[model.like_count intValue] + 1];
                }
                [weakSelf.tableView reloadData];
            }else{
                [weakSelf hideHUD];
                [weakSelf makeToast:@"失败"];
            }
        }];
    }else if (tag == 1){
        // 评论的话跳转页面
        NewDynamicsLayout *itemLayout = (NewDynamicsLayout *)cell.item;
        if ([itemLayout.model.object_type isEqualToString:@"1"]) {
            DetailViewController *vc = [[DetailViewController alloc] initWithArticleId:itemLayout.model.object_id htmlAddressL:@"article/" cellAddress:@"interest/"];
            [self.navigationController pushViewController:vc animated:YES];
        }else if([itemLayout.model.object_type isEqualToString:@"2"]){
            DetailViewController *vc = [[DetailViewController alloc] initWithArticleId:itemLayout.model.object_id htmlAddressL:@"activity/" cellAddress:@"interest/"];
            vc.position_id = @"36";
            [self.navigationController pushViewController:vc animated:YES];
        }else if([itemLayout.model.object_type isEqualToString:@"3"]){
            DetailViewController *vc = [[DetailViewController alloc] initWithArticleId:itemLayout.model.object_id htmlAddressL:@"article/" cellAddress:@"interest/"];
            [self.navigationController pushViewController:vc animated:YES];
        }
    }else if (tag == 2){
        // 分享
    }
}
- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self createload];
}
@end
