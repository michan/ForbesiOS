//
//  FBSMyActivityManagerViewController.m
//  Forbes
//
//  Created by 赵志辉 on 2019/9/30.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import "FBSRecordViewController.h"
#import "WMPageController.h"
#import "FBSMyActivitylistViewController.h"
#import "FBSRecordPinglunViewController.h"
#import "FBSRecordLikeViewController.h"
#import "FBSRecordCollectionViewController.h"
#import "FBSRecordHositoryViewController.h"

@interface FBSRecordViewController()<WMPageControllerDelegate,WMPageControllerDataSource>

@property (nonatomic, strong) WMPageController *pageController;
@property (nonatomic, strong) NSArray *tabTitles;
@property (nonatomic, strong) UIView *pageContainerView;
@property (nonatomic, strong) NSArray *channelIdArray;
@property(nonatomic, strong)FBSNavigationItem *btn;
@property(nonatomic, assign)NSInteger selectIndex;



@end

@implementation FBSRecordViewController
- (instancetype)initwithSelect:(NSInteger)selectIndex{
    if (self == [super init]) {
        self.selectIndex = selectIndex;
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self initNavBar];
    
    
    self.tabTitles = @[@"评论",@"收藏",@"点赞",@"历史"];
    [self.pageContainerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.navigationBar.mas_bottom);
        make.bottom.left.right.mas_equalTo(0);
    }];
    
    
    [self.pageController.view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.mas_equalTo(0);
        make.width.mas_equalTo(kScreenWidth);
        make.height.mas_equalTo(self.pageContainerView);
    }];
    
    
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

- (void)initNavBar {
    self.title = @"我的足迹";
    //self.navigationBar.rightBarButtonItems = @[self.btn];
    
}
- (void)clickRight:(BOOL)status{
    if ([self.pageController.currentViewController isKindOfClass:[FBSMyActivitylistViewController class]]) {
        FBSMyActivitylistViewController*vc = (FBSMyActivitylistViewController *)self.pageController.currentViewController;
        [vc clickRight:status];
    }
}

- (FBSNavigationItem *)btn{
    if (!_btn) {
        _btn = [[FBSNavigationItem alloc] init];
        _btn.title = @"编辑";
        _btn.titleAttributes = @{NSFontAttributeName:kLabelFontSize14,NSForegroundColorAttributeName:Hexcolor(0x4A4A4A)};
        WEAKSELF
        _btn.action = ^(FBSNavigationItem *iten){
            if ([weakSelf.btn.title isEqualToString:@"编辑"]) {
                weakSelf.btn.title =  @"完成";
                [weakSelf clickRight:YES];
                
            }else{
                weakSelf.btn.title =  @"编辑";
                [weakSelf clickRight:NO];
                
            }
        };
    }
    return _btn;
}

#pragma mark - WMPageControllerDataSource

- (NSInteger)numbersOfChildControllersInPageController:(WMPageController *)pageController {
    return self.tabTitles.count;
}

- (NSString *)pageController:(WMPageController *)pageController titleAtIndex:(NSInteger)index {
    return [self.tabTitles objectAtIndex:index];
}

- (UIViewController *)pageController:(WMPageController *)pageController viewControllerAtIndex:(NSInteger)index {
    
    if (index == 0) {
        return [[FBSRecordPinglunViewController alloc] init];
    }else if (index == 1) {
        return [[FBSRecordCollectionViewController alloc] init];
    }else if (index == 2) {
        return [[FBSRecordLikeViewController alloc] init];
    }else if (index == 3) {
        return [[FBSRecordHositoryViewController alloc] init];
    }else{
        return nil;
    }
}

- (CGRect)pageController:(WMPageController *)pageController preferredFrameForMenuView:(WMMenuView *)menuView {
    CGFloat leftMargin = 0;
    CGFloat rightMargin = 0;
    CGFloat originY = 0;
    return CGRectMake(leftMargin, originY, pageController.view.frame.size.width - leftMargin - rightMargin, 44);
}

- (CGRect)pageController:(WMPageController *)pageController preferredFrameForContentView:(WMScrollView *)contentView {
    CGFloat originY = CGRectGetMaxY([self pageController:pageController preferredFrameForMenuView:self.pageController.menuView]);
    return CGRectMake(0, originY + 1, pageController.view.frame.size.width, pageController.view.frame.size.height - originY - 1);
}


#pragma mark - property




- (UIView *)pageContainerView {
    if (!_pageContainerView) {
        _pageContainerView = [[UIView alloc] init];
        _pageContainerView.backgroundColor = [UIColor yellowColor];
        [self.view addSubview:_pageContainerView];
    }
    return _pageContainerView;
}

- (WMPageController *)pageController {
    if (!_pageController) {
        _pageController = [[WMPageController alloc] init];
        _pageController.dataSource = self;
        _pageController.delegate = self;
        _pageController.selectIndex = self.selectIndex;
        _pageController.menuViewStyle = WMMenuViewStyleLine;
        _pageController.progressViewIsNaughty = YES;
        _pageController.progressWidth = 25;
        _pageController.automaticallyCalculatesItemWidths = YES;
        _pageController.titleColorNormal = Hexcolor(0x4A4A4A);
        _pageController.titleColorSelected = MainThemeColor;
        _pageController.titleSizeNormal = WScale(17);
        _pageController.titleSizeSelected = WScale(19);
        _pageController.view.backgroundColor = RGB(242,242,242);
        _pageController.menuView.backgroundColor = [UIColor whiteColor];
        _pageController.itemMargin = WScale(10);
        [self addChildViewController:_pageController];
        [self.pageContainerView addSubview:_pageController.view];
    }
    return _pageController;
}

@end
