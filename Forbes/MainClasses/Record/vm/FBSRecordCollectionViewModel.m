//
//  FBSRecordCollectionViewModel.m
//  Forbes
//
//  Created by 赵志辉 on 2019/11/26.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import "FBSRecordCollectionViewModel.h"
#import "FBSRecordModel.h"
#import "DynamicsModel.h"
#import "NewDynamicsLayout.h"
@interface FBSRecordCollectionViewModel ()

@property (nonatomic ,strong)FBSRecordModel *model;
@property (nonatomic ,strong)FBSGetRequest *detailAPI;
@end

@implementation FBSRecordCollectionViewModel

- (void)rqContent:(void(^)(BOOL success,NSInteger count))complete;{
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    FBSUserData *user = [FBSUserData sharedData];
    [dic setObject:user.useId forKey:@"user_id"];
    [dic setObject:@(self.page) forKey:@"page"];
    self.detailAPI.requestParameter = dic;
    [self.detailAPI startWithSuccess:^(YBNetworkResponse * _Nonnull response) {
        self.model = [FBSRecordModel mj_objectWithKeyValues:response.responseObject];
        if (self.page == 1) {
            [self.items removeAllObjects];
        }
        [self createItem];
        complete(YES, 0);
    } failure:^(YBNetworkResponse * _Nonnull response) {
        complete(NO, 0);
    }];
}
- (void)createItem{
    for (FBSRecordModelItem *item in self.model.data.items) {
        DynamicsModel *model =  [DynamicsModel mj_objectWithKeyValues:[item mj_keyValues]];
        NewDynamicsLayout *layout = [[NewDynamicsLayout alloc] initWithModel:model type:cellTypeOneImage];
        layout.bottomLineHidden = YES;
        [self.items addObject:layout];
    }
}
- (FBSGetRequest *)detailAPI {
    if (!_detailAPI) {
        _detailAPI = [[FBSGetRequest alloc] init];
        _detailAPI.requestURI = @"user/favs";
    }
    return _detailAPI;
}

- (FBSRecordPinglunLikeCollectionHositoryModel *)PinglunLikeCollectionHositoryModel
{
    if (!_PinglunLikeCollectionHositoryModel) {
        _PinglunLikeCollectionHositoryModel = [[FBSRecordPinglunLikeCollectionHositoryModel alloc] init];
    }
    return _PinglunLikeCollectionHositoryModel;
}
@end
