//
//  FBSRecordPinglunLikeCollectionHositoryModel.h
//  Forbes
//
//  Created by 赵志辉 on 2019/11/26.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import "FBSBaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface FBSRecordPinglunLikeCollectionHositoryModel : FBSBaseModel
- (void)rqLikeobject_type:(NSString *)object_type object_id:(NSString *)object_id action:(NSString *)action object_title:(NSString *)object_title block:(void(^)(BOOL success,NSInteger count))complete;
@end

NS_ASSUME_NONNULL_END
