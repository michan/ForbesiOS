//
//  FBSRecordCollectionViewModel.h
//  Forbes
//
//  Created by 赵志辉 on 2019/11/26.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import "FBSTableViewModel.h"
#import "FBSRecordPinglunLikeCollectionHositoryModel.h"

@interface FBSRecordCollectionViewModel : FBSTableViewModel
@property(nonatomic, strong) FBSRecordPinglunLikeCollectionHositoryModel *PinglunLikeCollectionHositoryModel;

- (void)rqContent:(void(^)(BOOL success,NSInteger count))complete;

@end
