//
//  FBSRecordPinglunLikeCollectionHositoryModel.m
//  Forbes
//
//  Created by 赵志辉 on 2019/11/26.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import "FBSRecordPinglunLikeCollectionHositoryModel.h"

@interface FBSRecordPinglunLikeCollectionHositoryModel()

@property (nonatomic ,strong)FBSPostRequest *isLikeApi;
@property(nonatomic, strong) FBSPostRequest *favs;

@end
@implementation FBSRecordPinglunLikeCollectionHositoryModel

- (void)rqLikeobject_type:(NSString *)object_type object_id:(NSString *)object_id action:(NSString *)action object_title:(NSString *)object_title block:(void(^)(BOOL success,NSInteger count))complete{
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    FBSUserData *user = [FBSUserData sharedData];
    [dic setObject:user.useId forKey:@"user_id"];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"yyyy-MM-dd";
    NSString *plainText = [NSString stringWithFormat:@"%@%@",user.account,[formatter stringFromDate:[NSDate date]]];
    [dic setObject:[HLWJMD5 MD5ForLower32Bate:plainText] forKey:@"token"];
    [dic setObject:object_type forKey:@"object_type"];
    [dic setObject:object_id forKey:@"object_id"];
    [dic setObject:object_title forKey:@"object_title"];
    [dic setObject:action forKey:@"action"];
    self.isLikeApi.requestParameter = dic;
    [self.isLikeApi startWithSuccess:^(YBNetworkResponse * _Nonnull response) {
        NSInteger code = [response.responseObject[@"code"] integerValue];
        if (code == 200) {
            complete(YES, 0);
        }else{
            complete(NO,0);
        }
    } failure:^(YBNetworkResponse * _Nonnull response) {
        complete(NO, 0);
    }];
}
- (void)rqfansobject_type:(NSString *)object_type object_id:(NSString *)object_id action:(NSString *)action object_title:(NSString *)object_title  block:(void(^)(BOOL success,NSInteger count))complete{
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    FBSUserData *user = [FBSUserData sharedData];
    [dic setObject:user.useId forKey:@"user_id"];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"yyyy-MM-dd";
    NSString *plainText = [NSString stringWithFormat:@"%@%@",user.account,[formatter stringFromDate:[NSDate date]]];
    [dic setObject:[HLWJMD5 MD5ForLower32Bate:plainText] forKey:@"token"];
    [dic setObject:object_type forKey:@"object_type"];
    [dic setObject:object_title forKey:@"object_title"];
    [dic setObject:action forKey:@"action"];
    [dic setObject:object_id forKey:@"object_id"];
    self.favs.requestParameter = dic;
    [self.favs startWithSuccess:^(YBNetworkResponse * _Nonnull response) {
        NSInteger code = (NSInteger)response.responseObject[@"code"];
        if (code == 200) {
            complete(YES, 0);
        }else{
            complete(NO,0);
        }
    } failure:^(YBNetworkResponse * _Nonnull response) {
        complete(NO, 0);
    }];
}
- (FBSPostRequest *)isLikeApi {
    if (!_isLikeApi) {
        _isLikeApi = [[FBSPostRequest alloc] init];
        _isLikeApi.requestURI = @"user/like";
    }
    return _isLikeApi;
}
- (FBSPostRequest *)favs {
    if (!_favs) {
        _favs = [[FBSPostRequest alloc] init];
        _favs.requestURI = @"user/favs";
    }
    return _favs;
}
@end
