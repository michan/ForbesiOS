//
//  FBSliveViewController.m
//  Forbes
//
//  Created by michan on 2020/6/17.
//  Copyright © 2020 周灿华. All rights reserved.
//

#import "FBSliveViewController.h"
#import "WeiboSDK.h"
#import "WeiboLiveSDK.h"
#import "WBIMLiveManager.h"
#import "WBIMLiveListener.h"
#pragma "WBIMPushMessageModel.h"
#import "WBIMBaseRequest.h"
#import "WBIMUser.h"
#import "WBIMBundle.h"
#import "FBSliveAuthorization.h"
#import "FBSliveBGView.h"
#import "FBLPromises.h"
#import "FBSliveModel.h"
//#import "PlayerViewController.h"
#import "LiveShowViewController.h"
#import "ZJSwitch.h"
#import "LiveVideoCoreSDK.h"

@interface FBSliveViewController ()<LIVEVCSessionDelegate>

{
    FBSliveBGView * _BGView;
    FBSliveModel *_liveModel;
    
}

@property (nonatomic) WeiboLiveSDK *weiboLiveSDK;


@end



@implementation FBSliveViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"直播";
    self.weiboLiveSDK = [[WeiboLiveSDK alloc] init];

    _BGView = [[[NSBundle mainBundle]loadNibNamed:@"FBSliveBGView" owner:self options:nil]lastObject];
    [self.view addSubview:_BGView];
    [_BGView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(kNavTopMargin);
        make.bottom.mas_equalTo(0);
        make.left.mas_equalTo(0);
        make.right.mas_equalTo(0);
    }];
    [_BGView.btn1 addTarget:self action:@selector(createlive) forControlEvents:1<<6];
    [_BGView.btn2 addTarget:self action:@selector(updateLive) forControlEvents:1<<6];
    [_BGView.btn3 addTarget:self action:@selector(createlive) forControlEvents:1<<6];
    [_BGView.btn4 addTarget:self action:@selector(createlive) forControlEvents:1<<6];
    [_BGView.btn5 addTarget:self action:@selector(createlive) forControlEvents:1<<6];

    
    
    
//    [self prepareUI];
    // Do any additional setup after loading the view.
}
//创建
-(void)createlive{
    
//    NSString* result = [self.weiboLiveSDK createLive:self.accessToken?:@"" title:@"直播" width:@"320" height:@"320" summary:@"" published:@"" image:@"" replay:@"" is_panolive:@""];
//
//    NSLog(@"result == %@",result);

      [self showHUD];
      FBLPromise<NSString *> * promise = [FBLPromise onQueue:dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0) async:^(FBLPromiseFulfillBlock  _Nonnull fulfill, FBLPromiseRejectBlock  _Nonnull reject) {
          //耗时操作
          //执行成功
         
        
          NSString* result = [self.weiboLiveSDK createLive:self.accessToken?:@"" title:@"直播" width: [NSString stringWithFormat:@"%f",Main_Screen_Width] height: [NSString stringWithFormat:@"%f",Main_Screen_Height] summary:@"" published:@"" image:@"" replay:@"" is_panolive:@""];

          
          fulfill(result);
    
      
      }];
      [promise then:^id _Nullable(NSString * _Nullable value) {
          [self hideHUD];
          NSString *result =(NSString *)value;
          NSLog(@"result===%@",result);

          [self createlivedata:result];
          
          //这里返回的对象，可以是值，error，或者另一个promise，
          return value;
      }];

    
    
}

-(void)createlivedata:(NSString*)result{
//    result==={"code":100000,"msg":"success","error_code":0,"data":{"id":"41dc2834b7cb3e83699cb090278f6df8","room_id":"1022:23205041dc2834b7cb3e83699cb090278f6df8","rtmp_url":"rtmp:\/\/psmedia.live.weibo.com\/alicdn\/4543908837065930?auth_key=1599440551-0-0-c03e2ab39682a9833eb58240d0a52deb"}}
    if (!result) {
        [self makeToast:@"创建失败"];
        return;
    }
    
    NSData *jsonData = [result dataUsingEncoding:NSUTF8StringEncoding];
    if (!jsonData) {
        [self makeToast:@"创建失败"];
        return;

    }
    NSError *err;
    
    NSDictionary *  resultDic = [NSJSONSerialization JSONObjectWithData:jsonData
                                                                options:NSJSONReadingMutableContainers
                                                                  error:&err];
    
    if (resultDic[@"code"]) {
        NSInteger code =[resultDic[@"code"] integerValue];
        if (code ==100000) {
            _liveModel = [FBSliveModel mj_objectWithKeyValues:resultDic[@"data"]];
          LiveShowViewController*  _LiveShowViewController = [[LiveShowViewController alloc] init];
            _LiveShowViewController.liveModel = _liveModel;
            _LiveShowViewController.accessToken = _accessToken;
            //[_LiveShowViewController setModalTransitionStyle:UIModalTransitionStyleFlipHorizontal];
            _LiveShowViewController.RtmpUrl = [NSURL URLWithString:_liveModel.rtmp_url];
//            _LiveShowViewController.IsHorizontal = [_horizontalSwitch isOn];

                        [self.navigationController pushViewController:_LiveShowViewController animated:NO];

            
            
            
//            PlayerViewController *playervc =[[PlayerViewController alloc]init];
//            playervc.playUrl = _liveModel.rtmp_url;
//            [self.navigationController pushViewController:playervc animated:NO];

//            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:_liveModel.rtmp_url]];
//            [[LiveVideoCoreSDK sharedinstance] LiveInit:[NSURL URLWithString:_liveModel.rtmp_url]
//                                                Preview:self.view
//                                               VideSize:(CGSizeMake(540, 960))
//                                                BitRate:LIVE_BITRATE_800Kbps
//                                              FrameRate:LIVE_VIDEO_DEF_FRAMERATE
//                                            highQuality:true];
//            [LiveVideoCoreSDK sharedinstance].delegate = self;
//            [[LiveVideoCoreSDK sharedinstance] connect];
//
//            [LiveVideoCoreSDK sharedinstance].micGain = 5;

        }
        else
        {
            [self makeToast:resultDic[@"msg"]];
            return;

        }
    }
    
    

    
}
- (void) LiveConnectionStatusChanged: (LIVE_VCSessionState) sessionState{
    NSLog(@"RTMP状态 : %d", (int)sessionState);
}






-(void)updateLive{
  NSString* result =  [self.weiboLiveSDK updateLive:[FBSliveAuthorization sharedInstance].accessToken?:@"" liveid:@"id" title:@"直播" summary:nil published:nil image:nil stop:nil replay_url:nil];
    
    NSLog(@"result == %@",result);
       
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
