//
//  FBSliveModel.h
//  Forbes
//
//  Created by michan on 2020/8/31.
//  Copyright © 2020 周灿华. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface FBSliveModel : NSObject



@property(nonatomic,copy)NSString * room_id;
@property(nonatomic,copy)NSString * id;
@property(nonatomic,copy)NSString * rtmp_url;
@property(nonatomic,copy)NSString * url;

@end

NS_ASSUME_NONNULL_END
