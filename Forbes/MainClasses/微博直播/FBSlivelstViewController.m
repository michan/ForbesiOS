//
//  FBSlivelstViewController.m
//  Forbes
//
//  Created by michan on 2020/11/18.
//  Copyright © 2020 周灿华. All rights reserved.
//

#import "FBSlivelstViewController.h"

@interface FBSlivelstViewController ()

@end

@implementation FBSlivelstViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"直播";
    [self proxylivelst];
    // Do any additional setup after loading the view.
}

#pragma mark - 直播列表接口
-(void)proxylivelst{
    
    NSDictionary *parameters = @{
        @"access_token":_accessToken?:@"",
        @"page":@"1",
        @"count":@"10"
    };
    [self showHUD];

    [MCNetworking ResponseNetworkGET_API:@"proxy/live/livelist" parameters:parameters TheServer:1 cachePolicy:NO ISNeedLogin:YES RequestEnd:^(id  _Nonnull EndObject) {
        [self hideHUD];

    } MCHttpCacheData:^(id  _Nonnull CacheObject) {
        
    } success:^(id  _Nonnull responseObject) {
        NSLog(@"responseObject == %@",responseObject);
        
        
    } failure:^(NSURLSessionDataTask * _Nonnull operation, NSError * _Nonnull error, id  _Nonnull responseObject, NSString * _Nonnull description) {
        [self makeToast:description];

    }];

    
    
    
    
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
