//
//  FBSliveAuthorization.m
//  Forbes
//
//  Created by michan on 2020/6/17.
//  Copyright © 2020 周灿华. All rights reserved.
//

#import "FBSliveAuthorization.h"
#import "FBSliveViewController.h"
#import "LiveShowViewController.h"
#import "FBSliveModel.h"
#import "FBLPromises.h"
#import "FBSlivelstViewController.h"

@implementation FBSliveAuthorization

+ (FBSliveAuthorization *)sharedInstance
{
    static dispatch_once_t  onceToken;
    static FBSliveAuthorization * sSharedInstance;
    dispatch_once(&onceToken, ^{

        sSharedInstance = [[FBSliveAuthorization alloc]init];

        
    });
    return sSharedInstance;
}

-(void)liveAuthorization:(UIViewController*)Controller{
    
//    if (self.accessToken.length) {
//        [self GOFBSliveViewController:Controller];
//        return;
//
//    }
    
    [SVProgressHUD show];
    
    WEAKSELF;
    
      [ShareSDK authorize:SSDKPlatformTypeSinaWeibo
                           settings:nil
                  onStateChanged:^(SSDKResponseState state, SSDKUser *user, NSError *error) {
           [SVProgressHUD dismiss];

           if (state == SSDKResponseStateSuccess)
           {
               NSLog(@"uid=%@",user.uid);
               NSLog(@"%@",user.credential);
               NSLog(@"token=%@",user.credential.token);
               NSLog(@"nickname=%@",user.nickname);
               
               weakSelf.accessToken =user.credential.token?:@"";
               weakSelf.userID =user.uid?:@"";
               
               [weakSelf GOFBSliveViewController:Controller];
           }
           else
           {
               [SVProgressHUD showImage:nil status:@"授权失败"];
               
               
           }
       }];
    
    
    
    
}



-(void)GOFBSliveViewController:(UIViewController*)Controller{
    
    _Controller =Controller;
    
//    [self createlive];
    
    
    
//    return;
    FBSlivelstViewController *ctl =[[FBSlivelstViewController alloc]init];
    ctl.accessToken = self.accessToken;
    [Controller.navigationController pushViewController:ctl animated:YES];
    
//    FBSliveViewController *ctl =[[FBSliveViewController alloc]init];
//    ctl.accessToken = self.accessToken;
//    [Controller.navigationController pushViewController:ctl animated:YES];
    
    
    
    
}
//创建
-(void)createlive{

    self.weiboLiveSDK = [[WeiboLiveSDK alloc] init];
    WEAKSELF;

      [SVProgressHUD show];
      FBLPromise<NSString *> * promise = [FBLPromise onQueue:dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0) async:^(FBLPromiseFulfillBlock  _Nonnull fulfill, FBLPromiseRejectBlock  _Nonnull reject) {
          //耗时操作
          //执行成功
        
          NSString* result = [self.weiboLiveSDK createLive:self.accessToken?:@"" title:@"直播" width: [NSString stringWithFormat:@"%f",Main_Screen_Width] height: [NSString stringWithFormat:@"%f",Main_Screen_Height] summary:@"" published:@"" image:@"" replay:@"" is_panolive:@""];

          
          fulfill(result);
    
      
      }];
      [promise then:^id _Nullable(NSString * _Nullable value) {
          [SVProgressHUD dismiss];
          NSString *result =(NSString *)value;
          NSLog(@"result===%@",result);

          [weakSelf createlivedata:result];
          
          //这里返回的对象，可以是值，error，或者另一个promise，
          return value;
      }];

    
    
}

-(void)createlivedata:(NSString*)result{
//    result==={"code":100000,"msg":"success","error_code":0,"data":{"id":"41dc2834b7cb3e83699cb090278f6df8","room_id":"1022:23205041dc2834b7cb3e83699cb090278f6df8","rtmp_url":"rtmp:\/\/psmedia.live.weibo.com\/alicdn\/4543908837065930?auth_key=1599440551-0-0-c03e2ab39682a9833eb58240d0a52deb"}}
    if (!result) {
        [SVProgressHUD showImage:nil status:@"创建失败"];
        return;
    }
    
    NSData *jsonData = [result dataUsingEncoding:NSUTF8StringEncoding];
    if (!jsonData) {
        [SVProgressHUD showImage:nil status:@"创建失败"];

        return;

    }
    NSError *err;
    
    NSDictionary *  resultDic = [NSJSONSerialization JSONObjectWithData:jsonData
                                                                options:NSJSONReadingMutableContainers
                                                                  error:&err];
    
    if (resultDic[@"code"]) {
        NSInteger code =[resultDic[@"code"] integerValue];
        if (code ==100000) {
         FBSliveModel*   _liveModel = [FBSliveModel mj_objectWithKeyValues:resultDic[@"data"]];
          LiveShowViewController*  _LiveShowViewController = [[LiveShowViewController alloc] init];
            _LiveShowViewController.liveModel = _liveModel;
            _LiveShowViewController.accessToken = _accessToken;
            _LiveShowViewController.RtmpUrl = [NSURL URLWithString:_liveModel.rtmp_url];

            [_Controller.navigationController pushViewController:_LiveShowViewController animated:NO];

   
        }
        else
        {
            [SVProgressHUD showImage:nil status:resultDic[@"msg"]];

            return;

        }
    }
    else if ([resultDic[@"rtmp_url"] length]){
        
                FBSliveModel*   _liveModel = [FBSliveModel mj_objectWithKeyValues:resultDic];
        if ([_liveModel.rtmp_url hasPrefix:@"rtmp:"]) {
            LiveShowViewController*  _LiveShowViewController = [[LiveShowViewController alloc] init];
              _LiveShowViewController.liveModel = _liveModel;
              _LiveShowViewController.accessToken = _accessToken;
              _LiveShowViewController.RtmpUrl = [NSURL URLWithString:_liveModel.rtmp_url];

              [_Controller.navigationController pushViewController:_LiveShowViewController animated:NO];

        }
        else
        {
            [SVProgressHUD showImage:nil status:@"创建失败"];

        }

        
    }
    
    

    
}




@end
