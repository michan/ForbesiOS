//
//  FBSliveAuthorization.h
//  Forbes
//
//  Created by michan on 2020/6/17.
//  Copyright © 2020 周灿华. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WeiboSDK.h"
#import "WeiboLiveSDK.h"

NS_ASSUME_NONNULL_BEGIN

@interface FBSliveAuthorization : NSObject
+ (FBSliveAuthorization *)sharedInstance;

-(void)liveAuthorization:(UIViewController*)Controller;

@property(nonatomic,copy)NSString * accessToken;
@property(nonatomic,copy)NSString * userID;
@property (nonatomic) WeiboLiveSDK *weiboLiveSDK;

@property(nonatomic,strong)UIViewController * Controller;
@end

NS_ASSUME_NONNULL_END
