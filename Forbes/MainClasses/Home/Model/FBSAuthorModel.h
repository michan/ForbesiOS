//
//  FBSAuthorModel.h
//  Forbes
//
//  Created by 周灿华 on 2019/10/1.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSBaseModel.h"

@interface FBSAuthorModel : FBSBaseModel
@property (nonatomic, copy) NSString *ID;
@property (nonatomic, copy) NSString *nickname;
@property (nonatomic, copy) NSString *describe;
@property (nonatomic, copy) NSString *file_url;
@property (nonatomic, copy) NSString *who;
@end
