//
//  FBSBrandvoiceModel.h
//  Forbes
//
//  Created by 周灿华 on 2019/9/29.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSBaseModel.h"

@class FBSBrandSummary,FBSBrandUser,FBSBrandContent;
@interface FBSBrandvoiceModel : FBSBaseModel
@property (nonatomic, strong) FBSBrandSummary *summary;
@property (nonatomic, strong) NSArray<FBSBrandUser *> *brand_users;
@property (nonatomic, strong) FBSBrandContent *brand_content;
@property (nonatomic, copy) NSString *channel_id;
@end


@interface FBSBrandSummary : FBSBaseModel
@property (nonatomic, strong) NSString *ID;
@property (nonatomic, strong) NSString *title;
@end


@interface FBSBrandUser : FBSBaseModel
@property (nonatomic, copy) NSString *ID;
@property (nonatomic, copy) NSString *nickname;
@property (nonatomic, copy) NSString *file_url;
@end


@interface FBSBrandContent : FBSBaseModel
@property (nonatomic, copy) NSString *file_url;
@property (nonatomic, copy) NSString *brand;
@property (nonatomic, copy) NSString *content;
@property (nonatomic, copy) NSString *logo;
@end
