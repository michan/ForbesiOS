//
//  FBSTopNewsModel.h
//  Forbes
//
//  Created by 周灿华 on 2019/9/22.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSBaseModel.h"

@class FBSTopNewsInfo;
@interface FBSTopNewsModel : FBSBaseModel
@property (nonatomic, strong) NSArray<FBSTopNewsInfo *> *data;
@end


@interface FBSTopNewsInfo : FBSBaseModel
@property (nonatomic, copy) NSString *ID;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *nickname;
@property (nonatomic, copy) NSString *time;
@end
