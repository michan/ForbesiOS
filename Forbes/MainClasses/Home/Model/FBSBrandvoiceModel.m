//
//  FBSBrandvoiceModel.m
//  Forbes
//
//  Created by 周灿华 on 2019/9/29.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSBrandvoiceModel.h"

@implementation FBSBrandvoiceModel

+ (NSDictionary *)mj_replacedKeyFromPropertyName {
    return @{
             @"summary" : @"data.brand",
             @"brand_users" : @"data.brand_users",
             @"brand_content" : @"data.brand_content",
             @"channel_id" : @"data.channel_id",
             };
}


+ (NSDictionary *)mj_objectClassInArray {
    return @{
             @"brand_users" : @"FBSBrandUser"
             };
}

@end


@implementation FBSBrandSummary

+ (NSDictionary *)mj_replacedKeyFromPropertyName {
    return @{
             @"ID" : @"id",
             };
}

@end


@implementation FBSBrandUser

+ (NSDictionary *)mj_replacedKeyFromPropertyName {
    return @{
             @"ID" : @"id",
             };
}

@end


@implementation FBSBrandContent


@end

