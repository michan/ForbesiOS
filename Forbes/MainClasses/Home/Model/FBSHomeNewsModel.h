//
//  FBSHomeNewsModel.h
//  Forbes
//
//  Created by 周灿华 on 2019/9/24.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSBaseModel.h"

///首页新闻列表模型
@class FBSHomeNewsInfo;
@interface FBSHomeNewsModel : FBSBaseModel
@property (nonatomic, strong) NSArray<FBSHomeNewsInfo *> *list;
@property (nonatomic, strong) NSArray<FBSHomeNewsInfo *> *items;
@property (nonatomic, copy)   NSString *page;
@property (nonatomic, copy)   NSString *total;
@property (nonatomic, copy)   NSString *limit;
@end



///首页新闻列表模型
@interface FBSHomeNewsInfo : NSObject
@property (nonatomic, copy) NSString *articleId;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *describe;
@property (nonatomic, copy) NSString *updated_at;
@property (nonatomic, copy) NSString *user_id;
@property (nonatomic, copy) NSString *nickname;
@property (nonatomic, copy) NSString *file_url;
@end

