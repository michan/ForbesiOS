//
//  FBSTopicModel.h
//  Forbes
//
//  Created by 周灿华 on 2019/9/22.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSBaseModel.h"

@class FBSTopicData;
@interface FBSTopicModel : FBSBaseModel
@property (nonatomic, strong) FBSTopicData *data;
@end


@interface FBSTopicData : FBSBaseModel
@property (nonatomic, copy) NSString *ID;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *created_at;
@property (nonatomic, copy) NSString *file_url;
@property (nonatomic, copy) NSString *nickname;
@end
