//
//  FBSTopNewsModel.m
//  Forbes
//
//  Created by 周灿华 on 2019/9/22.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSTopNewsModel.h"

@implementation FBSTopNewsModel

+ (NSDictionary *)mj_objectClassInArray {
    return @{@"data" : @"FBSTopNewsInfo"};
}

@end



@implementation FBSTopNewsInfo


+ (NSDictionary *)mj_replacedKeyFromPropertyName {
    return @{@"ID" : @"id"};
}

@end

