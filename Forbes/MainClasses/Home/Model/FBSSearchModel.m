//
//  FBSSearchModel.m
//  Forbes
//
//  Created by 周灿华 on 2019/9/27.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSSearchModel.h"

@implementation FBSSearchModel

+ (NSDictionary *)mj_replacedKeyFromPropertyName {
    return @{
             @"total" : @"data.total",
             @"page" : @"data.page",
             @"limit" : @"data.limit",
             @"items" : @"data.items",
             };
}


+ (NSDictionary *)mj_objectClassInArray {
    return @{
             @"items" : @"FBSSearchResult"
             };
}

@end



@implementation FBSSearchResult


+ (NSDictionary *)mj_replacedKeyFromPropertyName {
    return @{
             @"ID" : @"id",
             @"describe" : @"description"
             };
}

@end
