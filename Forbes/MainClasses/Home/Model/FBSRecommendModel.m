//
//  FBSRecommendModel.m
//  Forbes
//
//  Created by 周灿华 on 2019/9/23.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSRecommendModel.h"

@implementation FBSRecommendModel

+ (NSDictionary *)mj_objectClassInArray {
    return @{@"data" : @"FBSRecommendInfo"};
}

@end


@implementation FBSRecommendInfo

@end
