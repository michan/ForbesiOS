//
//  FBSTopicModel.m
//  Forbes
//
//  Created by 周灿华 on 2019/9/22.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSTopicModel.h"

@implementation FBSTopicModel

+ (NSDictionary *)mj_objectClassInArray {
    return @{@"data" : @"FBSTopicData"};
}

@end



@implementation FBSTopicData


+ (NSDictionary *)mj_replacedKeyFromPropertyName {
    return @{@"ID" : @"id"};
}

@end
