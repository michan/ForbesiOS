//
//  FBSRecommendModel.h
//  Forbes
//
//  Created by 周灿华 on 2019/9/23.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSBaseModel.h"

@class FBSRecommendInfo;
@interface FBSRecommendModel : FBSBaseModel
@property (nonatomic, strong) NSArray<FBSRecommendInfo *> *data;
@end


@interface FBSRecommendInfo : FBSBaseModel
//公用属性

//ad : 广告  content: 文章
@property (nonatomic, copy) NSString *data_type;
@property (nonatomic, copy) NSString *type;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *file_url;


//频道推荐属性
@property (nonatomic, copy) NSString *user_id;
@property (nonatomic, copy) NSString *nickname;
@property (nonatomic, copy) NSString *article_id;
@property (nonatomic, copy) NSString *channel_id;
@property (nonatomic, copy) NSString *channel_title;


//榜单推荐属性
@property (nonatomic, copy) NSString *relation_id;
@property (nonatomic, copy) NSString *created_at;

//广告推荐属性
@property (nonatomic, copy) NSString *end_at;
@property (nonatomic, copy) NSString *ad_unit_id;
@property (nonatomic, copy) NSString *file_id;
@property (nonatomic, copy) NSString *start_at;
@property (nonatomic, copy) NSString *type_id;
@property (nonatomic, copy) NSString *ad_type_id;
@property (nonatomic, copy) NSString *updated_at;
@property (nonatomic, copy) NSString *sort;
@property (nonatomic, copy) NSString *position_id;
//"id" : 16,
@end
