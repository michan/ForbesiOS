//
//  FBSSearchModel.h
//  Forbes
//
//  Created by 周灿华 on 2019/9/27.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSBaseModel.h"

@class FBSSearchResult;
@interface FBSSearchModel : FBSBaseModel
@property (nonatomic, copy) NSString *total;
@property (nonatomic, copy) NSString *page;
@property (nonatomic, copy) NSString *limit;
@property (nonatomic, strong) NSArray<FBSSearchResult *> *items;
@end


@interface FBSSearchResult : NSObject
@property (nonatomic, copy) NSString *ID;
@property (nonatomic, copy) NSString *describe;
@property (nonatomic, copy) NSString *relation_id;
@property (nonatomic, copy) NSString *relation_type;
@property (nonatomic, copy) NSString *created_at;
@property (nonatomic, copy) NSString *link;
@property (nonatomic, copy) NSString *time;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *file_url;
@property (nonatomic, copy) NSString *updated_at;
@property (nonatomic, copy) NSString *nickname;
@property (nonatomic, copy) NSString *is_open;

@property (nonatomic, copy) NSString *video_url;  //视频结果才有返回
@end

