//
//  FBSKeyNewsModel.h
//  Forbes
//
//  Created by 周灿华 on 2019/9/8.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSBaseModel.h"

@class FBSKeyNewsData,FBSKeyNewsItem;
@interface FBSKeyNewsModel : FBSBaseModel
@property (nonatomic, strong) NSArray<FBSKeyNewsData *> *data;
@end


@interface FBSKeyNewsData : NSObject
@property (nonatomic, strong) NSString *ID;
@property (nonatomic, strong) NSString *category;
@property (nonatomic, strong) NSArray<FBSKeyNewsItem *>*items;

@end


@interface FBSKeyNewsItem : NSObject
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *nickname;
@property (nonatomic, copy) NSString *user_id;
@property (nonatomic, copy) NSString *describe;
@property (nonatomic, copy) NSString *link;
@property (nonatomic, copy) NSString *type;
@property (nonatomic, copy) NSString *relation_type;
@property (nonatomic, copy) NSString *relation_id;
@property (nonatomic, copy) NSString *file_url;
@end

