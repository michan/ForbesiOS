//
//  FBSFollowModel.h
//  Forbes
//
//  Created by michan on 2020/6/16.
//  Copyright © 2020 周灿华. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface FBSFollowModel : NSObject
@property(nonatomic,copy)NSString * account;
@property(nonatomic,copy)NSString * id;
@property(nonatomic,copy)NSString * nickname;
@property(nonatomic,copy)NSString * channel_title;
@property(nonatomic,copy)NSString * image_url;
@property(nonatomic,copy)NSString * follow_count;
@property(nonatomic,copy)NSString * follow;


@end

NS_ASSUME_NONNULL_END
