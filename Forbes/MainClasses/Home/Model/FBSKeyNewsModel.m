//
//  FBSKeyNewsModel.m
//  Forbes
//
//  Created by 周灿华 on 2019/9/8.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSKeyNewsModel.h"

@implementation FBSKeyNewsModel

+ (NSDictionary *)mj_objectClassInArray {
    return @{@"data" : @"FBSKeyNewsData"};
}

@end



@implementation FBSKeyNewsData

+ (NSDictionary *)mj_replacedKeyFromPropertyName {
    return @{@"ID" : @"id"};
}


+ (NSDictionary *)mj_objectClassInArray {
    return @{@"items" : @"FBSKeyNewsItem"};
}

@end




@implementation FBSKeyNewsItem

+ (NSDictionary *)mj_replacedKeyFromPropertyName {
    return @{@"describe" : @"description"};
}

@end


