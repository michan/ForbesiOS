//
//  FBSAuthorModel.m
//  Forbes
//
//  Created by 周灿华 on 2019/10/1.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSAuthorModel.h"

@implementation FBSAuthorModel

+ (NSDictionary *)mj_replacedKeyFromPropertyName {
    return @{
             @"ID" : @"data.id",
             @"nickname" : @"data.nickname",
             @"describe" : @"data.description",
             @"file_url" : @"data.file_url",
             @"who" : @"data.who",
             };
}

@end
