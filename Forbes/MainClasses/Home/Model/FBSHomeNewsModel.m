//
//  FBSHomeNewsModel.m
//  Forbes
//
//  Created by 周灿华 on 2019/9/24.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSHomeNewsModel.h"

@implementation FBSHomeNewsModel

+ (NSDictionary *)mj_replacedKeyFromPropertyName {
    return @{
             @"list"  : @"data.list",
             @"items" : @"data.items",
             @"page"  : @"data.page",
             @"total" : @"data.total",
             @"limit" : @"data.limit",
             };
}



+ (NSDictionary *)mj_objectClassInArray {
    return @{
             @"list" : @"FBSHomeNewsInfo",
             @"items" : @"FBSHomeNewsInfo",
             };
}


@end




@implementation FBSHomeNewsInfo

+ (NSDictionary *)mj_replacedKeyFromPropertyName {
    return @{
             @"articleId"  : @"id",
             @"describe"  : @"description",
             };
}


@end
