//
//  FBSHomeListViewModel.h
//  Forbes
//
//  Created by 周灿华 on 2019/7/26.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSTableViewModel.h"
#import "FBSBannerCell.h"
#import "FBSOneImageCell.h"
#import "FBSThreeImageCell.h"
#import "FBSOnlyTextCell.h"

@interface FBSHomeListViewModel : FBSTableViewModel

@property (nonatomic, copy) NSString *channelId;

- (instancetype)initWithChannelId:(NSString *)channelId;

- (void)rqRecommendAndNews:(void(^)(BOOL success))complete;

- (void)rqLatestNews:(void(^)(BOOL success, NSInteger count))complete;

@end

