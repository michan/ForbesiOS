//
//  FBSTopicViewModel.h
//  Forbes
//
//  Created by 周灿华 on 2019/9/25.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSTableViewModel.h"
#import "FBSOneImageCell.h"
#import "FBSThreeImageCell.h"
#import "FBSOnlyTextCell.h"
#import "FBSSpaceCell.h"
#import "FBSGroupHeadCell.h"
#import "FBSTopicHeadCell.h"

@interface FBSTopicViewModel : FBSTableViewModel

- (instancetype)initWithTopicId:(NSString *)topicId;

- (void)rqTopicData:(void(^)(BOOL success))complete;

@end

