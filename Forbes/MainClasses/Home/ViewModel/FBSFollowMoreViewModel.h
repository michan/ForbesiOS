//
//  FBSFollowMoreViewModel.h
//  Forbes
//
//  Created by 周灿华 on 2019/8/24.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSTableViewModel.h"
#import "FBSFollowMoreCell.h"
#import "FBSFollowListCell.h"
#import "FBSSpaceCell.h"

@interface FBSFollowMoreViewModel : FBSTableViewModel

@end

