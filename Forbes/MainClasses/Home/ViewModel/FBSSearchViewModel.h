//
//  FBSSearchViewModel.h
//  Forbes
//
//  Created by 周灿华 on 2019/9/25.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSTableViewModel.h"
#import "FBSSearchTagsCell.h"
#import "FBSOneImageCell.h"
#import "FBSThreeImageCell.h"
#import "FBSOnlyTextCell.h"
#import "FBSVideoListCell.h"

#define SaveKeywordFilePath kCacheFilePath(@"keywordHistory.plist")

@interface FBSSearchViewModel : FBSTableViewModel
@property (nonatomic, strong) NSArray *tags;
@property (nonatomic, strong) FBSSearchTagsItem *tagsItem;


- (void)clearInputKeyWord;

- (void)rqToSearch:(NSString *)keyword complete:(void(^)(BOOL success,NSInteger count))complete;

@end
