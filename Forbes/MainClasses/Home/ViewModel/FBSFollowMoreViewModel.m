//
//  FBSFollowMoreViewModel.m
//  Forbes
//
//  Created by 周灿华 on 2019/8/24.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSFollowMoreViewModel.h"

@implementation FBSFollowMoreViewModel

- (void)makeItems {
    FBSFollowMoreItem *moreItem = [FBSFollowMoreItem item];
    moreItem.cellHeight = WScale(40);
    [self.items addObject:moreItem];
    
    [self.items addObject:[FBSSpaceItem item]];
    
    for (NSInteger i = 0; i < 10; i++) {
        FBSFollowListItem *listItem = [FBSFollowListItem item];
        listItem.name = @"用户名";
        listItem.desc = @"福布斯中国官方账号";
        listItem.followCount = @"10万关注";
        listItem.isFollow = i % 2;
        listItem.cellHeight = WScale(75);
        [self.items addObject:listItem];
    }
}

@end
