//
//  FBSBrandvoiceViewModel.h
//  Forbes
//
//  Created by 周灿华 on 2019/9/25.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSTableViewModel.h"
#import "FBSAuthorHeadCell.h"
#import "FBSAuthorCollectCell.h"
#import "FBSOneImageCell.h"
#import "FBSOnlyTextCell.h"
#import "FBSGroupHeadCell.h"

@interface FBSBrandvoiceViewModel : FBSTableViewModel

- (instancetype)initWithChannelId:(NSString *)channelId;

- (void)rqBrandvoiceData:(void(^)(BOOL success))complete;

- (void)rqLatestNews:(void(^)(BOOL success, NSInteger count))complete;

@end


