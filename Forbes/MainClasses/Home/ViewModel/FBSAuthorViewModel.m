//
//  FBSAuthorViewModel.m
//  Forbes
//
//  Created by 周灿华 on 2019/9/25.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSAuthorViewModel.h"
#import "FBSAuthorModel.h"
#import "FBSHomeNewsModel.h"

@interface FBSAuthorViewModel ()
@property (nonatomic, copy) NSString *authorId;
@property (nonatomic, strong) FBSGetRequest *authorAPI;
@property (nonatomic, strong) FBSGetRequest *newsAPI;  //最新新闻
@property (nonatomic, strong) FBSAuthorModel *authorModel;
@property (nonatomic, strong) FBSHomeNewsModel *newsModel;
@property (nonatomic, strong) void(^authorDataBlock)(BOOL success);
@end

@implementation FBSAuthorViewModel

- (instancetype)initWithAuthorId:(NSString *)authorId {
    self = [super init];
    if (self) {
        self.authorId = authorId ?: @"";
    }
    return self;
}

- (void)createItems {
    if (self.page == 1) {
        [self.items removeAllObjects];
        
        FBSAuthorHeadItem *headItem = [FBSAuthorHeadItem item];
        headItem.imgUrl = self.authorModel.file_url ?: @"";
        headItem.title = self.authorModel.nickname ?: @"";
        headItem.detail = self.authorModel.who ?: @"";
        headItem.content = self.authorModel.describe ?: @"";
        headItem.cellHeight = UITableViewAutomaticDimension;
        [self.items addObject:headItem];
        
        FBSGroupHeadItem *allGroupItem = [FBSGroupHeadItem item];
        allGroupItem.title = @"全部发表";
        [self.items addObject:allGroupItem];
        
    }
    
    //最新新闻列表
    for (FBSHomeNewsInfo *news in self.newsModel.items) {
        if (news.file_url.length) {
            FBSOneImageItem *oneImageItem = [FBSOneImageItem item];
            oneImageItem.detailType = DetailPageTypeArticle;
            oneImageItem.ID = news.articleId;
            oneImageItem.content = news.title;
            oneImageItem.author = news.nickname;
            oneImageItem.imgUrl = news.file_url;
            oneImageItem.comment = @"";
            oneImageItem.time = [NSString publishedTimeFormatWithTimeStamp:news.updated_at];;
            oneImageItem.cellHeight = WScale(95);
            [self.items addObject:oneImageItem];
            
        } else {
            FBSOnlyTextItem *textItem = [FBSOnlyTextItem item];
            textItem.detailType = DetailPageTypeArticle;
            textItem.ID = news.articleId;
            textItem.isTop = NO;
            textItem.content = news.title;
            textItem.author = news.nickname;
            textItem.comment = @"";
            textItem.time = [NSString publishedTimeFormatWithTimeStamp:news.updated_at];
            textItem.cellHeight = [textItem calculateCellHeight];
            [self.items addObject:textItem];
        }
    }
}


#pragma mark - request

- (void)rqAuthorData:(void(^)(BOOL success))complete {
    WEAKSELF
    self.authorDataBlock = complete;
    self.authorAPI.requestURI = [NSString stringWithFormat:@"author/%@",self.authorId];
    [self.authorAPI startWithSuccess:^(YBNetworkResponse * _Nonnull response) {
        FBSLog(@"获取作者主页回调 : %@",response.responseObject);
        weakSelf.authorModel = [FBSAuthorModel mj_objectWithKeyValues:response.responseObject];
       [weakSelf rqIssue:nil];
        
    } failure:^(YBNetworkResponse * _Nonnull response) {
        FBSLog(@"获取作者主页失败 : %@",response.error.localizedDescription);
    }];
    
}


- (void)rqIssue:(void(^)(BOOL success, NSInteger count))complete {
    WEAKSELF
    self.newsAPI.requestParameter = @{
                                      @"page" : [@(self.page) stringValue],
                                      @"user_id" : self.authorId
                                      };
    
    [self.newsAPI startWithSuccess:^(YBNetworkResponse * _Nonnull response) {
        FBSLog(@"获取品牌之声最新新闻 : %@",response.responseObject);
        weakSelf.newsModel = [FBSHomeNewsModel mj_objectWithKeyValues:response.responseObject];
        [weakSelf createItems];
        
        if (weakSelf.page == 1) {
            if (weakSelf.authorDataBlock) {
                weakSelf.authorDataBlock(YES);
            }
        } else {
            if (complete) {
                complete(YES,weakSelf.newsModel.list.count);
            }
        }
        
    } failure:^(YBNetworkResponse * _Nonnull response) {
        FBSLog(@"获取频道最新新闻失败 : %@",response.error.localizedDescription);
        [weakSelf createItems];
        
        if (weakSelf.page == 1) {
            if (weakSelf.authorDataBlock) {
                weakSelf.authorDataBlock(YES);
            }
        } else {
            if (complete) {
                complete(NO,0);
            }
        }
    }];
}

#pragma mark - property

- (FBSGetRequest *)authorAPI {
    if (!_authorAPI) {
        _authorAPI  = [[FBSGetRequest alloc] init];
    }
    return _authorAPI;
}


- (FBSGetRequest *)newsAPI {
    if (!_newsAPI) {
        _newsAPI  = [[FBSGetRequest alloc] init];
        _newsAPI.requestURI = @"author/news";
    }
    return _newsAPI;
}



@end
