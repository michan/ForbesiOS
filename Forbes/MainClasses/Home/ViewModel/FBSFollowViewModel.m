//
//  FBSFollowViewModel.m
//  Forbes
//
//  Created by 周灿华 on 2019/8/24.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSFollowViewModel.h"

@implementation FBSFollowViewModel


- (void)makeItems {
    FBSFollowCollectionItem *collectionItem  = [FBSFollowCollectionItem item];
    collectionItem.entities = @[
                                @"",
                                @"",
                                @"",
                                ];
    collectionItem.cellHeight = WScale(155);
    [self.items addObject:collectionItem];
    
    
    for (NSInteger i = 0; i < 10; i++) {
        FBSFollowOneImageItem *oneImageItem = [FBSFollowOneImageItem item];
        oneImageItem.author = @"福布斯";
        oneImageItem.desc = @"福布斯中国官方账号";
        oneImageItem.content = @"“王室”帽子救不了通灵珠宝，沈东军的钻石了通灵珠宝，沈东军的钻石";
        oneImageItem.hasFollow = YES;
        
        oneImageItem.cellHeight = WScale(145);
        [self.items addObject:oneImageItem];
    }
}

@end
