//
//  FBSBrandvoiceViewModel.m
//  Forbes
//
//  Created by 周灿华 on 2019/9/25.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSBrandvoiceViewModel.h"
#import "FBSHomeNewsModel.h"
#import "FBSBrandvoiceModel.h"

@interface FBSBrandvoiceViewModel ()
@property (nonatomic, copy) NSString *channelId;
@property (nonatomic, strong) FBSGetRequest *brandvoiceAPI;
@property (nonatomic, strong) FBSGetRequest *newsAPI;  //最新新闻
@property (nonatomic, strong) FBSBrandvoiceModel *brandvoiceModel;
@property (nonatomic, strong) FBSHomeNewsModel *newsModel;
@property (nonatomic, strong) void(^brandvoiceDataBlock)(BOOL success);
@end

@implementation FBSBrandvoiceViewModel

- (instancetype)initWithChannelId:(NSString *)channelId {
    self = [super init];
    if (self) {
        self.channelId = channelId ?: @"";
    }
    return self;
}

- (void)createItems {
    if (self.page == 1) {
        [self.items removeAllObjects];
       
        FBSAuthorHeadItem *headItem = [FBSAuthorHeadItem item];
        headItem.imgUrl = self.brandvoiceModel.brand_content.logo ?: @"";
        headItem.title = self.brandvoiceModel.summary.title ?: @"";
        headItem.detail = self.brandvoiceModel.brand_content.brand ?: @"";
        headItem.content = self.brandvoiceModel.brand_content.content ?: @"";
        headItem.cellHeight = UITableViewAutomaticDimension;
        [self.items addObject:headItem];
        
        FBSGroupHeadItem *authorGroupItem = [FBSGroupHeadItem item];
        authorGroupItem.title = @"品牌专栏作家";
        [self.items addObject:authorGroupItem];
        
        
        
        FBSAuthorCollectItem *collectItem = [FBSAuthorCollectItem item];
        collectItem.brand_users = self.brandvoiceModel.brand_users;
        collectItem.cellHeight = WScale(105);
        [self.items addObject:collectItem];
        
        FBSGroupHeadItem *allGroupItem = [FBSGroupHeadItem item];
        allGroupItem.title = @"全部发表";
        [self.items addObject:allGroupItem];
        
    }
    
    //最新新闻列表
    for (FBSHomeNewsInfo *news in self.newsModel.list) {
        if (news.file_url.length) {
            FBSOneImageItem *oneImageItem = [FBSOneImageItem item];
            oneImageItem.detailType = DetailPageTypeArticle;
            oneImageItem.ID = news.articleId;
            oneImageItem.content = news.title;
            oneImageItem.author = news.nickname;
            oneImageItem.imgUrl = news.file_url;
            oneImageItem.comment = @"";
            oneImageItem.time = [NSString publishedTimeFormatWithTimeStamp:news.updated_at];
            oneImageItem.cellHeight = WScale(95);
            [self.items addObject:oneImageItem];
            
        } else {
            FBSOnlyTextItem *textItem = [FBSOnlyTextItem item];
            textItem.detailType = DetailPageTypeArticle;
            textItem.ID = news.articleId;
            textItem.isTop = NO;
            textItem.content = news.title;
            textItem.author = news.nickname;
            textItem.comment = @"";
            textItem.time = [NSString publishedTimeFormatWithTimeStamp:news.updated_at];
            textItem.cellHeight = [textItem calculateCellHeight];
            [self.items addObject:textItem];
        }
    }
}


#pragma mark - request

- (void)rqBrandvoiceData:(void(^)(BOOL success))complete {
    WEAKSELF
    weakSelf.brandvoiceDataBlock = complete;
    [self.brandvoiceAPI startWithSuccess:^(YBNetworkResponse * _Nonnull response) {
        FBSLog(@"获取品牌之声返回 : %@",response.responseObject);
        weakSelf.brandvoiceModel = [FBSBrandvoiceModel mj_objectWithKeyValues:response.responseObject];
        if (weakSelf.brandvoiceModel.success) {
            weakSelf.channelId = weakSelf.brandvoiceModel.channel_id;
            
            [weakSelf rqLatestNews:nil];
            
        } else {
            if (weakSelf.brandvoiceDataBlock) {
                weakSelf.brandvoiceDataBlock(NO);
            }
        }
        
    } failure:^(YBNetworkResponse * _Nonnull response) {
        FBSLog(@"获取品牌之声失败 : %@",response.error.localizedDescription);
        
        if (weakSelf.brandvoiceDataBlock) {
            weakSelf.brandvoiceDataBlock(NO);
        }
    }];
    
}


- (void)rqLatestNews:(void(^)(BOOL success, NSInteger count))complete {
    WEAKSELF
    self.newsAPI.requestURI = [NSString stringWithFormat:@"news/%@/%@",self.channelId,[@(self.page) stringValue]];
    
    [self.newsAPI startWithSuccess:^(YBNetworkResponse * _Nonnull response) {
        FBSLog(@"获取品牌之声最新新闻 : %@",response.responseObject);
        weakSelf.newsModel = [FBSHomeNewsModel mj_objectWithKeyValues:response.responseObject];
        [weakSelf createItems];
        
        if (weakSelf.page == 1) {
            if (weakSelf.brandvoiceDataBlock) {
                weakSelf.brandvoiceDataBlock(YES);
            }
        } else {
            if (complete) {
                complete(YES,weakSelf.newsModel.list.count);
            }
        }
        
    } failure:^(YBNetworkResponse * _Nonnull response) {
        FBSLog(@"获取品牌之声最新新闻失败 : %@",response.error.localizedDescription);
        
        if (weakSelf.page == 1) {
            if (weakSelf.brandvoiceDataBlock) {
                weakSelf.brandvoiceDataBlock(NO);
            }
        } else {
            if (complete) {
                complete(NO,0);
            }
        }

    }];
}

#pragma mark - property

- (FBSGetRequest *)brandvoiceAPI {
    if (!_brandvoiceAPI) {
        _brandvoiceAPI  = [[FBSGetRequest alloc] init];
        _brandvoiceAPI.requestURI = @"brandvoice";
    }
    return _brandvoiceAPI;
}


- (FBSGetRequest *)newsAPI {
    if (!_newsAPI) {
        _newsAPI  = [[FBSGetRequest alloc] init];
        _newsAPI.requestURI = @"";
    }
    return _newsAPI;
}



@end
