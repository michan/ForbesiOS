
//
//  FBSHomeListViewModel.m
//  Forbes
//
//  Created by 周灿华 on 2019/7/26.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSHomeListViewModel.h"
#import "FBSRecommendModel.h"
#import "FBSHomeNewsModel.h"

@interface FBSHomeListViewModel ()
@property (nonatomic, strong) FBSGetRequest *recommendAPI; //推荐
@property (nonatomic, strong) FBSGetRequest *newsAPI;  //最新新闻
@property (nonatomic, strong) FBSRecommendModel *recommendModel;
@property (nonatomic, strong) FBSHomeNewsModel *newsModel;
@property (nonatomic, strong) void(^complete)(BOOL success);
@end

@implementation FBSHomeListViewModel

- (instancetype)initWithChannelId:(NSString *)channelId {
    self = [super init];
    if (self) {
        self.channelId = channelId ?: @"";
    }
    return self;
}

- (void)createItems {
    if (self.page == 1) {
        [self.items removeAllObjects];
        //创建banner
        if (self.recommendModel.data.count) {
            NSMutableArray *infos  = [NSMutableArray array];
            for (FBSRecommendInfo *recInfo in self.recommendModel.data) {
                
                FBSBannerInfo *bannerInfo = [FBSBannerInfo new];
                bannerInfo.isAd = [recInfo.data_type isEqualToString:@"ad"];
                bannerInfo.ID = recInfo.article_id;
                bannerInfo.title = recInfo.title;
                
                if (bannerInfo.isAd) {
                   bannerInfo.author = @"广告";
                } else {
                    bannerInfo.author = recInfo.nickname;
                }
                
                bannerInfo.time = @"";
                bannerInfo.imgUrl = recInfo.file_url;
                [infos addObject:bannerInfo];
            }
            if (infos.count > 1) {
                [infos exchangeObjectAtIndex:1 withObjectAtIndex:0];

            }
            
            
            
            FBSBannerItem *bannerItem = [FBSBannerItem item];
            bannerItem.datas = [infos copy];
            [self.items addObject:bannerItem];
        }
    }
    
    //最新新闻列表
    for (FBSHomeNewsInfo *news in self.newsModel.list) {
        if (news.file_url.length) {
            FBSOneImageItem *oneImageItem = [FBSOneImageItem item];
            oneImageItem.detailType = DetailPageTypeArticle;
            oneImageItem.ID = news.articleId;
            oneImageItem.content = news.title;
            oneImageItem.author = news.nickname;
            oneImageItem.imgUrl = news.file_url;
            oneImageItem.comment = @"";
            oneImageItem.time = [NSString publishedTimeFormatWithTimeStamp:news.updated_at];
            oneImageItem.cellHeight = WScale(95);
            [self.items addObject:oneImageItem];
            
        } else {
            FBSOnlyTextItem *textItem = [FBSOnlyTextItem item];
            textItem.detailType = DetailPageTypeArticle;
            textItem.ID = news.articleId;
            textItem.isTop = NO;
            textItem.content = news.title;
            textItem.author = news.nickname;
            textItem.comment = @"";
            textItem.time = [NSString publishedTimeFormatWithTimeStamp:news.updated_at];
            textItem.cellHeight = [textItem calculateCellHeight];
            [self.items addObject:textItem];
        }
    }
}


#pragma mark - request

- (void)rqRecommendAndNews:(void(^)(BOOL success))complete {
    WEAKSELF
    self.complete = complete;
    
    dispatch_group_t group = dispatch_group_create();
    //获取banner
    dispatch_group_enter(group);
    [self.recommendAPI startWithSuccess:^(YBNetworkResponse * _Nonnull response) {
        FBSLog(@"获取banner成功 : %@",response.responseObject);
        weakSelf.recommendModel = [FBSRecommendModel mj_objectWithKeyValues:response.responseObject];
        dispatch_group_leave(group);
        
    } failure:^(YBNetworkResponse * _Nonnull response) {
        FBSLog(@"获取banner失败 : %@",response.error.localizedDescription);
        dispatch_group_leave(group);
    }];
    
    
    //获取最新新闻
    dispatch_group_enter(group);
    self.newsAPI.requestURI = [NSString stringWithFormat:@"news/%@/%@",self.channelId,[@(self.page) stringValue]];
    [self.newsAPI startWithSuccess:^(YBNetworkResponse * _Nonnull response) {
        FBSLog(@"获取频道最新新闻成功 : %@",response.responseObject);
        weakSelf.newsModel = [FBSHomeNewsModel mj_objectWithKeyValues:response.responseObject];
        dispatch_group_leave(group);
        
    } failure:^(YBNetworkResponse * _Nonnull response) {
        FBSLog(@"获取频道最新新闻失败 : %@",response.error.localizedDescription);
        dispatch_group_leave(group);
        
    }];
    
    dispatch_group_notify(group, dispatch_get_main_queue(), ^{
        [weakSelf createItems];
        weakSelf.complete(YES);
    });
    
}


- (void)rqLatestNews:(void(^)(BOOL success, NSInteger count))complete {
    WEAKSELF
    self.newsAPI.requestURI = [NSString stringWithFormat:@"news/%@/%@",self.channelId,[@(self.page) stringValue]];
    
    [self.newsAPI startWithSuccess:^(YBNetworkResponse * _Nonnull response) {
        FBSLog(@"获取频道最新新闻成功 : %@",response.responseObject);
        weakSelf.newsModel = [FBSHomeNewsModel mj_objectWithKeyValues:response.responseObject];
        [weakSelf createItems];
        if (complete) {
            complete(YES,weakSelf.newsModel.list.count);
        }
    } failure:^(YBNetworkResponse * _Nonnull response) {
        FBSLog(@"获取频道最新新闻失败 : %@",response.error.localizedDescription);
        if (complete) {
            complete(NO,0);
        }

    }];
}

#pragma mark - property

- (FBSGetRequest *)recommendAPI {
    if (!_recommendAPI) {
        _recommendAPI  = [[FBSGetRequest alloc] init];
//        _recommendAPI.requestURI = [NSString stringWithFormat:@"recommend/%@",self.channelId];
        //新的频道推荐,包含广告
        _recommendAPI.requestURI = [NSString stringWithFormat:@"latest-recommend/%@",self.channelId];
    }
    return _recommendAPI;
}


- (FBSGetRequest *)newsAPI {
    if (!_newsAPI) {
        _newsAPI  = [[FBSGetRequest alloc] init];
        _newsAPI.requestURI = @"";
    }
    return _newsAPI;
}



@end
