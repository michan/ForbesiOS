//
//  FBSTopicViewModel.m
//  Forbes
//
//  Created by 周灿华 on 2019/9/25.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSTopicViewModel.h"
#import "FBSHomeAPI.h"
#import "FBSTopicModel.h"

@interface FBSTopicViewModel ()
@property (nonatomic, copy) NSString *topicId;
@property (nonatomic, strong) FBSTopicAPI *topicAPI;
@property (nonatomic, strong) FBSGetRequest *topicListAPI;

@property (nonatomic, strong) FBSTopicModel *topicModel;
@property (nonatomic, strong) NSArray *topicList;
@end

@implementation FBSTopicViewModel

- (instancetype)initWithTopicId:(NSString *)topicId {
    self = [super init];
    if (self) {
        self.topicId = topicId;
    }
    return self;
}

- (void)rqTopicData:(void(^)(BOOL success))complete {
    WEAKSELF
    
    dispatch_group_t group = dispatch_group_create();
    
    //获取专题
    dispatch_group_enter(group);
    [self.topicAPI startWithSuccess:^(YBNetworkResponse * _Nonnull response) {
        FBSLog(@"获取专题成功 : %@",response.responseObject);
        weakSelf.topicModel = [FBSTopicModel mj_objectWithKeyValues:response.responseObject];
        dispatch_group_leave(group);
        
    } failure:^(YBNetworkResponse * _Nonnull response) {
        FBSLog(@"获取专题失败 : %@",response.error.localizedDescription);
        dispatch_group_leave(group);
        
    }];
    
    
    //获取最新新闻
    dispatch_group_enter(group);
    self.topicListAPI.requestURI = [NSString stringWithFormat:@"topic/%@",self.topicId];
    
    [self.topicListAPI startWithSuccess:^(YBNetworkResponse * _Nonnull response) {
        FBSLog(@"获取专题相关成功 : %@",response.responseObject);
        weakSelf.topicList = [FBSTopicData mj_objectArrayWithKeyValuesArray:response.responseObject[@"data"]];
        dispatch_group_leave(group);
        
    } failure:^(YBNetworkResponse * _Nonnull response) {
        FBSLog(@"获取专题相关失败 : %@",response.error.localizedDescription);
        dispatch_group_leave(group);
        
    }];
    
    dispatch_group_notify(group, dispatch_get_main_queue(), ^{
        [weakSelf createItems];
        complete(YES);
    });
    
}



- (void)createItems {
    [self.items removeAllObjects];
    
    
    if (self.topicModel) {
        FBSTopicHeadItem *headItem = [[FBSTopicHeadItem alloc] init];
        headItem.title = self.topicModel.data.title;
        headItem.imgUrl = self.topicModel.data.file_url;
        headItem.bottomLineHidden = YES;
        headItem.cellHeight = UITableViewAutomaticDimension;
        [self.items addObject:headItem];
    }
    
    FBSGroupHeadItem *headItem = [FBSGroupHeadItem item];
    headItem.title = @"相关报道";
    [self.items addObject:headItem];
    
    for (FBSTopicData *info in self.topicList) {
        if (info.file_url.length) {
            FBSOneImageItem *oneImageItem = [FBSOneImageItem item];
            oneImageItem.detailType = DetailPageTypeArticle;
            oneImageItem.ID = info.ID;
            oneImageItem.content = info.title;
            oneImageItem.author = info.nickname;
            oneImageItem.imgUrl = info.file_url;
            oneImageItem.comment = @"";
            oneImageItem.time = [NSString publishedTimeFormatWithTimeStamp:info.created_at];
            oneImageItem.cellHeight = WScale(95);
            [self.items addObject:oneImageItem];
            
        } else {
            FBSOnlyTextItem *textItem = [FBSOnlyTextItem item];
            textItem.detailType = DetailPageTypeArticle;
            textItem.ID = info.ID;
            textItem.isTop = NO;
            textItem.content = info.title;
            textItem.author = info.nickname;
            textItem.comment = @"";
            textItem.time = [NSString publishedTimeFormatWithTimeStamp:info.created_at];
            textItem.cellHeight = [textItem calculateCellHeight];
            [self.items addObject:textItem];
        }
    }
    
    
    FBSSpaceItem *spaceItem = [FBSSpaceItem item];
    spaceItem.backgroundColor = Hexcolor(0xF6F7F9);
    spaceItem.cellHeight = WScale(10);
    [self.items addObject:spaceItem];
    
}



#pragma mark - property

- (FBSTopicAPI *)topicAPI {
    if (!_topicAPI) {
        _topicAPI  = [[FBSTopicAPI alloc] init];
    }
    return _topicAPI;
}


- (FBSGetRequest *)topicListAPI {
    if (!_topicListAPI) {
        _topicListAPI  = [[FBSGetRequest alloc] init];
    }
    return _topicListAPI;
}


@end
