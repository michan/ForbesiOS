//
//  FBSAuthorViewModel.h
//  Forbes
//
//  Created by 周灿华 on 2019/9/25.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSTableViewModel.h"
#import "FBSAuthorHeadCell.h"
#import "FBSOneImageCell.h"
#import "FBSOnlyTextCell.h"
#import "FBSGroupHeadCell.h"

@interface FBSAuthorViewModel : FBSTableViewModel

- (instancetype)initWithAuthorId:(NSString *)authorId;

- (void)rqAuthorData:(void(^)(BOOL success))complete;

- (void)rqIssue:(void(^)(BOOL success, NSInteger count))complete;

@end




