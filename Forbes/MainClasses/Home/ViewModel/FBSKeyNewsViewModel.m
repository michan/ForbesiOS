//
//  FBSKeyNewsViewModel.m
//  Forbes
//
//  Created by 周灿华 on 2019/9/8.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSKeyNewsViewModel.h"
#import "FBSHomeAPI.h"
#import "FBSTopNewsModel.h"
#import "FBSTopicModel.h"
#import "FBSKeyNewsModel.h"
#import "FBSRecommendModel.h"
#import "FBSHomeNewsModel.h"

@interface FBSKeyNewsViewModel ()

@property (nonatomic, strong) FBSTopNewsAPI *topNewsAPI;
@property (nonatomic, strong) FBSTopicAPI *topicAPI;
@property (nonatomic, strong) FBSKeyNewsAPI *keyNewsAPI;
@property (nonatomic, strong) FBSAllRecommendAPI *allRecommendAPI;
@property (nonatomic, strong) FBSAllArticlesAPI *allArticlesAPI;

@property (nonatomic, strong) FBSTopNewsModel *topNewsModel;
@property (nonatomic, strong) FBSTopicModel *topicModel;
@property (nonatomic, strong) FBSKeyNewsModel *keyNewsModel;
@property (nonatomic, strong) FBSRecommendModel *recommendModel;
@property (nonatomic, strong) FBSHomeNewsModel *allArticlesModel;

@property (nonatomic, copy) void(^complete)(BOOL);
@end

@implementation FBSKeyNewsViewModel

- (void)rqKeyNewsData:(void(^)(BOOL success))complete {
    self.complete = complete;
    
    dispatch_group_t group = dispatch_group_create();
    
    WEAKSELF
    
    //获取banner
//    dispatch_group_enter(group);
//    [self.allRecommendAPI startWithSuccess:^(YBNetworkResponse * _Nonnull response) {
//        FBSLog(@"获取要闻banner成功 : %@",response.responseObject);
//        weakSelf.recommendModel = [FBSRecommendModel mj_objectWithKeyValues:response.responseObject];
//        NSArray *sortArr = [weakSelf.recommendModel.data sortedArrayUsingComparator:^NSComparisonResult(FBSRecommendInfo *obj1, FBSRecommendInfo *obj2) {
//            return [obj1.article_id compare:obj2.article_id];
//        }];
//
//        weakSelf.recommendModel.data = sortArr;
//
//        dispatch_group_leave(group);
//
//    } failure:^(YBNetworkResponse * _Nonnull response) {
//        FBSLog(@"获取要闻banner失败 : %@",response.error.localizedDescription);
//        dispatch_group_leave(group);
//    }];
    
    
    //新的顶部推荐栏位
    dispatch_group_enter(group);
    [self.keyNewsAPI startWithSuccess:^(YBNetworkResponse * _Nonnull response) {
        FBSLog(@"获取首页推荐成功 : %@",response.responseObject);
        weakSelf.keyNewsModel = [FBSKeyNewsModel mj_objectWithKeyValues:response.responseObject];
        dispatch_group_leave(group);
        
    } failure:^(YBNetworkResponse * _Nonnull response) {
        FBSLog(@"获取首页推荐失败 : %@",response.error.localizedDescription);
        dispatch_group_leave(group);
        
    }];

    
    //获取置顶
    dispatch_group_enter(group);
    [self.topNewsAPI startWithSuccess:^(YBNetworkResponse * _Nonnull response) {
        FBSLog(@"获取要闻置顶成功 : %@",response.responseObject);
        weakSelf.topNewsModel = [FBSTopNewsModel mj_objectWithKeyValues:response.responseObject];
        dispatch_group_leave(group);
        
    } failure:^(YBNetworkResponse * _Nonnull response) {
        FBSLog(@"获取要闻置顶失败 : %@",response.error.localizedDescription);
        dispatch_group_leave(group);
        
    }];
    
    
    //获取专题
    dispatch_group_enter(group);
    [self.topicAPI startWithSuccess:^(YBNetworkResponse * _Nonnull response) {
        FBSLog(@"获取要闻专题成功 : %@",response.responseObject);
        weakSelf.topicModel = [FBSTopicModel mj_objectWithKeyValues:response.responseObject];
        dispatch_group_leave(group);
        
    } failure:^(YBNetworkResponse * _Nonnull response) {
        FBSLog(@"获取要闻专题失败 : %@",response.error.localizedDescription);
        dispatch_group_leave(group);
        
    }];


    //获取所有文章前30条数据
    dispatch_group_enter(group);
    self.allArticlesAPI.page = self.page;
    self.allArticlesAPI.limit = self.limit;
    [self.allArticlesAPI startWithSuccess:^(YBNetworkResponse * _Nonnull response) {
        FBSLog(@"获取30条数据成功 : %@",response.responseObject);
        weakSelf.allArticlesModel = [FBSHomeNewsModel mj_objectWithKeyValues:response.responseObject];

        dispatch_group_leave(group);
        
    } failure:^(YBNetworkResponse * _Nonnull response) {
        FBSLog(@"获取30条数据失败 : %@",response.error.localizedDescription);
        dispatch_group_leave(group);
        
    }];

    dispatch_group_notify(group, dispatch_get_main_queue(), ^{
        [weakSelf createItems];
        weakSelf.complete(YES);
    });
    
}

- (void)rqAllArticles:(void(^)(BOOL success, NSInteger count))complete {
    self.allArticlesAPI.page = self.page;
    self.allArticlesAPI.limit = self.limit;
    WEAKSELF
    [self.allArticlesAPI startWithSuccess:^(YBNetworkResponse * _Nonnull response) {
        
        FBSLog(@"获取所有文章列表成功 : %@",response.responseObject);
        weakSelf.allArticlesModel = [FBSHomeNewsModel mj_objectWithKeyValues:response.responseObject];
        [weakSelf createItems];
        
        if (complete) {
            complete(YES, weakSelf.allArticlesModel.items.count);
        }
    } failure:^(YBNetworkResponse * _Nonnull response) {
        if (complete) {
            complete(NO, 0);
        }
    }];
}


#pragma mark - 创建item

- (void)createItems {
    
    if (self.page == 1) {
        [self.items removeAllObjects];
        
        //创建banner
        NSMutableArray *infos  = [NSMutableArray array];
        
        ///旧的banner 数据
//        for (FBSRecommendInfo *recInfo in self.recommendModel.data) {
//            FBSBannerInfo *bannerInfo = [FBSBannerInfo new];
//
//            bannerInfo.ID = recInfo.article_id;
//            bannerInfo.title = recInfo.title;
//            bannerInfo.author = recInfo.nickname;
//            bannerInfo.time = @"";
//            bannerInfo.imgUrl = recInfo.file_url;
//            [infos addObject:bannerInfo];
//        }
        
        
        //新的banner数据
        FBSKeyNewsData *data = [self.keyNewsModel.data firstObject];
        for (FBSKeyNewsItem *news in data.items) {
            
            FBSBannerInfo *bannerInfo = [FBSBannerInfo new];
            bannerInfo.ID = news.relation_id;
            bannerInfo.title = news.title;
            bannerInfo.author = news.nickname;
            bannerInfo.time = @"";
            bannerInfo.imgUrl = news.file_url;
            [infos addObject:bannerInfo];
        }
        
        FBSBannerItem *bannerItem = [FBSBannerItem item];
        bannerItem.datas = [infos copy];
        [self.items addObject:bannerItem];
        
        
        //置顶数据
        if (self.topNewsModel.data) {
            for (FBSTopNewsInfo *info in self.topNewsModel.data) {
                
                FBSOnlyTextItem *topItem = [FBSOnlyTextItem item];
                topItem.detailType = DetailPageTypeArticle;
                topItem.ID = info.ID;
                topItem.isTop = YES;
                topItem.content = info.title;
                topItem.author = info.nickname;
                topItem.comment = @"";
                topItem.time = [NSString publishedTimeFormatWithTimeStamp:info.time];
                topItem.cellHeight = [topItem calculateCellHeight];
                [self.items addObject:topItem];
            }
        }
        
        //专题
        if (self.topicModel.data) {
            FBSOneImageItem *topicItem = [FBSOneImageItem item];
            topicItem.detailType = DetailPageTypeTopic;
            topicItem.ID = self.topicModel.data.ID;
            topicItem.content = self.topicModel.data.title;
            topicItem.author = @"";
            topicItem.imgUrl = self.topicModel.data.file_url;
            topicItem.comment = @"";
            topicItem.time = [NSString publishedTimeFormatWithTimeStamp:self.topicModel.data.created_at];
            topicItem.cellHeight = WScale(95);
            [self.items addObject:topicItem];
        }
        
//        //要闻推荐数据, 已经变更为banner数据 😢
//        FBSKeyNewsData *data = [self.keyNewsModel.data firstObject];
//        for (FBSKeyNewsItem *news in data.items) {
//            if (news.file_url.length) {
//                FBSOneImageItem *oneImageItem = [FBSOneImageItem item];
//                oneImageItem.detailType = DetailPageTypeArticle;
//                oneImageItem.ID = news.relation_id;
//                oneImageItem.content = news.title;
//                oneImageItem.author = news.nickname;
//                oneImageItem.imgUrl = news.file_url;
//                oneImageItem.comment = @"";
//                oneImageItem.time = @"";
//                oneImageItem.cellHeight = WScale(95);
//                [self.items addObject:oneImageItem];
//
//            } else {
//                FBSOnlyTextItem *textItem = [FBSOnlyTextItem item];
//                textItem.detailType = DetailPageTypeArticle;
//                textItem.ID = news.relation_id;
//                textItem.content = news.title;
//                textItem.author = news.nickname;
//                textItem.comment = @"";
//                textItem.time = @"";
//                textItem.cellHeight = [textItem calculateCellHeight];
//                [self.items addObject:textItem];
//            }
//        }
    }
    
    //所有文章列表
    for (FBSHomeNewsInfo *news in self.allArticlesModel.items) {
        if (news.file_url.length) {
            FBSOneImageItem *oneImageItem = [FBSOneImageItem item];
            oneImageItem.detailType = DetailPageTypeArticle;
            oneImageItem.ID = news.articleId;
            oneImageItem.content = news.title;
            oneImageItem.author = news.nickname;
            oneImageItem.imgUrl = news.file_url;
            oneImageItem.comment = @"";
            oneImageItem.time = [NSString publishedTimeFormatWithTimeStamp:news.updated_at];
            oneImageItem.cellHeight = WScale(95);
            [self.items addObject:oneImageItem];
            
        } else {
            FBSOnlyTextItem *textItem = [FBSOnlyTextItem item];
            textItem.detailType = DetailPageTypeArticle;
            textItem.ID = news.articleId;
            textItem.content = news.title;
            textItem.author = news.nickname;
            textItem.comment = @"";
            textItem.time = [NSString publishedTimeFormatWithTimeStamp:news.updated_at];
            textItem.cellHeight = [textItem calculateCellHeight];
            [self.items addObject:textItem];
        }
    }

}



#pragma mark - property

- (FBSTopNewsAPI *)topNewsAPI {
    if (!_topNewsAPI) {
        _topNewsAPI  = [[FBSTopNewsAPI alloc] init];
    }
    return _topNewsAPI;
}

- (FBSTopicAPI *)topicAPI {
    if (!_topicAPI) {
        _topicAPI  = [[FBSTopicAPI alloc] init];
    }
    return _topicAPI;
}


- (FBSKeyNewsAPI *)keyNewsAPI {
    if (!_keyNewsAPI) {
        _keyNewsAPI  = [[FBSKeyNewsAPI alloc] init];
    }
    return _keyNewsAPI;
}

- (FBSAllRecommendAPI *)allRecommendAPI {
    if (!_allRecommendAPI) {
        _allRecommendAPI  = [[FBSAllRecommendAPI alloc] init];
    }
    return _allRecommendAPI;
}


- (FBSAllArticlesAPI *)allArticlesAPI {
    if (!_allArticlesAPI) {
        _allArticlesAPI  = [[FBSAllArticlesAPI alloc] init];
    }
    return _allArticlesAPI;
}


@end
