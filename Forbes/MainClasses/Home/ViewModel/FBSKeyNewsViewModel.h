//
//  FBSKeyNewsViewModel.h
//  Forbes
//
//  Created by 周灿华 on 2019/9/8.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSTableViewModel.h"
#import "FBSOneImageCell.h"
#import "FBSThreeImageCell.h"
#import "FBSOnlyTextCell.h"

#import "FBSBannerCell.h"
#import "FBSAdvertCell.h"


@interface FBSKeyNewsViewModel : FBSTableViewModel

- (void)rqKeyNewsData:(void(^)(BOOL success))complete;

- (void)rqAllArticles:(void(^)(BOOL success, NSInteger count))complete;

@end

