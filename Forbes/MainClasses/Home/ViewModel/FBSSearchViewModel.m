//
//  FBSSearchViewModel.m
//  Forbes
//
//  Created by 周灿华 on 2019/9/25.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSSearchViewModel.h"
#import "FBSHomeAPI.h"
#import "FBSSearchModel.h"

@interface FBSSearchViewModel ()
{
    NSArray *_tags;
}
@property (nonatomic, copy) void(^searchComplete)(BOOL success,NSInteger count);
@property (nonatomic, copy) NSString *keyword;
@property (nonatomic, strong) FBSSearchAPI *searchAPI;
@property (nonatomic, strong) FBSSearchModel *searchModel;
@end

@implementation FBSSearchViewModel

- (void)makeItems {
    [self.items addObject:self.tagsItem];
}


#pragma mark - pribat mehtod

- (void)clearInputKeyWord {
    [self.items removeAllObjects];
    [self.items addObject:self.tagsItem];
}

- (void)rqToSearch:(NSString *)keyword complete:(void(^)(BOOL success,NSInteger count))complete {
    
    if (!keyword.length) {
        return;
    }
    self.keyword = keyword;
    self.searchComplete = complete;
    
    self.searchAPI.keyword = keyword;
    self.searchAPI.page = self.page;
    self.searchAPI.limit = self.limit;
    
    WEAKSELF
    [self.searchAPI startWithSuccess:^(YBNetworkResponse * _Nonnull response) {
        FBSLog(@"搜索成功 : %@",response.responseObject);
        weakSelf.searchModel = [FBSSearchModel mj_objectWithKeyValues:response.responseObject];
        [weakSelf createItems];
        
        if (complete) {
            complete(YES, weakSelf.searchModel.items.count);
        }
        
    } failure:^(YBNetworkResponse * _Nonnull response) {
        FBSLog(@"搜索失败 : %@",response.error.localizedDescription);
        if (complete) {
            complete(NO, 0);
        }

    }];
}


- (void)createItems {
    if (self.page == 1) {
        [self.items removeAllObjects];
    }
    
    //最新新闻列表
    for (FBSSearchResult *result in self.searchModel.items) {
        if (result.file_url.length) {
            FBSOneImageItem *oneImageItem = [FBSOneImageItem item];
            oneImageItem.detailType = [self getDetailPageType:result.relation_type];
            oneImageItem.ID = result.relation_id;
            oneImageItem.videoUrl = result.video_url;
            oneImageItem.content = result.title;
            oneImageItem.author = result.nickname;
            oneImageItem.imgUrl = result.file_url;
            oneImageItem.comment = @"";
            oneImageItem.time = [NSString publishedTimeFormatWithTimeStamp:result.time];
            oneImageItem.keyword = self.searchAPI.keyword;
            oneImageItem.cellHeight = WScale(95);
            [self.items addObject:oneImageItem];
            
        } else {
            FBSOnlyTextItem *textItem = [FBSOnlyTextItem item];
            textItem.detailType = [self getDetailPageType:result.relation_type];
            textItem.ID = result.relation_id;
            textItem.videoUrl = result.video_url;
            textItem.content = result.title;
            textItem.author = result.nickname;
            textItem.comment = @"";
            textItem.time = [NSString publishedTimeFormatWithTimeStamp:result.time];
            textItem.keyword = self.searchAPI.keyword;
            textItem.cellHeight = [textItem calculateCellHeight];
            [self.items addObject:textItem];
        }
    }
    
    if (self.items.count == 0) {
        [self.items addObject:self.tagsItem];
    }
}



- (DetailPageType)getDetailPageType:(NSString *)relation_type {
    DetailPageType detailType = DetailPageTypeArticle;
    
    if ([relation_type isEqualToString:@"1"]) { //视频
        detailType = DetailPageTypeVideo;
        
    } else if ([relation_type isEqualToString:@"2"]) { //榜单
        detailType = DetailPageTypeRank;
        
    } else if ([relation_type isEqualToString:@"3"]) {  //活动
        detailType = DetailPageTypeActivity;
        
    } else if ([relation_type isEqualToString:@"4"]) { //文章
        detailType = DetailPageTypeArticle;
    }
    
    return detailType;
}


- (void)setTags:(NSArray *)tags {
    _tags = tags;
    
    self.tagsItem.tags = tags;
}

#pragma mark - property


- (FBSSearchAPI *)searchAPI {
    if (!_searchAPI) {
        _searchAPI  = [[FBSSearchAPI alloc] init];
    }
    return _searchAPI;
}


- (NSArray *)tags {
    if (!_tags) {
        NSString *savaPath = SaveKeywordFilePath;
        NSMutableArray *keywordArr = [[NSArray arrayWithContentsOfFile:savaPath] mutableCopy];
        
        if (!keywordArr) {
            keywordArr = [NSMutableArray array];
        }
        _tags = [keywordArr copy];;
    }
    return _tags;
}

- (FBSSearchTagsItem *)tagsItem {
    if (!_tagsItem) {
        _tagsItem = [FBSSearchTagsItem new];
        _tagsItem.tags = self.tags;
        _tagsItem.cellHeight = [_tagsItem calculateCellHeight];
    }
    return _tagsItem;
}

@end

