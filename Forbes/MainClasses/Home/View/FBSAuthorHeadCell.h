//
//  FBSAuthorHeadCell.h
//  Forbes
//
//  Created by 周灿华 on 2019/9/29.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSCell.h"

@interface FBSAuthorHeadItem : FBSItem
@property (nonatomic, copy) NSString *imgUrl;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *detail;
@property (nonatomic, copy) NSString *content;

@end


@interface FBSAuthorHeadCell : FBSCell

@end
