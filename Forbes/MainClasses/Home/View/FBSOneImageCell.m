//
//  FBSOneImageCell.m
//  Forbes
//
//  Created by 周灿华 on 2019/7/26.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSOneImageCell.h"
#import "FBSKeyNewsModel.h"

#define ImgW ((kScreenWidth - 2 * WScale(10) - 2 * WScale(5)) / 3.0)
#define ImgH  WScale(75)

@implementation FBSOneImageItem

- (void)ctreatItemData:(FBSBaseModel *)model{
    if ([model isKindOfClass:[FBSKeyNewsItem class]]) {
        self.content = @"“王室”帽子救不了通灵珠宝，沈东军的钻石了通灵珠宝，沈东军的钻石";
        self.author = @"福布斯";
        self.comment = @"111";
        self.time = @"1天";
        self.cellHeight = WScale(95);
    }
}

@end



@interface FBSOneImageCell ()
@property (nonatomic, strong) UILabel *contenL;
@property (nonatomic, strong) UIImageView *imgV;
@property (nonatomic, strong) UILabel *descL;
@property (nonatomic, strong) UIImageView *playIcon;
@property (nonatomic, strong) UILabel *flagL;   //专题标识
@property (nonatomic, strong) NSDictionary *attributeDic;

@end

@implementation FBSOneImageCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self.contentView addSubview:self.contenL];
        [self.contentView addSubview:self.descL];
        [self.contentView addSubview:self.imgV];
        [self.imgV addSubview:self.playIcon];
        
        [self.contenL mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(WScale(10));
            make.top.mas_equalTo(self.imgV);
            make.right.mas_equalTo(self.imgV.mas_left).offset(-WScale(10));
        }];
        
        [self.descL mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.contenL);
            make.bottom.mas_equalTo(self.imgV);
            make.height.mas_equalTo(WScale(11));
        }];
        self.imgV.clipsToBounds = YES;
        self.imgV.contentMode = UIViewContentModeScaleAspectFill;
        [self.imgV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.mas_equalTo(ImgW);
            make.height.mas_equalTo(ImgH);
            make.right.mas_equalTo(-WScale(10));
            make.centerY.mas_equalTo(0);
        }];
        
        [self.playIcon mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.height.mas_equalTo(WScale(35));
            make.centerX.centerY.mas_equalTo(0);
        }];
        
    }
    
    return self;
}



- (void)setItem:(FBSOneImageItem *)item {
    if (![item isKindOfClass:[FBSOneImageItem class]]) {
        return ;
    }
    
    [super setItem:item];
    
    
    NSMutableAttributedString *attStr = [[NSMutableAttributedString alloc] initWithString:item.content attributes:self.attributeDic];
    
    if (item.detailType == DetailPageTypeTopic) {  //专题标志
        // 创建图片附件
        NSTextAttachment *attach = [[NSTextAttachment alloc] init];
        attach.image = [CommonTool makeTopicImage];
        attach.bounds = CGRectMake(0, -1, WScale(35), WScale(15));
        NSAttributedString *attachString = [NSAttributedString attributedStringWithAttachment:attach];
        
        //将图片插入到合适的位置
        [attStr insertAttributedString:attachString atIndex:0];
        [attStr insertAttributedString:[[NSAttributedString alloc] initWithString:@"  "] atIndex:1];
    }
    
    self.contenL.attributedText = [attStr copy];
    self.contenL.lineBreakMode = NSLineBreakByTruncatingTail;

    if (item.imgUrl.length) {
        [self.imgV sd_setImageWithURL:[NSURL URLWithString:item.imgUrl]];
    } else {
        self.imgV.image = nil;
    }
    
    if (item.time.length) {
        if (item.author.length) {
            self.descL.text = [NSString stringWithFormat:@"%@  %@",item.author,item.time];
        } else {
            self.descL.text = item.time;
        }
    } else {
        self.descL.text = item.author;
    }
    
    self.playIcon.hidden = item.detailType != DetailPageTypeVideo;
}

#pragma mark - property


- (UILabel *)contenL {
    if (!_contenL) {
        _contenL  = [[UILabel alloc] init];
        _contenL.textColor = Hexcolor(0x333333);
        _contenL.font = kLabelFontSize18;
        _contenL.numberOfLines = 2;
    }
    return _contenL;
}

- (UILabel *)descL {
    if (!_descL) {
        _descL  = [[UILabel alloc] init];
        _descL.textColor = Hexcolor(0x666666);
        _descL.font = kLabelFontSize11;
    }
    return _descL;
}

- (UIImageView *)imgV {
    if (!_imgV) {
        _imgV  = [[UIImageView alloc] init];
        _imgV.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5];
    }
    return _imgV;
}

- (UIImageView *)playIcon {
    if (!_playIcon) {
        _playIcon  = [[UIImageView alloc] init];
        _playIcon.image = kImageWithName(@"video_listplay");
        _playIcon.hidden = YES;
    }
    return _playIcon;
}

- (NSDictionary *)attributeDic {
    if (!_attributeDic) {
        NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc] init];
        style.lineSpacing = 5;
        
        _attributeDic  = @{
                           NSForegroundColorAttributeName : Hexcolor(0x333333),
                           NSFontAttributeName : kLabelFontSize18,
                           NSParagraphStyleAttributeName : style
                           };
    }
    return _attributeDic;
}


@end
