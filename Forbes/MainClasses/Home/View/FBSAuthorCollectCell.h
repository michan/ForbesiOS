//
//  FBSAuthorCollectCell.h
//  Forbes
//
//  Created by 周灿华 on 2019/9/29.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSCell.h"
#import "FBSBrandvoiceModel.h"

@interface FBSAuthorCollectItem : FBSItem
@property (nonatomic, strong) NSArray<FBSBrandUser *> *brand_users;
@end

@interface FBSAuthorCollectCell : FBSCell

@end
