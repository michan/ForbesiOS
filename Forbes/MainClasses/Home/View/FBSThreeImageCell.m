//
//  FBSThreeImageCell.m
//  Forbes
//
//  Created by 周灿华 on 2019/7/26.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSThreeImageCell.h"
#import "FBSKeyNewsModel.h"

#define EdgeMargin WScale(10)
#define Margin     WScale(5)
#define ImgW       ((kScreenWidth - 2 * EdgeMargin - 2 * Margin) / 3.0)
#define ImgH       WScale(75)


@implementation FBSThreeImageItem

- (CGFloat)calculateCellHeight {
    CGFloat cellHeight = WScale(10);
    
    NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc] init];
    style.lineSpacing = 5;
    
    NSDictionary *dict = @{
                           NSFontAttributeName : kLabelFontSize18,
                           NSParagraphStyleAttributeName : style
                           };

    
    if (self.content.length) {
        cellHeight += [self.content boundingRectWithSize:CGSizeMake(kScreenWidth  - 2 * WScale(10), MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:dict context:NULL].size.height;
    }
    
    cellHeight += WScale(10) + ImgH + WScale(10) + WScale(11) +  WScale(10);
    
    return cellHeight;
    
}
- (void)ctreatItemData:(FBSBaseModel *)model{
    if ([model isKindOfClass:[FBSKeyNewsItem class]]) {
        FBSThreeImageItem *threeImgeItem1 = [FBSThreeImageItem item];
        self.content = @"震撼消息!";
        self.author = @"娱人记";
        self.comment = @"200";
        self.time = @"1小时";
        self.cellHeight = [self calculateCellHeight];
    }
}


@end


@interface FBSThreeImageCell ()
@property (nonatomic, strong) UILabel *contenL;
@property (nonatomic, strong) UIImageView *imgV1;
@property (nonatomic, strong) UIImageView *imgV2;
@property (nonatomic, strong) UIImageView *imgV3;
@property (nonatomic, strong) UILabel *descL;
@property (nonatomic, strong) NSDictionary *attributeDic;

@end

@implementation FBSThreeImageCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self.contentView addSubview:self.contenL];
        [self.contentView addSubview:self.imgV1];
        [self.contentView addSubview:self.imgV2];
        [self.contentView addSubview:self.imgV3];
        [self.contentView addSubview:self.descL];
        
        [self.contenL mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(WScale(10));
            make.top.mas_equalTo(WScale(10));
            make.right.mas_equalTo(-WScale(10));
        }];
        

        [self.imgV1 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.contenL.mas_bottom).offset(WScale(10));
            make.left.mas_equalTo(EdgeMargin);
            make.width.mas_equalTo(ImgW);
            make.height.mas_equalTo(ImgH);
        }];
        
        
        [self.imgV2 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.imgV1);
            make.left.mas_equalTo(self.imgV1.mas_right).offset(Margin);
            make.width.mas_equalTo(ImgW);
            make.height.mas_equalTo(ImgH);
        }];

        
        [self.imgV3 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.imgV1);
            make.left.mas_equalTo(self.imgV2.mas_right).offset(Margin);
            make.width.mas_equalTo(ImgW);
            make.height.mas_equalTo(ImgH);
        }];

        
        [self.descL mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.imgV1);
            make.top.mas_equalTo(self.imgV1.mas_bottom).offset(WScale(10));
            make.height.mas_equalTo(WScale(11));
        }];

        
    }
    
    return self;
}



- (void)setItem:(FBSThreeImageItem *)item {
    if (![item isKindOfClass:[FBSThreeImageItem class]]) {
        return ;
    }
    
    [super setItem:item];
    self.contenL.attributedText = [[NSAttributedString alloc] initWithString:item.content attributes:self.attributeDic];
    self.contenL.lineBreakMode = NSLineBreakByTruncatingTail;
    self.descL.text = [NSString stringWithFormat:@"%@ %@评论 %@前",item.author,item.comment,item.time];
    
}

#pragma mark - property

- (UILabel *)contenL {
    if (!_contenL) {
        _contenL  = [[UILabel alloc] init];
        _contenL.textColor = Hexcolor(0x333333);
        _contenL.font = kLabelFontSize18;
        _contenL.numberOfLines = 2;
    }
    return _contenL;
}

- (UILabel *)descL {
    if (!_descL) {
        _descL  = [[UILabel alloc] init];
        _descL.textColor = Hexcolor(0x666666);
        _descL.font = kLabelFontSize11;
    }
    return _descL;
}

- (UIImageView *)imgV1 {
    if (!_imgV1) {
        _imgV1  = [[UIImageView alloc] init];
        _imgV1.backgroundColor = RandomColor;
    }
    return _imgV1;
}


- (UIImageView *)imgV2 {
    if (!_imgV2) {
        _imgV2  = [[UIImageView alloc] init];
        _imgV2.backgroundColor = RandomColor;
    }
    return _imgV2;
}


- (UIImageView *)imgV3 {
    if (!_imgV3) {
        _imgV3  = [[UIImageView alloc] init];
        _imgV3.backgroundColor = RandomColor;
    }
    return _imgV3;
}

- (NSDictionary *)attributeDic {
    if (!_attributeDic) {
        NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc] init];
        style.lineSpacing = 5;
        
        _attributeDic  = @{
                           NSForegroundColorAttributeName : Hexcolor(0x333333),
                           NSFontAttributeName : kLabelFontSize18,
                           NSParagraphStyleAttributeName : style
                           };
    }
    return _attributeDic;
}


@end
