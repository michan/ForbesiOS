//
//  FBSAuthorCollectCell.m
//  Forbes
//
//  Created by 周灿华 on 2019/9/29.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSAuthorCollectCell.h"

@implementation FBSAuthorCollectItem

- (instancetype)init {
    self = [super init];
    if (self) {
        self.backgroundColor = RGBA(246,247,249,1);
        self.bottomLineHidden = YES;
    }
    return self;
}

@end


@interface FBSAuthorEntryCell : UICollectionViewCell
@property (nonatomic, strong) UIImageView *iconV;
@property (nonatomic, strong) UILabel *nameL;
@end


@implementation FBSAuthorEntryCell


- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.contentView.backgroundColor = [UIColor clearColor];
        [self.contentView addSubview:self.iconV];
        [self.contentView addSubview:self.nameL];
        
        [self.iconV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(0);
            make.centerX.mas_equalTo(0);
            make.width.height.mas_equalTo(WScale(60));
        }];
        
        [self.nameL mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.iconV.mas_bottom).offset(WScale(10));
            make.left.right.mas_equalTo(0);
            make.centerX.mas_equalTo(0);
            make.height.mas_equalTo(WScale(13));
        }];
        
    }
    return self;
}


- (UIImageView *)iconV {
    if (!_iconV) {
        _iconV  = [[UIImageView alloc] init];
        _iconV.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5];
        _iconV.layer.cornerRadius = WScale(30);
        _iconV.layer.masksToBounds = YES;
    }
    return _iconV;
}


- (UILabel *)nameL {
    if (!_nameL) {
        _nameL = [[UILabel alloc] init];
        _nameL.font = kLabelFontSize13;
        _nameL.textColor = Hexcolor(0x999999);
        _nameL.text = @"-";
        _nameL.textAlignment = NSTextAlignmentCenter;
//        _nameL.adjustsFontSizeToFitWidth = YES;
    }
    return _nameL;
}


@end


@interface FBSAuthorCollectCell ()<UICollectionViewDataSource,UICollectionViewDelegate>
{
    FBSAuthorCollectItem *_currentItem;
}
@property (nonatomic, strong) UICollectionView *collectionView;

@end

@implementation FBSAuthorCollectCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self.contentView addSubview:self.collectionView];
        
        [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(0);
            make.left.right.mas_equalTo(0);
            make.height.mas_equalTo(self);
        }];
    }
    
    return self;
}



- (void)setItem:(FBSAuthorCollectItem *)item {
    if (![item isKindOfClass:[FBSAuthorCollectItem class]]) {
        return ;
    }
    
    [super setItem:item];
    _currentItem = item;
    [self.collectionView reloadData];
}


#pragma mark - UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return _currentItem.brand_users.count;
}


static NSString * const CELLID = @"FBSAuthorEntryCell";

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    FBSAuthorEntryCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:CELLID forIndexPath:indexPath];
    
    if (indexPath.row < _currentItem.brand_users.count) {
        FBSBrandUser *user = [_currentItem.brand_users objectAtIndex:indexPath.row];
        [cell.iconV sd_setImageWithURL:[NSURL URLWithString:user.file_url]];
        cell.nameL.text = user.nickname;
    }
    return cell;
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (self.delegate && [self.delegate respondsToSelector:@selector(fbsCell:didClickButtonAtIndex:)]) {
        [self.delegate fbsCell:self didClickButtonAtIndex:indexPath.item];
    }
}

#pragma mark - property

- (UICollectionView *)collectionView {
    if (!_collectionView) {
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
        
        NSInteger column = 4;
        CGFloat margin = WScale(10);
        CGFloat itemW = (kScreenWidth - (column + 1) * margin) / column;
        layout.sectionInset = UIEdgeInsetsMake(margin, margin, margin, margin);
        layout.itemSize = CGSizeMake(itemW, WScale(85));
        layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        layout.minimumLineSpacing = margin;
        layout.minimumInteritemSpacing = 0;
        
        _collectionView = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:layout];
        _collectionView.backgroundColor = [UIColor whiteColor];
        _collectionView.dataSource = self;
        _collectionView.delegate = self;
        
        _collectionView.showsHorizontalScrollIndicator = NO;
        _collectionView.showsVerticalScrollIndicator = NO;
        [_collectionView registerClass:[FBSAuthorEntryCell class] forCellWithReuseIdentifier:CELLID];
        [self addSubview:_collectionView];
        
    }
    return _collectionView;
}

@end
