//
//  FBSEntityInfoView.m
//  Forbes
//
//  Created by 周灿华 on 2019/8/24.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSEntityInfoView.h"


@implementation FBSEntityInfoView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self addSubview:self.iconV];
        [self addSubview:self.nameL];
        [self addSubview:self.descL];
        [self addSubview:self.followBtn];
        
        [self.iconV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(WScale(20));
            make.left.mas_equalTo(WScale(10));
            make.width.height.mas_equalTo(WScale(34));
        }];
        
        [self.nameL mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.iconV);
            make.left.mas_equalTo(self.iconV.mas_right).offset(WScale(8));
            make.height.mas_equalTo(WScale(12));
        }];
        
        
        [self.descL mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.mas_equalTo(self.iconV);
            make.left.mas_equalTo(self.nameL);
            make.height.mas_equalTo(WScale(11));
        }];
        
        [self.followBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(-WScale(10));
            make.width.mas_equalTo(WScale(64));
            make.height.mas_equalTo(WScale(22));
            make.centerY.mas_equalTo(self.iconV);
        }];
    }
    return self;
}


#pragma mark - property

- (UIImageView *)iconV {
    if (!_iconV) {
        _iconV  = [[UIImageView alloc] init];
        _iconV.backgroundColor = RandomColor;
        _iconV.layer.cornerRadius = WScale(17);
        _iconV.layer.masksToBounds = YES;
    }
    return _iconV;
}


- (UILabel *)nameL {
    if (!_nameL) {
        _nameL  = [[UILabel alloc] init];
        _nameL.textColor = RGB(51, 51, 51);
        _nameL.font = kLabelBoldFontSize(12);
    }
    return _nameL;
}


- (UILabel *)descL {
    if (!_descL) {
        _descL  = [[UILabel alloc] init];
        _descL.textColor = RGBA(153,153,153,1);
        _descL.font = kLabelFontSize11;
    }
    return _descL;
}


- (UIButton *)followBtn {
    if (!_followBtn) {
        _followBtn  = [UIButton buttonWithType:UIButtonTypeCustom];
        [_followBtn setBackgroundImage:kImageWithName(@"follow_unsel") forState:UIControlStateNormal];
        [_followBtn setBackgroundImage:kImageWithName(@"follow_sel") forState:UIControlStateSelected];
    }
    return _followBtn;
}


@end
