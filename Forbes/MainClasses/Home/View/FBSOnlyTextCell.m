//
//  FBSOnlyTextCell.m
//  Forbes
//
//  Created by 周灿华 on 2019/7/26.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSOnlyTextCell.h"

#import "FBSKeyNewsModel.h"


@implementation FBSOnlyTextItem

- (CGFloat)calculateCellHeight {
    CGFloat cellHeight = WScale(10);
    
    NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc] init];
    style.lineSpacing = 5;
    
    NSDictionary *dict = @{
                           NSFontAttributeName : kLabelFontSize18,
                           NSParagraphStyleAttributeName : style
                           };
    
    
    if (self.content.length) {
        cellHeight += [self.content boundingRectWithSize:CGSizeMake(kScreenWidth  - 2 * WScale(10), MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:dict context:NULL].size.height;
    }
    
    cellHeight += WScale(13) + WScale(15) + WScale(10);
    
    return cellHeight;

}
- (void)ctreatItemData:(FBSBaseModel *)model
{
    if ([model isKindOfClass:[FBSKeyNewsItem class]]) {
        FBSKeyNewsItem *item = (FBSKeyNewsItem *)model;
        self.content = item.title;
        self.author = item.nickname;
        self.comment = @"-";
        self.time = @"-";
        self.cellHeight = [self calculateCellHeight];
    }
}
@end



@interface FBSOnlyTextCell ()
@property (nonatomic, strong) UILabel *contenL;
@property (nonatomic, strong) UILabel *flagL;
@property (nonatomic, strong) UILabel *descL;
@property (nonatomic, strong) NSDictionary *attributeDic;

@end

@implementation FBSOnlyTextCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self.contentView addSubview:self.contenL];
        [self.contentView addSubview:self.flagL];
        [self.contentView addSubview:self.descL];
        
        [self.contenL mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(WScale(10));
            make.top.mas_equalTo(WScale(10));
            make.right.mas_equalTo(-WScale(10));
        }];
        
        [self.flagL mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.contenL);
            make.centerY.mas_equalTo(self.descL);
            make.width.mas_equalTo(WScale(35));
            make.height.mas_equalTo(WScale(15));
        }];

        [self.descL mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.contenL.mas_bottom).offset(WScale(13));
            make.left.mas_equalTo(self.flagL.mas_right).offset(WScale(10));
            make.height.mas_equalTo(WScale(11));
        }];
        
    }
    
    return self;
}



- (void)setItem:(FBSOnlyTextItem *)item {
    if (![item isKindOfClass:[FBSOnlyTextItem class]]) {
        return ;
    }
    
    [super setItem:item];
    self.contenL.attributedText = [[NSAttributedString alloc] initWithString:item.content attributes:self.attributeDic];
    self.contenL.lineBreakMode = NSLineBreakByTruncatingTail;
    
    if (item.time.length) {
        if (item.author.length) {
            self.descL.text = [NSString stringWithFormat:@"%@  %@",item.author,item.time];
        } else {
            self.descL.text = item.time;
        }
    } else {
        self.descL.text = item.author;
    }

    
    self.flagL.hidden = !item.isTop;
    if (item.isTop) {
        [self.descL mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.contenL.mas_bottom).offset(WScale(13));
            make.left.mas_equalTo(self.flagL.mas_right).offset(WScale(10));
            make.height.mas_equalTo(WScale(11));
        }];
        
    } else {
        [self.descL mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.contenL.mas_bottom).offset(WScale(13));
            make.left.mas_equalTo(self.contenL);
            make.height.mas_equalTo(WScale(11));
        }];
    }
    
    
}

#pragma mark - property

- (UILabel *)contenL {
    if (!_contenL) {
        _contenL  = [[UILabel alloc] init];
        _contenL.textColor = Hexcolor(0x333333);
        _contenL.font = kLabelFontSize18;
        _contenL.numberOfLines = 2;
    }
    return _contenL;
}

- (UILabel *)flagL {
    if (!_flagL) {
        _flagL  = [[UILabel alloc] init];
        _flagL.font = kLabelFontSize11;
        _flagL.textAlignment = NSTextAlignmentCenter;
        _flagL.textColor = MainThemeColor;
        _flagL.text = @"置顶";
        _flagL.layer.cornerRadius = 3;
        _flagL.layer.borderColor = _flagL.textColor.CGColor;
        _flagL.layer.borderWidth = 1;
    }
    return _flagL;
}


- (UILabel *)descL {
    if (!_descL) {
        _descL  = [[UILabel alloc] init];
        _descL.textColor = Hexcolor(0x666666);
        _descL.font = kLabelFontSize11;
    }
    return _descL;
}



- (NSDictionary *)attributeDic {
    if (!_attributeDic) {
        NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc] init];
        style.lineSpacing = 5;
        
        _attributeDic  = @{
                           NSForegroundColorAttributeName : Hexcolor(0x333333),
                           NSFontAttributeName : kLabelFontSize18,
                           NSParagraphStyleAttributeName : style
                           };
    }
    return _attributeDic;
}


@end
