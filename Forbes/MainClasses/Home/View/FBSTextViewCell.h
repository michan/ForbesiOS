//
//  FBSTextViewCell.h
//  Forbes
//
//  Created by 赵志辉 on 2019/9/25.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import "FBSCell.h"
#import "FBSluRuViewController.h"

NS_ASSUME_NONNULL_BEGIN
@protocol ACEExpandableTableViewDelegate <UITableViewDelegate, UITextViewDelegate>

@required
- (void)tableView:(UITableView *)tableView updatedText:(NSString *)text atIndexPath:(NSIndexPath *)indexPath;

@optional
- (void)tableView:(UITableView *)tableView updatedHeight:(CGFloat)height atIndexPath:(NSIndexPath *)indexPath;
- (BOOL)tableView:(UITableView *)tableView textView:(UITextView*)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text;
- (void)tableView:(UITableView *)tableView textViewDidChangeSelection:(UITextView*)textView;
- (void)tableView:(UITableView *)tableView textViewDidEndEditing:(UITextView*)textView;
@end

@interface FBSTextViewCell : FBSCell

@property (nonatomic, weak) FBSluRuViewController *vc;

@property (nonatomic, weak) UITableView *expandableTableView;
@property (nonatomic, strong) SZTextView *textView;

@property (nonatomic, readonly) CGFloat cellHeight;
@property (nonatomic, strong) NSString *text;

-(void)updateTextViewHeight; // Call to update the textView height (useful for viewdidload)

@end

NS_ASSUME_NONNULL_END
