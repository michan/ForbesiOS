//
//  FBSFollowOneImageCell.m
//  Forbes
//
//  Created by 周灿华 on 2019/8/24.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSFollowOneImageCell.h"
#import "FBSEntityInfoView.h"

#define ImgW ((kScreenWidth - 2 * WScale(10) - 2 * WScale(5)) / 3.0)
#define ImgH  WScale(75)


@implementation FBSFollowOneImageItem

@end



@interface FBSFollowOneImageCell ()
@property (nonatomic, strong) FBSEntityInfoView *infoView;
@property (nonatomic, strong) UILabel *contenL;
@property (nonatomic, strong) UIImageView *imgV;
@property (nonatomic, strong) NSDictionary *attributeDic;

@end

@implementation FBSFollowOneImageCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self.contentView addSubview:self.infoView];
        [self.contentView addSubview:self.contenL];
        [self.contentView addSubview:self.imgV];
        
        [self.infoView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.left.right.mas_equalTo(0);
            make.height.mas_equalTo(WScale(70));
        }];

        
        [self.contenL mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(WScale(10));
            make.top.mas_equalTo(self.imgV);
            make.right.mas_equalTo(self.imgV.mas_left).offset(-WScale(10));
        }];
        
        [self.imgV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.infoView.mas_bottom);
            make.width.mas_equalTo(ImgW);
            make.height.mas_equalTo(ImgH);
            make.right.mas_equalTo(-WScale(10));
        }];
        
    }
    
    return self;
}


- (void)setItem:(FBSFollowOneImageItem *)item {
    if (![item isKindOfClass:[FBSFollowOneImageItem class]]) {
        return ;
    }
    
    [super setItem:item];
    self.infoView.nameL.text = item.author;
    self.infoView.descL.text = item.desc;
    self.infoView.followBtn.selected = item.hasFollow;
    
    self.contenL.attributedText = [[NSAttributedString alloc] initWithString:item.content attributes:self.attributeDic];
    self.contenL.lineBreakMode = NSLineBreakByTruncatingTail;
    
}

#pragma mark - property

- (FBSEntityInfoView *)infoView {
    if (!_infoView) {
        _infoView  = [[FBSEntityInfoView alloc] init];
    }
    return _infoView;
}


- (UILabel *)contenL {
    if (!_contenL) {
        _contenL  = [[UILabel alloc] init];
        _contenL.textColor = Hexcolor(0x333333);
        _contenL.font = kLabelFontSize18;
        _contenL.numberOfLines = 2;
    }
    return _contenL;
}


- (UIImageView *)imgV {
    if (!_imgV) {
        _imgV  = [[UIImageView alloc] init];
        _imgV.backgroundColor = RandomColor;
    }
    return _imgV;
}

- (NSDictionary *)attributeDic {
    if (!_attributeDic) {
        NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc] init];
        style.lineSpacing = 5;
        
        _attributeDic  = @{
                           NSForegroundColorAttributeName : Hexcolor(0x333333),
                           NSFontAttributeName : kLabelFontSize18,
                           NSParagraphStyleAttributeName : style
                           };
    }
    return _attributeDic;
}


@end
