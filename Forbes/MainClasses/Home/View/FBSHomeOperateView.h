//
//  FBSHomeOperateView.h
//  Forbes
//
//  Created by 周灿华 on 2019/8/24.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import <UIKit/UIKit.h>

#define PraiseIndex  1000
#define CommentIndex 1001
#define MoreIndex    1002


NS_ASSUME_NONNULL_BEGIN

@interface FBSHomeOperateView : UIView
@property (nonatomic, strong) UIButton *praiseBtn;
@property (nonatomic, strong) UIButton *commentBtn;
@property (nonatomic, strong) UIButton *shareBtn;

@end

NS_ASSUME_NONNULL_END
