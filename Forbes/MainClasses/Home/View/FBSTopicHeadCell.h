//
//  FBSTopicHeadCell.h
//  Forbes
//
//  Created by 周灿华 on 2019/9/29.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSCell.h"

@interface FBSTopicHeadItem : FBSItem
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *imgUrl;
@end

@interface FBSTopicHeadCell : FBSCell

@end
