//
//  FBSFollowListCell.m
//  Forbes
//
//  Created by 周灿华 on 2019/8/24.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSFollowListCell.h"

@implementation FBSFollowListItem

@end



@interface FBSFollowListCell ()
@property (nonatomic, strong) UIImageView *iconV;
@property (nonatomic, strong) UILabel *nameL;
@property (nonatomic, strong) UILabel *descL;
@property (nonatomic, strong) UILabel *followL;
@property (nonatomic, strong) UIButton *followBtn;

@end

@implementation FBSFollowListCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self.contentView addSubview:self.iconV];
        [self.contentView addSubview:self.nameL];
        [self.contentView addSubview:self.descL];
        [self.contentView addSubview:self.followL];
        [self.contentView addSubview:self.followBtn];

        [self.iconV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(WScale(15));
            make.left.mas_equalTo(WScale(15));
            make.width.height.mas_equalTo(WScale(34));
        }];
        
        
        [self.nameL mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.iconV);
            make.left.mas_equalTo(self.iconV.mas_right).offset(WScale(15));
            make.height.mas_equalTo(WScale(14));
        }];
        
        [self.descL mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.nameL.mas_bottom).offset(WScale(8));
            make.left.mas_equalTo(self.nameL);
            make.height.mas_equalTo(WScale(12));
        }];


        [self.followL mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.descL.mas_bottom).offset(WScale(8));
            make.left.mas_equalTo(self.nameL);
            make.height.mas_equalTo(WScale(10));
        }];

        
        
        
        [self.followBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(-WScale(15));
            make.width.mas_equalTo(WScale(64));
            make.height.mas_equalTo(WScale(22));
            make.centerY.mas_equalTo(self.iconV);
        }];

    }
    
    return self;
}



- (void)setItem:(FBSFollowListItem *)item {
    if (![item isKindOfClass:[FBSFollowListItem class]]) {
        return ;
    }
    
    [super setItem:item];
    
    self.nameL.text = item.name;
    self.descL.text = item.desc;
    self.followL.text = item.followCount;
    self.followBtn.selected = item.isFollow;
}


#pragma mark - private method

- (void)followAction {
    if (self.delegate && [self.delegate respondsToSelector:@selector(fbsCell:didClickButtonAtIndex:)]) {
        [self.delegate fbsCell:self didClickButtonAtIndex:0];
    }
}

#pragma mark - property

- (UIImageView *)iconV {
    if (!_iconV) {
        _iconV  = [[UIImageView alloc] init];
        _iconV.backgroundColor = RandomColor;
        _iconV.layer.cornerRadius = WScale(17);
        _iconV.layer.masksToBounds = YES;
    }
    return _iconV;
}


- (UILabel *)nameL {
    if (!_nameL) {
        _nameL = [[UILabel alloc] init];
        _nameL.textColor = RGBA(51,51,51,1);
        _nameL.font = kLabelBoldFontSize(14);
    }
    return _nameL;
}

- (UILabel *)descL {
    if (!_descL) {
        _descL  = [[UILabel alloc] init];
        _descL.textColor = RGBA(155,155,155,1);
        _descL.font = kLabelFontSize12;
    }
    return _descL;
}


- (UILabel *)followL {
    if (!_followL) {
        _followL = [[UILabel alloc] init];
        _followL.textColor = RGBA(155,155,155,1);
        _followL.font = kLabelFontSize10;
    }
    return _followL;
}

- (UIButton *)followBtn {
    if (!_followBtn) {
        _followBtn  = [UIButton buttonWithType:UIButtonTypeCustom];
        [_followBtn setBackgroundImage:kImageWithName(@"follow_unsel") forState:UIControlStateNormal];
        [_followBtn setBackgroundImage:kImageWithName(@"follow_sel") forState:UIControlStateSelected];
        [_followBtn addTarget:self action:@selector(followAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _followBtn;
}


@end
