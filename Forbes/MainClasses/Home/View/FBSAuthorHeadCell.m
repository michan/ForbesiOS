//
//  FBSAuthorHeadCell.m
//  Forbes
//
//  Created by 周灿华 on 2019/9/29.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSAuthorHeadCell.h"

@implementation FBSAuthorHeadItem

- (instancetype)init {
    self = [super init];
    if (self) {
        self.bottomLineHidden = YES;
    }
    return self;
}

@end


@interface FBSAuthorHeadCell ()
@property (nonatomic, strong) UIImageView *iconV;
@property (nonatomic, strong) UILabel *titleL;
@property (nonatomic, strong) UILabel *detailL;
@property (nonatomic, strong) UILabel *contentL;
@property (nonatomic, strong) NSDictionary *attributeDic;

@end

@implementation FBSAuthorHeadCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self.contentView addSubview:self.iconV];
        [self.contentView addSubview:self.titleL];
        [self.contentView addSubview:self.detailL];
        [self.contentView addSubview:self.contentL];
        
        [self.iconV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(WScale(20));
            make.left.mas_equalTo(WScale(15));
            make.width.height.mas_equalTo(WScale(60));
        }];
        
        [self.titleL mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.iconV).offset(WScale(10));
            make.left.mas_equalTo(self.iconV.mas_right).offset(WScale(20));
            make.height.mas_equalTo(WScale(18));
        }];
        
        
        [self.detailL mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.mas_equalTo(self.iconV).offset(-WScale(10));
            make.left.mas_equalTo(self.titleL);
            make.height.mas_equalTo(WScale(12));
        }];
        
        
        [self.contentL mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.iconV.mas_bottom).offset(WScale(10));
            make.left.mas_equalTo(WScale(10));
            make.width.mas_equalTo(kScreenWidth -  WScale(20));
            make.right.mas_equalTo(-WScale(10));
            make.bottom.mas_equalTo(-WScale(20));
        }];

    }
    
    return self;
}


- (void)setItem:(FBSAuthorHeadItem *)item {
    if (![item isKindOfClass:[FBSAuthorHeadItem class]]) {
        return ;
    }
    
    [super setItem:item];
    self.titleL.text = item.title;
    self.detailL.text = item.detail;
    
    self.contentL.attributedText = [[NSAttributedString alloc] initWithString:item.content attributes:self.attributeDic];
    [self.iconV sd_setImageWithURL:[NSURL URLWithString:item.imgUrl]];
}

#pragma mark - property

- (UIImageView *)iconV {
    if (!_iconV) {
        _iconV  = [[UIImageView alloc] init];
        _iconV.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5];
        _iconV.layer.cornerRadius = WScale(30);
        _iconV.layer.masksToBounds = YES;
        _iconV.layer.borderColor = [UIColor grayColor].CGColor;
        _iconV.layer.borderWidth = 1.0;
    }
    return _iconV;
}

- (UILabel *)titleL {
    if (!_titleL) {
        _titleL  = [[UILabel alloc] init];
        _titleL.textColor = RGB(51, 51, 51);
        _titleL.font = kLabelFontSize18;
    }
    return _titleL;
}


- (UILabel *)detailL {
    if (!_detailL) {
        _detailL  = [[UILabel alloc] init];
        _detailL.textColor = RGBA(153,153,153,1);
        _detailL.font = kLabelFontSize13;
    }
    return _detailL;
}


- (UILabel *)contentL {
    if (!_contentL) {
        _contentL  = [[UILabel alloc] init];
        _contentL.textColor = Hexcolor(0x333333);
        _contentL.font = kLabelFontSize13;
        _contentL.numberOfLines = 0;
    }
    return _contentL;
}

- (NSDictionary *)attributeDic {
    if (!_attributeDic) {
        NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc] init];
        style.lineSpacing = 5;
        
        _attributeDic  = @{
                           NSForegroundColorAttributeName : Hexcolor(0x333333),
                           NSFontAttributeName : kLabelFontSize13,
                           NSParagraphStyleAttributeName : style
                           };
    }
    return _attributeDic;
}


@end
