//
//  FBSFollowMoreSeachView.h
//  Forbes
//
//  Created by michan on 2020/6/23.
//  Copyright © 2020 周灿华. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FBSFollowMoreSeachView : UIView
@property (weak, nonatomic) IBOutlet UITextField *F;
@property (weak, nonatomic) IBOutlet UIButton *btn;

@end

NS_ASSUME_NONNULL_END
