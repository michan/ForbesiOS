//
//  FBSTopicHeadCell.m
//  Forbes
//
//  Created by 周灿华 on 2019/9/29.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSTopicHeadCell.h"

@implementation FBSTopicHeadItem

@end



@interface FBSTopicHeadCell ()
@property (nonatomic, strong) UILabel *titleL;
@property (nonatomic, strong) UIImageView *imgView;
@property (nonatomic, strong) NSDictionary *attributeDic;

@end

@implementation FBSTopicHeadCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self.contentView addSubview:self.titleL];
        [self.contentView addSubview:self.imgView];
        
        [self.titleL mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(WScale(10));
            make.top.mas_equalTo(WScale(10));
            make.width.mas_equalTo(kScreenWidth - WScale(20));
        }];
        
        [self.imgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.titleL.mas_bottom).offset(WScale(15));
            make.left.mas_equalTo(self.titleL);
            make.width.mas_equalTo(kScreenWidth - WScale(20));
            make.height.mas_equalTo(WScale(180));
            make.bottom.mas_equalTo(-WScale(20));
        }];
        
        
    }
    
    return self;
}



- (void)setItem:(FBSTopicHeadItem *)item {
    if (![item isKindOfClass:[FBSTopicHeadItem class]]) {
        return ;
    }
    
    [super setItem:item];
    
    NSMutableAttributedString *attStr = [[NSMutableAttributedString alloc] initWithString:item.title attributes:self.attributeDic];

    // 创建图片附件
    NSTextAttachment *attach = [[NSTextAttachment alloc] init];
    attach.image = [CommonTool makeTopicImage];
    attach.bounds = CGRectMake(0, -1, WScale(35), WScale(15));
    NSAttributedString *attachString = [NSAttributedString attributedStringWithAttachment:attach];
    
    //将图片插入到合适的位置
    [attStr insertAttributedString:attachString atIndex:0];
    [attStr insertAttributedString:[[NSAttributedString alloc] initWithString:@"  "] atIndex:1];
    
    self.titleL.attributedText = [attStr copy];
    [self.imgView sd_setImageWithURL:[NSURL URLWithString:item.imgUrl]];
}

#pragma mark - property

- (UILabel *)titleL {
    if (!_titleL) {
        _titleL  = [[UILabel alloc] init];
        _titleL.textColor = Hexcolor(0x333333);
        _titleL.font = kLabelFontSize18;
        _titleL.numberOfLines = 0;
    }
    return _titleL;
}


- (UIImageView *)imgView {
    if (!_imgView) {
        _imgView  = [[UIImageView alloc] init];
        _imgView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5];
    }
    return _imgView;
}


- (NSDictionary *)attributeDic {
    if (!_attributeDic) {
        NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc] init];
        style.lineSpacing = 10;
        
        _attributeDic  = @{
                           NSForegroundColorAttributeName : Hexcolor(0x333333),
                           NSFontAttributeName : [UIFont boldSystemFontOfSize:20],
                           NSParagraphStyleAttributeName : style
                           };
    }
    return _attributeDic;
}


@end
