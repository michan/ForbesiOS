//
//  FBSFollowMoreCell.m
//  Forbes
//
//  Created by 周灿华 on 2019/8/24.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSFollowMoreCell.h"

@implementation FBSFollowMoreItem

@end


@interface FBSFollowMoreCell ()
@property (nonatomic, strong) UIView *wrapV;
@property (nonatomic, strong) UILabel *detailL;
@property (nonatomic, strong) UIImageView *arrowV;
@end

@implementation FBSFollowMoreCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self.contentView addSubview:self.wrapV];
        [self.wrapV addSubview:self.detailL];
        [self.wrapV addSubview:self.arrowV];
        
        [self.wrapV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.centerY.mas_equalTo(0);
            make.height.mas_equalTo(WScale(30));
        }];
        
        [self.detailL mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(0);
            make.centerY.mas_equalTo(0);
        }];
        

        
        [self.arrowV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.detailL.mas_right).offset(WScale(10));
            make.right.mas_equalTo(0);
            make.centerY.mas_equalTo(0);
            make.width.mas_equalTo(WScale(5));
            make.height.mas_equalTo(WScale(8));
        }];
        
    }
    
    return self;
}



- (void)setItem:(FBSFollowMoreItem *)item {
    if (![item isKindOfClass:[FBSFollowMoreItem class]]) {
        return ;
    }
    
    [super setItem:item];

}


#pragma mark - property


- (UIView *)wrapV {
    if (!_wrapV) {
        _wrapV = [[UIView alloc] init];
    }
    return _wrapV;
}


- (UILabel *)detailL {
    if (!_detailL) {
        _detailL = [[UILabel alloc] init];
        _detailL.font = kLabelFontSize12;
        _detailL.textColor = RGBA(51,51,51,1);
        _detailL.text = @"关注更多用户";
    }
    return _detailL;
}


- (UIImageView *)arrowV {
    if (!_arrowV) {
        _arrowV  = [[UIImageView alloc] init];
        _arrowV.image = kImageWithName(@"follow_more");
    }
    return _arrowV;
}

@end
