//
//  FBSHomeOperateView.m
//  Forbes
//
//  Created by 周灿华 on 2019/8/24.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSHomeOperateView.h"

@implementation FBSHomeOperateView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self addSubview:self.praiseBtn];
        [self addSubview:self.commentBtn];
        [self addSubview:self.shareBtn];
    }
    return self;
}

- (UIButton *)praiseBtn {
    if (!_praiseBtn) {
        _praiseBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _praiseBtn.tag = PraiseIndex;
        _praiseBtn.titleLabel.font = kLabelFontSize11;
        [_praiseBtn setTitleColor:Hexcolor(0x666666) forState:UIControlStateNormal];
        [_praiseBtn setTitleColor:[UIColor redColor] forState:UIControlStateSelected];
        [_praiseBtn setTitle:@"3" forState:UIControlStateNormal];
        [_praiseBtn setTitle:@"4" forState:UIControlStateSelected];
        [_praiseBtn setImage:kImageWithName(@"video_praise1") forState:UIControlStateNormal];
        [_praiseBtn setImage:kImageWithName(@"video_praise2") forState:UIControlStateSelected];
//        [_praiseBtn addTarget:self action:@selector(clickAction:) forControlEvents:UIControlEventTouchUpInside];
        
        _praiseBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 2.5);
        _praiseBtn.titleEdgeInsets = UIEdgeInsetsMake(0, 2.5, 0, 0);
    }
    return _praiseBtn;
}


- (UIButton *)commentBtn {
    if (!_commentBtn) {
        _commentBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _commentBtn.tag = CommentIndex;
        _commentBtn.titleLabel.font = kLabelFontSize11;
        [_commentBtn setTitleColor:Hexcolor(0x666666) forState:UIControlStateNormal];
        [_commentBtn setTitle:@"18" forState:UIControlStateNormal];
        [_commentBtn setImage:kImageWithName(@"follow_comment") forState:UIControlStateNormal];
//        [_commentBtn addTarget:self action:@selector(clickAction:) forControlEvents:UIControlEventTouchUpInside];
        
        _commentBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 2.5);
        _commentBtn.titleEdgeInsets = UIEdgeInsetsMake(0, 2.5, 0, 0);
    }
    return _commentBtn;
}


- (UIButton *)shareBtn {
    if (!_shareBtn) {
        _shareBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _shareBtn.tag = CommentIndex;
        _shareBtn.titleLabel.font = kLabelFontSize11;
        [_shareBtn setTitleColor:Hexcolor(0x666666) forState:UIControlStateNormal];
        [_shareBtn setTitle:@"18" forState:UIControlStateNormal];
        [_shareBtn setImage:kImageWithName(@"video_comment") forState:UIControlStateNormal];
        //        [_commentBtn addTarget:self action:@selector(clickAction:) forControlEvents:UIControlEventTouchUpInside];
        
        _shareBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 2.5);
        _shareBtn.titleEdgeInsets = UIEdgeInsetsMake(0, 2.5, 0, 0);
    }
    return _shareBtn;
}


@end
