//
//  FBSManinItem.h
//  Forbes
//
//  Created by 周灿华 on 2019/9/27.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSCell.h"

@interface FBSMainItem : FBSItem
@property (nonatomic, assign) DetailPageType detailType;
@property (nonatomic, copy) NSString *ID;
@property (nonatomic, copy) NSString *content;
@property (nonatomic, copy) NSString *author;
@property (nonatomic, copy) NSString *comment;
@property (nonatomic, copy) NSString *time;
@property (nonatomic, copy) NSString *imgUrl;

@property (nonatomic, copy) NSString *keyword;  //搜索页面关键字
@property (nonatomic, copy) NSString *videoUrl;  //视频才会赋值
@end


@interface FBSMainCell : FBSCell

@end
