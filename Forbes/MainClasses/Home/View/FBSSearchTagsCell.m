//
//  FBSSearchTagsCell.m
//  Forbes
//
//  Created by 周灿华 on 2019/9/27.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSSearchTagsCell.h"
#import "FBSTagCollectionViewCell.h"
#import "FBSTagAttribute.h"

@interface FBSSearchTagsItem ()
@property (nonatomic, strong) FBSTagsLayout *layout;
@property (nonatomic, strong) FBSTagAttribute *tagAttribute;
@end

@implementation FBSSearchTagsItem

- (CGFloat)calculateCellHeight {
    CGFloat contentHeight = 0;
    
    //cell的高度 = 顶部 + 高度
    contentHeight = self.layout.sectionInset.top + self.layout.itemSize.height;
    CGFloat originX = self.layout.sectionInset.left;
    CGFloat originY = self.layout.sectionInset.top;
    
    NSInteger itemCount = self.tags.count;
    
    for (NSInteger i = 0; i < itemCount; i++) {
        CGSize maxSize = CGSizeMake(kScreenWidth - self.layout.sectionInset.left - self.layout.sectionInset.right, self.layout.itemSize.height);
        
        CGRect frame = [self.tags[i] boundingRectWithSize:maxSize options:NSStringDrawingUsesFontLeading|NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:self.tagAttribute.titleSize]} context:nil];
        
        CGSize itemSize = CGSizeMake(frame.size.width + self.tagAttribute.tagSpace, self.layout.itemSize.height);
        
        if (self.layout.scrollDirection == UICollectionViewScrollDirectionVertical) {
            //垂直滚动
            //当前CollectionViewCell的起点 + 当前CollectionViewCell的宽度 + 当前CollectionView距离右侧的间隔 > collectionView的宽度
            if ((originX + itemSize.width + self.layout.sectionInset.right) > kScreenWidth) {
                originX = self.layout.sectionInset.left;
                originY += itemSize.height + self.layout.minimumLineSpacing;
                
                contentHeight += itemSize.height + self.layout.minimumLineSpacing;
            }
        }
        
        originX += itemSize.width + self.layout.minimumInteritemSpacing;
    }
    
    contentHeight += self.layout.sectionInset.bottom;
    contentHeight += + WScale(45);
    return contentHeight;
}

- (FBSTagsLayout *)layout {
    if (!_layout) {
        _layout = [[FBSTagsLayout alloc] init];
        _layout.scrollDirection = UICollectionViewScrollDirectionVertical;
    }
    return _layout;
}

- (FBSTagAttribute *)tagAttribute {
    if (!_tagAttribute) {
        _tagAttribute = [[FBSTagAttribute alloc] init];
    }
    return _tagAttribute;
}

@end


@interface FBSSearchTagsCell () <UICollectionViewDelegate, UICollectionViewDataSource>
@property (nonatomic,strong) FBSTagsLayout *layout;//布局layout
@property (nonatomic, strong) UICollectionView *collectionView;
@property (nonatomic, strong) UILabel *titleL;

@end

@implementation FBSSearchTagsCell

static NSString * const reuseIdentifier = @"FBSTagCollectionViewCellId";

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self =  [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setup];
    }
    
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder]) {
        [self setup];
    }
    
    return self;
}

- (void)setup
{
    //初始化样式
    _tagAttribute = [FBSTagAttribute new];
    
    _layout = [[FBSTagsLayout alloc] init];
    [self addSubview:self.titleL];
    [self addSubview:self.collectionView];
    
    [self.titleL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(WScale(15));
        make.left.mas_equalTo(WScale(15));
        make.right.mas_equalTo(-WScale(10));
        make.height.mas_equalTo(WScale(15));
    }];
    
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.mas_equalTo(0);
        make.top.mas_equalTo(self.titleL.mas_bottom).offset(WScale(15));
    }];
}




- (void)setItem:(FBSSearchTagsItem *)item {
    if (![item isKindOfClass:[FBSSearchTagsItem class]]) {
        return ;
    }
    [super setItem:item];
    [self.collectionView setCollectionViewLayout:self.layout];
    self.collectionView.frame = self.bounds;
    
    self.layout = item.layout;
    self.tags = item.tags;
    [self reloadData];
    
    
}

#pragma mark - UICollectionViewDelegate | UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.tags.count;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    FBSTagsLayout *layout = (FBSTagsLayout *)collectionView.collectionViewLayout;
    CGSize maxSize = CGSizeMake(collectionView.frame.size.width - layout.sectionInset.left - layout.sectionInset.right, layout.itemSize.height);
    
    CGRect frame = [_tags[indexPath.item] boundingRectWithSize:maxSize options:NSStringDrawingUsesFontLeading|NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:_tagAttribute.titleSize]} context:nil];
    
    return CGSizeMake(frame.size.width + _tagAttribute.tagSpace, layout.itemSize.height);
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    FBSTagCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    cell.backgroundColor = _tagAttribute.normalBackgroundColor;
    cell.layer.borderColor = [UIColor clearColor].CGColor;
    cell.layer.cornerRadius = _tagAttribute.cornerRadius;
    cell.layer.borderWidth = _tagAttribute.borderWidth;
//    cell.titleLabel.textColor = _tagAttribute.textColor;
    cell.titleLabel.font = [UIFont systemFontOfSize:_tagAttribute.titleSize];
    
    NSString *title = self.tags[indexPath.item];
    if (_key.length > 0) {
        cell.titleLabel.attributedText = [self searchTitle:title key:_key keyColor:_tagAttribute.keyColor];
    } else {
        cell.titleLabel.text = title;
    }
    
    if ([self.selectedTags containsObject:self.tags[indexPath.item]]) {
        cell.backgroundColor = _tagAttribute.selectedBackgroundColor;
    }
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    FBSTagCollectionViewCell *cell = (FBSTagCollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
    
    if ([self.selectedTags containsObject:self.tags[indexPath.item]]) {
        cell.backgroundColor = _tagAttribute.normalBackgroundColor;
        [self.selectedTags removeObject:self.tags[indexPath.item]];
    }
    else {
        if (_isMultiSelect) {
            cell.backgroundColor = _tagAttribute.selectedBackgroundColor;
            [self.selectedTags addObject:self.tags[indexPath.item]];
        } else {
            [self.selectedTags removeAllObjects];
            [self.selectedTags addObject:self.tags[indexPath.item]];
            
            //            [self reloadData];
        }
    }
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(fbsCell:didClickButtonAtIndex:)]) {
        [self.delegate fbsCell:self didClickButtonAtIndex:indexPath.item];
    }
}

// 设置文字中关键字高亮
- (NSMutableAttributedString *)searchTitle:(NSString *)title key:(NSString *)key keyColor:(UIColor *)keyColor {
    
    NSMutableAttributedString *titleStr = [[NSMutableAttributedString alloc] initWithString:title];
    NSString *copyStr = title;
    
    NSMutableString *xxstr = [NSMutableString new];
    for (int i = 0; i < key.length; i++) {
        [xxstr appendString:@"*"];
    }
    
    while ([copyStr rangeOfString:key options:NSCaseInsensitiveSearch].location != NSNotFound) {
        
        NSRange range = [copyStr rangeOfString:key options:NSCaseInsensitiveSearch];
        
        [titleStr addAttribute:NSForegroundColorAttributeName value:keyColor range:range];
        copyStr = [copyStr stringByReplacingCharactersInRange:NSMakeRange(range.location, range.length) withString:xxstr];
    }
    return titleStr;
}

- (void)reloadData {
    [self.collectionView reloadData];
}


#pragma mark - 懒加载


- (UILabel *)titleL {
    if (!_titleL) {
        _titleL = [[UILabel alloc] init];
        _titleL.font = kLabelFontSize15;
        _titleL.textColor = RGB(102,102,102);
        _titleL.text = @"搜索历史";
    }
    return _titleL;
}


- (NSMutableArray *)selectedTags
{
    if (!_selectedTags) {
        _selectedTags = [NSMutableArray array];
    }
    return _selectedTags;
}

- (UICollectionView *)collectionView {
    if (!_collectionView) {
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, 362) collectionViewLayout:_layout];
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        _collectionView.backgroundColor = [UIColor whiteColor];
        [_collectionView registerClass:[FBSTagCollectionViewCell class] forCellWithReuseIdentifier:reuseIdentifier];
    }
    
    _collectionView.collectionViewLayout = _layout;
    
    if (_layout.scrollDirection == UICollectionViewScrollDirectionVertical) {
        //垂直
        _collectionView.showsVerticalScrollIndicator = YES;
        _collectionView.showsHorizontalScrollIndicator = NO;
    } else {
        _collectionView.showsHorizontalScrollIndicator = YES;
        _collectionView.showsVerticalScrollIndicator = NO;
    }
    
    if (@available(iOS 11.0, *)) {
        _collectionView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    }
    
    _collectionView.scrollEnabled = NO;
    _collectionView.frame = self.bounds;
    
    return _collectionView;
}


@end
