//
//  FBSFollowTableViewCell.m
//  Forbes
//
//  Created by michan on 2020/6/16.
//  Copyright © 2020 周灿华. All rights reserved.
//

#import "FBSFollowTableViewCell.h"

@implementation FBSFollowTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    ViewRadius(_imgview, 20);
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
