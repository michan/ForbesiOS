//
//  FBSEntityInfoView.h
//  Forbes
//
//  Created by 周灿华 on 2019/8/24.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FBSEntityInfoView : UIView
@property (nonatomic, strong) UIImageView *iconV;
@property (nonatomic, strong) UILabel *nameL;
@property (nonatomic, strong) UILabel *descL;
@property (nonatomic, strong) UIButton *followBtn;

@end

NS_ASSUME_NONNULL_END
