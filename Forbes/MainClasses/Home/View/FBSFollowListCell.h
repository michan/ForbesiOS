//
//  FBSFollowListCell.h
//  Forbes
//
//  Created by 周灿华 on 2019/8/24.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSCell.h"

@interface FBSFollowListItem : FBSItem
@property (nonatomic, copy) NSString *imgUrl;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *desc;
@property (nonatomic, copy) NSString *followCount;
@property (nonatomic, assign) BOOL isFollow;
@end

@interface FBSFollowListCell : FBSCell

@end
