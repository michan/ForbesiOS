//
//  FBSTextViewCell.m
//  Forbes
//
//  Created by 赵志辉 on 2019/9/25.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import "FBSTextViewCell.h"
#import "FBSTextViewItem.h"
#import "FLNiceSpinner.h"

@interface FBSTextViewCell()<UITextViewDelegate,FLNiceSpinnerDelegate>

@property (nonatomic, strong) UILabel *contenL;
@property (nonatomic, strong) UILabel *detail;
@property (nonatomic, strong) UIButton *button;

@property (nonatomic, strong)FLNiceSpinner *sp;


@end

@implementation FBSTextViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        
        [self.contentView addSubview:self.contenL]; //免费票
        [self.contentView addSubview:self.detail];
        [self.contentView addSubview:self.textView];
        [self.contentView addSubview:self.sp];
        
        [self.contenL sizeToFit];
        [self.contenL setContentHuggingPriority:UILayoutPriorityRequired
                                  forAxis:UILayoutConstraintAxisHorizontal];
        [self.detail setContentHuggingPriority:UILayoutPriorityDefaultLow
                                  forAxis:UILayoutConstraintAxisHorizontal];
        [self.contenL mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(WScale(13));
            make.top.mas_equalTo(self).offset(WScale(10));
            make.height.mas_equalTo(WScale(20));
            make.right.mas_equalTo(self.detail.mas_left).offset(WScale(-4));

        }];
        [self.detail mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.contenL.mas_right).offset(WScale(4));
            make.top.mas_equalTo(self.contenL.mas_top).offset(WScale(5));
            make.height.mas_equalTo(WScale(14));
            make.right.mas_equalTo(self).offset(WScale(-13));
        }];
        
        [self.textView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.mas_left).offset(WScale(13));
            make.top.mas_equalTo(self.mas_top).offset(WScale(40));
            make.right.mas_equalTo(self).offset(WScale(-13));
            make.bottom.mas_equalTo(self.mas_bottom).offset(WScale(-20));
        }];
        [self.sp mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.mas_left).offset(WScale(13));
            make.top.mas_equalTo(self.mas_top).offset(WScale(40));
            make.right.mas_equalTo(self).offset(WScale(-13));
            make.bottom.mas_equalTo(self.mas_bottom).offset(WScale(-20));
        }];
       
        
    }
    
    return self;
}
- (void)click{
    if ([self.delegate respondsToSelector:@selector(fbsCell:didClickButtonAtIndex:)]) {
        [self.delegate fbsCell:self didClickButtonAtIndex:0];
    }
}

- (SZTextView *)textView
{
    if (_textView == nil) {
//        CGRect cellFrame = self.contentView.bounds;
//        cellFrame.origin.y += kPadding;
//        cellFrame.size.height -= kPadding;
        
        _textView = [[SZTextView alloc] init];
        _textView.delegate = self;
        _textView.layer.borderColor = Hexcolor(0x999999).CGColor;
        _textView.layer.borderWidth = WScale(1);
        _textView.layer.cornerRadius = WScale(2);
        
        _textView.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
        _textView.backgroundColor = [UIColor clearColor];
        _textView.font = [UIFont systemFontOfSize:18.0f];
        
        _textView.scrollEnabled = NO;
        _textView.showsVerticalScrollIndicator = NO;
        _textView.showsHorizontalScrollIndicator = NO;
        // textView.contentInset = UIEdgeInsetsZero;
    }
    return _textView;
}
- (void)setItem:(FBSTextViewItem *)item{
    [super setItem:item];

    self.textView.text = item.text;
    self.contenL.text = item.title;
    self.detail.text = item.detail;
    self.textView.editable = !item.isEdit;
    self.sp.hidden = !item.isEdit;
    self.textView.hidden = item.isEdit;
    [self performSelector:@selector(textViewDidChange:)
               withObject:self.textView
               afterDelay:0.1];
}
- (void)fl_spinner:(FLNiceSpinner *)spinner didSelectedItemAtIndex:(NSInteger)index{
    [(id<FLNiceSpinnerDelegate>)self.vc fl_spinner:spinner didSelectedItemAtIndex:index];
}
- (NSInteger)fl_itemsCountOfSpinner:(FLNiceSpinner *)spinner{
    return   [(id<FLNiceSpinnerDelegate>)self.vc fl_itemsCountOfSpinner:spinner];
}
- (NSString *)fl_spinner:(FLNiceSpinner *)spinner showItemStringAtIndex:(NSInteger)index{
    return [(id<FLNiceSpinnerDelegate>)self.vc fl_spinner:spinner showItemStringAtIndex:index];
}
- (CGFloat)cellHeight
{
    return [self.textView sizeThatFits:CGSizeMake(self.textView.frame.size.width, FLT_MAX)].height + WScale(60);
}

- (void)updateTextViewHeight {
    [self textViewDidChange:self.textView];
}

#pragma mark - Text View Delegate

-(void)textViewDidEndEditing:(UITextView *)textView{
    if ([self.expandableTableView.delegate respondsToSelector:@selector(tableView:textViewDidEndEditing:)]) {
        [(id<ACEExpandableTableViewDelegate>)self.expandableTableView.delegate tableView:self.expandableTableView textViewDidEndEditing:self.textView];
    }
}

- (void)textViewDidChangeSelection:(UITextView *)textView {
    if ([self.expandableTableView.delegate respondsToSelector:@selector(tableView:textViewDidChangeSelection:)]) {
        [(id<ACEExpandableTableViewDelegate>)self.expandableTableView.delegate tableView:self.expandableTableView textViewDidChangeSelection:self.textView];
    }
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if ([self.expandableTableView.delegate respondsToSelector:@selector(tableView:textView:shouldChangeTextInRange:replacementText:)]) {
        id<ACEExpandableTableViewDelegate> delegate = (id<ACEExpandableTableViewDelegate>)self.expandableTableView.delegate;
        return [delegate tableView:self.expandableTableView
                          textView:textView
           shouldChangeTextInRange:range
                   replacementText:text];
    }
    return YES;
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    // make sure the cell is at the top
    [self.expandableTableView scrollToRowAtIndexPath:[self.expandableTableView indexPathForCell:self]
                                    atScrollPosition:UITableViewScrollPositionTop
                                            animated:YES];
    
    return YES;
}

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    if ([self.expandableTableView.delegate respondsToSelector:@selector(textViewDidBeginEditing:)]) {
        [(id<ACEExpandableTableViewDelegate>)self.expandableTableView.delegate textViewDidBeginEditing:textView];
    }
}

- (void)textViewDidChange:(UITextView *)theTextView
{
    if ([self.expandableTableView.delegate conformsToProtocol:@protocol(ACEExpandableTableViewDelegate)]) {
        
        id<ACEExpandableTableViewDelegate> delegate = (id<ACEExpandableTableViewDelegate>)self.expandableTableView.delegate;
        NSIndexPath *indexPath = [self.expandableTableView indexPathForCell:self];
        
        // update the text
        _text = self.textView.text;
        
        [delegate tableView:self.expandableTableView
                updatedText:_text
                atIndexPath:indexPath];
        
        CGFloat newHeight = [self cellHeight];
        CGFloat oldHeight = [delegate tableView:self.expandableTableView heightForRowAtIndexPath:indexPath];
        if (fabs(newHeight - oldHeight) > 0.01) {
            
            // update the height
            if ([delegate respondsToSelector:@selector(tableView:updatedHeight:atIndexPath:)]) {
                [delegate tableView:self.expandableTableView
                      updatedHeight:newHeight
                        atIndexPath:indexPath];
            }
            
            // refresh the table without closing the keyboard
            [self.expandableTableView beginUpdates];
            [self.expandableTableView endUpdates];
        }
    }
}

- (FLNiceSpinner *)sp{
    if (!_sp) {
        _sp = [[FLNiceSpinner alloc] init];
        _sp.spaceString = @"";
        _sp.clearNoteString = @"";
        _sp.layer.borderColor = Hexcolor(0x999999).CGColor;
        _sp.layer.borderWidth = WScale(1);
        _sp.layer.cornerRadius = WScale(2);
        _sp.delegate = self;
    }
    return _sp;
}

//- (void)setItem:(TicketsItem *)item {
//    if (![item isKindOfClass:[TicketsItem class]]) {
//        return ;
//    }
//    [super setItem:item];
//    self.contenL.text = item.title;
//    self.descL.text = item.price;
//    self.time.text = item.remarks;
//    self.numberButton.maxValue = [item.number floatValue];
//    
//}

- (UILabel *)contenL {
    if (!_contenL) {
        _contenL  = [[UILabel alloc] init];
        _contenL.textColor = Hexcolor(0x333333);
        _contenL.font = kLabelFontSize14;
       
    }
    return _contenL;
}

- (UILabel *)detail {
    if (!_detail) {
        _detail  = [[UILabel alloc] init];
        _detail.textColor = Hexcolor(0xD0021B);
        _detail.font = kLabelFontSize10;
    }
    return _detail;
}

- (UIButton *)button{
    if (!_button) {
        _button = [[UIButton alloc] init];
        [_button addTarget:self action:@selector(click) forControlEvents:UIControlEventTouchUpInside];
        [_button setBackgroundImage:[UIImage imageNamed:@"展开更多"] forState:UIControlStateNormal];
    }
    return _button;
}

@end
