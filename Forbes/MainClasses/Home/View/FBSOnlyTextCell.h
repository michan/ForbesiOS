//
//  FBSOnlyTextCell.h
//  Forbes
//
//  Created by 周灿华 on 2019/7/26.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSMainCell.h"

@interface FBSOnlyTextItem : FBSMainItem
@property (nonatomic, assign) BOOL isTop;  //是否置顶
@end


@interface FBSOnlyTextCell : FBSMainCell


@end

