//
//  FBSFollowTableViewCell.h
//  Forbes
//
//  Created by michan on 2020/6/16.
//  Copyright © 2020 周灿华. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FBSFollowTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imgview;

@property (weak, nonatomic) IBOutlet UILabel *lbl2;
@property (weak, nonatomic) IBOutlet UILabel *lbl3;

@property (weak, nonatomic) IBOutlet UIButton *guanzhuBtn;
@property (weak, nonatomic) IBOutlet UILabel *nameLbl;

@end

NS_ASSUME_NONNULL_END
