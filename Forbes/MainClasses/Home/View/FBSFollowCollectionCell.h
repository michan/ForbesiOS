//
//  FBSFollowCollectionCell.h
//  Forbes
//
//  Created by 周灿华 on 2019/8/24.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSCell.h"

#define MoreTag 1000

@interface FBSFollowCollectionItem : FBSItem
@property (nonatomic, strong) NSArray *entities;
@end

@interface FBSFollowCollectionCell : FBSCell

@end
