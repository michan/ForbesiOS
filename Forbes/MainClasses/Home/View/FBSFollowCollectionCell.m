//
//  FBSFollowCollectionCell.m
//  Forbes
//
//  Created by 周灿华 on 2019/8/24.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSFollowCollectionCell.h"

@implementation FBSFollowCollectionItem

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.backgroundColor = RGBA(246,247,249,1);
        self.bottomLineHidden = YES;
    }
    return self;
}

@end


@interface FBSEntityCollectionViewCell : UICollectionViewCell
@property (nonatomic, strong) UIImageView *iconV;
@property (nonatomic, strong) UILabel *nameL;
@end


@implementation FBSEntityCollectionViewCell


- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.contentView.backgroundColor = [UIColor clearColor];
        [self.contentView addSubview:self.iconV];
        [self.contentView addSubview:self.nameL];
        
        [self.iconV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(0);
            make.centerX.mas_equalTo(0);
            make.width.height.mas_equalTo(WScale(59));
        }];
        
        [self.nameL mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.iconV.mas_bottom).offset(WScale(10));
            make.left.right.mas_equalTo(0);
            make.centerX.mas_equalTo(0);
            make.height.mas_equalTo(WScale(12));
        }];

    }
    return self;
}


- (UIImageView *)iconV {
    if (!_iconV) {
        _iconV  = [[UIImageView alloc] init];
        _iconV.backgroundColor = RandomColor;
        _iconV.layer.cornerRadius = WScale(29);
        _iconV.layer.masksToBounds = YES;
    }
    return _iconV;
}


- (UILabel *)nameL {
    if (!_nameL) {
        _nameL = [[UILabel alloc] init];
        _nameL.font = kLabelFontSize12;
        _nameL.textColor = RGB(51, 51, 51);
        _nameL.text = @"福布斯中国";
        _nameL.textAlignment = NSTextAlignmentCenter;
        _nameL.adjustsFontSizeToFitWidth = YES;
    }
    return _nameL;
}


@end


@interface FBSFollowCollectionCell ()<UICollectionViewDataSource,UICollectionViewDelegate>
{
    FBSFollowCollectionItem *_currentItem;
}
@property (nonatomic, strong) UIView *topView;
@property (nonatomic, strong) UILabel *titleL;
@property (nonatomic, strong) UIControl *actionView;
@property (nonatomic, strong) UILabel *detailL;
@property (nonatomic, strong) UIImageView *arrowV;
@property (nonatomic, strong) UICollectionView *collectionView;

@end

@implementation FBSFollowCollectionCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self.contentView addSubview:self.topView];
        [self.topView addSubview:self.titleL];
        [self.topView addSubview:self.actionView];
        [self.topView addSubview:self.detailL];
        [self.topView addSubview:self.arrowV];
        [self.contentView addSubview:self.collectionView];
        
        
        [self.topView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.top.right.mas_equalTo(0);
            make.height.mas_equalTo(WScale(50));
        }];
        
        [self.titleL mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(WScale(10));
            make.top.mas_equalTo(WScale(15));
            make.height.mas_equalTo(WScale(15));
        }];
        
        
        [self.arrowV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(-WScale(15));
            make.centerY.mas_equalTo(self.titleL);
            make.width.mas_equalTo(WScale(5));
            make.height.mas_equalTo(WScale(8));
        }];
        
        [self.detailL mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(self.arrowV.mas_left).offset(-WScale(10));
            make.centerY.mas_equalTo(self.titleL);
        }];

        
        [self.actionView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.bottom.right.mas_equalTo(0);
            make.left.mas_equalTo(self.detailL);
        }];
        
        [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.topView.mas_bottom);
            make.left.right.mas_equalTo(0);
            make.height.mas_equalTo(WScale(100));
        }];
    }
    
    return self;
}



- (void)setItem:(FBSFollowCollectionItem *)item {
    if (![item isKindOfClass:[FBSFollowCollectionItem class]]) {
        return ;
    }
    
    [super setItem:item];
    _currentItem = item;
    
    [self.collectionView reloadData];
}

- (void)tapAction {
    if (self.delegate && [self.delegate respondsToSelector:@selector(fbsCell:didClickButtonAtIndex:)]) {
        [self.delegate fbsCell:self didClickButtonAtIndex:MoreTag];
    }
}


#pragma mark - UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return _currentItem.entities.count;
}


static NSString * const CELLID = @"FBSEntityCollectionViewCell";

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    FBSEntityCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:CELLID forIndexPath:indexPath];
    
    return cell;
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (self.delegate && [self.delegate respondsToSelector:@selector(fbsCell:didClickButtonAtIndex:)]) {
        [self.delegate fbsCell:self didClickButtonAtIndex:indexPath.item];
    }
}

#pragma mark - property

- (UIView *)topView {
    if (!_topView) {
        _topView  = [[UIView alloc] init];
        _topView.backgroundColor = [UIColor whiteColor];
    }
    return _topView;
}

- (UIControl *)actionView {
    if (!_actionView) {
        _actionView = [[UIControl alloc] init];
        [_actionView addTarget:self action:@selector(tapAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _actionView;
}


- (UILabel *)titleL {
    if (!_titleL) {
        _titleL = [[UILabel alloc] init];
        _titleL.font = kLabelBoldFontSize(15);
        _titleL.textColor = RGB(51, 51, 51);
        _titleL.text = @"最近浏览";
    }
    return _titleL;
}


- (UILabel *)detailL {
    if (!_detailL) {
        _detailL = [[UILabel alloc] init];
        _detailL.font = kLabelFontSize12;
        _detailL.textColor = RGB(153, 153, 153);
        _detailL.text = @"查看我的关注";
    }
    return _detailL;
}


- (UIImageView *)arrowV {
    if (!_arrowV) {
        _arrowV  = [[UIImageView alloc] init];
        _arrowV.image = kImageWithName(@"follow_more");
    }
    return _arrowV;
}

- (UICollectionView *)collectionView {
    if (!_collectionView) {
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
        
        NSInteger column = 4;
        CGFloat margin = WScale(10);
        CGFloat itemW = (kScreenWidth - (column + 1) * margin) / column;
        layout.sectionInset = UIEdgeInsetsMake(0, margin, 0, margin);
        layout.itemSize = CGSizeMake(itemW, WScale(85));
        layout.scrollDirection = UICollectionViewScrollDirectionVertical;
        layout.minimumLineSpacing = 0;
        layout.minimumInteritemSpacing = margin;
        
        _collectionView = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:layout];
        _collectionView.backgroundColor = [UIColor whiteColor];
        _collectionView.dataSource = self;
        _collectionView.delegate = self;

        _collectionView.showsHorizontalScrollIndicator = NO;
        _collectionView.showsVerticalScrollIndicator = NO;
        [_collectionView registerClass:[FBSEntityCollectionViewCell class] forCellWithReuseIdentifier:CELLID];
        [self addSubview:_collectionView];
        
    }
    return _collectionView;
}

@end
