//
//  FBSBrandvoiceViewController.m
//  Forbes
//
//  Created by 周灿华 on 2019/9/25.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSBrandvoiceViewController.h"
#import "FBSBrandvoiceViewModel.h"
#import "FBSTableViewController+Refresh.h"
#import "DetailViewController.h"
#import "FBSAuthorViewController.h"

@interface FBSBrandvoiceViewController ()
@property (nonatomic, strong) FBSBrandvoiceViewModel *viewModel;
@end

@implementation FBSBrandvoiceViewController
@synthesize viewModel = _viewModel;


- (instancetype)initWithChannelId:(NSString *)channelId {
    self = [super init];
    if (self) {
        self.viewModel = [[FBSBrandvoiceViewModel alloc] initWithChannelId:channelId];
    }
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationBarHidden = YES;
    
    self.registerDictionary = @{
                                [FBSOneImageItem reuseIdentifier] : [FBSOneImageCell class],
                                [FBSOnlyTextItem reuseIdentifier] : [FBSOnlyTextCell class],
                                [FBSGroupHeadItem reuseIdentifier] : [FBSGroupHeadCell class],
                                [FBSAuthorCollectItem reuseIdentifier] : [FBSAuthorCollectCell class],
                                [FBSAuthorHeadItem reuseIdentifier] : [FBSAuthorHeadCell class],
                                };
    [self createload];
}


- (void)createload {
    self.page = @(1);
    self.pageCount = @(6);
    WEAKSELF
    [self normalHeaderRefreshingActionBlock:^(FooterConfigBlock  _Nonnull footerConfig) {
        
        [weakSelf.viewModel rqBrandvoiceData:^(BOOL success) {
            [weakSelf.tableView reloadData];
            
            if (success) {
                if(footerConfig){
                    footerConfig(10);
                }
            } else {
                if(footerConfig){
                    footerConfig(0);
                }
            }
        }];
    }];
    
    
    [self backNormalFooterRefreshingActionBlock:^(FooterConfigBlock  _Nonnull footerConfig) {
        [weakSelf.viewModel rqLatestNews:^(BOOL success, NSInteger count) {
            if (success) {
                [weakSelf.tableView reloadData];
                if(footerConfig){
                    footerConfig(count);
                }
            }else{
                
            }
        }];
    }];
}

#pragma mark - delegate

- (void)fbsCell:(FBSCell *)cell didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([cell isKindOfClass:[FBSMainCell class]]) {
        FBSMainItem *item = (FBSMainItem *)cell.item;
        
        DetailViewController *detail = [[DetailViewController alloc] initWithArticleId:item.ID htmlAddressL:@"article/" cellAddress:@"interest/"];
        [self.navigationController pushViewController:detail animated:YES];
    }
}


- (void)fbsCell:(FBSCell *)cell didClickButtonAtIndex:(NSInteger)index {
    if ([cell isKindOfClass:[FBSAuthorCollectCell class]]) {
        FBSAuthorCollectItem *item = (FBSAuthorCollectItem *)cell.item;
        
        if (index < item.brand_users.count) {
            FBSBrandUser *user = [item.brand_users objectAtIndex:index];
            FBSAuthorViewController *authorVC = [[FBSAuthorViewController alloc] initWithAuthorId:user.ID];
            [self.navigationController pushViewController:authorVC animated:YES];
        }
    }
}

@end
