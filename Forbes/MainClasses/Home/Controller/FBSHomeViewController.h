//
//  FBSHomeViewController.h
//  Forbes
//
//  Created by 周灿华 on 2019/7/24.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface FBSHomeViewController : FBSViewController

///初始化频道
- (void)setupChannels;

@end

NS_ASSUME_NONNULL_END
