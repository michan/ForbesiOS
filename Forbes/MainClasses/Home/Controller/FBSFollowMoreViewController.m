//
//  FBSFollowMoreViewController.m
//  Forbes
//
//  Created by 周灿华 on 2019/8/24.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSFollowMoreViewController.h"
#import "FBSFollowMoreViewModel.h"
#import "FBSFollowModel.h"
#import "FBSFollowTableViewCell.h"
#import "FBSAuthorViewController.h"
#import "FBSFollowMoreSeachView.h"

@interface FBSFollowMoreViewController ()<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>
{
    
    UITableView *_tableView;
    NSMutableArray * _dataArray;
    NSInteger _page;
    FBSFollowMoreSeachView *_SeachView;

}
@end

@implementation FBSFollowMoreViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"更多关注";
    _dataArray = [NSMutableArray array];
    _page = 1;
    [self prepareUI];
}
-(void)prepareUI{
    
    _SeachView = [[[NSBundle mainBundle]loadNibNamed:@"FBSFollowMoreSeachView" owner:self options:nil]lastObject];
    [self.view addSubview:_SeachView];
    [_SeachView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(kNavTopMargin);
        make.height.mas_equalTo(45);
        make.left.mas_equalTo(0);
        make.right.mas_equalTo(0);
    }];
    ViewRadius(_SeachView.F, 5);
    _SeachView.F.returnKeyType = UIReturnKeySearch;
    _SeachView.F.delegate = self;
    [_SeachView.btn addTarget:self action:@selector(actionSeachBtn) forControlEvents:1<<6];
    
    
    _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0,kNavTopMargin , 0, 0) style:UITableViewStyleGrouped];
    _tableView.delegate =self;
    _tableView.dataSource = self;
    _tableView.estimatedRowHeight = 0;
    _tableView.estimatedSectionHeaderHeight = 0;
    _tableView.estimatedSectionFooterHeight = 0;
    _tableView.backgroundColor = COLOR_CommomBG;
    _tableView.separatorColor = COLOR_CommomBG;
    [self.view addSubview:_tableView];
    [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(kNavTopMargin+50 );
        make.bottom.mas_equalTo(0);
        make.left.mas_equalTo(0);
        make.right.mas_equalTo(0);
    }];
    
    _tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(RefreshHeader)];
    
    _tableView.mj_footer = [MJRefreshBackStateFooter footerWithRefreshingTarget:self refreshingAction:@selector(RefreshFooter)];
    
    [self loadData];

    
}

-(void)RefreshHeader{
    _page = 1;
    [_dataArray removeAllObjects];
    [self loadData];
    
}
-(void)RefreshFooter{
    _page ++;
    [self loadData];


}

-(void)loadData{
    
     NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        formatter.dateFormat = @"yyyy-MM-dd";
        NSString *plainText = [NSString stringWithFormat:@"%@%@",[FBSUserData sharedData].account,[formatter stringFromDate:[NSDate date]]];
        NSDictionary * parameters =@{
            @"user_id":[FBSUserData sharedData].useId?:@"",
//            @"token":[HLWJMD5 MD5ForLower32Bate:plainText]?:@"",
            @"page":@(_page),
            @"limit":@"20",
            @"search":_SeachView.F.text?:@""
            
        };
        [self showHUD];
        
        [MCNetworking ResponseNetworkGET_API:@"follow/list" parameters:parameters TheServer:0  cachePolicy:NO ISNeedLogin:NO RequestEnd:^(id  _Nonnull EndObject) {
            [self hideHUD];
            [_tableView.mj_header endRefreshing];
            [_tableView.mj_footer endRefreshing];

        } MCHttpCacheData:^(id  _Nonnull CacheObject) {
            
        } success:^(id  _Nonnull responseObject) {
            NSLog(@"responseObject == %@",responseObject);
            if (responseObject[@"data"][@"items"]) {
                for (NSDictionary *dic  in responseObject[@"data"][@"items"]) {
                    
                    FBSFollowModel * mdoel =[FBSFollowModel mj_objectWithKeyValues:dic];
                    [_dataArray addObject:mdoel];
                    
                }
            }
            
            [_tableView reloadData];
            
        } failure:^(NSURLSessionDataTask * _Nonnull operation, NSError * _Nonnull error, id  _Nonnull responseObject, NSString * _Nonnull description) {

        }];
    
    
}
-(void)actionSeachBtn{
    [_SeachView.F resignFirstResponder];
    
    [self RefreshHeader];
    
    
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    [_SeachView.F resignFirstResponder];

    [self RefreshHeader];

    
    return YES;
    
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _dataArray.count;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.01;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.0001;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 70;
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    FBSFollowTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"FBSFollowTableViewCell"];
    if (!cell) {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"FBSFollowTableViewCell" owner:self options:nil]lastObject];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
    }
    if (_dataArray.count) {
        FBSFollowModel * model =_dataArray[indexPath.row];
        [cell.imgview sd_setImageWithURL:[NSURL URLWithString:model.image_url]];
        cell.nameLbl.text = model.nickname?:@"";
        cell.lbl2.text = model.channel_title?:@"";
        cell.lbl3.text = [NSString stringWithFormat:@"%@ 关注",model.follow_count?:@"0"];
        if ([model.follow isEqualToString:@"N"]) {
            cell.guanzhuBtn.selected = NO;

        }
        else
            cell.guanzhuBtn.selected = YES;
        cell.guanzhuBtn.tag = 700+indexPath.row;
        [cell.guanzhuBtn addTarget:self action:@selector(actionguanzhuBtn:) forControlEvents:1<<6];
    }
    
    
    return cell;

    
    return [[UITableViewCell alloc]init];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (_dataArray.count) {
        FBSFollowModel * model =_dataArray[indexPath.row];
        
        FBSAuthorViewController *authorVC = [[FBSAuthorViewController alloc] initWithAuthorId:model.id?:@""];
        [self.navigationController pushViewController:authorVC animated:YES];

    }


}
-(void)actionguanzhuBtn:(UIButton*)btn{
    NSInteger idnex =btn.tag - 700;
    if (_dataArray.count) {
        FBSFollowModel * model =_dataArray[idnex];
        [self follow:model];
        
    }
}
-(void)follow:(FBSFollowModel*)model{
    
    NSString *action = @"unfollow";
    if ([model.follow isEqualToString:@"N"]) {
        action =@"follow";
    }
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        formatter.dateFormat = @"yyyy-MM-dd";
        NSString *plainText = [NSString stringWithFormat:@"%@%@",[FBSUserData sharedData].account,[formatter stringFromDate:[NSDate date]]];
        NSDictionary * parameters =@{
            
            @"user_id":@([[FBSUserData sharedData].useId integerValue]),
            @"token":[HLWJMD5 MD5ForLower32Bate:plainText]?:@"",
            @"follow_user_id":@([model.id integerValue]),
            @"action":action
            
        };
        [self showHUD];
        
        [MCNetworking ResponseNetworkPOST_API:@"user/follow" parameters:parameters  TheServer:0 cachePolicy:NO ISNeedLogin:YES RequestEnd:^(id  _Nonnull EndObject) {
            [self hideHUD];

        } MCHttpCacheData:^(id  _Nonnull CacheObject) {
            
        } success:^(id  _Nonnull responseObject) {
            NSLog(@"responseObject == %@",responseObject);
            
            if ([action isEqualToString:@"follow"]) {
                [self makeToast:@"关注成功"];
                model.follow = @"Y";

            }
            else{
            [self makeToast:@"取消成功"];
                model.follow = @"N";

            }
            [_tableView reloadData];
    //        _HeaderUser.attent.selected = YES;
            
            //发送通知
            [[NSNotificationCenter defaultCenter] postNotificationName:@"didsFBSFollowViewController" object:nil];
        } failure:^(NSURLSessionDataTask * _Nonnull operation, NSError * _Nonnull error, id  _Nonnull responseObject, NSString * _Nonnull description) {
            [self makeToast:@"请求失败"];

        }];
    
    
}


@end
