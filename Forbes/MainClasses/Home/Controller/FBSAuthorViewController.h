//
//  FBSAuthorViewController.h
//  Forbes
//
//  Created by 周灿华 on 2019/9/25.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSTableViewController.h"

@interface FBSAuthorViewController : FBSTableViewController

- (instancetype)initWithAuthorId:(NSString *)authorId;

@end

