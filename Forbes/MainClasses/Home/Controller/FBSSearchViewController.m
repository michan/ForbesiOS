//
//  FBSSearchViewController.m
//  Forbes
//
//  Created by 周灿华 on 2019/9/25.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSSearchViewController.h"
#import "FBSSearchViewModel.h"
#import "FBSTableViewController+Refresh.h"
#import "DetailViewController.h"         //文章和活动详情
#import "FBSRankDetailViewController.h"  //榜单详情
#import "FBSVideoDetailViewController.h" //视频详情

@interface FBSSearchViewController ()<UITextFieldDelegate>
@property (nonatomic, strong) FBSSearchViewModel *viewModel;
@property (nonatomic, strong) UIView *searchView;
@property (nonatomic, strong) UIImageView *searchIcon;
@property (nonatomic, strong) UITextField *searchTF;
@property (nonatomic, strong) UIButton *operateButton;
@end

@implementation FBSSearchViewController
@synthesize viewModel = _viewModel;

dispatch_semaphore_t semaphoreVar_;

#pragma mark - life cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initNavBar];
    
    self.registerDictionary = @{
                                [FBSOneImageItem reuseIdentifier] : [FBSOneImageCell class],
                                [FBSThreeImageItem reuseIdentifier] : [FBSThreeImageCell class],
                                [FBSOnlyTextItem reuseIdentifier] : [FBSOnlyTextCell class],
                                 [FBSSearchTagsItem reuseIdentifier] : [FBSSearchTagsCell class],
                                };

}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}


#pragma mark - private method

- (void)createload {
    WEAKSELF
    [self backNormalFooterRefreshingActionBlock:^(FooterConfigBlock  _Nonnull footerConfig) {
        [self.viewModel rqToSearch:self.searchTF.text complete:^(BOOL success, NSInteger count) {
            if (success) {
                [weakSelf.tableView reloadData];
            }
            
            if(footerConfig){
                footerConfig(count);
            }
        }];
    }];
}


- (void)initNavBar {
    semaphoreVar_ = dispatch_semaphore_create(1);
    
    self.navigationBackButtonHidden = YES;
    self.navigationBar.titleView.hidden = YES;
    self.navigationBar.bottomLine.hidden = YES;
    self.navigationBar.contentView.backgroundColor = Hexcolor(0x191919);
    
    [self.navigationBar addSubview:self.searchView];
    [self.navigationBar addSubview:self.operateButton];
    [self.searchView addSubview:self.searchIcon];
    [self.searchView addSubview:self.searchTF];

    [self.operateButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(0);
        make.width.mas_equalTo(60);
        make.centerY.mas_equalTo(self.navigationBar.titleLabel);
    }];
    
    [self.searchView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.right.mas_equalTo(self.operateButton.mas_left);
        make.height.mas_equalTo(30);
        make.centerY.mas_equalTo(self.navigationBar.titleLabel);
    }];
    
    [self.searchIcon mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_offset(13);
        make.centerY.mas_offset(0);
        make.width.mas_equalTo(16);
        make.height.mas_equalTo(16);
    }];

    
    [self.searchTF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(0);
        make.left.mas_equalTo(self.searchIcon.mas_right).offset(7);
        make.right.mas_equalTo(-10);
        make.height.mas_equalTo(30);
    }];
    
    //监听输入栏文字变化
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textChanged:) name:UITextFieldTextDidChangeNotification object:self.searchTF];
}


- (void)operateAction:(UIButton *)sender {
    [self.searchTF resignFirstResponder];
    
    if (sender.selected) {
        [self searchAction:self.searchTF.text immediately:YES];
    } else {
        [self.navigationController popViewControllerAnimated:NO];
    }
}


- (void)searchAction:(NSString *)keyword immediately:(BOOL)immediately {
    FBSLog(@"搜索的关键字是 : %@", self.searchTF.text);
    
    //每次搜索复位
    self.page = @(1);
    self.pageCount = @(20);
    
    if (keyword.length) {
        if (immediately) { //立即搜索
            WEAKSELF
            
            [self showHUD];
            [self.viewModel rqToSearch:keyword complete:^(BOOL success, NSInteger count) {
                [weakSelf.tableView reloadData];
                [weakSelf hideHUD];

                if (count) {
                    [weakSelf searchKeyWordSave:keyword];
                    
                    if (count < weakSelf.pageCount.integerValue) {
                        weakSelf.tableView.mj_footer = nil;
                    } else {
                        [weakSelf createload];
                    }
                }
            }];
        }
        
    } else {
        [self clearInputKeyWord];
    }
}


- (void)searchKeyWordSave:(NSString *)keyword {
    
    dispatch_semaphore_wait(semaphoreVar_, DISPATCH_TIME_FOREVER);
    NSString *savaPath = SaveKeywordFilePath;
    
    //新建一个plist 保存所有的keyeod
    NSMutableArray *keywordArr = [[NSArray arrayWithContentsOfFile:savaPath] mutableCopy];
    
    if (!keywordArr) {
        keywordArr = [NSMutableArray array];
    }
    
    NSMutableArray *temo = [keywordArr mutableCopy];
    
    for (NSString *key  in temo) {
        if ([key isEqualToString:keyword] ) {
            [keywordArr removeObject:key];
        }
    }
    
    FBSLog(@"保存的路径 %@",keywordArr);
    
    [keywordArr insertObject:keyword atIndex:0];
    
    BOOL succes =  [keywordArr writeToFile:savaPath atomically:YES];
    FBSLog(@"保存关键字 %@",succes ? @"成功 ": @"失败");
    
    self.viewModel.tags = [keywordArr copy];
    
    dispatch_semaphore_signal(semaphoreVar_);
    
}

#pragma mark - 监听输入框文字变化

- (void)textChanged:(NSNotification *)notification {
    
    if (notification.object != self.searchTF) {
        return ;
    }
    
    self.operateButton.selected = self.searchTF.text.length;
    if (self.searchTF.text.length == 0) {
        [self clearInputKeyWord];
    }
}


- (void)clearInputKeyWord {
    [self.viewModel clearInputKeyWord];
    self.tableView.mj_footer = nil;
    [self.tableView reloadData];
    [self.tableView setContentOffset:CGPointMake(0, -kNavTopMargin)];

}


#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    return YES;
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    return YES;
}


- (void)textFieldDidEndEditing:(UITextField *)textField {
//    [self searchAction:textField.text immediately:YES];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    [self searchAction:textField.text immediately:YES];
    return YES;
}


#pragma mark - LgCLBaseCellDelegate

- (void)fbsCell:(FBSCell *)cell didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([cell isKindOfClass:[FBSMainCell class]]) {
        FBSMainItem *item = (FBSMainItem *)cell.item;
        FBSViewController *detailVC;
        
        switch (item.detailType) {
                
            case DetailPageTypeArticle:
                detailVC = [[DetailViewController alloc] initWithArticleId:item.ID htmlAddressL:@"article/" cellAddress:@"interest/"];
                break;
                
            case DetailPageTypeRank:
                detailVC = [[FBSRankDetailViewController alloc] initWithRankId:item.ID];
                break;

                
            case DetailPageTypeActivity:
                detailVC = [[DetailViewController alloc] initWithArticleId:item.ID htmlAddressL:@"activity/" cellAddress:nil];
                break;
                
            case DetailPageTypeVideo:
            {
                FBSVideoDetailViewController *vc = [[FBSVideoDetailViewController alloc] init];
                vc.imgV = item.imgUrl;
                if (item.videoUrl) {
                    vc.video_url = [NSURL URLWithString:item.videoUrl];
                }
                vc.titleContnt = item.content;
                vc.currentTime = 0;
                vc.id = item.ID;
                detailVC = vc;
            }
                break;
                
            default:
                break;
        }
        
        [self.navigationController pushViewController:detailVC animated:YES];
    }
}


- (void)fbsCell:(FBSCell *)cell didClickButtonAtIndex:(NSInteger)index {
    if ([cell isKindOfClass:[FBSSearchTagsCell class]]) {
        [self.searchTF resignFirstResponder];
        
        NSString *keyword = [self.viewModel.tags objectAtIndex:index];
        
        FBSLog(@"点击了tag: %ld  文字是: %@",(long)index,keyword);
        self.searchTF.text = keyword;
        self.operateButton.selected = YES;
        [self searchAction:keyword immediately:YES];
        
    }
}

#pragma mark - property

- (FBSSearchViewModel *)viewModel {
    if (!_viewModel) {
        _viewModel  = [[FBSSearchViewModel alloc] init];
    }
    return _viewModel;
}



- (UIView *)searchView {
    if (!_searchView) {
        _searchView = [[UIView alloc] init];
        _searchView.backgroundColor = Hexcolor(0x333333);
        _searchView.layer.cornerRadius = 5;
        _searchView.layer.masksToBounds = YES;
    }
    return _searchView;
}


- (UIImageView *)searchIcon {
    if (!_searchIcon) {
        _searchIcon  = [[UIImageView alloc] init];
        _searchIcon.image = kImageWithName(@"home_search_icon_nor");
    }
    return _searchIcon;
}


- (UITextField *)searchTF {
    if (!_searchTF) {
        _searchTF = [[UITextField alloc] init];
        _searchTF.tintColor =  [[UIColor whiteColor] colorWithAlphaComponent:0.6];
        _searchTF.textColor = [[UIColor whiteColor] colorWithAlphaComponent:0.6];
        _searchTF.font = [UIFont systemFontOfSize:14];
        
        _searchTF.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"30 under 30" attributes:@{NSFontAttributeName:_searchTF.font,NSForegroundColorAttributeName:_searchTF.textColor}];
        _searchTF.delegate = self;
        _searchTF.clearButtonMode = UITextFieldViewModeWhileEditing;
        _searchTF.returnKeyType = UIReturnKeySearch;
    }
    return _searchTF;
}


- (UIButton *)operateButton {
    if (!_operateButton) {
        _operateButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _operateButton.titleLabel.font = kLabelFontSize(15);
        [_operateButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_operateButton setTitle:@"取消" forState:UIControlStateNormal];
        [_operateButton setTitle:@"搜索" forState:UIControlStateSelected];
        [_operateButton addTarget:self action:@selector(operateAction:) forControlEvents:UIControlEventTouchUpInside];
        
    }
    return _operateButton;
}


@end
