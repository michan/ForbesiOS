//
//  FBSKeyNewsViewController.m
//  Forbes
//
//  Created by 周灿华 on 2019/9/8.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSKeyNewsViewController.h"
#import "FBSKeyNewsViewModel.h"
#import "DetailViewController.h"
#import "FBSTableViewController+Refresh.h"
#import "FBSTopicViewController.h"
#import "AdvertAopTableView.h"

@interface FBSKeyNewsViewController ()
@property (nonatomic, strong) FBSKeyNewsViewModel *viewModel;
///只是声明，防止提前释放
@property (nonatomic, strong) AdvertAopTableView *aopDemo;
@end

@implementation FBSKeyNewsViewController
@synthesize viewModel = _viewModel;

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationBarHidden = YES;
    
    self.registerDictionary = @{
                                [FBSBannerItem reuseIdentifier] : [FBSBannerCell class],
                                [FBSOneImageItem reuseIdentifier] : [FBSOneImageCell class],
                                [FBSThreeImageItem reuseIdentifier] : [FBSThreeImageCell class],
                                [FBSOnlyTextItem reuseIdentifier] : [FBSOnlyTextCell class]                                };
    [self createload];
    
    [self adsInit];
}

- (void)createload {
    self.page = @(1);
    self.pageCount = @(50);
    WEAKSELF
    [self normalHeaderRefreshingActionBlock:^(FooterConfigBlock  _Nonnull footerConfig) {
        [weakSelf.viewModel rqKeyNewsData:^(BOOL success) {
            if (success) {
                [weakSelf.tableView reloadData];
            }
            
            if(footerConfig){
                footerConfig(60);
            }

        }];
    }];
    
    
    [self backNormalFooterRefreshingActionBlock:^(FooterConfigBlock  _Nonnull footerConfig) {
        [weakSelf.viewModel rqAllArticles:^(BOOL success, NSInteger count) {
            if (success) {
                [weakSelf.tableView reloadData];
            }
            
            if(footerConfig){
                footerConfig(count);
            }

        }];
    }];
}


///广告初始化
- (void)adsInit {
    NSString *position_id = @"3";  //首页广告
    if (position_id) {
        self.aopDemo = [AdvertAopTableView new];
        self.aopDemo.position_id = position_id;
        self.aopDemo.vc = self;
        self.aopDemo.softArray = @[@"3",@"8",@"13",@"18",@"23",@"28"];
        self.aopDemo.aopUtils = self.tableView.aop_utils;
    }
}


#pragma mark - property

- (FBSKeyNewsViewModel *)viewModel {
    if (!_viewModel) {
        _viewModel  = [[FBSKeyNewsViewModel alloc] init];
    }
    return _viewModel;
}

#pragma mark - delegate
- (void)fbsCell:(FBSCell *)cell didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([cell isKindOfClass:[FBSMainCell class]]) {
        FBSMainItem *item = (FBSMainItem *)cell.item;
        if (item.detailType == DetailPageTypeTopic) {
            //专题
            FBSTopicViewController *topic = [[FBSTopicViewController alloc] initWithTopicId:item.ID];
            [self.navigationController pushViewController:topic animated:YES];
            
        } else {
            DetailViewController *detail = [[DetailViewController alloc] initWithArticleId:item.ID htmlAddressL:@"article/" cellAddress:@"interest/"];
            detail.position_id = @"43";
            [self.navigationController pushViewController:detail animated:YES];
        }
    }
}


- (void)fbsCell:(FBSCell *)cell didClickButtonAtIndex:(NSInteger)index {
    if ([cell isKindOfClass:[FBSBannerCell class]]) {
        FBSBannerItem *item = (FBSBannerItem *)cell.item;
        if (index < item.datas.count) {
            FBSBannerInfo *bannerInfo = [item.datas objectAtIndex:index];
            DetailViewController *detail = [[DetailViewController alloc] initWithArticleId:bannerInfo.ID htmlAddressL:@"article/" cellAddress:@"interest/"];
            detail.position_id = @"43";
            [self.navigationController pushViewController:detail animated:YES];
        }
    }
}


@end
