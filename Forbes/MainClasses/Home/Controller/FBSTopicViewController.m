//
//  FBSTopicViewController.m
//  Forbes
//
//  Created by 周灿华 on 2019/9/25.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSTopicViewController.h"
#import "FBSTopicViewModel.h"
#import "DetailViewController.h"

@interface FBSTopicViewController ()
@property (nonatomic, strong) FBSTopicViewModel *viewModel;
@end

@implementation FBSTopicViewController
@synthesize viewModel = _viewModel;

- (instancetype)initWithTopicId:(NSString *)topicId {
    self = [super init];
    if (self) {
        self.viewModel = [[FBSTopicViewModel alloc] initWithTopicId:topicId];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"专题";
    
    self.registerDictionary = @{
                                [FBSTopicHeadItem reuseIdentifier] : [FBSTopicHeadCell class],
                                [FBSOneImageItem reuseIdentifier] : [FBSOneImageCell class],
                                [FBSThreeImageItem reuseIdentifier] : [FBSThreeImageCell class],
                                [FBSOnlyTextItem reuseIdentifier] : [FBSOnlyTextCell class],
                                [FBSGroupHeadItem reuseIdentifier] : [FBSGroupHeadCell class],
                                [FBSSpaceItem reuseIdentifier] : [FBSSpaceCell class]
                                };

    WEAKSELF
    [self showHUD];
    [self.viewModel rqTopicData:^(BOOL success) {
        [weakSelf.tableView reloadData];
        [weakSelf hideHUD];
    }];
    
}


#pragma mark - FBSCellDelegate

- (void)fbsCell:(FBSCell *)cell didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([cell isKindOfClass:[FBSMainCell class]]) {
        FBSMainItem *item = (FBSMainItem *)cell.item;
        
        DetailViewController *detail = [[DetailViewController alloc] initWithArticleId:item.ID htmlAddressL:@"article/" cellAddress:@"interest/"];
        [self.navigationController pushViewController:detail animated:YES];
    }
}



#pragma mark - property

- (FBSTopicViewModel *)viewModel {
    if (!_viewModel) {
        _viewModel  = [[FBSTopicViewModel alloc] init];
    }
    return _viewModel;
}



@end
