//
//  FBSAuthorViewController.m
//  Forbes
//
//  Created by 周灿华 on 2019/9/25.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSAuthorViewController.h"
#import "FBSAuthorViewModel.h"
#import "FBSTableViewController+Refresh.h"
#import "DetailViewController.h"

@interface FBSAuthorViewController ()
@property (nonatomic, strong) FBSAuthorViewModel *viewModel;
@end

@implementation FBSAuthorViewController
@synthesize viewModel = _viewModel;


- (instancetype)initWithAuthorId:(NSString *)authorId {
    self = [super init];
    if (self) {
        self.viewModel = [[FBSAuthorViewModel alloc] initWithAuthorId:authorId];
    }
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.registerDictionary = @{
                                [FBSOneImageItem reuseIdentifier] : [FBSOneImageCell class],
                                [FBSOnlyTextItem reuseIdentifier] : [FBSOnlyTextCell class],
                                [FBSGroupHeadItem reuseIdentifier] : [FBSGroupHeadCell class],
                                [FBSAuthorHeadItem reuseIdentifier] : [FBSAuthorHeadCell class],
                                };
    [self createload];
}


- (void)createload {
    self.page = @(1);
    self.pageCount = @(6);
    
    WEAKSELF
    [self normalHeaderRefreshingActionBlock:^(FooterConfigBlock  _Nonnull footerConfig) {
        
        [weakSelf.viewModel rqAuthorData:^(BOOL success) {
            if (success) {
                [weakSelf.tableView reloadData];
                if(footerConfig){
                    footerConfig(10);
                }
            }
            
        }];
        
    }];
    [self backNormalFooterRefreshingActionBlock:^(FooterConfigBlock  _Nonnull footerConfig) {
        [weakSelf.viewModel rqIssue:^(BOOL success, NSInteger count) {
            if (success) {
                [weakSelf.tableView reloadData];
                if(footerConfig){
                    footerConfig(count);
                }
            }else{
                
            }
        }];
    }];
}

#pragma mark - delegate

- (void)fbsCell:(FBSCell *)cell didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([cell isKindOfClass:[FBSMainCell class]]) {
        FBSMainItem *item = (FBSMainItem *)cell.item;
        
        DetailViewController *detail = [[DetailViewController alloc] initWithArticleId:item.ID htmlAddressL:@"article/" cellAddress:@"interest/"];
        [self.navigationController pushViewController:detail animated:YES];
    }
}

@end

