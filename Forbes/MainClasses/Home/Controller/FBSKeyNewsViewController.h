//
//  FBSKeyNewsViewController.h
//  Forbes
//
//  Created by 周灿华 on 2019/9/8.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSTableViewController.h"

NS_ASSUME_NONNULL_BEGIN

///要闻频道
@interface FBSKeyNewsViewController : FBSTableViewController

@end

NS_ASSUME_NONNULL_END
