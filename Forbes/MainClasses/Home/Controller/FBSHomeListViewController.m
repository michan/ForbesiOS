//
//  FBSHomeListViewController.m
//  Forbes
//
//  Created by 周灿华 on 2019/7/26.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSHomeListViewController.h"
#import "FBSHomeListViewModel.h"
#import "FBSDFPBannerViewController.h"
#import "FBSTableViewController+Refresh.h"
#import "DetailViewController.h"
#import "AdvertAopTableView.h"

@interface FBSHomeListViewController ()
@property (nonatomic, strong) FBSHomeListViewModel *viewModel;
///只是声明，防止提前释放
@property (nonatomic, strong) AdvertAopTableView *aopDemo;
@end

@implementation FBSHomeListViewController
@synthesize viewModel = _viewModel;


- (instancetype)initWithChannelId:(NSString *)channelId {
    self = [super init];
    if (self) {
        self.viewModel = [[FBSHomeListViewModel alloc] initWithChannelId:channelId];
    }
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationBarHidden = YES;
    
    self.registerDictionary = @{
                                [FBSBannerItem reuseIdentifier] : [FBSBannerCell class],
                                [FBSOneImageItem reuseIdentifier] : [FBSOneImageCell class],
                                [FBSThreeImageItem reuseIdentifier] : [FBSThreeImageCell class],
                                [FBSOnlyTextItem reuseIdentifier] : [FBSOnlyTextCell class]
                                };
    [self createload];
    
    [self adsInit];;
}


- (void)createload {
    self.page = @(1);
    self.pageCount = @(6);
    WEAKSELF
    [self normalHeaderRefreshingActionBlock:^(FooterConfigBlock  _Nonnull footerConfig) {
        
        [weakSelf.viewModel rqRecommendAndNews:^(BOOL success) {
            if (success) {
                [weakSelf.tableView reloadData];
                if(footerConfig){
                    footerConfig(10);
                }
            }
        }];
        
    }];
    [self backNormalFooterRefreshingActionBlock:^(FooterConfigBlock  _Nonnull footerConfig) {
        [weakSelf.viewModel rqLatestNews:^(BOOL success, NSInteger count) {
            if (success) {
                [weakSelf.tableView reloadData];
                if(footerConfig){
                    footerConfig(count);
                }
            }else{
                
            }
        }];
    }];
}

///广告初始化
- (void)adsInit {
    NSString *position_id = [self getPositonIdWithCannelId:self.viewModel.channelId];
    
    if (position_id) {
        self.aopDemo = [AdvertAopTableView new];
        self.aopDemo.position_id = position_id;
        self.aopDemo.vc = self;
        self.aopDemo.softArray = @[@"4",@"10",@"16",@"22",@"28"];
        self.aopDemo.aopUtils = self.tableView.aop_utils;
    }
}

#pragma mark - delegate

//- (void)fbsCell:(FBSCell *)cell didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
//    FBSItem *item = [self.viewModel itemAtRow:indexPath.row inSection:indexPath.section];
//
//    FBSDFPBannerViewController *detail = [[FBSDFPBannerViewController alloc] init];
//    [self.navigationController pushViewController:detail animated:YES];
//
//
//}


- (void)fbsCell:(FBSCell *)cell didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([cell isKindOfClass:[FBSMainCell class]]) {
        FBSMainItem *item = (FBSMainItem *)cell.item;

        DetailViewController *detail = [[DetailViewController alloc] initWithArticleId:item.ID htmlAddressL:@"article/" cellAddress:@"interest/"];
        detail.position_id = [self getArticalAPositonIdWithCannelId:self.viewModel.channelId];
        [self.navigationController pushViewController:detail animated:YES];
    }
    
    
//    [[HLWJShareSDKManager sharedInstance] shareWithTitle:@"分享了" iconArray:nil content:@"我是分享的内容" url:@"https://www.baidu.com" successHandler:^{
//       FBSLog(@"%@",@"分享成功");
//    } failHandler:^{
//        FBSLog(@"%@",@"分享失败");
//    }];
}


- (void)fbsCell:(FBSCell *)cell didClickButtonAtIndex:(NSInteger)index {
    if ([cell isKindOfClass:[FBSBannerCell class]]) {
        FBSBannerItem *item = (FBSBannerItem *)cell.item;
        if (index < item.datas.count) {
            FBSBannerInfo *bannerInfo = [item.datas objectAtIndex:index];
            if (bannerInfo.isAd) {
                FBSLog(@"%@",@"点击的是广告");
                return ;
            }
            DetailViewController *detail = [[DetailViewController alloc] initWithArticleId:bannerInfo.ID htmlAddressL:@"article/" cellAddress:@"interest/"];
            detail.position_id = [self getArticalAPositonIdWithCannelId:self.viewModel.channelId];
            [self.navigationController pushViewController:detail animated:YES];
        }
    }
}


- (NSString *)getPositonIdWithCannelId:(NSString *)channelId {
    NSDictionary *positonDic = @{
             @"7" : @"9",   //富豪频道位
             @"8" : @"10",  //创业频道位
             @"9" : @"13",  //科技频道位
             @"10" : @"19", //商业频道位
             @"11" : @"22", //城市频道位
             @"12" : @"25", //城市频道位
             @"13" : @"28", //生活频道位
             @"14" : @"31", //美食频道位
             };
    
    if (channelId.length) {
        return positonDic[channelId];
    }
    return nil;
}
- (NSString *)getArticalAPositonIdWithCannelId:(NSString *)channelId {
    NSDictionary *positonDic = @{
                                 @"7" : @"14",   //富豪频道位
                                 @"8" : @"15",  //创业频道位
                                 @"9" : @"17",  //科技频道位
                                 @"10" : @"21", //商业频道位
                                 @"11" : @"24", //城市频道位
                                 @"12" : @"27", //城市频道位
                                 @"13" : @"30", //生活频道位
                                 @"14" : @"32", //美食频道位
                                 };
    
    if (channelId.length) {
        return positonDic[channelId];
    }
    return nil;
}

@end
