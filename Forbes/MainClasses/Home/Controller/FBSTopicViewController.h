//
//  FBSTopicViewController.h
//  Forbes
//
//  Created by 周灿华 on 2019/9/25.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSTableViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface FBSTopicViewController : FBSTableViewController

- (instancetype)initWithTopicId:(NSString *)topicId;

@end

NS_ASSUME_NONNULL_END
