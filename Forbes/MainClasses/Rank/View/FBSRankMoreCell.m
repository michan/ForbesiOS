//
//  FBSRankMoreCell.m
//  Forbes
//
//  Created by 周灿华 on 2019/9/24.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSRankMoreCell.h"

@implementation FBSRankMoreItem

@end


@interface FBSRankMoreCell ()
@property (nonatomic, strong) UIButton *moreBtn;
@end

@implementation FBSRankMoreCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self.contentView addSubview:self.moreBtn];
        
        [self.moreBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(5);
            make.centerX.mas_equalTo(0);
            make.width.mas_equalTo(350);
            make.height.mas_equalTo(41);
        }];
    }
    
    return self;
}



- (void)setItem:(FBSRankMoreItem *)item {
    if (![item isKindOfClass:[FBSRankMoreItem class]]) {
        return ;
    }
    
    [super setItem:item];
    
}


- (void)clickAction:(UIButton *)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(fbsCell:didClickButtonAtIndex:)]) {
        [self.delegate fbsCell:self didClickButtonAtIndex:0];
    }
}

#pragma mark - property

- (UIButton *)moreBtn {
    if (!_moreBtn) {
        _moreBtn  = [UIButton buttonWithType:UIButtonTypeCustom];
        _moreBtn.backgroundColor = Hexcolor(0xF6F7F9);
        _moreBtn.titleLabel.font = kLabelFontSize15;
        [_moreBtn setTitleColor:Hexcolor(0x333333) forState:UIControlStateNormal];
        [_moreBtn setTitle:@"查看更多榜单>" forState:UIControlStateNormal];
        [_moreBtn addTarget:self action:@selector(clickAction:) forControlEvents:UIControlEventTouchUpInside];
        _moreBtn.layer.shadowColor = [UIColor lightGrayColor].CGColor;
        _moreBtn.layer.shadowOpacity = 0.7;
        _moreBtn.layer.shadowRadius = 3.0;
        _moreBtn.layer.shadowOffset = CGSizeMake(0,0);
        _moreBtn.layer.cornerRadius = 5;
    }
    return _moreBtn;
}

@end
