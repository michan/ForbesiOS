//
//  FBSRankListCell.h
//  Forbes
//
//  Created by 周灿华 on 2019/9/24.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSCell.h"

@interface FBSRankListItem : FBSItem
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *rankId;
@end

@interface FBSRankListCell : FBSCell

@end

