//
//  FBSRankSpinner.m
//  Forbes
//
//  Created by 周灿华 on 2019/9/26.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSRankSpinner.h"
#import <Foundation/Foundation.h>

@interface FBSRankSpinner() <UITableViewDelegate, UITableViewDataSource>

// 展开收起提示图片
@property (nonatomic, strong) UIImageView *unfoldImage;
// 展示列表
@property (nonatomic, strong) UITableView *table;
// 阴影
@property (nonatomic, strong) CALayer *shadowLayer;
// 遮罩视图
@property (nonatomic, strong) UIView *coverView;
// 数据源
@property (nonatomic, strong) NSArray *dataArray;
//左边文字
@property (nonatomic, strong) UILabel *leftL;
//值文字
@property (nonatomic, strong) UILabel *valueL;

@end

@implementation FBSRankSpinner

#pragma mark - Lazy
- (UITableView *)table {
    if (!_table) {
        _table = [[UITableView alloc] init];
        _table.delegate = self;
        _table.dataSource = self;
        _table.rowHeight = self.listItemHeight;
        [_table setSeparatorStyle:UITableViewCellSeparatorStyleNone];
        _table.layer.borderWidth = 0.5;
        _table.layer.borderColor = [UIColor colorWithRed:183/255.0 green:183/255.0 blue:183/255.0 alpha:1].CGColor;
        _table.layer.cornerRadius = 5;
        _table.bounces = YES;
        
        // 添加阴影
        self.shadowLayer = [[CALayer alloc] init];
        self.shadowLayer.bounds = _table.bounds;
        self.shadowLayer.backgroundColor = [UIColor whiteColor].CGColor;
        self.shadowLayer.cornerRadius = 5;
        // 设置阴影
        self.shadowLayer.shadowColor = [UIColor grayColor].CGColor;
        self.shadowLayer.shadowOffset = CGSizeMake(2, 2);
        self.shadowLayer.shadowOpacity = 0.9;
    }
    return _table;
}
- (UIView *)coverView {
    if (!_coverView) {
        _coverView = [[UIView alloc] initWithFrame:[self getWindow].bounds];
        _coverView.backgroundColor = [UIColor clearColor];
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showTable)];
        [_coverView addGestureRecognizer:tap];
        
        UIPanGestureRecognizer *pan = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(hide)];
        
        [_coverView addGestureRecognizer:pan];
    }
    return _coverView;
}


- (UILabel *)leftL {
    if (!_leftL) {
        _leftL  = [[UILabel alloc] init];
        _leftL.font = [UIFont systemFontOfSize:14];
        _leftL.textColor = Hexcolor(0x999999);
        _leftL.text = @"";
    }
    return _leftL;
}

- (UILabel *)valueL {
    if (!_valueL) {
        _valueL  = [[UILabel alloc] init];
        _valueL.font = [UIFont systemFontOfSize:14];
        _valueL.textColor = Hexcolor(0x333333);
        _valueL.textAlignment = NSTextAlignmentRight;
        _valueL.text = @"";
    }
    return _valueL;
}


#pragma mark - LifeCycle
- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        
        [self initialize];
        [self creatUI];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        
        [self initialize];
        [self creatUI];
    }
    return self;
}

/**
 数据初始化
 */
- (void)initialize {
    self.selectedIndex = 0;
    self.backColor = [UIColor whiteColor];
    _selectedBackColor = [UIColor whiteColor];
    _listItemFont = [UIFont systemFontOfSize:14];
    _listItemHeight = 35.0f;
    _listItemTextColor = [UIColor blackColor];
    _maxShowLines = 5;
    _spinnerBackColor = [UIColor whiteColor];
    self.cornerRadius = 3;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    CGFloat width = self.frame.size.width;
    CGFloat height = self.frame.size.height;
    
    self.unfoldImage.frame = CGRectMake(width - 10 - 2, height/2.0 - 12/2.0, 12, 12);

    self.leftL.frame = CGRectMake(0, height/2.0 - 14/2.0, width * 0.5, 14);
    
    self.valueL.frame = CGRectMake(CGRectGetMinX(self.unfoldImage.frame) - 10 - 0.5 * width, height/2.0 - 14/2.0, width * 0.5, 14);

}

#pragma mark - UI
- (void)creatUI {
    
    [self addTarget:self action:@selector(showTable) forControlEvents:UIControlEventTouchUpInside];
    [self setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    
    [self addSubview:self.leftL];
    [self addSubview:self.valueL];
    
    self.unfoldImage = [[UIImageView alloc] init];
    self.unfoldImage.image = kImageWithName(@"rank_spinner");;
    [self.unfoldImage setContentMode:UIViewContentModeScaleAspectFit];
    [self addSubview:self.unfoldImage];
    
}

#pragma mark - private

- (void)setSelected:(BOOL)selected {
    [super setSelected:selected];
    self.backgroundColor = selected?_selectedBackColor:_backColor;
}


- (void)showTable {
    
    if (self.selected) {
        [self hide];
    } else {
        [self show];
        
        if (self.delegate && [self.delegate respondsToSelector:@selector(didClickSpinner:)]) {
            [self.delegate didClickSpinner:self];
        }
    }
    
    self.selected = !self.selected;
}

/**
 展示列表
 */
- (void)show {
    [self.table reloadData];
    
    NSInteger count = 0;
    if (self.delegate && [self.delegate respondsToSelector:@selector(itemsCountOfSpinner:)]) {
        count = [self.delegate itemsCountOfSpinner:self];
    }
    
    NSInteger padding = 5;
    CGFloat tableHeight = count > _maxShowLines ? _listItemHeight * _maxShowLines : _listItemHeight * count - 5;
    CGRect selfFrame = [self convertRect:self.bounds toView:[self getWindow]];
    
    UIScrollView *superScrollView = [self findSuperScrollViewWithView:self];
    
    CGFloat rotation = M_PI/2.0;
    if (superScrollView) {
        
        CGRect selfFrameInScrollView = [self convertRect:self.bounds toView:superScrollView];
        
        CGFloat insertV = CGRectGetMaxY(selfFrameInScrollView) + padding + tableHeight - superScrollView.frame.size.height - superScrollView.contentOffset.y;
        if (insertV > 0) {
            
            if (superScrollView.contentOffset.y + superScrollView.frame.size.height + insertV < superScrollView.contentSize.height) {
                
                CGPoint offset = superScrollView.contentOffset;
                offset.y += insertV;
                [superScrollView setContentOffset:offset];
                _table.frame = CGRectMake(selfFrame.origin.x, CGRectGetMaxY(selfFrame) + 5 - insertV, selfFrame.size.width, tableHeight);
            }
            else {
                rotation = -rotation;
                _table.frame = CGRectMake(selfFrame.origin.x, selfFrame.origin.y - padding - tableHeight, selfFrame.size.width, tableHeight);
            }
        }
        else {
            _table.frame = CGRectMake(selfFrame.origin.x, CGRectGetMaxY(selfFrame) + 5, selfFrame.size.width, tableHeight);
        }
    }
    else {
        
        CGRect selfFrameInScrollView = [self convertRect:self.bounds toView:[self getWindow]];
        
        CGFloat insertV = CGRectGetMaxY(selfFrameInScrollView) + padding + tableHeight - [self getWindow].frame.size.height;
        if (insertV > 0) {
            rotation = -rotation;
            _table.frame = CGRectMake(selfFrame.origin.x, selfFrame.origin.y - padding - tableHeight, selfFrame.size.width, tableHeight);
        }
        else {
            
            _table.frame = CGRectMake(selfFrame.origin.x, CGRectGetMaxY(selfFrame) + 5, selfFrame.size.width, tableHeight);
        }
    }
    
    
    
    
    // 阴影
    //设置中心点
    self.shadowLayer.position = CGPointMake(_table.frame.origin.x + _table.frame.size.width/2, _table.frame.origin.y + _table.frame.size.height/2);
    //设置大小
    self.shadowLayer.bounds = _table.bounds;
    
    
    [UIView animateWithDuration:0.15 animations:^{
        
        
        [self.unfoldImage setTransform:CGAffineTransformRotate(CGAffineTransformIdentity, rotation)];
    } completion:^(BOOL finished) {
        
        [[self getWindow] addSubview:self.coverView];
        [[self getWindow].layer addSublayer:self.shadowLayer];
        [[self getWindow] addSubview:self.table];
    }];
}


/**
 收起列表
 */
- (void)hide {
    
    [self.coverView removeFromSuperview];
    [self.shadowLayer removeFromSuperlayer];
    [self.table removeFromSuperview];
    [UIView animateWithDuration:0.3 animations:^{
        
        [self.unfoldImage setTransform:CGAffineTransformIdentity];
    }];
}

- (UIWindow *)getWindow {
    return [UIApplication sharedApplication].delegate.window;
}

#pragma mark - Set

- (void)setCornerRadius:(CGFloat)cornerRadius {
    _cornerRadius = cornerRadius;
    self.layer.cornerRadius = cornerRadius;
}
- (void)setSelectedIndex:(NSInteger)selectedIndex {
    _selectedIndex = selectedIndex;
    NSString *string;
    if (self.delegate && [self.delegate respondsToSelector:@selector(spinner:showItemStringAtIndex:)]) {
        string = [self.delegate spinner:self showItemStringAtIndex:selectedIndex];
        self.valueL.text = string;
    }
}

// 找寻上层滚动视图
- (UIScrollView *)findSuperScrollViewWithView:(UIView *)view {
    if ([view isEqual:[self getWindow]]) {  // 找到顶层，没有找到
        return nil;
    }
    else if ([view.superview isKindOfClass:[UIScrollView class]]) {
        
        if ([view.superview.superview isKindOfClass:[UITableView class]]) { // 如果是tableview，往上找
            UITableView *tableView = (UITableView *)view.superview.superview;
            
            UIScrollView *scrollView = [self findSuperScrollViewWithView:tableView];
            if (scrollView) {   // tableView往上找到，返回
                
                return scrollView;
            }
            else {  // tableview往上没有找到，返回tableview
                return tableView;
            }
            
        }
        else if ([view.superview isKindOfClass:[UITableView class]]) {
            UITableView *tableView = (UITableView *)view.superview;
            
            UIScrollView *scrollView = [self findSuperScrollViewWithView:tableView];
            if (scrollView) {   // tableView往上找到，返回
                
                return scrollView;
            }
            else {  // tableview往上没有找到，返回tableview
                return tableView;
            }
        }
        else {
            UIScrollView *scrollView = (UIScrollView *)view.superview;
            if (scrollView.contentSize.height > scrollView.frame.size.height) { // 如果是纵向滚动的tableView， 返回
                return scrollView;
            }
            else {
                return nil;
            }
        }
    }
    else { // 没有找到往上继续
        return [self findSuperScrollViewWithView:view.superview];
    }
}

- (void)setNoteImage:(NSString *)noteImage {
    _noteImage = noteImage;
    UIImage *image = [UIImage imageNamed:noteImage];
    if (image != nil) {
        self.unfoldImage.image = [UIImage imageNamed:noteImage];
    }
}

- (void)setBackColor:(UIColor *)backColor {
    _backColor = backColor;
    self.backgroundColor = backColor;
}


- (void)setLeftDefaultText:(NSString *)leftDefaultText {
    _leftDefaultText = leftDefaultText;
    self.leftL.text = leftDefaultText;
}

- (void)setValueDefaultText:(NSString *)valueDefaultText {
    _valueDefaultText = valueDefaultText;
    self.valueL.text  = valueDefaultText;
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.delegate && [self.delegate respondsToSelector:@selector(itemsCountOfSpinner:)]) {
        return [self.delegate itemsCountOfSpinner:self];
    }
    return 0;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    CGRect frame = cell.frame;
    frame.origin.x -= 15;
    cell.textLabel.frame = frame;
    NSString *str = @"代理方法未实现";
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(spinner:showItemStringAtIndex:)]) {
        str = [self.delegate spinner:self showItemStringAtIndex:indexPath.row ];
    }
    cell.textLabel.text = str;
    cell.textLabel.font = self.listItemFont;
    cell.textLabel.textColor = self.listItemTextColor;
    cell.backgroundColor = self.spinnerBackColor;
    
    return cell;
}
#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    self.selectedIndex = indexPath.row;
    if (self.delegate && [self.delegate respondsToSelector:@selector(spinner:didSelectedItemAtIndex:)]) {
        [self.delegate spinner:self didSelectedItemAtIndex:indexPath.row];
    }
    [self showTable];
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    cell.layer.anchorPoint = CGPointMake(0, 0.5);
    //为了防止cell视图移动，重新把cell放回原来的位置
    cell.layer.position = CGPointMake(0, cell.layer.position.y);
    [cell.layer setTransform:CATransform3DTranslate(CATransform3DIdentity, 0, -15, 0)];
    cell.alpha = 0.2;
    [UIView animateWithDuration:0.5 animations:^{
        cell.layer.transform = CATransform3DIdentity;
        cell.alpha = 1.0;
    }];
}

@end

