//
//  FBSRankCategoryCell.h
//  Forbes
//
//  Created by 周灿华 on 2019/9/24.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSCell.h"

@interface FBSRankCategoryItem : FBSItem
@property (nonatomic, copy) NSString *group_id;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, strong) NSArray *categories;
@property (nonatomic, strong) NSArray *categoryIds;
@property (nonatomic, strong) NSArray *data;
@property (nonatomic, assign) NSInteger selectedIndex; //当前选中索引
@property (nonatomic, assign) NSInteger section;
@end


@interface FBSRankCategoryCell : FBSCell

@end

