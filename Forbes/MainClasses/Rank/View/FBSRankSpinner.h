//
//  FBSRankSpinner.h
//  Forbes
//
//  Created by 周灿华 on 2019/9/26.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol FBSRankSpinnerDelegate;

@interface FBSRankSpinner : UIControl

@property (nonatomic, weak) NSObject<FBSRankSpinnerDelegate> *delegate;

/**
 左边默认文字
 */
@property (nonatomic, strong) NSString *leftDefaultText;

/**
 值默认文字
 */
@property (nonatomic, strong) NSString *valueDefaultText;

/**
 文本框背景颜色（默认clearColor）
 */
@property (nonatomic, strong) UIColor *backColor;

/**
 文本框选中后背景颜色(默认 rgb(220, 220, 220))
 */
@property (nonatomic, strong) UIColor *selectedBackColor;


/**
 列表项文字大小，默认14pt
 */
@property (nonatomic, strong) UIFont *listItemFont;


/**
 列表项高度,默认35
 */
@property (nonatomic, assign) CGFloat listItemHeight;


/**
 列表文字颜色，默认黑色
 */
@property (nonatomic, strong) UIColor *listItemTextColor;


/**
 弹出时最大显示行数，默认5
 */
@property (nonatomic, assign) NSUInteger maxShowLines;


/**
 弹出框的背景颜色，默认为whiteColor
 */
@property (nonatomic, strong) UIColor *spinnerBackColor;


/**
 按钮圆角，默认为3
 */
@property (nonatomic, assign) CGFloat cornerRadius;


/**
 展开收起提示用图标
 */
@property (nonatomic, copy) NSString *noteImage;

/**
 选中项的下标，-1时表示未选择
 */
@property (nonatomic, assign) NSInteger selectedIndex;

@end



@protocol FBSRankSpinnerDelegate

@optional

/**
 点击 spinner
 
 @param spinner 下拉视图
 */
- (void)didClickSpinner:(FBSRankSpinner *)spinner;

/**
 下拉菜单数据总数
 
 @param spinner 下拉视图
 @return 数量
 */
- (NSInteger)itemsCountOfSpinner:(FBSRankSpinner *)spinner;


/**
 某行展示字符
 
 @param spinner 下拉视图
 @param index 行数
 @return 展示字符
 */
- (NSString *)spinner:(FBSRankSpinner *)spinner showItemStringAtIndex:(NSInteger)index;


/**
 选中了某行数据（-1表示数据清空）
 
 @param spinner 下拉视图
 @param index 选中行
 */
- (void)spinner:(FBSRankSpinner *)spinner didSelectedItemAtIndex:(NSInteger)index;

@end

