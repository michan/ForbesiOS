//
//  FBSRankListCell.m
//  Forbes
//
//  Created by 周灿华 on 2019/9/24.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSRankListCell.h"

@implementation FBSRankListItem

@end



@interface FBSRankListCell ()
@property (nonatomic, strong) UILabel *titleL;

@end

@implementation FBSRankListCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self.contentView addSubview:self.titleL];
        
        [self.titleL mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(WScale(10));
            make.right.mas_equalTo(-WScale(10));
            make.centerY.mas_equalTo(0);
        }];
        
    }
    
    return self;
}


- (void)setItem:(FBSRankListItem *)item {
    if (![item isKindOfClass:[FBSRankListItem class]]) {
        return ;
    }
    
    [super setItem:item];
    self.titleL.text = item.title;
}


- (UILabel *)titleL {
    if (!_titleL) {
        _titleL = [[UILabel alloc] init];
        _titleL.font = kLabelFontSize14;
        _titleL.textColor = Hexcolor(0x333333);
        _titleL.text = @"";
    }
    return _titleL;
}

@end
