//
//  FBSRankMoreCell.h
//  Forbes
//
//  Created by 周灿华 on 2019/9/24.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSCell.h"

@interface FBSRankMoreItem : FBSItem
@property (nonatomic, copy) NSString *group_id;
@end


@interface FBSRankMoreCell : FBSCell

@end
