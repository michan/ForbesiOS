//
//  FBSRankCategoryCell.m
//  Forbes
//
//  Created by 周灿华 on 2019/9/24.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSRankCategoryCell.h"

@implementation FBSRankCategoryItem

- (instancetype)init {
    self = [super init];
    if (self) {
        _selectedIndex = NSNotFound;
    }
    return self;
}

@end


@interface FBSRankCategoryCell ()
{
    __weak FBSRankCategoryItem *_currentItem;
    UIButton *_currentBtn;
}
@property (nonatomic, strong) UIView *squareView;
@property (nonatomic, strong) UILabel *titleL;
@property (nonatomic, strong) UIView *grayView;
@property (nonatomic, strong) UIButton *firstBtn;
@property (nonatomic, strong) UIButton *secondBtn;
@property (nonatomic, strong) UIButton *thirdBtn;
@end

@implementation FBSRankCategoryCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self.contentView addSubview:self.squareView];
        [self.contentView addSubview:self.titleL];
        [self.contentView addSubview:self.grayView];
        [self.grayView addSubview:self.firstBtn];
        [self.grayView addSubview:self.secondBtn];
        [self.grayView addSubview:self.thirdBtn];
        
        
        [self.squareView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(WScale(24));
            make.left.mas_equalTo(WScale(10));
            make.height.width.mas_equalTo(WScale(20));
        }];

        
        [self.titleL mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(self.squareView);
            make.left.mas_equalTo(self.squareView.mas_right).offset(WScale(10));
        }];

        
        [self.grayView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.bottom.mas_equalTo(0);
            make.top.mas_equalTo(WScale(55));
        }];

        
        [self.firstBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.bottom.mas_equalTo(0);
            make.left.mas_equalTo(WScale(10));
        }];
        
        [self.secondBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.bottom.mas_equalTo(0);
            make.centerX.centerY.mas_equalTo(0);
        }];
        
        [self.thirdBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.bottom.mas_equalTo(0);
            make.right.mas_equalTo(-WScale(10));
        }];

        
    }
    
    return self;
}



- (void)setItem:(FBSRankCategoryItem *)item {
    if (![item isKindOfClass:[FBSRankCategoryItem class]]) {
        return ;
    }
    
    [super setItem:item];
    _currentItem = item;
    
    self.titleL.text = item.title;
    
    BOOL firstHidden = item.categories.count < 1;
    BOOL secondHidden = item.categories.count < 2;
    BOOL thirdHidden = item.categories.count < 3;
    
    self.firstBtn.hidden = firstHidden;
    self.secondBtn.hidden = secondHidden;
    self.thirdBtn.hidden = thirdHidden;
    
    if (!firstHidden) {
        [self.firstBtn setTitle:item.categories[0] forState:UIControlStateNormal];
    }
    
    if (!secondHidden) {
        [self.secondBtn setTitle:item.categories[1] forState:UIControlStateNormal];
    }
    
    if (!thirdHidden) {
        [self.thirdBtn setTitle:item.categories[2] forState:UIControlStateNormal];
    }
    
    [self changeSelectState:item.selectedIndex];
    
}


- (void)changeSelectState:(NSInteger)selectIndex {
    if (selectIndex == NSNotFound) {
        self.firstBtn.selected = NO;
        self.secondBtn.selected = NO;
        self.thirdBtn.selected = NO;
        _currentBtn = nil;
        return ;
    }
    if (selectIndex == 0) {
        self.firstBtn.selected = YES;
        self.secondBtn.selected = NO;
        self.thirdBtn.selected = NO;
        _currentBtn = self.firstBtn;
    }
    
    if (selectIndex == 1) {
        self.firstBtn.selected = NO;
        self.secondBtn.selected = YES;
        self.thirdBtn.selected = NO;
        _currentBtn = self.secondBtn;
    }
    
    if (selectIndex == 2) {
        self.firstBtn.selected = NO;
        self.secondBtn.selected = NO;
        self.thirdBtn.selected = YES;
        _currentBtn = self.thirdBtn;
    }
}


- (void)clickAction:(UIButton *)sender {
    if (_currentBtn != sender) {
        _currentBtn.selected = NO;
        sender.selected = YES;
        _currentBtn = sender;
        _currentItem.selectedIndex = sender.tag;
        
        if (self.delegate && [self.delegate respondsToSelector:@selector(fbsCell:didClickButtonAtIndex:)]) {
            [self.delegate fbsCell:self didClickButtonAtIndex:sender.tag];
        }
    }
}

#pragma mark - property

- (UIView *)squareView {
    if (!_squareView) {
        _squareView  = [[UIView alloc] init];
        _squareView.backgroundColor = MainThemeColor;
    }
    return _squareView;
}

- (UILabel *)titleL {
    if (!_titleL) {
        _titleL = [[UILabel alloc] init];
        _titleL.font = kLabelFontSize18;
        _titleL.textColor = RGBA(51,51,51,1);
        _titleL.text = @"";
    }
    return _titleL;
}


- (UIView *)grayView {
    if (!_grayView) {
        _grayView  = [[UIView alloc] init];
        _grayView.backgroundColor = Hexcolor(0xF6F7F9);
    }
    return _grayView;
}


- (UIButton *)firstBtn {
    if (!_firstBtn) {
        _firstBtn  = [UIButton buttonWithType:UIButtonTypeCustom];
        _firstBtn.tag = 0;
        _firstBtn.titleLabel.font = kLabelFontSize15;
        [_firstBtn setTitleColor:Hexcolor(0x333333) forState:UIControlStateNormal];
        [_firstBtn setTitleColor:MainThemeColor forState:UIControlStateSelected];
        [_firstBtn addTarget:self action:@selector(clickAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _firstBtn;
}


- (UIButton *)secondBtn {
    if (!_secondBtn) {
        _secondBtn  = [UIButton buttonWithType:UIButtonTypeCustom];
        _secondBtn.tag = 1;
        [_secondBtn setTitleColor:Hexcolor(0x333333) forState:UIControlStateNormal];
        _secondBtn.titleLabel.font = kLabelFontSize15;
        [_secondBtn setTitleColor:MainThemeColor forState:UIControlStateSelected];
        [_secondBtn addTarget:self action:@selector(clickAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _secondBtn;
}


- (UIButton *)thirdBtn {
    if (!_thirdBtn) {
        _thirdBtn  = [UIButton buttonWithType:UIButtonTypeCustom];
        _thirdBtn.tag = 2;
        [_thirdBtn setTitleColor:Hexcolor(0x333333) forState:UIControlStateNormal];
        _thirdBtn.titleLabel.font = kLabelFontSize15;
        [_thirdBtn setTitleColor:MainThemeColor forState:UIControlStateSelected];
        [_thirdBtn addTarget:self action:@selector(clickAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _thirdBtn;
}

@end
