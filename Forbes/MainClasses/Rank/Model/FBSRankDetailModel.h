//
//  FBSRankDetailModel.h
//  Forbes
//
//  Created by 周灿华 on 2019/9/26.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSBaseModel.h"

@class FBSRankDetailPager,FBSRankDetailOption,FBSRankDetailInfo;
@interface FBSRankDetailModel : FBSBaseModel
@property (nonatomic, copy) NSString *status;
@property (nonatomic, copy) NSString *ID;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *describe;
@property (nonatomic, strong) FBSRankDetailPager *pager;
@property (nonatomic, strong) NSArray<FBSRankDetailOption *> *options;
//内部需要手动解析
@property (nonatomic, strong) NSArray<NSArray<FBSRankDetailInfo *> *> *items;
@end


@interface FBSRankDetailPager : NSObject
@property (nonatomic, copy) NSString *limit;
@property (nonatomic, copy) NSString *total;
@property (nonatomic, copy) NSString *page;
@end



@interface FBSRankDetailOption : NSObject
@property (nonatomic, copy) NSString *ID;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *select_show;
@property (nonatomic, copy) NSString *sort;
@property (nonatomic, copy) NSString *type;
@end



@interface FBSRankDetailInfo : NSObject
@property (nonatomic, copy) NSString *list_options_id;
@property (nonatomic, copy) NSString *sort;
@property (nonatomic, copy) NSString *ID;
@property (nonatomic, copy) NSString *list_id;
@property (nonatomic, copy) NSString *created_at;
@property (nonatomic, copy) NSString *value;
@property (nonatomic, copy) NSString *field_id;
@property (nonatomic, copy) NSString *token;
@property (nonatomic, copy) NSString *updated_at;
@end
