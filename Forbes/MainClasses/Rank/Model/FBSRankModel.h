//
//  FBSRankModel.h
//  Forbes
//
//  Created by 周灿华 on 2019/9/24.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSBaseModel.h"

@class FBSRankCategory,FBSRankItemInfo;
@interface FBSRankModel : FBSBaseModel
@property (nonatomic, copy) NSString *limit;
@property (nonatomic, copy) NSString *page;
@property (nonatomic, copy) NSString *total;
@property (nonatomic, strong) NSArray<FBSRankCategory *> *categories;
@property (nonatomic, strong) NSArray<FBSRankItemInfo *> *items;
@end



@interface FBSRankCategory : NSObject
@property (nonatomic, copy) NSString *category_id;
@property (nonatomic, copy) NSString *category;
@end



@interface FBSRankItemInfo : NSObject
@property (nonatomic, copy) NSString *rankId;
@property (nonatomic, copy) NSString *category_id;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *describe;
@end

