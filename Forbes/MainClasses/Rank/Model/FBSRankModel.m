//
//  FBSRankModel.m
//  Forbes
//
//  Created by 周灿华 on 2019/9/24.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSRankModel.h"

@implementation FBSRankModel

+ (NSDictionary *)mj_objectClassInArray {
    return @{
             @"categories" : @"FBSRankCategory",
             @"items"      : @"FBSRankItemInfo"
             };
}


+ (NSDictionary *)mj_replacedKeyFromPropertyName {
    return @{
             @"limit" : @"data.limit",
             @"page" : @"data.page",
             @"total" : @"data.total",
             @"categories" : @"data.categories",
             @"items" : @"data.items",
             };
}

@end



@implementation FBSRankCategory


@end



@implementation FBSRankItemInfo

+ (NSDictionary *)mj_replacedKeyFromPropertyName {
    return @{
             @"rankId" : @"id",
             @"describe" : @"description"
             };
}

@end
