//
//  FBSRankDetailModel.m
//  Forbes
//
//  Created by 周灿华 on 2019/9/26.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSRankDetailModel.h"

@implementation FBSRankDetailModel

+ (NSDictionary *)mj_replacedKeyFromPropertyName {
    return @{
             @"status" : @"data.status",
             @"ID"    : @"data.id",
             @"title" : @"data.title",
             @"pager" : @"data.pager",
             @"describe" : @"data.description",
             @"options" : @"data.options",
             @"items" : @"data.items"
             };
}


+ (NSDictionary *)mj_objectClassInArray {
    return @{
             @"options" : @"FBSRankDetailOption"
             };
}

- (void)setItems:(NSArray *)items {
    if ([items isKindOfClass:[NSArray class]]) {
        NSMutableArray *outerArray = [NSMutableArray array];
        
        for (NSArray *subArray in items) {
            if ([subArray isKindOfClass:[NSArray class]]) {
                //模型解析
                NSMutableArray *innerArray = [FBSRankDetailInfo mj_objectArrayWithKeyValuesArray:subArray];
                [outerArray addObject:innerArray];
            }
        }
        
        _items = [outerArray copy];
    }
}

@end


@implementation FBSRankDetailPager

@end



@implementation FBSRankDetailOption

+ (NSDictionary *)mj_replacedKeyFromPropertyName {
    return @{
             @"ID" : @"id"
             };
}

@end



@implementation FBSRankDetailInfo

+ (NSDictionary *)mj_replacedKeyFromPropertyName {
    return @{
             @"ID" : @"id"
             };
}

@end

