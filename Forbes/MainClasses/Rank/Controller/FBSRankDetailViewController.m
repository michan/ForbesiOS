//
//  FBSRankDetailViewController.m
//  Forbes
//
//  Created by 周灿华 on 2019/9/24.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSRankDetailViewController.h"
#import "FBSRankDetailViewModel.h"
#import "FBSTableViewController+Refresh.h"
#import "YWExcelView.h"
#import "YWExcelViewMode.h"
#import "AdvertAopTableView.h"

@interface FBSRankDetailViewController ()<YWExcelViewDataSource,YWExcelViewDelegate>
@property (nonatomic, strong) FBSRankDetailViewModel *viewModel;
@property (nonatomic, strong) NSMutableArray *excelData;
@property (nonatomic, strong) YWExcelView *excelView;
@property (nonatomic, assign) NSInteger column;
@property (nonatomic, strong)AdvertAopTableView *aopDemo;
@property (nonatomic, strong) NSString *rankId;
@property (nonatomic, strong) NSMutableArray *softArray;



@end

@implementation FBSRankDetailViewController
@synthesize viewModel = _viewModel;

#pragma mark - life cycle

- (instancetype)initWithRankId:(NSString *)rankId {
    self = [super init];
    if (self) {
        self.viewModel = [[FBSRankDetailViewModel alloc] initWithRankId:rankId];
        self.rankId =rankId;
        self.column = 0;
        _softArray = [NSMutableArray array];
        [_softArray addObject:@"5"];
        [_softArray addObject:@"21"];
        [_softArray addObject:@"42"];
        [_softArray addObject:@"63"];
        [_softArray addObject:@"84"];
        [_softArray addObject:@"105"];

    }
    return self;
}
///广告初始化
- (void)adsInit {
    NSString *position_id = @"5";
    
    if (position_id) {
        self.aopDemo = [AdvertAopTableView new];
        self.aopDemo.position_id = position_id;
        self.aopDemo.vc = self;
        self.aopDemo.softArray = _softArray;

       //@[@"5",@"20",@"40",@"60",@"80",@"100"];
        self.aopDemo.aopUtils = self.excelView.tableView.aop_utils;
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    //加载第一页数据
    
    [self showHUD];
    
    [self loadData];
//    [self adsInit];
//    [self prepareUI];
}
-(void)prepareUI{
    [self showHUD];
    NSDictionary *parameters = @{
        @"limit":@"50",
        @"page":@"1"
    };
    WEAKSELF
    [MCNetworking ResponseNetworkGET_API:[NSString stringWithFormat:@"list/%@",_rankId?:@""] parameters:parameters  TheServer:0 cachePolicy:YES ISNeedLogin:NO RequestEnd:^(id  _Nonnull EndObject) {
        [self hideHUD];
        

    } MCHttpCacheData:^(id  _Nonnull CacheObject) {
        //缓存回调
        NSLog(@"类名===%@   方法名=====%@  缓存数据===%@",[self class],NSStringFromSelector(_cmd),CacheObject);




    } success:^(id  _Nonnull responseObject) {
        NSLog(@"类名===%@   方法名=====%@  成功数据===%@",[self class],NSStringFromSelector(_cmd),responseObject);

        FBSRankDetailModel*model = [FBSRankDetailModel mj_objectWithKeyValues:responseObject];
        
        
        
        [weakSelf showExcelData:model];
        


    } failure:^(NSURLSessionDataTask * _Nonnull operation, NSError * _Nonnull error, id  _Nonnull responseObject, NSString * _Nonnull description) {
        NSLog(@"类名===%@   方法名=====%@  请求失败数据===%@",[self class],NSStringFromSelector(_cmd),responseObject);

    }];
    

    
    
    
}

#pragma mark - private method

- (void)loadData {
    
//    NSDictionary *parameters = @{
//        @"limit":@"10",
//        @"page":@"1"
//    };
//    WEAKSELF
//    [MCNetworking ResponseNetworkPOST_API:[NSString stringWithFormat:@"list/%@",_rankId?:@""] parameters:parameters cachePolicy:YES ISNeedLogin:NO RequestEnd:^(id  _Nonnull EndObject) {
//
//
//
//    } MCHttpCacheData:^(id  _Nonnull CacheObject) {
//
//
//
//
//    } success:^(id  _Nonnull responseObject) {
//
//
//
//
//    } failure:^(NSURLSessionDataTask * _Nonnull operation, NSError * _Nonnull error, id  _Nonnull responseObject, NSString * _Nonnull description) {
//
//    }];
    
    
    
    
    WEAKSELF
    [self.viewModel rqRankDetail:^(BOOL success, FBSRankDetailModel *model) {
        if (success) {
            [weakSelf showExcelData:model];
        }
        [weakSelf hideHUD];
    }];
}


- (void)showExcelData:(FBSRankDetailModel *)model {
    if (self.viewModel.page == 1) { //第一页初始化
         self.title = [NSString stringWithFormat:@"%@%@",model.title,model.describe];
        self.excelData = [NSMutableArray array];
        
        //设置头部
        YWExcelViewMode *mode = [YWExcelViewMode new];
        mode.style = YWExcelViewStyleDefalut;
        mode.headTexts = [model.options valueForKey:@"title"];
        mode.headHeight = 40;
        mode.cellHeight = 55;
        self.column = model.options.count;
        
        //推荐使用这样初始化
        YWExcelView *exceView = [[YWExcelView alloc] initWithFrame:CGRectMake(0, kNavTopMargin, CGRectGetWidth(self.view.frame), kScreenHeight - kNavTopMargin) mode:mode];
        exceView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        exceView.dataSource = self;
        exceView.delegate = self;
        exceView.showBorder = NO;
        [self.view addSubview:exceView];
        self.excelView = exceView;
        
        [self adsInit];
        
        if (model.pager.total.integerValue > self.viewModel.limit) {
            WEAKSELF
            exceView.tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
                [weakSelf loadData];
            }];
        }
        
        if (self.column > 4) {
            [self makeToast:@"可左右滑动屏幕"];
        }
    }
    
    //区分头部
    //    [_list addObject:@{@"grade":@"年级",@"score":@[@"10",@"20",@"30",@"40",@"50",@"60",@"70"]}];
    
    for (NSArray *subarray in model.items) {
        NSMutableArray *values = [[subarray valueForKey:@"value"] mutableCopy];
        
        //判断是否与标题的个数一致
        if (values.count > self.column) {//删除多余的
            values = [[values subarrayWithRange:NSMakeRange(0, self.column)] mutableCopy];
        }
        
        if (values.count < self.column) { //补齐
            NSInteger delta = self.column - values.count;
            for (NSInteger i = 0; i < delta; i++) {
                [values addObject:@""];
            }
        }
        
        [self.excelData addObject:values];
//        if (values.count) {
//            NSString * indexc = values[0];
//            NSString * numStr =[NSString stringWithFormat:@"%ld",self.excelData.count];
//            if ([indexc isEqualToString:@"5"]) {
//                [_softArray replaceObjectAtIndex:0 withObject:numStr];
//            }
//            if ([indexc isEqualToString:@"20"]) {
//                [_softArray replaceObjectAtIndex:1 withObject:numStr];
//
//            }
//            if ([indexc isEqualToString:@"40"]) {
//                [_softArray replaceObjectAtIndex:2 withObject:numStr];
//
//            }
//
//            if ([indexc isEqualToString:@"60"]) {
//                [_softArray replaceObjectAtIndex:3 withObject:numStr];
//
//            }
//
//            if ([indexc isEqualToString:@"80"]) {
//                [_softArray replaceObjectAtIndex:4 withObject:numStr];
//
//            }
//            if ([indexc isEqualToString:@"100"]) {
//                [_softArray replaceObjectAtIndex:5 withObject:numStr];
//
//            }
//
//
//        }
        
    }
    
    
    if (model.pager.page.integerValue > 1) {
        if (model.items.count < model.pager.limit.integerValue) {
            [self.excelView.tableView.mj_footer endRefreshingWithNoMoreData];
        } else {
           [self.excelView.tableView.mj_footer endRefreshing];
        }
    }
    
    [self.excelView reloadData];
    
}

#pragma mark - test
//多少行
- (NSInteger)excelView:(YWExcelView *)excelView numberOfRowsInSection:(NSInteger)section{
    return self.excelData.count;
}
//多少列
- (NSInteger)itemOfRow:(YWExcelView *)excelView{
    return self.column;
}
- (void)excelView:(YWExcelView *)excelView label:(UILabel *)label textAtIndexPath:(YWIndexPath *)indexPath{
    if (indexPath.row < self.excelData.count) {
        NSArray *values = self.excelData[indexPath.row];
        label.text = values[indexPath.item];
    }
}

- (NSArray *)widthForItemOnExcelView:(YWExcelView *)excelView {
    NSMutableArray *widths = [NSMutableArray array];
    for (NSInteger i = 0; i < self.column; i++) {
        [widths addObject:@([self widthForItem])];
    }
    return widths;
}



- (CGFloat)widthForItem {
    CGFloat width = 0;
    if (self.column <= 0) {
        return kScreenWidth;
    }
    
    if (kScreenWidth < 375.0) { //6s以下的屏幕
        
        //最多显示3列
        if (self.column < 4) {
            width =  1.0 * kScreenWidth / self.column;
            return width;
        }
        
        return 1.0 * kScreenWidth / 3;
        
        
    } else {
        
        //最多显示4列
        if (self.column < 5) {
            width =  1.0 * kScreenWidth / self.column;
            return width;
        }
        
        return 1.0 * kScreenWidth / 4;
    }
   
}

@end
