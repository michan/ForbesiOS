
//
//  FBSRankViewController.m
//  Forbes
//
//  Created by 周灿华 on 2019/7/24.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSRankViewController.h"
#import "FBSRankViewModel.h"
#import "FBSTableViewController+Refresh.h"
#import "FBSNavView.h"
#import "FBSSearchViewController.h"
#import "FBSRankDetailViewController.h"
#import "FBSRankSearchViewController.h"
#import "AdvertAopTableView.h"

@interface FBSRankViewController ()
@property (nonatomic, strong) FBSRankViewModel *viewModel;
@property (nonatomic, strong)AdvertAopTableView *aopDemo;
@end

@implementation FBSRankViewController
@synthesize viewModel = _viewModel;

///广告初始化
- (void)adsInit {
    NSString *position_id = @"4";
    
    if (position_id) {
        self.aopDemo = [AdvertAopTableView new];
        self.aopDemo.position_id = position_id;
        self.aopDemo.vc = self;
        NSIndexPath *indexP1 = [NSIndexPath indexPathForRow:0 inSection:2];
        NSIndexPath *indexP2 = [NSIndexPath indexPathForRow:0 inSection:3];
        NSIndexPath *indexP3 = [NSIndexPath indexPathForRow:6 inSection:3];

        self.aopDemo.softArray = @[indexP1,indexP2,indexP3];
        self.aopDemo.aopUtils = self.tableView.aop_utils;
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initNavBar];
    
    self.registerDictionary = @{
                                [FBSBannerItem reuseIdentifier] : [FBSBannerCell class],
                                [FBSRankCategoryItem reuseIdentifier] : [FBSRankCategoryCell class],
                                [FBSRankListItem reuseIdentifier] : [FBSRankListCell class],
                                [FBSRankMoreItem reuseIdentifier] : [FBSRankMoreCell class]
                                };
    
    WEAKSELF
    [self normalHeaderRefreshingActionBlock:^(FooterConfigBlock  _Nonnull footerConfig) {
        
        [self.viewModel rqRankData:^(BOOL success) {
            if (success) {
                [weakSelf.tableView reloadData];
            }
            
            if(footerConfig){
                footerConfig(10);
            }

        }];
    }];
    [self adsInit];

}


- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}



- (void)initNavBar {
    self.navigationBackButtonHidden = YES;
    self.navigationBar.titleView.hidden = YES;
    self.navigationBar.bottomLine.hidden = YES;
    
    FBSNavView *navView = [[FBSNavView alloc] initWithFrame:CGRectZero];
    navView.placeholder = @"30 under 30";
    navView.liveAction = ^{
        FBSLog(@"点击了直播");
        [[FBSliveAuthorization sharedInstance] liveAuthorization:self];

    };
    
    WEAKSELF
    navView.searchAction = ^{
        FBSLog(@"点击了搜索");
        FBSSearchViewController *searchVC = [[FBSSearchViewController alloc] init];
        [weakSelf.navigationController pushViewController:searchVC animated:YES];
        
    };
    
    [self.navigationBar addSubview:navView];
    [navView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(0);
    }];
}


#pragma mark - FBSCellDelegate

- (void)fbsCell:(FBSCell *)cell didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if([cell isKindOfClass:[FBSRankListCell class]]) {
        FBSRankListItem *item = (FBSRankListItem *)cell.item;
        FBSRankDetailViewController *detailVC = [[FBSRankDetailViewController alloc] initWithRankId:item.rankId];
        [self.navigationController pushViewController:detailVC animated:YES];
    }

}


- (void)fbsCell:(FBSCell *)cell didClickButtonAtIndex:(NSInteger)index {
    if ([cell isKindOfClass:[FBSRankCategoryCell class]]) {
        WEAKSELF
        FBSRankCategoryItem *item = (FBSRankCategoryItem *)cell.item;
        
        [self.viewModel filterRankData:item.group_id categoryId:item.categoryIds[index] complete:^(BOOL success) {
            [weakSelf.tableView reloadSection:item.section withRowAnimation:UITableViewRowAnimationFade];
        }];
        
    } else if([cell isKindOfClass:[FBSRankMoreCell class]]) {
        FBSRankMoreItem *item = (FBSRankMoreItem *)cell.item;
        FBSRankSearchViewController *moreVC = [[FBSRankSearchViewController alloc] initWithGroupId:item.group_id];
        [self.navigationController pushViewController:moreVC animated:YES];
        
    } else if([cell isKindOfClass:[FBSBannerCell class]]){
        FBSBannerItem *item = (FBSBannerItem *)cell.item;
        if (index < item.datas.count) {
            FBSBannerInfo *bannerInfo = [item.datas objectAtIndex:index];
            FBSRankDetailViewController *detailVC = [[FBSRankDetailViewController alloc] initWithRankId:bannerInfo.ID];
            [self.navigationController pushViewController:detailVC animated:YES];        }
    }
}



#pragma mark - property

- (FBSRankViewModel *)viewModel {
    if (!_viewModel) {
        _viewModel  = [[FBSRankViewModel alloc] init];
    }
    return _viewModel;
}

@end
