//
//  FBSRankSearchViewController.h
//  Forbes
//
//  Created by 周灿华 on 2019/9/24.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSTableViewController.h"

@interface FBSRankSearchViewController : FBSTableViewController

- (instancetype)initWithGroupId:(NSString *)groupId;

@end

