//
//  FBSRankSearchViewController.m
//  Forbes
//
//  Created by 周灿华 on 2019/9/24.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSRankSearchViewController.h"
#import "FBSRankSearchViewModel.h"
#import "FBSRankSpinner.h"
#import "FBSRankDetailViewController.h"
#import "FBSTableViewController+Refresh.h"
#import "AdvertAopTableView.h"


@interface FBSRankSearchViewController ()<UITextFieldDelegate,FBSRankSpinnerDelegate>
@property (nonatomic, strong) FBSRankSearchViewModel *viewModel;
@property (nonatomic, strong) UIView *searchView;
@property (nonatomic, strong) UIImageView *searchIcon;
@property (nonatomic, strong) UITextField *searchTF;
@property (nonatomic, strong) FBSRankSpinner *spinner;

@property (nonatomic, strong) NSArray *cateforyIds;
@property (nonatomic, strong) NSArray *cateforyStrs;
@property (nonatomic, copy) NSString *currentCateforyId;
@property (nonatomic, copy) NSString *currentKeyWord;
@property (nonatomic, strong)AdvertAopTableView *aopDemo;

@end

@implementation FBSRankSearchViewController
@synthesize viewModel = _viewModel;

- (instancetype)initWithGroupId:(NSString *)groupId {
    self = [super init];
    if (self) {
        self.viewModel = [[FBSRankSearchViewModel alloc] initWithGroupId:groupId];
    }
    return self;
}
///广告初始化
- (void)adsInit {
    NSString *position_id = @"42";
    
    if (position_id) {
        self.aopDemo = [AdvertAopTableView new];
        self.aopDemo.position_id = position_id;
        self.aopDemo.vc = self;
        self.aopDemo.softArray = @[@"5",@"20",@"40",@"60",@"80",@"100"];
        self.aopDemo.aopUtils = self.tableView.aop_utils;
    }
}


- (void)viewDidLoad {
    [super viewDidLoad];
    [self UIInit];
    
    WEAKSELF
    [self showHUD];
    [self.viewModel rqCategoryData:^(BOOL success, NSArray * _Nonnull cateforyIds, NSArray * _Nonnull cateforyStrs) {
        
        [weakSelf hideHUD];
        
        if (success) {
            weakSelf.cateforyIds = cateforyIds;
            weakSelf.cateforyStrs = cateforyStrs;
            
            dispatch_async(dispatch_get_main_queue(), ^{
                if (cateforyStrs.count) {
                    self.spinner.valueDefaultText = cateforyStrs[0]?:@"";

                }

                [weakSelf createload];
            });
        }
    }];
    [self adsInit];

}




#pragma mark -  private method


- (void)createload {
    self.page = @(1);
    self.pageCount = @(20);
    WEAKSELF
    [self normalHeaderRefreshingActionBlock:^(FooterConfigBlock  _Nonnull footerConfig) {
        [self.viewModel searchRankData:self.currentCateforyId keyword:self.currentKeyWord complete:^(BOOL success, NSInteger count) {
            if (success) {
                [weakSelf.tableView reloadData];
                if(footerConfig){
                    footerConfig(count);
                }
            }
        }];
    }];
    
    [self backNormalFooterRefreshingActionBlock:^(FooterConfigBlock  _Nonnull footerConfig) {
        [self.viewModel searchRankData:self.currentCateforyId keyword:self.currentKeyWord complete:^(BOOL success, NSInteger count) {
            if (success) {
                [weakSelf.tableView reloadData];
                if(footerConfig){
                    footerConfig(count);
                }
            }
        }];
    }];
}

- (void)UIInit {
    self.view.backgroundColor = [UIColor whiteColor];
    if ([self.viewModel.groupId isEqualToString:PeopleGroupId]) {
        self.title = @"人";
    } else if ([self.viewModel.groupId isEqualToString:ZoneGroupId]) {
        self.title = @"地方";
    } else if ([self.viewModel.groupId isEqualToString:CompanyGroupId]) {
        self.title = @"公司";
    }
    
    [self.view addSubview:self.searchView];
    [self.searchView addSubview:self.searchIcon];
    [self.searchView addSubview:self.searchTF];
    
    [self.searchView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.navigationBar.mas_bottom).offset(5);
        make.left.mas_equalTo(12);
        make.right.mas_equalTo(-12);
        make.height.mas_equalTo(30);
    }];
    
    
    [self.searchIcon mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(0);
        make.left.mas_equalTo(7);
        make.width.height.mas_equalTo(16);
    }];

    
    [self.searchTF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(0);
        make.left.mas_equalTo(self.searchIcon.mas_right).offset(10);
        make.right.mas_equalTo(-10);
        make.height.mas_equalTo(30);
    }];
    
    self.spinner = [[FBSRankSpinner alloc] init];
    self.spinner.leftDefaultText = @"筛选";
    self.spinner.valueDefaultText = @"所有";
    self.spinner.delegate = self;
    self.spinner.backColor = [UIColor whiteColor];
    self.spinner.selectedBackColor = [UIColor whiteColor];

    [self.view addSubview:self.spinner];
    [self.spinner mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(10);
        make.right.mas_equalTo(-10);
        make.top.mas_equalTo(self.searchView.mas_bottom).offset(10);
        make.height.mas_equalTo(50);
    }];
    
    //添加分割线
    UIView *bottomLine = [[UIView alloc] init];
    [self.view addSubview:bottomLine];
    
    bottomLine.backgroundColor = Hexcolor(0xdddddd);
    [bottomLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0);
        make.top.mas_equalTo(self.spinner.mas_bottom);
        make.height.equalTo(@(OnePixel));
    }];

    self.tableView.contentInset = UIEdgeInsetsZero;
    [self.tableView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(bottomLine.mas_bottom);
        make.left.bottom.right.mas_equalTo(0);
    }];

    self.registerDictionary = @{
                                [FBSRankListItem reuseIdentifier] : [FBSRankListCell class],
                                };
    
    //监听输入栏文字变化
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textChanged:) name:UITextFieldTextDidChangeNotification object:self.searchTF];
}


- (void)searchAction:(NSString *)keyword immediately:(BOOL)immediately {
    FBSLog(@"搜索的关键字是 : %@", self.searchTF.text);
    
    if (immediately) { //立即搜索
        WEAKSELF
        self.page = @(1);
        
        [self showHUD];
        [self.viewModel searchRankData:self.currentCateforyId keyword:self.currentKeyWord complete:^(BOOL success, NSInteger count) {
            [weakSelf.tableView reloadData];
            [weakSelf hideHUD];
            
            if (count < weakSelf.pageCount.integerValue) {
                [weakSelf.tableView.mj_footer endRefreshingWithNoMoreData];
            } else {
                [weakSelf.tableView.mj_footer resetNoMoreData];
            }
        }];
        
    }
}



- (NSString *)spinner:(FBSRankSpinner *)spinner showItemStringAtIndex:(NSInteger)index {
    return self.cateforyStrs[index];
}


- (NSInteger)itemsCountOfSpinner:(FBSRankSpinner *)spinner {
    return self.cateforyStrs.count;
}


- (void)didClickSpinner:(FBSRankSpinner *)spinner {
    [self.searchTF resignFirstResponder];
}

- (void)spinner:(FBSRankSpinner *)spinner didSelectedItemAtIndex:(NSInteger)index {
    FBSLog(@"didSelectedItemAtIndex : %ld",index);
    NSString *category = [self.cateforyIds objectAtIndex:index];
    self.currentCateforyId = category;
    
    WEAKSELF
    self.page = @(1);
    
    [self showHUD];
    [self.viewModel searchRankData:category keyword:self.currentKeyWord complete:^(BOOL success, NSInteger count) {
        [weakSelf.tableView reloadData];
        [weakSelf hideHUD];
        
        if (count < weakSelf.pageCount.integerValue) {
            [weakSelf.tableView.mj_footer endRefreshingWithNoMoreData];
        } else {
           [weakSelf.tableView.mj_footer resetNoMoreData];
        }
    }];
}

#pragma mark - FBSCellDelegate

- (void)fbsCell:(FBSCell *)cell didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.searchTF.isFirstResponder) {
        [self.searchTF resignFirstResponder];
    }
    
    if([cell isKindOfClass:[FBSRankListCell class]]) {
        FBSRankListItem *item = (FBSRankListItem *)cell.item;
        FBSRankDetailViewController *detailVC = [[FBSRankDetailViewController alloc] initWithRankId:item.rankId];
        [self.navigationController pushViewController:detailVC animated:YES];
    }
    
}

#pragma mark - 监听输入框文字变化

- (void)textChanged:(NSNotification *)notification {
    
    if (notification.object != self.searchTF) {
        return ;
    }
    self.currentKeyWord = self.searchTF.text;
}


#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    return YES;
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    return YES;
}


- (void)textFieldDidEndEditing:(UITextField *)textField {
//    [self searchAction:textField.text immediately:YES];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    //开始搜索
    [self searchAction:textField.text immediately:YES];
    return YES;
}


#pragma mark - property

- (UIView *)searchView {
    if (!_searchView) {
        _searchView = [[UIView alloc] init];
        _searchView.backgroundColor = Hexcolor(0xF6F7F9);
        _searchView.layer.cornerRadius = 5;
        _searchView.layer.masksToBounds = YES;
    }
    return _searchView;
}

- (UIImageView *)searchIcon {
    if (!_searchIcon) {
        _searchIcon = [[UIImageView alloc] init];
        _searchIcon.image = kImageWithName(@"Fill19");
    }
    return _searchIcon;
}


- (UITextField *)searchTF {
    if (!_searchTF) {
        _searchTF = [[UITextField alloc] init];
        _searchTF.textColor = Hexcolor(0x333333);
        _searchTF.font = kLabelFontSize15;
        _searchTF.placeholder = @"请输入关键字查询";
        _searchTF.delegate = self;
        _searchTF.clearButtonMode = UITextFieldViewModeWhileEditing;
        _searchTF.returnKeyType = UIReturnKeySearch;
    }
    return _searchTF;
}


@end
