//
//  FBSRankDetailViewModel.h
//  Forbes
//
//  Created by 周灿华 on 2019/9/24.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSViewModel.h"
#import "FBSRankDetailModel.h"

@interface FBSRankDetailViewModel : FBSViewModel
@property (nonatomic, assign) NSInteger page;
@property (nonatomic, assign) NSInteger limit;

- (instancetype)initWithRankId:(NSString *)rankId;

- (void)rqRankDetail:(void(^)(BOOL success,FBSRankDetailModel *model))complete;


@end

