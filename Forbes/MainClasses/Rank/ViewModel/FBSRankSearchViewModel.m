//
//  FBSRankSearchViewModel.m
//  Forbes
//
//  Created by 周灿华 on 2019/9/24.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSRankSearchViewModel.h"
#import "FBSRankAPI.h"
#import "FBSRankModel.h"

@interface FBSRankSearchViewModel ()
@property (nonatomic, strong) FBSRankAPI *searchAPI;
@property (nonatomic, strong) FBSRankModel *searchModel;

@end

@implementation FBSRankSearchViewModel

- (instancetype)initWithGroupId:(NSString *)groupId {
    self = [super init];
    if (self) {
        self.groupId = groupId;
    }
    return self;
}



- (void)rqCategoryData:(void(^)(BOOL success,NSArray *cateforyIds, NSArray *cateforyStrs))complete {
    FBSRankAPI *companyAPI = [[FBSRankAPI alloc] init];
    companyAPI.requestURI = [NSString stringWithFormat:@"list?group_id=%@&limit=10&page=1",self.groupId];
    [companyAPI startWithSuccess:^(YBNetworkResponse * _Nonnull response) {
        FBSRankModel *rankModel = [FBSRankModel mj_objectWithKeyValues:response.responseObject];
        
        NSMutableArray *cateforyIds = [[rankModel.categories valueForKey:@"category_id"] mutableCopy];;
        NSMutableArray *cateforyStrs = [[rankModel.categories valueForKey:@"category"] mutableCopy];

//        //插入所有选项
//        [cateforyIds insertObject:@"" atIndex:0];
//        [cateforyStrs insertObject:@"所有" atIndex:0];
        
        if (complete) {
            complete(YES,cateforyIds,cateforyStrs);
        }
    } failure:^(YBNetworkResponse * _Nonnull response) {
        complete(NO,nil,nil);
    }];
}

- (void)searchRankData:(NSString *)categoryId
               keyword:(NSString *)keyword
              complete:(void(^)(BOOL success, NSInteger count))complete {
    
    NSMutableString *requestURI = [[NSMutableString alloc] init];
    [requestURI appendFormat:@"list?group_id=%@",self.groupId];
    if (categoryId.length) {
        [requestURI appendFormat:@"&category_id=%@",categoryId];
    }
    
    if (keyword.length) {
        //关键字中文编码
        keyword = [keyword stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLPathAllowedCharacterSet]];
        
        [requestURI appendFormat:@"&keyword=%@",keyword];
    }
    
    [requestURI appendFormat:@"&limit=%ld&page=%ld",self.limit,self.page];
    self.searchAPI.requestURI = [requestURI copy];
    
    WEAKSELF
    [self.searchAPI startWithSuccess:^(YBNetworkResponse * _Nonnull response) {
        FBSLog(@"搜索榜单成功 : %@",response.responseObject);
        weakSelf.searchModel = [FBSRankModel mj_objectWithKeyValues:response.responseObject];
        
        [weakSelf createItems];
        
        if (complete) {
            complete(YES,weakSelf.searchModel.items.count);
        }
        
    } failure:^(YBNetworkResponse * _Nonnull response) {
        FBSLog(@"搜索榜单失败 : %@",response.error.localizedDescription);
        if (complete) {
            complete(NO,0);
        }
        
    }];
    
}


- (void)createItems {
    if (self.page == 1) {
        [self.items removeAllObjects];
    }
    
    for (FBSRankItemInfo *info in self.searchModel.items) {
        FBSRankListItem *newItem = [FBSRankListItem item];
        newItem.rankId = info.rankId;
        newItem.title = [NSString stringWithFormat:@"%@%@",info.title,info.describe];
        newItem.cellHeight = WScale(48);
        [self.items addObject:newItem];
    }

}


#pragma mark - propety

- (FBSRankAPI *)searchAPI {
    if (!_searchAPI) {
        _searchAPI  = [[FBSRankAPI alloc] init];
    }
    return _searchAPI;
}



@end
