//
//  FBSRankSearchViewModel.h
//  Forbes
//
//  Created by 周灿华 on 2019/9/24.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSTableViewModel.h"
#import "FBSRankListCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface FBSRankSearchViewModel : FBSTableViewModel

@property (nonatomic, copy) NSString *groupId;

- (instancetype)initWithGroupId:(NSString *)groupId;

//获取分类数据
- (void)rqCategoryData:(void(^)(BOOL success,NSArray *cateforyIds, NSArray *cateforyStrs))complete;


//搜索榜单数据
- (void)searchRankData:(NSString *)categoryId
               keyword:(NSString *)keyword
              complete:(void(^)(BOOL success, NSInteger count))complete;

@end

NS_ASSUME_NONNULL_END
