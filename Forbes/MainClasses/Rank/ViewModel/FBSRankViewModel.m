
//
//  FBSRankViewModel.m
//  Forbes
//
//  Created by 周灿华 on 2019/7/28.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSRankViewModel.h"
#import "FBSRankAPI.h"
#import "FBSRecommendModel.h"
#import "FBSRankModel.h"

@interface FBSRankViewModel ()
@property (nonatomic, strong) FBSRankAPI *filterAPI;
@property (nonatomic, strong) FBSRankModel *filterModel;

@property (nonatomic, strong) FBSRankRecommendAPI *recommendAPI;
@property (nonatomic, strong) FBSRecommendModel *recommendModel;
@property (nonatomic, strong) FBSRankModel *peopleRankModel;
@property (nonatomic, strong) FBSRankModel *zoneRankModel;
@property (nonatomic, strong) FBSRankModel *companyRankModel;

@property (nonatomic, strong) FBSRankCategoryItem *peopleCategoryItem;
@property (nonatomic, strong) FBSRankCategoryItem *companyCategoryItem;
@property (nonatomic, strong) FBSRankCategoryItem *zoneCategoryItem;

@property (nonatomic, strong) FBSRankMoreItem *peopleMoreItem;
@property (nonatomic, strong) FBSRankMoreItem *companyMoreItem;
@property (nonatomic, strong) FBSRankMoreItem *zoneMoreItem;

@property (nonatomic, strong) void(^complete)(BOOL);
@end

@implementation FBSRankViewModel

- (void)rqRankData:(void(^)(BOOL success))complete {
    WEAKSELF
    self.complete = complete;
    
    dispatch_group_t group = dispatch_group_create();
    //获取banner
    dispatch_group_enter(group);
    [self.recommendAPI startWithSuccess:^(YBNetworkResponse * _Nonnull response) {
        FBSLog(@"获取榜单推荐成功 : %@",response.responseObject);
        weakSelf.recommendModel = [FBSRecommendModel mj_objectWithKeyValues:response.responseObject];
        dispatch_group_leave(group);
        
    } failure:^(YBNetworkResponse * _Nonnull response) {
        FBSLog(@"获取榜单推荐失败 : %@",response.error.localizedDescription);
        dispatch_group_leave(group);
    }];
    
    
    //获取人榜单
    dispatch_group_enter(group);
    FBSRankAPI *peopleAPI = [[FBSRankAPI alloc] init];
    peopleAPI.requestURI = @"list?group_id=2&limit=3&page=1";
    [peopleAPI startWithSuccess:^(YBNetworkResponse * _Nonnull response) {
        FBSLog(@"获取人榜单成功 : %@",response.responseObject);
        weakSelf.peopleRankModel = [FBSRankModel mj_objectWithKeyValues:response.responseObject];
        NSInteger filterCount = weakSelf.peopleRankModel.items.count;
        if (filterCount > 3) {
            filterCount = 3;
            NSArray *filterArr = [weakSelf.peopleRankModel.items subarrayWithRange:NSMakeRange(0, filterCount)];
            weakSelf.peopleRankModel.items = [filterArr copy];
        }

        
        dispatch_group_leave(group);

    } failure:^(YBNetworkResponse * _Nonnull response) {
        FBSLog(@"获取人榜单失败 : %@",response.error.localizedDescription);
        dispatch_group_leave(group);

    }];
    
    
    //获取地方榜单
    dispatch_group_enter(group);
    FBSRankAPI *zoneAPI = [[FBSRankAPI alloc] init];
    zoneAPI.requestURI = @"list?group_id=3&limit=3&page=1";
    [zoneAPI startWithSuccess:^(YBNetworkResponse * _Nonnull response) {
        FBSLog(@"获取地方榜单成功 : %@",response.responseObject);
        weakSelf.zoneRankModel = [FBSRankModel mj_objectWithKeyValues:response.responseObject];
        NSInteger filterCount = weakSelf.zoneRankModel.items.count;
        if (filterCount > 3) {
            filterCount = 3;
            NSArray *filterArr = [weakSelf.zoneRankModel.items subarrayWithRange:NSMakeRange(0, filterCount)];
            weakSelf.zoneRankModel.items = [filterArr copy];
        }
        
        dispatch_group_leave(group);
        
    } failure:^(YBNetworkResponse * _Nonnull response) {
        FBSLog(@"获取地方榜单失败 : %@",response.error.localizedDescription);
        dispatch_group_leave(group);
        
    }];
    
    
    //获取公司榜单
    dispatch_group_enter(group);
    FBSRankAPI *companyAPI = [[FBSRankAPI alloc] init];
    companyAPI.requestURI = @"list?group_id=4&limit=3&page=1";
    [companyAPI startWithSuccess:^(YBNetworkResponse * _Nonnull response) {
        FBSLog(@"获取公司榜单成功 : %@",response.responseObject);
        weakSelf.companyRankModel = [FBSRankModel mj_objectWithKeyValues:response.responseObject];
        
        NSInteger filterCount = weakSelf.companyRankModel.items.count;
        if (filterCount > 3) {
            filterCount = 3;
            NSArray *filterArr = [weakSelf.companyRankModel.items subarrayWithRange:NSMakeRange(0, filterCount)];
            weakSelf.companyRankModel.items = [filterArr copy];
        }

        dispatch_group_leave(group);
        
    } failure:^(YBNetworkResponse * _Nonnull response) {
        FBSLog(@"获取公司榜单失败 : %@",response.error.localizedDescription);
        dispatch_group_leave(group);
        
    }];


    dispatch_group_notify(group, dispatch_get_main_queue(), ^{
        [weakSelf createItems];
        weakSelf.complete(YES);
    });
}

- (void)filterRankData:(NSString *)group_id
            categoryId:(NSString *)category_id
              complete:(void(^)(BOOL success))complete {
    if (!group_id.length) {
        group_id  = @"";
    }
    
    if (!category_id.length) {
        category_id  = @"";
    }

    self.filterAPI.group_id = group_id;
    self.filterAPI.category_id = category_id;
    self.filterAPI.requestURI = [NSString stringWithFormat:@"list?group_id=%@&category_id=%@&limit=3&page=1",group_id,category_id];
    
    WEAKSELF
    [self.filterAPI startWithSuccess:^(YBNetworkResponse * _Nonnull response) {
        FBSLog(@"获取过滤榜单成功 : %@",response.responseObject);
        weakSelf.filterModel = [FBSRankModel mj_objectWithKeyValues:response.responseObject];
        
        NSInteger filterCount = weakSelf.filterModel.items.count;
        if (filterCount > 3) {
            filterCount = 3;
            NSArray *filterArr = [weakSelf.filterModel.items subarrayWithRange:NSMakeRange(0, filterCount)];
            weakSelf.filterModel.items = [filterArr copy];
        }
        
        
        [weakSelf filterItems];
        
        if (complete) {
            complete(YES);
        }
        
    } failure:^(YBNetworkResponse * _Nonnull response) {
        FBSLog(@"获取过滤榜单失败 : %@",response.error.localizedDescription);
        if (complete) {
            complete(NO);
        }

    }];

}


#pragma mark - 生成item


- (void)filterItems {
    
    NSMutableArray *filterItems = [NSMutableArray array];
    NSInteger count = self.filterModel.items.count;
    for (NSInteger i = 0; i < count; i++) {
        FBSRankItemInfo *info = [self.filterModel.items objectAtIndex:i];
        FBSRankListItem *newItem = [FBSRankListItem item];
        newItem.rankId = info.rankId;
        newItem.title = [NSString stringWithFormat:@"%@%@",info.title,info.describe];
        newItem.bottomLineHidden = (i == count - 1);
        newItem.cellHeight = WScale(48);
        [filterItems addObject:newItem];
    }
    
    if ([self.filterAPI.group_id isEqualToString:PeopleGroupId]) {
        [self.items removeObjectAtIndex:1];
        [filterItems insertObject:self.peopleCategoryItem atIndex:0];
        [filterItems addObject:self.peopleMoreItem];
        [self.items insertObject:filterItems atIndex:1];
        
    } else if ([self.filterAPI.group_id isEqualToString:CompanyGroupId]) {
        [self.items removeObjectAtIndex:2];
        [filterItems insertObject:self.companyCategoryItem atIndex:0];
        [filterItems addObject:self.companyMoreItem];
        [self.items insertObject:filterItems atIndex:2];
        
    } else if ([self.filterAPI.group_id isEqualToString:ZoneGroupId]) {
        [self.items removeObjectAtIndex:3];
        [filterItems insertObject:self.zoneCategoryItem atIndex:0];
        [filterItems addObject:self.zoneMoreItem];
        [self.items insertObject:filterItems atIndex:3];
    }
    
}

- (void)createItems {
    [self.items removeAllObjects];
    //创建banner
    NSMutableArray *infos  = [NSMutableArray array];
    for (FBSRecommendInfo *recInfo in self.recommendModel.data) {
        FBSBannerInfo *bannerInfo = [FBSBannerInfo new];
        bannerInfo.ID = recInfo.relation_id;
        bannerInfo.title = recInfo.title;
        bannerInfo.author = recInfo.nickname;
        bannerInfo.time = [NSString publishedTimeFormatWithTimeStamp:recInfo.created_at];;
        bannerInfo.imgUrl = recInfo.file_url;
        [infos addObject:bannerInfo];
    }
    
    FBSBannerItem *bannerItem = [FBSBannerItem item];
    bannerItem.datas = [infos copy];
    [self.items addObject:@[bannerItem]];  //section 0
    

    NSMutableArray *section1 = [NSMutableArray array];
    if (self.peopleRankModel.categories.count) {
        self.peopleCategoryItem.selectedIndex = NSNotFound;
        self.peopleCategoryItem.categories = [self.peopleRankModel.categories valueForKey:@"category"];
        self.peopleCategoryItem.categoryIds = [self.peopleRankModel.categories valueForKey:@"category_id"];
        [section1 addObject:self.peopleCategoryItem];
        
    
        NSInteger count = self.peopleRankModel.items.count;
        for (NSInteger i = 0; i < count; i++) {
            FBSRankItemInfo *info = [self.peopleRankModel.items objectAtIndex:i];
            FBSRankListItem *listItem = [FBSRankListItem item];
            listItem.rankId = info.rankId;
            listItem.title = [NSString stringWithFormat:@"%@%@",info.title,info.describe];
            listItem.bottomLineHidden = (i == count - 1);
            listItem.cellHeight = WScale(48);
            [section1 addObject:listItem];
        }
        [section1 addObject:self.peopleMoreItem];
    }
    [self.items addObject:section1];  //section1
    
    
    NSMutableArray *section2 = [NSMutableArray array];
    if (self.companyRankModel.categories.count) {
        self.companyCategoryItem.selectedIndex = NSNotFound;
        self.companyCategoryItem.categories = [self.companyRankModel.categories valueForKey:@"category"];
        self.companyCategoryItem.categoryIds = [self.companyRankModel.categories valueForKey:@"category_id"];
        [section2 addObject:self.companyCategoryItem];
        
        NSInteger count = self.companyRankModel.items.count;
        for (NSInteger i = 0; i < count; i++) {
            FBSRankItemInfo *info = [self.companyRankModel.items objectAtIndex:i];
            FBSRankListItem *listItem = [FBSRankListItem item];
            listItem.rankId = info.rankId;
            listItem.title = [NSString stringWithFormat:@"%@%@",info.title,info.describe];
            listItem.bottomLineHidden = (i == count - 1);
            listItem.cellHeight = WScale(48);
            [section2 addObject:listItem];
        }
        
        [section2 addObject:self.companyMoreItem];
    }
    [self.items addObject:section2];  //section2

    
    NSMutableArray *section3 = [NSMutableArray array];
    if (self.zoneRankModel.categories.count) {
        self.zoneCategoryItem.selectedIndex = NSNotFound;
        self.zoneCategoryItem.categories = [self.zoneRankModel.categories valueForKey:@"category"];
        self.zoneCategoryItem.categoryIds = [self.zoneRankModel.categories valueForKey:@"category_id"];
        [section3 addObject:self.zoneCategoryItem];
        
        
        NSInteger count = self.zoneRankModel.items.count;
        for (NSInteger i = 0; i < count; i++) {
            FBSRankItemInfo *info = [self.zoneRankModel.items objectAtIndex:i];
            FBSRankListItem *listItem = [FBSRankListItem item];
            listItem.rankId = info.rankId;
            listItem.title = [NSString stringWithFormat:@"%@%@",info.title,info.describe];
            listItem.bottomLineHidden = (i == count - 1);
            listItem.cellHeight = WScale(48);
            [section3 addObject:listItem];
        }
        [section3 addObject:self.zoneMoreItem];
    }
    [self.items addObject:section3];  //section3

}


#pragma mark - property

- (FBSRankRecommendAPI *)recommendAPI {
    if (!_recommendAPI) {
        _recommendAPI  = [[FBSRankRecommendAPI alloc] init];
    }
    return _recommendAPI;
}

- (FBSRankAPI *)filterAPI {
    if (!_filterAPI) {
        _filterAPI  = [[FBSRankAPI alloc] init];
    }
    return _filterAPI;
}

- (FBSRankCategoryItem *)peopleCategoryItem {
    if (!_peopleCategoryItem) {
        _peopleCategoryItem = [FBSRankCategoryItem item];
        _peopleCategoryItem.section = 1;
        _peopleCategoryItem.group_id = PeopleGroupId;
        _peopleCategoryItem.title = @"人";
        _peopleCategoryItem.bottomLineHidden = YES;
        _peopleCategoryItem.cellHeight = WScale(95);
    }
    return _peopleCategoryItem;
}


- (FBSRankCategoryItem *)companyCategoryItem {
    if (!_companyCategoryItem) {
        _companyCategoryItem = [FBSRankCategoryItem item];
        _companyCategoryItem.section = 2;
        _companyCategoryItem.group_id = CompanyGroupId;
        _companyCategoryItem.title = @"公司";
        _companyCategoryItem.bottomLineHidden = YES;
        _companyCategoryItem.cellHeight = WScale(95);
    }
    return _companyCategoryItem;
}


- (FBSRankCategoryItem *)zoneCategoryItem {
    if (!_zoneCategoryItem) {
        _zoneCategoryItem = [FBSRankCategoryItem item];
        _zoneCategoryItem.section = 3;
        _zoneCategoryItem.group_id = ZoneGroupId;
        _zoneCategoryItem.title = @"地方";
        _zoneCategoryItem.bottomLineHidden = YES;
        _zoneCategoryItem.cellHeight = WScale(95);
    }
    return _zoneCategoryItem;
}


- (FBSRankMoreItem *)peopleMoreItem {
    if (!_peopleMoreItem) {
        _peopleMoreItem  = [[FBSRankMoreItem alloc] init];
        _peopleMoreItem.group_id = PeopleGroupId;
        _peopleMoreItem.cellHeight = WScale(60);
    }
    return _peopleMoreItem;
}


- (FBSRankMoreItem *)companyMoreItem {
    if (!_companyMoreItem) {
        _companyMoreItem  = [[FBSRankMoreItem alloc] init];
        _companyMoreItem.group_id = CompanyGroupId;
        _companyMoreItem.cellHeight = WScale(60);
    }
    return _companyMoreItem;
}


- (FBSRankMoreItem *)zoneMoreItem {
    if (!_zoneMoreItem) {
        _zoneMoreItem  = [[FBSRankMoreItem alloc] init];
        _zoneMoreItem.group_id = ZoneGroupId;
        _zoneMoreItem.cellHeight = WScale(60);
    }
    return _zoneMoreItem;
}

@end
