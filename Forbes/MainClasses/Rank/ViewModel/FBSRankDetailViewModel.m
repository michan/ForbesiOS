//
//  FBSRankDetailViewModel.m
//  Forbes
//
//  Created by 周灿华 on 2019/9/24.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSRankDetailViewModel.h"
#import "FBSRankAPI.h"

@interface FBSRankDetailViewModel ()
@property (nonatomic, strong) FBSRankDetailAPI *detailAPI;
@property (nonatomic, strong) FBSRankDetailModel *detailModel;
@property (nonatomic, copy) NSString *rankId;
@end

@implementation FBSRankDetailViewModel


- (instancetype)initWithRankId:(NSString *)rankId {
    self = [super init];
    if (self) {
        self.rankId = rankId;
        self.page = 1;
        self.limit = 50;
    }
    return self;
}

- (void)rqRankDetail:(void(^)(BOOL success,FBSRankDetailModel *model))complete  {
    WEAKSELF
    self.detailAPI.requestURI = [NSString stringWithFormat:@"list/%@?limit=%ld&page=%ld",self.rankId,self.limit,self.page];
    [self.detailAPI startWithSuccess:^(YBNetworkResponse * _Nonnull response) {
        FBSLog(@"response.responseObject : %@",response.responseObject);
        weakSelf.detailModel = [FBSRankDetailModel mj_objectWithKeyValues:response.responseObject];
        if (complete) {
            complete(YES, weakSelf.detailModel);
        }
        weakSelf.page++;
        
    } failure:^(YBNetworkResponse * _Nonnull response) {
        if (complete) {
            complete(NO, nil);
        }

    }];
}

#pragma mark - property

- (FBSRankDetailAPI *)detailAPI {
    if (!_detailAPI) {
        _detailAPI  = [[FBSRankDetailAPI alloc] init];
    }
    return _detailAPI;
}

@end
