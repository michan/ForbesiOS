//
//  FBSRankViewModel.h
//  Forbes
//
//  Created by 周灿华 on 2019/7/28.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSTableViewModel.h"
#import "FBSBannerCell.h"
#import "FBSRankCategoryCell.h"
#import "FBSRankListCell.h"
#import "FBSRankMoreCell.h"

@interface FBSRankViewModel : FBSTableViewModel

- (void)rqRankData:(void(^)(BOOL success))complete;


///过滤榜单数据
- (void)filterRankData:(NSString *)group_id
            categoryId:(NSString *)categoryId
              complete:(void(^)(BOOL success))complete;

@end

