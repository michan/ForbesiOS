//
//  FBSPinglunDetialViewModel.h
//  Forbes
//
//  Created by 赵志辉 on 2019/11/17.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import "FBSTableViewModel.h"
#import "FBSPinglunItem.h"


@interface FBSPinglunDetialViewModel : FBSTableViewModel

- (void)rqContent:(void(^)(BOOL success,NSInteger count))complete;
@property(nonatomic, strong) FBSPinglunItem *pinglunItem;
- (void)rqSendIndex:(FBSReplayContentItem *)indexItem text:(NSString *)text block:(void(^)(BOOL success,NSInteger count))complete;
- (void)rqLikeIndex:(FBSReplayContentItem *)indexItem block:(void(^)(BOOL success,NSInteger count))complete;
@end
