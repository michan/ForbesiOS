//
//  FBSPinglunLikeModel.m
//  Forbes
//
//  Created by 赵志辉 on 2019/11/20.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import "FBSPinglunLikeViewModel.h"
#import "FBSPinglunLikeModel.h"
#import "FBSPinglunLikeItem.h"

@interface FBSPinglunLikeViewModel ()

@property (nonatomic ,strong)FBSPinglunLikeModel *model;
@property (nonatomic ,strong)FBSGetRequest *detailAPI;
@end

@implementation FBSPinglunLikeViewModel

- (void)rqContent:(void(^)(BOOL success,NSInteger count))complete{
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    FBSUserData *user = [FBSUserData sharedData];
    [dic setObject:user.useId forKey:@"user_id"];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"yyyy-MM-dd";
    NSString *plainText = [NSString stringWithFormat:@"%@%@",user.account,[formatter stringFromDate:[NSDate date]]];
    [dic setObject:[HLWJMD5 MD5ForLower32Bate:plainText] forKey:@"token"];
    [dic setObject:self.pinglunItem.object_type forKey:@"object_type"];
    [dic setObject:self.pinglunItem.object_id forKey:@"object_id"];
    [dic setObject:self.pinglunItem.id forKey:@"comment_id"];
    [dic setObject:@(self.page) forKey:@"page"];
    self.detailAPI.requestParameter = dic;
    [self.detailAPI startWithSuccess:^(YBNetworkResponse * _Nonnull response) {
        self.model = [FBSPinglunLikeModel mj_objectWithKeyValues:response.responseObject];
        [self createItem];
        complete(YES, 0);
    } failure:^(YBNetworkResponse * _Nonnull response) {
        complete(NO, 0);
    }];
}
- (void)createItem{
    for (FBSPinglunLikeItemsItem *item in self.model.data.items) {
        FBSPinglunLikeItem *itemLike =[FBSPinglunLikeItem mj_objectWithKeyValues:[item mj_keyValues]];
        [self.items addObject:itemLike];
    }
}
- (FBSGetRequest *)detailAPI {
    if (!_detailAPI) {
        _detailAPI = [[FBSGetRequest alloc] init];
        _detailAPI.requestURI = @"comment/like/list";
    }
    return _detailAPI;
}

@end
