//
//  FBSPinglunViewModel.m
//  Forbes
//
//  Created by 赵志辉 on 2019/11/13.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import "FBSPinglunViewModel.h"
#import "FBSPinglunModel.h"
#import "FBSPinglunItem.h"
#import "HLWJMD5.h"

@interface FBSPinglunViewModel()

@property (nonatomic ,strong)FBSPinglunModel *model;
@property (nonatomic ,strong)FBSGetRequest *detailAPI;
@property (nonatomic ,strong)FBSPostRequest *sendApi;
@property (nonatomic ,strong)FBSPostRequest *isLikeApi;



@end

@implementation FBSPinglunViewModel

- (void)rqContent:(void(^)(BOOL success,NSInteger count))complete{
    [self.detailAPI startWithSuccess:^(YBNetworkResponse * _Nonnull response) {
        
        self.model = [FBSPinglunModel mj_objectWithKeyValues:response.responseObject];
       
        [self createItem];
        complete(YES, 0);
    } failure:^(YBNetworkResponse * _Nonnull response) {
        complete(NO, 0);
    }];
}
- (void)rqSend:(void(^)(BOOL success,NSInteger count))complete{
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    FBSUserData *user = [FBSUserData sharedData];
    [dic setObject:user.useId forKey:@"user_id"];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"yyyy-MM-dd";
    NSString *plainText = [NSString stringWithFormat:@"%@%@",user.account,[formatter stringFromDate:[NSDate date]]];
    [dic setObject:[HLWJMD5 MD5ForLower32Bate:plainText] forKey:@"token"];
    [dic setObject:@"1" forKey:@"object_type"];
    [dic setObject:@"45577" forKey:@"object_id"];
    [dic setObject:@"15" forKey:@"pid"];
    [dic setObject:@"我就想弄点测试数据我就想弄点测试数据我就想弄点测试数据我就想弄点测试数据我就想弄点测试数据我就想弄点测试数据" forKey:@"content"];
    self.sendApi.requestParameter = dic;
    [self.sendApi startWithSuccess:^(YBNetworkResponse * _Nonnull response) {
        complete(YES, 0);
    } failure:^(YBNetworkResponse * _Nonnull response) {
        complete(NO, 0);
    }];
}
- (void)rqLike:(void(^)(BOOL success,NSInteger count))complete{
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    FBSUserData *user = [FBSUserData sharedData];
    [dic setObject:user.useId forKey:@"user_id"];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"yyyy-MM-dd";
    NSString *plainText = [NSString stringWithFormat:@"%@%@",user.account,[formatter stringFromDate:[NSDate date]]];
    [dic setObject:[HLWJMD5 MD5ForLower32Bate:plainText] forKey:@"token"];
    [dic setObject:@"1" forKey:@"object_type"];
    [dic setObject:@"45577" forKey:@"object_id"];
    [dic setObject:@"15" forKey:@"comment_id"];
    [dic setObject:@"like" forKey:@"action"];
    self.isLikeApi.requestParameter = dic;
    [self.isLikeApi startWithSuccess:^(YBNetworkResponse * _Nonnull response) {
        complete(YES, 0);
    } failure:^(YBNetworkResponse * _Nonnull response) {
        complete(NO, 0);
    }];
}
- (void)createItem{
    NSMutableArray *array = [NSMutableArray array];
    for (FBSPinglunModelItemsItem *item in self.model.data.items) {
        NSDictionary *dic = [item mj_keyValues];
        FBSPinglunItem *itemP = [[FBSPinglunItem alloc] init];
        itemP = [FBSPinglunItem mj_objectWithKeyValues:dic];
        [array addObject:itemP];
        
    }
    [array addObjectsFromArray:array];
    [array addObjectsFromArray:array];
    [array addObjectsFromArray:array];
    [array addObjectsFromArray:array];
    [array addObjectsFromArray:array];
    self.items = array;

}

- (FBSPostRequest *)sendApi {
    if (!_sendApi) {
        _sendApi = [[FBSPostRequest alloc] init];
        _sendApi.requestURI = @"comment/add";
    }
    return _sendApi;
}
- (FBSPostRequest *)isLikeApi {
    if (!_isLikeApi) {
        _isLikeApi = [[FBSPostRequest alloc] init];
        _isLikeApi.requestURI = @"comment/like";
    }
    return _isLikeApi;
}

- (FBSGetRequest *)detailAPI {
    if (!_detailAPI) {
        _detailAPI = [[FBSGetRequest alloc] init];
        _detailAPI.requestURI = @"comment/list";
        _detailAPI.requestParameter = @{@"object_type":@"1",@"object_id":@"45577"};
    }
    return _detailAPI;
}
@end
