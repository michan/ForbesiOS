//
//  FBSPinglunViewModel.h
//  Forbes
//
//  Created by 赵志辉 on 2019/11/13.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import "FBSTableViewModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface FBSPinglunViewModel : FBSTableViewModel
- (void)rqContent:(void(^)(BOOL success,NSInteger count))complete;
- (void)rqSend:(void(^)(BOOL success,NSInteger count))complete;
- (void)rqLike:(void(^)(BOOL success,NSInteger count))complete;

@end

NS_ASSUME_NONNULL_END
