//
//  FBSPinglunLikeModel.h
//  Forbes
//
//  Created by 赵志辉 on 2019/11/20.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import "FBSTableViewModel.h"
#import "FBSPinglunItem.h"
@interface FBSPinglunLikeViewModel : FBSTableViewModel

- (void)rqContent:(void(^)(BOOL success,NSInteger count))complete;
@property(nonatomic, strong) FBSPinglunItem *pinglunItem;
@end
