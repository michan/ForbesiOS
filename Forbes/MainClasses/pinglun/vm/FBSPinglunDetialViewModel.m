//
//  FBSPinglunDetialViewModel.m
//  Forbes
//
//  Created by 赵志辉 on 2019/11/17.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import "FBSPinglunDetialViewModel.h"
#import "FBSPinglunModel.h"

@interface FBSPinglunDetialViewModel ()

@property (nonatomic ,strong)FBSPinglunModel *model;
@property (nonatomic ,strong)FBSGetRequest *detailAPI;
@property(nonatomic, strong) FBSPostRequest *sendApi;
@property(nonatomic, strong) FBSPostRequest *isLikeApi;
@end

@implementation FBSPinglunDetialViewModel

- (void)rqContent:(void(^)(BOOL success,NSInteger count))complete;{
    [self createItem];
    complete(YES, 0);
}
- (void)createItem{
    for (FBSReplayContentItem *item in self.pinglunItem.reply) {
        [self.items addObject:item];
    }
}
- (void)rqSendIndex:(FBSReplayContentItem *)indexItem text:(NSString *)text block:(void(^)(BOOL success,NSInteger count))complete{
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    FBSUserData *user = [FBSUserData sharedData];
    [dic setObject:user.useId forKey:@"user_id"];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"yyyy-MM-dd";
    NSString *plainText = [NSString stringWithFormat:@"%@%@",user.account,[formatter stringFromDate:[NSDate date]]];
    [dic setObject:[HLWJMD5 MD5ForLower32Bate:plainText] forKey:@"token"];
    [dic setObject:indexItem.object_type forKey:@"object_type"];
    [dic setObject:indexItem.object_id forKey:@"object_id"];
    [dic setObject:indexItem.id forKey:@"pid"];
    [dic setObject:text forKey:@"content"];
    self.sendApi.requestParameter = dic;
    [self.sendApi startWithSuccess:^(YBNetworkResponse * _Nonnull response) {
        complete(YES, 0);
    } failure:^(YBNetworkResponse * _Nonnull response) {
        complete(NO, 0);
    }];
}
- (void)rqLikeIndex:(FBSReplayContentItem *)indexItem block:(void(^)(BOOL success,NSInteger count))complete{
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    FBSUserData *user = [FBSUserData sharedData];
    [dic setObject:user.useId forKey:@"user_id"];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"yyyy-MM-dd";
    NSString *plainText = [NSString stringWithFormat:@"%@%@",user.account,[formatter stringFromDate:[NSDate date]]];
    [dic setObject:[HLWJMD5 MD5ForLower32Bate:plainText] forKey:@"token"];
    [dic setObject:indexItem.object_type forKey:@"object_type"];
    [dic setObject:indexItem.object_id forKey:@"object_id"];
    [dic setObject:indexItem.id forKey:@"comment_id"];
    if([indexItem.has_like isEqualToString:@"Y"]){
        [dic setObject:@"unlike" forKey:@"action"];
    }else{
        [dic setObject:@"like" forKey:@"action"];
    }
    self.isLikeApi.requestParameter = dic;
    [self.isLikeApi startWithSuccess:^(YBNetworkResponse * _Nonnull response) {
        complete(YES, 0);
    } failure:^(YBNetworkResponse * _Nonnull response) {
        complete(NO, 0);
    }];
}
- (FBSPostRequest *)isLikeApi {
    if (!_isLikeApi) {
        _isLikeApi = [[FBSPostRequest alloc] init];
        _isLikeApi.requestURI = @"comment/like";
    }
    return _isLikeApi;
}
- (FBSGetRequest *)detailAPI {
    if (!_detailAPI) {
        _detailAPI = [[FBSGetRequest alloc] init];
        _detailAPI.requestURI = @"";
    }
    return _detailAPI;
}
- (FBSPostRequest *)sendApi {
    if (!_sendApi) {
        _sendApi = [[FBSPostRequest alloc] init];
        _sendApi.requestURI = @"comment/add";
    }
    return _sendApi;
}
@end
