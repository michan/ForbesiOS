//
//  FBSPinglunLikeItem.h
//  Forbes
//
//  Created by 赵志辉 on 2019/11/20.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import "FBSItem.h"

NS_ASSUME_NONNULL_BEGIN

@interface FBSPinglunLikeItem : FBSItem
@property (nonatomic , copy) NSString              * user_id;
@property (nonatomic , copy) NSString              * account;
@property (nonatomic , copy) NSString              * nickname;
@property (nonatomic , copy) NSString              * image_url;
@end

NS_ASSUME_NONNULL_END
