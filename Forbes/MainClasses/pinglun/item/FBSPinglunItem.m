//
//  FBSPinglunItem.m
//  Forbes
//
//  Created by 赵志辉 on 2019/11/13.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import "FBSPinglunItem.h"

@implementation FBSPinglunItem

+ (NSDictionary *)mj_objectClassInArray {
    return @{@"reply" : @"FBSReplayContentItem"};
}
- (void)updateTab{
    if (self.isHeight) {
        return;
    }
    self.isHeight = YES;
    CGFloat strHeight;
    if (self.reply.count > 0) {
        self.isShowPinlun = YES;
        if (self.reply.count > 2) {
            strHeight = WScale(9) + WScale(14) + WScale(9) + WScale(9) * 2;
            self.isShowCheckMore = YES;
        }else{
            strHeight = WScale(9) + WScale(9) * self.reply.count;
            self.isShowCheckMore = NO;
        }
    }else{
        self.isShowPinlun = NO;
        strHeight = 0;
    }
    int i = 0;
    for (FBSReplayContentItem *item in self.reply) {
        NSString *str = [NSString stringWithFormat:@"%@ ：%@",item.nickname,item.content];
        strHeight = strHeight +  [self contentHeight:str withWidth:WScale(297)   withFont:WScale(14)];
        i ++ ;
        if (i == 2) {
            break;
        }
    }
    self.pinglunHeight = strHeight;
    self.titlteHeight = [self contentHeight:self.content withWidth:WScale(294) withFont:WScale(16)];
    self.allHeight = self.pinglunHeight + self.titlteHeight + WScale(47) + WScale(8) + WScale(14) + WScale(13) + (self.pinglunHeight > 0 ? WScale(8) : WScale(0));
    
}
-(CGFloat)contentHeight:(NSString *)content withWidth:(float)width withFont:(float)f
{
    CGRect rect=[content boundingRectWithSize:CGSizeMake(width, 0) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:f]} context:nil];
    return rect.size.height;
}
@end
