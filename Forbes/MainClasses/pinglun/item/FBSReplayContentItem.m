//
//  FBSReplayContentItem.m
//  Forbes
//
//  Created by 赵志辉 on 2019/11/15.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import "FBSReplayContentItem.h"

@implementation FBSReplayContentItem

- (void)updateTab{
    if (self.isHeight) {
        return;
    }
    self.isHeight = YES;
    self.titlteHeight = [self contentHeight:self.content withWidth:WScale(294) withFont:WScale(16)];
    self.allHeight = self.titlteHeight + WScale(47) + WScale(8) + WScale(14) + WScale(13);
    
}
-(CGFloat)contentHeight:(NSString *)content withWidth:(float)width withFont:(float)f
{
    CGRect rect=[content boundingRectWithSize:CGSizeMake(width, 0) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:f]} context:nil];
    return rect.size.height;
}
@end
