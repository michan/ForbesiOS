//
//  FBSPinglunHeaderItem.m
//  Forbes
//
//  Created by 赵志辉 on 2019/11/18.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import "FBSPinglunHeaderItem.h"

@implementation FBSPinglunHeaderItem
+ (NSDictionary *)mj_objectClassInArray {
    return @{@"reply" : @"FBSReplayContentItem"};
}
- (void)updateTab{
    self.titlteHeight = [self contentHeight:self.content withWidth:WScale(294) withFont:WScale(14)];
    self.allHeight = WScale(90) + self.titlteHeight;
}
-(CGFloat)contentHeight:(NSString *)content withWidth:(float)width withFont:(float)f
{
    CGRect rect=[content boundingRectWithSize:CGSizeMake(width, 0) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:f]} context:nil];
    return rect.size.height;
}
@end
