//
//  FBSPinglunItem.h
//  Forbes
//
//  Created by 赵志辉 on 2019/11/13.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import "FBSItem.h"
#import "FBSReplayContentItem.h"

NS_ASSUME_NONNULL_BEGIN

@interface FBSPinglunItem :FBSItem

@property (nonatomic , copy) NSString              *id;
@property (nonatomic , copy) NSString              *status;
@property (nonatomic , copy) NSString              *object_id;
@property (nonatomic , copy) NSString              *object_type;
@property (nonatomic , copy) NSString              *pid;
@property (nonatomic , copy) NSString              *user_id;
@property (nonatomic , copy) NSString              * content;
@property (nonatomic , copy) NSString              *created_at;
@property (nonatomic , copy) NSString              *updated_at;
@property (nonatomic , copy) NSString              *like_count;
@property (nonatomic , copy) NSString              * nickname;
@property (nonatomic , copy) NSString              * image_url;
@property (nonatomic , copy) NSString              * has_like;

@property (nonatomic , strong) NSArray <FBSReplayContentItem *>              * reply;

@property (nonatomic , assign) BOOL isShowPinlun;
@property (nonatomic , assign) BOOL isShowCheckMore;

@property (nonatomic , assign) CGFloat allHeight;
@property (nonatomic , assign) CGFloat titlteHeight;
@property (nonatomic , assign) CGFloat pinglunHeight;
@property (nonatomic , assign) BOOL isHeight;
- (void)updateTab;






@end


NS_ASSUME_NONNULL_END
