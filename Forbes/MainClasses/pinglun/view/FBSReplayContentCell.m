//
//  FBSReplayContentCell.m
//  Forbes
//
//  Created by 赵志辉 on 2019/11/15.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import "FBSReplayContentCell.h"
#import "FBSReplayContentItem.h"

@interface FBSReplayContentCell ()

@end

@implementation FBSReplayContentCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self createView];
    }
    return self;
}
#pragma mark - view
- (void)createView{
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    self.backgroundColor = [UIColor clearColor];
    self.contentView.backgroundColor = [UIColor clearColor];
    _replayLabel = [[UILabel alloc]init];
    _replayLabel.numberOfLines = 0;
    _replayLabel.font = [UIFont systemFontOfSize:WScale(14)];
    _replayLabel.textColor = UIColorHex(0x333333);
    _replayLabel.backgroundColor = [UIColor clearColor];
    [self.contentView addSubview:self.replayLabel];
    [_replayLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0);
        make.top.mas_equalTo(0);
        make.bottom.mas_equalTo(WScale(-8));
    }];
}
#pragma mark - model
- (void)setItem:(FBSReplayContentItem *)item {
    if (![item isKindOfClass:[FBSReplayContentItem class]]) {
        return ;
    }
    self.replayLabel.text =  [NSString stringWithFormat:@"%@ ：%@",item.nickname,item.content];
    self.replayLabel.textColor = UIColorHex(0x333333);
}
#pragma mark - property
@end
