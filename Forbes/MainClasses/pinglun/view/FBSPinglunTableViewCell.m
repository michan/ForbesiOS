//
//  FBSPinglunTableViewCell.m
//  Forbes
//
//  Created by 赵志辉 on 2019/11/13.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import "FBSPinglunTableViewCell.h"
#import "FBSReplayContentCell.h"
#import "FBSReplayContentItem.h"
#import "FBSPinglunItem.h"

@interface FBSPinglunTableViewCell()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong) UIImageView *headerV;
@property(nonatomic, strong) UILabel *titleLabel;
@property (nonatomic,strong) UILabel *nameLabel;
@property (nonatomic,strong) YYLabel *addressAndTimeNumLabel;
@property (nonatomic,strong) UIButton *shareBT;
@property (nonatomic,strong) UIButton *attentBt;
@property (nonatomic,strong) UILabel *attentLabel;
@property (nonatomic,strong) UIView *pinLunView;
@property (nonatomic,strong) UITableView *replayTable;
@property (nonatomic,strong) NSMutableArray *replayArray;
@property (nonatomic,strong) UIButton *checkMore;


@end

@implementation FBSPinglunTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self createView];
        
    }
    return self;
}
- (void)createView{
    [self.contentView addSubview:self.headerV];
    [self.contentView addSubview:self.nameLabel];
    [self.contentView addSubview:self.titleLabel];
    [self.contentView addSubview:self.addressAndTimeNumLabel];
    [self.contentView addSubview:self.shareBT];
    [self.contentView addSubview:self.attentBt];
    [self.contentView addSubview:self.attentLabel];
    [self.contentView addSubview:self.pinLunView];    
    
    [self.headerV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.mas_left).offset(WScale(10));
        make.top.mas_equalTo(self.mas_top).offset(WScale(13));
        make.height.mas_equalTo(WScale(34));
        make.width.mas_equalTo(WScale(34));
    }];
    [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.headerV.mas_right).offset(WScale(6));
        make.centerY.mas_equalTo(self.headerV.mas_centerY);
        make.right.mas_equalTo(self.mas_right).offset(WScale(-10));
        make.height.mas_equalTo(WScale(20));
    }];
    [self.attentBt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(WScale(14));
        make.centerY.mas_equalTo(self.headerV.mas_centerY);
        make.right.mas_equalTo(self.mas_right).offset(WScale(-13));
        make.height.mas_equalTo(WScale(14));
    }];
    [self.attentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(WScale(30));
        make.centerY.mas_equalTo(self.headerV.mas_centerY);
        make.right.mas_equalTo(self.attentBt.mas_left).offset(WScale(-3));
        make.height.mas_equalTo(WScale(14));
    }];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.headerV.mas_right).offset(WScale(6));
        make.top.mas_equalTo(self.headerV.mas_bottom);
        make.width.mas_equalTo(WScale(294));
    }];
    [self.addressAndTimeNumLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.headerV.mas_right).offset(WScale(6));
        make.top.mas_equalTo(self.titleLabel.mas_bottom).offset(WScale(8));
        make.right.mas_equalTo(self.mas_right).offset(WScale(-33));
        make.height.mas_equalTo(WScale(14));
    }];
    [self.shareBT mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(WScale(14));
        make.centerY.mas_equalTo(self.addressAndTimeNumLabel.mas_centerY);
        make.right.mas_equalTo(self.mas_right).offset(WScale(-13));
        make.height.mas_equalTo(WScale(14));
    }];
    self.pinLunView.backgroundColor = UIColorHex(0xF5F5F5);
    [self.pinLunView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.headerV.mas_right).offset(WScale(6));
        make.top.mas_equalTo(self.addressAndTimeNumLabel.mas_bottom);
        make.right.mas_equalTo(self.mas_right).offset(WScale(-33));
        make.bottom.mas_equalTo(self.mas_bottom).offset(-13);
    }];
    _replayTable = [[UITableView alloc]init];
    _replayTable.scrollEnabled = NO;
    _replayTable.backgroundColor = [UIColor clearColor];
    _replayTable.separatorColor=[UIColor clearColor];
    _replayTable.dataSource = self;
    _replayTable.delegate = self;
    
    [_replayTable registerClass:[FBSReplayContentCell class] forCellReuseIdentifier:[FBSReplayContentItem reuseIdentifier]];
    self.checkMore = [UIButton buttonWithType:UIButtonTypeCustom];
    self.checkMore.titleLabel.font = [UIFont systemFontOfSize:13];
    [self.checkMore setTitleColor:Hexcolor(0x666666) forState:UIControlStateNormal];
    self.checkMore.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [self.checkMore setTitle:@"查看更多回复..." forState:UIControlStateNormal];
    [self.pinLunView addSubview:self.replayTable];
    [self.pinLunView addSubview:self.checkMore];
    
}
- (void)attent{
    if (self.delegate && [self.delegate respondsToSelector:@selector(fbsCell:didClickButtonAtIndex:)]) {
        [self.delegate fbsCell:self didClickButtonAtIndex:0];
    }
}
- (void)share{
    if (self.delegate && [self.delegate respondsToSelector:@selector(fbsCell:didClickButtonAtIndex:)]) {
        [self.delegate fbsCell:self didClickButtonAtIndex:1];
    }
}
- (void)changeLike{
    FBSPinglunItem *item = (FBSPinglunItem *)self.item;
    if ([item.has_like isEqualToString:@"Y"]) {
        item.has_like = @"N";
        self.attentBt.selected = NO;
        int count = [item.like_count intValue];
        count--;
        item.like_count = [NSString stringWithFormat:@"%d",count];
        self.attentLabel.text = item.like_count;
    }else{
        item.has_like = @"Y";
        self.attentBt.selected = YES;
        int count = [item.like_count intValue];
        count++;
        item.like_count = [NSString stringWithFormat:@"%d",count];
        self.attentLabel.text = item.like_count;
    }
}
- (void)setItem:(FBSItem *)item
{
    if([item isKindOfClass:[FBSPinglunItem class]]){
        FBSPinglunItem *pinlunItem = (FBSPinglunItem *)item;
        [super setItem:item];
        [self.headerV sd_setImageWithURL:[NSURL URLWithString:pinlunItem.image_url]];
        self.nameLabel.text = pinlunItem.nickname;
        self.titleLabel.text = pinlunItem.content;
        NSString *time = [NSString getTimestamp:pinlunItem.created_at formatter:@"MM月dd日"];
        NSString *timeAnd = [NSString stringWithFormat:@"%@  %@",time,@"回复他"];
        self.addressAndTimeNumLabel.text = timeAnd;
        UIColor *color = Hexcolor(0x999999);
        NSDictionary *attributes = @{NSFontAttributeName:kLabelFontSize14, NSForegroundColorAttributeName: color};
        NSMutableAttributedString *text = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@",timeAnd] attributes:attributes];
        //设置高亮色和点击事件
        [text setTextHighlightRange:[[text string] rangeOfString:@"回复他"] color:Hexcolor(0x333333) backgroundColor:[UIColor clearColor] tapAction:^(UIView * _Nonnull containerView, NSAttributedString * _Nonnull text, NSRange range, CGRect rect) {
            if ([self.delegate respondsToSelector:@selector(fbsCell:didClickButtonAtIndex:)]) {
                [self.delegate fbsCell:self didClickButtonAtIndex:3];
            }
        }];
        self.addressAndTimeNumLabel.attributedText = text;
        self.attentLabel.text = pinlunItem.like_count;
        [self.attentBt setTitleColor:Hexcolor(0x333333) forState:UIControlStateNormal];
        if ([pinlunItem.has_like isEqualToString:@"Y"]) {
            self.attentBt.selected = YES;
        }else{
            self.attentBt.selected = NO;
        }
        self.replayArray = [pinlunItem.reply mutableCopy];
        [self updateTab:pinlunItem];
    }else if([item isKindOfClass:[FBSReplayContentItem class]]){
        FBSReplayContentItem *pinlunItem = (FBSReplayContentItem *)item;
        [super setItem:item];
        [self.headerV sd_setImageWithURL:[NSURL URLWithString:pinlunItem.image_url]];
        self.nameLabel.text = pinlunItem.nickname;
        self.titleLabel.text = pinlunItem.content;
        NSString *time = [NSString getTimestamp:pinlunItem.created_at formatter:@"MM月dd日"];
        NSString *timeAnd = [NSString stringWithFormat:@"%@  %@",time,@"回复他"];
        self.addressAndTimeNumLabel.text = timeAnd;
        UIColor *color = Hexcolor(0x999999);
        NSDictionary *attributes = @{NSFontAttributeName:kLabelFontSize14, NSForegroundColorAttributeName: color};
        NSMutableAttributedString *text = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@",timeAnd] attributes:attributes];
        //设置高亮色和点击事件
        [text setTextHighlightRange:[[text string] rangeOfString:@"回复他"] color:Hexcolor(0x333333) backgroundColor:[UIColor clearColor] tapAction:^(UIView * _Nonnull containerView, NSAttributedString * _Nonnull text, NSRange range, CGRect rect) {
            if ([self.delegate respondsToSelector:@selector(fbsCell:didClickButtonAtIndex:)]) {
                [self.delegate fbsCell:self didClickButtonAtIndex:3];
            }
        }];
        self.addressAndTimeNumLabel.attributedText = text;
        self.attentLabel.text = pinlunItem.like_count;
        [self.attentBt setTitleColor:Hexcolor(0x333333) forState:UIControlStateNormal];
        if ([pinlunItem.has_like isEqualToString:@"Y"]) {
            self.attentBt.selected = YES;
        }else{
            self.attentBt.selected = NO;
        }
        self.pinLunView.hidden = YES;
        self.checkMore.hidden = YES;
    }
}
- (void)updateTab:(FBSPinglunItem *)item{
    self.pinLunView.hidden = !item.isShowPinlun;
    self.checkMore.hidden = !item.isShowCheckMore;
    
    [self.pinLunView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.mas_left).offset(WScale(49));
        make.top.mas_equalTo(self.addressAndTimeNumLabel.mas_bottom).offset(WScale(8));
        make.right.mas_equalTo(self.mas_right).offset(WScale(-11));
        make.height.mas_equalTo(item.pinglunHeight);
    }];
    [_replayTable mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(WScale(9));
        make.right.mas_equalTo(WScale(-10));
        make.top.mas_equalTo(self.pinLunView.mas_top).offset(WScale(9));
        if (item.isShowCheckMore) {
            make.bottom.mas_equalTo(self.pinLunView.mas_bottom).offset(WScale(-25));
        }else{
            make.bottom.mas_equalTo(self.pinLunView.mas_bottom).offset(WScale(-9));
        }
    }];
    if (item.isShowCheckMore) {
        [self.checkMore mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(WScale(9));
            make.right.mas_equalTo(WScale(-10));      make.height.mas_equalTo(WScale(14));
            make.bottom.mas_equalTo(self.pinLunView.mas_bottom).offset(WScale(-9));
        }];
    }
    [_replayTable reloadData];
}
-(CGFloat)contentHeight:(NSString *)content withWidth:(float)width withFont:(float)f
{
    CGRect rect=[content boundingRectWithSize:CGSizeMake(width, 0) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:f]} context:nil];
    return rect.size.height;
}
#pragma mark - delegate
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.replayArray.count > 2 ? 2 : self.replayArray.count;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    tableView.rowHeight = UITableViewAutomaticDimension;
    tableView.estimatedRowHeight = 30;
    return tableView.rowHeight;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    FBSReplayContentCell *cell=[tableView dequeueReusableCellWithIdentifier:[FBSReplayContentItem reuseIdentifier]];
    FBSReplayContentItem *item = self.replayArray[indexPath.row];
    cell.item = item;
    return cell;
}
#pragma mark - Get
- (UIImageView *)headerV{
    if(!_headerV){
        _headerV = [[UIImageView alloc] init];
        _headerV.image = [UIImage imageNamed:@"支付宝"];
    }
    return _headerV;
}
- (UILabel *)titleLabel{
    if(!_titleLabel){
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.font = kLabelFontSize16;
        _titleLabel.textColor = UIColorHex(0x333333);
        _titleLabel.text = @"我都去网点去OK武动乾坤我大青蛙多群无多权威的期望大萨达砂石厂";
        _titleLabel.numberOfLines = 0;
    }
    return _titleLabel;
}
- (UILabel *)nameLabel{
    if(!_nameLabel){
        _nameLabel = [[UILabel alloc] init];
        _nameLabel.font = kLabelFontSize16;
        _nameLabel.text = @"张三";
        _nameLabel.textColor = UIColorHex(0x333333);
    }
    return _nameLabel;
}
- (UILabel *)attentLabel{
    if(!_attentLabel){
        _attentLabel = [[UILabel alloc] init];
        _attentLabel.font = kLabelFontSize14;
        _attentLabel.text = @"张三";
        _attentLabel.textAlignment = NSTextAlignmentRight;
        _attentLabel.textColor = UIColorHex(0x333333);
    }
    return _attentLabel;
}
- (YYLabel *)addressAndTimeNumLabel{
    if(!_addressAndTimeNumLabel){
        _addressAndTimeNumLabel = [[YYLabel alloc] init];
        _addressAndTimeNumLabel.font = kLabelFontSize10;
        _addressAndTimeNumLabel.text = @"我都去网点去OK武动乾坤我大";
        
    }
    return _addressAndTimeNumLabel;
}
- (UIButton *)attentBt{
    if(!_attentBt){
        _attentBt = [[UIButton alloc] init];
        [_attentBt setImage:[UIImage imageNamed:@"video_praise1"] forState:UIControlStateNormal];
        [_attentBt setImage:[UIImage imageNamed:@"video_praise2"] forState:UIControlStateSelected];
        [_attentBt addTarget:self action:@selector(attent) forControlEvents:UIControlEventTouchUpInside];
        [_attentBt setTitleColor:Hexcolor(0x333333) forState:UIControlStateNormal];
        _attentBt.titleLabel.font = kLabelFontSize14;
        _attentBt.transform = CGAffineTransformMakeScale(-1.0, 1.0);
        _attentBt.titleLabel.transform = CGAffineTransformMakeScale(-1.0, 1.0);
        _attentBt.imageView.transform = CGAffineTransformMakeScale(-1.0, 1.0);
    }
    return _attentBt;
}
- (UIButton *)shareBT{
    if(!_shareBT){
        _shareBT = [[UIButton alloc] init];
        [_shareBT setImage:[UIImage imageNamed:@"video_more"] forState:UIControlStateNormal];
        [_shareBT  addTarget:self action:@selector(share) forControlEvents:UIControlEventTouchUpInside];


    }
    return _shareBT;
}
- (UIView *)pinLunView{
    if(!_pinLunView){
        _pinLunView = [[UIView alloc] init];
    }
    return _pinLunView;
}

@end
