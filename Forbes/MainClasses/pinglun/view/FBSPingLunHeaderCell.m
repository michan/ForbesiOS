//
//  FBSPinglunTableViewCell.m
//  Forbes
//
//  Created by 赵志辉 on 2019/11/13.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import "FBSPingLunHeaderCell.h"
#import "FBSPinglunHeaderItem.h"

@interface FBSPingLunHeaderCell()
@property (nonatomic,strong) UIImageView *headerV;
@property(nonatomic, strong) UILabel *titleLabel;
@property (nonatomic,strong) UILabel *nameLabel;
@property (nonatomic,strong) UILabel *addressAndTimeNumLabel;
@property (nonatomic,strong) UIButton *shareBT;
@property (nonatomic,strong) UIButton *attentBt;
@property (nonatomic,strong) UILabel *attentLabel;


@end

@implementation FBSPingLunHeaderCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self createView];
        
    }
    return self;
}
- (void)createView{
    [self.contentView addSubview:self.headerV];
    [self.contentView addSubview:self.nameLabel];
    [self.contentView addSubview:self.titleLabel];
    [self.contentView addSubview:self.addressAndTimeNumLabel];
    [self.contentView addSubview:self.shareBT];
    [self.contentView addSubview:self.attentBt];
    [self.contentView addSubview:self.attentLabel];

    [self.headerV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.mas_left).offset(WScale(10));
        make.top.mas_equalTo(self.mas_top).offset(WScale(13));
        make.height.mas_equalTo(WScale(34));
        make.width.mas_equalTo(WScale(34));
    }];
    [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.headerV.mas_right).offset(WScale(6));
        make.centerY.mas_equalTo(self.headerV.mas_centerY);
        make.right.mas_equalTo(self.mas_right).offset(WScale(-10));
        make.height.mas_equalTo(WScale(20));
    }];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.mas_left).offset(WScale(20));
        make.top.mas_equalTo(self.headerV.mas_bottom);
        make.right.mas_equalTo(self.mas_right).offset(WScale(-33));
    }];
    [self.addressAndTimeNumLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.mas_left).offset(WScale(20));
        make.top.mas_equalTo(self.titleLabel.mas_bottom).offset(WScale(8));
        make.right.mas_equalTo(self.mas_right).offset(WScale(-33));
        make.height.mas_equalTo(WScale(14));
    }];
    [self.shareBT mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(WScale(14));
        make.centerY.mas_equalTo(self.addressAndTimeNumLabel.mas_centerY);
        make.right.mas_equalTo(self.mas_right).offset(WScale(-13));
        make.height.mas_equalTo(WScale(14));
    }];
    [self.attentBt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(WScale(14));
        make.centerY.mas_equalTo(self.shareBT.mas_centerY);
        make.right.mas_equalTo(self.shareBT.mas_left).offset(WScale(-21));
        make.height.mas_equalTo(WScale(14));
    }];
    [self.attentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(WScale(30));
        make.centerY.mas_equalTo(self.attentBt.mas_centerY);
        make.right.mas_equalTo(self.attentBt.mas_left).offset(WScale(-3));
        make.height.mas_equalTo(WScale(14));
    }];
}
- (void)attent{
    if (self.delegate && [self.delegate respondsToSelector:@selector(fbsCell:didClickButtonAtIndex:)]) {
        [self.delegate fbsCell:self didClickButtonAtIndex:0];
    }
}
- (void)share{
    if (self.delegate && [self.delegate respondsToSelector:@selector(fbsCell:didClickButtonAtIndex:)]) {
        [self.delegate fbsCell:self didClickButtonAtIndex:1];
    }
}
- (void)changeLike{
    FBSPinglunHeaderItem *item = (FBSPinglunHeaderItem *)self.item;
    if ([item.has_like isEqualToString:@"Y"]) {
        item.has_like = @"N";
        self.attentBt.selected = NO;
        int count = [item.like_count intValue];
        count--;
        item.like_count = [NSString stringWithFormat:@"%d",count];
        self.attentLabel.text = item.like_count;
    }else{
        item.has_like = @"Y";
        self.attentBt.selected = YES;
        int count = [item.like_count intValue];
        count++;
        item.like_count = [NSString stringWithFormat:@"%d",count];
        self.attentLabel.text = item.like_count;
    }
}
- (void)setItem:(FBSPinglunHeaderItem *)item
{
    if(![item isKindOfClass:[FBSPinglunHeaderItem class]]){
        return;
    }
    [super setItem:item];
    self.nameLabel.text = item.nickname;
    self.titleLabel.text = item.content;
    self.addressAndTimeNumLabel.text = item.created_at;
    self.attentLabel.text = item.like_count;
    if ([item.has_like isEqualToString:@"Y"]) {
        self.attentBt.selected = YES;
    }else{
        self.attentBt.selected = NO;
    }
}
#pragma mark - Get
- (UIImageView *)headerV{
    if(!_headerV){
        _headerV = [[UIImageView alloc] init];
        _headerV.image = [UIImage imageNamed:@"支付宝"];
    }
    return _headerV;
}
- (UILabel *)titleLabel{
    if(!_titleLabel){
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.font = kLabelFontSize16;
        _titleLabel.textColor = UIColorHex(0x333333);
        _titleLabel.text = @"我都去网点去OK武动乾坤我大青蛙多群无多权威的期望大萨达砂石厂";
        _titleLabel.numberOfLines = 0;
    }
    return _titleLabel;
}
- (UILabel *)nameLabel{
    if(!_nameLabel){
        _nameLabel = [[UILabel alloc] init];
        _nameLabel.font = kLabelFontSize16;
        _nameLabel.text = @"张三";
        _nameLabel.textColor = UIColorHex(0x333333);
    }
    return _nameLabel;
}
- (UILabel *)addressAndTimeNumLabel{
    if(!_addressAndTimeNumLabel){
        _addressAndTimeNumLabel = [[UILabel alloc] init];
        _addressAndTimeNumLabel.font = kLabelFontSize10;
        _addressAndTimeNumLabel.text = @"我都去网点去OK武动乾坤我大";
        
    }
    return _addressAndTimeNumLabel;
}
- (UIButton *)attentBt{
    if(!_attentBt){
        _attentBt = [[UIButton alloc] init];
        [_attentBt setImage:[UIImage imageNamed:@"video_praise1"] forState:UIControlStateNormal];
        [_attentBt setImage:[UIImage imageNamed:@"video_praise2"] forState:UIControlStateSelected];
        [_attentBt addTarget:self action:@selector(attent) forControlEvents:UIControlEventTouchUpInside];
    }
    return _attentBt;
}
- (UILabel *)attentLabel{
    if(!_attentLabel){
        _attentLabel = [[UILabel alloc] init];
        _attentLabel.font = kLabelFontSize14;
        _attentLabel.text = @"张三";
        _attentLabel.textAlignment = NSTextAlignmentRight;
        _attentLabel.textColor = UIColorHex(0x333333);
    }
    return _attentLabel;
}
- (UIButton *)shareBT{
    if(!_shareBT){
        _shareBT = [[UIButton alloc] init];
        [_shareBT setImage:[UIImage imageNamed:@"video_more"] forState:UIControlStateNormal];
        [_shareBT  addTarget:self action:@selector(share) forControlEvents:UIControlEventTouchUpInside];
        
        
    }
    return _shareBT;
}

@end
