//
//  FBSArticalFooterView.m
//  Forbes
//
//  Created by 赵志辉 on 2019/9/28.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import "FBSPinglunReplayFooterView.h"
@interface FBSPinglunReplayFooterView()
@property(nonatomic, strong)UIButton *say;


@end

@implementation FBSPinglunReplayFooterView
- (instancetype)init{
    if (self = [super init]) {
        self.backgroundColor = Hexcolor(0xFFFFFF);
        [self addSubview:self.say];
        [self createView];
    }
    return self;
}
- (void)createView{
    
    [self.say mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self).offset(WScale(0));
        make.top.mas_equalTo(self).offset(WScale(6));
        make.right.mas_equalTo(self).offset(WScale(0));
        make.height.mas_equalTo(WScale(33));
    }];
    
}
- (void)cllick:(UIButton *)sender{
    if ([self.delegate respondsToSelector:@selector(bidInfoCell:didClickWithCompanyModel:)]) {
        [self.delegate bidInfoCell:self didClickWithCompanyModel:0];
    }
}
- (UIButton *)say{
    if (!_say) {
        _say = [[UIButton alloc] init];
        _say.tag = 200000;
        [_say setBackgroundImage:[UIImage imageNamed:@"pinglunBig"] forState:UIControlStateNormal];
        [_say addTarget:self action:@selector(cllick:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _say;
}


@end
