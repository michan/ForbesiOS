//
//  FBSClickZanCell.m
//  Forbes
//
//  Created by 赵志辉 on 2019/11/17.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import "FBSClickZanCell.h"
#import "FBSClickZanItem.h"

@interface FBSClickZanCell ()
@property (nonatomic,strong) UIImageView *headerV;
@property(nonatomic, strong) UILabel *titleLabel;
@property (nonatomic,strong) UILabel *nameLabel;
@end

@implementation FBSClickZanCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self createView];
    }
    return self;
}
#pragma mark - view
- (void)createView{
    [self.contentView addSubview:self.headerV];
    [self.contentView addSubview:self.nameLabel];
    [self.headerV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.mas_left).offset(WScale(10));
        make.top.mas_equalTo(self.mas_top).offset(WScale(13));
        make.height.mas_equalTo(WScale(34));
        make.width.mas_equalTo(WScale(34));
    }];
    [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.headerV.mas_right).offset(WScale(6));
        make.centerY.mas_equalTo(self.headerV.mas_centerY);
        make.right.mas_equalTo(self.mas_right).offset(WScale(-10));
        make.height.mas_equalTo(WScale(20));
    }];
}
#pragma mark - model
- (void)setItem:(FBSClickZanItem *)item {
    if (![item isKindOfClass:[FBSClickZanItem class]]) {
        return ;
    }
    [super setItem:item];
}
#pragma mark - property
- (UIImageView *)headerV{
    if(!_headerV){
        _headerV = [[UIImageView alloc] init];
        _headerV.image = [UIImage imageNamed:@"支付宝"];
    }
    return _headerV;
}
- (UILabel *)titleLabel{
    if(!_titleLabel){
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.font = kLabelFontSize16;
        _titleLabel.textColor = UIColorHex(0x333333);
        _titleLabel.text = @"我都去网点去OK武动乾坤我大青蛙多群无多权威的期望大萨达砂石厂";
        _titleLabel.numberOfLines = 0;
    }
    return _titleLabel;
}
@end
