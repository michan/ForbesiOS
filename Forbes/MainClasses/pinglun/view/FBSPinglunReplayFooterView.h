//
//  FBSPinglunReplayFooterView.h
//  Forbes
//
//  Created by 赵志辉 on 2019/11/21.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class FBSPinglunReplayFooterView;
@protocol FBSPinglunReplayFooterDelegate<NSObject>

@required
- (void)bidInfoCell:(FBSPinglunReplayFooterView *)bidInfoCell didClickWithCompanyModel:(int)type;
@end

@interface FBSPinglunReplayFooterView : UIView

@property(nonatomic, weak) id <FBSPinglunReplayFooterDelegate> delegate;
@end

NS_ASSUME_NONNULL_END
