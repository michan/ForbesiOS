//
//  FBSScrollview.h
//  Forbes
//
//  Created by 赵志辉 on 2019/11/18.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FBSScrollview : UIScrollView<UIGestureRecognizerDelegate>

@end

NS_ASSUME_NONNULL_END
