//
//  FBSReplayContentCell.h
//  Forbes
//
//  Created by 赵志辉 on 2019/11/15.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import "FBSCell.h"

@interface FBSReplayContentCell : FBSCell
@property(nonatomic,strong)UILabel *replayLabel;
@end
