//
//  FBSPinglunLikeCell.m
//  Forbes
//
//  Created by 赵志辉 on 2019/11/20.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import "FBSPinglunLikeCell.h"
#import "FBSPinglunLikeItem.h"

@interface FBSPinglunLikeCell ()
@property (nonatomic,strong) FBSarticleNavView *headerV;

@end

@implementation FBSPinglunLikeCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self createView];
    }
    return self;
}
#pragma mark - view
- (void)createView{
    [self.contentView addSubview:self.headerV];
    [self.headerV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.mas_left).offset(WScale(10));
        make.centerY.mas_equalTo(self.mas_centerY);
        make.right.mas_equalTo(self.mas_right).offset(WScale(-10));
        make.width.mas_equalTo(WScale(34));
    }];
    
}
#pragma mark - model
- (void)setItem:(FBSPinglunLikeItem *)item {
    if (![item isKindOfClass:[FBSPinglunLikeItem class]]) {
        return ;
    }
    [super setItem:item];
    [self.headerV setImage:item.image_url name:item.nickname];
}
#pragma mark - property
- (FBSarticleNavView *)headerV{
    if(!_headerV){
        _headerV = [[FBSarticleNavView alloc] init];
    }
    return _headerV;
}
@end
