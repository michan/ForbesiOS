//
//  FBSPinglunModel.m
//  Forbes
//
//  Created by 赵志辉 on 2019/11/13.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import "FBSPinglunModel.h"

@implementation FBSPinglunModelReplyItem

@end
@implementation FBSPinglunModelItemsItem

+ (NSDictionary *)mj_objectClassInArray {
    return @{@"reply" : @"FBSPinglunModelReplyItem"};
}


@end
@implementation FBSPinglunModelData

+ (NSDictionary *)mj_objectClassInArray {
    return @{@"items" : @"FBSPinglunModelItemsItem"};
}

@end
@implementation FBSPinglunModel


@end
