//
//  FBSPinglunModel.h
//  Forbes
//
//  Created by 赵志辉 on 2019/11/13.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import "FBSBaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface FBSPinglunModelReplyItem :NSObject
@property (nonatomic , copy) NSString              *id;
@property (nonatomic , copy) NSString              *status;
@property (nonatomic , copy) NSString              *object_id;
@property (nonatomic , copy) NSString              *object_type;
@property (nonatomic , copy) NSString              *pid;
@property (nonatomic , copy) NSString              *user_id;
@property (nonatomic , copy) NSString              *like_count;
@property (nonatomic , copy) NSString              * content;
@property (nonatomic , copy) NSString              *created_at;
@property (nonatomic , copy) NSString              *updated_at;
@property (nonatomic , copy) NSString              * nickname;
@property (nonatomic , copy) NSString              * image_url;
@property (nonatomic , copy) NSString              * has_like;


@end


@interface FBSPinglunModelItemsItem :NSObject
@property (nonatomic , copy) NSString              *id;
@property (nonatomic , copy) NSString              *status;
@property (nonatomic , copy) NSString              *object_id;
@property (nonatomic , copy) NSString              *object_type;
@property (nonatomic , copy) NSString              *pid;
@property (nonatomic , copy) NSString              *user_id;
@property (nonatomic , copy) NSString              * content;
@property (nonatomic , copy) NSString              *created_at;
@property (nonatomic , copy) NSString              *updated_at;
@property (nonatomic , copy) NSString              *like_count;
@property (nonatomic , copy) NSString              * nickname;
@property (nonatomic , copy) NSString              * image_url;
@property (nonatomic , copy) NSString              * has_like;
@property (nonatomic , strong) NSArray <FBSPinglunModelReplyItem *>              * reply;

@end


@interface FBSPinglunModelData :NSObject
@property (nonatomic , copy) NSString              *limit;
@property (nonatomic , copy) NSString              *page;
@property (nonatomic , copy) NSString              *total;
@property (nonatomic , strong) NSArray <FBSPinglunModelItemsItem *>              * items;

@end


@interface FBSPinglunModel :FBSBaseModel
@property (nonatomic , strong) FBSPinglunModelData  * data;
@end

NS_ASSUME_NONNULL_END
