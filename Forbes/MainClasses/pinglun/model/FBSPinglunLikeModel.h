//
//  FBSPinglunLikeModel.h
//  Forbes
//
//  Created by 赵志辉 on 2019/11/20.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import "FBSBaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface FBSPinglunLikeItemsItem :NSObject
@property (nonatomic , copy) NSString              * user_id;
@property (nonatomic , copy) NSString              * account;
@property (nonatomic , copy) NSString              * nickname;
@property (nonatomic , copy) NSString              * image_url;

@end


@interface FBSPinglunLikeData :NSObject
@property (nonatomic , assign) NSInteger              limit;
@property (nonatomic , assign) NSInteger              page;
@property (nonatomic , assign) NSInteger              total;
@property (nonatomic , strong) NSArray <FBSPinglunLikeItemsItem *>              * items;

@end


@interface FBSPinglunLikeModel :FBSBaseModel
@property (nonatomic , strong) FBSPinglunLikeData              * data;
@end

NS_ASSUME_NONNULL_END
