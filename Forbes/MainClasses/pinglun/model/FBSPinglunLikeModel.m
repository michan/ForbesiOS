//
//  FBSPinglunLikeModel.m
//  Forbes
//
//  Created by 赵志辉 on 2019/11/20.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import "FBSPinglunLikeModel.h"

@implementation FBSPinglunLikeItemsItem

@end

@implementation FBSPinglunLikeData

+ (NSDictionary *)mj_objectClassInArray {
    return @{@"items" : @"FBSPinglunLikeItemsItem"};
}

@end

@implementation FBSPinglunLikeModel

@end
