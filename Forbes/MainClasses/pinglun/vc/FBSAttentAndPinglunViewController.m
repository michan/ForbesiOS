//
//  FBSAttentAndPinglunViewController.m
//  Forbes
//
//  Created by 赵志辉 on 2019/11/18.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import "FBSAttentAndPinglunViewController.h"
#import "FBSHomeListViewController.h"
#import "FBSFollowViewController.h"
#import "FBSKeyNewsViewController.h"
#import "FBSBrandvoiceViewController.h"
#import "FBSSearchViewController.h"
#import "FBSChannelConfigController.h"

#import <WMPageController.h>
#import "FBSChannelManager.h"
#import "FBSPingLunDetialViewController.h"

#import "FBSPlusView.h"
#import "FBSNavView.h"
#import "FBSScrollview.h"
#import "FBSPingLunHeaderCell.h"
#import "FBSPinglunHeaderItem.h"
#import "FBSPinglunLikeViewController.h"
#import "FBSPinglunReplayFooterView.h"
#import "XHInputView.h"

@interface FBSAttentAndPinglunViewController ()
<
WMPageControllerDelegate,
WMPageControllerDataSource,
UIScrollViewDelegate,
FBSPinglunReplayFooterDelegate,
XHInputViewDelagete,
FBSCellDelegate
>
@property (nonatomic, strong) WMPageController *pageController;
@property (nonatomic, strong) NSArray *tabTitles;
@property (nonatomic, strong) FBSScrollview *scroller;
@property (nonatomic, strong) FBSTableViewController *tabVC;
@property (nonatomic, assign) BOOL isFirst;
@property (nonatomic, assign) CGFloat headerHeight;
@property(nonatomic, strong) FBSPostRequest *sendApi;
@property (nonatomic ,strong)FBSPostRequest *isLikeApi;
@property(nonatomic, strong) FBSPinglunReplayFooterView *footer;


@end

@implementation FBSAttentAndPinglunViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initNavBar];
    self.canScroll = YES;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeScrollStatus) name:@"leaveTop" object:nil];
    FBSScrollview *view = [[FBSScrollview alloc] initWithFrame:CGRectMake(0, kNavTopMargin, kScreenWidth, kScreenHeight)];
    self.scroller = view;
    FBSPingLunHeaderCell *cellHeader = [[FBSPingLunHeaderCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[FBSPinglunHeaderItem reuseIdentifier]];
    cellHeader.delegate = self;
    FBSPinglunHeaderItem *item = [FBSPinglunHeaderItem mj_objectWithKeyValues:[self.pinglunItem mj_keyValues]];
    [item updateTab];
    self.headerHeight = item.allHeight;
    cellHeader.item = item;
    cellHeader.frame = CGRectMake(0, 0, kScreenWidth, self.headerHeight);
    [view addSubview:cellHeader];
    view.delegate = self;
    view.contentSize = CGSizeMake(kScreenWidth, kScreenHeight + self.headerHeight);
    [self.view addSubview:view];
    
    self.pageController.scrollEnable = NO;
    [self.pageController.view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.scroller).offset(100);
        make.left.mas_equalTo(0);
        make.width.mas_equalTo(kScreenWidth);
        make.height.mas_equalTo(kScreenHeight - kNavTopMargin - kDiffTabBarH - WScale(45));
    }];
    [self.view addSubview:self.footer];
    [self.footer mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.view);
        if (IPHONE_X) {
            make.height.mas_equalTo(WScale(kDiffTabBarH + 45));
        }else{
            make.height.mas_equalTo(WScale(45));
        }
        make.bottom.mas_equalTo(self.view);
    }];
}
- (void)bidInfoCell:(FBSPinglunReplayFooterView *)bidInfoCell didClickWithCompanyModel:(int)type
{
    // 判断是否登录
    if (![self panduanIsLogin]) {
        return;
    }
    [self click];
}
- (BOOL)panduanIsLogin{
    if(![FBSUserData sharedData].isLogin){
        FBSLoginViewController *loginVC = [[FBSLoginViewController alloc] init];
        [self.navigationController pushViewController:loginVC animated:YES];
        return NO;
    }
    return YES;
}
- (void)click{
    [XHInputView showWithStyle:0 configurationBlock:^(XHInputView *inputView) {
        /** 请在此block中设置inputView属性 */
        
        /** 代理 */
        inputView.delegate = self;
        
        /** 占位符文字 */
        inputView.placeholder = @"请输入评论文字...";
        /** 设置最大输入字数 */
        inputView.maxCount = 50;
        /** 输入框颜色 */
        inputView.textViewBackgroundColor = [UIColor groupTableViewBackgroundColor];
        
        /** 更多属性设置,详见XHInputView.h文件 */
        
    } sendBlock:^BOOL(NSString *text) {
        if(text.length){
            NSLog(@"输入的信息为:%@",text);
            [self showHUD];
            [self rqSendIndex:nil text:text block:^(BOOL success, NSInteger count) {
                [self hideHUD];
                if (success) {
                    [self makeToast:@"提交成功,请等待审核"];
                }else{
                    [self makeToast:@"提交失败，请重试"];
                }
            }];
            return YES;//return YES,收起键盘
        }else{
            NSLog(@"显示提示框-请输入要评论的的内容");
            return NO;//return NO,不收键盘
        }
    }];
}
- (void)fbsCell:(FBSCell *)cell didClickButtonAtIndex:(NSInteger)index
{
    if ([cell isKindOfClass:[FBSPingLunHeaderCell class]]) {
        FBSPingLunHeaderCell *pinglunCell = (FBSPingLunHeaderCell *)cell;
        FBSPinglunHeaderItem *item = (FBSPinglunHeaderItem *)cell.item;
        if (index == 0) {
            WEAKSELF
            if (![self panduanIsLogin]) {
                return;
            }
            [self showHUD];
            [self rqLikeIndex:item block:^(BOOL success, NSInteger count) {
                [pinglunCell changeLike];
                self.pinglunItem.has_like = item.has_like;
                self.pinglunItem.like_count = item.like_count;
                [weakSelf hideHUD];
            }];
        }
    }
}

- (void)rqLikeIndex:(FBSPinglunHeaderItem *)indexItem block:(void(^)(BOOL success,NSInteger count))complete{
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    FBSUserData *user = [FBSUserData sharedData];
    [dic setObject:user.useId forKey:@"user_id"];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"yyyy-MM-dd";
    NSString *plainText = [NSString stringWithFormat:@"%@%@",user.account,[formatter stringFromDate:[NSDate date]]];
    [dic setObject:[HLWJMD5 MD5ForLower32Bate:plainText] forKey:@"token"];
    [dic setObject:self.pinglunItem.object_type forKey:@"object_type"];
    [dic setObject:self.pinglunItem.object_id forKey:@"object_id"];
    [dic setObject:self.pinglunItem.id forKey:@"comment_id"];
    if([self.pinglunItem.has_like isEqualToString:@"Y"]){
        [dic setObject:@"unlike" forKey:@"action"];
    }else{
        [dic setObject:@"like" forKey:@"action"];
        
    }
    self.isLikeApi.requestParameter = dic;
    [self.isLikeApi startWithSuccess:^(YBNetworkResponse * _Nonnull response) {
        complete(YES, 0);
    } failure:^(YBNetworkResponse * _Nonnull response) {
        complete(NO, 0);
    }];
}
- (void)rqSendIndex:(FBSPinglunItem *)indexItem text:(NSString *)text block:(void(^)(BOOL success,NSInteger count))complete{
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    FBSUserData *user = [FBSUserData sharedData];
    [dic setObject:user.useId forKey:@"user_id"];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"yyyy-MM-dd";
    NSString *plainText = [NSString stringWithFormat:@"%@%@",user.account,[formatter stringFromDate:[NSDate date]]];
    [dic setObject:[HLWJMD5 MD5ForLower32Bate:plainText] forKey:@"token"];
    [dic setObject:self.pinglunItem.object_type forKey:@"object_type"];
    [dic setObject:self.pinglunItem.object_id forKey:@"object_id"];
    [dic setObject:self.pinglunItem.id forKey:@"pid"];
    [dic setObject:text forKey:@"content"];
    self.sendApi.requestParameter = dic;
    [self.sendApi startWithSuccess:^(YBNetworkResponse * _Nonnull response) {
        complete(YES, 0);
    } failure:^(YBNetworkResponse * _Nonnull response) {
        complete(NO, 0);
    }];
}
- (FBSPostRequest *)sendApi {
    if (!_sendApi) {
        _sendApi = [[FBSPostRequest alloc] init];
        _sendApi.requestURI = @"comment/add";
    }
    return _sendApi;
}
- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}
#pragma mark - private method

- (void)initNavBar {
    self.navigationBackButtonHidden = NO;

}
#pragma mark notify
- (void)changeScrollStatus//改变主视图的状态
{
    self.canScroll = YES;
    if (self.pageController.selectIndex == 0) {
      FBSPingLunDetialViewController *vc  = (FBSPingLunDetialViewController *)self.pageController.currentViewController;
      vc.canScroll = NO;
    }else{
      FBSPinglunLikeViewController *vc  = (FBSPinglunLikeViewController *)self.pageController.currentViewController;
        vc.canScroll = NO;
    }
}
-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    NSLog(@"scrollView=====滚动==%.2F",scrollView.contentOffset.y);
    /*
     当 底层滚动式图滚动到指定位置时，
     停止滚动，开始滚动子视图
     */
    CGFloat bottomCellOffset = 100;
    //CGFloat bottomCellOffset = [_tableView rectForSection:1].origin.y - 64;
    if (scrollView.contentOffset.y >= bottomCellOffset) {
        scrollView.contentOffset = CGPointMake(0, bottomCellOffset);
        if (self.canScroll) {
            self.canScroll = NO;
            self.pageController.scrollEnable = YES;
            if (self.pageController.selectIndex == 0) {
                FBSPingLunDetialViewController *vc  = (FBSPingLunDetialViewController *)self.pageController.currentViewController;
                vc.canScroll = YES;
            }else{
                FBSPinglunLikeViewController *vc  = (FBSPinglunLikeViewController *)self.pageController.currentViewController;
                vc.canScroll = YES;
            }
        }
    }else{
        // 如果子tab还能滑动说明未滑动最顶部
        if (self.pageController.selectIndex == 0) {
            
            FBSPingLunDetialViewController *vc  = (FBSPingLunDetialViewController *)self.pageController.currentViewController;
            if (vc.canScroll) {
                scrollView.contentOffset = CGPointMake(0, bottomCellOffset);
            }else{
                if (!self.canScroll) {
                    self.canScroll = YES;
                }
                vc.canScroll = NO;
                self.pageController.scrollEnable = NO;
            }
        }else{
            FBSPinglunLikeViewController *vc  = (FBSPinglunLikeViewController *)self.pageController.currentViewController;
            if (vc.canScroll) {
                scrollView.contentOffset = CGPointMake(0, bottomCellOffset);
            }else{
                if (!self.canScroll) {
                    self.canScroll = YES;
                }
                vc.canScroll = NO;
                self.pageController.scrollEnable = NO;
            }
        }
    }
    self.scroller.showsVerticalScrollIndicator = _canScroll?YES:NO;
}
#pragma mark - WMPageControllerDataSource

- (NSInteger)numbersOfChildControllersInPageController:(WMPageController *)pageController {
    return self.tabTitles.count;
}

- (NSString *)pageController:(WMPageController *)pageController titleAtIndex:(NSInteger)index {
    
    return [self.tabTitles objectAtIndex:index];
}
- (void)pageController:(WMPageController *)pageController didEnterViewController:(__kindof UIViewController *)viewController withInfo:(NSDictionary *)info{
    
    self.tabVC = (FBSPingLunDetialViewController *)viewController;
    if (self.isFirst) {
        self.scroller.contentOffset = CGPointMake(0, 100);
        self.canScroll = NO;
        if (self.pageController.selectIndex == 0) {
            FBSPingLunDetialViewController *vc  = (FBSPingLunDetialViewController *)self.pageController.currentViewController;
            vc.canScroll = YES;
        }else{
            FBSPinglunLikeViewController *vc  = (FBSPinglunLikeViewController *)self.pageController.currentViewController;
            vc.canScroll = YES;
        }
    }else{
        self.canScroll = YES;
        if (self.pageController.selectIndex == 0) {
            FBSPingLunDetialViewController *vc  = (FBSPingLunDetialViewController *)self.pageController.currentViewController;
            vc.canScroll = NO;
        }else{
            FBSPinglunLikeViewController *vc  = (FBSPinglunLikeViewController *)self.pageController.currentViewController;
            vc.canScroll = NO;
        }
    }
    self.isFirst = YES;
}

- (UIViewController *)pageController:(WMPageController *)pageController viewControllerAtIndex:(NSInteger)index {
    if (index == 0) {
        return [[FBSPingLunDetialViewController alloc] initWith:self.pinglunItem];
    }else{
        return [[FBSPinglunLikeViewController alloc] initWith:self.pinglunItem];
    }
}

- (CGRect)pageController:(WMPageController *)pageController preferredFrameForMenuView:(WMMenuView *)menuView {
    CGFloat leftMargin = 0;
    CGFloat rightMargin = 0;
    CGFloat originY = 0;
    return CGRectMake(leftMargin, originY, pageController.view.frame.size.width - leftMargin - rightMargin, 44);
}

- (CGRect)pageController:(WMPageController *)pageController preferredFrameForContentView:(WMScrollView *)contentView {
    CGFloat originY = CGRectGetMaxY([self pageController:pageController preferredFrameForMenuView:self.pageController.menuView]);
    return CGRectMake(0, originY + 1, pageController.view.frame.size.width, pageController.view.frame.size.height - originY - 1);
}


#pragma mark - property

- (WMPageController *)pageController {
    if (!_pageController) {
        _pageController = [[WMPageController alloc] init];
        _pageController.dataSource = self;
        _pageController.delegate = self;
        _pageController.selectIndex = 0;
        _pageController.menuViewStyle = WMMenuViewStyleLine;
        _pageController.progressViewIsNaughty = YES;
        _pageController.progressWidth = 25;
        _pageController.automaticallyCalculatesItemWidths = YES;
        _pageController.titleColorNormal = Hexcolor(0x4A4A4A);
        _pageController.titleColorSelected = MainThemeColor;
        _pageController.titleSizeNormal = WScale(17);
        _pageController.titleSizeSelected = WScale(19);
        _pageController.view.backgroundColor = RGB(242,242,242);
        _pageController.menuView.backgroundColor = [UIColor whiteColor];
        _pageController.itemMargin = WScale(10);
        [self addChildViewController:_pageController];
        [self.scroller addSubview:_pageController.view];
    }
    return _pageController;
}
- (NSArray *)tabTitles {
    if (!_tabTitles) {
        _tabTitles = @[
                       @"评论",
                       @"点赞"
                       ];
    }
    return _tabTitles;
}
- (FBSPinglunReplayFooterView *)footer
{
    if (!_footer) {
        _footer = [[FBSPinglunReplayFooterView alloc] init];
        _footer.delegate = self;
    }
    return _footer;
}
- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (FBSPostRequest *)isLikeApi {
    if (!_isLikeApi) {
        _isLikeApi = [[FBSPostRequest alloc] init];
        _isLikeApi.requestURI = @"comment/like";
    }
    return _isLikeApi;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
