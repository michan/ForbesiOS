//
//  FBSAttentAndPinglunViewController.h
//  Forbes
//
//  Created by 赵志辉 on 2019/11/18.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import "FBSViewController.h"
#import "FBSPinglunItem.h"

NS_ASSUME_NONNULL_BEGIN

@interface FBSAttentAndPinglunViewController : FBSViewController
@property (nonatomic,assign) BOOL canScroll;
@property (nonatomic,strong) FBSPinglunItem *pinglunItem;
@end

NS_ASSUME_NONNULL_END
