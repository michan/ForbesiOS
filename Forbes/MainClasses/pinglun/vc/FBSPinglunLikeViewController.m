//
//  FBSPinglunLikeViewController.m
//  Forbes
//
//  Created by 赵志辉 on 2019/11/20.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import "FBSPinglunLikeViewController.h"
#import "FBSPinglunLikeViewModel.h"
#import "FBSPinglunLikeCell.h"
#import "FBSPinglunLikeItem.h"
#import "FBSTableViewController+Refresh.h"

@interface FBSPinglunLikeViewController ()<UIScrollViewDelegate>
@property (nonatomic, strong) FBSPinglunLikeViewModel *viewModel;
@property (nonatomic,assign) CGFloat lastOffset;

@end

@implementation FBSPinglunLikeViewController
@synthesize viewModel = _viewModel;
- (instancetype)initWith:(FBSPinglunItem *)pinglunItem{
    if (self = [super init]) {
        self.viewModel.pinglunItem = pinglunItem;
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationBarHidden = YES;
    
    self.registerDictionary = @{
                                [FBSPinglunLikeItem reuseIdentifier] : [FBSPinglunLikeCell class],
                                };
    [self createload];
    WEAKSELF
    self.page = @(1);
    self.pageCount = @(10);
    [self.viewModel rqContent:^(BOOL success, NSInteger count)  {
        if (success) {
            [weakSelf.tableView reloadData];
        }
    }];
    
}
-(void)setCanScroll:(BOOL)canScroll{
    _canScroll = canScroll;
    self.lastOffset = self.tableView.contentOffset.y;
}
- (void)createload {
    WEAKSELF
    [self backNormalFooterRefreshingActionBlock:^(FooterConfigBlock  _Nonnull footerConfig) {
        [weakSelf.viewModel rqContent:^(BOOL success, NSInteger count)  {
            if (success) {
                [weakSelf.tableView reloadData];
            }
            if(footerConfig){
                footerConfig(count);
            }
        }];
    }];
}
#pragma mark - delegate
-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    NSLog(@"tabelview >>>>>>> 滚动 == %.2F",scrollView.contentOffset.y);
    
    if (!self.canScroll) {
        scrollView.contentOffset = CGPointZero;
    }
    if (scrollView.contentOffset.y <= 0 ) {
        self.canScroll = NO;
        scrollView.contentOffset = CGPointZero;
        [[NSNotificationCenter defaultCenter] postNotificationName:@"leaveTop" object:nil];//到顶通知父视图改变状态
    }
    scrollView.showsVerticalScrollIndicator = self.canScroll?YES:NO;
}
#pragma mark - getters and setters
-(FBSPinglunLikeViewModel *)viewModel{
    if (!_viewModel) {
        _viewModel = [FBSPinglunLikeViewModel new];
    }
    return _viewModel;
}
#pragma mark - delegate
- (void)fbsCell:(FBSCell *)cell didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    //    if ([cell isKindOfClass:[FBSMainCell class]]) {
    //
    //    }
}
- (void)fbsCell:(FBSCell *)cell didClickButtonAtIndex:(NSInteger)index {
    //    if ([cell isKindOfClass:[FBSBannerCell class]]) {
    //
    //    }
}

@end
