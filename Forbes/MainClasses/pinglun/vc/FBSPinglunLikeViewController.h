//
//  FBSPinglunLikeViewController.h
//  Forbes
//
//  Created by 赵志辉 on 2019/11/20.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import "FBSTableViewController.h"
#import "FBSPinglunItem.h"

NS_ASSUME_NONNULL_BEGIN

@interface FBSPinglunLikeViewController : FBSTableViewController
@property (nonatomic,assign) BOOL canScroll;
- (instancetype)initWith:(FBSPinglunItem *)pinglunItem;
@end

NS_ASSUME_NONNULL_END
