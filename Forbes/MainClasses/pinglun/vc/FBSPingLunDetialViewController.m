//
//  FBSPingLunDetialViewController.m
//  Forbes
//
//  Created by 赵志辉 on 2019/11/17.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import "FBSPingLunDetialViewController.h"
#import "FBSPinglunDetialViewModel.h"
#import "FBSPinglunItem.h"
#import "FBSPinglunTableViewCell.h"
#import "FBSTableViewController+Refresh.h"
#import "FBSPinglunViewModel.h"
#import "XHInputView.h"

@interface FBSPingLunDetialViewController ()<UIScrollViewDelegate,XHInputViewDelagete>

@property (nonatomic, strong) FBSPinglunDetialViewModel *viewModel;
@property (nonatomic,assign) CGFloat lastOffset;


@end
@implementation FBSPingLunDetialViewController

@synthesize viewModel = _viewModel;
- (instancetype)initWith:(FBSPinglunItem *)pinglunItem{
    if (self = [super init]) {
        self.viewModel.pinglunItem = pinglunItem;
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationBarHidden = YES;
    self.registerDictionary = @{
                                [FBSReplayContentItem reuseIdentifier] : [FBSPinglunTableViewCell class],
                                
                                };
    [self createload];
}
- (void)click:(FBSReplayContentItem *)item{
    [XHInputView showWithStyle:0 configurationBlock:^(XHInputView *inputView) {
        /** 请在此block中设置inputView属性 */
        
        /** 代理 */
        inputView.delegate = self;
        
        /** 占位符文字 */
        inputView.placeholder = @"请输入评论文字...";
        /** 设置最大输入字数 */
        inputView.maxCount = 50;
        /** 输入框颜色 */
        inputView.textViewBackgroundColor = [UIColor groupTableViewBackgroundColor];
        
        /** 更多属性设置,详见XHInputView.h文件 */
        
    } sendBlock:^BOOL(NSString *text) {
        if(text.length){
            NSLog(@"输入的信息为:%@",text);
            [self showHUD];
            [self.viewModel rqSendIndex:item text:text block:^(BOOL success, NSInteger count) {
                [self hideHUD];
                if (success) {
                    [self makeToast:@"提交成功,请等待审核"];
                }else{
                    [self makeToast:@"提交失败，请重试"];
                }
            }];
            return YES;//return YES,收起键盘
        }else{
            NSLog(@"显示提示框-请输入要评论的的内容");
            return NO;//return NO,不收键盘
        }
    }];
}
-(void)setCanScroll:(BOOL)canScroll{
    _canScroll = canScroll;
    self.lastOffset = self.tableView.contentOffset.y;
}
- (void)createload {
    self.page = @(1);
    self.pageCount = @(50);
    WEAKSELF
    [weakSelf.viewModel rqContent:^(BOOL success, NSInteger count) {
        if (success) {
            [weakSelf.tableView reloadData];
        }
    }];
//    [self normalHeaderRefreshingActionBlock:^(FooterConfigBlock  _Nonnull footerConfig) {
//        [weakSelf.viewModel rqContent:^(BOOL success, NSInteger count) {
//            if (success) {
//                [weakSelf.tableView reloadData];
//            }
//            if(footerConfig){
//                footerConfig(count);
//            }
//        }];
//    }];
//
//
//    [self backNormalFooterRefreshingActionBlock:^(FooterConfigBlock  _Nonnull footerConfig) {
//        [weakSelf.viewModel rqContent:^(BOOL success, NSInteger count)  {
//            if (success) {
//                [weakSelf.tableView reloadData];
//            }
//            if(footerConfig){
//                footerConfig(count);
//            }
//        }];
//    }];
}

#pragma mark - getters and setters
-(FBSPinglunDetialViewModel *)viewModel{
    if (!_viewModel) {
        _viewModel = [FBSPinglunDetialViewModel new];
    }
    return _viewModel;
}
#pragma mark - delegate
-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    NSLog(@"tabelview >>>>>>> 滚动 == %.2F",scrollView.contentOffset.y);
    
    if (!self.canScroll) {
        scrollView.contentOffset = CGPointZero;
    }
    if (scrollView.contentOffset.y <= 0 ) {
        self.canScroll = NO;
        scrollView.contentOffset = CGPointZero;
        [[NSNotificationCenter defaultCenter] postNotificationName:@"leaveTop" object:nil];//到顶通知父视图改变状态
    }
    scrollView.showsVerticalScrollIndicator = self.canScroll?YES:NO;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    //    tableView.rowHeight = UITableViewAutomaticDimension;
    //    tableView.estimatedRowHeight = 100;
    //    return tableView.rowHeight;
    FBSItem *item  = [self.viewModel itemAtRow:indexPath.row inSection:indexPath.section];
    if ([item isKindOfClass:[FBSReplayContentItem class]]) {
        FBSReplayContentItem *pinglunItem = (FBSReplayContentItem *)item;
        [pinglunItem updateTab];
        return pinglunItem.allHeight;
    }
    return 200;
}
- (void)fbsCell:(FBSCell *)cell didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    //    if ([cell isKindOfClass:[FBSMainCell class]]) {
    //
    //    }
}
- (void)fbsCell:(FBSCell *)cell didClickButtonAtIndex:(NSInteger)index {
    if ([cell isKindOfClass:[FBSPinglunTableViewCell class]]) {
        FBSReplayContentItem *replayItem = (FBSReplayContentItem *)cell.item;
        FBSPinglunTableViewCell *pinglunCell = (FBSPinglunTableViewCell *)cell;
        if ([self panduanIsLogin]) {
            return;
        }
        if (index == 3) {
            [self click:replayItem];
        }else if(index == 0){
            WEAKSELF
            [self showHUD];
            [self.viewModel rqLikeIndex:replayItem block:^(BOOL success, NSInteger count) {
                [pinglunCell changeLike];
                [weakSelf hideHUD];
            }];
        }
    }
}
- (BOOL)panduanIsLogin{
    if(![FBSUserData sharedData].isLogin){
        FBSLoginViewController *loginVC = [[FBSLoginViewController alloc] init];
        [self.navigationController pushViewController:loginVC animated:YES];
        return NO;
    }
    return YES;
}
@end
