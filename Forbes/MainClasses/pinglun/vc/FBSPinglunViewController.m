//
//  FBSPinglunViewController.m
//  Forbes
//
//  Created by 赵志辉 on 2019/11/13.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import "FBSPinglunViewController.h"
#import "FBSPinglunViewModel.h"
#import "FBSPinglunItem.h"
#import "FBSPinglunTableViewCell.h"
#import "FBSTableViewController+Refresh.h"
#import "XHInputView.h"
#import "FBSAttentAndPinglunViewController.h"


@interface FBSPinglunViewController ()<XHInputViewDelagete>

@property (nonatomic, strong)FBSPinglunViewModel *viewModel;

@end

@implementation FBSPinglunViewController

@synthesize viewModel = _viewModel;

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationBarHidden = NO;
    self.registerDictionary = @{
                                [FBSPinglunItem reuseIdentifier] : [FBSPinglunTableViewCell class],
                                    };
    [self createload];
    
    [self showXHInputViewWithStyle:0];
    

}
- (void)createload {
    self.page = @(1);
    self.pageCount = @(50);
    WEAKSELF
    [self normalHeaderRefreshingActionBlock:^(FooterConfigBlock  _Nonnull footerConfig) {
        [weakSelf.viewModel rqContent:^(BOOL success, NSInteger count) {
            if (success) {
                [weakSelf.tableView reloadData];
            }
            if(footerConfig){
                footerConfig(count);
            }
        }];
    }];
    
    
    [self backNormalFooterRefreshingActionBlock:^(FooterConfigBlock  _Nonnull footerConfig) {
        [weakSelf.viewModel rqContent:^(BOOL success, NSInteger count)  {
            if (success) {
                [weakSelf.tableView reloadData];
            }
            if(footerConfig){
                footerConfig(count);
            }
        }];
    }];
}


///广告初始化

#pragma mark - property

- (FBSPinglunViewModel *)viewModel {
    if (!_viewModel) {
        _viewModel  = [[FBSPinglunViewModel alloc] init];
    }
    return _viewModel;
}
#pragma mark - delegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
//    tableView.rowHeight = UITableViewAutomaticDimension;
//    tableView.estimatedRowHeight = 100;
//    return tableView.rowHeight;
    FBSItem *item  = [self.viewModel itemAtRow:indexPath.row inSection:indexPath.section];
    if ([item isKindOfClass:[FBSPinglunItem class]]) {
        FBSPinglunItem *pinglunItem = (FBSPinglunItem *)item;
        [pinglunItem updateTab];
        return pinglunItem.allHeight;
    }
    return 200;
}
- (void)fbsCell:(FBSCell *)cell didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([cell isKindOfClass:[FBSPinglunTableViewCell class]]) {

    }
    FBSAttentAndPinglunViewController *vc = [[FBSAttentAndPinglunViewController alloc] init];
    vc.pinglunItem = (FBSPinglunItem *)cell.item;
    [self.navigationController pushViewController:vc animated:YES];
    //[self showXHInputViewWithStyle:InputViewStyleDefault];
}


- (void)fbsCell:(FBSCell *)cell didClickButtonAtIndex:(NSInteger)index {
    if ([cell isKindOfClass:[FBSPinglunTableViewCell class]]) {
        if (index == 0) {
            WEAKSELF
            [self.viewModel rqLike:^(BOOL success, NSInteger count) {
                if (success) {
                    FBSPinglunTableViewCell *cellPinglun = (FBSPinglunTableViewCell *)cell;
                    [cellPinglun changeLike];
                    [weakSelf.tableView reloadData];
                }
            }];
        }else{
            
        }
    }
}
-(void)showXHInputViewWithStyle:(InputViewStyle)style{
    
    [XHInputView showWithStyle:style configurationBlock:^(XHInputView *inputView) {
        /** 请在此block中设置inputView属性 */
        
        /** 代理 */
        inputView.delegate = self;
        
        /** 占位符文字 */
        inputView.placeholder = @"请输入评论文字...";
        /** 设置最大输入字数 */
        inputView.maxCount = 50;
        /** 输入框颜色 */
        inputView.textViewBackgroundColor = [UIColor groupTableViewBackgroundColor];
        
        /** 更多属性设置,详见XHInputView.h文件 */
        
    } sendBlock:^BOOL(NSString *text) {
        if(text.length){
            NSLog(@"输入的信息为:%@",text);
            [self showHUD];
            [self.viewModel rqSend:^(BOOL success, NSInteger count) {
                [self hideHUD];
            }];
            return YES;//return YES,收起键盘
        }else{
            NSLog(@"显示提示框-请输入要评论的的内容");
            return NO;//return NO,不收键盘
        }
    }];
    
}

#pragma mark - XHInputViewDelagete
/** XHInputView 将要显示 */
-(void)xhInputViewWillShow:(XHInputView *)inputView{
    
    /** 如果你工程中有配置IQKeyboardManager,并对XHInputView造成影响,请在XHInputView将要显示时将其关闭 */
    
    //[IQKeyboardManager sharedManager].enableAutoToolbar = NO;
    //[IQKeyboardManager sharedManager].enable = NO;
    
}

/** XHInputView 将要影藏 */
-(void)xhInputViewWillHide:(XHInputView *)inputView{
    
    /** 如果你工程中有配置IQKeyboardManager,并对XHInputView造成影响,请在XHInputView将要影藏时将其打开 */
    
    //[IQKeyboardManager sharedManager].enableAutoToolbar = YES;
    //[IQKeyboardManager sharedManager].enable = YES;
}


@end
