
//
//  FBSLoginViewController.m
//  Forbes
//
//  Created by 周灿华 on 2019/8/3.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSLoginViewController.h"
#import "FBSSignViewController.h"
#import "FBSLoginViewModel.h"
#import "FBSLoginSegment.h"
#import "FBSLoginView.h"
#import "FBSLoginPlatformView.h"
#import "FBSLoginPopView.h"
#import "FBSForgetViewController.h"
#import <AuthenticationServices/AuthenticationServices.h>
#import "LFSignInApple.h"

@interface FBSLoginViewController ()<FBSLoginPlatformViewDelegate>
@property (nonatomic, strong) FBSLoginViewModel *viewModel;
@property (nonatomic, strong) FBSLoginSegment *segment;
@property (nonatomic, strong) UIScrollView *bgScrollView;
@property (nonatomic, strong) FBSLoginView *fastLoginView;
@property (nonatomic, strong) FBSLoginView *pwdLoginView;
@property (nonatomic, strong) LFSignInApple *signInApple;

@property (nonatomic, strong) FBSLoginPlatformView *platformView;
@end

@implementation FBSLoginViewController
@synthesize viewModel = _viewModel;

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"登陆";
    
    [self UIInit];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    //一个bug
    [self.bgScrollView setContentOffset:CGPointMake(kScreenWidth * self.segment.selecIndex, 0) animated:NO];

}

- (void)UIInit {
    self.view.backgroundColor = COLOR_CommomBG;
    self.navigationBar.contentView.backgroundColor = MainThemeColor;
    self.navigationBar.backImage = kImageWithName(@"navback");
    self.navigationBar.titleLabel.textColor = [UIColor whiteColor];
    self.navigationBar.bottomLine.hidden = NO;
    
    [self.view addSubview:self.segment];
    [self.view addSubview:self.bgScrollView];
    [self.view addSubview:self.platformView];
    [self.bgScrollView addSubview:self.fastLoginView];
    [self.bgScrollView addSubview:self.pwdLoginView];
    
    [self.segment mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(kNavTopMargin + 10);
        make.left.mas_equalTo(0);
        make.width.mas_equalTo(kScreenWidth);
        make.height.mas_equalTo(46);
    }];
    
    [self.bgScrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.top.mas_equalTo(self.segment.mas_bottom).offset(WScale(15));
        make.bottom.mas_equalTo(self.platformView.mas_top);
        make.width.mas_equalTo(kScreenWidth);
    }];
    
    [self.platformView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.mas_equalTo(0);
        make.height.mas_equalTo(WScale(130) + kDiffTabBarH);
    }];

    [self.fastLoginView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.mas_equalTo(0);
        make.width.mas_equalTo(kScreenWidth);
        make.height.mas_equalTo(300);
    }];
    
    [self.pwdLoginView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(0);
        make.left.width.mas_equalTo(kScreenWidth);
        make.height.mas_equalTo(300);
    }];
    
    
    // 使用系统提供的按钮，要注意不支持系统版本的处理
    if (@available(iOS 13.0, *)) {
        // Sign In With Apple Button
        ASAuthorizationAppleIDButton *appleIDBtn = [ASAuthorizationAppleIDButton buttonWithType:ASAuthorizationAppleIDButtonTypeDefault style:ASAuthorizationAppleIDButtonStyleBlack];
        appleIDBtn.frame = CGRectMake(50, Main_Screen_Height - WScale(130) - kDiffTabBarH - 60, self.view.bounds.size.width - 100, 50);
        //    appleBtn.cornerRadius = 22.f;
        [appleIDBtn addTarget:self action:@selector(didAppleIDBtnClicked) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:appleIDBtn];
    }

}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}


- (void)showAlert:(NSString *)tip {
    FBSLoginPopView *tipView = [[FBSLoginPopView alloc] initWithConfirmTitle:tip detail:nil];
    [tipView show];
}


#pragma mark - 登录操作

- (BOOL)phoneCodeCheck:(UITextField *)tf1 {
    [self.view endEditing:YES];
    
    if (!tf1.text.length) {
        [self makeToast:tf1.placeholder];
        return NO;
    }
    
    if (![NSString validateContactNumber:tf1.text]) {
        [self makeToast:@"请输入正确的手机号码"];
        return NO;
    }
    
    return YES;
    
}



- (void)sendCode:(NSString *)text {
    if (!text.length) {
        return ;
    }
    
    [self showHUD:NO];
    
    WEAKSELF
    [self.viewModel rqPhoneCode:text complete:^(BOOL success, NSString *msg) {
        
        [weakSelf hideHUD];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            
            if (success) {
                [weakSelf makeToast:@"验证码已发送"];
                
            } else {
                [weakSelf showAlert:msg];
                
            }
            
        });
        
    }];

}


- (void)phoneLoginCheck:(UITextField *)tf1 tf2:(UITextField *)tf2 {
    [self.view endEditing:YES];
    
    if (!tf1.text.length) {
        [self makeToast:tf1.placeholder];
        return ;
    }
    
    if (![NSString validateContactNumber:tf1.text]) {
        [self makeToast:@"请输入正确的手机号码"];
        return ;
    }

    
    if (!tf2.text.length) {
        [self makeToast:tf2.placeholder];
        return ;
    }
    
    [self showHUD:NO];
    
    WEAKSELF
    [self.viewModel rqPhoneLogin:tf1.text code:tf2.text complete:^(BOOL success, NSString *msg) {
        [weakSelf hideHUD];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            
            if (success) {
                [weakSelf makeToast:@"登录成功"];
                
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    if (weakSelf.islogin) {
                        weakSelf.islogin(YES);
                    }
                    [weakSelf.navigationController popViewControllerAnimated:YES];
                });
                
            } else {
                [weakSelf showAlert:msg];
                
            }
            
        });
    }];
}

- (void)pwdLoginCheck:(UITextField *)tf1 tf2:(UITextField *)tf2 {
    [self.view endEditing:YES];
    
    if (!tf1.text.length) {
        [self makeToast:tf1.placeholder];
        return ;
    }
    
    if (!tf2.text.length) {
        [self makeToast:tf2.placeholder];
        return ;
    }
    
    
    BOOL isEmail = [tf1.text containsString:@"@"];
    
    [self showHUD];
    
    WEAKSELF
    [self.viewModel rqPwdLogin:(isEmail ? nil : tf1.text) email:(isEmail ? tf1.text : nil) password:tf2.text complete:^(BOOL success, NSString *msg) {
        
        [weakSelf hideHUD];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            
            if (success) {
                [weakSelf makeToast:@"登录成功"];
                
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    if (weakSelf.islogin) {
                        weakSelf.islogin(YES);
                    }
                    [weakSelf.navigationController popViewControllerAnimated:YES];
                });
                
            } else {
                [weakSelf showAlert:msg];
                
            }
            
        });
        
    }];
}

#pragma mark - property

- (FBSLoginViewModel *)viewModel {
    if (!_viewModel) {
        _viewModel  = [[FBSLoginViewModel alloc] init];
    }
    return _viewModel;
}


- (FBSLoginSegment *)segment {
    if (!_segment) {
        _segment  = [[FBSLoginSegment alloc] initWithFrame:CGRectZero];
        _segment.backgroundColor = [UIColor whiteColor];
        WEAKSELF
        _segment.selecBlock = ^(NSInteger selecIndex) {
            [weakSelf.view endEditing:YES];
            [weakSelf.bgScrollView setContentOffset:CGPointMake(kScreenWidth * selecIndex, 0) animated:YES];
        };
    }
    return _segment;
}


- (UIScrollView *)bgScrollView {
    if (!_bgScrollView) {
        _bgScrollView  = [[UIScrollView alloc] initWithFrame:CGRectZero];
        _bgScrollView.backgroundColor = [UIColor clearColor];
        if (@available(iOS 11.0, *)) {
            _bgScrollView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
            
        } else {
            self.automaticallyAdjustsScrollViewInsets = NO;
        }
    }
    return _bgScrollView;
}

- (FBSLoginView *)fastLoginView {
    if (!_fastLoginView) {
        _fastLoginView  = [[FBSLoginView alloc] init];
        _fastLoginView.isPwd = NO;
        
        WEAKSELF
        _fastLoginView.loginBlock = ^(UITextField *tf1, UITextField *tf2) {
            [weakSelf phoneLoginCheck:tf1 tf2:tf2];
        };
        
        _fastLoginView.signupBlock = ^{
            [weakSelf.view endEditing:YES];
            FBSSignViewController *signVC = [[FBSSignViewController alloc] init];
            [weakSelf.navigationController pushViewController:signVC animated:YES];
        };
        
        _fastLoginView.countDownBlock = ^BOOL(UITextField *tf1, UITextField *tf2) {
            BOOL result = [weakSelf phoneCodeCheck:tf1];
            if (result) {
                [weakSelf sendCode:tf1.text];
            }
            return result;
        };

    }
    return _fastLoginView;
}


- (FBSLoginView *)pwdLoginView {
    if (!_pwdLoginView) {
        _pwdLoginView  = [[FBSLoginView alloc] init];
        _pwdLoginView.isPwd = YES;
        
        WEAKSELF
        _pwdLoginView.loginBlock = ^(UITextField *tf1, UITextField *tf2) {
            [weakSelf pwdLoginCheck:tf1 tf2:tf2];
        };
        
        _pwdLoginView.signupBlock = ^{
            [weakSelf.view endEditing:YES];
            FBSSignViewController *signVC = [[FBSSignViewController alloc] init];
            [weakSelf.navigationController pushViewController:signVC animated:YES];
        };
        _pwdLoginView.forgetBlock = ^{
            [weakSelf.view endEditing:YES];
            FBSForgetViewController *vc= [[FBSForgetViewController alloc] init];
            [self.navigationController pushViewController:vc animated:YES];
        };

    }
    return _pwdLoginView;
}



- (FBSLoginPlatformView *)platformView {
    if (!_platformView) {
        _platformView  = [[FBSLoginPlatformView alloc] init];
        _platformView.delegate = self;
    }
    return _platformView;
}


- (void)clickShareLogin:(FBSLoginPlatformView *)PlatformView didClickWithCompanyModel:(ShareLoginType)type
{
    if (type == ShareLoginTypeqq) {
        [self login:SSDKPlatformTypeQQ];
    }else if (type == ShareLoginTypewx) {
        [self login:SSDKPlatformTypeWechat];
    }else if (type == ShareLoginTypewb) {
        [self login:SSDKPlatformTypeSinaWeibo];
    }
    else if (type == ShareLoginTypeapple) {
        [self didAppleIDBtnClicked];

    }
}
- (void)login:(SSDKPlatformType)type{
    
//    [ShareSDK authorize:type
//                         settings:nil
//                onStateChanged:^(SSDKResponseState state, SSDKUser *user, NSError *error) {
//            if (state == SSDKResponseStateSuccess)
//               {
//                    NSLog(@"%@",user.rawData);
//                    NSLog(@"uid===%@",user.uid);
//                    NSLog(@"%@",user.credential);
//               }
//            else if (state == SSDKResponseStateCancel)
//               {
//                    NSLog(@"取消");
//               }
//            else if (state == SSDKResponseStateFail)
//               {
//                    NSLog(@"%@",error);
//
//               }
//    }];
//
//    return;
    
    WEAKSELF;
    [ShareSDK authorize:type
                         settings:nil
                onStateChanged:^(SSDKResponseState state, SSDKUser *user, NSError *error) {
         
         if (state == SSDKResponseStateSuccess)
         {
             [weakSelf showHUD];
             NSLog(@"uid=%@",user.uid);
             NSLog(@"%@",user.credential);
             NSLog(@"token=%@",user.credential.token);
             NSLog(@"nickname=%@",user.nickname);
             
             if (type == SSDKPlatformTypeQQ) {
                 [weakSelf.viewModel rqQQLoginApp_type:@"IOS" access_token:user.credential.token openid:user.uid complete:^(BOOL success, NSString *msg) {
                     [weakSelf loginSuccessAndError:success msg:msg];
                 }];
             }else if (type == SSDKPlatformTypeWechat){
                 [weakSelf.viewModel rqWXLoginCode:user.credential.token Openid:user.uid complete:^(BOOL success, NSString *msg) {
                     [weakSelf loginSuccessAndError:success msg:msg];
                 }];
             }else if (type == SSDKPlatformTypeSinaWeibo){
                 [weakSelf.viewModel rqWBLoginaccess_token:user.credential.token complete:^(BOOL success, NSString *msg) {
                     [weakSelf loginSuccessAndError:success msg:msg];
                 }];
             }
         }
         else
         {
//             [self showAlert:[NSString stringWithFormat:@"%ld",error.code]];
//             NSLog(@"%@",error);
         }
     }];
}
- (void)loginSuccessAndError:(BOOL)success msg:(NSString *)msg{
    [self hideHUD];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        if (success) {
            [self makeToast:@"登录成功"];
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                if (self.islogin) {
                    self.islogin(YES);
                }
                [self.navigationController popViewControllerAnimated:YES];
            });
            
        } else {
            [self showAlert:msg];
            
        }
        
    });
}
-(void)didAppleIDBtnClicked{
    [self.signInApple handleAuthorizationAppleIDButtonPress];
    WEAKSELF
    self.signInApple.loginBlock = ^(NSString * _Nonnull authorizationCode) {
        [weakSelf applelogin:authorizationCode];
    };

}
-(void)applelogin:(NSString*)authorizationCode{
    NSDictionary * parameters =@{
             
             @"authorizationCode":authorizationCode?:@"",
             
         };
         [self showHUD];
         WEAKSELF

         [MCNetworking ResponseNetworkPOST_API:@"login/apple" parameters:parameters  TheServer:0 cachePolicy:NO ISNeedLogin:YES RequestEnd:^(id  _Nonnull EndObject) {
             [self hideHUD];

         } MCHttpCacheData:^(id  _Nonnull CacheObject) {
             
         } success:^(id  _Nonnull responseObject) {
             NSLog(@"responseObject == %@",responseObject);
             


             FBSBaseModel *model = [FBSBaseModel mj_objectWithKeyValues:responseObject];
             
             if (model.success) {
                 //更新用户数据
                 [[FBSUserData sharedData] saveUserData:responseObject];
                 
                 //发布登录成功通知
                 [[NSNotificationCenter defaultCenter] postNotificationName:FBSLoginSuccessNotication object:nil];
             }
             
             [weakSelf loginSuccessAndError:model.success msg:model.msg];

         } failure:^(NSURLSessionDataTask * _Nonnull operation, NSError * _Nonnull error, id  _Nonnull responseObject, NSString * _Nonnull description) {
             [self makeToast:@"登录失败"];

         }];

     
     
}
// 如果存在iCloud Keychain 凭证或者AppleID 凭证提示用户
- (void)perfomExistingAccount{
    // 封装Sign In with Apple 登录工具类，使用这个类时要把类对象设置为全局变量，或者直接把这个工具类做成单例，如果使用局部变量，和IAP支付工具类一样，会导致苹果回调不会执行
//    self.signInApple = [[SignInApple alloc] init];
    [self.signInApple perfomExistingAccountSetupFlows];
}

- (LFSignInApple *)signInApple {
    if (!_signInApple) {
        _signInApple = [[LFSignInApple alloc]init];
    }
    return _signInApple;
}

@end
