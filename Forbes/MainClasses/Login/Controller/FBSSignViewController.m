//
//  FBSSignViewController.m
//  Forbes
//
//  Created by 周灿华 on 2019/9/8.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSSignViewController.h"
#import "FBSSignViewModel.h"
#import "FBSLoginPopView.h"

@interface FBSSignViewController ()<UITextFieldDelegate>
@property (nonatomic, strong) FBSSignViewModel *viewModel;
@property (nonatomic, strong) UIScrollView *bgScrollView;
@property (nonatomic, strong) UIView *contentView;
@property (nonatomic, strong) UIButton *commitBtn;
@property (nonatomic, strong) UIButton *signinBtn;

@property (nonatomic, strong) UITextField *phoneTF;
@property (nonatomic, strong) UITextField *nickNameTF;
@property (nonatomic, strong) UITextField *emailTF;
@property (nonatomic, strong) UITextField *pwdTF;
@property (nonatomic, strong) UITextField *codeTF;
@property (nonatomic, strong) UIButton *sendCodeBtn;
@end

@implementation FBSSignViewController
@synthesize viewModel = _viewModel;

#pragma mark - life cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self UIInit];
}


- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}


- (void)UIInit {
    self.title = @"注册";
    self.view.backgroundColor = COLOR_CommomBG;
    self.navigationBar.contentView.backgroundColor = MainThemeColor;
    self.navigationBar.backImage = kImageWithName(@"navback");
    self.navigationBar.titleLabel.textColor = [UIColor whiteColor];
    self.navigationBar.bottomLine.hidden = NO;
    
    [self.view addSubview:self.bgScrollView];
    [self.bgScrollView addSubview:self.contentView];
    
    [self.bgScrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(kNavTopMargin);
        make.left.right.mas_equalTo(self.view);
        make.height.mas_equalTo(self.view).offset(-kNavTopMargin);
    }];
    
    
    [self.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.mas_equalTo(0);
        make.width.mas_equalTo(self.bgScrollView);
        make.height.mas_equalTo(kScreenHeight);
//        make.bottom.mas_equalTo(10);
    }];

    
    
    [self signViewInit];
}


- (void)signViewInit  {
    UIView *line1 = [self createLine];
    UIView *line2 = [self createLine];
    UIView *line3 = [self createLine];
    UIView *line4 = [self createLine];
    UIView *line5 = [self createLine];
    UIView *line6 = [self createLine];
    
    [self.contentView addSubview:line1];
    [self.contentView addSubview:line2];
    [self.contentView addSubview:line3];
    [self.contentView addSubview:line4];
    [self.contentView addSubview:line5];
    [self.contentView addSubview:line6];
    
    [self.contentView addSubview:self.phoneTF];
    [self.contentView addSubview:self.nickNameTF];
    [self.contentView addSubview:self.emailTF];
    [self.contentView addSubview:self.pwdTF];
    [self.contentView addSubview:self.codeTF];
    [self.contentView addSubview:self.sendCodeBtn];
    [self.contentView addSubview:self.commitBtn];
    [self.contentView addSubview:self.signinBtn];
    
    [line1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.navigationBar.mas_bottom).offset(WScale(60));
        make.left.right.mas_equalTo(0);
        make.height.mas_equalTo(1);
    }];
    
    
    [self.phoneTF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(line1.mas_bottom);
        make.left.mas_equalTo(WScale(30));
        make.width.mas_equalTo(WScale(200));
        make.height.mas_equalTo(WScale(44));
    }];
    
    
    [line2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.phoneTF.mas_bottom);
        make.left.right.mas_equalTo(0);
        make.height.mas_equalTo(1);
    }];
    
    
    [self.nickNameTF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(line2.mas_bottom);
        make.left.mas_equalTo(WScale(30));
        make.width.mas_equalTo(WScale(200));
        make.height.mas_equalTo(WScale(44));
    }];
    
    
    [line3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.nickNameTF.mas_bottom);
        make.left.right.mas_equalTo(0);
        make.height.mas_equalTo(1);
    }];
    
    
    [self.emailTF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(line3.mas_bottom);
        make.left.mas_equalTo(WScale(30));
        make.width.mas_equalTo(WScale(200));
        make.height.mas_equalTo(WScale(44));
    }];

    
    [line4 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.emailTF.mas_bottom);
        make.left.right.mas_equalTo(0);
        make.height.mas_equalTo(1);
    }];
    
    [self.pwdTF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(line4.mas_bottom);
        make.left.mas_equalTo(WScale(30));
        make.width.mas_equalTo(WScale(200));
        make.height.mas_equalTo(WScale(44));
    }];
    
    [line5 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.pwdTF.mas_bottom);
        make.left.right.mas_equalTo(0);
        make.height.mas_equalTo(1);
    }];
    
    [self.codeTF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(line5.mas_bottom);
        make.left.mas_equalTo(WScale(30));
        make.width.mas_equalTo(WScale(200));
        make.height.mas_equalTo(WScale(44));
    }];
    
    [line6 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.codeTF.mas_bottom);
        make.left.right.mas_equalTo(0);
        make.height.mas_equalTo(1);
    }];



    [self.sendCodeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(WScale(70));
        make.height.mas_equalTo(WScale(25));
        make.centerY.mas_equalTo(self.codeTF);
        make.right.mas_equalTo(-WScale(34));
    }];
    
    
    [self.commitBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(line6.mas_bottom).offset(WScale(17));
        make.left.mas_equalTo(WScale(15));
        make.right.mas_equalTo(-WScale(15));
        make.height.mas_equalTo(WScale(40));
    }];
    
    
    [self.signinBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.commitBtn.mas_bottom);
        make.right.mas_equalTo(self.commitBtn);
        make.height.mas_equalTo(WScale(34));
    }];
    

    UIView *whiteView = [[UIView alloc] init];
    whiteView.backgroundColor = [UIColor whiteColor];
    [self.contentView insertSubview:whiteView atIndex:0];
    
    [whiteView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0);
        make.top.mas_equalTo(line1);
        make.bottom.mas_equalTo(line6);
    }];
}


#pragma mark - action

- (void)signinAction {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)commitAction {
    [self.view endEditing:YES];
    
    if (self.phoneTF.text.length == 0) {
        [self makeToast:@"请输入手机号码"];
        return ;
    }
    
    if (![NSString validateContactNumber:self.phoneTF.text]) {
        [self makeToast:@"请输入正确的手机号码"];
        return ;
    }
    
    
    if (self.nickNameTF.text.length == 0) {
        [self makeToast:@"请输入账户名称"];
        return ;
    }
    
    if (![NSString validateEmail:self.emailTF.text]) {
        [self makeToast:@"邮箱格式错误"];
        return ;
    }


    if (![NSString validatePassword:self.pwdTF.text]) {
        [self makeToast:@"密码格式错误"];
        return ;
    }
    
    
    if (self.codeTF.text.length == 0) {
        [self makeToast:@"请输入验证码"];
        return ;
    }

    
    [self showHUD];
    WEAKSELF
    [self.viewModel rqSignup:self.phoneTF.text
                    nickName:self.nickNameTF.text
                       email:self.emailTF.text
                    password:self.pwdTF.text
                        code:self.codeTF.text
                    complete:^(BOOL success, NSString *msg) {
                        
        [weakSelf hideHUD];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            
            if (success) {
                [weakSelf makeToast:@"登录成功"];
                
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    NSInteger count = weakSelf.navigationController.viewControllers.count;
                    
                    if (count > 2) {
                        NSArray *childArray = [weakSelf.navigationController.viewControllers subarrayWithRange:NSMakeRange(0, count - 2)];
                        [weakSelf.navigationController setViewControllers:childArray animated:YES];
                    } else {
                        [weakSelf.navigationController popViewControllerAnimated:YES];
                    }
                });
                
            } else {
                [weakSelf showAlert:msg];
                
            }
            
        });
    }];

}


- (void)showAlert:(NSString *)tip {
    FBSLoginPopView *tipView = [[FBSLoginPopView alloc] initWithConfirmTitle:tip detail:nil];
    [tipView show];
}


#pragma mark - 发送验证码

- (void)sendCodeCheck {
    [self.view endEditing:YES];
    
    if (self.phoneTF.text.length == 0) {
        [self makeToast:@"请输入手机号码"];
        return ;
    }
    
    if (![NSString validateContactNumber:self.phoneTF.text]) {
        [self makeToast:@"请输入正确的手机号码"];
        return ;
    }
    
    
    FBSLog(@"发送验证码。。");
    
    WEAKSELF
    [self showHUD];
    [self.viewModel rqSignupCode:self.phoneTF.text complete:^(BOOL success, NSString *error) {
        
        [weakSelf hideHUD];
        
        if (success) {
            [weakSelf makeToast:@"验证码已发送"];
            [weakSelf sentPhoneCodeTimeMethod];
        } else {
            [weakSelf makeToast:@"获取验证码失败"];
        }
    
    }];
  
}


//计时器发送验证码
- (void)sentPhoneCodeTimeMethod{
    //倒计时时间 - 60秒
    __block NSInteger timeOut = 59;
    //执行队列
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    //计时器 -》dispatch_source_set_timer自动生成
    dispatch_source_t timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, queue);
    dispatch_source_set_timer(timer, DISPATCH_TIME_NOW, 1.0 * NSEC_PER_SEC, 0 * NSEC_PER_SEC);
    dispatch_source_set_event_handler(timer, ^{
        if (timeOut <= 0) {
            dispatch_source_cancel(timer);
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.sendCodeBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
                [self.sendCodeBtn setUserInteractionEnabled:YES];
            });
        }else{
            //开始计时
            //剩余秒数 seconds
            NSInteger seconds = timeOut % 60;
            NSString *strTime = [NSString stringWithFormat:@"%.1ld",seconds];
            //主线程设置按钮样式
            dispatch_async(dispatch_get_main_queue(), ^{
                [UIView beginAnimations:nil context:nil];
                [UIView setAnimationDuration:1.0];
                [self.sendCodeBtn setTitle:[NSString stringWithFormat:@"%@S",strTime] forState:UIControlStateNormal];
                [UIView commitAnimations];
                //计时器件不允许点击
                [self.sendCodeBtn setUserInteractionEnabled:NO];
            });
            timeOut--;
        }
    });
    dispatch_resume(timer);
}


- (UIView *)createLine {
    UIView *line  = [[UIView alloc] init];
    line.backgroundColor = RGB(232, 232, 232);
    return line;
}

- (UITextField *)createTextField:(NSString *)placeholder {
    UITextField *tf = [[UITextField alloc] init];
    tf.font = kLabelFontSize14;
    tf.textColor = Hexcolor(0x333333);
//    [tf setValue:Hexcolor(0xCBCCCC) forKeyPath:@"_placeholderLabel.textColor"];
//    tf.placeholder = placeholder;
    
    tf.attributedPlaceholder = [[NSAttributedString alloc] initWithString:placeholder ?:@"" attributes:@{NSFontAttributeName:kLabelFontSize14,NSForegroundColorAttributeName:Hexcolor(0xCBCCCC)}];
    tf.returnKeyType = UIReturnKeyDone;
    tf.keyboardType = UIKeyboardTypeNumberPad;
    tf.clearButtonMode = UITextFieldViewModeWhileEditing;
    tf.delegate = self;
    return tf;
}


#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

#pragma mark - property

- (FBSSignViewModel *)viewModel {
    if (!_viewModel) {
        _viewModel  = [[FBSSignViewModel alloc] init];
    }
    return _viewModel;
}


- (UIScrollView *)bgScrollView {
    if (!_bgScrollView) {
        _bgScrollView  = [[UIScrollView alloc] init];
        _bgScrollView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
        _bgScrollView.alwaysBounceVertical = YES;
        if (@available(iOS 11.0, *)) {
            _bgScrollView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        }
    }
    return _bgScrollView;
}


- (UIView *)contentView {
    if (!_contentView) {
        _contentView  = [[UIView alloc] init];
        _contentView.backgroundColor = [UIColor clearColor];
    }
    return _contentView;
}


- (UIButton *)commitBtn {
    if (!_commitBtn) {
        _commitBtn  = [UIButton buttonWithType:UIButtonTypeCustom];
        _commitBtn.backgroundColor = MainThemeColor;
        _commitBtn.titleLabel.font = kLabelFontSize15;
        [_commitBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_commitBtn setTitle:@"确定" forState:UIControlStateNormal];
        [_commitBtn addTarget:self action:@selector(commitAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _commitBtn;
    
}


- (UITextField *)phoneTF {
    if (!_phoneTF) {
        _phoneTF = [self createTextField:@"输入手机号码"];
    }
    return _phoneTF;
}



- (UITextField *)nickNameTF {
    if (!_nickNameTF) {
        _nickNameTF = [self createTextField:@"输入账户名称"];
        _nickNameTF.keyboardType = UIKeyboardTypeDefault;
    }
    return _nickNameTF;
}


- (UITextField *)emailTF {
    if (!_emailTF) {
        _emailTF = [self createTextField:@"请输入邮箱"];
        _emailTF.keyboardType = UIKeyboardTypeDefault;
    }
    return _emailTF;
}


- (UITextField *)pwdTF {
    if (!_pwdTF) {
        _pwdTF = [self createTextField:@"设置密码(6-8位字母 ，数字）"];
        _pwdTF.keyboardType = UIKeyboardTypeDefault;
    }
    return _pwdTF;
}


- (UITextField *)codeTF {
    if (!_codeTF) {
        _codeTF = [self createTextField:@"输入验证码"];
    }
    return _codeTF;
}


- (UIButton *)sendCodeBtn {
    if (!_sendCodeBtn) {
        _sendCodeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_sendCodeBtn setBackgroundColor:[UIColor whiteColor]];
        _sendCodeBtn.titleLabel.font = kLabelFontSize12;
        _sendCodeBtn.titleLabel.adjustsFontSizeToFitWidth = YES;
        [_sendCodeBtn setTitleColor:Hexcolor(0xBB833E) forState:UIControlStateNormal];
        [_sendCodeBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
        [_sendCodeBtn.layer setBorderWidth:1.0];
        [_sendCodeBtn.layer setBorderColor:Hexcolor(0xBB833E).CGColor];
        
        [_sendCodeBtn addTarget:self action:@selector(sendCodeCheck) forControlEvents:UIControlEventTouchUpInside];
    }
    return _sendCodeBtn;
}

- (UIButton *)signinBtn {
    if (!_signinBtn) {
        _signinBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _signinBtn.titleLabel.font = kLabelFontSize12;
        [_signinBtn setTitleColor:MainThemeColor forState:UIControlStateNormal];
        [_signinBtn setTitle:@"已有账号，立即登录" forState:UIControlStateNormal];
        [_signinBtn addTarget:self action:@selector(signinAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _signinBtn;
}


@end
