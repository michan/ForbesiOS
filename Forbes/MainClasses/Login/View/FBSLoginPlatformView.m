//
//  FBSLoginPlatformView.m
//  Forbes
//
//  Created by 周灿华 on 2019/8/4.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSLoginPlatformView.h"

@interface FBSLoginPlatformView ()
@property (nonatomic, strong) UIView *topView;
@property (nonatomic, strong) UIView *leftLine;
@property (nonatomic, strong) UIView *rightLine;
@property (nonatomic, strong) UILabel *otherL;

@property (nonatomic, strong) UIView *protocolView;
@property (nonatomic, strong) UILabel *agreeL;
@property (nonatomic, strong) UIButton *protocolBtn;

@property (nonatomic, strong) UIView *platformsView;
@property (nonatomic, strong) NSArray *platforms;

@end

@implementation FBSLoginPlatformView


- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self addSubview:self.topView];
        [self addSubview:self.platformsView];
        [self addSubview:self.protocolView];
        
        [self.topView addSubview:self.leftLine];
        [self.topView addSubview:self.otherL];
        [self.topView addSubview:self.rightLine];
        
        [self.protocolView addSubview:self.agreeL];
        [self.protocolView addSubview:self.protocolBtn];
        
        
        [self.topView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.top.right.mas_equalTo(0);
            make.height.mas_equalTo(WScale(12));
        }];
        
        [self.platformsView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.topView.mas_bottom);
            make.left.right.mas_equalTo(0);
            make.height.mas_equalTo(WScale(76));
        }];

        
        [self.protocolView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.platformsView.mas_bottom);
            make.centerX.mas_equalTo(0);
            make.height.mas_equalTo(WScale(22));
        }];
        
        [self.otherL mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.centerX.mas_equalTo(0);
            make.height.mas_equalTo(WScale(12));
        }];
        
        
        [self.leftLine mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(WScale(35));
            make.height.mas_equalTo(1);
            make.right.mas_equalTo(self.otherL.mas_left).offset(-WScale(10));
            make.centerY.mas_equalTo(0);
        }];
        
        
        [self.rightLine mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.otherL.mas_right).offset(WScale(10));
            make.right.mas_equalTo(-WScale(35));
            make.height.mas_equalTo(1);
            make.centerY.mas_equalTo(0);
        }];


        [self.agreeL mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.centerY.mas_equalTo(0);
        }];
        
        [self.protocolBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.bottom.mas_equalTo(0);
            make.left.mas_equalTo(self.agreeL.mas_right).offset(5);
            make.right.mas_equalTo(0);
        }];
        
        [self createPlatforms];
    }
    return self;
}


- (void)tapAction:(UITapGestureRecognizer *)gesture {
    FBSLog(@"点击了平台 : %ld",gesture.view.tag);
    if (self.delegate && [self.delegate respondsToSelector:@selector(clickShareLogin:didClickWithCompanyModel:)]) {
        [self.delegate clickShareLogin:self didClickWithCompanyModel:gesture.view.tag];
    }
}


- (void)createPlatforms {
    NSInteger itemCount = self.platforms.count;
    CGFloat itemMargin = WScale(25);
    CGFloat wh = WScale(36);
    CGFloat marginToSuperView = (kScreenWidth - itemCount * (wh + itemMargin) - itemCount + itemMargin) / 2.0;
    UIImageView *lastView;
    for (NSInteger i =0; i < itemCount; i++) {
        UIImageView *imageView = [[UIImageView alloc] init];
        imageView.userInteractionEnabled = YES;
        imageView.image = kImageWithName([self.platforms objectAtIndex:i]);
        imageView.tag = i;
        
        UITapGestureRecognizer *tapGes = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAction:)];
        [imageView addGestureRecognizer:tapGes];
        [self.platformsView addSubview:imageView];
        
        [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
            if (i == 0) {
                make.left.mas_equalTo(marginToSuperView);
            } else {
                make.left.mas_equalTo(lastView.mas_right).offset(itemMargin);
            }
            make.width.height.mas_equalTo(wh);
            make.centerY.mas_equalTo(0);
        }];
        
        lastView = imageView;
    }
}


- (void)clickProtocol {
    FBSLog(@"点击了协议");
}

#pragma mark - property

- (UIView *)topView {
    if (!_topView) {
        _topView  = [[UIView alloc] init];
        _topView.backgroundColor = [UIColor clearColor];
    }
    return _topView;
}


- (UIView *)leftLine {
    if (!_leftLine) {
        _leftLine  = [[UIView alloc] init];
        _leftLine.backgroundColor = Hexcolor(0xD6D6D6);
    }
    return _leftLine;
}

- (UIView *)rightLine {
    if (!_rightLine) {
        _rightLine  = [[UIView alloc] init];
        _rightLine.backgroundColor = Hexcolor(0xD6D6D6);
    }
    return _rightLine;
}


- (UILabel *)otherL {
    if (!_otherL) {
        _otherL = [[UILabel alloc] init];
        _otherL.font = kLabelFontSize12;
        _otherL.textColor = Hexcolor(0xCCCCCC);
        _otherL.text = @"其他方式登录";
    }
    return _otherL;
}


- (UIView *)protocolView {
    if (!_protocolView) {
        _protocolView  = [[UIView alloc] init];
        _protocolView.backgroundColor = [UIColor clearColor];
    }
    return _protocolView;
}


- (UILabel *)agreeL {
    if (!_agreeL) {
        _agreeL = [[UILabel alloc] init];
        _agreeL.font = kLabelFontSize12;
        _agreeL.textColor = Hexcolor(0xCCCCCC);
        _agreeL.text = @"登录即代表您已阅读并同意";
    }
    return _agreeL;
}


- (UIButton *)protocolBtn {
    if (!_protocolBtn) {
        _protocolBtn  = [UIButton buttonWithType:UIButtonTypeCustom];
        _protocolBtn.titleLabel.font = kLabelFontSize12;
        [_protocolBtn setTitleColor:Hexcolor(0x61A0FC) forState:UIControlStateNormal];
        [_protocolBtn setTitle:@"福布斯用户注册协议" forState:UIControlStateNormal];
        [_protocolBtn addTarget:self action:@selector(clickProtocol) forControlEvents:UIControlEventTouchUpInside];
    }
    return _protocolBtn;
}



- (UIView *)platformsView {
    if (!_platformsView) {
        _platformsView  = [[UIView alloc] init];
        _platformsView.backgroundColor = [UIColor clearColor];
    }
    return _platformsView;
}


- (NSArray *)platforms {
    if (!_platforms) {
        _platforms  = @[
                        @"qqlogin",
                        @"weixinlogin",
                        @"weibologin",

                        ];
    }
    return _platforms;
}


@end
