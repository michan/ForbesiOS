//
//  FBSLoginSegment.h
//  Forbes
//
//  Created by 周灿华 on 2019/8/3.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FBSLoginSegment : UIView
{
    UIButton *_currentBtn;
}
@property (nonatomic, strong) UIButton *fastBtn;
@property (nonatomic, strong) UIButton *pwdBtn;
@property (nonatomic, strong) UIView *middleLine;
@property (nonatomic, strong) UIImageView *indicator;
@property (nonatomic, assign) NSInteger selecIndex;
@property (nonatomic, strong) void(^selecBlock)(NSInteger selecIndex);
@end
