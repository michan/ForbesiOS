
//
//  FBSLoginSegment.m
//  Forbes
//
//  Created by 周灿华 on 2019/8/3.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSLoginSegment.h"

@implementation FBSLoginSegment

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self addSubview:self.fastBtn];
        [self addSubview:self.pwdBtn];
        [self addSubview:self.middleLine];
        [self addSubview:self.indicator];
        
        [self.fastBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.top.mas_equalTo(0);
            make.width.mas_equalTo(kScreenWidth * 0.5 - 1);
            make.height.mas_equalTo(44);
        }];
        
        
        [self.pwdBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.right.mas_equalTo(0);
            make.width.mas_equalTo(kScreenWidth * 0.5 - 1);
            make.height.mas_equalTo(44);
            
        }];
        
        [self.middleLine mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.centerY.mas_equalTo(0);
            make.width.mas_equalTo(1);
            make.height.mas_equalTo(22);
        }];
        
        
        [self.indicator mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.mas_equalTo(WScale(80));
            make.height.mas_equalTo(2);
            make.bottom.mas_equalTo(0);
            make.centerX.mas_equalTo(- kScreenWidth * 0.25);
        }];
        
    }
    return self;
}


#pragma mark - action

- (void)clickButton:(UIButton *)button {
    if (_currentBtn != button) {
        _currentBtn.selected = NO;
        button.selected = YES;
        _currentBtn = button;
        
        self.selecIndex = button.tag;
        if (self.selecBlock) {
            self.selecBlock(button.tag);
        }
        
        CGFloat relation = (button.tag) == 1 ? 1.0 : -1.0;
        
        [UIView animateWithDuration:0.25 animations:^{
            [self.indicator mas_updateConstraints:^(MASConstraintMaker *make) {
                make.centerX.mas_equalTo(0.25 * kScreenWidth * relation);
            }];
            
            [self layoutIfNeeded];
        }];
    }
}

#pragma mark - property

- (UIButton *)fastBtn {
    if (!_fastBtn) {
        _fastBtn  = [UIButton buttonWithType:UIButtonTypeCustom];
        _fastBtn.tag = 0;
        _fastBtn.titleLabel.font = kLabelFontSize15;
        [_fastBtn setTitleColor:Hexcolor(0x999999) forState:UIControlStateNormal];
        [_fastBtn setTitleColor:MainThemeColor forState:UIControlStateSelected];
        [_fastBtn setTitle:@"手机号快捷登录" forState:UIControlStateNormal];
        [_fastBtn addTarget:self action:@selector(clickButton:) forControlEvents:UIControlEventTouchUpInside];
        _fastBtn.selected = YES;
        
        _currentBtn = _fastBtn;
    }
    return _fastBtn;
}


- (UIButton *)pwdBtn {
    if (!_pwdBtn) {
        _pwdBtn  = [UIButton buttonWithType:UIButtonTypeCustom];
        _pwdBtn.tag = 1;
        _pwdBtn.titleLabel.font = kLabelFontSize15;
        [_pwdBtn setTitleColor:Hexcolor(0x999999) forState:UIControlStateNormal];
        [_pwdBtn setTitleColor:MainThemeColor forState:UIControlStateSelected];
        [_pwdBtn setTitle:@"账户密码登陆" forState:UIControlStateNormal];
        [_pwdBtn addTarget:self action:@selector(clickButton:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _pwdBtn;
}

- (UIView *)middleLine {
    if (!_middleLine) {
        _middleLine  = [[UIView alloc] init];
        _middleLine.backgroundColor = Hexcolor(0xCCCCCC);
    }
    return _middleLine;
}


- (UIImageView *)indicator {
    if (!_indicator) {
        _indicator  = [[UIImageView alloc] init];
        _indicator.backgroundColor = MainThemeColor;
        _indicator.layer.cornerRadius = 1;
    }
    return _indicator;
}


@end
