//
//  FBSLoginPlatformView.h
//  Forbes
//
//  Created by 周灿华 on 2019/8/4.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import <UIKit/UIKit.h>

@class FBSLoginPlatformView;

typedef enum : NSUInteger {
    ShareLoginTypeqq,
    ShareLoginTypewx,
    ShareLoginTypewb,
    ShareLoginTypeapple,

} ShareLoginType;
@protocol FBSLoginPlatformViewDelegate<NSObject>

@required
- (void)clickShareLogin:(FBSLoginPlatformView *)PlatformView didClickWithCompanyModel:( ShareLoginType)type;

@end

@interface FBSLoginPlatformView : UIView
@property(nonatomic, weak) id <FBSLoginPlatformViewDelegate>delegate;
@end

