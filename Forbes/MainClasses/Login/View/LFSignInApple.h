//
//  LFSignInApple.h
//  instalment
//
//  Created by Dollar on 2020/6/5.
//  Copyright © 2020 BSD. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface LFSignInApple : NSObject

// 处理授权
- (void)handleAuthorizationAppleIDButtonPress;

// 如果存在iCloud Keychain 凭证或者AppleID 凭证提示用户
- (void)perfomExistingAccountSetupFlows;


@property (nonatomic, strong) void(^loginBlock)(NSString *authorizationCode);

@end

NS_ASSUME_NONNULL_END
