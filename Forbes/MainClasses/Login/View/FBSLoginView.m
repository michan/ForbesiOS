//
//  FBSLoginView.m
//  Forbes
//
//  Created by 周灿华 on 2019/8/3.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSLoginView.h"

@interface FBSLoginView ()<UITextFieldDelegate>
@property (nonatomic, strong) UIView *inputBgView;
@property (nonatomic, strong) UIView *topLine;
@property (nonatomic, strong) UIView *middleLine;
@property (nonatomic, strong) UIView *bottomLine;
@end

@implementation FBSLoginView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self addSubview:self.inputBgView];
        [self addSubview:self.loginBtn];
        [self addSubview:self.tipL];
        [self addSubview:self.signupBtn];
        [self addSubview:self.forgetBtn];
        
        [self.inputBgView addSubview:self.tf1];
        [self.inputBgView addSubview:self.tf2];
        [self.inputBgView addSubview:self.topLine];
        [self.inputBgView addSubview:self.middleLine];
        [self.inputBgView addSubview:self.bottomLine];
        [self.inputBgView addSubview:self.sendCodeBtn];
        [self.inputBgView addSubview:self.seeBtn];
        self.tf2.secureTextEntry = NO;
        self.seeBtn.hidden = YES;

        
        

        
        [self.inputBgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.top.right.mas_equalTo(0);
            make.height.mas_equalTo(91);
        }];
        
        
        [self.loginBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(0);
            make.top.mas_equalTo(self.inputBgView.mas_bottom).offset(WScale(21));
            make.width.mas_equalTo(kScreenWidth - 50);
            make.height.mas_equalTo(40);
        }];
        
        
        [self.tipL mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.loginBtn);
            make.top.mas_equalTo(self.loginBtn.mas_bottom).offset(WScale(10));
            make.height.mas_equalTo(WScale(12));
        }];
        
        
        [self.signupBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.loginBtn.mas_bottom);
            make.right.mas_equalTo(self.loginBtn);
            make.height.mas_equalTo(WScale(34));
        }];
        
        
        [self.forgetBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.loginBtn.mas_bottom);
            make.left.mas_equalTo(self.loginBtn);
            make.height.mas_equalTo(WScale(34));
        }];

        
        [self.topLine mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.top.right.mas_equalTo(0);
            make.height.mas_equalTo(1);
        }];
        
        [self.middleLine mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.mas_equalTo(0);
            make.height.mas_equalTo(1);
            make.centerY.mas_equalTo(0);
        }];
        
        
        [self.bottomLine mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.bottom.mas_equalTo(0);
            make.height.mas_equalTo(1);
        }];
        
        [self.tf1 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.topLine.mas_bottom);
            make.left.mas_equalTo(WScale(30));
            make.width.mas_equalTo(WScale(200));
            make.bottom.mas_equalTo(self.middleLine.mas_top);
        }];
        
        
        [self.tf2 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.middleLine.mas_bottom);
            make.left.mas_equalTo(WScale(30));
            make.width.mas_equalTo(WScale(200));
            make.bottom.mas_equalTo(self.bottomLine.mas_top);
        }];
        
        
        [self.sendCodeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.mas_equalTo(WScale(70));
            make.height.mas_equalTo(WScale(25));
            make.centerY.mas_equalTo(self.tf2);
            make.right.mas_equalTo(-WScale(34));
        }];
        [self.seeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.mas_equalTo(WScale(25));
            make.height.mas_equalTo(WScale(25));
            make.centerY.mas_equalTo(self.tf2);
            make.right.mas_equalTo(-WScale(34));
        }];


    }
    return self;
}


- (void)setIsPwd:(BOOL)isPwd {
    _isPwd = isPwd;
    if (isPwd) {
        self.tf1.placeholder = @"输入账户名称";
        self.tf2.placeholder = @"请输入密码";
        self.sendCodeBtn.hidden = YES;
        self.tipL.hidden = YES;
        self.forgetBtn.hidden = NO;
        self.tf2.secureTextEntry = YES;
        self.seeBtn.hidden = NO;

    } else {
        self.tf1.placeholder = @"请输入手机号";
        self.tf2.placeholder = @"请输入验证码";
        self.sendCodeBtn.hidden = NO;
        self.tf1.keyboardType = UIKeyboardTypeNumberPad;
        self.tf2.keyboardType = UIKeyboardTypeNumberPad;
        self.tipL.hidden = NO;
        self.forgetBtn.hidden = YES;
        self.tf2.secureTextEntry = NO;
        self.seeBtn.hidden = YES;


    }
}


#pragma mark - action

- (void)loginAciton {
    if (self.loginBlock) {
        self.loginBlock(self.tf1, self.tf2);
    }
}


- (void)signupAciton {
    if (self.signupBlock) {
        self.signupBlock();
    }
}

-(void)seeBtnAction:(UIButton*)btn{
    btn.selected= !btn.selected;
    self.tf2.secureTextEntry = !btn.selected;

}
- (void)forgetAction {
    if (self.forgetBlock) {
        self.forgetBlock();
    }
}

- (void)sendCodeAction {
    if (self.countDownBlock) {
        BOOL result = self.countDownBlock(self.tf1,self.tf2);
        if (result) {
            NSLog(@"发送验证码。。");
            [self sentPhoneCodeTimeMethod];

        }
    }
}

#pragma mark - 发送验证码

//计时器发送验证码
-(void)sentPhoneCodeTimeMethod{
    //倒计时时间 - 60秒
    __block NSInteger timeOut = 59;
    //执行队列
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    //计时器 -》dispatch_source_set_timer自动生成
    dispatch_source_t timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, queue);
    dispatch_source_set_timer(timer, DISPATCH_TIME_NOW, 1.0 * NSEC_PER_SEC, 0 * NSEC_PER_SEC);
    dispatch_source_set_event_handler(timer, ^{
        if (timeOut <= 0) {
            dispatch_source_cancel(timer);
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.sendCodeBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
                [self.sendCodeBtn setUserInteractionEnabled:YES];
            });
        }else{
            //开始计时
            //剩余秒数 seconds
            NSInteger seconds = timeOut % 60;
            NSString *strTime = [NSString stringWithFormat:@"%.1ld",seconds];
            //主线程设置按钮样式
            dispatch_async(dispatch_get_main_queue(), ^{
                [UIView beginAnimations:nil context:nil];
                [UIView setAnimationDuration:1.0];
                [self.sendCodeBtn setTitle:[NSString stringWithFormat:@"%@S",strTime] forState:UIControlStateNormal];
                [UIView commitAnimations];
                //计时器件不允许点击
                [self.sendCodeBtn setUserInteractionEnabled:NO];
            });
            timeOut--;
        }
    });
    dispatch_resume(timer);
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

#pragma mark - property

- (UIView *)inputBgView {
    if (!_inputBgView) {
        _inputBgView  = [[UIView alloc] init];
        _inputBgView.backgroundColor = [UIColor whiteColor];
    }
    return _inputBgView;
}


- (UITextField *)tf1 {
    if (!_tf1) {
        _tf1  = [[UITextField alloc] init];
        _tf1.font = kLabelFontSize14;
        _tf1.textColor = Hexcolor(0x333333);
        _tf1.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"" attributes:@{NSFontAttributeName:kLabelFontSize14,NSForegroundColorAttributeName:Hexcolor(0xCBCCCC)}];
        _tf1.returnKeyType = UIReturnKeyDone;
        _tf1.clearButtonMode = UITextFieldViewModeWhileEditing;
        _tf1.delegate = self;
    }
    return _tf1;
}


- (UITextField *)tf2 {
    if (!_tf2) {
        _tf2  = [[UITextField alloc] init];
        _tf2.font = kLabelFontSize14;
        _tf2.textColor = Hexcolor(0x333333);
        _tf2.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"" attributes:@{NSFontAttributeName:kLabelFontSize14,NSForegroundColorAttributeName:Hexcolor(0xCBCCCC)}];

        _tf2.returnKeyType = UIReturnKeyDone;
        _tf2.clearButtonMode = UITextFieldViewModeWhileEditing;
        _tf2.delegate = self;
    }
    return _tf2;
}


- (UIView *)topLine {
    if (!_topLine) {
        _topLine  = [[UIView alloc] init];
        _topLine.backgroundColor = RGB(232, 232, 232);
    }
    return _topLine;
}


- (UIView *)middleLine {
    if (!_middleLine) {
        _middleLine  = [[UIView alloc] init];
        _middleLine.backgroundColor = RGB(232, 232, 232);
    }
    return _middleLine;
}


- (UIView *)bottomLine {
    if (!_bottomLine) {
        _bottomLine  = [[UIView alloc] init];
        _bottomLine.backgroundColor = RGB(232, 232, 232);
    }
    return _bottomLine;
}




- (UIButton *)loginBtn {
    if (!_loginBtn) {
        _loginBtn  = [UIButton buttonWithType:UIButtonTypeCustom];
        [_loginBtn setBackgroundImage:[UIImage imageWithColor:MainThemeColor] forState:UIControlStateNormal];
        [_loginBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_loginBtn setTitle:@"登陆" forState:UIControlStateNormal];
        _loginBtn.layer.cornerRadius = 5;
        _loginBtn.layer.masksToBounds = YES;
        [_loginBtn addTarget:self action:@selector(loginAciton) forControlEvents:UIControlEventTouchUpInside];
    }
    return _loginBtn;
}


- (UILabel *)tipL {
    if (!_tipL) {
        _tipL  = [[UILabel alloc] init];
        _tipL.font = kLabelFontSize12;
        _tipL.textColor = Hexcolor(0xCCCCCC);
        _tipL.text = @"未注册手机号请点击立即注册";
    }
    return _tipL;
}



- (UIButton *)sendCodeBtn {
    if (!_sendCodeBtn) {
        _sendCodeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_sendCodeBtn setBackgroundColor:[UIColor whiteColor]];
        _sendCodeBtn.titleLabel.font = kLabelFontSize12;
        _sendCodeBtn.titleLabel.adjustsFontSizeToFitWidth = YES;
        [_sendCodeBtn setTitleColor:Hexcolor(0xBB833E) forState:UIControlStateNormal];
        [_sendCodeBtn setTitle:@"获取验证码" forState:UIControlStateNormal];

        [_sendCodeBtn.layer setBorderWidth:1.0];
        [_sendCodeBtn.layer setBorderColor:Hexcolor(0xBB833E).CGColor];

        [_sendCodeBtn addTarget:self action:@selector(sendCodeAction) forControlEvents:UIControlEventTouchUpInside];
        
    }
    return _sendCodeBtn;
}
-(UIButton*)seeBtn{
    if (!_seeBtn) {
        _seeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//        [_seeBtn setBackgroundColor:[UIColor redColor]];
        [_seeBtn setImage:[UIImage imageNamed:@"icon_keshi_dengluzhuce"] forState:UIControlStateSelected];
        [_seeBtn setImage:[UIImage imageNamed:@"icon_bukeshi_dengluzhuce"] forState:0];

        [_seeBtn addTarget:self action:@selector(seeBtnAction:) forControlEvents:UIControlEventTouchUpInside];

        
        
        
    }
    
    return _seeBtn;
    
}
- (UIButton *)signupBtn {
    if (!_signupBtn) {
        _signupBtn  = [UIButton buttonWithType:UIButtonTypeCustom];
        _signupBtn.titleLabel.font = kLabelFontSize12;
        [_signupBtn setTitleColor:MainThemeColor forState:UIControlStateNormal];
        [_signupBtn setTitle:@"立即注册" forState:UIControlStateNormal];
        [_signupBtn addTarget:self action:@selector(signupAciton) forControlEvents:UIControlEventTouchUpInside];
    }
    return _signupBtn;
}



- (UIButton *)forgetBtn {
    if (!_forgetBtn) {
        _forgetBtn  = [UIButton buttonWithType:UIButtonTypeCustom];
        _forgetBtn.titleLabel.font = kLabelFontSize12;
        [_forgetBtn setTitleColor:MainThemeColor forState:UIControlStateNormal];
        [_forgetBtn setTitle:@"忘记密码？" forState:UIControlStateNormal];
        [_forgetBtn addTarget:self action:@selector(forgetAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _forgetBtn;
}



@end
