//
//  FBSLoginView.h
//  Forbes
//
//  Created by 周灿华 on 2019/8/3.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FBSLoginView : UIView
@property (nonatomic, assign) BOOL isPwd;
@property (nonatomic, strong) UITextField *tf1;
@property (nonatomic, strong) UITextField *tf2;
@property (nonatomic, strong) UIButton *loginBtn;
@property (nonatomic, strong) UIButton *seeBtn;

@property (nonatomic, strong) UIButton *signupBtn;
@property (nonatomic, strong) UIButton *forgetBtn;
@property (nonatomic, strong) UILabel *tipL;
@property (nonatomic, strong) UIButton *sendCodeBtn;

@property (nonatomic, strong) void(^loginBlock)(UITextField *tf1,UITextField *tf2);
@property (nonatomic, strong) void(^signupBlock)(void);
@property (nonatomic, strong) void(^forgetBlock)(void);
@property (nonatomic, strong) BOOL(^countDownBlock)(UITextField *tf1,UITextField *tf2);

@end

