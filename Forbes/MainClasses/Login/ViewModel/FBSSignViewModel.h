//
//  FBSSignViewModel.h
//  Forbes
//
//  Created by 周灿华 on 2019/9/8.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSViewModel.h"

@interface FBSSignViewModel : FBSViewModel

- (void)rqSignupCode:(NSString *)tel
            complete:(void(^)(BOOL success, NSString *msg))complete;

- (void)rqSignup:(NSString *)tel
        nickName:(NSString *)nickName
           email:(NSString *)email
        password:(NSString *)password
            code:(NSString *)code
        complete:(void(^)(BOOL success, NSString *msg))complete;
- (void)rqForgetSignupCode:(NSString *)tel
                  complete:(void(^)(BOOL success, NSString *msg))complete;
- (void)rqForgetSignupCodeReplay:(NSString *)tel password:(NSString *)password code:(NSString *)code
                        complete:(void(^)(BOOL success, NSString *msg))complete;
@end

