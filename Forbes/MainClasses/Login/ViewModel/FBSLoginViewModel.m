//
//  FBSLoginViewModel.m
//  Forbes
//
//  Created by 周灿华 on 2019/8/3.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSLoginViewModel.h"
#import "FBSLoginAPI.h"

@interface FBSLoginViewModel ()
@property (nonatomic, strong) FBSAccountLoginAPI *accountLoginAPI;
@property (nonatomic, strong) FBSPhoneCodeAPI *phoneCodeAPI;
@property (nonatomic, strong) FBSPhoneLoginAPI *phoneLoginAPI;

@end

@implementation FBSLoginViewModel

- (void)rqPwdLogin:(NSString *)account
             email:(NSString *)email
          password:(NSString *)password
          complete:(void(^)(BOOL success, NSString *msg))complete {
    
    self.accountLoginAPI.account = nil;
    self.accountLoginAPI.email = nil;
    self.accountLoginAPI.password= nil;
    
    if (account.length) {
        self.accountLoginAPI.account = account;
    }
    
    if (email.length) {
        self.accountLoginAPI.email = email;
    }

    self.accountLoginAPI.password = password;
    
    [self.accountLoginAPI startWithSuccess:^(YBNetworkResponse * _Nonnull response) {
        
        FBSLog(@"登录返回信息 : %@",response.responseObject);
        
        FBSBaseModel *model = [FBSBaseModel mj_objectWithKeyValues:response.responseObject];
        if (model.success) {
            //更新用户数据
            [[FBSUserData sharedData] saveUserData:response.responseObject];
            
            //发布登录成功通知
            [[NSNotificationCenter defaultCenter] postNotificationName:FBSLoginSuccessNotication object:nil];
        }
        
        if (complete) {
            complete(model.success,model.msg);
        }
    
    } failure:^(YBNetworkResponse * _Nonnull response) {
        FBSLog(@"登录失败: %@",response.error.localizedDescription);
        
        if (complete) {
            complete(NO ,response.error.localizedDescription);
        }

    }];
}


- (void)rqPhoneCode:(NSString *)tel
           complete:(void(^)(BOOL success, NSString *msg))complete {
    
    self.phoneCodeAPI.tel = tel;
    
    [self.phoneCodeAPI startWithSuccess:^(YBNetworkResponse * _Nonnull response) {
        
        FBSLog(@"获取手机号登录验证码 : %@",response.responseObject);
        
        FBSBaseModel *model = [FBSBaseModel mj_objectWithKeyValues:response.responseObject];
        
        if (complete) {
            complete(model.success,model.msg);
        }
        
    } failure:^(YBNetworkResponse * _Nonnull response) {
        FBSLog(@"获取手机号登录验证码失败: %@",response.error.localizedDescription);
        
        if (complete) {
            complete(NO ,response.error.localizedDescription);
        }
        
    }];
}
- (void)rqWXLoginCode:(NSString *)code Openid:(NSString*)openid complete:(void(^)(BOOL success, NSString *msg))complete {
    FBSPostRequest *rq = [[FBSPostRequest alloc] init];
    rq.requestURI = @"login/wechat";
    rq.requestParameter = @{@"access_token":code?:@"",
                            @"openid":openid?:@""
                            
    };
    [rq startWithSuccess:^(YBNetworkResponse * _Nonnull response) {
        
        FBSLog(@"登录返回信息 : %@",response.responseObject);
        
        FBSBaseModel *model = [FBSBaseModel mj_objectWithKeyValues:response.responseObject];
        if (model.success) {
            //更新用户数据
            [[FBSUserData sharedData] saveUserData:response.responseObject];
            
            //发布登录成功通知
            [[NSNotificationCenter defaultCenter] postNotificationName:FBSLoginSuccessNotication object:nil];
        }
        
        if (complete) {
            complete(model.success,model.msg);
        }
        
    } failure:^(YBNetworkResponse * _Nonnull response) {
        FBSLog(@"登录失败: %@",response.error.localizedDescription);
        
        if (complete) {
            complete(NO ,response.error.localizedDescription);
        }
        
    }];
}
- (void)rqQQLoginApp_type:(NSString *)app_type access_token:(NSString *)access_token openid:(NSString *)openid complete:(void(^)(BOOL success, NSString *msg))complete {
    FBSPostRequest *rq = [[FBSPostRequest alloc] init];
    rq.requestURI = @"login/qq";
    rq.requestParameter = @{@"app_type":app_type,@"access_token":access_token,@"openid":openid?:@""};
    
    
    
    [rq startWithSuccess:^(YBNetworkResponse * _Nonnull response) {
        
        FBSLog(@"登录返回信息 : %@",response.responseObject);
        
        FBSBaseModel *model = [FBSBaseModel mj_objectWithKeyValues:response.responseObject];
        if (model.success) {
            //更新用户数据
            [[FBSUserData sharedData] saveUserData:response.responseObject];
            
            //发布登录成功通知
            [[NSNotificationCenter defaultCenter] postNotificationName:FBSLoginSuccessNotication object:nil];
        }
        
        if (complete) {
            complete(model.success,model.msg);
        }
        
    } failure:^(YBNetworkResponse * _Nonnull response) {
        FBSLog(@"登录失败: %@",response.error.localizedDescription);
        
        if (complete) {
            complete(NO ,response.error.localizedDescription);
        }
        
    }];
}
- (void)rqWBLoginaccess_token:(NSString *)access_token complete:(void(^)(BOOL success, NSString *msg))complete {
    FBSPostRequest *rq = [[FBSPostRequest alloc] init];
    rq.requestURI = @"login/weibo";
    rq.requestParameter = @{@"access_token":access_token?:@""};
    [rq startWithSuccess:^(YBNetworkResponse * _Nonnull response) {
        
        FBSLog(@"登录返回信息 : %@",response.responseObject);
        
        FBSBaseModel *model = [FBSBaseModel mj_objectWithKeyValues:response.responseObject];
        if (model.success) {
            //更新用户数据
            [[FBSUserData sharedData] saveUserData:response.responseObject];
            
            //发布登录成功通知
            [[NSNotificationCenter defaultCenter] postNotificationName:FBSLoginSuccessNotication object:nil];
        }
        
        if (complete) {
            complete(model.success,model.msg);
        }
        
    } failure:^(YBNetworkResponse * _Nonnull response) {
        FBSLog(@"登录失败: %@",response.error.localizedDescription);
        
        if (complete) {
            complete(NO ,response.error.localizedDescription);
        }
        
    }];
}

- (void)rqPhoneLogin:(NSString *)phone
                code:(NSString *)code
            complete:(void(^)(BOOL success, NSString *msg))complete {
    
    self.phoneLoginAPI.tel = phone;
    self.phoneLoginAPI.code = code;
    
    [self.phoneLoginAPI startWithSuccess:^(YBNetworkResponse * _Nonnull response) {
        
        FBSLog(@"登录返回信息 : %@",response.responseObject);
        
        FBSBaseModel *model = [FBSBaseModel mj_objectWithKeyValues:response.responseObject];
        if (model.success) {
            //更新用户数据
            [[FBSUserData sharedData] saveUserData:response.responseObject];
            
            //发布登录成功通知
            [[NSNotificationCenter defaultCenter] postNotificationName:FBSLoginSuccessNotication object:nil];
        }
        
        if (complete) {
            complete(model.success,model.msg);
        }
        
    } failure:^(YBNetworkResponse * _Nonnull response) {
        FBSLog(@"登录失败: %@",response.error.localizedDescription);
        
        if (complete) {
            complete(NO ,response.error.localizedDescription);
        }
        
    }];
    
}


#pragma mark - propety

- (FBSAccountLoginAPI *)accountLoginAPI {
    if (!_accountLoginAPI) {
        _accountLoginAPI  = [[FBSAccountLoginAPI alloc] init];
    }
    return _accountLoginAPI;
}


- (FBSPhoneCodeAPI *)phoneCodeAPI {
    if (!_phoneCodeAPI) {
        _phoneCodeAPI  = [[FBSPhoneCodeAPI alloc] init];
    }
    return _phoneCodeAPI;
}


- (FBSPhoneLoginAPI *)phoneLoginAPI {
    if (!_phoneLoginAPI) {
        _phoneLoginAPI  = [[FBSPhoneLoginAPI alloc] init];
    }
    return _phoneLoginAPI;
}



@end
