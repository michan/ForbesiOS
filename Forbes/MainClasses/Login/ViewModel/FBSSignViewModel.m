//
//  FBSSignViewModel.m
//  Forbes
//
//  Created by 周灿华 on 2019/9/8.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSSignViewModel.h"
#import "FBSLoginAPI.h"
#import "HLWJMD5.h"

@interface FBSSignViewModel ()
@property (nonatomic, strong) FBSSignupcodeAPI *signupcodeAPI;
@property (nonatomic, strong) FBSSignupAPI *signupAPI;
@end


@implementation FBSSignViewModel

- (void)rqForgetSignupCodeReplay:(NSString *)tel password:(NSString *)password code:(NSString *)code
                  complete:(void(^)(BOOL success, NSString *msg))complete {
    
    if (!tel.length||!code.length ||!password.length ) {
        return ;
    }
    FBSPostRequest *forgetCode  = [[FBSPostRequest alloc] init];
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setObject:tel forKey:@"tel"];
    [dic setObject:code forKey:@"code"];
    [dic setObject:password forKey:@"password"];
    forgetCode.requestURI = @"login/changepassword";
    forgetCode.requestParameter = dic;
    [forgetCode startWithSuccess:^(YBNetworkResponse * _Nonnull response) {
        FBSLog(@"获取验证码成功 responseObject : %@",response.responseObject);
        FBSBaseModel *model = [FBSBaseModel mj_objectWithKeyValues:response.responseObject];
        if(model.code == 200){
            if (complete) {
                complete(model.success, model.msg);
            }
        }else{
            complete(NO, model.msg);
        }
    } failure:^(YBNetworkResponse * _Nonnull response) {
        if (complete) {
            complete(NO, response.error.localizedDescription);
        }
    }];
}

- (void)rqForgetSignupCode:(NSString *)tel
            complete:(void(^)(BOOL success, NSString *msg))complete {
    
    if (!tel.length) {
        return ;
    }
    FBSPostRequest *forgetCode  = [[FBSPostRequest alloc] init];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"yyyy-MM-dd";
    
    NSString *plainText = [NSString stringWithFormat:@"%@%@",tel,[formatter stringFromDate:[NSDate date]]];
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setObject:[HLWJMD5 MD5ForLower32Bate:plainText] forKey:@"token"];
    [dic setObject:tel forKey:@"tel"];
    forgetCode.requestURI = @"login/forgetcode";
    forgetCode.requestParameter = dic;
    [forgetCode startWithSuccess:^(YBNetworkResponse * _Nonnull response) {
        FBSLog(@"获取验证码成功 responseObject : %@",response.responseObject);
        FBSBaseModel *model = [FBSBaseModel mj_objectWithKeyValues:response.responseObject];
        if(model.code == 200){
            if (complete) {
                complete(model.success, model.msg);
            }
        }else{
            complete(NO, model.msg);
        }
    } failure:^(YBNetworkResponse * _Nonnull response) {
        if (complete) {
            complete(NO, response.error.localizedDescription);
        }
        
    }];
    
}


- (void)rqSignupCode:(NSString *)tel
            complete:(void(^)(BOOL success, NSString *msg))complete {

    if (!tel.length) {
        return ;
    }
    
    self.signupcodeAPI.tel = tel;
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"yyyy-MM-dd";
    
    NSString *plainText = [NSString stringWithFormat:@"%@%@",tel,[formatter stringFromDate:[NSDate date]]];
    self.signupcodeAPI.token = [HLWJMD5 MD5ForLower32Bate:plainText];
    
    [self.signupcodeAPI startWithSuccess:^(YBNetworkResponse * _Nonnull response) {
        FBSLog(@"获取验证码成功 responseObject : %@",response.responseObject);
        FBSBaseModel *model = [FBSBaseModel mj_objectWithKeyValues:response.responseObject];
        if (complete) {
            complete(model.success, model.msg);
        }
        
        
    } failure:^(YBNetworkResponse * _Nonnull response) {
        if (complete) {
            complete(NO, response.error.localizedDescription);
        }

    }];

}


- (void)rqSignup:(NSString *)tel
        nickName:(NSString *)nickName
           email:(NSString *)email
        password:(NSString *)password
            code:(NSString *)code
        complete:(void(^)(BOOL success, NSString *msg))complete {
    self.signupAPI.tel = tel;
    self.signupAPI.account = nickName;;
    self.signupAPI.nickname = nickName;
    self.signupAPI.email = email;
    self.signupAPI.password = password;
    self.signupAPI.code = code;
    
    [self.signupAPI startWithSuccess:^(YBNetworkResponse * _Nonnull response) {
        FBSLog(@"注册成功 responseObject : %@",response.responseObject);
        FBSBaseModel *model = [FBSBaseModel mj_objectWithKeyValues:response.responseObject];
        
        if (model.success) {
            //更新用户数据
            [[FBSUserData sharedData] saveUserData:response.responseObject];
            
            //发布登录成功通知
            [[NSNotificationCenter defaultCenter] postNotificationName:FBSLoginSuccessNotication object:nil];
        }
        
        if (complete) {
            complete(model.success, model.msg);
        }
    } failure:^(YBNetworkResponse * _Nonnull response) {
        if (complete) {
            complete(NO, response.error.localizedDescription);
        }
    }];

}


#pragma mark - property

- (FBSSignupcodeAPI *)signupcodeAPI {
    if (!_signupcodeAPI) {
        _signupcodeAPI  = [[FBSSignupcodeAPI alloc] init];
    }
    return _signupcodeAPI;
}


- (FBSSignupAPI *)signupAPI {
    if (!_signupAPI) {
        _signupAPI  = [[FBSSignupAPI alloc] init];
    }
    return _signupAPI;
}



@end
