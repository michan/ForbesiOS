//
//  FBSLoginViewModel.h
//  Forbes
//
//  Created by 周灿华 on 2019/8/3.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSViewModel.h"

@interface FBSLoginViewModel : FBSViewModel

- (void)rqPwdLogin:(NSString *)account
             email:(NSString *)email
          password:(NSString *)password
           complete:(void(^)(BOOL success, NSString *msg))complete;


- (void)rqPhoneCode:(NSString *)tel
            complete:(void(^)(BOOL success, NSString *msg))complete;


- (void)rqPhoneLogin:(NSString *)phone
                code:(NSString *)code
            complete:(void(^)(BOOL success, NSString *msg))complete;
- (void)rqQQLoginApp_type:(NSString *)app_type access_token:(NSString *)access_token openid:(NSString *)openid complete:(void(^)(BOOL success, NSString *msg))complete;
- (void)rqWXLoginCode:(NSString *)code Openid:(NSString*)openid complete:(void(^)(BOOL success, NSString *msg))complete;
- (void)rqWBLoginaccess_token:(NSString *)access_token complete:(void(^)(BOOL success, NSString *msg))complete;
@end

