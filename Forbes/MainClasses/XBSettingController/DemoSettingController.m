//
//  DemoSettingController.m
//  XBSettingControllerDemo
//
//  Created by XB on 15/9/23.
//  Copyright © 2015年 XB. All rights reserved.
//

#import "DemoSettingController.h"
#import "XBConst.h"
#import "XBSettingCell.h"
#import "XBSettingItemModel.h"
#import "XBSettingSectionModel.h"
#import "FBSMineInfoViewController.h"
#import <StoreKit/SKStoreProductViewController.h>

@interface DemoSettingController ()<UITableViewDelegate,UITableViewDataSource,SKStoreProductViewControllerDelegate>
@property (nonatomic,strong) NSArray  *sectionArray; /**< section模型数组*/
@property (nonatomic,strong) FBSTableView *tableView;
@end

@implementation DemoSettingController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationBarHidden = NO;
    self.view.backgroundColor = XBMakeColorWithRGB(234, 234, 234, 1);
    self.tableView.backgroundColor = [UIColor whiteColor];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.separatorStyle = UITableViewCellSelectionStyleNone;
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    [self.view addSubview:self.tableView];
    [self.view insertSubview:self.tableView belowSubview:self.navigationBar];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.left.right.mas_equalTo(self.view);
        make.top.mas_equalTo(self.view.mas_top).offset(kNavTopMargin);
    }];
    [self setupSections];
}
- (FBSTableView *)tableView{
    if (!_tableView) {
        _tableView = [[FBSTableView alloc] init];
    }
    return _tableView;
}
- (void)showAlert:(NSString *)title
{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"温馨提示" message:title delegate:nil cancelButtonTitle:@"确定" otherButtonTitles: nil];
    [alert show];
}
//计算出大小
- (NSString *)fileSizeWithInterge:(NSInteger)size{
    // 1k = 1024, 1m = 1024k
    if (size < 1024) {// 小于1k
        return [NSString stringWithFormat:@"%ldB",(long)size];
    }else if (size < 1024 * 1024){// 小于1m
        CGFloat aFloat = size/1024;
        return [NSString stringWithFormat:@"%.0fK",aFloat];
    }else if (size < 1024 * 1024 * 1024){// 小于1G
        CGFloat aFloat = size/(1024 * 1024);
        return [NSString stringWithFormat:@"%.1fM",aFloat];
    }else{
        CGFloat aFloat = size/(1024*1024*1024);
        return [NSString stringWithFormat:@"%.1fG",aFloat];
    }
}
#pragma - mark setup
- (void)setupSections
{
    //************************************section1
    XBSettingItemModel *item1 = [[XBSettingItemModel alloc]init];
    item1.funcName = @"个人资料";
    item1.executeCode = ^{
        FBSMineInfoViewController *vc = [[FBSMineInfoViewController alloc] init];
        [self.navigationController pushViewController:vc animated:YES];
    };
    NSUInteger intg = [[SDImageCache sharedImageCache] getSize];
    //
    NSString * currentVolum = [NSString stringWithFormat:@"%@",[self fileSizeWithInterge:intg]];

    item1.accessoryType = XBSettingAccessoryTypeDisclosureIndicator;
    XBSettingItemModel *item2 = [[XBSettingItemModel alloc]init];
    item2.funcName = @"清除缓存";
    item2.executeCode = ^{
        [[SDImageCache sharedImageCache] clearDiskOnCompletion:nil];
        [[SDImageCache sharedImageCache] clearMemory];//可不写
        NSUInteger intg = [[SDImageCache sharedImageCache] getSize];
        NSString * currentVolum = [NSString stringWithFormat:@"%@",[self fileSizeWithInterge:intg]];
        XBSettingSectionModel *section = self.sectionArray[0];
        XBSettingItemModel *item = section.itemArray[1];
        item.detailText = @"0";
        [self.tableView reloadData];
    };
    item2.detailText = currentVolum;
    item2.accessoryType = XBSettingAccessoryTypeDisclosureIndicator;
    
    XBSettingItemModel *item3 = [[XBSettingItemModel alloc]init];
    item3.funcName = @"关于我们";
    item3.executeCode = ^{
        [self showAlert:@"福布斯一款成功人士的y软件"];
    };
    item3.accessoryType = XBSettingAccessoryTypeDisclosureIndicator;
    
    XBSettingItemModel *item4 = [[XBSettingItemModel alloc]init];
    item4.funcName = @"评价";
    item4.executeCode = ^{
        [self showAlert:@"上线后获取id即可进行评价"];
       // [self loadAppStoreController];
    };
    item4.accessoryType = XBSettingAccessoryTypeDisclosureIndicator;
    XBSettingSectionModel *section1 = [[XBSettingSectionModel alloc]init];
    section1.sectionHeaderHeight = 18;
    section1.itemArray = @[item1,item2];
    self.sectionArray = @[section1];
}
//自定义方法

- (void)loadAppStoreController

{
    
    /*
     NSString *str = [NSString stringWithFormat:  @"itms-apps://ax.itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?
     
     type=Purple+Software&id=%d",436957167 ];
     [[UIApplication sharedApplication] openURL:[NSURL urlWithString:str]];
     */
    // 初始化控制器
    
    SKStoreProductViewController*storeProductViewContorller = [[SKStoreProductViewController alloc]init];
    
    // 设置代理请求为当前控制器本身
    
    storeProductViewContorller.delegate=self;
    
    [storeProductViewContorller loadProductWithParameters:@{SKStoreProductParameterITunesItemIdentifier:@"1485976996"}completionBlock:^(BOOL result,NSError *error)   {
        
        if(error)  {
            
            NSLog(@"error %@ with userInfo %@",error,[error userInfo]);
            
        }else{
            
            // 模态弹出appstore
            
            [self presentViewController:storeProductViewContorller animated:YES completion:nil];
            
        }
        
    }];
    
}



//AppStore取消按钮监听

- (void)productViewControllerDidFinish:(SKStoreProductViewController*)viewController

{
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.sectionArray.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    XBSettingSectionModel *sectionModel = self.sectionArray[section];
    return sectionModel.itemArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"setting";
    XBSettingSectionModel *sectionModel = self.sectionArray[indexPath.section];
    XBSettingItemModel *itemModel = sectionModel.itemArray[indexPath.row];
    
    XBSettingCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell) {
        cell = [[XBSettingCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    cell.item = itemModel;
    return cell;
}

#pragma - mark UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    XBSettingSectionModel *sectionModel = self.sectionArray[section];
    return sectionModel.sectionHeaderHeight;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    XBSettingSectionModel *sectionModel = self.sectionArray[indexPath.section];
    XBSettingItemModel *itemModel = sectionModel.itemArray[indexPath.row];
    if (itemModel.executeCode) {
        itemModel.executeCode();
    }
}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    XBSettingSectionModel *sectionModel = [self.sectionArray firstObject];
    CGFloat sectionHeaderHeight = sectionModel.sectionHeaderHeight;
    
    if (scrollView.contentOffset.y<=sectionHeaderHeight&&scrollView.contentOffset.y>=0) {
        scrollView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0);
    } else if (scrollView.contentOffset.y>=sectionHeaderHeight) {
        scrollView.contentInset = UIEdgeInsetsMake(-sectionHeaderHeight, 0, 0, 0);
    }
}

@end
