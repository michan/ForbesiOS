//
//  DetailViewModel.h
//  Forbes
//
//  Created by 周灿华 on 2019/9/22.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSTableViewModel.h"
#import "FBSOneImageCell.h"
#import "FBSThreeImageCell.h"
#import "FBSOnlyTextCell.h"
#import "FBSSpaceCell.h"
#import "FBSGroupHeadCell.h"
#import "FBSArticalModel.h"
#import "FBSWenZhangModel.h"
#import "FBSPinglunItem.h"
#import "FBSRecordPinglunLikeCollectionHositoryModel.h"

@interface DetailViewModel : FBSTableViewModel

@property (nonatomic, strong) NSString *cellAdress;

@property (nonatomic, strong)FBSArticalModel *model;
@property (nonatomic, strong)FBSWenZhangModel *WenZhangModel;

- (instancetype)initWithArticleId:(NSString *)articleId htmlAddressL:(NSString *)html cellAddress:(NSString *)cell;

///获取文章详情
- (void)rqArticleDetail:(void(^)(BOOL success,NSString *bodyHTML))complete;

///获取感兴趣文章
- (void)rqArticleInterest:(void(^)(BOOL success))complete;
// 获取评论数据
- (void)rqLikeIndex:(FBSPinglunItem *)indexItem block:(void(^)(BOOL success,NSInteger count))complete;
- (void)rqContent:(void(^)(BOOL success,NSInteger count))complete;
- (void)rqSendIndex:(FBSPinglunItem *)indexItem text:(NSString *)text block:(void(^)(BOOL success,NSInteger count))complete;
- (void)rqfans:(FBSPinglunItem *)indexItem block:(void(^)(BOOL success,NSInteger count))complete;
- (void)rqwenzhangLike:(FBSPinglunItem *)indexItem block:(void(^)(BOOL success,NSInteger count))complete;
@property(nonatomic, strong) FBSRecordPinglunLikeCollectionHositoryModel *PinglunLikeCollectionHositoryModel;

@end

