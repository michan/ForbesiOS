//
//  DetailViewModel.m
//  Forbes
//
//  Created by 周灿华 on 2019/9/22.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "DetailViewModel.h"
#import "DetailInterestModel.h"
#import "HLWJMD5.h"
#import "FBSPinglunModel.h"
#import "FBSPinglunItem.h"

@interface DetailViewModel ()
@property (nonatomic, strong) NSString *articleId;
@property (nonatomic, strong) NSString *htmlAdress;
@property (nonatomic, strong) FBSGetRequest *detailAPI;
@property (nonatomic, strong) FBSGetRequest *interestAPI;
@property (nonatomic, strong) DetailInterestModel *interstModel;

@property (nonatomic, strong) NSDictionary *itemTypeDic;
@property (nonatomic ,strong) NSMutableDictionary *dataDic;
@property (nonatomic ,strong)FBSPostRequest *isLikeApi;
@property (nonatomic ,strong)FBSPinglunModel *modelPinglun;
@property(nonatomic, strong) FBSGetRequest *pinglunAPI;
@property(nonatomic, strong) FBSPostRequest *sendApi;
@property(nonatomic, strong) FBSPostRequest *favs;


@end

@implementation DetailViewModel


- (instancetype)initWithArticleId:(NSString *)articleId htmlAddressL:(NSString *)html cellAddress:(NSString *)cell{
    self = [super init];
    if (self) {
        self.articleId = articleId;
        self.cellAdress = cell;
        self.htmlAdress = html;
    }
    return self;
}

- (void)rqArticleDetail:(void(^)(BOOL success,NSString *bodyHTML))complete; {
    
    [self.detailAPI startWithSuccess:^(YBNetworkResponse * _Nonnull response) {
        if(self.cellAdress.length > 0 ){
            FBSLog(@"文章详情 : %@",response.responseObject);

            NSArray *parts = [response.responseObject valueForKeyPath:@"data.parts"];
            NSDictionary *objDic = [parts firstObject];
            NSString *content = [objDic valueForKey:@"content"];
            self.WenZhangModel = [FBSWenZhangModel mj_objectWithKeyValues:response.responseObject];
            complete(YES, content);
            
        }else{
            FBSLog(@"活动详情 : %@",response.responseObject);
            self.model = [FBSArticalModel mj_objectWithKeyValues:response.responseObject];
            NSDictionary *parts = [response.responseObject valueForKeyPath:@"data"];
            NSString *content = parts[@"describe"];
            complete(YES, content);
        }
    } failure:^(YBNetworkResponse * _Nonnull response) {
        complete(NO, nil);
    }];
}


- (void)rqArticleInterest:(void(^)(BOOL success))complete; {
    WEAKSELF
    [self.interestAPI startWithSuccess:^(YBNetworkResponse * _Nonnull response) {
        FBSLog(@"获取要闻成功 : %@",response.responseObject);
        weakSelf.interstModel = [DetailInterestModel mj_objectWithKeyValues:response.responseObject];
        [weakSelf rqContent:^(BOOL success, NSInteger count) {
            if (success) {
                [weakSelf createItems];
                complete(YES);
            }else{
                [weakSelf createItems];
                complete(YES);
            }
        }];
    } failure:^(YBNetworkResponse * _Nonnull response) {
        complete(NO);
        
    }];
}
- (void)rqContent:(void(^)(BOOL success,NSInteger count))complete{
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    FBSUserData *user = [FBSUserData sharedData];
    [dic setObject:user.useId forKey:@"user_id"];
    [dic setObject:self.articleId forKey:@"object_id"];
    if (self.cellAdress.length) {
        [dic setObject:@"1" forKey:@"object_type"];
    }else{
        [dic setObject:@"2" forKey:@"object_type"];
    }
    self.pinglunAPI.requestParameter = dic;
    [self.pinglunAPI startWithSuccess:^(YBNetworkResponse * _Nonnull response) {
        self.modelPinglun = [FBSPinglunModel mj_objectWithKeyValues:response.responseObject];
        complete(YES, 0);
    } failure:^(YBNetworkResponse * _Nonnull response) {
        complete(NO, 0);
    }];
}
- (void)rqLikeIndex:(FBSPinglunItem *)indexItem block:(void(^)(BOOL success,NSInteger count))complete{
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    FBSUserData *user = [FBSUserData sharedData];
    [dic setObject:user.useId forKey:@"user_id"];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"yyyy-MM-dd";
    NSString *plainText = [NSString stringWithFormat:@"%@%@",user.account,[formatter stringFromDate:[NSDate date]]];
    [dic setObject:[HLWJMD5 MD5ForLower32Bate:plainText] forKey:@"token"];
    if (self.cellAdress.length) {
        [dic setObject:@"1" forKey:@"object_type"];
    }else{
        [dic setObject:@"2" forKey:@"object_type"];
    }
    [dic setObject:self.articleId forKey:@"object_id"];
    [dic setObject:indexItem.id forKey:@"comment_id"];
    if([indexItem.has_like isEqualToString:@"Y"]){
        [dic setObject:@"unlike" forKey:@"action"];
    }else{
        [dic setObject:@"like" forKey:@"action"];

    }
    self.isLikeApi.requestParameter = dic;
    [self.isLikeApi startWithSuccess:^(YBNetworkResponse * _Nonnull response) {
        complete(YES, 0);
    } failure:^(YBNetworkResponse * _Nonnull response) {
        complete(NO, 0);
    }];
}
- (void)rqSendIndex:(FBSPinglunItem *)indexItem text:(NSString *)text block:(void(^)(BOOL success,NSInteger count))complete{
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    FBSUserData *user = [FBSUserData sharedData];
    [dic setObject:user.useId forKey:@"user_id"];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"yyyy-MM-dd";
    NSString *plainText = [NSString stringWithFormat:@"%@%@",user.account,[formatter stringFromDate:[NSDate date]]];
    [dic setObject:[HLWJMD5 MD5ForLower32Bate:plainText] forKey:@"token"];
    if (self.cellAdress.length) {
        [dic setObject:@"1" forKey:@"object_type"];
    }else{
        [dic setObject:@"2" forKey:@"object_type"];
    }
    [dic setObject:self.articleId forKey:@"object_id"];
    if (indexItem == nil) {
        [dic setObject:@"0" forKey:@"pid"];
    }else{
        [dic setObject:indexItem.id forKey:@"pid"];
    }
    [dic setObject:text forKey:@"content"];
    self.sendApi.requestParameter = dic;
    [self.sendApi startWithSuccess:^(YBNetworkResponse * _Nonnull response) {
        complete(YES, 0);
    } failure:^(YBNetworkResponse * _Nonnull response) {
        complete(NO, 0);
    }];
}
- (void)rqfans:(FBSPinglunItem *)indexItem block:(void(^)(BOOL success,NSInteger count))complete{
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    FBSUserData *user = [FBSUserData sharedData];
    [dic setObject:user.useId forKey:@"user_id"];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"yyyy-MM-dd";
    NSString *plainText = [NSString stringWithFormat:@"%@%@",user.account,[formatter stringFromDate:[NSDate date]]];
    [dic setObject:[HLWJMD5 MD5ForLower32Bate:plainText] forKey:@"token"];
    if (self.cellAdress.length) {
        if (self.WenZhangModel == nil) {
            complete(NO,0);
            return;
        }
        [dic setObject:@"1" forKey:@"object_type"];
        [dic setObject:self.WenZhangModel.data.title forKey:@"object_title"];
        if ([self.WenZhangModel.data.has_fav isEqualToString:@"N"]) {
            [dic setObject:@"add" forKey:@"action"];
        }else{
            [dic setObject:@"remove" forKey:@"action"];
        }
    }else{
        if (self.model == nil) {
            complete(NO,0);
            return;
        }
        [dic setObject:@"2" forKey:@"object_type"];
        [dic setObject:self.model.data.title forKey:@"object_title"];
        if ([self.model.data.has_fav isEqualToString:@"N"]) {
            [dic setObject:@"add" forKey:@"action"];
        }else{
            [dic setObject:@"remove" forKey:@"action"];
        }
    }
    [dic setObject:self.articleId forKey:@"object_id"];
    self.favs.requestParameter = dic;
    [self.favs startWithSuccess:^(YBNetworkResponse * _Nonnull response) {
        complete(YES, 0);
    } failure:^(YBNetworkResponse * _Nonnull response) {
        complete(NO, 0);
    }];
}
- (void)rqwenzhangLike:(FBSPinglunItem *)indexItem block:(void(^)(BOOL success,NSInteger count))complete{
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    FBSUserData *user = [FBSUserData sharedData];
    [dic setObject:user.useId forKey:@"user_id"];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"yyyy-MM-dd";
    NSString *plainText = [NSString stringWithFormat:@"%@%@",user.account,[formatter stringFromDate:[NSDate date]]];
    [dic setObject:[HLWJMD5 MD5ForLower32Bate:plainText] forKey:@"token"];
    if (self.cellAdress.length) {
        if (self.WenZhangModel == nil) {
            complete(NO,0);
            return;
        }
        [dic setObject:@"1" forKey:@"object_type"];
        [dic setObject:self.WenZhangModel.data.title forKey:@"object_title"];
        if ([self.WenZhangModel.data.has_like isEqualToString:@"N"]) {
            [dic setObject:@"like" forKey:@"action"];
        }else{
            [dic setObject:@"unlike" forKey:@"action"];
        }
    }else{
        if (self.model == nil) {
            complete(NO,0);
            return;
        }
        [dic setObject:@"2" forKey:@"object_type"];
        [dic setObject:self.model.data.title forKey:@"object_title"];
        if ([self.model.data.has_like isEqualToString:@"N"]) {
            [dic setObject:@"like" forKey:@"action"];
        }else{
            [dic setObject:@"unlike" forKey:@"action"];
        }
    }
    [dic setObject:self.articleId forKey:@"object_id"];
    
    [self.PinglunLikeCollectionHositoryModel rqLikeobject_type:dic[@"object_type"] object_id:dic[@"object_id"] action:dic[@"action"] object_title:dic[@"object_title"] block:^(BOOL success, NSInteger count) {
        if (success) {
            complete(YES,0);
        }else{
            complete(NO,0);
        }
    }];
}
- (void)createItems {
    [self.items removeAllObjects];
    
    FBSGroupHeadItem *headItem = [FBSGroupHeadItem item];
    headItem.title = @"你可能感兴趣";
    [self.items addObject:headItem];
    
    for (DetailInterestInfo *info in self.interstModel.data) {
        if (info.file_url.length) {
            FBSOneImageItem *oneImageItem = [FBSOneImageItem item];
            oneImageItem.detailType = DetailPageTypeArticle;
            oneImageItem.ID = info.ID;
            oneImageItem.content = info.title;
            oneImageItem.author = info.nickname;
            oneImageItem.imgUrl = info.file_url;
            oneImageItem.comment = @"";
            oneImageItem.time = [NSString publishedTimeFormatWithTimeStamp:info.updated_at];
            oneImageItem.cellHeight = WScale(95);
            [self.items addObject:oneImageItem];
            
        } else {
            FBSOnlyTextItem *textItem = [FBSOnlyTextItem item];
            textItem.detailType = DetailPageTypeArticle;
            textItem.ID = info.ID;
            textItem.isTop = NO;
            textItem.content = info.title;
            textItem.author = info.nickname;
            textItem.comment = @"";
            textItem.time = [NSString publishedTimeFormatWithTimeStamp:info.updated_at];
            textItem.cellHeight = [textItem calculateCellHeight];
            [self.items addObject:textItem];
        }
    }
    
    
    FBSSpaceItem *spaceItem = [FBSSpaceItem item];
    spaceItem.backgroundColor = Hexcolor(0xF6F7F9);
    spaceItem.cellHeight = WScale(10);
    [self.items addObject:spaceItem];
    
    for (FBSPinglunModelItemsItem *item in self.modelPinglun.data.items) {
        NSDictionary *dic = [item mj_keyValues];
        FBSPinglunItem *itemP = [[FBSPinglunItem alloc] init];
        itemP = [FBSPinglunItem mj_objectWithKeyValues:dic];
        [self.items addObject:itemP];
    }

}



#pragma mark - property

- (FBSGetRequest *)detailAPI {
    if (!_detailAPI) {
        _detailAPI = [[FBSGetRequest alloc] init];
        _detailAPI.requestURI = [NSString stringWithFormat:@"%@%@",self.htmlAdress,self.articleId];
        NSMutableDictionary *dic = [NSMutableDictionary dictionary];
        
        _detailAPI.requestURI = [NSString stringWithFormat:@"%@%@",self.htmlAdress,self.articleId];
        FBSUserData *user = [FBSUserData sharedData];
        [dic setObject:user.useId forKey:@"user_id"];
        _detailAPI.requestParameter = dic;
    }
    return _detailAPI;
}


- (FBSGetRequest *)interestAPI {
    if (!_interestAPI) {
        _interestAPI  = [[FBSGetRequest alloc] init];
        //interest/
        _interestAPI.requestURI = [NSString stringWithFormat:@"%@%@",self.cellAdress,self.articleId];
    }
    return _interestAPI;
}


- (NSDictionary *)itemTypeDic
{
    if (!_itemTypeDic) {
        _itemTypeDic = @{
                         @"1": [FBSOnlyTextItem item],
                         @"2": [FBSThreeImageItem item],
                         @"3": [FBSOneImageItem item],
                         @"4": [FBSOneImageItem item]
                         };
        
    }
    return _itemTypeDic;
}
- (NSMutableDictionary *)dataDic
{
    if (!_dataDic) {
        _dataDic = [NSMutableDictionary dictionary];
    }
    return _dataDic;
}
- (FBSPostRequest *)isLikeApi {
    if (!_isLikeApi) {
        _isLikeApi = [[FBSPostRequest alloc] init];
        _isLikeApi.requestURI = @"comment/like";
    }
    return _isLikeApi;
}
- (FBSGetRequest *)pinglunAPI {
    if (!_pinglunAPI) {
        _pinglunAPI = [[FBSGetRequest alloc] init];
        _pinglunAPI.requestURI = @"comment/list";
    }
    return _pinglunAPI;
}
- (FBSPostRequest *)favs {
    if (!_favs) {
        _favs = [[FBSPostRequest alloc] init];
        _favs.requestURI = @"user/favs";
    }
    return _favs;
}
- (FBSPostRequest *)sendApi {
    if (!_sendApi) {
        _sendApi = [[FBSPostRequest alloc] init];
        _sendApi.requestURI = @"comment/add";
    }
    return _sendApi;
}
- (FBSRecordPinglunLikeCollectionHositoryModel *)PinglunLikeCollectionHositoryModel
{
    if (!_PinglunLikeCollectionHositoryModel) {
        _PinglunLikeCollectionHositoryModel = [[FBSRecordPinglunLikeCollectionHositoryModel alloc] init];
    }
    return _PinglunLikeCollectionHositoryModel;
}
@end
