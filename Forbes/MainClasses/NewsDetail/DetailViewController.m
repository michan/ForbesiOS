//
//  DetailViewController.m
//  NatvieWebView
//
//  Created by hejianyuan on 2018/7/18.
//  Copyright © 2018年 ThinkCode. All rights reserved.
//

#import "DetailViewController.h"
#import "DetailViewModel.h"
#import <WebKit/WebKit.h>
#import "FBSActivityHeaderImageView.h"
#import "FBSActivityHeaderContenView.h"
#import "FBSarticleHeaderView.h"
#import "FBSActivityFooterView.h"
#import "FBSActivityListShopController.h"
#import "FBSArticalFooterView.h"
#import "FBSarticleNavView.h"
#import "FBSAuthorViewController.h"
#import "AdvertAopTableView.h"


#import <ShareSDK/ShareSDK.h>
#import <ShareSDKUI/ShareSDK+SSUI.h>

#import <ShareSDKUI/ShareSDKUI.h>
#import <ShareSDKUI/SSUIShareSheetConfiguration.h>
#import "FBSPinglunItem.h"
#import "FBSPinglunTableViewCell.h"
#import "XHInputView.h"
#import "FBSAttentAndPinglunViewController.h"
#import "FBSPingLunFooterView.h"
#import "FBSLoginViewController.h"
/**
 * 最上面有个View，WebView和TableView
 */
@interface DetailViewController ()<UIScrollViewDelegate,
WKNavigationDelegate,FBSActivityFooterViewDelegate,FBSArticalFooterViewDelegate,XHInputViewDelagete,FBSPingLunFooterViewDelegate>

@property (nonatomic, strong) WKWebView     *webView;

@property (nonatomic, strong) UIScrollView  *containerScrollView;

@property (nonatomic, strong) UIView        *contentView;

@property (nonatomic, strong) UIView        *topView;

@property (nonatomic, strong)AdvertAopTableView *aopDemo;

@property (nonatomic, strong) DetailViewModel *viewModel;

@property (nonatomic ,strong)ZWHTMLSDK *htmlSDK;
@property (nonatomic ,strong)FBSActivityHeaderImageView *HeaderImageView;
@property (nonatomic ,strong)FBSActivityHeaderContenView *HeaderContenView;
@property (nonatomic ,strong)FBSarticleHeaderView *HeaderUser;
@property (nonatomic ,strong)FBSarticleNavView *navHeaderUser;
@property (nonatomic ,assign) BOOL  canShare;
@property (nonatomic ,strong)FBSPingLunFooterView *footer;

@property (nonatomic ,strong)UIView *BaoMingFooterView;




@end

@implementation DetailViewController{
    CGFloat _lastWebViewContentHeight;
    CGFloat _lastTableViewContentHeight;
}
@synthesize viewModel = _viewModel;


- (instancetype)initWithArticleId:(NSString *)articleId htmlAddressL:(NSString *)html cellAddress:(NSString *)cell {
    self = [super init];
    if (self) {
        self.viewModel = [[DetailViewModel alloc] initWithArticleId:articleId htmlAddressL:html cellAddress:cell];
        if (cell.length) {
            self.canShare = YES;
        }
    }
    return self;
}
///广告初始化
- (void)adsInit {
    NSString *position_id = @"3";
    if (self.position_id.length) {
        position_id = self.position_id;
    }
    if (position_id) {
        self.aopDemo = [AdvertAopTableView new];
        self.aopDemo.position_id = position_id;
        self.aopDemo.vc = self;
    }
}
- (BOOL)panduanIsLogin{
    if(![FBSUserData sharedData].isLogin){
        FBSLoginViewController *loginVC = [[FBSLoginViewController alloc] init];
        loginVC.islogin = ^(BOOL islogin) {
            [self request];
        };
        [self.navigationController pushViewController:loginVC animated:YES];
        return NO;
    }
    return YES;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initValue];
    [self initView];
    [self addObservers];
    [self adsInit];
    
    
    WEAKSELF
    if (self.canShare) { //文章页支持分享
        FBSNavigationItem *rightItem = [[FBSNavigationItem alloc] init];
        rightItem.image = kImageWithName(@"video_more");
        rightItem.action = ^(FBSNavigationItem *item) {
            [weakSelf shareAction];
        };
        
        self.navigationBar.rightBarButtonItems = @[rightItem];
    }
    
//    NSString *path = @"https://www.jianshu.com/p/f31e39d3ce41";
//    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:path]];
//    request.cachePolicy = NSURLRequestReloadIgnoringCacheData;
//    [self.webView loadRequest:request];
    
    
    self.registerDictionary = @{
                                [FBSOneImageItem reuseIdentifier] : [FBSOneImageCell class],
                                [FBSThreeImageItem reuseIdentifier] : [FBSThreeImageCell class],
                                [FBSOnlyTextItem reuseIdentifier] : [FBSOnlyTextCell class],
                                [FBSGroupHeadItem reuseIdentifier] : [FBSGroupHeadCell class],
                                [FBSSpaceItem reuseIdentifier] : [FBSSpaceCell class],
                                 [FBSPinglunItem reuseIdentifier] : [FBSPinglunTableViewCell class]
                                };
    
    self.tableView.bounces = NO;
    [self request];
    [self.view addSubview:self.footer];
    self.footer.delegate = self;
    [self.footer mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.view);
        if (IPHONE_X) {
            make.height.mas_equalTo(WScale(kDiffTabBarH + 45));
        }else{
            make.height.mas_equalTo(WScale(45));
        }
        make.bottom.mas_equalTo(self.view);
    }];
}
- (void)request{
    WEAKSELF;
    if(self.viewModel.cellAdress.length){
        [self showHUD];
        [self.viewModel rqArticleInterest:^(BOOL success) {
            [weakSelf hideHUD];
            self.aopDemo.soft = @"0";
            self.aopDemo.softArray = @[@"0",[NSString stringWithFormat:@"%ld",self.viewModel.items.count+1]];
            self.aopDemo.aopUtils = self.tableView.aop_utils;
            [weakSelf.tableView reloadData];
            
        }];
    }else{
        [self showHUD];
        [self.viewModel rqContent:^(BOOL success, NSInteger count) {
            [weakSelf hideHUD];
            [weakSelf.tableView reloadData];
        }];
    }
    
    [self.viewModel rqArticleDetail:^(BOOL success,NSString *bodyHTML) {
        [weakSelf changeSelectCollect];
        [weakSelf changeSelectLike];
        if (weakSelf.viewModel.cellAdress.length) {
            [weakSelf createViewAticle];
        }else{
            [weakSelf createView];
        }
        

        
        [weakSelf.webView loadHTMLString:[weakSelf getNewsDetailHTML:bodyHTML] baseURL:[NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"iPhone" ofType:@"css"]]];
        
        
        
        
    }];
}
- (void)changeHas_fav{
    if (self.viewModel.cellAdress.length) {
        if ([self.viewModel.WenZhangModel.data.has_fav isEqualToString:@"Y"]) {
            self.viewModel.WenZhangModel.data.has_fav = @"N";
        }else{
            self.viewModel.WenZhangModel.data.has_fav = @"Y";
        }
    }else{
        if ([self.viewModel.model.data.has_fav isEqualToString:@"Y"]) {
            self.viewModel.model.data.has_fav = @"N";
        }else{
            self.viewModel.model.data.has_fav = @"Y";
        }
    }
}
- (void)changeHas_Like{
    if (self.viewModel.cellAdress.length) {
        if ([self.viewModel.WenZhangModel.data.has_like isEqualToString:@"Y"]) {
            self.viewModel.WenZhangModel.data.has_like = @"N";
        }else{
            self.viewModel.WenZhangModel.data.has_like = @"Y";
        }
    }else{
        if ([self.viewModel.model.data.has_like isEqualToString:@"Y"]) {
            self.viewModel.model.data.has_like = @"N";
        }else{
            self.viewModel.model.data.has_like = @"Y";
        }
    }
}
- (void)changeSelectCollect{
    if (self.viewModel.cellAdress.length) {
        if ([self.viewModel.WenZhangModel.data.has_fav isEqualToString:@"Y"]) {
            [self.footer changeSelectCollection:YES];
        }else{
            [self.footer changeSelectCollection:NO];
        }
    }else{
        if ([self.viewModel.model.data.has_fav isEqualToString:@"Y"]) {
            [self.footer changeSelectCollection:YES];
        }else{
            [self.footer changeSelectCollection:NO];
        }
    }
}
- (void)changeSelectLike{
    if (self.viewModel.cellAdress.length) {
        if ([self.viewModel.WenZhangModel.data.has_like isEqualToString:@"Y"]) {
            [self.footer changeSelectLike:YES];
        }else{
            [self.footer changeSelectLike:NO];
        }
    }else{
        if ([self.viewModel.model.data.has_like isEqualToString:@"Y"]) {
            [self.footer changeSelectLike:YES];
        }else{
            [self.footer changeSelectLike:NO];
        }
    }
}
- (void)click{
    [XHInputView showWithStyle:0 configurationBlock:^(XHInputView *inputView) {
        /** 请在此block中设置inputView属性 */
        
        /** 代理 */
        inputView.delegate = self;
        
        /** 占位符文字 */
        inputView.placeholder = @"请输入评论文字...";
        /** 设置最大输入字数 */
        inputView.maxCount = 50;
        /** 输入框颜色 */
        inputView.textViewBackgroundColor = [UIColor groupTableViewBackgroundColor];
        
        /** 更多属性设置,详见XHInputView.h文件 */
        
    } sendBlock:^BOOL(NSString *text) {
        if(text.length){
            NSLog(@"输入的信息为:%@",text);
            [self showHUD];
            [self.viewModel rqSendIndex:nil text:text block:^(BOOL success, NSInteger count) {
                [self hideHUD];
                if (success) {
                    [self makeToast:@"提交成功,请等待审核"];
                }else{
                    [self makeToast:@"提交失败，请重试"];
                }
            }];
            return YES;//return YES,收起键盘
        }else{
            NSLog(@"显示提示框-请输入要评论的的内容");
            return NO;//return NO,不收键盘
        }
    }];
}
- (FBSPingLunFooterView *)footer{
    if (!_footer) {
        _footer = [[FBSPingLunFooterView alloc] init];
    }
    return _footer;
}
-(UIView*)BaoMingFooterView
{
    if (!_BaoMingFooterView) {
        _BaoMingFooterView = [[UIView alloc] init];
        
        
        
        
    }
    return _BaoMingFooterView;

}



- (void)createViewAticle{
    UILabel *label = [[UILabel alloc] init];
    [self.topView addSubview:label];
    label.textColor = Hexcolor(0x333333);
    label.text = self.viewModel.WenZhangModel.data.title;
    label.font = kLabelFontSize20;
    label.numberOfLines = 0;
    label.preferredMaxLayoutWidth = (kScreenWidth - WScale(20));
    [label setContentHuggingPriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisHorizontal];
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.topView).offset(WScale(10));
        make.right.mas_equalTo(self.topView).offset(WScale(-10));
        make.top.mas_equalTo(self.topView).offset(WScale(10));

    }];
    [self.topView addSubview:self.HeaderUser];
    [self.HeaderUser mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.topView);
        make.top.mas_equalTo(label.mas_bottom);
        make.height.mas_equalTo(WScale(54));
    }];
    [self.topView setContentHuggingPriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisHorizontal];
    [self.topView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.mas_equalTo(self.contentView);
        make.bottom.mas_equalTo(self.HeaderUser.mas_bottom);
    }];
    [self.HeaderUser setDataModel:self.viewModel.WenZhangModel.data];
    [self.topView layoutIfNeeded];
    [self.contentView layoutIfNeeded];
    self.webView.top = self.topView.height;
//    FBSArticalFooterView *footer = [[FBSArticalFooterView alloc] init];
//    footer.delegate = self;
//    [self.view addSubview:footer];
//    [footer mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.bottom.left.right.mas_equalTo(self.view);
//        make.height.mas_equalTo(WScale(45));
//    }];
    [self.navigationBar addSubview:self.navHeaderUser];
    [self.navHeaderUser mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.navigationBar).offset(kStatusBarH);
        make.bottom.mas_equalTo(self.navigationBar);
        make.left.mas_equalTo(self.navigationBar).offset(WScale(38));
        make.right.mas_equalTo(self.navigationBar).offset(WScale(-54));

    }];
    [self.navHeaderUser setDataModel:self.viewModel.WenZhangModel.data];
    self.navHeaderUser.alpha = 0;
}
-(void)actionBaomingBtn{
    if(![FBSUserData sharedData].isLogin){
        FBSLoginViewController *loginVC = [[FBSLoginViewController alloc] init];
        [self.navigationController pushViewController:loginVC animated:YES];
        return ;
    }

    
    FBSArticalDataModel *model =  self.viewModel.model.data;
    NSDictionary *dic = [model mj_keyValues];
    FBSActivitiItem *item = [FBSActivitiItem mj_objectWithKeyValues:dic];

    FBSActivityListShopController *vc = [[FBSActivityListShopController alloc] initWithArticleId:item.id];
    [self.navigationController pushViewController:vc animated:YES];

}



- (void)createView{
    [self.topView addSubview:self.HeaderImageView];
    [self.topView addSubview:self.HeaderContenView];
    [self.view addSubview:self.BaoMingFooterView];
    [self.BaoMingFooterView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(self.footer.mas_top);
        make.left.mas_equalTo(0);
        make.right.mas_equalTo(0);
        make.height.mas_equalTo(40);

    }];
    UIButton *BaoMingbtn = [[UIButton alloc]initWithFrame:CGRectMake((Main_Screen_Width - 100)/2, 0, 100, 40)];
    [BaoMingbtn setTitle:@"立即报名" forState:0];
    BaoMingbtn.titleLabel.font = [UIFont systemFontOfSize:13];
    [BaoMingbtn setTitleColor:[UIColor whiteColor] forState:0];
    ViewRadius(BaoMingbtn, 20);
    [_BaoMingFooterView addSubview:BaoMingbtn];
    BaoMingbtn.backgroundColor = Hexcolor(0x8C734B);
    [BaoMingbtn addTarget:self action:@selector(actionBaomingBtn) forControlEvents:1<<6];

    self.containerScrollView.mj_h = kScreenHeight - kNavTopMargin - WScale(45) - 40;
    
    self.BaoMingFooterView.backgroundColor = [UIColor whiteColor];

    [self.HeaderImageView setContentHuggingPriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisHorizontal];
    [self.HeaderImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.mas_equalTo(self.topView);
        make.bottom.mas_equalTo(self.HeaderContenView.mas_top);
    }];
    [self.HeaderContenView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.topView);
        make.top.mas_equalTo(self.HeaderImageView.mas_bottom);
        make.height.mas_equalTo(WScale(120));
    }];
    [self.HeaderImageView setDataModel:self.viewModel.model.data];
    [self.HeaderContenView setDataModel:self.viewModel.model.data];
    
    UILabel *label = [[UILabel alloc] init];
    [self.topView addSubview:label];
    label.textColor = Hexcolor(0x333333);
    label.text = @"活动信息";
    label.font = kLabelFontSize18;
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.topView).offset(WScale(13));
        make.right.mas_equalTo(self.topView).offset(WScale(-13));
        make.top.mas_equalTo(self.HeaderContenView.mas_bottom);
        make.height.mas_equalTo(WScale(71));
    }];
     [self.topView setContentHuggingPriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisHorizontal];
    [self.topView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.mas_equalTo(self.contentView);
        make.bottom.mas_equalTo(label.mas_bottom);
    }];
    [self.topView layoutIfNeeded];
    [self.contentView layoutIfNeeded];
    self.webView.top = self.topView.height;
//    FBSActivityFooterView *footer = [[FBSActivityFooterView alloc] init];
//    footer.delegate = self;
//    [self.view addSubview:footer];
//    [footer mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.right.mas_equalTo(self.view);
//        make.height.mas_equalTo(WScale(45));
//        make.top.mas_equalTo(self.tableView.mas_bottom);
//    }];

}
- (void)clickActivityFooterView:(FBSActivityFooterView *)view clickType:(clickType)type{
    if (type == clickTypeBaoming) {
        
        FBSArticalDataModel *model =  self.viewModel.model.data;
        NSDictionary *dic = [model mj_keyValues];
        FBSActivitiItem *item = [FBSActivitiItem mj_objectWithKeyValues:dic];
        FBSActivityListShopController *vc = [[FBSActivityListShopController alloc] initWithArticleId:item.id];
        [self.navigationController pushViewController:vc animated:YES];
    }
}

- (FBSActivityHeaderImageView *)HeaderImageView{
    if (!_HeaderImageView) {
        _HeaderImageView = [[FBSActivityHeaderImageView alloc] init];
    }
    return _HeaderImageView;
}
-(FBSActivityHeaderContenView *)HeaderContenView{
    if (!_HeaderContenView) {
        _HeaderContenView = [[FBSActivityHeaderContenView alloc] init];
    }
    return _HeaderContenView;
}
- (FBSarticleNavView *)navHeaderUser{
    if (!_navHeaderUser) {
        _navHeaderUser = [[FBSarticleNavView alloc] init];
        UITapGestureRecognizer *tapGes = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAuthorAction:)];
        [_navHeaderUser addGestureRecognizer:tapGes];

    }
    return _navHeaderUser;
}
- (FBSarticleHeaderView *)HeaderUser{
    if (!_HeaderUser) {
        _HeaderUser = [[FBSarticleHeaderView alloc] init];
//        _HeaderUser.attent.hidden = NO;
        [_HeaderUser.attent addTarget:self action:@selector(actionattent) forControlEvents:1<<6];
        UITapGestureRecognizer *tapGes = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAuthorAction:)];
        [_HeaderUser addGestureRecognizer:tapGes];
    }
    return _HeaderUser;
}
-(void)actionattent{
    // 判断是否登录
    if (![self panduanIsLogin]) {
        return;
    }

    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"yyyy-MM-dd";
    NSString *plainText = [NSString stringWithFormat:@"%@%@",[FBSUserData sharedData].account,[formatter stringFromDate:[NSDate date]]];
    NSDictionary * parameters =@{
        
        @"user_id":[FBSUserData sharedData].useId?:@"",
        @"token":[HLWJMD5 MD5ForLower32Bate:plainText]?:@"",
        @"follow_user_id":self.viewModel.WenZhangModel.data.user_id?:@"",
        @"action":@"follow"
        
    };
    [self showHUD];
    
    [MCNetworking ResponseNetworkPOST_API:@"user/follow" parameters:parameters TheServer:0  cachePolicy:NO ISNeedLogin:YES RequestEnd:^(id  _Nonnull EndObject) {
        [self hideHUD];

    } MCHttpCacheData:^(id  _Nonnull CacheObject) {
        
    } success:^(id  _Nonnull responseObject) {
        NSLog(@"responseObject == %@",responseObject);
        
        [self makeToast:@"关注成功"];
//        _HeaderUser.attent.selected = YES;
        
    } failure:^(NSURLSessionDataTask * _Nonnull operation, NSError * _Nonnull error, id  _Nonnull responseObject, NSString * _Nonnull description) {
        [self makeToast:@"关注失败"];

    }];
    
    
}
//添加跳转作者主页
- (void)tapAuthorAction:(UIGestureRecognizer *)ges {
    if (self.viewModel.WenZhangModel.data) {
        FBSAuthorViewController *authorVC = [[FBSAuthorViewController alloc] initWithAuthorId:self.viewModel.WenZhangModel.data.user_id];
        [self.navigationController pushViewController:authorVC animated:YES];
    }
}

- (void)dealloc{
    [self removeObservers];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)initValue{
    _lastWebViewContentHeight = 0;
    _lastTableViewContentHeight = 0;
    self.tableView.scrollEnabled = NO;
}

- (void)initView{
    
    [self.contentView addSubview:self.topView];
    [self.contentView addSubview:self.webView];
    [self.contentView addSubview:self.tableView];
    
    [self.view addSubview:self.containerScrollView];
    [self.containerScrollView addSubview:self.contentView];
    
    self.contentView.frame = CGRectMake(0, 0, self.view.width, self.view.height * 2);
//    if(self.oneImageItem){
//        self.topView.frame = CGRectMake(0, 0, self.view.width, WScale(54));
//
//    }else{
//        self.topView.frame = CGRectMake(0, 0, self.view.width, WScale(379 + 120 + 71));
//    }
//
//    self.webView.top = self.topView.height;
//    self.webView.height = self.view.height;
//    self.tableView.top = self.webView.bottom;
    [self.tableView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.top.mas_offset(self.webView.bottom);
        
    }];
}



- (void)shareAction {
    FBSLog(@"%@",@"点击了分享");

    if (!self.viewModel.WenZhangModel.data.title.length) {
        [self makeToast:@"文章加载中"];
        return;
    }
    
    id images = nil;
    if (self.viewModel.WenZhangModel.data.file_url.length) {
        images = self.viewModel.WenZhangModel.data.file_url;
    } else {
        images = [UIImage imageNamed:@"applogo"];
    }
    
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params SSDKSetupShareParamsByText:self.viewModel.WenZhangModel.data.Description ?: @""
                                images:images
                                   url:[NSURL URLWithString:self.viewModel.WenZhangModel.data.share_link ?:@""]
                                 title:self.viewModel.WenZhangModel.data.title ?:@""
                                  type:SSDKContentTypeAuto];
    
    
    // 定制新浪微博的分享内容
    [params SSDKSetupSinaWeiboShareParamsByText:self.viewModel.WenZhangModel.data.Description ?: @""
                                               title:self.viewModel.WenZhangModel.data.title ?:@""
                                              images:images
                                               video:nil
                                                 url:[NSURL URLWithString:self.viewModel.WenZhangModel.data.share_link ?:@""]
                                            latitude:0
                                           longitude:0
                                            objectID:0
                                      isShareToStory:false
                                                type:SSDKContentTypeWebPage];
    
    //直接分享,无UI
//    [ShareSDK share:SSDKPlatformTypeWechat
//         parameters:params
//     onStateChanged:^(SSDKResponseState state, NSDictionary *userData,
//                      SSDKContentEntity *contentEntity, NSError *error) {
//         switch (state) {
//             case SSDKResponseStateSuccess:
//                 NSLog(@"分享成功成功");//成功
//                 break;
//             case SSDKResponseStateFail:
//             {
//                 NSLog(@"分享失败--%@",error.description);
//                 //失败
//                 break;
//             }
//             case SSDKResponseStateCancel:
//                 //取消
//                 break;
//
//             default:
//                 break;
//         }
//     }];
    
    
    
//    SSUIShareSheetConfiguration *config = [[SSUIShareSheetConfiguration alloc] init];
//
//    //设置分享菜单为简洁样式
//    config.style = SSUIActionSheetStyleSimple;
//
//    //设置竖屏有多少个item平台图标显示
//    config.columnPortraitCount = 4;
//
//    //设置横屏有多少个item平台图标显示
//    config.columnLandscapeCount = 4;
//
//    //设置取消按钮标签文本颜色
//    config.cancelButtonTitleColor = [UIColor redColor];
//
//    //设置对齐方式（简约版菜单无居中对齐）
//    config.itemAlignment = SSUIItemAlignmentLeft;
//
//    //设置标题文本颜色
//    config.itemTitleColor = [UIColor greenColor];
//
//    //设置分享菜单栏状态栏风格
//    config.statusBarStyle = UIStatusBarStyleLightContent;
//
//    //设置支持的页面方向（单独控制分享菜单栏）
//    config.interfaceOrientationMask = UIInterfaceOrientationMaskPortrait|UIInterfaceOrientationMaskLandscape;
//
//    //设置分享菜单栏的背景颜色
//    config.menuBackgroundColor = [UIColor whiteColor];
//
//    //取消按钮是否隐藏，默认不隐藏
//    config.cancelButtonHidden = NO;
//
    //设置直接分享的平台（不弹编辑界面）
//    config.directSharePlatforms = @[@(SSDKPlatformTypeSinaWeibo),@(SSDKPlatformTypeTwitter)];

    WEAKSELF
    
    //带下拉框UI分享
    NSArray *items = @[
                       @(SSDKPlatformSubTypeWechatSession),
                       @(SSDKPlatformSubTypeWechatTimeline),
                       @(SSDKPlatformSubTypeQQFriend),
                       @(SSDKPlatformSubTypeQZone),
                       @(SSDKPlatformTypeSinaWeibo)
                       ];
    
    [ShareSDK showShareActionSheet:nil
                       customItems:items
                       shareParams:params
                sheetConfiguration:nil
                    onStateChanged:^(SSDKResponseState state,
                                     SSDKPlatformType platformType,
                                     NSDictionary *userData,
                                     SSDKContentEntity *contentEntity,
                                     NSError *error,
                                     BOOL end)
     {
         
         switch (state) {
                 
             case SSDKResponseStateSuccess:
                 NSLog(@"成功");//成功
                 [weakSelf makeToast:@"分享成功"];
                 break;
             case SSDKResponseStateFail:
             {
                 NSLog(@"--%@",error.description);
                 [weakSelf makeToast:@"分享失败"];
                 //失败
                 break;
             }
             case SSDKResponseStateCancel:
//                 [weakSelf makeToast:@"分享取消"];
                 break;
             default:
                 break;
         }
     }];
}
#pragma mark - footerDelegate
- (void)clickPinlunFooterView:(FBSPingLunFooterView *)view clickType:(PingLunType)type
{
    // 分享单独处理
    if (type == PingLunTypeShare){
        [self shareAction];
        return;
    }
    // 判断是否登录
    if (![self panduanIsLogin]) {
        return;
    }
    if (type == PingLunTypeSay) {
        [self click];
    }else if (type == PingLunTypelook){
        [self showHUD];
        WEAKSELF;
        [self.viewModel rqwenzhangLike:nil block:^(BOOL success, NSInteger count) {
            [weakSelf hideHUD];
            if (success) {
                [weakSelf makeToast:@"成功"];
                [weakSelf changeHas_Like];
                [weakSelf changeSelectLike];
            }else{
                [weakSelf makeToast:@"失败，请重试"];
            }
        }];
    }else if (type == PingLunTypeCollect){
        [self showHUD];
        WEAKSELF;
        [self.viewModel rqfans:nil block:^(BOOL success, NSInteger count) {
            [weakSelf hideHUD];
            if (success) {
                [weakSelf makeToast:@"成功"];
                [weakSelf changeHas_fav];
                [weakSelf changeSelectCollect];
            }else{
                [weakSelf makeToast:@"失败，请重试"];
            }
        }];
    }
}
#pragma mark - WKNavigationDelegate

- (void)webView:(WKWebView *)webView decidePolicyForNavigationAction:(nonnull WKNavigationAction *)navigationAction decisionHandler:(nonnull void (^)(WKNavigationActionPolicy))decisionHandler
{
    decisionHandler(WKNavigationActionPolicyAllow);
    [self.htmlSDK zw_handlePreviewImageRequest:navigationAction.request];
}
- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation
{


//    [webView evaluateJavaScript:@"document.getElementsByTagName('body')[0].style.webkitTextSizeAdjust= '110%'" completionHandler:nil];

    //通过js获取htlm中图片url
    self.htmlSDK = [ZWHTMLSDK zw_loadBridgeJSWebview:webView];
}

#pragma mark - Observers
- (void)addObservers{
    [self.webView addObserver:self forKeyPath:@"scrollView.contentSize" options:NSKeyValueObservingOptionNew context:nil];
    [self.tableView addObserver:self forKeyPath:@"contentSize" options:NSKeyValueObservingOptionNew context:nil];
}

- (void)removeObservers{
    [self.webView removeObserver:self forKeyPath:@"scrollView.contentSize"];
    [self.tableView removeObserver:self forKeyPath:@"contentSize"];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context{
    if (object == _webView) {
        if ([keyPath isEqualToString:@"scrollView.contentSize"]) {
            [self updateContainerScrollViewContentSize:0 webViewContentHeight:0];
        }
    }else if(object == self.tableView) {
        if ([keyPath isEqualToString:@"contentSize"]) {
            [self updateContainerScrollViewContentSize:0 webViewContentHeight:0];
        }
    }
}

- (void)updateContainerScrollViewContentSize:(NSInteger)flag webViewContentHeight:(CGFloat)inWebViewContentHeight{
    
    CGFloat webViewContentHeight = flag==1 ?inWebViewContentHeight :self.webView.scrollView.contentSize.height;
    CGFloat tableViewContentHeight = self.tableView.contentSize.height;
    
    if (webViewContentHeight == _lastWebViewContentHeight && tableViewContentHeight == _lastTableViewContentHeight) {
        return;
    }
    
    _lastWebViewContentHeight = webViewContentHeight;
    _lastTableViewContentHeight = tableViewContentHeight;
    
    self.containerScrollView.contentSize = CGSizeMake(self.view.width, self.webView.top + webViewContentHeight + tableViewContentHeight);
    
    CGFloat webViewHeight = (webViewContentHeight < self.view.height) ?webViewContentHeight :self.view.height ;
    CGFloat tableViewHeight = tableViewContentHeight < self.view.height ?tableViewContentHeight :self.view.height;
    self.webView.height = webViewHeight <= 0.1 ?0.1 :webViewHeight;
    self.contentView.height = self.webView.top +webViewHeight + tableViewHeight + WScale(45);
//    self.tableView.height = tableViewHeight;
//    self.tableView.top = self.webView.bottom;
    [self.tableView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.top.mas_offset(self.webView.bottom);
        make.left.mas_offset(self.webView.left);
        make.height.mas_equalTo(tableViewHeight);
        make.width.mas_equalTo(self.view);
    }];
    //Fix:contentSize变化时需要更新各个控件的位置
    [self scrollViewDidScroll:self.containerScrollView];
}

#pragma mark - UIScrollViewDelegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    if (_containerScrollView != scrollView) {
        return;
    }
    if (scrollView.contentOffset.y >= WScale(140) && scrollView.contentOffset.y <= WScale(180)) {

        self.navHeaderUser.alpha = (scrollView.contentOffset.y - WScale(140)) / WScale(40);
    }else if(scrollView.contentOffset.y < WScale(140)){
        self.navHeaderUser.alpha = 0;
    }else if (scrollView.contentOffset.y >WScale(180)){
        self.navHeaderUser.alpha = 1;
    }
    CGFloat offsetY = scrollView.contentOffset.y;
    
    CGFloat webViewHeight = self.webView.height;
    CGFloat tableViewHeight = self.tableView.height;
    
    CGFloat webViewContentHeight = self.webView.scrollView.contentSize.height;
    CGFloat tableViewContentHeight = self.tableView.contentSize.height;
    //CGFloat topViewHeight = self.topView.height;
    CGFloat webViewTop = self.webView.top;
    
    CGFloat netOffsetY = offsetY - webViewTop;
    
    if (netOffsetY <= 0) {
        self.contentView.top = 0;
        self.webView.scrollView.contentOffset = CGPointZero;
        self.tableView.contentOffset = CGPointZero;
    }else if(netOffsetY  < webViewContentHeight - webViewHeight){
        self.contentView.top = netOffsetY;
        self.webView.scrollView.contentOffset = CGPointMake(0, netOffsetY);
        self.tableView.contentOffset = CGPointZero;
    }else if(netOffsetY < webViewContentHeight){
        self.contentView.top = webViewContentHeight - webViewHeight;
        self.webView.scrollView.contentOffset = CGPointMake(0, webViewContentHeight - webViewHeight);
        self.tableView.contentOffset = CGPointZero;
    }else if(netOffsetY < webViewContentHeight + tableViewContentHeight - tableViewHeight){
        self.contentView.top = offsetY - webViewHeight - webViewTop;
        self.tableView.contentOffset = CGPointMake(0, offsetY - webViewContentHeight - webViewTop);
        self.webView.scrollView.contentOffset = CGPointMake(0, webViewContentHeight - webViewHeight);
    }else if(netOffsetY <= webViewContentHeight + tableViewContentHeight ){
        self.contentView.top = self.containerScrollView.contentSize.height - self.contentView.height;
        self.webView.scrollView.contentOffset = CGPointMake(0, webViewContentHeight - webViewHeight);
        self.tableView.contentOffset = CGPointMake(0, tableViewContentHeight - tableViewHeight);
    }else {
        //do nothing
        NSLog(@"do nothing");
    }
}


- (NSString *)getNewsDetailHTML:(NSString *)bodyHTML {
    if ([bodyHTML isKindOfClass:[NSNull class]]||!bodyHTML.length) {
        return @"";
    }
    
    NSMutableString *fullHTML = [[NSMutableString alloc] init];
    
    //head
    NSString *head = @"<head>"
    "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, user-scalable=no\"> "
    "<style>html{padding:15px;} body{word-wrap:break-word;font-size:17px;padding:0px;margin:0px} p{padding:0px;margin:0px;font-size:17px;color:#333333;line-height:1.8;} img{padding:0px,margin:0px;max-width:100%; width:auto; height:auto;}</style>"
    "</head>";
    
    //结构
    [fullHTML appendString:@"<html>"];
    [fullHTML appendString:head];
    [fullHTML appendString:@"<body>"];
    if (![bodyHTML isKindOfClass:[NSNull class]] && bodyHTML.length) {
//        [fullHTML appendString:@"<div style='Width:148px; height: 70px; opacity: 1;background: rgba(FF,FF,FF,1);font-size: 26px;color: rgba(74,74,74,1);line-height: 70px;letter-spacing: 0.92px;'>活动信息</div>"];
        [fullHTML appendString:bodyHTML];
    }
    [fullHTML appendString:@"</body>"];
    [fullHTML appendString:@"</html>"];
    
//    //我们在header标签里加上meta标签（大小关键是minimum-scale=1.1，数值越大，字体越大）
//    NSString *htmlHeader = @"<header><meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=3.0, minimum-scale=2.1, user-scalable=no'></header>";
//    [htmlHeader stringByAppendingString:fullHTML];

    
    return [fullHTML copy];
}


#pragma mark - FBSCellDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    //    tableView.rowHeight = UITableViewAutomaticDimension;
    //    tableView.estimatedRowHeight = 100;
    //    return tableView.rowHeight;
    FBSItem *item  = [self.viewModel itemAtRow:indexPath.row inSection:indexPath.section];
    if ([item isKindOfClass:[FBSPinglunItem class]]) {
        FBSPinglunItem *pinglunItem = (FBSPinglunItem *)item;
        [pinglunItem updateTab];
        return pinglunItem.allHeight;
    }
    
    return item.cellHeight;
}
- (void)fbsCell:(FBSCell *)cell didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if ([cell isKindOfClass:[FBSMainCell class]]) {
        FBSMainItem *item = (FBSMainItem *)cell.item;
        
        DetailViewController *detailVC = [[DetailViewController alloc] initWithArticleId:item.ID htmlAddressL:@"article/" cellAddress:@"interest/"];
        [self.navigationController pushViewController:detailVC animated:YES];
    }
}
- (void)fbsCell:(FBSCell *)cell didClickButtonAtIndex:(NSInteger)index
{
    if ([cell isKindOfClass:[FBSPinglunTableViewCell class]]) {
        FBSPinglunTableViewCell *pinglunCell = (FBSPinglunTableViewCell *)cell;
        FBSPinglunItem *item = (FBSPinglunItem *)cell.item;
        if (index == 0) {
            // 判断是否登录
            if (![self panduanIsLogin]) {
                return;
            }
            WEAKSELF
            [self showHUD];
            [self.viewModel rqLikeIndex:item block:^(BOOL success, NSInteger count) {
                [pinglunCell changeLike];
                [weakSelf hideHUD];
            }];
        }else if (index == 1){
            [self shareAction];
        }else if (index == 3){
            // 判断是否登录
            if (![self panduanIsLogin]) {
                return;
            }
            FBSAttentAndPinglunViewController *vc = [[FBSAttentAndPinglunViewController alloc] init];
            vc.pinglunItem = (FBSPinglunItem *)cell.item;
            [self.navigationController pushViewController:vc animated:YES];
        }
    }
}
- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self.tableView reloadData];
}

#pragma mark - private

- (WKWebView *)webView{
    if (_webView == nil) {
        WKWebViewConfiguration *configuration = [[WKWebViewConfiguration alloc] init];
        _webView = [[WKWebView alloc] initWithFrame:self.view.bounds configuration:configuration];
        _webView.scrollView.scrollEnabled = NO;
        _webView.navigationDelegate = self;
        //
        
    }
    
    return _webView;
}

- (UIScrollView *)containerScrollView{
    if (_containerScrollView == nil) {
        _containerScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, kNavTopMargin, kScreenWidth, kScreenHeight - kNavTopMargin)];
        _containerScrollView.delegate = self;
        _containerScrollView.alwaysBounceVertical = YES;
    }
    
    return _containerScrollView;
}

- (UIView *)contentView{
    if (_contentView == nil) {
        _contentView = [[UIView alloc] init];
    }
    
    return _contentView;
}

- (UIView *)topView{
    if (_topView == nil) {
        _topView = [[UIView alloc] init];
        _topView.backgroundColor = Hexcolor(0xFFFFFF);
    }
    
    return _topView;
}

@end
