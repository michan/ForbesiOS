//
//  DetailViewController.h
//  NatvieWebView
//
//  Created by hejianyuan on 2018/7/18.
//  Copyright © 2018年 ThinkCode. All rights reserved.
//

#import "FBSTableViewController.h"
#import "FBSActivitiItem.h"
#import "FBSOneImageCell.h"

@interface DetailViewController : FBSTableViewController

- (instancetype)initWithArticleId:(NSString *)articleId htmlAddressL:(NSString *)html cellAddress:(NSString *)cell;

@property(nonatomic, strong)NSString *position_id;

@end
