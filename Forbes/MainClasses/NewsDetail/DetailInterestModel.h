//
//  DetailInterestModel.h
//  Forbes
//
//  Created by 周灿华 on 2019/9/23.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSBaseModel.h"

@class DetailInterestInfo;
@interface DetailInterestModel : FBSBaseModel
@property (nonatomic, strong) NSArray<DetailInterestInfo *> *data;
@end

@interface DetailInterestInfo : NSObject
@property (nonatomic, copy) NSString *ID;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *user_id;
@property (nonatomic, copy) NSString *author;
@property (nonatomic, copy) NSString *nickname;
@property (nonatomic, copy) NSString *file_url;
@property (nonatomic, copy) NSString *time;
@property (nonatomic, copy) NSString *updated_at;

@end

