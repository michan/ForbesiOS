//
//  DetailInterestModel.m
//  Forbes
//
//  Created by 周灿华 on 2019/9/23.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "DetailInterestModel.h"

@implementation DetailInterestModel

+ (NSDictionary *)mj_objectClassInArray {
    return @{@"data" : @"DetailInterestInfo"};
}

@end



@implementation DetailInterestInfo

+ (NSDictionary *)mj_replacedKeyFromPropertyName {
    return @{@"ID" : @"id"};
}


@end
