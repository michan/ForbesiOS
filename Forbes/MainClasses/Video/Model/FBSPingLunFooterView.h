//
//  FBSPingLunFooterView.h
//  Forbes
//
//  Created by 赵志辉 on 2019/11/19.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
typedef enum : NSUInteger {
    PingLunTypeSay = 200000,
    PingLunTypelook = 200001,
    PingLunTypeCollect = 200002,
    PingLunTypeShare = 200003
} PingLunType;
@class FBSPingLunFooterView;
@protocol FBSPingLunFooterViewDelegate <NSObject>
- (void)clickPinlunFooterView:(FBSPingLunFooterView *)view clickType:(PingLunType)type;
@end
@interface FBSPingLunFooterView : UIView
@property(nonatomic,weak)id<FBSPingLunFooterViewDelegate> delegate;
- (void)changeSelectCollection:(BOOL)select;
- (void)changeSelectLike:(BOOL)select;

@end

NS_ASSUME_NONNULL_END
