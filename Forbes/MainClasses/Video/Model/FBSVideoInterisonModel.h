//
//  FBSVideoInterisonModel.h
//  Forbes
//
//  Created by 赵志辉 on 2019/11/18.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import "FBSBaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface FBSVideoInterisonModelItemsItem :NSObject
@property (nonatomic , copy) NSString              * id;
@property (nonatomic , copy) NSString              * category_id;
@property (nonatomic , copy) NSString              * sort;
@property (nonatomic , copy) NSString              * title;
@property (nonatomic , copy) NSString              * file_img;
@property (nonatomic , copy) NSString              * created_at;
@property (nonatomic , copy) NSString              * file_url;

@end


@interface FBSVideoInterisonModelData :NSObject
@property (nonatomic , copy) NSString              * limit;
@property (nonatomic , strong) NSArray <FBSVideoInterisonModelItemsItem *>              * items;

@end


@interface FBSVideoInterisonModel :NSObject
@property (nonatomic , strong) FBSVideoInterisonModelData              * data;

@end

NS_ASSUME_NONNULL_END
