//
//  FBSVideoModel.h
//  Forbes
//
//  Created by 周灿华 on 2019/9/7.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import <Foundation/Foundation.h>

@class FBSVideoData,FBSVideoInfo;
@interface FBSVideoModel : FBSBaseModel
@property (nonatomic, strong) FBSVideoData *data;
@end



@interface FBSVideoData : NSObject
@property (nonatomic, copy) NSString *limit;
@property (nonatomic, copy) NSString *page;
@property (nonatomic, copy) NSString *total;
@property (nonatomic, strong) NSArray<FBSVideoInfo *> *items;
@end


@interface FBSVideoInfo : NSObject
@property (nonatomic, copy) NSString *id;
@property(nonatomic, copy) NSString *category_id;
@property (nonatomic, copy) NSString *sort;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *file_img;
@property (nonatomic, copy) NSString *created_at;
@property (nonatomic, copy) NSString *file_url;
@property (nonatomic, copy) NSString *has_like;
@property (nonatomic, copy) NSString *has_fav;
@end
