
//
//  FBSVideoViewController.m
//  Forbes
//
//  Created by 周灿华 on 2019/7/24.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSVideoViewController.h"
#import "FBSHomeViewModel.h"
#import "FBSHomeListViewController.h"
#import "FBSFollowViewController.h"
#import "FBSKeyNewsViewController.h"
#import "FBSBrandvoiceViewController.h"
#import "FBSSearchViewController.h"
#import "FBSChannelConfigController.h"

#import <WMPageController.h>
#import "FBSChannelVideoManager.h"

#import "FBSPlusView.h"
#import "FBSNavView.h"
#import "FBSVideoListViewController.h"



typedef void(^block1)(NSString *num);
@interface FBSVideoViewController ()<
WMPageControllerDelegate,
WMPageControllerDataSource,
FBSChannelConfigControllerDelegate
>
@property (nonatomic, strong) FBSHomeViewModel *viewModel;
@property (nonatomic, strong) WMPageController *pageController;
@property (nonatomic, strong) UIView *pageContainerView;
@property (nonatomic, strong) NSArray *enableTitles;
@property (nonatomic, strong) NSArray *disableTitles;

@end

@implementation FBSVideoViewController
@synthesize viewModel = _viewModel;


- (void)viewDidLoad {
    [super viewDidLoad];
    [self initNavBar];
    
    [self.pageContainerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.navigationBar.mas_bottom);
        make.bottom.left.right.mas_equalTo(0);
    }];
    
    [self.pageController.view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.mas_equalTo(0);
        make.width.mas_equalTo(kScreenWidth);
        make.height.mas_equalTo(self.pageContainerView);
    }];
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

#pragma mark - public method
- (void)setupChannels {
    self.view.backgroundColor = [UIColor whiteColor];
    
    WEAKSELF
    [[FBSChannelVideoManager sharedManager] fetchChannels:^(NSArray<NSString *> *enableTitles, NSArray<NSString *> *disableTitles) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [weakSelf reloadMenu:enableTitles disableTitles:disableTitles];
        });
    }];
}

#pragma mark - private method

- (void)initNavBar {
    self.navigationBackButtonHidden = YES;
    self.navigationBar.titleView.hidden = YES;
    self.navigationBar.bottomLine.hidden = YES;
    
    FBSNavView *navView = [[FBSNavView alloc] initWithFrame:CGRectZero];
    navView.placeholder = @"30 under 30";
    navView.liveAction = ^{
        FBSLog(@"点击了直播");
        [[FBSliveAuthorization sharedInstance] liveAuthorization:self];

    };
    
    WEAKSELF
    navView.searchAction = ^{
        FBSLog(@"点击了搜索");
        FBSSearchViewController *searchVC = [[FBSSearchViewController alloc] init];
        [weakSelf.navigationController pushViewController:searchVC animated:YES];
        
    };
    
    [self.navigationBar addSubview:navView];
    [navView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(0);
    }];
}

- (void)reloadMenu:(NSArray *)enableTitles
     disableTitles:(NSArray *)disableTitles {
    NSMutableArray *enableTitles2 = [NSMutableArray arrayWithArray:enableTitles];
    [enableTitles2 insertObject:@"推荐" atIndex:0];
    self.enableTitles = enableTitles2;
    self.disableTitles = disableTitles;
    
    [self.pageController reloadData];
    
    FBSPlusView *rightView = [[FBSPlusView alloc] initWithFrame:CGRectMake(0, 0, 40, 44)];
    WEAKSELF
    rightView.plusBlock = ^{
        [weakSelf showChannel];
    };
    self.pageController.menuView.rightView = rightView;
    self.pageController.menuView.rightView.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.5];
}


- (void)showChannel {
    FBSChannelConfigController *configVC = [[FBSChannelConfigController alloc] init];
    configVC.delegete = self;
    configVC.fixItemCount = 1;
    [self presentViewController:configVC animated:YES completion:^{
        FBSLog(@"%@",@"打开设置页完成");
    }];
}

#pragma mark - FBSChannelConfigControllerDelegate

- (NSArray *)enabledTitles:(FBSChannelConfigController *)channel {
    return self.enableTitles;
}

- (NSArray *)disabledTitles:(FBSChannelConfigController *)channel {
    return self.disableTitles;
}

- (void)channelViewController:(FBSChannelConfigController *)channel clickAtChannelTitle:(NSString *)channelTitle {
    FBSLog(@"点击了频道 %@",channelTitle);
    int channelIndex = (int)[self.enableTitles indexOfObject:channelTitle];
    
    [self.pageController setSelectIndex:channelIndex];
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (void)channelViewControllerClickToSave:(FBSChannelConfigController *)channel
                            enableTitles:(NSArray *)enableTitles
                           disableTitles:(NSArray *)disableTitles {
    FBSLog(@"点击了频道保存");
    [[FBSChannelVideoManager sharedManager] updateChannels:enableTitles disableTitles:disableTitles];
    
    self.enableTitles = enableTitles;
    self.disableTitles = disableTitles;
    
    [self.pageController reloadData];
}

#pragma mark - WMPageControllerDataSource

- (NSInteger)numbersOfChildControllersInPageController:(WMPageController *)pageController {
    return self.enableTitles.count;
}

- (NSString *)pageController:(WMPageController *)pageController titleAtIndex:(NSInteger)index {
    return [self.enableTitles objectAtIndex:index];
}

- (UIViewController *)pageController:(WMPageController *)pageController viewControllerAtIndex:(NSInteger)index {
    
    NSString *channelId = @"";
    if (index < self.enableTitles.count) {
        NSString *channelTitle = [self.enableTitles objectAtIndex:index];
        channelId = [[FBSChannelVideoManager sharedManager] channelIdWithTitle:channelTitle];
    }
    FBSVideoListViewController *vc  = [[FBSVideoListViewController alloc] initWithCatagery:channelId];
    return vc;
    
//    if ([channelId isEqualToString:KeyNewsChannelId]) {
//        FBSKeyNewsViewController *newsVC = [[FBSKeyNewsViewController alloc] init];
//        return newsVC;
//        
//    } else if ([channelId isEqualToString:BrandvoiceChannelId]) {  //品牌之声
//        FBSBrandvoiceViewController *vc = [[FBSBrandvoiceViewController alloc] initWithChannelId:BrandvoiceChannelId];
//        return vc;
//        
//    }  else { //其他频道
//        FBSHomeListViewController *vc = [[FBSHomeListViewController alloc] initWithChannelId:channelId];
//        return vc;
//    }
}

- (CGRect)pageController:(WMPageController *)pageController preferredFrameForMenuView:(WMMenuView *)menuView {
    CGFloat leftMargin = 0;
    CGFloat rightMargin = 0;
    CGFloat originY = 0;
    return CGRectMake(leftMargin, originY, pageController.view.frame.size.width - leftMargin - rightMargin, 44);
}

- (CGRect)pageController:(WMPageController *)pageController preferredFrameForContentView:(WMScrollView *)contentView {
    CGFloat originY = CGRectGetMaxY([self pageController:pageController preferredFrameForMenuView:self.pageController.menuView]);
    return CGRectMake(0, originY + 1, pageController.view.frame.size.width, pageController.view.frame.size.height - originY - 1);
}


#pragma mark - property

- (FBSHomeViewModel *)viewModel {
    if (!_viewModel) {
        _viewModel  = [[FBSHomeViewModel alloc] init];
    }
    return _viewModel;
}



- (UIView *)pageContainerView {
    if (!_pageContainerView) {
        _pageContainerView = [[UIView alloc] init];
        _pageContainerView.backgroundColor = [UIColor yellowColor];
        [self.view addSubview:_pageContainerView];
    }
    return _pageContainerView;
}

- (WMPageController *)pageController {
    if (!_pageController) {
        _pageController = [[WMPageController alloc] init];
        _pageController.dataSource = self;
        _pageController.delegate = self;
        _pageController.selectIndex = 0;
        _pageController.menuViewStyle = WMMenuViewStyleLine;
        _pageController.progressViewIsNaughty = YES;
        _pageController.progressWidth = 25;
        _pageController.automaticallyCalculatesItemWidths = YES;
        _pageController.titleColorNormal = Hexcolor(0x4A4A4A);
        _pageController.titleColorSelected = MainThemeColor;
        _pageController.titleSizeNormal = WScale(17);
        _pageController.titleSizeSelected = WScale(19);
        _pageController.view.backgroundColor = RGB(242,242,242);
        _pageController.menuView.backgroundColor = [UIColor whiteColor];
        _pageController.itemMargin = WScale(10);
        [self addChildViewController:_pageController];
        [self.pageContainerView addSubview:_pageController.view];
    }
    return _pageController;
}
@end
