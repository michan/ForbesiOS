//
//  FBSVideoDetailViewController.h
//  Forbes
//
//  Created by 赵志辉 on 2019/9/27.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import "FBSTableViewController.h"
#import "FBSVideoListCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface FBSVideoDetailViewController : FBSTableViewController


@property(nonatomic, strong)NSString *imgV;//图片
@property(nonatomic, strong)NSURL *video_url;// 视频地址
@property(nonatomic, strong)NSString *id;//图片集id
@property(nonatomic, assign)CGFloat currentTime;//当前观看的时间
@property(nonatomic, strong)NSString *titleContnt;//biao标题
@property(nonatomic, strong)NSString *has_like;//当前观看的时间
@property(nonatomic, strong)NSString *has_fav;//biao标题
@property(nonatomic, strong)FBSVideoListItem *listItem;//回传数据用的

@end

NS_ASSUME_NONNULL_END
