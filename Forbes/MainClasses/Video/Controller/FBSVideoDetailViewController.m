//
//  FBSVideoDetailViewController.m
//  Forbes
//
//  Created by 赵志辉 on 2019/9/27.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import "FBSVideoDetailViewController.h"
#import <ZFPlayer/ZFPlayer.h>
#import <ZFPlayer/ZFAVPlayerManager.h>
//#import <ZFPlayer/ZFCustomControlView1.h>
#import "UIImageView+ZFCache.h"
#import "ZFUtilities.h"
#import "FBSVideoDetailModel.h"
#import "AdvertAopTableView.h"
#import "FBSPinglunItem.h"
#import "FBSPinglunTableViewCell.h"
#import "FBSPingLunFooterView.h"
#import "XHInputView.h"
#import "FBSAttentAndPinglunViewController.h"
#import "ZFCustomControlView1.h"




@interface FBSVideoDetailViewController ()<FBSPingLunFooterViewDelegate,XHInputViewDelagete>

@property (nonatomic, strong) ZFPlayerController *player;
@property (nonatomic, strong) UIImageView *containerView;
@property (nonatomic, strong) ZFCustomControlView1 *controlView;
@property (nonatomic, strong) NSArray <NSURL *>*assetURLs;
@property (nonatomic, strong) UIButton *playBtn;

@property (nonatomic, strong) FBSVideoDetailModel *viewModel;
@property (nonatomic, strong)AdvertAopTableView *aopDemo;
@property (nonatomic ,strong)FBSPingLunFooterView *footer;


@end

@implementation FBSVideoDetailViewController
@synthesize viewModel = _viewModel;
///广告初始化
- (void)adsInit {
    NSString *position_id = @"41";  //首页广告
    if (position_id) {
        self.aopDemo = [AdvertAopTableView new];
        self.aopDemo.position_id = position_id;
        self.aopDemo.vc = self;
        self.aopDemo.soft = @"0";
        self.aopDemo.softArray = @[@"0",@"6"];

        self.aopDemo.aopUtils = self.tableView.aop_utils;
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationBarHidden = NO;
    self.navigationBackButtonHidden = NO;
    [self.navigationBar changeContentViewAlpha:0];
    self.registerDictionary = @{
                                [FBSPinglunItem reuseIdentifier] : [FBSPinglunTableViewCell class],
                                [FBSOneImageItem reuseIdentifier] :
                                    [FBSOneImageCell class]
                                };
    [self.view addSubview:self.containerView];
    [self.containerView addSubview:self.playBtn];
    [self.view addSubview:self.tableView];
    self.tableView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
    [self.tableView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.containerView.mas_bottom);
        make.left.right.mas_equalTo(self.view);
        make.bottom.mas_equalTo(self.view.mas_bottom).offset(WScale(-45));
    }];
    self.viewModel.id = self.id;
    self.viewModel.title = self.titleContnt;
    self.viewModel.has_fav = self.has_fav;
    self.viewModel.has_like = self.has_like;

    [self.viewModel rqInterster:^(BOOL success, NSInteger count) {
        [self.tableView reloadData];
    }];
    [self createPlayer];
    [self adsInit];
    [self.view addSubview:self.footer];
    self.footer.delegate = self;
    [self.footer mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.mas_equalTo(self.view);
        make.height.mas_equalTo(WScale(45));
    }];
    if ([self.has_fav isEqualToString:@"Y"]) {
        [self.footer changeSelectCollection:YES];
    }else{
        [self.footer changeSelectCollection:NO];
    }
    if ([self.has_like isEqualToString:@"Y"]) {
        [self.footer changeSelectLike:YES];
    }else{
        [self.footer changeSelectLike:NO];
    }
    // Do any additional setup after loading the view.
}
- (void)clickPinlunFooterView:(FBSPingLunFooterView *)view clickType:(PingLunType)type
{
    if (type == PingLunTypeSay) {
        [self click];
    }else if (type == PingLunTypelook){
        [self showHUD];
        WEAKSELF;
        [self.viewModel rqwenzhangLike:nil block:^(BOOL success, NSInteger count) {
            [weakSelf hideHUD];
            if (success) {
                [self makeToast:@"成功"];
                if ([self.has_like isEqualToString:@"Y"]) {
                    self.has_like = @"N";
                    self.listItem.has_like = @"N";
                    self.viewModel.has_like = @"N";
                    [self.footer changeSelectLike:NO];
                }else{
                    self.has_like = @"Y";
                    self.listItem.has_like = @"Y";
                    self.viewModel.has_like = @"Y";
                    [self.footer changeSelectLike:YES];
                }
            }else{
                [self makeToast:@"失败"];
            }
        }];
    }else if (type == PingLunTypeCollect){
        [self showHUD];
        WEAKSELF;
        [self.viewModel rqfans:nil block:^(BOOL success, NSInteger count) {
            [weakSelf hideHUD];
            if (success) {
                [self makeToast:@"成功"];
                if ([self.has_fav isEqualToString:@"Y"]) {
                    self.has_fav = @"N";
                    self.listItem.has_fav = @"N";
                    self.viewModel.has_fav = @"N";
                    [self.footer changeSelectCollection:NO];
                }else{
                    self.has_fav = @"Y";
                    self.listItem.has_fav = @"Y";
                    self.viewModel.has_fav = @"Y";
                    [self.footer changeSelectCollection:YES];
                }
            }else{
                [self makeToast:@"失败"];
            }
        }];
    }else if (type == PingLunTypeShare){
        
    }
}
- (void)click{
    [XHInputView showWithStyle:0 configurationBlock:^(XHInputView *inputView) {
        /** 请在此block中设置inputView属性 */
        
        /** 代理 */
        inputView.delegate = self;
        
        /** 占位符文字 */
        inputView.placeholder = @"请输入评论文字...";
        /** 设置最大输入字数 */
        inputView.maxCount = 50;
        /** 输入框颜色 */
        inputView.textViewBackgroundColor = [UIColor groupTableViewBackgroundColor];
        
        /** 更多属性设置,详见XHInputView.h文件 */
        
    } sendBlock:^BOOL(NSString *text) {
        if(text.length){
            NSLog(@"输入的信息为:%@",text);
            WEAKSELF;
            [self showHUD];
            [self.viewModel rqSendIndex:nil text:text block:^(BOOL success, NSInteger count) {
                [weakSelf hideHUD];
                if (success) {
                    [weakSelf makeToast:@"提交成功,请等待审核"];
                }else{
                    [weakSelf makeToast:@"提交失败，请重试"];
                }
            }];
            return YES;//return YES,收起键盘
        }else{
            NSLog(@"显示提示框-请输入要评论的的内容");
            return NO;//return NO,不收键盘
        }
    }];
}
- (FBSPingLunFooterView *)footer{
    if (!_footer) {
        _footer = [[FBSPingLunFooterView alloc] init];
    }
    return _footer;
}
- (void)createPlayer{
    ZFAVPlayerManager *playerManager = [[ZFAVPlayerManager alloc] init];
    /// 播放器相关
    self.player = [ZFPlayerController playerWithPlayerManager:playerManager containerView:self.containerView];
    self.player.controlView = self.controlView;
    /// 设置退到后台继续播放
    self.player.pauseWhenAppResignActive = NO;
    
    @weakify(self)
    self.player.orientationWillChange = ^(ZFPlayerController * _Nonnull player, BOOL isFullScreen) {
        @strongify(self)
        [self setNeedsStatusBarAppearanceUpdate];
    };
    /// 播放完成
    self.player.playerDidToEnd = ^(id  _Nonnull asset) {
        @strongify(self)
        [self.player.currentPlayerManager replay];
        [self.player playTheNext];
        [self.player stop];
    };
    
    self.player.assetURL = self.video_url;
    [self.player seekToTime:self.currentTime completionHandler:^(BOOL finished) {
        
    }];
    
    [self.controlView showTitle:self.titleContnt coverURLString:self.imgV fullScreenMode:ZFFullScreenModeAutomatic];
    
}
- (void)fbsCell:(FBSCell *)cell didClickButtonAtIndex:(NSInteger)index
{
    if ([cell isKindOfClass:[FBSPinglunTableViewCell class]]) {
        FBSPinglunTableViewCell *pinglunCell = (FBSPinglunTableViewCell *)cell;
        FBSPinglunItem *item = (FBSPinglunItem *)cell.item;
        if (index == 0) {
            WEAKSELF
            [self showHUD];
            [self.viewModel rqLikeIndex:item block:^(BOOL success, NSInteger count) {
                [pinglunCell changeLike];
                [weakSelf hideHUD];
            }];
        }else if (index == 1){
            //[self shareAction];
        }else if (index == 3){
            FBSAttentAndPinglunViewController *vc = [[FBSAttentAndPinglunViewController alloc] init];
            vc.pinglunItem = (FBSPinglunItem *)cell.item;
            [self.navigationController pushViewController:vc animated:YES];
        }
    }
}
- (void)fbsCell:(FBSCell *)cell didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell isKindOfClass:[FBSOneImageCell class]]) {
        FBSOneImageItem *item = (FBSOneImageItem *)cell.item;
        FBSVideoDetailViewController *vc = [[FBSVideoDetailViewController alloc] init];
        vc.id = item.ID;
        vc.titleContnt = item.content;
        vc.currentTime = 0;
        vc.imgV = item.imgUrl;
        vc.video_url =[NSURL URLWithString:item.videoUrl];
        [self.player stop];
        [self.navigationController pushViewController:vc animated:YES];
    }else if ([cell isKindOfClass:[FBSPinglunTableViewCell class]]){
        
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    FBSItem *item  = [self.viewModel itemAtRow:indexPath.row inSection:indexPath.section];
    if ([item isKindOfClass:[FBSPinglunItem class]]) {
        FBSPinglunItem *pinglunItem = (FBSPinglunItem *)item;
        [pinglunItem updateTab];
        return pinglunItem.allHeight;
    }
    return  item.cellHeight;
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.player.viewControllerDisappear = NO;
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    self.player.viewControllerDisappear = YES;
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    self.view.backgroundColor =[UIColor blackColor];
    CGFloat x = 0;
    CGFloat y = kStatusBarH;//CGRectGetMaxY(self.navigationController.navigationBar.frame);
    CGFloat w = CGRectGetWidth(self.view.frame);
    CGFloat h = w*9/16;
    self.containerView.frame = CGRectMake(x, y, w, h);
    
    w = 44;
    h = w;
    x = (CGRectGetWidth(self.containerView.frame)-w)/2;
    y = (CGRectGetHeight(self.containerView.frame)-h)/2;
    self.playBtn.frame = CGRectMake(x, y, w, h);
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    if (self.player.isFullScreen) {
        return UIStatusBarStyleLightContent;
    }
    return UIStatusBarStyleLightContent;

//    return UIStatusBarStyleDefault;
}

- (BOOL)prefersStatusBarHidden {
    /// 如果只是支持iOS9+ 那直接return NO即可，这里为了适配iOS8
    return self.player.isStatusBarHidden;
}

- (UIStatusBarAnimation)preferredStatusBarUpdateAnimation {
    return UIStatusBarAnimationSlide;
}

- (BOOL)shouldAutorotate {
    return self.player.shouldAutorotate;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    if (self.player.isFullScreen) {
        return UIInterfaceOrientationMaskLandscape;
    }
    return UIInterfaceOrientationMaskPortrait;
}

- (ZFCustomControlView1 *)controlView {
    if (!_controlView) {
        _controlView = [ZFCustomControlView1 new];
        _controlView.autoHiddenTimeInterval = 5;
        _controlView.autoFadeTimeInterval = 0.5;
        WEAKSELF
        _controlView.cancelLeft = ^(){
            [weakSelf.navigationController popViewControllerAnimated:YES];
        };
    }
    return _controlView;
}

- (UIImageView *)containerView {
    if (!_containerView) {
        _containerView = [UIImageView new];
        [_containerView setImageWithURLString:self.imgV placeholder:[ZFUtilities imageWithColor:[UIColor colorWithRed:220/255.0 green:220/255.0 blue:220/255.0 alpha:1] size:CGSizeMake(1, 1)]];
    }
    return _containerView;
}

- (UIButton *)playBtn {
    if (!_playBtn) {
        _playBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_playBtn setImage:[UIImage imageNamed:@"video_listplay"] forState:UIControlStateNormal];
        [_playBtn addTarget:self action:@selector(playClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _playBtn;
}
- (void)playClick:(UIButton *)sender {
    [self.player playTheIndex:0];
    [self.controlView showTitle:self.titleContnt coverURLString:self.imgV fullScreenMode:ZFFullScreenModeAutomatic];
}
- (FBSTableViewModel *)viewModel{
    if (!_viewModel) {
        _viewModel = [[FBSVideoDetailModel alloc] init];
    }
    return _viewModel;
}
@end
