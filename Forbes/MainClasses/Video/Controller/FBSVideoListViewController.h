//
//  FBSVideoListViewController.h
//  Forbes
//
//  Created by 周灿华 on 2019/7/28.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSTableViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface FBSVideoListViewController : FBSTableViewController
@property (nonatomic, strong) NSString *type;
- (instancetype)initWithCatagery:(NSString *)catagery;
@end

NS_ASSUME_NONNULL_END
