
//
//  FBSVideoListViewController.m
//  Forbes
//
//  Created by 周灿华 on 2019/7/28.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSVideoListViewController.h"
#import "FBSVideoViewModel.h"
#import <ZFPlayer/ZFPlayer.h>
#import <ZFPlayer/ZFAVPlayerManager.h>
#import <ZFPlayer/ZFPlayerControlView.h>
#import "FBSTableViewController+Refresh.h"
#import "FBSVideoDetailViewController.h"
#import "AdvertAopTableView.h"


typedef void(^block1)(NSString *num);
@interface FBSVideoListViewController ()<UIScrollViewDelegate>
@property (nonatomic, strong) FBSVideoViewModel *viewModel;

@property (nonatomic, strong) ZFPlayerController *player;
@property (nonatomic, strong) ZFPlayerControlView *controlView;
@property (nonatomic, strong) NSMutableArray *dataSource;
@property (nonatomic, strong) NSMutableArray *urls;
@property (nonatomic, strong)AdvertAopTableView *aopDemo;

@end

@implementation FBSVideoListViewController
@synthesize viewModel = _viewModel;
///广告初始化
- (void)adsInit {
    NSString *position_id = @"40";  //首页广告
    if (position_id) {
        self.aopDemo = [AdvertAopTableView new];
        self.aopDemo.position_id = position_id;
        self.aopDemo.vc = self;
        self.aopDemo.soft = @"1";
        self.aopDemo.softArray = @[@"1",@"7",@"13",@"19",@"25",@"31"];
        self.aopDemo.aopUtils = self.tableView.aop_utils;
    }
}
- (instancetype)initWithCatagery:(NSString *)catagery{
    if (self == [super init]) {
        self.viewModel.category = catagery;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    self.navigationBarHidden = YES;
    
    self.registerDictionary = @{
                                [FBSVideoListItem reuseIdentifier] : [FBSVideoListCell class],
                                };
    

    @weakify(self)
    self.tableView.zf_scrollViewDidStopScrollCallback = ^(NSIndexPath * _Nonnull indexPath) {
        @strongify(self)
        if (!self.player.playingIndexPath) {
            [self playTheVideoAtIndexPath:indexPath scrollToTop:NO];
        }
    };
    [self createload];
    [self createPlayer];
    [self adsInit];
    
}
- (void)createload{
    
    self.page = @(1);
    self.pageCount = @(5);
    WEAKSELF
    [self normalHeaderRefreshingActionBlock:^(FooterConfigBlock  _Nonnull footerConfig) {
        [self.player stop];
        self.player.shouldAutoPlay = NO;
        [self.viewModel rqVideoList:^(BOOL success,NSMutableArray *urls) {
            if (success) {
                weakSelf.urls = [urls mutableCopy];
                [weakSelf.tableView reloadData];
                weakSelf.player.assetURLs = [weakSelf.urls copy];
                if(footerConfig){
                    footerConfig(urls.count);
                }
                self.player.shouldAutoPlay = YES;
            }else{
                
            }
        }];
    }];
    [self backNormalFooterRefreshingActionBlock:^(FooterConfigBlock  _Nonnull footerConfig) {
        [self.player stop];
        self.player.shouldAutoPlay = NO;
        [self.viewModel rqVideoList:^(BOOL success,NSMutableArray *urls) {
            if (success) {
                [weakSelf.urls addObjectsFromArray:urls];
                [weakSelf.tableView reloadData];
                weakSelf.player.assetURLs = [weakSelf.urls copy];
                if(footerConfig){
                    footerConfig(urls.count);
                }
                self.player.shouldAutoPlay = YES;
            }else{
                
            }
        }];
    }];
}
- (void)createPlayer{
    ZFAVPlayerManager *playerManager = [[ZFAVPlayerManager alloc] init];
    /// player的tag值必须在cell里设置
    self.player = [ZFPlayerController playerWithScrollView:self.tableView playerManager:playerManager containerViewTag:100];
    self.player.controlView = self.controlView;
    self.player.assetURLs = self.urls;
    /// 0.8是消失80%时候，默认0.5
    self.player.playerDisapperaPercent = 0.8;
    /// 移动网络依然自动播放
    self.player.WWANAutoPlay = YES;
    
    self.player.viewControllerDisappear = YES;
    
    @weakify(self)
    self.player.playerDidToEnd = ^(id  _Nonnull asset) {
        @strongify(self)
        if (self.player.playingIndexPath.row < self.urls.count - 1) {
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:self.player.playingIndexPath.row+1 inSection:0];
            [self playTheVideoAtIndexPath:indexPath scrollToTop:YES];
        } else {
            [self.player stopCurrentPlayingCell];
        }
    };
    self.player.playerPlayTimeChanged = ^(id<ZFPlayerMediaPlayback>  _Nonnull asset, NSTimeInterval currentTime, NSTimeInterval duration){
        NSLog(@"%f+++++++",currentTime);
    };
    
    self.player.orientationWillChange = ^(ZFPlayerController * _Nonnull player, BOOL isFullScreen) {
        @strongify(self)
        [self setNeedsStatusBarAppearanceUpdate];
        [UIViewController attemptRotationToDeviceOrientation];
        self.tableView.scrollsToTop = !isFullScreen;
    };
}
- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    CGFloat y = CGRectGetMaxY(self.navigationController.navigationBar.frame);
    CGFloat h = CGRectGetMaxY(self.view.frame);
    self.tableView.frame = CGRectMake(0, y, self.view.frame.size.width, h-y);
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    @weakify(self)
    [self.tableView zf_filterShouldPlayCellWhileScrolled:^(NSIndexPath *indexPath) {
        @strongify(self)
        [self playTheVideoAtIndexPath:indexPath scrollToTop:NO];
    }];
}
- (BOOL)shouldAutorotate {
    /// 如果只是支持iOS9+ 那直接return NO即可，这里为了适配iOS8
    return self.player.shouldAutorotate;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    if (self.player.isFullScreen && self.player.orientationObserver.fullScreenMode == ZFFullScreenModeLandscape) {
        return UIInterfaceOrientationMaskLandscape;
    }
    return UIInterfaceOrientationMaskPortrait;
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    if (self.player.isFullScreen) {
        return UIStatusBarStyleLightContent;
    }
    return UIStatusBarStyleDefault;
}

- (BOOL)prefersStatusBarHidden {
    return self.player.isStatusBarHidden;
}

- (UIStatusBarAnimation)preferredStatusBarUpdateAnimation {
    return UIStatusBarAnimationSlide;
}

#pragma mark - UIScrollViewDelegate 列表播放必须实现

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    [scrollView zf_scrollViewDidEndDecelerating];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    [scrollView zf_scrollViewDidEndDraggingWillDecelerate:decelerate];
}

- (void)scrollViewDidScrollToTop:(UIScrollView *)scrollView {
    [scrollView zf_scrollViewDidScrollToTop];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    [scrollView zf_scrollViewDidScroll];
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    [scrollView zf_scrollViewWillBeginDragging];
}
#pragma mark - ZFTableViewCellDelegate

- (void)zf_playTheVideoAtIndexPath:(NSIndexPath *)indexPath {
    [self playTheVideoAtIndexPath:indexPath scrollToTop:NO];
}

#pragma mark - private method

/// play the video
- (void)playTheVideoAtIndexPath:(NSIndexPath *)indexPath scrollToTop:(BOOL)scrollToTop {
    [self.player playTheIndexPath:indexPath scrollToTop:scrollToTop];
    FBSItem *Item = [self.viewModel itemAtRow:indexPath.row inSection:indexPath.section];
    if ([Item isKindOfClass:[FBSVideoListItem class]]) {
        FBSVideoListItem *itemVodio = (FBSVideoListItem *)Item;
        [self.controlView showTitle:itemVodio.title
                     coverURLString:itemVodio.imgUrl
                     fullScreenMode:ZFFullScreenModeLandscape];
    }
}
#pragma mark - delegate
- (void)fbsCell:(FBSCell *)cell didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
//    [self playTheVideoAtIndexPath:indexPath scrollToTop:NO];
    FBSItem *Item = [self.viewModel itemAtRow:indexPath.row inSection:indexPath.section];
    if ([Item isKindOfClass:[FBSVideoListItem class]]) {
        FBSVideoListItem *itemVodio = (FBSVideoListItem *)Item;
        FBSVideoDetailViewController *vc = [[FBSVideoDetailViewController alloc] init];
        vc.video_url = self.urls[indexPath.row];
        vc.imgV = itemVodio.imgUrl;
        vc.titleContnt = itemVodio.title;
        vc.currentTime = self.player.currentTime;
        vc.id = itemVodio.id;
        vc.has_fav = itemVodio.has_fav;
        vc.has_like = itemVodio.has_like;
        vc.listItem = itemVodio;
        [self.player stop];
        [self.navigationController pushViewController:vc animated:YES];
    }
}
- (void)fbsCell:(FBSCell *)cell didClickButtonAtIndex:(NSInteger)index{
    if (index == 0) {
        NSIndexPath *indexP = [self.tableView indexPathForCell:cell];
        [self playTheVideoAtIndexPath:indexP scrollToTop:NO];
    }
}

#pragma mark - getter



- (ZFPlayerControlView *)controlView {
    if (!_controlView) {
        _controlView = [ZFPlayerControlView new];
        _controlView.fastViewAnimated = YES;
        _controlView.horizontalPanShowControlView = NO;
        _controlView.prepareShowLoading = YES;
    }
    return _controlView;
}

#pragma mark - property

- (FBSVideoViewModel *)viewModel {
    if (!_viewModel) {
        _viewModel  = [[FBSVideoViewModel alloc] init];
    }
    return _viewModel;
}
-(NSMutableArray *)urls{
    if(!_urls){
        _urls = [NSMutableArray array];
    }
    return _urls;
}
@end
