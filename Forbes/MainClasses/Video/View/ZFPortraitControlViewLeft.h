//
//  ZFPortraitControlViewLeft.h
//  Forbes
//
//  Created by 赵志辉 on 2019/12/16.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import "ZFPortraitControlView.h"

NS_ASSUME_NONNULL_BEGIN

@interface ZFPortraitControlViewLeft : ZFPortraitControlView



@property(nonatomic, strong) UIButton *button;

@end

NS_ASSUME_NONNULL_END
