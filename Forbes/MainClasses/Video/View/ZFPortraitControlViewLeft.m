//
//  ZFPortraitControlViewLeft.m
//  Forbes
//
//  Created by 赵志辉 on 2019/12/16.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import "ZFPortraitControlViewLeft.h"

@implementation ZFPortraitControlViewLeft


- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        [self addSubview:self.button];
    }
    return self;
}
- (void)close{
    
}
- (UIButton *)button
{
    if (!_button) {
        _button = [UIButton buttonWithType:UIButtonTypeCustom];
        [_button addTarget:self action:@selector(close) forControlEvents:UIControlEventTouchUpInside];
    }
    return _button;
}
#pragma mark - Layout
- (void)layoutSubviews{
    [super layoutSubviews];
    
    CGFloat min_x = 30;
    CGFloat min_y = 0;
    CGFloat min_w = 0;
    CGFloat min_h = 0;
    CGFloat min_view_w = self.bounds.size.width;
    CGFloat min_view_h = self.bounds.size.height;
    CGFloat min_margin = 9;
    min_x = 15;
    min_y = 5;
    min_w = min_view_w - min_x - 15;
    min_h = 30;
    self.titleLabel.frame = CGRectMake(min_x, min_y, min_view_w - min_x, min_h);
    self.button.frame = CGRectMake(10, min_y, 20, 20);

}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
