//
//  FBSVideoListCell.m
//  Forbes
//
//  Created by 周灿华 on 2019/7/28.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSVideoListCell.h"
#import "UIImageView+WebCache.h"

@implementation FBSVideoListItem

@end

@interface FBSVideoListCell ()
@property (nonatomic, strong) UIImageView *imageV;
@property (nonatomic, strong) UIButton *playBtn;
@property (nonatomic, strong) UILabel *titleL;
@property (nonatomic, strong) UILabel *playCoutL;
@property (nonatomic, strong) UILabel *timeL;

@property (nonatomic, strong) UIView *bottomV;
@property (nonatomic, strong) UIImageView *iconV;
@property (nonatomic, strong) UILabel *nameL;
@property (nonatomic, strong) UIButton *praiseBtn;
@property (nonatomic, strong) UIButton *commentBtn;
@property (nonatomic, strong) UIButton *moreBtn;
@property (nonatomic, strong) NSDictionary *attributeDic;
@property (nonatomic, strong) UIView *bgBackView;

@end

@implementation FBSVideoListCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self.contentView addSubview:self.imageV];
        [self.imageV addSubview:self.bgBackView];

        [self.imageV addSubview:self.playBtn];
        [self.imageV addSubview:self.titleL];
        [self.imageV addSubview:self.playCoutL];
        _playCoutL.hidden = YES;
        [self.imageV addSubview:self.timeL];
        
        [self.contentView addSubview:self.bottomV];
        self.bottomV.hidden = YES;
        [self.bottomV addSubview:self.iconV];
        [self.bottomV addSubview:self.nameL];
        [self.bottomV addSubview:self.praiseBtn];
        [self.bottomV addSubview:self.commentBtn];
        [self.bottomV addSubview:self.moreBtn];
        
        self.imageV.userInteractionEnabled = YES;
        [self.imageV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.left.right.mas_equalTo(0);
            make.height.mas_equalTo(WScale(212));
        }];
        [self.bgBackView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(0);
            make.left.mas_equalTo(0);
            make.right.mas_equalTo(0);
            make.bottom.mas_equalTo(0);

        }];

        
        [self.titleL mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(WScale(15));
            make.left.mas_equalTo(WScale(13));
            make.right.mas_equalTo(-WScale(10));
        }];

        
        [self.playBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.height.mas_equalTo(WScale(49));
            make.centerX.centerY.mas_equalTo(self.imageV);
        }];

        
        
        [self.playCoutL mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.titleL);
            make.bottom.mas_equalTo(-WScale(13));
            make.height.mas_equalTo(WScale(11));
        }];
        
        [self.timeL mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(-WScale(10));
            make.width.mas_equalTo(WScale(45));
            make.height.mas_equalTo(WScale(20));
            make.centerY.mas_equalTo(self.playCoutL);
        }];

    
        [self.bottomV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.imageV.mas_bottom);
            make.left.right.mas_equalTo(0);
            make.height.mas_equalTo(WScale(48));
        }];
        
        [self.iconV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(WScale(12));
            make.width.height.mas_equalTo(WScale(34));
            make.centerY.mas_equalTo(0);
        }];
        
        
        [self.nameL mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.iconV.mas_right).offset(WScale(10));
            make.centerY.mas_equalTo(0);
        }];
        
        [self.moreBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(0);
            make.top.bottom.mas_equalTo(0);
            make.width.mas_equalTo(WScale(55));
        }];

        [self.commentBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(self.moreBtn.mas_left);
            make.top.bottom.mas_equalTo(0);
            make.width.mas_equalTo(WScale(55));

        }];

        [self.praiseBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(self.commentBtn.mas_left);
            make.top.bottom.mas_equalTo(0);
            make.width.mas_equalTo(WScale(55));
        }];

        
    }
    
    return self;
}



- (void)setItem:(FBSVideoListItem *)item {
    if (![item isKindOfClass:[FBSVideoListItem class]]) {
        return ;
    }
    
    FBSVideoListItem *itemV = (FBSVideoListItem *)item;
    [super setItem:item];
    
    self.titleL.attributedText = [[NSAttributedString alloc] initWithString:item.title attributes:self.attributeDic];
    self.playCoutL.text = item.playCout;
    self.timeL.text = item.time;
    self.nameL.text = item.name;
    [self.imageV sd_setImageWithURL:[NSURL URLWithString:itemV.imgUrl]];
    
//    [self.praiseBtn sizeToFit];
//    [self.commentBtn sizeToFit];
 
}

#pragma mark - action
- (void)player{
    
    if ([self.delegate respondsToSelector:@selector(fbsCell:didClickButtonAtIndex:)]) {
        [self.delegate fbsCell:self didClickButtonAtIndex:0];
    }
}
- (void)clickAction:(UIButton *)button {
    button.selected = !button.isSelected;
}
-(UIView *)bgBackView
{
    if (!_bgBackView) {
        _bgBackView = [[UIView alloc]init];
        _bgBackView.backgroundColor =[UIColor blackColor];
        _bgBackView.alpha = 0.7;
    }
    return _bgBackView;
    
}
#pragma mark - property

- (UIImageView *)imageV {
    if (!_imageV) {
        _imageV  = [[UIImageView alloc] init];
        _imageV.backgroundColor = RandomColor;
        _imageV.tag = 100;
    }
    return _imageV;
}

- (UIButton *)playBtn {
    if (!_playBtn) {
        _playBtn  = [UIButton buttonWithType:UIButtonTypeCustom];
        [_playBtn setBackgroundImage:kImageWithName(@"video_listplay") forState:UIControlStateNormal];
        [_playBtn addTarget:self action:@selector(player) forControlEvents:UIControlEventTouchUpInside];

    }
    return _playBtn;
}


- (UILabel *)titleL {
    if (!_titleL) {
        _titleL  = [[UILabel alloc] init];
        _titleL.textColor = [UIColor whiteColor];
        _titleL.font = kLabelFontSize18;
        _titleL.lineBreakMode = NSLineBreakByTruncatingTail;
        _titleL.numberOfLines = 2;
        _titleL.text = @"-";
    }
    return _titleL;
}

- (UILabel *)playCoutL {
    if (!_playCoutL) {
        _playCoutL  = [[UILabel alloc] init];
        _playCoutL.font = kLabelFontSize11;
        _playCoutL.textColor = [UIColor whiteColor];
        _playCoutL.text = @"-";
    }
    return _playCoutL;
}

- (UILabel *)timeL {
    if (!_timeL) {
        _timeL  = [[UILabel alloc] init];
        _timeL.backgroundColor = [Hexcolor(0x4A4A4A) colorWithAlphaComponent:0.53];
        _timeL.font = kLabelFontSize11;
        _timeL.textColor = [UIColor whiteColor];
        _timeL.textAlignment = NSTextAlignmentCenter;
        _timeL.text = @"-";
        _timeL.adjustsFontSizeToFitWidth = YES;
        _timeL.layer.cornerRadius = WScale(10);
        _timeL.layer.masksToBounds = YES;
    }
    return _timeL;
}

- (UIView *)bottomV {
    if (!_bottomV) {
        _bottomV  = [[UIView alloc] init];
        _bottomV.backgroundColor = [UIColor whiteColor];
    }
    return _bottomV;
}

- (UIImageView *)iconV {
    if (!_iconV) {
        _iconV  = [[UIImageView alloc] init];
        _iconV.backgroundColor = RandomColor;
        _iconV.layer.cornerRadius = WScale(17);
        _iconV.layer.masksToBounds = YES;
    }
    return _iconV;
}


- (UILabel *)nameL {
    if (!_nameL) {
        _nameL  = [[UILabel alloc] init];
        _nameL.textColor = COLOR_TitleBlack;
        _nameL.font = kLabelFontSize14;
        _nameL.text = @"-";
    }
    return _nameL;
}


- (UIButton *)praiseBtn {
    if (!_praiseBtn) {
        _praiseBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _praiseBtn.tag = PraiseIndex;
        _praiseBtn.titleLabel.font = kLabelFontSize11;
        [_praiseBtn setTitleColor:Hexcolor(0x666666) forState:UIControlStateNormal];
        [_praiseBtn setTitleColor:[UIColor redColor] forState:UIControlStateSelected];
        [_praiseBtn setTitle:@"3" forState:UIControlStateNormal];
        [_praiseBtn setTitle:@"4" forState:UIControlStateSelected];
        [_praiseBtn setImage:kImageWithName(@"video_praise1") forState:UIControlStateNormal];
        [_praiseBtn setImage:kImageWithName(@"video_praise2") forState:UIControlStateSelected];
        [_praiseBtn addTarget:self action:@selector(clickAction:) forControlEvents:UIControlEventTouchUpInside];
        
        _praiseBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 2.5);
        _praiseBtn.titleEdgeInsets = UIEdgeInsetsMake(0, 2.5, 0, 0);
    }
    return _praiseBtn;
}


- (UIButton *)commentBtn {
    if (!_commentBtn) {
        _commentBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _commentBtn.tag = CommentIndex;
        _commentBtn.titleLabel.font = kLabelFontSize11;
        [_commentBtn setTitleColor:Hexcolor(0x666666) forState:UIControlStateNormal];
        [_commentBtn setTitle:@"18" forState:UIControlStateNormal];
        [_commentBtn setImage:kImageWithName(@"video_comment") forState:UIControlStateNormal];
        [_commentBtn addTarget:self action:@selector(clickAction:) forControlEvents:UIControlEventTouchUpInside];
        
        _commentBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 2.5);
        _commentBtn.titleEdgeInsets = UIEdgeInsetsMake(0, 2.5, 0, 0);
    }
    return _commentBtn;
}


- (UIButton *)moreBtn {
    if (!_moreBtn) {
        _moreBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _moreBtn.tag = MoreIndex;
        [_moreBtn setImage:kImageWithName(@"video_more") forState:UIControlStateNormal];
        [_moreBtn addTarget:self action:@selector(clickAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _moreBtn;
}

- (NSDictionary *)attributeDic {
    if (!_attributeDic) {
        NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc] init];
        style.lineSpacing = 5;
        
        _attributeDic  = @{
                           NSForegroundColorAttributeName : [UIColor whiteColor],
                           NSFontAttributeName : kLabelFontSize18,
                           NSParagraphStyleAttributeName : style
                           };
    }
    return _attributeDic;
}


@end
