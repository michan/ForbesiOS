//
//  FBSVideoListCell.h
//  Forbes
//
//  Created by 周灿华 on 2019/7/28.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSCell.h"

#define PraiseIndex  1000
#define CommentIndex 1001
#define MoreIndex    1002

@interface FBSVideoListItem : FBSItem

@property (nonatomic, copy) NSString *id;
@property(nonatomic, copy) NSString *category_id;
@property (nonatomic, copy) NSString *imgUrl;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *playCout;
@property (nonatomic, copy) NSString *time;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, assign)NSTimeInterval currentTime;
@property (nonatomic, copy) NSString *has_fav;
@property (nonatomic, copy) NSString *has_like;
@end



@interface FBSVideoListCell : FBSCell

@end

