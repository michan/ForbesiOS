//
//  FBSWenZhangModel.m
//  Forbes
//
//  Created by 赵志辉 on 2019/9/28.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import "FBSWenZhangModel.h"

@implementation FBSWenZhangDataModelPartsItem
@end


@implementation FBSWenZhangDataModel
+ (NSDictionary *)mj_replacedKeyFromPropertyName {
    return @{@"Description" : @"description"};
}
+ (NSDictionary *)mj_objectClassInArray {
    return @{@"parts" : @"FBSWenZhangDataModelPartsItem"};
}
@end


@implementation FBSWenZhangModel

@end
