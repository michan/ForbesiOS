//
//  FBSarticleHeaderView.m
//  Forbes
//
//  Created by 赵志辉 on 2019/9/27.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import "FBSarticleHeaderView.h"

@interface FBSarticleHeaderView()
@property(nonatomic, strong)UIImageView *image;
@property(nonatomic, strong)UILabel *name;
//@property(nonatomic, strong)UIButton *FocusBtn;

@end

@implementation FBSarticleHeaderView

- (instancetype)init{
    if (self = [super init]) {
        self.backgroundColor = Hexcolor(0xFFFFFF);
        [self addSubview:self.image];
        
        [self addSubview:self.name];
        
        [self addSubview:self.attent];
//        self.attent.hidden = YES;
      
        [self createView];
    }
    return self;
}
- (void)createView{
    
    [self.image mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self).offset(WScale(11));
        make.top.mas_equalTo(self).offset(WScale(14));
        make.width.mas_equalTo(WScale(24));
        make.height.mas_equalTo(WScale(24));
    }];
    
    [self.name mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.image.mas_right).offset(WScale(5));
        make.top.mas_equalTo(self.mas_top).offset(WScale(11));
        make.right.mas_equalTo(self).offset(WScale(-14));
        make.height.mas_equalTo(WScale(35));
    }];
    [self.attent mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(WScale(64));
        make.centerY.mas_equalTo(self.image.mas_centerY);
        make.right.mas_equalTo(self).offset(WScale(-15));
        make.height.mas_equalTo(WScale(22));
    }];
}

- (UIImageView *)image{
    if (!_image) {
        _image = [[UIImageView alloc] init];
        _image.layer.cornerRadius = WScale(12);
        _image.layer.masksToBounds = YES;
        
    }
    return _image;
}
- (void)setDataModel:(FBSWenZhangDataModel *)DataModel{
    [self.image sd_setImageWithURL:[NSURL URLWithString:DataModel.file_url]];
    NSString *nameAndTime = [NSString stringWithFormat:@"%@\n%@",DataModel.nickname,[NSString getTimestamp:DataModel.time formatter:@"yyyy-MM-dd HH:mm:ss"]];
    NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:nameAndTime];
    if(DataModel.nickname.length){
         [str addAttributes:@{NSFontAttributeName:kLabelFontSize15,NSForegroundColorAttributeName:Hexcolor(0x333333)} range:[nameAndTime rangeOfString:DataModel.nickname]];
    }
    if(DataModel.time.length){
         [str addAttributes:@{NSFontAttributeName:kLabelFontSize13,NSForegroundColorAttributeName:Hexcolor(0x9B9B9B)} range:[nameAndTime rangeOfString:[NSString getTimestamp:DataModel.time formatter:@"yyyy-MM-dd HH:mm:ss"]]];
    }
    self.name.attributedText = str;
}


- (UILabel *)name{
    if (!_name) {
        _name = [[UILabel alloc] init];
        _name.textColor = Hexcolor(0x333333);
        _name.numberOfLines = 2;
    }
    return _name;
}

-(UIButton *)attent{
    if (!_attent) {
        _attent = [[UIButton alloc] init];
        [_attent setImage:[UIImage imageNamed:@"follow_sel"] forState:UIControlStateSelected];
        [_attent setImage:[UIImage imageNamed:@"follow_unsel"] forState:UIControlStateNormal];

        
    }
    return _attent;
}


@end
