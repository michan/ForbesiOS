//
//  FBSVideoViewModel.h
//  Forbes
//
//  Created by 周灿华 on 2019/7/28.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSViewModel.h"
#import "FBSVideoListCell.h"
#import "FBSVideoModel.h"


@interface FBSVideoViewModel : FBSTableViewModel

@property (nonatomic, strong) FBSVideoModel *videoModel;

@property (nonatomic, strong)NSString *category;

- (void)rqVideoList:(void(^)(BOOL success,NSMutableArray *urls))complete;

@end
