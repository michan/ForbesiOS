//
//  FBSActivityFooterView.m
//  Forbes
//
//  Created by 赵志辉 on 2019/9/28.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import "FBSActivityFooterView.h"

@interface FBSActivityFooterView()
@property(nonatomic, strong)UIButton *collect;
@property(nonatomic, strong)UIButton *share;
@property(nonatomic, strong)UIButton *baoming;
@end

@implementation FBSActivityFooterView

- (instancetype)init{
    if (self = [super init]) {
        self.backgroundColor = Hexcolor(0xFFFFFF);
        [self addSubview:self.collect];
        
        [self addSubview:self.share];
        
        [self addSubview:self.baoming];
        
        [self createView];
    }
    return self;
}
- (void)createView{
    
    [self.collect mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self).offset(WScale(19));
        make.top.mas_equalTo(self).offset(WScale(15));
        make.width.mas_equalTo(WScale(16));
        make.height.mas_equalTo(WScale(15));
    }];
    
    [self.share mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.collect.mas_right).offset(WScale(32));
        make.top.mas_equalTo(self).offset(WScale(15));
        make.width.mas_equalTo(WScale(16));
        make.height.mas_equalTo(WScale(15));
    }];
    [self.baoming mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.share.mas_right).offset(WScale(32));
        make.centerY.mas_equalTo(self.share.mas_centerY);
        make.right.mas_equalTo(self).offset(WScale(-17));
        make.height.mas_equalTo(WScale(35));
    }];
}
- (void)cllick:(UIButton *)sender{
    if ([self.delegate respondsToSelector:@selector(clickActivityFooterView:clickType:)]) {
        [self.delegate clickActivityFooterView:self clickType:sender.tag];
    }
}
- (void)changeSelectCollection:(BOOL)select{
    self.collect.selected = select;
}
- (UIButton *)collect{
    if (!_collect) {
        _collect = [[UIButton alloc] init];
        _collect.tag = 100000;

        [_collect setBackgroundImage:[UIImage imageNamed:@"mine_collection"] forState:UIControlStateNormal];
        [_collect addTarget:self action:@selector(cllick:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _collect;
}


- (UIButton *)share{
    if (!_share) {
        _share = [[UIButton alloc] init];
        _share.tag = 100001;
        [_share setBackgroundImage:[UIImage imageNamed:@"share"] forState:UIControlStateNormal];
        [_share addTarget:self action:@selector(cllick:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _share;
}

- (UIButton *)baoming{
    if (!_baoming) {
        _baoming = [[UIButton alloc] init];
        _baoming.tag = 100002;
        [_baoming setBackgroundImage:[UIImage imageNamed:@"立刻报名button"] forState:UIControlStateNormal];
        [_baoming addTarget:self action:@selector(cllick:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _baoming;
}
@end
