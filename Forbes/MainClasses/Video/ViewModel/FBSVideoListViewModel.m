
//
//  FBSVideoListViewModel.m
//  Forbes
//
//  Created by 周灿华 on 2019/7/28.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSVideoListViewModel.h"
#import "FBSVideoListAPI.h"

@interface FBSVideoListViewModel ()
@property (nonatomic, strong) FBSVideoListAPI *videoListAPI;

@property (nonatomic, strong) NSMutableArray *urls;

@end

@implementation FBSVideoListViewModel

- (void)makeItems {
    if (self.page == 1) {
        [self.items removeAllObjects];
    }
//    NSArray *titles = @[
//                         @"2019第五届中国（深圳）国际宠物用品展",
//                         @"中国进出口上平交易会中国进出口上平交易会中国进出口上平交易会中国进出口上平交易会中国进出口上平交易会中国进出口上平交易会",
//                         @"上海世博会",
//                         @"珠海航展",
//                         @"广州亚运会"
//                         ];
    self.urls = [NSMutableArray array];
    for (FBSVideoInfo *info in self.videoModel.data.items) {
        FBSVideoListItem *listItem = [FBSVideoListItem item];
        listItem.id = info.id;
        listItem.category_id = info.category_id;
        listItem.title = info.title;
        listItem.imgUrl = info.file_img;
//        listItem.playCout = @"1008播放";
//        listItem.time = info.created_at;
        NSString* time =  [self getVideoTimeByUrlString:info.file_url];
           
           listItem.time = time;

        listItem.name = @"福布斯中国";
        listItem.backgroundColor = Hexcolor(0xF6F7F9);
        listItem.cellHeight = WScale(270);
        [self.items addObject:listItem];
        //NSString *URLString = [info.file_url stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
        [self.urls addObject:[NSURL URLWithString:info.file_url]];
    }
    
}
- (NSString*)getVideoTimeByUrlString:(NSString*)urlString {

    AVURLAsset * audioAsset=[AVURLAsset URLAssetWithURL: [NSURL URLWithString:urlString] options:nil];
    CMTime audioDuration=audioAsset.duration;
    CGFloat audioDurationSeconds=CMTimeGetSeconds(audioDuration);
   NSString * totalTime =  [NSString stringWithFormat:@"%f",audioDurationSeconds];
    NSInteger seconds = [totalTime integerValue];

    //format of hour
    NSString *str_hour = [NSString stringWithFormat:@"%02ld",seconds/3600];
    //format of minute
    NSString *str_minute = [NSString stringWithFormat:@"%02ld",(seconds%3600)/60];
    //format of second
    NSString *str_second = [NSString stringWithFormat:@"%02ld",seconds%60];
    //format of time
    NSString *format_time = [NSString stringWithFormat:@"%@:%@",str_minute,str_second];

    return format_time;

}

- (void)rqVideoList:(void(^)(BOOL success,NSMutableArray *urls))complete {
    WEAKSELF
    self.videoListAPI.page = self.page;
    [self.videoListAPI startWithSuccess:^(YBNetworkResponse * _Nonnull response) {
        FBSLog(@"responseObject : %@",response.responseObject);
    
        weakSelf.videoModel = [FBSVideoModel mj_objectWithKeyValues:response.responseObject];
        
        [weakSelf makeItems];
        if (complete) {
            complete(YES,weakSelf.urls);
        }else{
            complete(NO,nil);
        }
    } failure:^(YBNetworkResponse * _Nonnull response) {
        if (complete) {
            complete(NO,nil);
        }
    }];
}



#pragma mark - propety

- (FBSVideoListAPI *)videoListAPI {
    if (!_videoListAPI) {
        _videoListAPI  = [[FBSVideoListAPI alloc] init];
        _videoListAPI.limit = self.limit;
        _videoListAPI.page = self.page;
    }
    return _videoListAPI;
}

@end
