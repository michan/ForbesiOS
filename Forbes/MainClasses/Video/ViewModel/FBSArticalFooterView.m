//
//  FBSArticalFooterView.m
//  Forbes
//
//  Created by 赵志辉 on 2019/9/28.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import "FBSArticalFooterView.h"
@interface FBSArticalFooterView()
@property(nonatomic, strong)UIButton *say;
@property(nonatomic, strong)UIButton *collect;
@property(nonatomic, strong)UIButton *share;
@property(nonatomic, strong)UIButton *look;

@end

@implementation FBSArticalFooterView
- (instancetype)init{
    if (self = [super init]) {
        self.backgroundColor = Hexcolor(0xFFFFFF);
        [self addSubview:self.say];

        [self addSubview:self.collect];
        
        [self addSubview:self.share];
        
        [self addSubview:self.look];
        
        [self createView];
    }
    return self;
}
- (void)createView{
    
    [self.say mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self).offset(WScale(12));
        make.top.mas_equalTo(self).offset(WScale(6));
        make.width.mas_equalTo(WScale(210));
        make.height.mas_equalTo(WScale(33));
    }];
    [self.look mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.say.mas_right).offset(WScale(24));
        make.centerY.mas_equalTo(self.say.mas_centerY);
        make.width.mas_equalTo(WScale(16));
        make.height.mas_equalTo(WScale(16));
    }];
    [self.collect mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.look.mas_right).offset(WScale(36));
        make.centerY.mas_equalTo(self.say.mas_centerY);
        make.width.mas_equalTo(WScale(16));
        make.height.mas_equalTo(WScale(16));
    }];
    
    [self.share mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.collect.mas_right).offset(WScale(36));
        make.centerY.mas_equalTo(self.say.mas_centerY);
        make.width.mas_equalTo(WScale(16));
        make.height.mas_equalTo(WScale(16));
    }];
    
}
- (void)cllick:(UIButton *)sender{
    if ([self.delegate respondsToSelector:@selector(clickActivityFooterView:clickType:)]) {
        [self.delegate clickActivityFooterView:self clickType:sender.tag];
    }
}
- (UIButton *)say{
    if (!_say) {
        _say = [[UIButton alloc] init];
        _say.tag = 200000;
        [_say setBackgroundImage:[UIImage imageNamed:@"立刻报名button"] forState:UIControlStateNormal];
        [_say addTarget:self action:@selector(cllick:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _say;
}
- (UIButton *)look{
    if (!_look) {
        _look = [[UIButton alloc] init];
        _look.tag = 200001;
        [_look setBackgroundImage:[UIImage imageNamed:@"mine_comment"] forState:UIControlStateNormal];
        [_look addTarget:self action:@selector(cllick:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _look;
}
- (UIButton *)collect{
    if (!_collect) {
        _collect = [[UIButton alloc] init];
        _collect.tag = 200002;
        [_collect setBackgroundImage:[UIImage imageNamed:@"mine_collection"] forState:UIControlStateNormal];
        [_collect setBackgroundImage:[UIImage imageNamed:@"collection_s"] forState:UIControlStateSelected];
        [_collect addTarget:self action:@selector(cllick:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _collect;
}
- (void)changeSelectCollection:(BOOL)select{
    self.collect.selected = select;
}


- (UIButton *)share{
    if (!_share) {
        _share = [[UIButton alloc] init];
        _share.tag = 200003;
        [_share setBackgroundImage:[UIImage imageNamed:@"share"] forState:UIControlStateNormal];
        [_share addTarget:self action:@selector(cllick:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _share;
}

@end
