//
//  FBSarticleNavView.m
//  Forbes
//
//  Created by 赵志辉 on 2019/9/28.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import "FBSarticleNavView.h"

@interface FBSarticleNavView()
@property(nonatomic, strong)UIImageView *image;
@property(nonatomic, strong)UILabel *name;
@property(nonatomic, strong)UIButton *attent;
@end

@implementation FBSarticleNavView
- (instancetype)init{
    if (self = [super init]) {
        self.backgroundColor = Hexcolor(0xFFFFFF);
        [self addSubview:self.image];
        
        [self addSubview:self.name];
        
        [self addSubview:self.attent];
        self.attent.hidden = YES;
        [self createView];
    }
    return self;
}
- (void)createView{
    
    [self.image mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self);
        make.centerY.mas_equalTo(self.centerY);
        make.width.mas_equalTo(WScale(24));
        make.height.mas_equalTo(WScale(24));
    }];
    
    [self.name mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.image.mas_right).offset(WScale(5));
        make.centerY.mas_equalTo(self.centerY);
        make.right.mas_equalTo(self).offset(WScale(-14));
        make.height.mas_equalTo(WScale(35));
    }];
    [self.attent mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(WScale(64));
        make.centerY.mas_equalTo(self.centerY);
        make.right.mas_equalTo(self);
        make.height.mas_equalTo(WScale(22));
    }];
}
- (void)setImage:(NSString *)str name:(NSString *)name{
    if (str.length) {
        [self.image sd_setImageWithURL:[NSURL URLWithString:str]];
    }
    if (name.length){
        self.name.text = name;
    }
}
- (UIImageView *)image{
    if (!_image) {
        _image = [[UIImageView alloc] init];
        _image.layer.cornerRadius = WScale(12);
        _image.layer.masksToBounds = YES;
        
    }
    return _image;
}
- (void)setDataModel:(FBSWenZhangDataModel *)DataModel{
    [self.image sd_setImageWithURL:[NSURL URLWithString:DataModel.file_url]];
    self.name.text = DataModel.nickname;
}


- (UILabel *)name{
    if (!_name) {
        _name = [[UILabel alloc] init];
        _name.font = kLabelFontSize15;
        _name.textColor = Hexcolor(0x333333);
        _name.numberOfLines = 2;
    }
    return _name;
}

-(UIButton *)attent{
    if (!_attent) {
        _attent = [[UIButton alloc] init];
        [_attent setImage:[UIImage imageNamed:@"follow_sel"] forState:UIControlStateNormal];
    }
    return _attent;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
