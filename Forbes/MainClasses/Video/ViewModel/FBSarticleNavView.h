//
//  FBSarticleNavView.h
//  Forbes
//
//  Created by 赵志辉 on 2019/9/28.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FBSWenZhangModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface FBSarticleNavView : UIView
@property(nonatomic, strong)FBSWenZhangDataModel *DataModel;
- (void)setImage:(NSString *)str name:(NSString *)name;

@end

NS_ASSUME_NONNULL_END
