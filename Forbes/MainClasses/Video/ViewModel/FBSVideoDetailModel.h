//
//  FBSVideoDetailModel.h
//  Forbes
//
//  Created by 赵志辉 on 2019/9/27.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import "FBSTableViewModel.h"
#import "FBSOneImageCell.h"
#import "FBSVideoListCell.h"
#import "FBSPinglunItem.h"
#import "FBSRecordPinglunLikeCollectionHositoryModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface FBSVideoDetailModel : FBSTableViewModel
@property(nonatomic, strong)NSString *id;
@property(nonatomic, strong)NSString *title;
@property(nonatomic, strong)NSString *has_fav;
@property(nonatomic, strong)NSString *has_like;
@property(nonatomic, strong) FBSRecordPinglunLikeCollectionHositoryModel *PinglunLikeCollectionHositoryModel;


- (void)rqInterster:(void(^)(BOOL success,NSInteger count))complete;
- (void)rqSendIndex:(FBSPinglunItem *)indexItem text:(NSString *)text block:(void(^)(BOOL success,NSInteger count))complete;
- (void)rqLikeIndex:(FBSPinglunItem *)indexItem block:(void(^)(BOOL success,NSInteger count))complete;
- (void)rqfans:(FBSPinglunItem *)indexItem block:(void(^)(BOOL success,NSInteger count))complete;
- (void)rqwenzhangLike:(FBSPinglunItem *)indexItem block:(void(^)(BOOL success,NSInteger count))complete;
@end

NS_ASSUME_NONNULL_END
