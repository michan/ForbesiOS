//
//  FBSArticalModel.h
//  Forbes
//
//  Created by 赵志辉 on 2019/9/28.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import "FBSBaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface FBSArticalDataModelTicketsItem :FBSBaseModel
@property (nonatomic , copy) NSString              *id;
@property (nonatomic , copy) NSString              * title;
@property (nonatomic , copy) NSString              * type;
@property (nonatomic , copy) NSString              * price;
@property (nonatomic , copy) NSString              * remarks;
@property (nonatomic , copy) NSString              * number;
@property (nonatomic , copy) NSString              * receive_number;
@property (nonatomic , copy) NSString              * choseNum;


@end


@interface FBSArticalDataModelArea_date_listItem :FBSBaseModel
@property (nonatomic , copy) NSString              * id;
@property (nonatomic , copy) NSString              * area_and_date;

@end


@interface FBSArticalDataModel :FBSBaseModel
@property (nonatomic , copy) NSString              * id;
@property (nonatomic , copy) NSString              * user_id;
@property (nonatomic , copy) NSString              * title;
@property (nonatomic , copy) NSString              * file_id;
@property (nonatomic , copy) NSString              * describe;
@property (nonatomic , copy) NSString              * limit_number;
@property (nonatomic , copy) NSString              * start_time;
@property (nonatomic , copy) NSString              * end_time;
@property (nonatomic , copy) NSString              * type;
@property (nonatomic , copy) NSString              * activity_time;
@property (nonatomic , copy) NSString              * activity_address;
@property (nonatomic , copy) NSString              * activity_sponsor;
@property (nonatomic , copy) NSString              * created_at;
@property (nonatomic , copy) NSString              * updated_at;
@property (nonatomic , copy) NSString              * file_url;
@property (nonatomic , copy) NSString              * maxCounts;
@property (nonatomic , copy) NSString              * hasCounts;
@property (nonatomic , strong) NSArray <FBSArticalDataModelTicketsItem *>              * tickets;
@property (nonatomic , strong) NSArray <FBSArticalDataModelArea_date_listItem *>              * area_date_list;
@property(nonatomic, copy) NSString *has_fav;
@property(nonatomic, copy) NSString *has_like;


@end


@interface FBSArticalModel :NSObject
@property (nonatomic , strong) FBSArticalDataModel              * data;


@end

NS_ASSUME_NONNULL_END
