//
//  FBSarticleHeaderView.h
//  Forbes
//
//  Created by 赵志辉 on 2019/9/27.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FBSWenZhangModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface FBSarticleHeaderView : UIView

@property(nonatomic, strong)FBSWenZhangDataModel *DataModel;
@property(nonatomic, strong)UIButton *attent;


@end

NS_ASSUME_NONNULL_END
