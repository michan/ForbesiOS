//
//  FBSArticalModel.m
//  Forbes
//
//  Created by 赵志辉 on 2019/9/28.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import "FBSArticalModel.h"

@implementation FBSArticalDataModelArea_date_listItem
@end


@implementation FBSArticalDataModelTicketsItem
@end


@implementation FBSArticalDataModel

+ (NSDictionary *)mj_objectClassInArray {
    return @{@"area_date_list" : @"FBSArticalDataModelArea_date_listItem",@"tickets" : @"FBSArticalDataModelTicketsItem"};
}
@end


@implementation FBSArticalModel
@end
