//
//  FBSActivityFooterView.h
//  Forbes
//
//  Created by 赵志辉 on 2019/9/28.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import <UIKit/UIKit.h>




NS_ASSUME_NONNULL_BEGIN

typedef enum : NSUInteger {
    clickTypeCollect = 100000,
    clickTypeShare = 100001,
    clickTypeBaoming = 100002,
} clickType;


@class FBSActivityFooterView;
@protocol FBSActivityFooterViewDelegate <NSObject>

- (void)clickActivityFooterView:(FBSActivityFooterView *)view clickType:(clickType)type;

@end

@interface FBSActivityFooterView : UIView

@property(nonatomic,weak)id<FBSActivityFooterViewDelegate> delegate;
- (void)changeSelectCollection:(BOOL)select;

@end

NS_ASSUME_NONNULL_END
