//
//  FBSArticalFooterView.h
//  Forbes
//
//  Created by 赵志辉 on 2019/9/28.
//  Copyright © 2019 周灿华. All rights reserved.
//Lo

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
typedef enum : NSUInteger {
    ActivityTypeSay = 200000,
    ActivityTypelook = 200001,
    ActivityTypeCollect = 200002,
    ActivityTypeShare = 200003,
} ActivityType;


@class FBSArticalFooterView;
@protocol FBSArticalFooterViewDelegate <NSObject>

- (void)clickActivityFooterView:(FBSArticalFooterView *)view clickType:(ActivityType)type;

@end
@interface FBSArticalFooterView : UIView
@property(nonatomic,weak)id<FBSArticalFooterViewDelegate> delegate;
- (void)changeSelectCollection:(BOOL)select;
@end

NS_ASSUME_NONNULL_END
