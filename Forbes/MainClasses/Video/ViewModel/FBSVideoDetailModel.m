//
//  FBSVideoDetailModel.m
//  Forbes
//
//  Created by 赵志辉 on 2019/9/27.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import "FBSVideoDetailModel.h"
#import "FBSVideoInterisonModel.h"
#import "FBSPinglunModel.h"
#import "FBSPinglunItem.h"
#import "HLWJMD5.H"

@interface FBSVideoDetailModel()

@property(nonatomic ,strong)FBSVideoInterisonModel *model;
@property(nonatomic ,strong)FBSPinglunModel *PinglunModel;
@property (nonatomic ,strong)FBSGetRequest *detailAPI;
@property (nonatomic ,strong)FBSGetRequest *IntersterAPI;
@property(nonatomic, strong) FBSPostRequest *sendApi;
@property (nonatomic ,strong)FBSPostRequest *isLikeApi;
@property(nonatomic, strong) FBSPostRequest *favs;

@end

@implementation FBSVideoDetailModel

- (void)rqInterster:(void(^)(BOOL success,NSInteger count))complete{
    self.IntersterAPI.requestParameter = @{@"id":self.id};
    [self.IntersterAPI startWithSuccess:^(YBNetworkResponse * _Nonnull response) {
        
        self.model = [FBSVideoInterisonModel mj_objectWithKeyValues:response.responseObject];
        
        [self rqContent:^(BOOL success, NSInteger count) {
           [self createItem];
           complete(YES, 0);
        }];
    } failure:^(YBNetworkResponse * _Nonnull response) {
        complete(NO, 0);
    }];
}
- (void)rqContent:(void(^)(BOOL success,NSInteger count))complete{
    self.detailAPI.requestParameter = @{@"object_type":@"3",@"object_id":self.id};
    [self.detailAPI startWithSuccess:^(YBNetworkResponse * _Nonnull response) {
        self.PinglunModel = [FBSPinglunModel mj_objectWithKeyValues:response.responseObject];
        complete(YES, 0);
    } failure:^(YBNetworkResponse * _Nonnull response) {
        complete(NO, 0);
    }];
}
- (void)rqLikeIndex:(FBSPinglunItem *)indexItem block:(void(^)(BOOL success,NSInteger count))complete{
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    FBSUserData *user = [FBSUserData sharedData];
    [dic setObject:user.useId forKey:@"user_id"];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"yyyy-MM-dd";
    NSString *plainText = [NSString stringWithFormat:@"%@%@",user.account,[formatter stringFromDate:[NSDate date]]];
    [dic setObject:[HLWJMD5 MD5ForLower32Bate:plainText] forKey:@"token"];
    [dic setObject:@"3" forKey:@"object_type"];
    [dic setObject:self.id forKey:@"object_id"];
    [dic setObject:indexItem.id forKey:@"comment_id"];
    if([indexItem.has_like isEqualToString:@"Y"]){
        [dic setObject:@"unlike" forKey:@"action"];

    }else{
        [dic setObject:@"like" forKey:@"action"];
    }
    self.isLikeApi.requestParameter = dic;
    [self.isLikeApi startWithSuccess:^(YBNetworkResponse * _Nonnull response) {
        NSInteger code = [response.responseObject[@"code"] integerValue];
        if (code == 200) {
            complete(YES, 0);
        }else{
            complete(NO,0);
        }
    } failure:^(YBNetworkResponse * _Nonnull response) {
        complete(NO, 0);
    }];
}
- (void)rqSendIndex:(FBSPinglunItem *)indexItem text:(NSString *)text block:(void(^)(BOOL success,NSInteger count))complete{
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    FBSUserData *user = [FBSUserData sharedData];
    [dic setObject:user.useId forKey:@"user_id"];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"yyyy-MM-dd";
    NSString *plainText = [NSString stringWithFormat:@"%@%@",user.account,[formatter stringFromDate:[NSDate date]]];
    [dic setObject:[HLWJMD5 MD5ForLower32Bate:plainText] forKey:@"token"];
    [dic setObject:@"3" forKey:@"object_type"];
    [dic setObject:self.id forKey:@"object_id"];
    if (indexItem == nil) {
        [dic setObject:@"0" forKey:@"pid"];
    }else{
        [dic setObject:indexItem.id forKey:@"pid"];
    }
    [dic setObject:text forKey:@"content"];
    self.sendApi.requestParameter = dic;
    [self.sendApi startWithSuccess:^(YBNetworkResponse * _Nonnull response) {
        NSInteger code = [response.responseObject[@"code"] integerValue];
        if (code == 200) {
            complete(YES, 0);
        }else{
            complete(NO,0);
        }
    } failure:^(YBNetworkResponse * _Nonnull response) {
        complete(NO, 0);
    }];
}
- (void)rqfans:(FBSPinglunItem *)indexItem block:(void(^)(BOOL success,NSInteger count))complete{
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    FBSUserData *user = [FBSUserData sharedData];
    [dic setObject:user.useId forKey:@"user_id"];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"yyyy-MM-dd";
    NSString *plainText = [NSString stringWithFormat:@"%@%@",user.account,[formatter stringFromDate:[NSDate date]]];
    [dic setObject:[HLWJMD5 MD5ForLower32Bate:plainText] forKey:@"token"];
    [dic setObject:@"3" forKey:@"object_type"];
    [dic setObject:self.title forKey:@"object_title"];
    if ([self.has_fav isEqualToString:@"N"]) {
        [dic setObject:@"add" forKey:@"action"];
    }else{
        [dic setObject:@"remove" forKey:@"action"];
    }
    [dic setObject:self.id forKey:@"object_id"];
    self.favs.requestParameter = dic;
    [self.favs startWithSuccess:^(YBNetworkResponse * _Nonnull response) {
        NSInteger code = [response.responseObject[@"code"] integerValue];
        if (code == 200) {
            complete(YES, 0);
        }else{
            complete(NO,0);
        }
    } failure:^(YBNetworkResponse * _Nonnull response) {
        complete(NO, 0);
    }];
}
- (void)rqwenzhangLike:(FBSPinglunItem *)indexItem block:(void(^)(BOOL success,NSInteger count))complete{
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    FBSUserData *user = [FBSUserData sharedData];
    [dic setObject:user.useId forKey:@"user_id"];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"yyyy-MM-dd";
    NSString *plainText = [NSString stringWithFormat:@"%@%@",user.account,[formatter stringFromDate:[NSDate date]]];
    [dic setObject:[HLWJMD5 MD5ForLower32Bate:plainText] forKey:@"token"];
    [dic setObject:@"3" forKey:@"object_type"];
    [dic setObject:self.title forKey:@"object_title"];
    if ([self.has_like isEqualToString:@"N"]) {
        [dic setObject:@"like" forKey:@"action"];
    }else{
        [dic setObject:@"unlike" forKey:@"action"];
    }
    [dic setObject:self.id forKey:@"object_id"];
    [self.PinglunLikeCollectionHositoryModel rqLikeobject_type:dic[@"object_type"] object_id:dic[@"object_id"] action:dic[@"action"] object_title:dic[@"object_title"] block:^(BOOL success, NSInteger count) {
        if (success) {
            complete(YES,0);
        }else{
            complete(NO,0);
        }
    }];
}
- (void)createItem{
    for (FBSVideoInterisonModelItemsItem *item in self.model.data.items) {
        FBSOneImageItem *oneImageItem = [[FBSOneImageItem alloc] init];
        oneImageItem.detailType = DetailPageTypeVideo;
        oneImageItem.ID = item.category_id;
        oneImageItem.content = item.title;
        oneImageItem.videoUrl = item.file_url;
        oneImageItem.imgUrl = item.file_img;
        oneImageItem.time = [NSString publishedTimeFormatWithTimeStamp:item.created_at];
        oneImageItem.cellHeight = WScale(95);
        [self.items addObject:oneImageItem];
    }
    for (FBSPinglunModelItemsItem *item in self.PinglunModel.data.items) {
        NSDictionary *dic = [item mj_keyValues];
        FBSPinglunItem *itemP = [[FBSPinglunItem alloc] init];
        itemP = [FBSPinglunItem mj_objectWithKeyValues:dic];
        [self.items addObject:itemP];
    }
}
- (FBSGetRequest *)detailAPI {
    if (!_detailAPI) {
        _detailAPI = [[FBSGetRequest alloc] init];
        _detailAPI.requestURI = @"comment/list";
    }
    return _detailAPI;
}
- (FBSGetRequest *)IntersterAPI {
    if (!_IntersterAPI) {
        _IntersterAPI = [[FBSGetRequest alloc] init];
        _IntersterAPI.requestURI = @"tv/interest";
    }
    return _IntersterAPI;
}
- (FBSPostRequest *)isLikeApi {
    if (!_isLikeApi) {
        _isLikeApi = [[FBSPostRequest alloc] init];
        _isLikeApi.requestURI = @"comment/like";
    }
    return _isLikeApi;
}
- (FBSPostRequest *)sendApi {
    if (!_sendApi) {
        _sendApi = [[FBSPostRequest alloc] init];
        _sendApi.requestURI = @"comment/add";
    }
    return _sendApi;
}
- (FBSPostRequest *)favs {
    if (!_favs) {
        _favs = [[FBSPostRequest alloc] init];
        _favs.requestURI = @"user/favs";
    }
    return _favs;
}
- (FBSRecordPinglunLikeCollectionHositoryModel *)PinglunLikeCollectionHositoryModel
{
    if (!_PinglunLikeCollectionHositoryModel) {
        _PinglunLikeCollectionHositoryModel = [[FBSRecordPinglunLikeCollectionHositoryModel alloc] init];
    }
    return _PinglunLikeCollectionHositoryModel;
}
@end
