//
//  FBSWenZhangModel.h
//  Forbes
//
//  Created by 赵志辉 on 2019/9/28.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import "FBSBaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface FBSWenZhangDataModelPartsItem :NSObject
@property (nonatomic , copy) NSString              * content;

@end


@interface FBSWenZhangDataModel :NSObject
@property (nonatomic , copy) NSString              * id;
@property (nonatomic , copy) NSString              * title;
@property (nonatomic , copy) NSString              * Description;
@property (nonatomic , copy) NSString              * open_comment;
@property (nonatomic , copy) NSString              * user_id;
@property (nonatomic , copy) NSString              * status;
@property (nonatomic , copy) NSString              * time;
@property (nonatomic , copy) NSString              * updated_at;
@property (nonatomic , copy) NSString              * select_count;
@property (nonatomic , copy) NSString              * nickname;
@property (nonatomic , copy) NSString              * file_url;
@property (nonatomic , strong) NSArray <FBSWenZhangDataModelPartsItem *>              * parts;
@property (nonatomic , copy) NSString              * share_link;
@property(nonatomic, copy) NSString *has_fav;
@property(nonatomic, copy) NSString *has_like;


@end


@interface FBSWenZhangModel :NSObject
@property (nonatomic , strong) FBSWenZhangDataModel              * data;


@end

NS_ASSUME_NONNULL_END
