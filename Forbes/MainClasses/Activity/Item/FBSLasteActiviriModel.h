//
//  FBSNewsActiviriModel.h
//  Forbes
//
//  Created by 赵志辉 on 2019/9/23.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import "FBSBaseModel.h"
#import "FBSActivitiesModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface FBSNewsActiviriPageItemModel :NSObject
@property (nonatomic , assign) NSInteger              id;
@property (nonatomic , copy) NSString              * title;
@property (nonatomic , copy) NSString              * activity_address;
@property (nonatomic , copy) NSString              * file_url;
@property (nonatomic , copy) NSString              * activity_sponsor;
@property (nonatomic , copy) NSString              * activity_time;
@property (nonatomic , assign) NSInteger              start_time;
@property (nonatomic , assign) NSInteger              end_time;
@property (nonatomic , strong) NSArray <TicketsItem *>              * tickets;

@end


@interface FBSNewsActiviriPageModel :NSObject
//@property (nonatomic , assign) NSInteger              total;
//@property (nonatomic , strong) NSMutableArray <FBSNewsActiviriPageItemModel *>              * items;
//@property (nonatomic , assign) NSInteger              page;
//@property (nonatomic , assign) NSInteger              limit;
@property (nonatomic , copy) NSString              * title;

@property (nonatomic , copy) NSString              * slug;

@property (nonatomic , copy) NSString              * tid;

@property (nonatomic , copy) NSString              * file_id;

@property (nonatomic , copy) NSString              * file_url;


@end


@interface FBSLasteActiviriModel :NSObject
@property (nonatomic , strong) NSArray              * data;
@property (nonatomic , assign) NSInteger              success;
@property (nonatomic , assign) NSInteger              code;

@end



NS_ASSUME_NONNULL_END
