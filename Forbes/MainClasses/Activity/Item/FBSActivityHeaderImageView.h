//
//  FBSActivityHeaderImageView.h
//  Forbes
//
//  Created by 赵志辉 on 2019/9/27.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FBSArticalModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface FBSActivityHeaderImageView : UIView

@property(nonatomic ,strong)FBSArticalDataModel *DataModel;
@end

NS_ASSUME_NONNULL_END
