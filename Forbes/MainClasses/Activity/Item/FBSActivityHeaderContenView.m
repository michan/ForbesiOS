//
//  FBSActivityHeaderContenView.m
//  Forbes
//
//  Created by 赵志辉 on 2019/9/27.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import "FBSActivityHeaderContenView.h"

@interface FBSActivityHeaderContenView()
@property(nonatomic, strong)UILabel *image;
@property(nonatomic, strong)UILabel *time;
@property(nonatomic, strong)UILabel * address;
@property(nonatomic, strong)UILabel * company;
@property(nonatomic, strong)UIView * line;
@end

@implementation FBSActivityHeaderContenView


- (instancetype)init{
    if (self = [super init]) {
        self.backgroundColor = Hexcolor(0xFFFFFF);
        [self addSubview:self.image];
        self.image.hidden = YES;
        
        [self addSubview:self.time];
        
        [self addSubview:self.address];
        
        [self addSubview:self.company];
        
        [self addSubview:self.line];
        
        [self createView];
    }
    return self;
}
- (void)createView{
    
    [self.image mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self).offset(WScale(14));
        make.top.mas_equalTo(self).offset(WScale(14));
        make.right.mas_equalTo(self).offset(WScale(-14));
        make.height.mas_equalTo(WScale(0));
    }];
    
    [self.time mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self).offset(WScale(14));
        make.top.mas_equalTo(self.image.mas_bottom).offset(WScale(8));
        make.right.mas_equalTo(self).offset(WScale(-14));
        make.height.mas_equalTo(WScale(17));
    }];
    [self.address mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self).offset(WScale(14));
        make.top.mas_equalTo(self.time.mas_bottom).offset(WScale(8));
        make.right.mas_equalTo(self).offset(WScale(-14));
        make.height.mas_equalTo(WScale(17));
    }];
    [self.company mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self).offset(WScale(14));
        make.top.mas_equalTo(self.address.mas_bottom).offset(WScale(8));
        make.height.mas_equalTo(WScale(17));
        make.right.mas_equalTo(self).offset(WScale(-14));
    }];
    [self.line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self);
        make.bottom.mas_equalTo(self.mas_bottom);
        make.right.mas_equalTo(self);
        make.height.mas_equalTo(WScale(5));
    }];
}

- (UILabel *)image{
    if (!_image) {
        _image = [[UILabel alloc] init];
        _image.textColor = Hexcolor(0xD0021B);
        _image.font =kLabelFontSize12;
    }
    return _image;
}
-(void)setDataModel:(FBSArticalDataModel *)DataModel{
    FBSArticalDataModelTicketsItem *item = [DataModel.tickets firstObject];
    self.image.text = [NSString stringWithFormat:@"限额%@人（已报名%@）",item.number,item.receive_number];
    self.time.text = [NSString stringWithFormat:@"活动时间：%@",DataModel.activity_time];
    self.address.text = [NSString stringWithFormat:@"活动地址：%@",DataModel.activity_address];
    self.company.text = [NSString stringWithFormat:@"主办单位：%@",DataModel.activity_sponsor];
}
- (UILabel *)time{
    if (!_time) {
        _time = [[UILabel alloc] init];
        _time.font = kLabelFontSize12;
        _time.textColor = Hexcolor(0x333333);
    }
    return _time;
}

-(UILabel *)address{
    if (!_address) {
        _address = [[UILabel alloc] init];
        _address.font = kLabelFontSize12;
        _address.textColor = Hexcolor(0x333333);
    }
    return _address;
}
-(UILabel *)company{
    if (!_company) {
        _company = [[UILabel alloc] init];
        _company.font = kLabelFontSize12;
        _company.textColor = Hexcolor(0x333333);
        _company.layer.borderColor = Hexcolor(0x8C734B).CGColor;
    }
    return _company;
}
- (UIView *)line{
    if (!_line) {
        _line = [[UIView alloc] init];
        _line.backgroundColor = Hexcolor(0xF6F7FB);
    }
    return _line;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
