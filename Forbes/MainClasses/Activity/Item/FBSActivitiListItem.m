//
//  FBSActivitiListItem.m
//  Forbes
//
//  Created by 赵志辉 on 2019/9/23.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import "FBSActivitiListItem.h"

@implementation FBSActivitiListItem
+ (NSDictionary *)mj_objectClassInArray {
    
    return @{@"data" : @"FBSActivityLastestItem"};
}
@end
