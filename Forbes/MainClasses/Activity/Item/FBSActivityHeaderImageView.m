//
//  FBSActivityHeaderImageView.m
//  Forbes
//
//  Created by 赵志辉 on 2019/9/27.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import "FBSActivityHeaderImageView.h"

@interface FBSActivityHeaderImageView()

@property(nonatomic, strong)UIImageView *image;
@property(nonatomic, strong)YYLabel *title;
@property(nonatomic, strong)UILabel * discussAndTime;
@property(nonatomic, strong)UILabel * money;
@property(nonatomic, strong)UIView * line;

@end

@implementation FBSActivityHeaderImageView

- (instancetype)init{
    if (self = [super init]) {
        self.backgroundColor = Hexcolor(0xFFFFFF);

        [self addSubview:self.image];
        
        [self addSubview:self.title];

        [self addSubview:self.discussAndTime];

        [self addSubview:self.money];

        [self addSubview:self.line];

        [self createView];
    }
    return self;
}
- (void)createView{
    
    [self.image mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self).offset(WScale(14));
        make.top.mas_equalTo(self).offset(WScale(15));
        make.right.mas_equalTo(self).offset(WScale(-14));
        make.height.mas_equalTo(WScale(197));
    }];
    self.title.numberOfLines = 0;
    self.title.preferredMaxLayoutWidth = (kScreenWidth - WScale(28));
    [self.title setContentHuggingPriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisHorizontal];
    [self.title mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self).offset(WScale(14));
        make.top.mas_equalTo(self.image.mas_bottom).offset(WScale(18));
        make.right.mas_equalTo(self).offset(WScale(-14));
    }];
    [self.discussAndTime mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self).offset(WScale(14));
        make.top.mas_equalTo(self.title.mas_bottom).offset(WScale(5));
        make.right.mas_equalTo(self).offset(WScale(-14));
        make.height.mas_equalTo(WScale(17));
    }];
    [self.money mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self).offset(WScale(14));
        make.top.mas_equalTo(self.discussAndTime.mas_bottom).offset(WScale(5));
        make.height.mas_equalTo(WScale(40));
        make.width.mas_equalTo(WScale(78));
    }];
    [self.line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self);
        make.top.mas_equalTo(self.money.mas_bottom).offset(WScale(24));
        make.right.mas_equalTo(self);
        make.height.mas_equalTo(WScale(1));
        make.bottom.mas_equalTo(self.mas_bottom);
    }];
}
- (void)setDataModel:(FBSArticalDataModel *)DataModel{
    [self.image sd_setImageWithURL:[NSURL URLWithString:DataModel.file_url]];
    self.title.text = DataModel.title;
    self.discussAndTime.text = DataModel.activity_time;
    FBSArticalDataModelTicketsItem *item = [DataModel.tickets firstObject];
    self.money.text = item.price;
}
- (UIImageView *)image{
    if (!_image) {
        _image = [[UIImageView alloc] init];
    }
    return _image;
}

- (YYLabel *)title{
    if (!_title) {
        _title = [[YYLabel alloc] init];
        _title.font = kLabelFontSize18;
        _title.textColor = Hexcolor(0x333333);
        _title.textVerticalAlignment = YYTextVerticalAlignmentTop;
    }
    return _title;
}

-(UILabel *)discussAndTime{
    if (!_discussAndTime) {
        _discussAndTime = [[UILabel alloc] init];
        _discussAndTime.font = kLabelFontSize12;
        _discussAndTime.textColor = Hexcolor(0x999999);
    }
    return _discussAndTime;
}
-(UILabel *)money{
    if (!_money) {
        _money = [[UILabel alloc] init];
        _money.font = kLabelFontSize12;
        _money.textColor = Hexcolor(0x8C734B);
        _money.textAlignment = NSTextAlignmentCenter;
        _money.layer.borderColor = Hexcolor(0x8C734B).CGColor;
        _money.layer.borderWidth = WScale(1);
        _money.layer.cornerRadius = WScale(2);

    }
    return _money;
}
- (UIView *)line{
    if (!_line) {
        _line = [[UIView alloc] init];
        _line.backgroundColor = Hexcolor(0xD8D8D8);
    }
    return _line;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
