//
//  FBSSelectItem.h
//  Forbes
//
//  Created by 赵志辉 on 2019/9/25.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import "FBSItem.h"

NS_ASSUME_NONNULL_BEGIN

typedef enum : NSUInteger {
    SelectTypeNone,
    SelectTypeMan,
    SelectTypeWOMan
}SelectType;

@interface FBSSelectItem : FBSItem

@property (nonatomic , copy) NSString *title;
@property (nonatomic , copy) NSString *detail;
@property (nonatomic , assign)SelectType typeMan;

@end

NS_ASSUME_NONNULL_END
