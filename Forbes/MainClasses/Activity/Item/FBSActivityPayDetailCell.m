//
//  FBSActivityPayDetailCell.m
//  Forbes
//
//  Created by 赵志辉 on 2019/9/26.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import "FBSActivityPayDetailCell.h"
#import "FBSActivityPayDetailItem.h"
#import "PPNumberButton.h"
#import "UIView+FBSLayer.h"
#import "NSMutableAttributedString+FBSAttributedString.h"

@interface FBSActivityPayDetailCell()<PPNumberButtonDelegate>
@property (nonatomic, strong) UILabel *contenL;
@property (nonatomic, strong) UIView *backView;
@property (nonatomic, strong) UIView *lineViewTop;
@property (nonatomic, strong) UIView *lineViewBottom;
@property (nonatomic, strong) YYLabel *titledetail;
@property (nonatomic, strong) UILabel *timeAndAddress;

@property (nonatomic, strong) UILabel *ticketType;

@property (nonatomic, strong) UILabel *ticketPrice;

@property (nonatomic, strong) UILabel *examineVerify;

@property (nonatomic, strong) UILabel *heji;

@property (nonatomic, strong) UILabel *totlePrice;

@property (nonatomic, strong)PPNumberButton *numberButton;

@property (nonatomic, strong) UIView *lineView;



@end
@implementation FBSActivityPayDetailCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        self.backgroundColor = Hexcolor(0xF6F7FB);
        [self.contentView addSubview:self.contenL];
        [self.contentView addSubview:self.backView];
        [self.backView addSubview:self.titledetail];
        [self.backView addSubview:self.timeAndAddress];
        [self.backView addSubview:self.lineViewTop];
        [self.backView addSubview:self.lineViewBottom];
        
        [self.backView addSubview:self.ticketType];
        [self.backView addSubview:self.ticketPrice];
        
        [self.backView addSubview:self.examineVerify];
        [self.backView addSubview:self.heji];
        
        [self.backView addSubview:self.totlePrice];
       
        [self.contentView addSubview:self.lineView];

        //[self.contentView addSubview:self.numberButton];
        

        [self.contenL mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(WScale(17));
            make.top.mas_equalTo(self).offset(WScale(15));
            make.height.mas_equalTo(WScale(25));
            make.right.mas_equalTo(WScale(-17));
            
        }];
        [self.backView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.contenL);
            make.top.mas_equalTo(self.mas_top).offset(WScale(45));
            make.right.mas_equalTo(self.contenL);
            make.bottom.mas_equalTo(self.mas_bottom).offset(WScale(-23));
        }];
        
        
        [self.titledetail mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.backView).offset(WScale(10));
            make.top.mas_equalTo(self.backView).offset(WScale(10));
            make.right.mas_equalTo(self.backView).offset(WScale(-11));
            make.height.mas_equalTo(WScale(52));
        }];
        
        [self.timeAndAddress mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.backView).offset(WScale(10));
            make.top.mas_equalTo(self.titledetail.mas_bottom).offset(WScale(4));
            make.right.mas_equalTo(self.backView).offset(WScale(-11));
            make.height.mas_equalTo(WScale(30));
        }];
        
        [self.lineViewTop mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.backView);
            make.top.mas_equalTo(self.timeAndAddress.mas_bottom).offset(WScale(4));
            make.right.mas_equalTo(self.backView);
            make.height.mas_equalTo(WScale(17));
        }];
        
        [self.ticketType mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.backView).offset(WScale(10));
            make.top.mas_equalTo(self.lineViewTop.mas_bottom).offset(WScale(9));
            make.right.mas_equalTo(self.backView).offset(WScale(-11));
            make.height.mas_equalTo(WScale(17));
        }];
        [self.ticketPrice mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.backView).offset(WScale(10));
            make.top.mas_equalTo(self.ticketType.mas_bottom).offset(WScale(3));
            make.right.mas_equalTo(self.backView).offset(WScale(-11));
            make.height.mas_equalTo(WScale(17));
        }];
        [self.examineVerify mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.backView).offset(WScale(10));
            make.top.mas_equalTo(self.ticketPrice.mas_bottom).offset(WScale(3));
            make.right.mas_equalTo(self.backView).offset(WScale(-11));
            make.height.mas_equalTo(WScale(17));
        }];
//        [self.numberButton mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.width.mas_equalTo(WScale(60));
//            make.centerY.mas_equalTo(self.examineVerify.mas_centerY);
//            make.right.mas_equalTo(self.backView).offset(WScale(-11));
//            make.height.mas_equalTo(WScale(17));
//        }];
        
        [self.lineViewBottom mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.backView);
            make.top.mas_equalTo(self.examineVerify.mas_bottom).offset(WScale(2));
            make.right.mas_equalTo(self.backView);
            make.height.mas_equalTo(WScale(17));
        }];
        
        [self.totlePrice setContentHuggingPriority:UILayoutPriorityRequired
                                        forAxis:UILayoutConstraintAxisHorizontal];
        [self.heji setContentHuggingPriority:UILayoutPriorityDefaultLow
                                       forAxis:UILayoutConstraintAxisHorizontal];
        
        [self.totlePrice sizeToFit];
        [self.totlePrice mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.heji.mas_right);
            make.top.mas_equalTo(self.lineViewBottom.mas_bottom).offset(WScale(8));
            make.right.mas_equalTo(self.backView).offset(WScale(-11));
            make.height.mas_equalTo(WScale(25));
        }];
        [self.heji mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.backView).offset(WScale(10));
            make.top.mas_equalTo(self.lineViewBottom.mas_bottom).offset(WScale(11));
            make.height.mas_equalTo(WScale(20));
            make.right.mas_equalTo(self.totlePrice.mas_left);
        }];
        
        
        
        [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.mas_left);
            make.height.mas_equalTo(WScale(5));
            make.right.mas_equalTo(self);
            make.bottom.mas_equalTo(self.mas_bottom);
        }];
        
        
        
        
        
    }
    
    return self;
}

- (void)setItem:(FBSActivityPayDetailItem *)item{
    [super setItem:item];
    
    self.timeAndAddress.text = item.timeAndAddress;
    self.titledetail.text = item.titleDetail;
    self.contenL.text = item.title;
    self.timeAndAddress.text = item.timeAndAddress;
    self.ticketType.attributedText =[self setTextAttributedString:[NSString stringWithFormat:@"票种%@",item.ticketType]];
    self.ticketPrice.attributedText = [self setTextAttributedString:[NSString stringWithFormat:@"票价%@",item.totlePrice]];;
    self.examineVerify.attributedText = [self setTextAttributedString:[NSString stringWithFormat:@"审核%@",item.examineVerify]];
    self.totlePrice.text = [NSString stringWithFormat:@"￥%@",item.ticketPrice];
    self.numberButton.currentNumber = [item.number floatValue];
    [self layoutIfNeeded];
    [self drawLineOfDashByCAShapeLayer:self.lineViewTop lineLength:1 lineSpacing:1 lineColor:Hexcolor(0xD1D1D1) lineDirection:YES];
    [self drawLineOfDashByCAShapeLayer:self.lineViewBottom lineLength:1 lineSpacing:1 lineColor:Hexcolor(0xD1D1D1) lineDirection:YES];
}
- (NSAttributedString *)setTextAttributedString:(NSString *)actionStr{
    //设置整段字符串的颜色
    NSDictionary *attributes = @{NSFontAttributeName:kLabelFontSize12, NSForegroundColorAttributeName:Hexcolor(0xD1D1D1)};
    NSRange range = NSMakeRange(1, 1);
    NSMutableAttributedString *text = [NSMutableAttributedString setTextAttributedString:actionStr attributes:attributes space:WScale(17) range:range];
    return [text copy];
}

- (UILabel *)contenL {
    if (!_contenL) {
        _contenL  = [[UILabel alloc] init];
        _contenL.textColor = Hexcolor(0x333333);
        _contenL.font = kLabelFontSize18;
        
    }
    return _contenL;
}


- (YYLabel *)titledetail {
    if (!_titledetail) {
        _titledetail  = [[YYLabel alloc] init];
        _titledetail.textColor = Hexcolor(0x4A4A4A);
        _titledetail.font = kLabelFontSize18;
        _titledetail.numberOfLines = 2;
        _titledetail.textVerticalAlignment = YYTextVerticalAlignmentTop;
    }
    return _titledetail;
}
- (UILabel *)timeAndAddress {
    if (!_timeAndAddress) {
        _timeAndAddress = [[UILabel alloc] init];
        _timeAndAddress.textColor = Hexcolor(0x999999);
        _timeAndAddress.font = kLabelFontSize11;
        _timeAndAddress.numberOfLines = 2;
    }
    return _timeAndAddress;
}
- (UIView *)lineViewTop{
    if (!_lineViewTop) {
        _lineViewTop = [[UIView alloc] init];
        _lineViewTop.backgroundColor = Hexcolor(0xFFFFFF);
    }
    return _lineViewTop;
}
- (UIView *)lineViewBottom{
    if (!_lineViewBottom) {
        _lineViewBottom = [[UIView alloc] init];
        _lineViewBottom.backgroundColor = Hexcolor(0xFFFFFF);
    }
    return _lineViewBottom;
}
- (UILabel *)ticketType{
    if (!_ticketType) {
        _ticketType = [[UILabel alloc] init];
        _ticketType.textColor = Hexcolor(0x999999);
        _ticketType.font = kLabelFontSize12;
    }
    return _ticketType;
}
- (UILabel *)ticketPrice{
    if (!_ticketPrice) {
        _ticketPrice = [[UILabel alloc] init];
        _ticketPrice.textColor = Hexcolor(0x999999);
        _ticketPrice.font = kLabelFontSize12;
    }
    return _ticketPrice;
}
- (UILabel *)examineVerify{
    if (!_examineVerify) {
        _examineVerify = [[UILabel alloc] init];
        _examineVerify.textColor = Hexcolor(0x999999);
        _examineVerify.font = kLabelFontSize12;
    }
    return _examineVerify;
}
- (UILabel *)heji{
    if (!_heji) {
        _heji = [[UILabel alloc] init];
        _heji.textColor = Hexcolor(0x666666);
        _heji.font = kLabelFontSize14;
        _heji.text = @"总计";
        _heji.textAlignment = NSTextAlignmentRight;
    }
    return _heji;
}
- (UILabel *)totlePrice{
    if (!_totlePrice) {
        _totlePrice = [[UILabel alloc] init];
        _totlePrice.textColor = Hexcolor(0x8C734B);
        _totlePrice.font = kLabelFontSize18;
        _totlePrice.textAlignment = NSTextAlignmentRight;

    }
    return _totlePrice;
}
- (PPNumberButton *)numberButton
{
    if (!_numberButton) {
        _numberButton = [[PPNumberButton alloc] init];
        _numberButton.shakeAnimation = YES;
        
        // 设置最大值
        _numberButton.maxValue = 1000;
        _numberButton.minValue = 0;
        _numberButton.increaseImage = [UIImage imageNamed:@"+"];
        _numberButton.decreaseImage = [UIImage imageNamed:@"-"];
        //        _numberButton.increaseTitle = @"+";
        //        _numberButton.decreaseTitle = @"-";
        // 设置输入框中的字体大小
        _numberButton.inputFieldFont = WScale(14);
        _numberButton.delegate = self;
        _numberButton.longPressSpaceTime = CGFLOAT_MAX;
        _numberButton.resultBlock = ^(PPNumberButton *ppBtn, CGFloat number, BOOL increaseStatus){
            NSLog(@"%f",number);
        };
        
    }
    return _numberButton;
}
- (UIView *)backView {
    if (!_backView) {
        _backView  = [[UILabel alloc] init];
        _backView.backgroundColor = Hexcolor(0xFFFFFF);
        _backView.layer.borderColor = Hexcolor(0xD8D8D8).CGColor;
        _backView.layer.borderWidth = WScale(1);
    }
    return _backView;
}
- (UIView *)lineView{
    if (!_lineView) {
        _lineView = [[UIView alloc] init];
        _lineView.backgroundColor = Hexcolor(0xF6F7FB);
    }
    return _lineView;
}
@end
