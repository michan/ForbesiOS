//
//  FBSProtocalCell.m
//  Forbes
//
//  Created by 赵志辉 on 2019/9/25.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import "FBSProtocalCell.h"
#import <NSAttributedString+YYText.h>

@interface FBSProtocalCell()
@property (nonatomic , strong)   YYLabel *yyLabel;

@end

@implementation FBSProtocalCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        [self.contentView addSubview:self.yyLabel];
        
        [self.yyLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.mas_left);
            make.right.mas_equalTo(self.mas_right);
            make.top.mas_equalTo(WScale(10));
            make.height.mas_equalTo(WScale(17));
        }];
        
    }
    
    return self;
}
//- (void)didTapCheckBox:(BEMCheckBox*)checkBox{
//    if ([self.delegate respondsToSelector:@selector(fbsCell:didClickButtonAtIndex:)]) {
//        if (checkBox == self.myCheckBox1) {
//            [self.delegate fbsCell:self didClickButtonAtIndex:SelectTypeMan];
//        }else{
//            [self.delegate fbsCell:self didClickButtonAtIndex:SelectTypeWOMan];
//        }
//    }
//}


- (void)setItem:(FBSProtocalItem *)item{
    [super setItem:item];

    [self protocolIsSelect:item.isSelect action:item.title];
}
- (void)protocolIsSelect:(BOOL)isSelect action:(NSString *)actionStr{
    //设置整段字符串的颜色
    UIColor *color = Hexcolor(0x999999);
    NSDictionary *attributes = @{NSFontAttributeName:kLabelFontSize12, NSForegroundColorAttributeName: color};
    
    NSMutableAttributedString *text = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"  %@",actionStr] attributes:attributes];
    //设置高亮色和点击事件
    [text setTextHighlightRange:[[text string] rangeOfString:actionStr] color:color backgroundColor:[UIColor clearColor] tapAction:^(UIView * _Nonnull containerView, NSAttributedString * _Nonnull text, NSRange range, CGRect rect) {
        if ([self.delegate respondsToSelector:@selector(fbsCell:didClickButtonAtIndex:)]) {
            [self.delegate fbsCell:self didClickButtonAtIndex:0];
        }
    }];
    //添加图片
    UIImage *image = [UIImage imageNamed:isSelect == NO ? @"unsel" : @"sel"];
    NSMutableAttributedString *attachment = [NSMutableAttributedString attachmentStringWithContent:image contentMode:UIViewContentModeCenter attachmentSize:CGSizeMake(WScale(14), WScale(14)) alignToFont:kLabelFontSize12 alignment:(YYTextVerticalAlignment)YYTextVerticalAlignmentCenter];
    //将图片放在最前面
    
    
    [text insertAttributedString:attachment atIndex:0];
    //添加图片的点击事件
//    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
//    paragraphStyle.alignment = NSTextAlignmentJustified;//设置对齐方式
//    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
//    [paragraphStyle setLineSpacing:WScale(14)];//调整行间距
//    [text addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, 2)];
//    NSNumber *number = [NSNumber numberWithFloat:WScale(14)];
//    [text addAttribute:(id)kCTKernAttributeName value:number range:NSMakeRange(0, 1)];
    WEAKSELF
    [text setTextHighlightRange:[[text string] rangeOfString:[attachment string]] color:[UIColor clearColor] backgroundColor:[UIColor clearColor] tapAction:^(UIView * _Nonnull containerView, NSAttributedString * _Nonnull text, NSRange range, CGRect rect) {
        FBSProtocalItem *iteM =  (FBSProtocalItem *)weakSelf.item;
        iteM.isSelect = !isSelect;
        [weakSelf protocolIsSelect:!isSelect action:(NSString *)actionStr];
    }];
    _yyLabel.attributedText = text;
    _yyLabel.textAlignment = NSTextAlignmentCenter;
    
}
- (YYLabel *)yyLabel {
    if (!_yyLabel) {
        _yyLabel = [[YYLabel alloc]init];
        _yyLabel.textVerticalAlignment = YYTextVerticalAlignmentCenter;
        _yyLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _yyLabel;
}
@end
