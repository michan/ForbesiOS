//
//  FBSActivitiListItem.h
//  Forbes
//
//  Created by 赵志辉 on 2019/9/23.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import "FBSItem.h"
#import "FBSActivityLastestItem.h"

NS_ASSUME_NONNULL_BEGIN

@interface FBSActivitiListItem : FBSItem
@property (nonatomic , strong) NSArray * data;
@end

NS_ASSUME_NONNULL_END
