//
//  FBSActivitiListShopItem.h
//  Forbes
//
//  Created by 赵志辉 on 2019/9/24.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import "FBSItem.h"
#import "FBSActivitiesModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface FBSActivitiShopItem : FBSItem
@property (nonatomic , copy) NSString            *   id;
@property (nonatomic , copy) NSString              * title;
@property (nonatomic , copy) NSString              * activity_address;
@property (nonatomic , copy) NSString              * file_url;
@property (nonatomic , copy) NSString              * activity_sponsor;
@property (nonatomic , copy) NSString              * activity_time;
@property (nonatomic , copy) NSString            *   start_time;
@property (nonatomic , copy) NSString            *   end_time;
@property (nonatomic , strong) NSArray <TicketsItem *>              * tickets;
@end

NS_ASSUME_NONNULL_END
