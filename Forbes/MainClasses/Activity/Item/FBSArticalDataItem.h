//
//  FBSArticalDataItem.h
//  Forbes
//
//  Created by 赵志辉 on 2019/9/29.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import "FBSItem.h"
#import "FBSArticalModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface FBSArticalDataItem : FBSItem

@property (nonatomic , copy) NSString              * id;
@property (nonatomic , copy) NSString              * user_id;
@property (nonatomic , copy) NSString              * title;
@property (nonatomic , copy) NSString              * file_id;
@property (nonatomic , copy) NSString              * describe;
@property (nonatomic , copy) NSString              * limit_number;
@property (nonatomic , copy) NSString              * start_time;
@property (nonatomic , copy) NSString              * end_time;
@property (nonatomic , copy) NSString              * type;
@property (nonatomic , copy) NSString              * activity_time;
@property (nonatomic , copy) NSString              * activity_address;
@property (nonatomic , copy) NSString              * activity_sponsor;
@property (nonatomic , copy) NSString              * created_at;
@property (nonatomic , copy) NSString              * updated_at;
@property (nonatomic , copy) NSString              * file_url;
@property (nonatomic , copy) NSString              * maxCounts;
@property (nonatomic , copy) NSString              * hasCounts;
@property (nonatomic , strong) NSArray <FBSArticalDataModelTicketsItem *>              * tickets;
@property (nonatomic , strong) NSArray <FBSArticalDataModelArea_date_listItem *>              * area_date_list;
@end

NS_ASSUME_NONNULL_END
