//
//  FBSActivityPayTypeItem.h
//  Forbes
//
//  Created by 赵志辉 on 2019/9/26.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import "FBSItem.h"

NS_ASSUME_NONNULL_BEGIN

@interface FBSActivityPayTypeItem : FBSItem

@end

NS_ASSUME_NONNULL_END
