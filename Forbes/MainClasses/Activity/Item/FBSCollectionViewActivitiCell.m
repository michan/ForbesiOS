//
//  FBSCollectionViewActivitiCell.m
//  Forbes
//
//  Created by 赵志辉 on 2019/9/23.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import "FBSCollectionViewActivitiCell.h"



@interface FBSCollectionViewActivitiCell ()

@property (strong, nonatomic) UIImageView *headImageV;

@property (strong, nonatomic) UILabel *nameLabel;

@property (nonatomic, strong) UILabel *time;
@property (nonatomic, strong) UILabel *price;

@end

@implementation FBSCollectionViewActivitiCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        [self.contentView addSubview:self.headImageV];
        [self.contentView addSubview:self.nameLabel];
        [self.contentView addSubview:self.time];
        [self.contentView addSubview:self.price];
        
        [self.headImageV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self);
            make.right.mas_equalTo(self);
            make.top.mas_equalTo(self.mas_top);
            make.height.mas_equalTo(WScale(98));
        }];
        [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self);
            make.right.mas_equalTo(self);
            make.top.mas_equalTo(self.headImageV.mas_bottom).offset(WScale(10));
            make.height.mas_equalTo(WScale(37));

        }];
        [self.time sizeToFit];
        [self.time mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self);
            make.top.mas_equalTo(self.nameLabel.mas_bottom).offset(WScale(10));
            make.height.mas_equalTo(WScale(15));
            make.width.mas_equalTo(self.mas_width).multipliedBy(0.5);
        }];
        [self.price sizeToFit];
        [self.price mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.nameLabel.mas_bottom).offset(WScale(10));
            make.right.mas_equalTo(self.mas_right);
            make.height.mas_equalTo(WScale(15));
            make.width.mas_equalTo(self.mas_width).multipliedBy(0.5);

        }];
    }
    
    return self;
}


- (void)setItem:(FBSActivityLastestItem *)item{
    [self.headImageV sd_setImageWithURL:[NSURL URLWithString:item.file_url]];
    self.nameLabel.text =item.title;
}

- (UILabel *)time{
    if (!_time) {
        _time = [[UILabel alloc] init];
        _time.textColor = Hexcolor(0x999999);
        _time.font = kLabelFontSize11;
        
    }
    return _time;
}

- (UILabel *)price{
    if (!_price) {
        _price = [[UILabel alloc] init];
        _price.textColor = Hexcolor(0x999999);
        _price.font = kLabelFontSize11;
        _price.textAlignment = NSTextAlignmentRight;
        
    }
    return _price;
}

- (UIImageView *)headImageV
{
    if (!_headImageV) {
        _headImageV = [[UIImageView alloc] init];
    }
    return _headImageV;
}

- (UILabel *)nameLabel
{
    if (!_nameLabel) {
        _nameLabel = [[UILabel alloc] init];
        _nameLabel.textAlignment = NSTextAlignmentLeft;
        _nameLabel.numberOfLines = 2;
        _nameLabel.font = kLabelFontSize13;
        _nameLabel.textColor = Hexcolor(0x333333);
    }
    return _nameLabel;
}
@end
