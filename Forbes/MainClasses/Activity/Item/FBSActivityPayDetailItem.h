//
//  FBSActivityPayDetailItem.h
//  Forbes
//
//  Created by 赵志辉 on 2019/9/26.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import "FBSItem.h"

NS_ASSUME_NONNULL_BEGIN

@interface FBSActivityPayDetailItem : FBSItem

@property(nonatomic, strong)NSString *title;

@property(nonatomic, strong)NSString *titleDetail;

@property(nonatomic, strong)NSString *timeAndAddress;//     ticket

@property(nonatomic, strong)NSString *ticketType;//     ticket

@property(nonatomic, strong)NSString *ticketPrice;//     ticket

@property(nonatomic, strong)NSString *examineVerify;//     ticket

@property(nonatomic, strong)NSString *number;//     ticket


@property(nonatomic, strong)NSString *totlePrice;//     ticket





@end

NS_ASSUME_NONNULL_END
