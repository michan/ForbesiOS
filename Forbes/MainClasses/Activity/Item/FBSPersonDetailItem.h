//
//  FBSPersonDetailItem.h
//  Forbes
//
//  Created by 赵志辉 on 2019/9/25.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import "FBSItem.h"

NS_ASSUME_NONNULL_BEGIN

@interface FBSPersonDetailItem : FBSItem

@property (nonatomic , copy) NSString *title;
@property (nonatomic , copy) NSString *detail;
@property (nonatomic , copy) NSString *name;
@property (nonatomic , copy) NSString *phone;
@property (nonatomic , copy) NSString *email;


@end

NS_ASSUME_NONNULL_END
