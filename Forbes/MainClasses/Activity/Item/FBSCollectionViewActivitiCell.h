//
//  FBSCollectionViewActivitiCell.h
//  Forbes
//
//  Created by 赵志辉 on 2019/9/23.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FBSActivityLastestItem.h"

NS_ASSUME_NONNULL_BEGIN

@interface FBSCollectionViewActivitiCell : UICollectionViewCell

@property(nonatomic ,strong)FBSActivityLastestItem *item;

@end

NS_ASSUME_NONNULL_END
