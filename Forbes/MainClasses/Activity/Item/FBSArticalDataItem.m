//
//  FBSArticalDataItem.m
//  Forbes
//
//  Created by 赵志辉 on 2019/9/29.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import "FBSArticalDataItem.h"

@implementation FBSArticalDataItem
+ (NSDictionary *)mj_objectClassInArray {
    return @{@"area_date_list" : @"FBSArticalDataModelArea_date_listItem",@"tickets" : @"FBSArticalDataModelTicketsItem"};
}
@end
