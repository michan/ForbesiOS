//
//  FBSActivityLastestItem.h
//  Forbes
//
//  Created by 赵志辉 on 2019/11/1.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import "FBSItem.h"

NS_ASSUME_NONNULL_BEGIN

@interface FBSActivityLastestItem : FBSItem
@property (nonatomic , copy) NSString              * title;

@property (nonatomic , copy) NSString              * slug;

@property (nonatomic , copy) NSString              * tid;

@property (nonatomic , copy) NSString              * file_id;

@property (nonatomic , copy) NSString              * file_url;
@end

NS_ASSUME_NONNULL_END
