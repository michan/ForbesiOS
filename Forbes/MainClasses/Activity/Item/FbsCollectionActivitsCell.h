//
//  FbsCollectionActivitsCell.h
//  Forbes
//
//  Created by 赵志辉 on 2019/9/23.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import "FBSCell.h"
#import "FBSActivitiListItem.h"
NS_ASSUME_NONNULL_BEGIN


@protocol FBSCollectionActivitsDelegate <FBSCellDelegate>

- (void)fbsCell:(FBSCell *)cell item:(FBSItem *)ActivitiItem didClickItemAtIndex:(NSIndexPath *)indexP;

@end


@interface FbsCollectionActivitsCell : FBSCell

@property (nonatomic, weak)   id<FBSCollectionActivitsDelegate,FBSCellDelegate> delegate;
@end

NS_ASSUME_NONNULL_END
