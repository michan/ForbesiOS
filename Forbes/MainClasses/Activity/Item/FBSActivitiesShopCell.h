//
//  FBSActivitiesShopCell.h
//  Forbes
//
//  Created by 赵志辉 on 2019/9/24.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import "FBSCell.h"

#import "PPNumberButton.h"
#import "FBSActivitiShopItem.h"

NS_ASSUME_NONNULL_BEGIN

@interface FBSActivitiesShopCell : FBSCell

@end

NS_ASSUME_NONNULL_END
