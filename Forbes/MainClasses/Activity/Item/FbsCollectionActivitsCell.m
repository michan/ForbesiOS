//
//  FbsCollectionActivitsCell.m
//  Forbes
//
//  Created by 赵志辉 on 2019/9/23.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import "FbsCollectionActivitsCell.h"
#import "FBSCollectionViewActivitiCell.h"


@interface FbsCollectionActivitsCell()<UICollectionViewDelegate,UICollectionViewDataSource>

@property(nonatomic,strong) UICollectionView *collectionView;
@property(nonatomic,strong) NSArray *dataArr;
@end
@implementation FbsCollectionActivitsCell
//@dynamic delegate;

#pragma mark - cell
- (UICollectionView *)collectionView
{
    if (!_collectionView) {
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
        CGFloat cellWidth=0;
        CGFloat intervalWidth= WScale(9);//间隙宽度
        layout.sectionInset = UIEdgeInsetsMake(intervalWidth, intervalWidth, intervalWidth, intervalWidth);
        cellWidth=([UIScreen mainScreen].bounds.size.width-WScale(9)*2-WScale(4.5)*2)/2;
        layout.itemSize = CGSizeMake(cellWidth, WScale(180));
        layout.minimumLineSpacing = intervalWidth;
        layout.minimumInteritemSpacing = intervalWidth;
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:layout];
        _collectionView.backgroundColor = [UIColor clearColor];
        [_collectionView registerClass:[FBSCollectionViewActivitiCell class] forCellWithReuseIdentifier:NSStringFromClass([FBSCollectionViewActivitiCell class])];
        _collectionView.showsHorizontalScrollIndicator=NO;
        _collectionView.showsVerticalScrollIndicator=NO;
        _collectionView.backgroundColor=[UIColor clearColor];
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        _collectionView.scrollEnabled = NO;
    }
    return _collectionView;
}
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self creatView];
    }
    return self;
}

- (void)creatView
{
    [self.contentView addSubview:self.collectionView];
    self.contentView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.bottom.top.right.mas_equalTo(self);
    }];
}
#pragma mark - cell data
- (void)setItem:(FBSActivitiListItem *)item {
    if (![item isKindOfClass:[FBSActivitiListItem class]]) {
        return ;
    }
    self.dataArr = item.data;
    [self.collectionView reloadData];
    [self.collectionView layoutIfNeeded];
//    [self.collectionView mas_updateConstraints:^(MASConstraintMaker *make) {
//        make.height.mas_equalTo(self.collectionView.collectionViewLayout.collectionViewContentSize.height);
//    }];
    [self.collectionView layoutIfNeeded];
    [super setItem:item];
    
}


#pragma mark - 计算高度
//- (CGSize)systemLayoutSizeFittingSize:(CGSize)targetSize withHorizontalFittingPriority:(UILayoutPriority)horizontalFittingPriority verticalFittingPriority:(UILayoutPriority)verticalFittingPriority
//{
//    // 先对bgview进行布局
//    self.bgView.frame = CGRectMake(0, 0, targetSize.width, 44);
//    [self.bgView layoutIfNeeded];
//
//    // 在对collectionView进行布局
//    self.collectionView.frame = CGRectMake(0, 0, targetSize.width-Margin*2, 44);
//    [self.collectionView layoutIfNeeded];
//
//    // 由于这里collection的高度是动态的，这里cell的高度我们根据collection来计算
//    CGSize collectionSize = self.collectionView.collectionViewLayout.collectionViewContentSize;
//    CGFloat cotentViewH = collectionSize.height + Margin*2;
//
//    // 返回当前cell的高度
//    return CGSizeMake([UIScreen mainScreen].bounds.size.width, cotentViewH);
//}

#pragma mark - collection
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.dataArr.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    FBSCollectionViewActivitiCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([FBSCollectionViewActivitiCell class]) forIndexPath:indexPath];
    [cell setItem:self.dataArr[indexPath.row]];
    return cell;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if ([self.delegate respondsToSelector:@selector(fbsCell:item:didClickItemAtIndex:)]) {
        [self.delegate fbsCell:self item:self.dataArr[indexPath.row] didClickItemAtIndex:indexPath];
    }
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
//- (void)setDelegate:(id<FBSCollectionActivitsDelegate>)delegate{
//    [super setDelegate:delegate];
//     self.delegate = delegate;
//}
@end
