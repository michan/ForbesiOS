//
//  FBSActivitiesShopCell.m
//  Forbes
//V
//  Created by 赵志辉 on 2019/9/24.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import "FBSActivitiesShopCell.h"
#import "FBSArticalDataItem.h"

@interface FBSActivitiesShopCell()<PPNumberButtonDelegate>


@property (nonatomic, strong) UIView *backView;
@property (nonatomic, strong) UILabel *contenL;
@property (nonatomic, strong) UIImageView *imgV;
@property (nonatomic, strong) UILabel *descL;
@property (nonatomic, strong) UILabel *time;
@property (nonatomic, strong) UILabel *line;
@property (nonatomic, strong)PPNumberButton *numberButton;


@property (nonatomic, strong) NSDictionary *attributeDic;

@end

@implementation FBSActivitiesShopCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        self.backgroundColor = Hexcolor(0xF6F7FB);
        [self.contentView addSubview:self.backView];
        [self.backView addSubview:self.contenL]; //免费票
        //[self.contentView addSubview:self.numberButton];
        [self.backView addSubview:self.descL];
        [self.backView addSubview:self.time];
        [self.backView addSubview:self.line];
        
        [self.backView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(WScale(9));
            make.right.mas_equalTo(WScale(-9));
            make.height.mas_equalTo(WScale(107));
            make.top.mas_equalTo(WScale(10));
        }];
        
        [self.contenL mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(WScale(15));
            make.top.mas_equalTo(self.backView).offset(WScale(11));
            make.right.mas_equalTo(self.right).offset(-WScale(15));
            make.height.mas_equalTo(WScale(20));
        }];
//        [self.numberButton mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.width.mas_equalTo(WScale(80));
//            make.top.mas_equalTo(self.backView).offset(WScale(11));
//            make.right.mas_equalTo(self.right).offset(-WScale(24));
//            make.height.mas_equalTo(WScale(19));
//        }];
        // 免费
        [self.descL mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.backView).offset(WScale(15));
            make.top.mas_equalTo(self.backView).offset(WScale(38));
            make.right.mas_equalTo(self.backView).offset(-WScale(15));
            make.height.mas_equalTo(WScale(25));
        }];
        [self.line mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.backView).offset(WScale(15));
            make.top.mas_equalTo(self.backView).offset(WScale(72));
            make.right.mas_equalTo(self.backView).offset(-WScale(11));
            make.height.mas_equalTo(WScale(1));
        }];
        
        [self.time mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.backView).offset(WScale(14));
            make.top.mas_equalTo(self.backView).offset(WScale(81));
            make.right.mas_equalTo(self.backView).offset(-WScale(11));
            make.height.mas_equalTo(WScale(14));
        }];

    }
    
    return self;
}



- (void)setItem:(FBSArticalDataItem *)item {
    if (![item isKindOfClass:[FBSArticalDataItem class]]) {
        return ;
    }
    FBSArticalDataModelTicketsItem *itemTicket = [item.tickets firstObject];
    [super setItem:item];
    self.contenL.text = itemTicket.title;
    self.descL.text = itemTicket.price;
    self.time.text = itemTicket.remarks;
    self.numberButton.maxValue = [itemTicket.number floatValue];
    self.numberButton.minValue = 0;
    
}

#pragma mark - property
- (PPNumberButton *)numberButton
{
    if (!_numberButton) {
        _numberButton = [[PPNumberButton alloc] init];
        _numberButton.shakeAnimation = YES;
       
        // 设置最大值
        _numberButton.maxValue = 1000;
        _numberButton.minValue = 0;
        _numberButton.increaseImage = [UIImage imageNamed:@"+"];
        _numberButton.decreaseImage = [UIImage imageNamed:@"-"];
//        _numberButton.increaseTitle = @"+";
//        _numberButton.decreaseTitle = @"-";
        // 设置输入框中的字体大小
        _numberButton.inputFieldFont = WScale(14);
        _numberButton.delegate = self;
        _numberButton.longPressSpaceTime = CGFLOAT_MAX;
        WEAKSELF
        _numberButton.resultBlock = ^(PPNumberButton *ppBtn, CGFloat number, BOOL increaseStatus){
            NSLog(@"%f",number);
            if ([weakSelf.item isKindOfClass:[FBSArticalDataModelTicketsItem class]]) {
                FBSArticalDataModelTicketsItem *ticketItem = (FBSArticalDataModelTicketsItem *)weakSelf.item;
                ticketItem.choseNum = [NSString stringWithFormat:@"%f",number];
            }
        };

    }
    return _numberButton;
}
- (UIView *)backView {
    if (!_backView) {
        _backView  = [[UILabel alloc] init];
        _backView.backgroundColor = Hexcolor(0xFFFFFF);
        _backView.layer.borderColor = Hexcolor(0x999999).CGColor;
        _backView.layer.borderWidth = WScale(1);
    }
    return _backView;
}
- (UILabel *)contenL {
    if (!_contenL) {
        _contenL  = [[UILabel alloc] init];
        _contenL.textColor = Hexcolor(0x333333);
        _contenL.font = kLabelFontSize14;
    }
    return _contenL;
}

- (UILabel *)descL {
    if (!_descL) {
        _descL  = [[UILabel alloc] init];
        _descL.textColor = Hexcolor(0x8C734B);
        _descL.font = kLabelFontSize18;
    }
    return _descL;
}

- (UIImageView *)imgV {
    if (!_imgV) {
        _imgV  = [[UIImageView alloc] init];
        _imgV.backgroundColor = RandomColor;
    }
    return _imgV;
}
- (UILabel *)time{
    if (!_time) {
        _time = [[UILabel alloc] init];
        _time.textColor = Hexcolor(0x999999);
        _time.font = kLabelFontSize10;
        
    }
    return _time;
}
- (UILabel *)line{
    if (!_line) {
        _line = [[UILabel alloc] init];
        _line.backgroundColor = Hexcolor(0xD8D8D8);
    }
    return _line;
}


- (NSDictionary *)attributeDic {
    if (!_attributeDic) {
        NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc] init];
        style.lineSpacing = 5;
        
        _attributeDic  = @{
                           NSForegroundColorAttributeName : Hexcolor(0x333333),
                           NSFontAttributeName : kLabelFontSize18,
                           NSParagraphStyleAttributeName : style
                           };
    }
    return _attributeDic;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end
