//
//  FBSActivitiItem.m
//  Forbes
//
//  Created by 赵志辉 on 2019/9/23.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import "FBSActivitiItem.h"

@implementation FBSActivitiItem
+ (NSDictionary *)mj_objectClassInArray {
    return @{@"tickets" : @"TicketsItem"};
}

@end
