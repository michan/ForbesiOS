//
//  FBSSelectCell.m
//  Forbes
//
//  Created by 赵志辉 on 2019/9/25.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import "FBSSelectCell.h"
#import "FBSSelectItem.h"
@interface FBSSelectCell()<BEMCheckBoxDelegate>

@property (nonatomic, strong) UILabel *contenL;
@property (nonatomic, strong) UILabel *detail;
@property (nonatomic, strong) BEMCheckBoxGroup *group;
@property (nonatomic, strong) BEMCheckBox *myCheckBox1;
@property (nonatomic, strong) BEMCheckBox *myCheckBox2;
@property (nonatomic, strong) UILabel *man;
@property (nonatomic, strong) UILabel *woman;





@end

@implementation FBSSelectCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        [self.contentView addSubview:self.myCheckBox1];
        [self.contentView addSubview:self.myCheckBox2];

        self.group = [BEMCheckBoxGroup groupWithCheckBoxes:@[self.myCheckBox1, self.myCheckBox2]];
        self.group.selectedCheckBox = self.myCheckBox1; // 可选择设置哪个复选框被预选
//        self.group.mustHaveSelection = YES;
        [self.contentView addSubview:self.contenL]; //免费票
        [self.contentView addSubview:self.detail];
        [self.contentView addSubview:self.man]; //免费票
        [self.contentView addSubview:self.woman];
        
        [self.contenL sizeToFit];
        [self.contenL setContentHuggingPriority:UILayoutPriorityRequired
                                        forAxis:UILayoutConstraintAxisHorizontal];
        [self.detail setContentHuggingPriority:UILayoutPriorityDefaultLow
                                       forAxis:UILayoutConstraintAxisHorizontal];
        [self.contenL mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(WScale(13));
            make.top.mas_equalTo(self).offset(WScale(10));
            make.height.mas_equalTo(WScale(20));
            make.right.mas_equalTo(self.detail.mas_left).offset(WScale(-4));
            
        }];
        [self.detail mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.contenL.mas_right).offset(WScale(4));
            make.top.mas_equalTo(self.contenL.mas_top).offset(WScale(5));
            make.height.mas_equalTo(WScale(14));
            make.right.mas_equalTo(self).offset(WScale(-13));
        }];
        
        [self.myCheckBox1 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.mas_left).offset(WScale(13));
            make.top.mas_equalTo(self.mas_top).offset(WScale(40));
            make.height.mas_equalTo(WScale(13));
            make.width.mas_equalTo(WScale(13));
        }];
        
        [self.man mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.mas_left).offset(WScale(51));
            make.centerY.mas_equalTo(self.myCheckBox1.centerY);
            make.width.mas_equalTo(WScale(27));
            make.height.mas_equalTo(WScale(14));
        }];
        
        
        [self.myCheckBox2 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.mas_left).offset(WScale(112));
            make.top.mas_equalTo(self.mas_top).offset(WScale(40));
            make.width.mas_equalTo(WScale(13));
            make.height.mas_equalTo(WScale(13));
        }];
        
        [self.woman mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.mas_left).offset(WScale(149));
            make.centerY.mas_equalTo(self.myCheckBox1.centerY);
            make.width.mas_equalTo(WScale(27));
            make.height.mas_equalTo(WScale(14));
        }];
        
    }
    
    return self;
}
- (void)didTapCheckBox:(BEMCheckBox*)checkBox{
    if ([self.delegate respondsToSelector:@selector(fbsCell:didClickButtonAtIndex:)]) {
        if (checkBox == self.myCheckBox1) {
            [self.delegate fbsCell:self didClickButtonAtIndex:SelectTypeMan];
        }else{
            [self.delegate fbsCell:self didClickButtonAtIndex:SelectTypeWOMan];
        }
    }
}


- (void)setItem:(FBSSelectItem *)item{
    [super setItem:item];
    if (item.typeMan == SelectTypeNone) {
        self.group.selectedCheckBox = nil; // 可选择设置哪个复选框被预
    }else if (item.typeMan == SelectTypeMan){
        self.group.selectedCheckBox = self.myCheckBox1; // 可选择设置哪个复选框被预选
    }else if (item.typeMan == SelectTypeWOMan){
        self.group.selectedCheckBox = self.myCheckBox2; // 可选择设置哪个复选框被预选
    }
    self.contenL.text = item.title;
    self.detail.text = item.detail;
}
- (UILabel *)contenL {
    if (!_contenL) {
        _contenL  = [[UILabel alloc] init];
        _contenL.textColor = Hexcolor(0x333333);
        _contenL.font = kLabelFontSize14;
        
    }
    return _contenL;
}
- (UILabel *)man {
    if (!_man) {
        _man  = [[UILabel alloc] init];
        _man.textColor = Hexcolor(0x333333);
        _man.font = kLabelFontSize13;
        _man.text = @"先生";
        
    }
    return _man;
}
- (UILabel *)woman {
    if (!_woman) {
        _woman  = [[UILabel alloc] init];
        _woman.textColor = Hexcolor(0x333333);
        _woman.font = kLabelFontSize13;
        _woman.text = @"女士";

        
    }
    return _woman;
}


- (UILabel *)detail {
    if (!_detail) {
        _detail  = [[UILabel alloc] init];
        _detail.textColor = Hexcolor(0xD0021B);
        _detail.font = kLabelFontSize10;
    }
    return _detail;
}
-  (BEMCheckBox *)myCheckBox1
{
    if (!_myCheckBox1) {
        _myCheckBox1 = [[BEMCheckBox alloc] init];
        _myCheckBox1.onTintColor = Hexcolor(0x999999);
        _myCheckBox1.onFillColor = Hexcolor(0xFFFFFF);
        _myCheckBox1.onCheckColor = Hexcolor(0x999999);
        _myCheckBox1.lineWidth = 1;
        _myCheckBox1.delegate = self;
    }
    return _myCheckBox1;
}
-  (BEMCheckBox *)myCheckBox2
{
    if (!_myCheckBox2) {
        _myCheckBox2 = [[BEMCheckBox alloc] init];
        _myCheckBox2.onTintColor = Hexcolor(0x999999);
        _myCheckBox2.onFillColor = Hexcolor(0xFFFFFF);
        _myCheckBox2.onCheckColor = Hexcolor(0x999999);
        _myCheckBox2.delegate = self;
        _myCheckBox2.lineWidth = 1;


    }
    return _myCheckBox2;
}

@end
