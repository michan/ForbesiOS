//
//  FBSActivityListViewController.h
//  Forbes
//
//  Created by 周灿华 on 2019/7/28.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSTableViewController.h"

@interface FBSActivityListViewController : FBSTableViewController

@property (nonatomic, copy) NSString *type;

@end

