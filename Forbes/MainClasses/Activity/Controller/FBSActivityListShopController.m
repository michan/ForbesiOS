//
//  FBSActivityListShopController.m
//  Forbes
//
//  Created by 赵志辉 on 2019/9/24.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import "FBSActivityListShopController.h"
#import "FBSActivituListShopModel.h"
#import "FBSActivitiItem.h"
#import "FBSActivitiesCell.h"
#import "FBSTableViewController+Refresh.h"
#import "FBSActivitiListItem.h"
#import "FBSActivitiesShopCell.h"
#import "FBSluRuViewController.h"
#import "FBSButtonCell.h"
#import "FBSArticalDataItem.h"

@interface FBSActivityListShopController ()
@property (nonatomic, strong) FBSActivituListShopModel *viewModel;

@end

@implementation FBSActivityListShopController
@synthesize viewModel = _viewModel;


- (instancetype)initWithArticleId:(NSString *)articleId{
    self = [super init];
    if (self) {
        self.viewModel = [[FBSActivituListShopModel alloc] init];
        self.viewModel.articleId = articleId;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    self.registerDictionary = @{
                                [FBSOneImageItem reuseIdentifier] : [FBSOneImageCell class],
                                [FBSThreeImageItem reuseIdentifier] : [FBSThreeImageCell class],
                                [FBSOnlyTextItem reuseIdentifier] : [FBSOnlyTextCell class],
                                [FBSActivitiItem reuseIdentifier] : [FBSActivitiesCell class],
                                [FBSArticalDataItem reuseIdentifier] : [FBSActivitiesShopCell class],
                                [FBSButtonItem reuseIdentifier] : [FBSButtonCell class]
                                };
    
    WEAKSELF
    [self.viewModel rqArticleDetail:^(BOOL success, NSString * _Nonnull bodyHTML) {
        if (success) {
            [weakSelf.tableView reloadData];
        }
    }];
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 44;
    self.title = @"选择票价";
}

- (FBSActivituListShopModel *)viewModel {
    if (!_viewModel) {
        _viewModel  = [[FBSActivituListShopModel alloc] init];
    }
    return _viewModel;
}
- (void)fbsCell:(FBSCell *)cell didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}
-(void)fbsCell:(FBSCell *)cell didClickButtonAtIndex:(NSInteger)index
{
    if ([cell isKindOfClass:[FBSButtonCell class]]) {
        FBSluRuViewController *vc = [[FBSluRuViewController alloc] init];
        vc.itemShop = self.viewModel.itemShop;
        [self.navigationController pushViewController:vc animated:YES];
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    FBSItem *item  = [self.viewModel itemAtRow:indexPath.row inSection:indexPath.section];
    FBSCell *cell;
    if ([[item class] reuseIdentifier].length) {
        cell = [self.tableView dequeueReusableCellWithIdentifier:[[item class] reuseIdentifier] forIndexPath:indexPath];
        if ([cell isKindOfClass:[FBSButtonCell class]]) {
            cell.frame = CGRectMake(cell.frame.origin.x, cell.frame.origin.y, cell.frame.size.width, kScreenHeight -  cell.frame.origin.y);
        }
        cell.item = item;
        cell.delegate = self;
    }
    return cell;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
