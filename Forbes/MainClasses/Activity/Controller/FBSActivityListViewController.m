
//
//  FBSActivityListViewController.m
//  Forbes
//
//  Created by 周灿华 on 2019/7/28.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSActivityListViewController.h"
#import "FBSActivityListViewModel.h"
#import "FBSActivitiItem.h"
#import "FBSActivitiesCell.h"
#import "FBSTableViewController+Refresh.h"
#import "FbsCollectionActivitsCell.h"
#import "FBSActivitiListItem.h"
#import "DetailViewController.h"
#import "FBSActivityListShopController.h"
#import "FBSActivityLastestItem.h"



@interface FBSActivityListViewController ()
@property (nonatomic, strong) FBSActivityListViewModel *viewModel;
@end

@implementation FBSActivityListViewController
@synthesize viewModel = _viewModel;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationBarHidden = YES;
    
    self.registerDictionary = @{
                                [FBSBannerItem reuseIdentifier] : [FBSBannerCell class],
                                [FBSOneImageItem reuseIdentifier] : [FBSOneImageCell class],
                                [FBSThreeImageItem reuseIdentifier] : [FBSThreeImageCell class],
                                [FBSOnlyTextItem reuseIdentifier] : [FBSOnlyTextCell class],
                                [FBSActivitiItem reuseIdentifier] : [FBSActivitiesCell class],
                                [FBSActivitiListItem reuseIdentifier] : [FbsCollectionActivitsCell class]
                                };
    
    [self createload];
    // 自动布局
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 44;
}
- (void)createload{
    
    self.page = @(1);
    self.pageCount = @(6);
    WEAKSELF
    [self normalHeaderRefreshingActionBlock:^(FooterConfigBlock  _Nonnull footerConfig) {
        [weakSelf.viewModel requestRecommend:^(BOOL success) {
            if (success) {
                [weakSelf.tableView reloadData];
                footerConfig(6);
            }else{
                
            }
        }];
    }];
    [self backNormalFooterRefreshingActionBlock:^(FooterConfigBlock  _Nonnull footerConfig) {
        [weakSelf.viewModel requestActivitiesaNewsisloadMore:YES complete:^(BOOL success,NSInteger count) {
            if (success) {
                [weakSelf.tableView reloadData];
                footerConfig(count);
            }else{
                
            }
        }];
    }];
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if (section==0) {
        return nil;
    }else{
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth,WScale(61))];
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(WScale(13), WScale(20), kScreenWidth-WScale(26), WScale(21))];
        view.backgroundColor = Hexcolor(0xFFFFFF);
        [view addSubview:label];
        if (section == 1) {
            label.text = @"最新活动";
            UILabel *labelt = [[UILabel alloc] initWithFrame:CGRectMake(WScale(312), WScale(20), WScale(36), WScale(20))];
            labelt.textColor = Hexcolor(0x999999);
            labelt.font = kLabelFontSize10;
            labelt.text = @"换一组";
            [view addSubview:labelt];
            UIButton *bu = [UIButton buttonWithType:UIButtonTypeCustom];
            bu.frame = CGRectMake(WScale(348), WScale(20), WScale(20), WScale(20));
            [bu setImage:[UIImage imageNamed:@"Path"] forState:UIControlStateNormal];
            [view addSubview:bu];
            [bu addTarget:self action:@selector(loadRefresh:) forControlEvents:UIControlEventTouchUpInside];
        }else{
            label.text = @"最新要闻";
            UILabel *labelLine = [[UILabel alloc] initWithFrame:CGRectMake(WScale(13), WScale(59), WScale(65), WScale(2))];
            labelLine.backgroundColor = Hexcolor(0x8C734B);
            [view addSubview:labelLine];
        }
        return view;
    }
}
- (void)loadRefresh:(UIButton *)sender{
    WEAKSELF
    [self showHUD];
    [self.viewModel requestLatestActivitiesIsRefresh:YES  complete:^(BOOL success) {
        [self hideHUD];
        [weakSelf.tableView reloadData];
    }];
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section==0) {
        return 0;
    }else{
        return WScale(61);
    }
}
#pragma mark - delegate
- (void)fbsCell:(FBSCell *)cell didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if([cell.item isKindOfClass:[FBSActivitiItem class]]){
        FBSActivitiItem *item = (FBSActivitiItem *)cell.item;
        DetailViewController *detail = [[DetailViewController alloc] initWithArticleId:item.id htmlAddressL:@"activity/" cellAddress:nil];
        detail.position_id = @"36";
        [self.navigationController pushViewController:detail animated:YES];
    }else if([cell.item isKindOfClass:[FBSOneImageItem class]]){
        FBSOneImageItem *item = (FBSOneImageItem *)cell.item;
        DetailViewController *detail = [[DetailViewController alloc] initWithArticleId:item.ID htmlAddressL:@"article/" cellAddress:@"interest/"];
        detail.position_id = @"36";
        [self.navigationController pushViewController:detail animated:YES];
    }
}
- (void)fbsCell:(FBSCell *)cell didClickButtonAtIndex:(NSInteger)index{
    if ([cell isKindOfClass:[FBSActivitiesCell class]]) {
        FBSActivitiItem *item = (FBSActivitiItem *)cell.item;
        FBSActivityListShopController *vc = [[FBSActivityListShopController alloc] initWithArticleId:item.id];
        [self.navigationController pushViewController:vc animated:YES];
    }
    
//    FBSOneImageItem *item = (FBSOneImageItem *)cell.item;
//    DetailViewController *detail = [[DetailViewController alloc] initWithArticleId:item.articleId];
//    [self.navigationController pushViewController:detail animated:YES];
}
- (void)fbsCell:(FBSCell *)cell item:(nonnull FBSItem *)ActivitiItem didClickItemAtIndex:(nonnull NSIndexPath *)indexP{
    if ([ActivitiItem isKindOfClass:[FBSActivityLastestItem class]]) {
        FBSActivityLastestItem *item = (FBSActivityLastestItem *)ActivitiItem;
        DetailViewController *detail = [[DetailViewController alloc] initWithArticleId:item.file_id htmlAddressL:@"activity/" cellAddress:nil];
        detail.position_id = @"36";
        [self.navigationController pushViewController:detail animated:YES];
    }
    
}
#pragma mark - property

- (FBSActivityListViewModel *)viewModel {
    if (!_viewModel) {
        _viewModel  = [[FBSActivityListViewModel alloc] init];
    }
    return _viewModel;
}

@end
