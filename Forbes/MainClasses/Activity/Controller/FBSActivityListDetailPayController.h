//
//  FBSActivityListDetailPayController.h
//  Forbes
//
//  Created by 赵志辉 on 2019/9/25.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import "FBSTableViewController.h"
#import "FBSActivitiShopItem.h"
#import "FBSArticalModel.h"
#import "FBSArticalDataItem.h"


NS_ASSUME_NONNULL_BEGIN

@interface FBSActivityListDetailPayController : FBSTableViewController
- (instancetype)initWithActivitiItem:(NSMutableDictionary *)dic itemSop:(FBSArticalDataItem *)itemSop;

@end

NS_ASSUME_NONNULL_END
