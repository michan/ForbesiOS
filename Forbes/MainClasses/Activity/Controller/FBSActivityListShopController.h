//
//  FBSActivityListShopController.h
//  Forbes
//
//  Created by 赵志辉 on 2019/9/24.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import "FBSTableViewController.h"
#import "FBSActivitiItem.h"
#import "FBSArticalModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface FBSActivityListShopController : FBSTableViewController

- (instancetype)initWithArticleId:(NSString *)articleId;

@end

NS_ASSUME_NONNULL_END
