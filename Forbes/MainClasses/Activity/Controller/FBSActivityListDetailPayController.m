//
//  FBSActivityListDetailPayController.m
//  Forbes
//
//  Created by 赵志辉 on 2019/9/25.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import "FBSActivityListDetailPayController.h"
#import "FBSActivityListDetailPayModel.h"
#import "FBSButtonCell.h"
#import "FBSPersonDetailCell.h"
#import "FBSPersonDetailItem.h"
#import "FBSActivityPayDetailCell.h"
#import "FBSActivityPayDetailItem.h"
#import "FBSActivityPayTypeCell.h"
#import "FBSActivityPayTypeItem.h"
#import "FBSContributeSuccessViewController.h"


@interface FBSActivityListDetailPayController()

@property(nonatomic, strong)FBSActivityListDetailPayModel *viewModel;
@end

@implementation FBSActivityListDetailPayController
@synthesize viewModel = _viewModel;

- (instancetype)initWithActivitiItem:(NSMutableDictionary *)dic itemSop:(FBSArticalDataItem *)itemSop{
    self = [super init];
    if (self) {
        self.viewModel = [[FBSActivityListDetailPayModel alloc] init];
        self.viewModel.dic = dic;
        self.viewModel.itemShop = itemSop;
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"请核对订单信息";

    self.registerDictionary = @{
                               
                                [FBSButtonItem reuseIdentifier] : [FBSButtonCell class],
                                [FBSPersonDetailItem reuseIdentifier] : [FBSPersonDetailCell class],
                                [FBSActivityPayDetailItem reuseIdentifier] : [FBSActivityPayDetailCell class],
//                                [FBSActivityPayTypeItem reuseIdentifier] : [FBSActivityPayTypeCell class]
                                };
    
    
    WEAKSELF
    [self.viewModel createItem:^(BOOL success) {
        [weakSelf.tableView reloadData];
    }];
    // 自动布局
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 44;
}
#pragma mark - Table View Delegate
- (void)fbsCell:(FBSCell *)cell didClickButtonAtIndex:(NSInteger)index{
    if ([cell isKindOfClass:[FBSButtonCell class]]){
//        user_id     是     int     用户id
//        activity_id     是     int     活动id
//        ticket_id     是     int     票种id
//        name     是     string     姓名
//        ipone     是     string     手机
//        email     是     string     邮箱
//        sex     是     string     先生/女士
//        position     是     string     职位
//        corporate_name     是     string     公司名称
//        activity_date_id     是     string     地区/时间
        NSMutableDictionary *dic = [NSMutableDictionary dictionary];
        
        FBSArticalDataModelTicketsItem *item = [self.viewModel.itemShop.tickets firstObject];
        FBSUserData *user = [FBSUserData sharedData];
        [dic setObject:user.useId forKey:@"user_id"];
        [dic setObject:item.id forKey:@"activity_id"];
        [dic setObject:item.id forKey:@"ticket_id"];
        [dic setObject:self.viewModel.dic[@"姓名"] forKey:@"name"];
        [dic setObject:self.viewModel.dic[@"手机"] forKey:@"ipone"];
        [dic setObject:self.viewModel.dic[@"邮箱"] forKey:@"email"];
        [dic setObject:self.viewModel.dic[@"性别"] forKey:@"sex"];
        [dic setObject:self.viewModel.dic[@"职位"] forKey:@"position"];
        [dic setObject:self.viewModel.dic[@"公司名称"] forKey:@"corporate_name"];
        [dic setObject:self.viewModel.dic[@"地区/时间"] forKey:@"activity_date_id"];
        [self showHUD];
        [self.viewModel requestBaoming:dic complete:^(BOOL success) {
            [self hideHUD];
            if (success) {
                WEAKSELF
                FBSContributeSuccessViewController *successVC = [[FBSContributeSuccessViewController alloc] initWithTitle:@"报名成功" image:@"icon" success:@"报名成功！" content:@"感谢您对福布斯的支持，我们将会尽快确认您的信息。" button:@"完成" block:^{
                    [weakSelf.navigationController popToRootViewControllerAnimated:YES];
                }];
                [self.navigationController pushViewController:successVC animated:YES];
            }else{
                [self makeToast:@"购买失败" duration:2 position:ToastPositionCenter];
            }
            
        }];
       
        FBSLog(@"%@",@"ok");
    }
}
- (FBSActivityListDetailPayModel *)viewModel {
    if (!_viewModel) {
        _viewModel  = [[FBSActivityListDetailPayModel alloc] init];
    }
    return _viewModel;
}
@end
