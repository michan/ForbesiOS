//
//  FBSActivityViewController.m
//  Forbes
//
//  Created by 周灿华 on 2019/7/24.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSActivityViewController.h"
#import "FBSActivityViewModel.h"
#import "FBSActivitiItem.h"
#import "FBSActivitiesCell.h"
#import "FBSTableViewController+Refresh.h"
#import "FbsCollectionActivitsCell.h"
#import "FBSActivitiListItem.h"
#import "DetailViewController.h"

#import "FBSNavView.h"
#import "FBSSearchViewController.h"
#import "FBSActivityListShopController.h"
#import "FBSGroupHeadCell.h"
#import "FBSButtonCell.h"
#import "AdvertAopTableView.h"
#import "FBSActivityLastestItem.h"
#import "FBSPinglunViewController.h"


@interface FBSActivityViewController ()
@property (nonatomic, strong) FBSActivityViewModel *viewModel;
@property (nonatomic, strong) AdvertAopTableView *aopDemo;
@end

@implementation FBSActivityViewController
@synthesize viewModel = _viewModel;

///广告初始化
- (void)adsInit {
    NSString *position_id = @"34";
    
    if (position_id) {
        self.aopDemo = [AdvertAopTableView new];
        self.aopDemo.position_id = position_id;
        self.aopDemo.vc = self;
        NSIndexPath *indexP1 = [NSIndexPath indexPathForRow:0 inSection:1];
//        NSIndexPath *indexP2 = [NSIndexPath indexPathForRow:4 inSection:2];
//        NSIndexPath *indexP3 = [NSIndexPath indexPathForRow:4 inSection:3];
        self.aopDemo.softArray = @[indexP1];

//        self.aopDemo.softArray = @[indexP1,indexP2,indexP3];
        self.aopDemo.aopUtils = self.tableView.aop_utils;
    }
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self initNavBar];
    
    self.registerDictionary = @{
                                [FBSBannerItem reuseIdentifier] : [FBSBannerCell class],
                                [FBSOneImageItem reuseIdentifier] : [FBSOneImageCell class],
                                [FBSThreeImageItem reuseIdentifier] : [FBSThreeImageCell class],
                                [FBSOnlyTextItem reuseIdentifier] : [FBSOnlyTextCell class],
                                [FBSActivitiItem reuseIdentifier] : [FBSActivitiesCell class],
                                [FBSActivitiListItem reuseIdentifier] : [FbsCollectionActivitsCell class],
                                [FBSGroupHeadItem reuseIdentifier] : [FBSGroupHeadCell class],
                                [FBSButtonItem reuseIdentifier] : [FBSButtonCell class]
                                };
    
    [self createload];
    // 自动布局
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 44;
    [self adsInit];
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}


- (void)initNavBar {
    self.navigationBackButtonHidden = YES;
    self.navigationBar.titleView.hidden = YES;
    self.navigationBar.bottomLine.hidden = YES;
    
    FBSNavView *navView = [[FBSNavView alloc] initWithFrame:CGRectZero];
    navView.placeholder = @"30 under 30";
    navView.liveAction = ^{
        FBSLog(@"点击了直播");
        [[FBSliveAuthorization sharedInstance] liveAuthorization:self];

//        FBSPinglunViewController *vc = [[FBSPinglunViewController alloc] init];
//        [self.navigationController pushViewController:vc animated:YES];
    };
    
    WEAKSELF
    navView.searchAction = ^{
        FBSLog(@"点击了搜索");
        FBSSearchViewController *searchVC = [[FBSSearchViewController alloc] init];
        [weakSelf.navigationController pushViewController:searchVC animated:YES];
        
    };
    
    [self.navigationBar addSubview:navView];
    [navView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(0);
    }];
    
}


- (void)createload{
    
    self.page = @(1);
    self.pageCount = @(6);
    WEAKSELF
    [self normalHeaderRefreshingActionBlock:^(FooterConfigBlock  _Nonnull footerConfig) {
        [weakSelf.viewModel requestRecommend:^(BOOL success) {
            if (success) {
                [weakSelf.tableView reloadData];
                footerConfig(6);
            }else{
                
            }
        }];
    }];
    [self backNormalFooterRefreshingActionBlock:^(FooterConfigBlock  _Nonnull footerConfig) {
        [weakSelf.viewModel requestActivitiesaNewsisloadMore:YES complete:^(BOOL success,NSInteger count) {
            if (success) {
                [weakSelf.tableView reloadData];
                footerConfig(count);
            }else{
                
            }
        }];
    }];
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    return nil;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0;
}
#pragma mark - delegate
- (void)fbsCell:(FBSCell *)cell didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if([cell.item isKindOfClass:[FBSActivitiItem class]]){
        FBSActivitiItem *item = (FBSActivitiItem *)cell.item;
        DetailViewController *detail = [[DetailViewController alloc] initWithArticleId:item.id htmlAddressL:@"activity/" cellAddress:nil];
        detail.position_id = @"36";
        [self.navigationController pushViewController:detail animated:YES];
    }else if([cell.item isKindOfClass:[FBSOneImageItem class]]){
        FBSOneImageItem *item = (FBSOneImageItem *)cell.item;
        DetailViewController *detail = [[DetailViewController alloc] initWithArticleId:item.ID htmlAddressL:@"article/" cellAddress:@"interest/"];
        detail.position_id = @"36";
        [self.navigationController pushViewController:detail animated:YES];
    }
}
- (void)fbsCell:(FBSCell *)cell didClickButtonAtIndex:(NSInteger)index{
    if ([cell isKindOfClass:[FBSActivitiesCell class]]) {
        if(![FBSUserData sharedData].isLogin){
              FBSLoginViewController *loginVC = [[FBSLoginViewController alloc] init];
              [self.navigationController pushViewController:loginVC animated:YES];
              return ;
          }
        FBSActivitiItem *item = (FBSActivitiItem *)cell.item;
        FBSActivityListShopController *vc = [[FBSActivityListShopController alloc] initWithArticleId:item.id];
        [self.navigationController pushViewController:vc animated:YES];
    }else if ([cell isKindOfClass:[FBSGroupHeadCell class]]){
        WEAKSELF
        [self showHUD];
        [self.viewModel requestLatestActivitiesIsRefresh:YES  complete:^(BOOL success) {
            [self hideHUD];
            [weakSelf.tableView reloadData];
        }];
    }else if ([cell isKindOfClass:[FBSBannerCell class]]) {
        FBSBannerItem *item = (FBSBannerItem *)cell.item;
        if (index < item.datas.count) {
            FBSBannerItem *item = (FBSBannerItem *)cell.item;
            if (index < item.datas.count) {
                FBSBannerInfo *bannerInfo = [item.datas objectAtIndex:index];
                if (bannerInfo.isAd) {
                    FBSLog(@"%@",@"点击的是广告");
                    
                    return ;
                }
                DetailViewController *detail = [[DetailViewController alloc] initWithArticleId:bannerInfo.ID htmlAddressL:@"article/" cellAddress:@"interest/"];
                detail.position_id = @"5";
                [self.navigationController pushViewController:detail animated:YES];
            }

            
            
//
//
//
//
//            FBSBannerInfo *bannerInfo = [item.datas objectAtIndex:index];
//            DetailViewController *detail = [[DetailViewController alloc] initWithArticleId:bannerInfo.ID htmlAddressL:@"activity/" cellAddress:nil];
//            detail.position_id = @"36";
//            [self.navigationController pushViewController:detail animated:YES];
        }
    }
}
- (void)fbsCell:(FBSCell *)cell item:(nonnull FBSItem *)ActivitiItem didClickItemAtIndex:(nonnull NSIndexPath *)indexP{
    if ([ActivitiItem isKindOfClass:[FBSActivityLastestItem class]]) {
        FBSActivityLastestItem *item = (FBSActivityLastestItem *)ActivitiItem;
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:item.slug]];
    }
}
#pragma mark - property

- (FBSActivityViewModel *)viewModel {
    if (!_viewModel) {
        _viewModel  = [[FBSActivityViewModel alloc] init];
    }
    return _viewModel;
}

@end
