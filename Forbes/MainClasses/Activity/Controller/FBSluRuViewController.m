//
//  FBSluRuViewController.m
//  Forbes
//
//  Created by 赵志辉 on 2019/9/25.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import "FBSluRuViewController.h"
#import "FBSLuRuModel.h"
#import "FBSTextViewCell.h"
#import "FBSTextViewItem.h"
#import "FBSSelectCell.h"
#import "FBSSelectItem.h"
#import "FBSButtonCell.h"
#import "FBSProtocalCell.h"
#import "FBSActivityListDetailPayController.h"
#import "FLNiceSpinner.h"


@interface FBSluRuViewController()<ACEExpandableTableViewDelegate>

@property(nonatomic, strong)FBSLuRuModel *viewModel;
@end
@implementation FBSluRuViewController
@synthesize viewModel = _viewModel;




- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"填写报名表单";
    self.registerDictionary = @{
                                [FBSTextViewItem reuseIdentifier] : [FBSTextViewCell class],
                                [FBSSelectItem reuseIdentifier] : [FBSSelectCell class],
                                [FBSButtonItem reuseIdentifier] : [FBSButtonCell class],
                                [FBSProtocalItem reuseIdentifier] : [FBSProtocalCell class]
                                };
    
    WEAKSELF
    [self.viewModel createItem:^(BOOL success) {
        [weakSelf.tableView reloadData];
    }];
    // 自动布局
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 44;
}
#pragma mark - Table View Delegate
- (void)fbsCell:(FBSCell *)cell didClickButtonAtIndex:(NSInteger)index{
    if ([cell isKindOfClass:[FBSSelectCell class]]) {
        FBSSelectItem *item = (FBSSelectItem *)cell.item;
        item.typeMan = index;
    }else if([cell isKindOfClass:[FBSProtocalCell class]]){
        // 跳转对应协议
    }else if ([cell isKindOfClass:[FBSButtonCell class]]){
        NSMutableDictionary *dic = [NSMutableDictionary dictionary];
        for (FBSItem *item in self.viewModel.items) {
            if ([item isKindOfClass:[FBSTextViewItem class]]) {
                FBSTextViewItem *TextViewItem = (FBSTextViewItem *)item;
                if (!TextViewItem.text.length) {
                    [self.navigationController.view makeToast:[NSString stringWithFormat:@"请填写%@",TextViewItem.title]
                                                     duration:1.0
                                                     position:CSToastPositionTop];
                    return;
                }
                if ([TextViewItem.title isEqualToString:@"地区/时间"]) {
                    [dic setObject:TextViewItem.activity_date_id forKey:TextViewItem.title];
                }else{
                    [dic setObject:TextViewItem.text forKey:TextViewItem.title];
                }
            }else if ([item isKindOfClass:[FBSSelectItem class]]){
                FBSSelectItem *SelectItem = (FBSSelectItem *)item;
                if (SelectItem.typeMan == SelectTypeNone) {
                    [self.navigationController.view makeToast:[NSString stringWithFormat:@"选择%@",SelectItem.title]
                                                     duration:1.0
                                                     position:CSToastPositionTop];
                    return;
                }
                if (SelectItem.typeMan == SelectTypeMan) {
                    [dic setObject:@"先生" forKey:SelectItem.title];
                }else if (SelectItem.typeMan == SelectTypeWOMan){
                    [dic setObject:@"女士" forKey:SelectItem.title];
                }
            }else if ([item isKindOfClass:[FBSProtocalItem class]]){
                FBSProtocalItem *ProtocalItem = (FBSProtocalItem *)item;
                if (!ProtocalItem.isSelect) {
                    [self.navigationController.view makeToast:@"需要同意用户协议"
                                                     duration:1.0
                                                     position:CSToastPositionTop];
                    return;
                }
            }else if ([item isKindOfClass:[FBSButtonCell class]]){
                
            }
        }
        //跳转页面
        
        FBSActivityListDetailPayController *vc = [[FBSActivityListDetailPayController alloc] initWithActivitiItem:dic itemSop:self.itemShop];
        [self.navigationController pushViewController:vc animated:YES];
    }else if ([cell isKindOfClass:[FBSTextViewCell class]]){

    }
}
- (void)fl_spinner:(FLNiceSpinner *)spinner didSelectedItemAtIndex:(NSInteger)index{
    if (index == -1) {
        FBSTextViewItem *item =  [self.viewModel.items firstObject];
        item.text = @"";
        item.activity_date_id = @"";
        return;
    }
    FBSArticalDataModelArea_date_listItem *Area_date_listItem = [self.itemShop.area_date_list objectAtIndex:index];
    FBSTextViewItem *item =  [self.viewModel.items firstObject];
    item.text = Area_date_listItem.area_and_date;
    item.activity_date_id = Area_date_listItem.id;

}
- (NSInteger)fl_itemsCountOfSpinner:(FLNiceSpinner *)spinner{
    return self.itemShop.area_date_list.count;
}
- (NSString *)fl_spinner:(FLNiceSpinner *)spinner showItemStringAtIndex:(NSInteger)index{
    FBSArticalDataModelArea_date_listItem *Area_date_listItem = [self.itemShop.area_date_list objectAtIndex:index];
    
    return Area_date_listItem.area_and_date;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSAssert(self.viewModel != nil && [self.viewModel isKindOfClass:[FBSTableViewModel class]], @"FBSTableViewController  viewModel属性不能为空，且必须为FBSTableViewModel子类！");
    
    FBSItem *item  = [self.viewModel itemAtRow:indexPath.row inSection:indexPath.section];
    return item.cellHeight;
}

- (void)tableView:(UITableView *)tableView updatedHeight:(CGFloat)height atIndexPath:(NSIndexPath *)indexPath
{
    FBSItem *item  = [self.viewModel itemAtRow:indexPath.row inSection:indexPath.section];
    item.cellHeight = height;
}

- (void)tableView:(UITableView *)tableView updatedText:(NSString *)text atIndexPath:(NSIndexPath *)indexPath
{
    FBSItem *item  = [self.viewModel itemAtRow:indexPath.row inSection:indexPath.section];
    if ([item isKindOfClass:[FBSTextViewItem class]]) {
        FBSTextViewItem *itemTEXT = (FBSTextViewItem *)item;
        itemTEXT.text = text;
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSAssert(self.viewModel != nil && [self.viewModel isKindOfClass:[FBSTableViewModel class]], @"FBSTableViewController  viewModel属性不能为空，且必须为FBSTableViewModel子类！");
    
    FBSItem *item  = [self.viewModel itemAtRow:indexPath.row inSection:indexPath.section];
    FBSCell *cell;
    if ([[item class] reuseIdentifier].length) {
        cell = [self.tableView dequeueReusableCellWithIdentifier:[[item class] reuseIdentifier] forIndexPath:indexPath];
        cell.item = item;
        cell.delegate = self;
        if ([cell isKindOfClass:[FBSTextViewCell class]]) {
            FBSTextViewCell *cellText = (FBSTextViewCell *)cell;
            cellText.expandableTableView = tableView;
            cellText.vc = self;
        }
    }
    return cell;
}


- (FBSLuRuModel *)viewModel {
    if (!_viewModel) {
        _viewModel  = [[FBSLuRuModel alloc] init];
    }
    return _viewModel;
}


@end
