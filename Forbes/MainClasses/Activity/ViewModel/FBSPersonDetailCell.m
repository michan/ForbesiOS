//
//  FBSPersonDetailCell.m
//  Forbes
//
//  Created by 赵志辉 on 2019/9/25.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import "FBSPersonDetailCell.h"
#import "FBSPersonDetailItem.h"

@interface FBSPersonDetailCell()
@property (nonatomic, strong) UILabel *contenL;
@property (nonatomic, strong) UILabel *detail;
@property (nonatomic, strong) UILabel *name;
@property (nonatomic, strong) UILabel *phone;
@property (nonatomic, strong) UILabel *email;
@property (nonatomic, strong) UIView *lineView;



@end

@implementation FBSPersonDetailCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        
        [self.contentView addSubview:self.contenL]; //免费票
        [self.contentView addSubview:self.detail];
        [self.contentView addSubview:self.name];
        [self.contentView addSubview:self.phone];
        [self.contentView addSubview:self.email];
        [self.contentView addSubview:self.lineView];



        
        [self.contenL sizeToFit];
        [self.contenL setContentHuggingPriority:UILayoutPriorityRequired
                                        forAxis:UILayoutConstraintAxisHorizontal];
        [self.detail setContentHuggingPriority:UILayoutPriorityDefaultLow
                                       forAxis:UILayoutConstraintAxisHorizontal];
        [self.contenL mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(WScale(17));
            make.top.mas_equalTo(self).offset(WScale(15));
            make.height.mas_equalTo(WScale(25));
            make.right.mas_equalTo(self.detail.mas_left);
            
        }];
        [self.detail mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.contenL.mas_right);
            make.bottom.mas_equalTo(self.contenL.mas_bottom);
            make.height.mas_equalTo(WScale(14));
            make.right.mas_equalTo(self).offset(WScale(-13));
        }];
        
        [self.name mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.mas_left).offset(WScale(17));
            make.top.mas_equalTo(self.mas_top).offset(WScale(56));
            make.right.mas_equalTo(self).offset(WScale(-13));
            make.height.mas_equalTo(WScale(20));
        }];
        
        [self.phone mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.mas_left).offset(WScale(17));
            make.top.mas_equalTo(self.name.mas_bottom).offset(WScale(9));
            make.right.mas_equalTo(self).offset(WScale(-13));
            make.height.mas_equalTo(WScale(20));
        }];
        
        [self.email mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.mas_left).offset(WScale(17));
            make.top.mas_equalTo(self.phone).offset(WScale(9));
            make.right.mas_equalTo(self).offset(WScale(-13));
            make.bottom.mas_equalTo(self.mas_bottom).offset(WScale(-5));
        }];
        [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.mas_left);
            make.height.mas_equalTo(WScale(5));
            make.right.mas_equalTo(self);
            make.bottom.mas_equalTo(self.mas_bottom);
        }];
        
        
    }
    
    return self;
}



- (void)setItem:(FBSPersonDetailItem *)item{
    [super setItem:item];

    self.phone.text = item.phone;
    self.email.text = item.email;

    self.name.text = item.name;
    self.contenL.text = item.title;
    self.detail.text = item.detail;
}

- (UILabel *)contenL {
    if (!_contenL) {
        _contenL  = [[UILabel alloc] init];
        _contenL.textColor = Hexcolor(0x333333);
        _contenL.font = kLabelFontSize18;
        
    }
    return _contenL;
}

- (UILabel *)detail {
    if (!_detail) {
        _detail  = [[UILabel alloc] init];
        _detail.textColor = Hexcolor(0xD0021B);
        _detail.font = kLabelFontSize10;
    }
    return _detail;
}
- (UILabel *)name {
    if (!_name) {
        _name  = [[UILabel alloc] init];
        _name.textColor = Hexcolor(0x666666);
        _name.font = kLabelFontSize14;
    }
    return _name;
}
- (UILabel *)phone {
    if (!_phone) {
        _phone = [[UILabel alloc] init];
        _phone.textColor = Hexcolor(0x666666);
        _phone.font = kLabelFontSize14;
    }
    return _phone;
}
- (UILabel *)email {
    if (!_email) {
        _email = [[UILabel alloc] init];
        _email.textColor = Hexcolor(0x666666);
        _email.font = kLabelFontSize14;
    }
    return _email;
}
- (UIView *)lineView{
    if (!_lineView) {
        _lineView = [[UIView alloc] init];
        _lineView.backgroundColor = Hexcolor(0xF6F7FB);
    }
    return _lineView;
}
@end
