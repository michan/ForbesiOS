//
//  FBSActivituListShopModel.h
//  Forbes
//
//  Created by 赵志辉 on 2019/9/24.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import "FBSTableViewModel.h"
#import "FBSBannerCell.h"
#import "FBSOneImageCell.h"
#import "FBSThreeImageCell.h"
#import "FBSOnlyTextCell.h"
#import "FBSActivitiItem.h"
#import "FBSActivitiShopItem.h"
#import "FBSArticalModel.h"
#import "FBSArticalDataItem.h"
NS_ASSUME_NONNULL_BEGIN

@interface FBSActivituListShopModel : FBSTableViewModel
@property (nonatomic, strong) FBSGetRequest *detailAPI;

@property(nonatomic, strong)NSString *articleId;
@property(nonatomic, strong)FBSArticalDataItem *itemShop;
@property (nonatomic, strong)FBSArticalModel *model;
- (void)rqArticleDetail:(void(^)(BOOL success,NSString *bodyHTML))complete;


@end

NS_ASSUME_NONNULL_END
