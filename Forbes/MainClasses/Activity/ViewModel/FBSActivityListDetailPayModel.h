//
//  FBSActivityListDetailPayModel.h
//  Forbes
//
//  Created by 赵志辉 on 2019/9/25.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import "FBSTableViewModel.h"
#import "FBSArticalModel.h"
#import "FBSArticalDataItem.h"

NS_ASSUME_NONNULL_BEGIN

@interface FBSActivityListDetailPayModel : FBSTableViewModel

@property(nonatomic, strong)NSMutableDictionary *dic;
@property(nonatomic, strong)FBSArticalDataItem *itemShop;
- (void)createItem:(void(^)(BOOL success))complete;

- (void)requestBaoming:(NSDictionary *)dic complete:(void(^)(BOOL success))complete;

@end

NS_ASSUME_NONNULL_END
