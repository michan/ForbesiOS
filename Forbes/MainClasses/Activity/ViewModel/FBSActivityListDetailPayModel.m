//
//  FBSActivityListDetailPayModel.m
//  Forbes
//
//  Created by 赵志辉 on 2019/9/25.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import "FBSActivityListDetailPayModel.h"
#import "FBSButtonCell.h"
#import "FBSPersonDetailItem.h"
#import "FBSActivityPayDetailItem.h"
#import "FBSActivityPayTypeItem.h"


@implementation FBSActivityListDetailPayModel
- (void)createItem:(void(^)(BOOL success))complete{
    
    FBSPersonDetailItem *personItem = [[FBSPersonDetailItem alloc] init];
    personItem.title = @"联系方式";
    personItem.detail = @"（用于接受电子票验证码）";
    personItem.cellHeight = WScale(148);
    
    personItem.name = [NSString stringWithFormat:@"姓名：%@",self.dic[@"姓名"]];
    personItem.phone = [NSString stringWithFormat:@"手机：%@",self.dic[@"手机"]];
    personItem.email = [NSString stringWithFormat:@"邮箱：%@",self.dic[@"邮箱"]];

    personItem.bottomLineHidden = YES;

    [self.items addObject:personItem];

    
    FBSActivityPayDetailItem *item = [[FBSActivityPayDetailItem alloc] init];
    FBSArticalDataModelTicketsItem *ticketItem = [self.itemShop.tickets firstObject];
    item.title = @"交易信息";
    item.ticketPrice = ticketItem.price;
    item.timeAndAddress = [NSString stringWithFormat:@"活动时间：%@\n活动地址：%@",self.itemShop.activity_time,self.itemShop.activity_address];
    item.ticketType = ticketItem.type;
    item.examineVerify = ticketItem.remarks;
    item.totlePrice = [NSString stringWithFormat:@"%lf",[ticketItem.choseNum intValue] * [ticketItem.price floatValue]];
    item.number = ticketItem.choseNum;
    item.cellHeight = WScale(311);
    item.titleDetail = self.itemShop.title;
    
    [self.items addObject:item];

//    FBSActivityPayTypeItem *itemPay = [[FBSActivityPayTypeItem alloc] init];
//    [self.items addObject:itemPay];
//    itemPay.cellHeight = WScale(159);
    
    FBSButtonItem *itemBt = [[FBSButtonItem alloc] init];
    itemBt.bottomLineHidden = YES;
    itemBt.buttonText = @"下一步";
    itemBt.buttonTextColor =  Hexcolor(0xFFFFFF);
    itemBt.buttonBgColor = Hexcolor(0x8C734B);
    itemBt.cellHeight = WScale(120);
    [self.items addObject:itemBt];
    
    
    complete(YES);
    
}
- (void)requestBaoming:(NSDictionary *)dic complete:(void(^)(BOOL success))complete{
    FBSGetRequest *request = [[FBSGetRequest alloc] init];
    request.cacheHandler.writeMode = YBNetworkCacheWriteModeMemoryAndDisk;
    request.cacheHandler.readMode = YBNetworkCacheReadModeAlsoNetwork;
    request.requestMethod = YBRequestMethodPOST;
    request.requestURI = @"activity/form";
    request.requestParameter = dic;
    [request startWithSuccess:^(YBNetworkResponse * _Nonnull response) {
        NSNumber *num = response.responseObject[@"success"];
        if([[num stringValue] isEqualToString:@"1"]){
           complete(YES);
        }else{
           complete(NO);
        }
    } failure:^(YBNetworkResponse * _Nonnull response) {
        complete(NO);
    }];
}
@end
