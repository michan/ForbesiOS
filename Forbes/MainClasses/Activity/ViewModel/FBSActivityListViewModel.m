
//
//  FBSActivityListViewModel.m
//  Forbes
//
//  Created by 周灿华 on 2019/7/28.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSActivityListViewModel.h"
#import "FBSBannerModel.h"
#import "FBSActivitiesModel.h"
#import "FBSRecommendActivityModel.h"
#import "FBSnewActivitiesModel.h"
#import "FBSActivitiItem.h"
#import "FBSLasteActiviriModel.h"
#import "FBSActivitiListItem.h"


@interface FBSActivityListViewModel()

@property(nonatomic , strong)FBSBannerModel *bannerModel;
@property(nonatomic , strong)FBSActivitiesModel *ActivityModel; // 活动列表
@property(nonatomic , strong)FBSRecommendActivityModel *RecommendModel; // 轮播图
@property(nonatomic , strong)FBSLasteActiviriModel *latestActivityModel; // 获取最新活动列表
@property(nonatomic , strong)FBSnewActivitiesModel *NewActivityModel; // 获取要闻活动列表




@end

@implementation FBSActivityListViewModel

// 请求轮播图接口
- (void)requestBanner:(void(^)(BOOL success))complete{
    FBSGetRequest *request = [[FBSGetRequest alloc] init];
    request.cacheHandler.writeMode = YBNetworkCacheWriteModeMemoryAndDisk;
    request.cacheHandler.readMode = YBNetworkCacheReadModeAlsoNetwork;
    request.requestMethod = YBRequestMethodGET;
    request.requestURI = @"focus/channels";
    //request.requestParameter = @{@"key":@"0e27c575047e83b407ff9e517cde9c76", @"type":@"2", @"text":@"呵呵呵呵"};
    WEAKSELF;
    [request startWithSuccess:^(YBNetworkResponse * _Nonnull response) {
        weakSelf.bannerModel = [FBSBannerModel mj_objectWithKeyValues:response.responseObject];
        [weakSelf createItem];
        complete(YES);
    } failure:^(YBNetworkResponse * _Nonnull response) {
        
    }];
}
// 请求活动列表
- (void)requestActivities:(void(^)(BOOL success))complete{
    FBSGetRequest *request = [[FBSGetRequest alloc] init];
    request.cacheHandler.writeMode = YBNetworkCacheWriteModeMemoryAndDisk;
    request.cacheHandler.readMode = YBNetworkCacheReadModeAlsoNetwork;
    request.requestMethod = YBRequestMethodGET;
    request.requestURI = @"activities";
    //request.requestParameter = @{@"key":@"0e27c575047e83b407ff9e517cde9c76", @"type":@"2", @"text":@"呵呵呵呵"};
    WEAKSELF;
    [request startWithSuccess:^(YBNetworkResponse * _Nonnull response) {
        weakSelf.ActivityModel = [FBSActivitiesModel mj_objectWithKeyValues:response.responseObject];
        [weakSelf requestLatestActivitiesIsRefresh:NO complete:^(BOOL success) {
            complete(success);
        }];
    } failure:^(YBNetworkResponse * _Nonnull response) {
        
    }];
}
// 请求最新活动列表
- (void)requestLatestActivitiesIsRefresh:(BOOL)isRefresh complete:(void(^)(BOOL success))complete{
    FBSGetRequest *request = [[FBSGetRequest alloc] init];
    request.cacheHandler.writeMode = YBNetworkCacheWriteModeMemoryAndDisk;
    request.cacheHandler.readMode = YBNetworkCacheReadModeAlsoNetwork;
    request.requestMethod = YBRequestMethodGET;
    request.requestURI = @"latest/activities";
    //request.requestParameter = @{@"key":@"0e27c575047e83b407ff9e517cde9c76", @"type":@"2", @"text":@"呵呵呵呵"};
    WEAKSELF;
    [request startWithSuccess:^(YBNetworkResponse * _Nonnull response) {
        weakSelf.latestActivityModel = [FBSLasteActiviriModel mj_objectWithKeyValues:response.responseObject];
        if (isRefresh) {
            [weakSelf refreshItem];
            complete(YES);
        }else{
            [weakSelf requestActivitiesaNewsisloadMore:NO complete:^(BOOL success,NSInteger count) {
                complete(success);
            }];
        }
    } failure:^(YBNetworkResponse * _Nonnull response) {
        
    }];
}
// 请求活动面要闻
- (void)requestActivitiesaNewsisloadMore:(BOOL )isload complete:(void(^)(BOOL success, NSInteger count))complete{
    FBSGetRequest *request = [[FBSGetRequest alloc] init];
    request.cacheHandler.writeMode = YBNetworkCacheWriteModeMemoryAndDisk;
    request.cacheHandler.readMode = YBNetworkCacheReadModeAlsoNetwork;
    request.requestMethod = YBRequestMethodGET;
    request.requestURI = [NSString stringWithFormat:@"activity/news/%ld",self.page];
    WEAKSELF;
    [request startWithSuccess:^(YBNetworkResponse * _Nonnull response) {
        weakSelf.NewActivityModel = [FBSnewActivitiesModel mj_objectWithKeyValues:response.responseObject];
        if (isload) {
            [weakSelf createLoadMoreNewsItem];
        }else{
            [weakSelf createItem];
        }
        complete(YES,self.NewActivityModel.data.list.count);
    } failure:^(YBNetworkResponse * _Nonnull response) {
        
    }];
}
- (void)refreshItem{
    NSMutableArray *arraySection2 = [NSMutableArray array];
    NSDictionary *ItemDic = [self.latestActivityModel.data mj_keyValues];
    FBSActivitiListItem *oneImgeItem = [FBSActivitiListItem mj_objectWithKeyValues:ItemDic] ;
    [arraySection2 addObject:oneImgeItem];
//    oneImgeItem.cellHeight = self.latestActivityModel.data.items.count > 2 ? WScale(390) : WScale(200);
    [self.items replaceObjectAtIndex:1 withObject:arraySection2];
}
- (void)createLoadMoreNewsItem{
    NSMutableArray *arraySection3 = self.items.lastObject;
    for (ListNewActivitieItem *newItemMode in self.NewActivityModel.data.list) {
        FBSOneImageItem *oneImgeItem = [FBSOneImageItem item];
        oneImgeItem.detailType = DetailPageTypeArticle;
        oneImgeItem.ID = newItemMode.id;
        oneImgeItem.content = newItemMode.title;
        oneImgeItem.author = newItemMode.nickname;
        oneImgeItem.comment = newItemMode.description;
        oneImgeItem.time = [NSString publishedTimeFormatWithTimeStamp:newItemMode.updated_at];
        oneImgeItem.imgUrl = newItemMode.file_url;
        oneImgeItem.cellHeight = WScale(95);
        [arraySection3 addObject:oneImgeItem];
    }
    [self.items replaceObjectAtIndex:2 withObject:arraySection3];
}
// 请求轮播图列表
- (void)requestRecommend:(void(^)(BOOL success))complete{
    FBSGetRequest *request = [[FBSGetRequest alloc] init];
    request.cacheHandler.writeMode = YBNetworkCacheWriteModeMemoryAndDisk;
    request.cacheHandler.readMode = YBNetworkCacheReadModeAlsoNetwork;
    request.requestMethod = YBRequestMethodGET;
    request.requestURI = @"recommend/activities";
    //request.requestParameter = @{@"key":@"0e27c575047e83b407ff9e517cde9c76", @"type":@"2", @"text":@"呵呵呵呵"};
    WEAKSELF;
    [request startWithSuccess:^(YBNetworkResponse * _Nonnull response) {
        weakSelf.RecommendModel = [FBSRecommendActivityModel mj_objectWithKeyValues:response.responseObject];
        [weakSelf requestActivities:^(BOOL success) {
            complete(success);
        }];
    } failure:^(YBNetworkResponse * _Nonnull response) {
        
    }];
    
    
}

- (void)createItem {
    //滚动列表
    NSMutableArray *itemArray = [NSMutableArray array];
    NSMutableArray *arraySection = [NSMutableArray array];
    NSMutableArray *infos  = [NSMutableArray array];
    for (FBSRecommendItemModel *ItemModel in self.RecommendModel.data) {
        FBSBannerInfo *info = [FBSBannerInfo new];
        info.title = ItemModel.title;
        info.author = ItemModel.activity_sponsor;
        info.time = ItemModel.activity_time;
        info.imgUrl = ItemModel.file_url;
        [infos addObject:info];
    }
    FBSBannerItem *bannerItem = [FBSBannerItem item];
    bannerItem.datas = [infos copy];
    [arraySection addObject:bannerItem];
    
    // 活动列表
    for (FBSActivitiesItemModel *ActivitiesItemModel in self.ActivityModel.data) {
        NSDictionary *ItemDic = [ActivitiesItemModel mj_keyValues];
        FBSActivitiItem *oneImgeItem = [FBSActivitiItem mj_objectWithKeyValues:ItemDic];
        oneImgeItem.isClick = YES;
        oneImgeItem.cellHeight = WScale(145);
        [arraySection addObject:oneImgeItem];
        
    }
    [itemArray addObject:arraySection];
    // 最新活动列表活动列表
    NSMutableArray *arraySection2 = [NSMutableArray array];
//    [self.latestActivityModel.data.items addObjectsFromArray:self.latestActivityModel.data.items];
//    [self.latestActivityModel.data.items addObjectsFromArray:self.latestActivityModel.data.items];
//    NSDictionary *ItemDic = [self.latestActivityModel.data mj_keyValues];
//    FBSActivitiListItem *oneImgeItem = [FBSActivitiListItem mj_objectWithKeyValues:ItemDic] ;
//    [arraySection2 addObject:oneImgeItem];
//    oneImgeItem.cellHeight = self.latestActivityModel.data.items.count > 2 ? WScale(390) : WScale(200);
    [itemArray addObject:arraySection2];
    
    
     NSMutableArray *arraySection3 = [NSMutableArray array];
    for (ListNewActivitieItem *newItemMode in self.NewActivityModel.data.list) {
        FBSOneImageItem *oneImgeItem = [FBSOneImageItem item];
        oneImgeItem.detailType = DetailPageTypeArticle;
        oneImgeItem.ID = newItemMode.id;
        oneImgeItem.content = newItemMode.title;
        oneImgeItem.author = newItemMode.nickname;
        oneImgeItem.comment = newItemMode.description;
        oneImgeItem.time = [NSString publishedTimeFormatWithTimeStamp:newItemMode.updated_at];
        oneImgeItem.imgUrl = newItemMode.file_url;
        oneImgeItem.cellHeight = WScale(95);
        [arraySection3 addObject:oneImgeItem];
    }
    [itemArray addObject:arraySection3];
    
    // 防止上面数据处理过长造成p崩溃
    self.items = itemArray;
}


@end
