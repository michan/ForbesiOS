//
//  FBSLuRuModel.h
//  Forbes
//
//  Created by 赵志辉 on 2019/9/25.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import "FBSTableViewModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface FBSLuRuModel : FBSTableViewModel
- (void)createItem:(void(^)(BOOL success))complete;

@end

NS_ASSUME_NONNULL_END
