//
//  FBSLuRuModel.m
//  Forbes
//
//  Created by 赵志辉 on 2019/9/25.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import "FBSLuRuModel.h"
#import "FBSTextViewItem.h"
#import "FBSSelectItem.h"
#import "FBSButtonCell.h"
#import "FBSProtocalItem.h"

@implementation FBSLuRuModel
- (void)createItem:(void(^)(BOOL success))complete{
    
    
    FBSTextViewItem *item = [[FBSTextViewItem alloc] init];
    item.bottomLineHidden = YES;
    item.cellHeight = WScale(97);
    item.title = @"地区/时间";
    item.detail = @"(必填*)";
    item.isEdit = YES;
    [self.items addObject:item];
    
    FBSTextViewItem *item1 = [[FBSTextViewItem alloc] init];
    item1.bottomLineHidden = YES;
    item1.cellHeight = WScale(97);
    item1.title = @"姓名";
    item1.detail = @"(必填*)";
    [self.items addObject:item1];
    
    FBSTextViewItem *item2 = [[FBSTextViewItem alloc] init];
    item2.bottomLineHidden = YES;
    item2.cellHeight = WScale(97);
    item2.title = @"手机";
    item2.detail = @"(必填*)";
    [self.items addObject:item2];
    
    FBSTextViewItem *item3 = [[FBSTextViewItem alloc] init];
    item3.bottomLineHidden = YES;
    item3.cellHeight = WScale(97);
    item3.title = @"邮箱";
    item3.detail = @"(必填*)";
    [self.items addObject:item3];
    
    FBSSelectItem *item4 = [[FBSSelectItem alloc] init];
    item4.bottomLineHidden = YES;
    item4.cellHeight = WScale(97);
    item4.title = @"性别";
    item4.detail = @"(必选*)";
    item4.typeMan = SelectTypeNone;
    [self.items addObject:item4];
    
    
    FBSTextViewItem *item5 = [[FBSTextViewItem alloc] init];
    item5.bottomLineHidden = YES;
    item5.cellHeight = WScale(97);
    item5.title = @"职位";
    item5.detail = @"(必填*)";
    [self.items addObject:item5];
    
    FBSTextViewItem *item6 = [[FBSTextViewItem alloc] init];
    item6.bottomLineHidden = YES;

    item6.cellHeight = WScale(97);
    item6.title = @"公司名称";
    item6.detail = @"(必填*)";
    [self.items addObject:item6];
    
    FBSProtocalItem *itemPR = [[FBSProtocalItem alloc] init];
    itemPR.bottomLineHidden = YES;
    itemPR.title = @"我已阅读《福布斯中文网普通用户注册协议》";
    
    [self.items addObject:itemPR];
    
    FBSButtonItem *itemBt = [[FBSButtonItem alloc] init];
    itemBt.bottomLineHidden = YES;
    itemBt.buttonText = @"下一步";
    itemBt.buttonTextColor =  Hexcolor(0xFFFFFF);
    itemBt.buttonBgColor = Hexcolor(0x8C734B);
    itemBt.cellHeight = WScale(120);

    
    [self.items addObject:itemBt];

    
    complete(YES);
    
}
@end
