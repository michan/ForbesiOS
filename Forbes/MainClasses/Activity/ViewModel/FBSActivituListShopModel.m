//
//  FBSActivituListShopModel.m
//  Forbes
//
//  Created by 赵志辉 on 2019/9/24.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import "FBSActivituListShopModel.h"
#import "FBSActivitiShopItem.h"
#import "FBSButtonCell.h"
#import "FBSArticalDataItem.h"

@implementation FBSActivituListShopModel

- (void)createItem{
    
    FBSActivitiItem *item = [FBSActivitiItem mj_objectWithKeyValues:self.model.data];
    item.cellHeight = WScale(145);
    item.isClick = NO;
    [self.items addObject:item];
    NSDictionary *dic = [self.model.data mj_keyValues];
    FBSArticalDataItem *itemD = [FBSArticalDataItem mj_objectWithKeyValues:dic];
    self.itemShop = itemD;
//    for (FBSArticalDataModelTicketsItem *item in self.model.data.tickets) {
//        item.cellHeight = WScale(120);
//        [self.items addObject:item];
//    }
    [self.items addObject:itemD];
    FBSButtonItem *itemBt = [[FBSButtonItem alloc] init];
    itemBt.buttonText = @"下一步";
    itemBt.buttonTextColor =  Hexcolor(0xFFFFFF);
    itemBt.buttonBgColor = Hexcolor(0x8C734B);
    itemBt.cellHeight = kScreenHeight - item.cellHeight - kNavTopMargin - WScale(120);
    
    [self.items addObject:itemBt];

}
- (void)rqArticleDetail:(void(^)(BOOL success,NSString *bodyHTML))complete{
    [self.detailAPI startWithSuccess:^(YBNetworkResponse * _Nonnull response) {
        
            self.model = [FBSArticalModel mj_objectWithKeyValues:response.responseObject];
            NSDictionary *parts = [response.responseObject valueForKeyPath:@"data"];
            NSString *content = parts[@"describe"];
            [self createItem];
            complete(YES, content);
    } failure:^(YBNetworkResponse * _Nonnull response) {
        complete(NO, nil);
    }];
}
- (FBSGetRequest *)detailAPI {
    if (!_detailAPI) {
        _detailAPI = [[FBSGetRequest alloc] init];
        _detailAPI.requestURI = [NSString stringWithFormat:@"activity/%@",self.articleId];
    }
    return _detailAPI;
}
@end
