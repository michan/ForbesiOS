//
//  FBSActivityViewModel.h
//  Forbes
//
//  Created by 周灿华 on 2019/7/28.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSViewModel.h"
#import "FBSBannerCell.h"
#import "FBSOneImageCell.h"
#import "FBSThreeImageCell.h"
#import "FBSOnlyTextCell.h"

@interface FBSActivityViewModel : FBSTableViewModel

- (void)requestBanner:(void(^)(BOOL success))complete;
- (void)requestActivities:(void(^)(BOOL success))complete;
- (void)requestRecommend:(void(^)(BOOL success))complete;
// 请求活动面要闻
- (void)requestActivitiesaNewsisloadMore:(BOOL )isload complete:(void(^)(BOOL success,NSInteger count))complete;
- (void)requestLatestActivitiesIsRefresh:(BOOL )isRefresh complete:(void(^)(BOOL success))complete;

@end

