//
//  FBSInterstitialViewController.h
//  Forbes
//
//  Created by 赵志辉 on 2019/9/19.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import "FBSViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface FBSInterstitialViewController : FBSViewController

@end

NS_ASSUME_NONNULL_END
