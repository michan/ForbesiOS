//
//  FBSDFPBannerViewController.m
//  Forbes
//
//  Created by 赵志辉 on 2019/9/19.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import "FBSDFPBannerViewController.h"

@interface FBSDFPBannerViewController ()<GADBannerViewDelegate,GADAdSizeDelegate,GADAppEventDelegate>


@property(nonatomic, strong) DFPBannerView *bannerView;


@end

@implementation FBSDFPBannerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    // In this case, we instantiate the banner with desired ad size.
    //设置横幅广告
    [self setBanner];
    
}
- (void)setBanner{
    // 自定义尺寸
    //GADAdSize customAdSize = GADAdSizeFromCGSize(CGSizeMake(250, 250));
    self.bannerView = [[DFPBannerView alloc]
                       initWithAdSize:kGADAdSizeBanner];
    self.bannerView.adUnitID = @"ca-app-pub-3940256099942544/2934735716";
    self.bannerView.rootViewController = self;
    [self.bannerView loadRequest:[DFPRequest request]];
    self.bannerView.delegate = self;
    self.bannerView.adSizeDelegate = self;
    self.bannerView.appEventDelegate = self;
    // 直接添加不推荐
    // [self addBannerViewToView:self.bannerView];
    //设置多种尺寸
    self.bannerView.validAdSizes = @[
                                     NSValueFromGADAdSize(kGADAdSizeBanner),
                                     NSValueFromGADAdSize(kGADAdSizeMediumRectangle),
                                     NSValueFromGADAdSize(GADAdSizeFromCGSize(CGSizeMake(120, 20)))
                                     ];
}

- (void)addBannerViewToView:(UIView *)bannerView {
    bannerView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:bannerView];
    
    
//    [self.bannerView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.centerX.mas_equalTo(self.view.centerX);
//        make.top.mas_offset(self.view.top).mas_offset(10);
//    }];
    [self.view addConstraints:@[
                                [NSLayoutConstraint constraintWithItem:bannerView
                                                             attribute:NSLayoutAttributeBottom
                                                             relatedBy:NSLayoutRelationEqual
                                                                toItem:self.bottomLayoutGuide
                                                             attribute:NSLayoutAttributeTop
                                                            multiplier:1
                                                              constant:0],
                                [NSLayoutConstraint constraintWithItem:bannerView
                                                             attribute:NSLayoutAttributeCenterX
                                                             relatedBy:NSLayoutRelationEqual
                                                                toItem:self.view
                                                             attribute:NSLayoutAttributeCenterX
                                                            multiplier:1
                                                              constant:0]
                                ]];
}
#pragma mark event delegate
- (void)adView:(DFPBannerView *)banner
didReceiveAppEvent:(NSString *)name
      withInfo:(NSString *)info {
    if ([name isEqual:@"color"]) {
        if ([info isEqual:@"green"]) {
            // Set background color to green.
            self.view.backgroundColor = [UIColor greenColor];
        } else if ([info isEqual:@"blue"]) {
            // Set background color to blue.
            self.view.backgroundColor = [UIColor blueColor];
        } else
        // Set background color to black.
        self.view.backgroundColor = [UIColor blackColor];
    }
}
#pragma mark size delegate
- (void)adView:(DFPBannerView *)view willChangeAdSizeTo:(GADAdSize)size;
{
    
}
    
#pragma mark DFPBannerView delegate
/// Tells the delegate an ad request loaded an ad.
/**
 广告加载成功之后添加到视图上
 
 @param adView 横幅广告
 */
- (void)adViewDidReceiveAd:(DFPBannerView *)adView {
    // Add adView to view and add constraints as above.
    [self addBannerViewToView:self.bannerView];
    adView.alpha = 0;
    [UIView animateWithDuration:1.0 animations:^{
        adView.alpha = 1;
    }];

}

/// Tells the delegate an ad request failed.
- (void)adView:(DFPBannerView *)adView
didFailToReceiveAdWithError:(GADRequestError *)error {
    NSLog(@"adView:didFailToReceiveAdWithError: %@", [error localizedDescription]);
}

/// Tells the delegate that a full-screen view will be presented in response
/// to the user clicking on an ad.
- (void)adViewWillPresentScreen:(DFPBannerView *)adView {
    NSLog(@"adViewWillPresentScreen");
}

/// Tells the delegate that the full-screen view will be dismissed.
- (void)adViewWillDismissScreen:(DFPBannerView *)adView {
    NSLog(@"adViewWillDismissScreen");
}

/// Tells the delegate that the full-screen view has been dismissed.
- (void)adViewDidDismissScreen:(DFPBannerView *)adView {
    NSLog(@"adViewDidDismissScreen");
}

/// Tells the delegate that a user click will open another app (such as
/// the App Store), backgrounding the current app.
- (void)adViewWillLeaveApplication:(DFPBannerView *)adView {
    NSLog(@"adViewWillLeaveApplication");
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
