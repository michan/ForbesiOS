//
//  FBSInterstitialViewController.m
//  Forbes
//
//  Created by 赵志辉 on 2019/9/19.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import "FBSInterstitialViewController.h"

@interface FBSInterstitialViewController ()<GADInterstitialDelegate>

@property(nonatomic, strong) DFPInterstitial *interstitial;

@end

@implementation FBSInterstitialViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    UIButton *bu = [UIButton buttonWithType:UIButtonTypeCustom];
    bu.backgroundColor = [UIColor redColor];
    [bu addTarget:self action:@selector(doSomething:) forControlEvents:UIControlEventTouchUpInside];
    bu.frame = CGRectMake(100, 100, 100, 100);
    [self.view addSubview:bu];
    // Do any additional setup after loading the view.

    self.interstitial = [self createAndLoadInterstitial];
}

- (DFPInterstitial *)createAndLoadInterstitial {
    DFPInterstitial *interstitial =
    [[DFPInterstitial alloc] initWithAdUnitID:@"/6499/example/interstitial"];
    interstitial.delegate = self;
    [interstitial loadRequest:[DFPRequest request]];
    
    [interstitial addObserver:self forKeyPath:@"isReady" options:NSKeyValueObservingOptionOld|NSKeyValueObservingOptionNew context:nil];
    
    return interstitial;
}
- (void)addObserver:(NSObject *)observer forKeyPath:(NSString *)keyPath options:(NSKeyValueObservingOptions)options context:(void *)context
{
    if ([keyPath isEqualToString:@"isReady"]) {
        
    }
}
- (void)interstitialDidDismissScreen:(DFPInterstitial *)interstitial {
    self.interstitial = [self createAndLoadInterstitial];
}
- (void)doSomething:(id)sender {
    if (self.interstitial.isReady) {
        [self.interstitial presentFromRootViewController:self];
    } else {
        NSLog(@"Ad wasn't ready");
    }
}
/// Tells the delegate an ad request succeeded.
- (void)interstitialDidReceiveAd:(DFPInterstitial *)ad {
    NSLog(@"interstitialDidReceiveAd");
    NSLog(@"interstitialDidReceiveAd %d",ad.isReady ? 1 : 0);

    
}

/// Tells the delegate an ad request failed.
- (void)interstitial:(DFPInterstitial *)ad
didFailToReceiveAdWithError:(GADRequestError *)error {
    NSLog(@"interstitial:didFailToReceiveAdWithError: %@", [error localizedDescription]);
}

/// Tells the delegate that an interstitial will be presented.
- (void)interstitialWillPresentScreen:(DFPInterstitial *)ad {
    NSLog(@"interstitialWillPresentScreen");
}

/// Tells the delegate the interstitial is to be animated off the screen.
- (void)interstitialWillDismissScreen:(DFPInterstitial *)ad {
    NSLog(@"interstitialWillDismissScreen");
}

/// Tells the delegate the interstitial had been animated off the screen.


/// Tells the delegate that a user click will open another app
/// (such as the App Store), backgrounding the current app.
- (void)interstitialWillLeaveApplication:(DFPInterstitial *)ad {
    NSLog(@"interstitialWillLeaveApplication");
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
