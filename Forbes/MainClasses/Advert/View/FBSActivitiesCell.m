//
//  FBSActivitiesCell.m
//  Forbes
//
//  Created by 赵志辉 on 2019/9/23.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import "FBSActivitiesCell.h"
#import "FBSActivitiItem.h"

@interface FBSActivitiesCell()

@property (nonatomic, strong) UILabel *contenL;
@property (nonatomic, strong) UIImageView *imgV;
@property (nonatomic, strong) UILabel *descL;
@property (nonatomic, strong) UILabel *time;
@property (nonatomic, strong) UILabel *price;
@property (nonatomic, strong) UIButton *baoming;
@property (nonatomic, strong) UILabel *line;
@property (nonatomic, strong) NSDictionary *attributeDic;

@end

@implementation FBSActivitiesCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        [self.contentView addSubview:self.contenL];
        [self.contentView addSubview:self.descL];
        [self.contentView addSubview:self.imgV];
        [self.contentView addSubview:self.time];
        [self.contentView addSubview:self.price];
        [self.contentView addSubview:self.baoming];
        [self.contentView addSubview:self.line];


        
        [self.imgV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.mas_equalTo(WScale(117));
            make.height.mas_equalTo(WScale(74));
            make.left.mas_equalTo(WScale(12));
            make.top.mas_equalTo(WScale(15));
        }];
        
        [self.contenL mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(WScale(138));
            make.top.mas_equalTo(self.imgV);
            make.right.mas_equalTo(self.right).offset(-WScale(12));
            make.height.mas_equalTo(WScale(50));
        }];
        
        [self.descL mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.contenL);
            make.top.mas_equalTo(self.contenL.mas_bottom);
            make.right.mas_equalTo(self.right).offset(-WScale(12));
            make.height.mas_equalTo(WScale(15));
        }];
        
        [self.time mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.contenL);
            make.top.mas_equalTo(self.descL.mas_bottom);
            make.right.mas_equalTo(self.right).offset(-WScale(12));
            make.height.mas_equalTo(WScale(15));
        }];
        
        [self.price mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.contenL);
            make.top.mas_equalTo(self.contenL.mas_top).offset(WScale(93));
            make.right.mas_equalTo(self.right).offset(-WScale(12));
            make.bottom.mas_equalTo(self.mas_bottom).offset(WScale(-17));
        }];
        [self.line mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self);
            make.height.mas_equalTo(WScale(5));
            make.right.mas_equalTo(self);
            make.bottom.mas_equalTo(self.mas_bottom);
        }];
        [self.baoming mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(self.mas_right).offset(-WScale(12));
            make.top.mas_equalTo(self.contenL.mas_top).offset(WScale(93));
            make.width.mas_equalTo(WScale(84));
            make.height.mas_equalTo(WScale(22));
        }];
    
        
        
        
    }
    
    return self;
}



- (void)setItem:(FBSActivitiItem *)item {
    if (![item isKindOfClass:[FBSActivitiItem class]]) {
        return ;
    }

    [super setItem:item];
    self.contenL.text = item.title;
    self.contenL.lineBreakMode = NSLineBreakByTruncatingTail;
    if (item.file_url.length) {
        [self.imgV sd_setImageWithURL:[NSURL URLWithString:item.file_url]];
    } else {
        self.imgV.image = nil;
    }

    if (item.activity_address.length) {
        self.descL.text = item.activity_address;
    }
    self.time.text = item.activity_time;
    TicketsItem *ticket = [item.tickets firstObject];
    self.baoming.hidden = !item.isClick;
    self.price.text = [NSString stringWithFormat:@"￥%@起",ticket.price];
}

#pragma mark - property

- (UILabel *)contenL {
    if (!_contenL) {
        _contenL  = [[UILabel alloc] init];
        _contenL.textColor = Hexcolor(0x333333);
        _contenL.font = kLabelFontSize18;
        _contenL.lineBreakMode = NSLineBreakByTruncatingTail;
        _contenL.numberOfLines = 2;
    }
    return _contenL;
}

- (UILabel *)descL {
    if (!_descL) {
        _descL  = [[UILabel alloc] init];
        _descL.textColor = Hexcolor(0x666666);
        _descL.font = kLabelFontSize11;
    }
    return _descL;
}

- (UIImageView *)imgV {
    if (!_imgV) {
        _imgV  = [[UIImageView alloc] init];
        _imgV.backgroundColor = RandomColor;
    }
    return _imgV;
}
- (UILabel *)time{
    if (!_time) {
        _time = [[UILabel alloc] init];
        _time.textColor = Hexcolor(0x666666);
        _time.font = kLabelFontSize11;

    }
    return _time;
}
- (UILabel *)line{
    if (!_line) {
        _line = [[UILabel alloc] init];
        _line.backgroundColor = Hexcolor(0xF6F7F9);
    }
    return _line;
}
- (UILabel *)price{
    if (!_price) {
        _price = [[UILabel alloc] init];
        _price.textColor = Hexcolor(0x8C734B);
        _price.font = kLabelFontSize14;
        
    }
    return _price;
}
- (UIButton *)baoming{
    if (!_baoming) {
        _baoming = [UIButton buttonWithType:UIButtonTypeCustom];
        _baoming.titleLabel.textColor = Hexcolor(0xFFFFFF);
        _baoming.titleLabel.font = kLabelFontSize13;
        _baoming.backgroundColor = Hexcolor(0x8C734B);
        _baoming.layer.cornerRadius = WScale(11);
        [_baoming setTitle:@"立即报名" forState:UIControlStateNormal];
        [_baoming addTarget:self action:@selector(clickTo) forControlEvents:UIControlEventTouchUpInside];
    }
    return _baoming;
}
- (void)clickTo{
    if ([self.delegate respondsToSelector:@selector(fbsCell:didClickButtonAtIndex:)]) {
        [self.delegate fbsCell:self didClickButtonAtIndex:0];
    }
}
- (NSDictionary *)attributeDic {
    if (!_attributeDic) {
        NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc] init];
        style.lineSpacing = 5;
        
        _attributeDic  = @{
                           NSForegroundColorAttributeName : Hexcolor(0x333333),
                           NSFontAttributeName : kLabelFontSize18,
                           NSParagraphStyleAttributeName : style
                           };
    }
    return _attributeDic;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
