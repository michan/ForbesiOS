//
//  FBSActivitiesModel.m
//  Forbes
//
//  Created by 赵志辉 on 2019/9/23.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import "FBSActivitiesModel.h"

@implementation TicketsItem
@end


@implementation FBSActivitiesItemModel

+ (NSDictionary *)mj_objectClassInArray {
    
    return @{@"tickets" : @"TicketsItem"};
}
@end


@implementation FBSActivitiesModel

+ (NSDictionary *)mj_objectClassInArray {
    return @{@"data" : @"FBSActivitiesItemModel"};
}
@end
