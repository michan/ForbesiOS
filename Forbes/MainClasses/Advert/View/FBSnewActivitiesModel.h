//
//  FBSnewActivitiesModel.h
//  Forbes
//
//  Created by 赵志辉 on 2019/9/23.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import "FBSBaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface ListNewActivitieItem :FBSBaseModel
@property (nonatomic , copy) NSString              * id;
@property (nonatomic , copy) NSString              * title;
@property (nonatomic , copy) NSString              * Description;
@property (nonatomic , copy) NSString              * updated_at;
@property (nonatomic , copy) NSString              * user_id;
@property (nonatomic , copy) NSString              * nickname;
@property (nonatomic , copy) NSString              * file_url;

@end


@interface FBSnewActivitiesItemModel :FBSBaseModel
@property (nonatomic , strong) NSArray <ListNewActivitieItem *>              * list;
@property (nonatomic , copy) NSString              * page;
@property (nonatomic , copy) NSString              * total;
@property (nonatomic , copy) NSString              * limit;

@end


@interface FBSnewActivitiesModel :FBSBaseModel
@property (nonatomic , strong) FBSnewActivitiesItemModel              * data;
@end

NS_ASSUME_NONNULL_END
