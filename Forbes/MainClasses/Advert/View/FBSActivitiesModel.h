//
//  FBSActivitiesModel.h
//  Forbes
//
//  Created by 赵志辉 on 2019/9/23.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import "FBSBaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface TicketsItem :FBSItem
@property (nonatomic , copy) NSString              * id;
@property (nonatomic , copy) NSString              * activity_id;
@property (nonatomic , copy) NSString              * title;
@property (nonatomic , copy) NSString              * type;
@property (nonatomic , copy) NSString              * price;
@property (nonatomic , copy) NSString              * remarks;
@property (nonatomic , copy) NSString              * number;
@property (nonatomic , copy) NSString              * receive_number;
@property (nonatomic , copy) NSString              * created_at;
@property (nonatomic , copy) NSString              * updated_at;
//@property (nonatomic , copy) NSString              * choseNum;

@end


@interface FBSActivitiesItemModel :NSObject
@property (nonatomic , copy) NSString            *   id;
@property (nonatomic , copy) NSString              * title;
@property (nonatomic , copy) NSString              * activity_address;
@property (nonatomic , copy) NSString              * file_url;
@property (nonatomic , copy) NSString              * activity_sponsor;
@property (nonatomic , copy) NSString              * activity_time;
@property (nonatomic , copy) NSString            *   start_time;
@property (nonatomic , copy) NSString            *   end_time;
@property (nonatomic , strong) NSArray <TicketsItem *>              * tickets;

@end


@interface FBSActivitiesModel :NSObject
@property (nonatomic , strong) NSArray <FBSActivitiesItemModel *>              * data;


@end
NS_ASSUME_NONNULL_END
