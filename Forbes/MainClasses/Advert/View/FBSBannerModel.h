//
//  FBSBannerModel.h
//  Forbes
//
//  Created by 赵志辉 on 2019/9/23.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import "FBSBaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface FBSBannerItemModel : FBSBaseModel

@property (nonatomic , copy) NSString              * type;
@property (nonatomic , copy) NSString              * title;
@property (nonatomic , copy) NSString              * user_id;
@property (nonatomic , copy) NSString              * nickname;
@property (nonatomic , copy) NSString              * file_url;
@property (nonatomic , copy) NSString              * article_id;
@property (nonatomic , copy) NSString              * channel_id;
@property (nonatomic , copy) NSString              * channel_title;
@end

@interface FBSBannerModel : FBSBaseModel

@property (nonatomic , strong) NSArray <FBSBannerItemModel *>* data;

@end

NS_ASSUME_NONNULL_END
