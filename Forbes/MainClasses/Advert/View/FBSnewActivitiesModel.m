//
//  FBSnewActivitiesModel.m
//  Forbes
//
//  Created by 赵志辉 on 2019/9/23.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import "FBSnewActivitiesModel.h"

@implementation ListNewActivitieItem


+ (NSDictionary *)mj_replacedKeyFromPropertyName {
    return @{@"Description" : @"description"};
}

@end

@implementation FBSnewActivitiesItemModel

+ (NSDictionary *)mj_objectClassInArray {
    return @{@"list" : @"ListNewActivitieItem"};
}

@end
@implementation FBSnewActivitiesModel


@end
