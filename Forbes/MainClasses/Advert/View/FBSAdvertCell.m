//
//  FBSAdvertCell.m
//  Forbes
//
//  Created by 赵志辉 on 2019/9/20.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import "FBSAdvertCell.h"
#import "UIImageView+WebCache.h"


@implementation FBSAdvertItem

- (void)ctreatItemData:(FBSBaseModel *)model{
    if ([model isKindOfClass:[FBSAdsData class]]) {
        FBSAdsData *API = (FBSAdsData *)model;
//        NSDictionary *dict = model.mj_keyValues;
//        FBSAdvertItem *item  = [FBSAdvertItem objectWithKeyValues:dict];
        self.position_title = API.position_title;
        self.id = API.id;
        self.sort = API.sort;
        self.position_id = API.position_id;
        self.type_id = API.type_id;
        self.title = API.title;
        self.ad_type_id = API.ad_type_id;
        self.ad_unit_id = API.ad_unit_id;
        self.start_at = API.start_at;
        self.end_at = API.end_at;
        self.created_at = API.created_at;
        self.updated_at = API.updated_at;
        self.file_url = API.file_url;
        self.link = API.link;
    }
}

@end

@interface FBSAdvertCell()
@property (nonatomic, strong) UILabel *contenL;
@property (nonatomic, strong) UIImageView *imgV;
@property (nonatomic, strong) UIImageView *bottomImage;
@property (nonatomic, strong) UIButton *cacelButton;
@end

@implementation FBSAdvertCell


- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self.contentView addSubview:self.contenL];
        [self.contentView addSubview:self.bottomImage];
        [self.contentView addSubview:self.imgV];
        [self.contentView addSubview:self.cacelButton];

        
        [self.contenL mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_offset(WScale(10));
            make.top.mas_equalTo(self.top).offset(WScale(10));
            make.right.mas_equalTo(self.right).offset(-WScale(10));
            make.height.mas_equalTo(WScale(20));
        }];
        [self.imgV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_offset(WScale(10));
            make.right.mas_equalTo(self.right).offset(-WScale(10));
            make.height.mas_equalTo(WScale(200));
            make.top.mas_equalTo(self.contenL.mas_bottom).offset(WScale(10));
        }];
        [self.bottomImage mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_offset(WScale(10));
            make.top.mas_equalTo(self.imgV.mas_bottom).offset(WScale(10));
            make.width.mas_equalTo(WScale(32.5));
            make.height.mas_equalTo(WScale(15));
        }];
        [self.cacelButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_offset(-WScale(10));
            make.top.mas_equalTo(self.imgV.mas_bottom).offset(WScale(10));
            make.width.mas_equalTo(WScale(20));
            make.height.mas_equalTo(WScale(20));
        }];
        
        [self.cacelButton setImage:[UIImage imageNamed:@"common_close"] forState:UIControlStateNormal];
        
    }
    
    return self;
}

- (CGFloat)cellHeight:(UITableView *)tablView item:(FBSAdvertItem *)item{
    

    CGFloat contentLabel = [item.title boundingRectWithSize:CGSizeMake(kScreenWidth  - 2 * WScale(10), MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:kLabelFontSize18} context:NULL].size.height;
   return  contentLabel + WScale(260);
}
- (void)setItem:(FBSAdvertItem *)item {
    if (![item isKindOfClass:[FBSAdvertItem class]]) {
        return ;
    }
    CGFloat contentLabel = [item.title boundingRectWithSize:CGSizeMake(kScreenWidth  - 2 * WScale(10), MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:kLabelFontSize18} context:NULL].size.height;
    [self.contenL mas_updateConstraints:^(MASConstraintMaker *make) {
        make.left.mas_offset(WScale(10));
        make.top.mas_equalTo(self.top).offset(WScale(10));
        make.right.mas_equalTo(self.right).offset(-WScale(10));
        make.height.mas_equalTo(contentLabel);
    }];
    self.contenL.text = item.title;
    [self.imgV sd_setImageWithURL:[NSURL URLWithString:item.file_url]];
    [super setItem:item];
}
#pragma mark - property

- (UILabel *)contenL {
    if (!_contenL) {
        _contenL  = [[UILabel alloc] init];
        _contenL.textColor = Hexcolor(0x333333);
        _contenL.font = kLabelFontSize18;
        _contenL.numberOfLines = 0;
    }
    return _contenL;
}

- (UIImageView *)bottomImage {
    if (!_bottomImage) {
        _bottomImage  = [[UIImageView alloc] init];
        _bottomImage.backgroundColor = [UIColor whiteColor];
        _bottomImage.image = kImageWithName(@"common_adsflag");
    }
    return _bottomImage;
}

- (UIImageView *)imgV {
    if (!_imgV) {
        _imgV  = [[UIImageView alloc] init];
        _imgV.backgroundColor = [UIColor whiteColor];
    }
    return _imgV;
}
- (UIButton *)cacelButton {
    if (!_cacelButton) {
        _cacelButton  = [[UIButton alloc] init];
    }
    return _cacelButton;
}



@end
