//
//  FBSAdvertCell.h
//  Forbes
//
//  Created by 赵志辉 on 2019/9/20.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FBSAdsDataModel.h"

NS_ASSUME_NONNULL_BEGIN


@interface FBSAdvertItem : FBSItem

/*
"position_title": "首页",
"id": 1,
"sort": 0,
"position_id": 3,
"type_id": 1,
"title": "首页频道-1",
"file_id": 67440,
"ad_type_id": 0,
"ad_unit_id": null,
"start_at": 1565677140,
"end_at": 1567145940,
"created_at": 1565669402,
"updated_at": 1568086916,
"file_url": "http://forbes-web.oss-cn-hongkong.aliyuncs.com/user-1/20190816946895.jpg"
 */

@property (nonatomic, copy) NSString *position_title;
@property (nonatomic, copy) NSString *id;
@property (nonatomic, copy) NSString *sort;
@property (nonatomic, copy) NSString *position_id;
@property (nonatomic, copy) NSString *type_id;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *ad_type_id;
@property (nonatomic, copy) NSString *ad_unit_id;
@property (nonatomic, copy) NSString *start_at;
@property (nonatomic, copy) NSString *end_at;
@property (nonatomic, copy) NSString *created_at;
@property (nonatomic, copy) NSString *updated_at;
@property (nonatomic, copy) NSString *file_url;
@property (nonatomic, copy) NSString *link;


@property (nonatomic, strong)id admodView;
@end
/**
 最简单的view
 */
@interface FBSAdvertCell : FBSCell
- (CGFloat)cellHeight:(UITableView *)tablView item:(FBSAdvertItem *)item;
@end

NS_ASSUME_NONNULL_END
