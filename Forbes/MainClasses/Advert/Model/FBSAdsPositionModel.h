
#import "FBSBaseModel.h"

@interface FBSAdsPosition : NSObject
@property (nonatomic , assign) NSInteger              position_id;
@property (nonatomic , copy) NSString              * position_title;

@end


@interface FBSAdsPositionModel :FBSBaseModel

@property (nonatomic , strong) NSArray <FBSAdsPosition *>     * data;

@end
