//
//  FBSAdsDataModel.h
//  Forbes
//
//  Created by 赵志辉 on 2019/9/20.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import "FBSBaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface FBSAdsData :FBSBaseModel
@property (nonatomic , copy) NSString              * position_title;
@property (nonatomic , copy) NSString              * id;
@property (nonatomic , copy) NSString              * sort;
@property (nonatomic , copy) NSString              * position_id;
@property (nonatomic , copy) NSString              * type_id;
@property (nonatomic , copy) NSString              * title;
@property (nonatomic , copy) NSString              * file_id;
@property (nonatomic , copy) NSString              * ad_type_id;
@property (nonatomic , copy) NSString              * ad_unit_id;
@property (nonatomic , copy) NSString              * start_at;
@property (nonatomic , copy) NSString              * end_at;
@property (nonatomic , copy) NSString              * created_at;
@property (nonatomic , copy) NSString              * updated_at;
@property (nonatomic , copy) NSString              * file_url;
@property (nonatomic , copy) NSString              * link;


@end

@interface FBSAdsDataModel : FBSBaseModel

@property (nonatomic , strong) NSArray <FBSAdsData *>              * data;

@end

NS_ASSUME_NONNULL_END
