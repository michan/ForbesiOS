//
//  AdvertAopTableView.m
//  Forbes
//
//  Created by 赵志辉 on 2019/9/18.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import "AdvertAopTableView.h"

@interface AdvertAopTableView () <IMYAOPTableViewDelegate, IMYAOPTableViewDataSource, IMYAOPTableViewGetModelProtocol,GADBannerViewDelegate,GADAdLoaderDelegate,GADInterstitialDelegate>

@end
@implementation AdvertAopTableView

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    ///只是为了不警告  该回调是不会被调用的
    return 0;
}
- (void)setAopUtils:(IMYAOPTableViewUtils *)aopUtils {
    _aopUtils = aopUtils;
    [self injectTableView];
}
- (void)injectTableView {
//    [FBSAdvertItem reuseIdentifier] : [FBSAdvertCell class]

    [self.aopUtils.tableView registerClass:[FBSAdvertCell class] forCellReuseIdentifier:[FBSAdvertItem reuseIdentifier]];
    
    ///广告回调，跟TableView的Delegate，DataSource 一样。
    self.aopUtils.delegate = self;
    self.aopUtils.dataSource = self;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self loadAdver];
    });
}
- (void)loadAdver{
    WEAKSELF
    self.viewModel.soft = self.soft;
    self.viewModel.softArray = self.softArray;
    [self.viewModel rqAdsData:self.position_id complete:^(BOOL success) {
        
        [weakSelf setBanner];
        [weakSelf preloadNextAd];
        [weakSelf insertRows];
        
        
    }];
}
- (void)setBanner{
    // 自定义尺寸
    self.adsToLoad = [[NSMutableArray alloc] init];
    self.loadStateForAds = [NSMutableDictionary dictionary];
    for (FBSAdvertItem *item in self.viewModel.items) {
        
        if ([item.type_id isEqualToString:@"1"]) {
        
            // 原生图片
        }else if ([item.type_id isEqualToString:@"2"]){
            
            if ([item.ad_type_id isEqualToString:@"0"]) {
                GADBannerView *adView = [[GADBannerView alloc]
                                         initWithAdSize:GADAdSizeFromCGSize(
                                                                            CGSizeMake(kScreenSize.width, 100))];
                adView.adUnitID = @"ca-app-pub-3940256099942544/2934735716";
                [self.adsToLoad addObject:adView];

            }else if ([item.ad_type_id isEqualToString:@"1"]){
                DFPInterstitial *interstitial =
                [[DFPInterstitial alloc] initWithAdUnitID:@"/6499/example/interstitial"];
                interstitial.delegate = self;
                [self.adsToLoad addObject:interstitial];
            }else if ([item.ad_type_id isEqualToString:@"2"]){
                DFPInterstitial *interstitial =
                [[DFPInterstitial alloc] initWithAdUnitID:@"/6499/example/interstitial"];
                interstitial.delegate = self;
                [self.adsToLoad addObject:interstitial];

            }else if ([item.ad_type_id isEqualToString:@"3"]){
                
                GADRewardedAd *rewardedAd = [[GADRewardedAd alloc]
                                             initWithAdUnitID:@"/6499/example/rewarded-video"];
                [self.adsToLoad addObject:rewardedAd];
            }else if ([item.ad_type_id isEqualToString:@"4"]){
                GADAdLoader *adLoader = [[GADAdLoader alloc] initWithAdUnitID:@"ca-app-pub-3940256099942544/3986624511"
                                                           rootViewController:self.vc
                                                                      adTypes:@[ kGADAdLoaderAdTypeUnifiedNative ]
                                                                      options:@[]];
                adLoader.delegate = self;
                [self.adsToLoad addObject:adLoader];
            }else if ([item.ad_type_id isEqualToString:@"5"]){
                GADVideoOptions *videoOptions = [[GADVideoOptions alloc] init];
                videoOptions.startMuted = YES;
                GADAdLoader *adLoader = [[GADAdLoader alloc] initWithAdUnitID:@"ca-app-pub-3940256099942544/3986624511"
                                                           rootViewController:self.vc
                                                                      adTypes:@[ kGADAdLoaderAdTypeUnifiedNative ]
                                                                      options:@[ videoOptions ]];
                adLoader.delegate = self;
                [self.adsToLoad addObject:adLoader];
            }
        }
    }
}
- (void)preloadNextAd {
    if (!self.adsToLoad.count) {
        [self.aopUtils.tableView reloadData];
        return;
    }
    if ([self.adsToLoad.firstObject isKindOfClass:[GADBannerView class]]) {
        GADBannerView *adView = self.adsToLoad.firstObject;
        [self.adsToLoad removeObjectAtIndex:0];
        GADRequest *request = [GADRequest request];
        request.testDevices = @[ kGADSimulatorID ];
        [adView loadRequest:request];
    }else if ([self.adsToLoad.firstObject isKindOfClass:[GADRewardedAd class]]){
        GADRewardedAd *rewardedAd = self.adsToLoad.firstObject;
        DFPRequest *request = [DFPRequest request];
        [rewardedAd loadRequest:request completionHandler:^(GADRequestError * _Nullable error) {
            if (error) {
                [self preloadNextAd];
            } else {
                [self preloadNextAd];
            }
        }];
    }else if ([self.adsToLoad.firstObject isKindOfClass:[GADAdLoader class]]){
        GADAdLoader *adLoader = self.adsToLoad.firstObject;
        [adLoader loadRequest:[GADRequest request]];
    }else if ([self.adsToLoad.firstObject isKindOfClass:[DFPInterstitial class]]){
        DFPInterstitial *Interstitial = self.adsToLoad.firstObject;
        [Interstitial loadRequest:[GADRequest request]];
    }else if ([self.adsToLoad.firstObject isKindOfClass:[GADBannerView class]]){
        
    }
//    GADBannerView *adView = self.adsToLoad.firstObject;
//    [self.adsToLoad removeObjectAtIndex:0];
//    GADRequest *request = [GADRequest request];
//    request.testDevices = @[ kGADSimulatorID ];
//    [adView loadRequest:request];
}
- (NSString *)referenceKeyForAdView:(GADBannerView *)adView {
    return [[NSString alloc] initWithFormat:@"%p", adView];
}
#pragma markDFPInterstitial
- (void)interstitialDidDismissScreen:(DFPInterstitial *)interstitial {
    
}
//- (void)doSomething:(id)sender {
//    if (self.interstitial.isReady) {
//        [self.interstitial presentFromRootViewController:self];
//    } else {
//        NSLog(@"Ad wasn't ready");
//    }
//}
/// Tells the delegate an ad request succeeded.
- (void)interstitialDidReceiveAd:(DFPInterstitial *)ad {
    NSLog(@"interstitialDidReceiveAd");
    [self preloadNextAd];
}

/// Tells the delegate an ad request failed.
- (void)interstitial:(DFPInterstitial *)ad
didFailToReceiveAdWithError:(GADRequestError *)error {
    NSLog(@"interstitial:didFailToReceiveAdWithError: %@", [error localizedDescription]);
    [self preloadNextAd];
}

/// Tells the delegate that an interstitial will be presented.
- (void)interstitialWillPresentScreen:(DFPInterstitial *)ad {
    NSLog(@"interstitialWillPresentScreen");
}

/// Tells the delegate the interstitial is to be animated off the screen.
- (void)interstitialWillDismissScreen:(DFPInterstitial *)ad {
    NSLog(@"interstitialWillDismissScreen");
}

/// Tells the delegate the interstitial had been animated off the screen.


/// Tells the delegate that a user click will open another app
/// (such as the App Store), backgrounding the current app.
- (void)interstitialWillLeaveApplication:(DFPInterstitial *)ad {
    NSLog(@"interstitialWillLeaveApplication");
}

#pragma mark GADRewardedAd delegate
/// Tells the delegate that the user earned a reward.
- (void)rewardedAd:(GADRewardedAd *)rewardedAd userDidEarnReward:(GADAdReward *)reward {
    // TODO: Reward the user.
    NSLog(@"rewardedAd:userDidEarnReward:");
}

/// Tells the delegate that the rewarded ad was presented.
- (void)rewardedAdDidPresent:(GADRewardedAd *)rewardedAd {
    NSLog(@"rewardedAdDidPresent:");
}

/// Tells the delegate that the rewarded ad failed to present.
- (void)rewardedAd:(GADRewardedAd *)rewardedAd didFailToPresentWithError:(NSError *)error {
    NSLog(@"rewardedAd:didFailToPresentWithError");
}

/// Tells the delegate that the rewarded ad was dismissed.
- (void)rewardedAdDidDismiss:(GADRewardedAd *)rewardedAd {
    NSLog(@"rewardedAdDidDismiss:");
}

#pragma mark GADAdLoader
- (void)adLoader:(nonnull GADAdLoader *)adLoader
didFailToReceiveAdWithError:(nonnull GADRequestError *)error{
    [self preloadNextAd];
    
}
/// Called after adLoader has finished loading.
- (void)adLoaderDidFinishLoading:(nonnull GADAdLoader *)adLoader{
    [self preloadNextAd];
}
#pragma mark event delegate

// MARK: - GADBannerView delegate methods

- (void)adViewDidReceiveAd:(GADBannerView *)bannerView {
    // Mark banner ad as succesfully loaded.
    // Load the next ad in the adsToLoad list.
    self.loadStateForAds[[self referenceKeyForAdView:bannerView]] = @YES;
    [self preloadNextAd];
    
}

- (void)adView:(GADBannerView *)bannerView didFailToReceiveAdWithError:(GADRequestError *)error {
    NSLog(@"Failed to receive ad: %@", error.localizedDescription);
    // Load the next ad in the adsToLoad list.
    [self preloadNextAd];
}
- (void)adView:(DFPBannerView *)banner
didReceiveAppEvent:(NSString *)name
      withInfo:(NSString *)info {
}
#pragma mark size delegate
- (void)adView:(DFPBannerView *)view willChangeAdSizeTo:(GADAdSize)size;
{
    
}

/// Tells the delegate that a full-screen view will be presented in response
/// to the user clicking on an ad.
- (void)adViewWillPresentScreen:(DFPBannerView *)adView {
    NSLog(@"adViewWillPresentScreen");
}

/// Tells the delegate that the full-screen view will be dismissed.
- (void)adViewWillDismissScreen:(DFPBannerView *)adView {
    NSLog(@"adViewWillDismissScreen");
}

/// Tells the delegate that the full-screen view has been dismissed.
- (void)adViewDidDismissScreen:(DFPBannerView *)adView {
    NSLog(@"adViewDidDismissScreen");
}

/// Tells the delegate that a user click will open another app (such as
/// the App Store), backgrounding the current app.
- (void)adViewWillLeaveApplication:(DFPBannerView *)adView {
    NSLog(@"adViewWillLeaveApplication");
}

///简单的rows插入
- (void)insertRows {
//    NSMutableArray<IMYAOPTableViewInsertBody *> *insertBodys = [NSMutableArray array];
//    ///随机生成了5个要插入的位置
//    for (int i = 0; i < self.viewModel.items.count; i++) {
//        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:arc4random() % 20 inSection:0];
//       [insertBodys addObject:[IMYAOPTableViewInsertBody insertBodyWithIndexPath:indexPath]];
//    }
    ///清空 旧数据
    [self.aopUtils insertWithSections:nil];
    [self.aopUtils insertWithIndexPaths:nil];
    
    ///插入 新数据, 同一个 row 会按数组的顺序 row 进行 递增
    [self.aopUtils insertWithIndexPaths:self.viewModel.insertBodys];
    
    ///调用tableView的reloadData，进行页面刷新
    [self.aopUtils.tableView reloadData];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        NSLog(@"%@", self.aopUtils.allModels);
    });
}

/**
 *      插入sections demo
 *      单纯插入section 是没法显示的，要跟 row 配合。
 */
- (void)insertSections {
    NSMutableArray<IMYAOPTableViewInsertBody *> *insertBodys = [NSMutableArray array];
    for (int i = 1; i < 6; i++) {
        NSInteger section = arc4random() % i;
        IMYAOPTableViewInsertBody *body = [IMYAOPTableViewInsertBody insertBodyWithSection:section];
        [insertBodys addObject:body];
    }
    [self.aopUtils insertWithSections:insertBodys];
    
    [insertBodys enumerateObjectsUsingBlock:^(IMYAOPTableViewInsertBody *_Nonnull obj, NSUInteger idx, BOOL *_Nonnull stop) {
        obj.indexPath = [NSIndexPath indexPathForRow:0 inSection:obj.resultSection];
    }];
    [self.aopUtils insertWithIndexPaths:insertBodys];
    
    [self.aopUtils.tableView reloadData];
}

#pragma mark -AOP Delegate
- (void)aopTableUtils:(IMYAOPTableViewUtils *)tableUtils numberOfSection:(NSInteger)sectionNumber {
    ///可以获取真实的 sectionNumber 可以在这边进行一些AOP的数据初始化
}

- (void)aopTableUtils:(IMYAOPTableViewUtils *)tableUtils willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    ///真实的 will display 回调. 有些时候统计需要
}

- (void)aopTableUtils:(IMYAOPTableViewUtils *)tableUtils didEndDisplayingCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    ///真实的 did end display 回调. 有些时候统计需要
}

- (id)tableView:(UITableView *)tableView modelForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [NSString stringWithFormat:@"ad: %ld, %d", (long)indexPath.section, indexPath.row];
}

#pragma mark - UITableView 回调
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    NSAssert(self.viewModel != nil && [self.viewModel isKindOfClass:[FBSTableViewModel class]], @"FBSTableViewController  viewModel属性不能为空，且必须为FBSTableViewModel子类！");
    
    return self.viewModel.numberOfSections;
}




- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSAssert(self.viewModel != nil && [self.viewModel isKindOfClass:[FBSTableViewModel class]], @"FBSTableViewController  viewModel属性不能为空，且必须为FBSTableViewModel子类！");
    
    
    FBSItem *item  = [self.viewModel itemAtRow:indexPath.row inSection:indexPath.section];
    FBSCell *cell;
    if ([[item class] reuseIdentifier].length) {
        cell = [tableView dequeueReusableCellWithIdentifier:[[item class] reuseIdentifier] forIndexPath:indexPath];
        cell.item = item;
        
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
     FBSItem *item  = [self.viewModel itemAtRow:indexPath.row inSection:indexPath.section];
    if ([item isKindOfClass:[FBSAdvertItem class]]) {
        FBSAdvertItem *AdvertItem = (FBSAdvertItem *)item;
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:AdvertItem.link]];
    }

}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSAssert(self.viewModel != nil && [self.viewModel isKindOfClass:[FBSTableViewModel class]], @"FBSTableViewController  viewModel属性不能为空，且必须为FBSTableViewModel子类！");
    
    FBSItem *item  = [self.viewModel itemAtRow:indexPath.row inSection:indexPath.section];
    if ([item isKindOfClass:[FBSAdvertItem class]]) {
        FBSAdvertItem *advertItem = (FBSAdvertItem *)item;
        CGFloat contentLabel = [advertItem.title boundingRectWithSize:CGSizeMake(kScreenWidth  - 2 * WScale(10), MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:kLabelFontSize18} context:NULL].size.height;
        return  contentLabel + WScale(260);
    }else{
        return WScale(280);
    }
}

- (FBSAdvertModel *)viewModel{
    if (!_viewModel) {
        _viewModel = [[FBSAdvertModel alloc] init];
    }
    return _viewModel;
}
@end
