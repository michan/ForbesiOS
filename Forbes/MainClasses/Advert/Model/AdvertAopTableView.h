//
//  AdvertAopTableView.h
//  Forbes
//
//  Created by 赵志辉 on 2019/9/18.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FBSAdvertCell.h"
#import "FBSAdvertModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface AdvertAopTableView : NSObject
@property (weak, nonatomic) IMYAOPTableViewUtils *aopUtils;

@property (nonatomic, strong) FBSAdvertModel *viewModel;

@property (nonatomic, weak) UIViewController *vc;
@property (nonatomic, strong) NSMutableArray *adsToLoad;

@property (nonatomic, strong) NSMutableDictionary<NSString *, NSNumber *> *loadStateForAds;


@property (nonatomic, strong) NSString *position_id;

@property (nonatomic, strong) NSString *soft;

@property (nonatomic, strong) NSArray *softArray;




@end

NS_ASSUME_NONNULL_END
