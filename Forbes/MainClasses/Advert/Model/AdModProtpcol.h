//
//  AdModProtpcol.h
//  Forbes
//
//  Created by 赵志辉 on 2019/9/20.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FBSAdvertCell.h"

NS_ASSUME_NONNULL_BEGIN

@protocol AdModProtpcol <NSObject>

- (id)createAdmod:(FBSAdvertItem *)item;

@end

NS_ASSUME_NONNULL_END
