//
//  FBSAdvertModel.m
//  Forbes
//
//  Created by 赵志辉 on 2019/9/18.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import "FBSAdvertModel.h"

#import "FBSAdvertListApi.h"

#import "FBSAdsPositionModel.h"

#import "FBSAdsDataAPI.h"

#import "FBSAdsDataModel.h"

#import "FBSAdmodModel.h"

#import "FBSCustomModel.h"


@implementation FBSAdvertitemAndIndexP

@end



@interface FBSAdvertModel()

@property (nonatomic, strong) FBSAdvertListApi *AdvertListApi;// id请求

@property (nonatomic, strong) FBSAdsDataAPI *adsDataAPI;// 具体id广告

@property (nonatomic, strong) FBSAdsPositionModel *adsPositionModel;

@property (nonatomic, strong) NSDictionary *itemTypeDic;
@property (nonatomic, copy) void(^complete)(BOOL);

@property (nonatomic ,strong) NSMutableDictionary *dataDic;

@property (nonatomic , strong)NSMutableArray<IMYAOPTableViewInsertBody *> *insertBodys;

@property (nonatomic , strong)NSMutableDictionary *dic;

@end
@implementation FBSAdvertModel

- (void)rqAdsData:(NSString *)position_id complete:(void(^)(BOOL success))complete {
    WEAKSELF
    self.adsDataAPI.position_id = position_id;
    if ([[NSUserDefaults standardUserDefaults] objectForKey:AdvertIdList]) {
        // 保存数据;
        [self rqlistWithPosition_id:^(BOOL success) {
            if (success) {
                complete(YES);
            }
        }];
        return;
    }
    [self.AdvertListApi startWithSuccess:^(YBNetworkResponse * _Nonnull response) {
        FBSLog(@"广告id : %@",response.responseObject);
        weakSelf.adsPositionModel = [FBSAdsPositionModel mj_objectWithKeyValues:response.responseObject];
        [weakSelf createItems];
        // 保存数据;
        [self rqlistWithPosition_id:^(BOOL success) {
            if (success) {
                complete(YES);
            }
        }];
    } failure:^(YBNetworkResponse * _Nonnull response) {
        complete(NO);
    }];
}
// 
- (void)rqlistWithPosition_id:(void(^)(BOOL success))complete {
    WEAKSELF
    [self.adsDataAPI startWithSuccess:^(YBNetworkResponse * _Nonnull response) {
        FBSLog(@"根据id获取的列表 : %@",response.responseObject);
        FBSAdsDataModel *adsDataModel = [FBSAdsDataModel mj_objectWithKeyValues:response.responseObject];
        [weakSelf createAdsItems:adsDataModel];
        complete(YES);
        
    } failure:^(YBNetworkResponse * _Nonnull response) {
        complete(NO);
    }];
}
- (void)createAdsItems:(FBSAdsDataModel *)model{
    //刷新时候移除原来的广告
    [self.items removeAllObjects];
    
//    for (FBSAdsData *ApiModel in model.data) {
//        NSLog(@"%@",ApiModel);
//        FBSAdvertItem *item = [[FBSAdvertItem alloc] init];
//        [item ctreatItemData:ApiModel];
//        [self.items addObject:item];
//    }
    #warning 广告展示设置
    if (self.softArray.count) {
        if ([[self.softArray firstObject] isKindOfClass:[NSIndexPath class]]) {
            [self creAdsIndexPth:(FBSAdsDataModel *)model];
            return;
        }
        for (int i = 0; i < model.data.count && i < self.softArray.count; i++) {
            FBSAdsData *adsData = model.data[i];
            FBSAdvertItem *item = [[FBSAdvertItem alloc] init];
            adsData.sort = self.softArray[i];
            [item ctreatItemData:adsData];
            [self.items addObject:item];
        }
    }else{
        for (FBSAdsData *adsData in model.data) {
            NSLog(@"adsData :%@",adsData);
            
            //暂时只显示第一个广告
            if ([self.adsDataAPI.position_id isEqualToString:@"3"]) {
                //要闻位置固定为第4个cell
                adsData.sort = @"3";
            } else {
                //位置固定为第5个cell
                adsData.sort = @"3";
            }
            if (self.items.count == 1) {
                break ;
            }
            if (self.soft.length) {
                adsData.sort = self.soft;
            }
            
            FBSAdvertItem *item = [[FBSAdvertItem alloc] init];
            [item ctreatItemData:adsData];
            [self.items addObject:item];
        }
    }
    //根据位置创建广告
    [self instert];
}
- (void)creAdsIndexPth:(FBSAdsDataModel *)model{
    NSMutableDictionary *oriDic = [NSMutableDictionary dictionary];
    NSMutableArray<IMYAOPTableViewInsertBody *> *insertBodys = [NSMutableArray array];
    for (int i = 0; i < self.softArray.count && model.data.count; i++) {

        IMYAOPTableViewInsertBody *body = [IMYAOPTableViewInsertBody insertBodyWithIndexPath:_softArray[i]];
        
        [insertBodys addObject:body];
        FBSAdsData *adsData = model.data[i];
        FBSAdvertItem *item = [[FBSAdvertItem alloc] init];
        [item ctreatItemData:adsData];
        [self.items addObject:item];
        [oriDic setObject:item forKey:self.softArray[i]];
    }
    self.dic = oriDic;
    self.insertBodys = insertBodys;
}


// 储存数据
- (void)createItems {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:self.adsPositionModel.mj_keyValues forKey:AdvertIdList];
    [defaults synchronize];
}
- (void)instert{

    NSMutableArray<IMYAOPTableViewInsertBody *> *insertBodys = [NSMutableArray array];
    ///随机生成了5个要插入的位置
    self.dic = [NSMutableDictionary dictionary];
    NSMutableDictionary *oriDic = [NSMutableDictionary dictionary];
    
    NSMutableArray *indexArray = [NSMutableArray array];
    self.insertBodys = insertBodys;
    // sort 需要顺序
    [self.items sortUsingComparator:^NSComparisonResult(FBSAdvertItem * _Nonnull obj1, FBSAdvertItem  * _Nonnull obj2) {
        if ([obj1.sort integerValue] > [obj2.sort integerValue])
        {
            return NSOrderedDescending;
        }
        else
        {
            return NSOrderedAscending;
        }
    }];
    for (int i = 0; i < self.items.count; i++) {
        FBSAdvertItem *item = self.items[i];
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[item.sort intValue] inSection:0];
        [insertBodys addObject:[IMYAOPTableViewInsertBody insertBodyWithIndexPath:indexPath]];
        // 如果有重复位置的进行+1
        for (NSIndexPath *indexP in indexArray) {
            if (indexPath == indexP) {
               indexPath = [NSIndexPath indexPathForRow:indexPath.row + 1 inSection:indexPath.section];
            }
        }
        [indexArray addObject:indexPath];
        [oriDic setObject:item forKey:indexPath];
    }
    //
    /*
    NSMutableDictionary *insertMap = [NSMutableDictionary dictionary];
    NSMutableDictionary *indexPItem = [NSMutableDictionary dictionary];
    [insertBodys enumerateObjectsUsingBlock:^(IMYAOPBaseInsertBody *_Nonnull obj, NSUInteger idx, BOOL *_Nonnull stop) {
        FBSItem *item =   [oriDic objectForKey:obj.indexPath];
        NSInteger section = obj.indexPath.section;
        NSInteger row = obj.indexPath.row;
        // 根据 section 查找 对应的 row 和 其余数据
        NSMutableArray *rowArray = insertMap[@(section)];
        // 如果没有 则穿件一个进行储存
        if (!rowArray) {
            rowArray = [NSMutableArray array];
            [insertMap setObject:rowArray forKey:@(section)];
        }
        while (YES) {
            BOOL hasEqual = NO;
            // 如果有一个section对应多个row 所以需要遍历  rowArray储存的是对应要插入g广告的 indexPath
            for (FBSAdvertitemAndIndexP *temAndIndexP in rowArray) {
                if (temAndIndexP.indexP.row == row) {
                    row++;
                    hasEqual = YES;
                    break;
                }
            }
            if (hasEqual == NO) {
                break;
            }
        }
        // 组装新的数据插入
        NSIndexPath *insertPath = [NSIndexPath indexPathForRow:row inSection:section];
        FBSAdvertitemAndIndexP *temAndIndexP = [[FBSAdvertitemAndIndexP alloc] init];
        temAndIndexP.indexP = insertPath;
        temAndIndexP.item = item;
        [rowArray addObject:temAndIndexP];
        [indexPItem setObject:item forKey:insertPath];
    }];
     */
    self.dic = oriDic;
}
- (FBSItem *)itemAtRow:(NSInteger)row inSection:(NSInteger)section {
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:section];
    return self.dic[indexPath];
}


#pragma mark - property

- (FBSAdvertListApi *)AdvertListApi {
    if (!_AdvertListApi) {
        _AdvertListApi  = [[FBSAdvertListApi alloc] init];
    }
    return _AdvertListApi;
}

- (FBSAdsDataAPI *)adsDataAPI {
    if (!_adsDataAPI) {
        _adsDataAPI = [[FBSAdsDataAPI alloc] init];
        _adsDataAPI.position_id = @"";
    }
    return _adsDataAPI;
}
- (NSDictionary *)itemTypeDic
{
    if (!_itemTypeDic) {
        _itemTypeDic = @{
                         @"1": [[FBSCustomModel alloc] init],
                         @"2": [[FBSCustomModel alloc] init],
                         };

    }
    return _itemTypeDic;
}
- (NSMutableDictionary *)dataDic
{
    if (!_dataDic) {
        _dataDic = [NSMutableDictionary dictionary];
    }
    return _dataDic;
}

@end
