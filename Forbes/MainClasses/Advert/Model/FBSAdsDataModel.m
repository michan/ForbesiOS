//
//  FBSAdsDataModel.m
//  Forbes
//
//  Created by 赵志辉 on 2019/9/20.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import "FBSAdsDataModel.h"

@implementation FBSAdsData

@end

@implementation FBSAdsDataModel


+ (NSDictionary *)mj_objectClassInArray {
    
    return @{@"data" : @"FBSAdsData"};
}

@end
