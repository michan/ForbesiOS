//
//  FBSAdmodModel.m
//  Forbes
//
//  Created by 赵志辉 on 2019/9/20.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import "FBSAdmodModel.h"

@implementation FBSAdmodModel

- (id)createAdmod:(FBSAdvertItem *)item
{
    if ([item.ad_type_id isEqualToString:@"0"]) {
        GADBannerView *adView = [[GADBannerView alloc]
                                 initWithAdSize:GADAdSizeFromCGSize(
                                                                    CGSizeMake(kScreenSize.width, 100))];
        adView.adUnitID = @"ca-app-pub-3940256099942544/2934735716";
        return adView;
    }else if ([item.ad_type_id isEqualToString:@"1"]){
        GADBannerView *adView = [[GADBannerView alloc]
                                 initWithAdSize:GADAdSizeFromCGSize(
                                                                    CGSizeMake(kScreenSize.width, 100))];
        adView.adUnitID = @"ca-app-pub-3940256099942544/2934735716";
        return adView;
    }else if ([item.ad_type_id isEqualToString:@"2"]){
        GADBannerView *adView = [[GADBannerView alloc]
                                 initWithAdSize:GADAdSizeFromCGSize(
                                                                    CGSizeMake(kScreenSize.width, 100))];
        adView.adUnitID = @"ca-app-pub-3940256099942544/2934735716";
        return adView;
    }else if ([item.ad_type_id isEqualToString:@"3"]){
       
        GADRewardedAd *rewardedAd = [[GADRewardedAd alloc]
                           initWithAdUnitID:@"/6499/example/rewarded-video"];

//        DFPRequest *request = [DFPRequest request];
//        [rewardedAd loadRequest:request completionHandler:^(GADRequestError * _Nullable error) {
//            if (error) {
//                // Handle ad failed to load case.
//            } else {
//                // Ad successfully loaded.
//            }
//        }];
        return rewardedAd;
    }else if ([item.ad_type_id isEqualToString:@"4"]){
        GADVideoOptions *videoOptions = [[GADVideoOptions alloc] init];
//        videoOptions.startMuted = self.startMutedSwitch.on;
        
       GADAdLoader *adLoader = [[GADAdLoader alloc] initWithAdUnitID:@"ca-app-pub-3940256099942544/3986624511"
                                           rootViewController:self
                                                      adTypes:@[ kGADAdLoaderAdTypeUnifiedNative ]
                                                      options:@[ videoOptions ]];
        return adLoader;
    }else if ([item.ad_type_id isEqualToString:@"1"]){
        GADBannerView *adView = [[GADBannerView alloc]
                                 initWithAdSize:GADAdSizeFromCGSize(
                                                                    CGSizeMake(kScreenSize.width, 100))];
        adView.adUnitID = @"ca-app-pub-3940256099942544/2934735716";
        return adView;
    }
    return [UIView new];
}

@end
