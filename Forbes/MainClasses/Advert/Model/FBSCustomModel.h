//
//  FBSCustomModel.h
//  Forbes
//
//  Created by 赵志辉 on 2019/9/20.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import "FBSBaseModel.h"
#import "AdModProtpcol.h"

NS_ASSUME_NONNULL_BEGIN

@interface FBSCustomModel : FBSBaseModel<AdModProtpcol,GADBannerViewDelegate>

@end

NS_ASSUME_NONNULL_END
