//
//  FBSAdvertItem.h
//  Forbes
//
//  Created by 赵志辉 on 2019/9/18.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import "FBSTableViewModel.h"
#import "FBSAdvertCell.h"


NS_ASSUME_NONNULL_BEGIN

static NSString * const AdvertIdList = @"AdvertIdList";

@interface FBSAdvertitemAndIndexP : NSObject

@property(nonatomic ,strong)NSIndexPath *indexP;

@property(nonatomic ,strong)FBSItem *item;// 存储对应位置的对应item

@end

@interface FBSAdvertModel : FBSTableViewModel

@property (nonatomic , strong, readonly)NSMutableArray<IMYAOPTableViewInsertBody *> *insertBodys;

// appdelegateq请求
@property (nonatomic, strong)NSString *soft;

// appdelegateq请求
@property (nonatomic, strong)NSArray *softArray;
- (void)rqAdsData:(NSString *)position_id complete:(void(^)(BOOL success))complete;

- (void)rqlistWithPosition_id:(void(^)(BOOL success))complete;


@end

NS_ASSUME_NONNULL_END
