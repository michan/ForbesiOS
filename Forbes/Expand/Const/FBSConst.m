//
//  FBSConst.m
//  Forbes
//
//  Created by 周灿华 on 2019/9/28.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSConst.h"


NSString *FBSLoginSuccessNotication = @"FBSLoginSuccessNotication";

NSString *FBSLogoutSuccessNotication = @"FBSLogoutSuccessNotication";

NSString *FBSEditUserInfoNotication = @"FBSEditUserInfoNotication";
