//
//  FBSConst.h
//  Forbes
//
//  Created by 周灿华 on 2019/9/28.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import <Foundation/Foundation.h>

///登录成功
CC_EXTERN NSString *FBSLoginSuccessNotication;

///退出登录成功
CC_EXTERN NSString *FBSLogoutSuccessNotication;

///修改昵称
CC_EXTERN NSString *FBSEditUserInfoNotication;

