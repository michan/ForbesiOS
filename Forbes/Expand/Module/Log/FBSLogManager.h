//
//  FBSLogManager.h
//  Forbes
//
//  Created by 周灿华 on 2019/7/20.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import <Foundation/Foundation.h>

/** 打印日志级别 */
#define LOG_LEVEL_DEF ddLogLevel
#import <CocoaLumberjack/CocoaLumberjack.h>

#ifdef DEBUG
static const DDLogLevel ddLogLevel = DDLogLevelVerbose;
#else
static const DDLogLevel ddLogLevel = DDLogLevelWarning;
#endif  /* DEBUG */


//日志宏定义
#define FBSLogError   DDLogError
#define FBSLogWarn    DDLogWarn
#define FBSLogInfo    DDLogInfo
#define FBSLogDebug   DDLogDebug
#define FBSLogVerbose DDLogVerbose
#define FBSLog        DDLogDebug


////日志管理者,基于 CocoaLumberjack
@interface FBSLogManager : NSObject


/**
 日志系统初始化
 
 @param tty  输出到xcode控制台
 @param asl  输出到console
 @param file 输出到文件,开发阶段设置为yes比较合适
 */
+ (void)setup:(BOOL)tty asl:(BOOL)asl file:(BOOL)file;


+ (NSArray *)getAllLogFilePath;             // 获取日志路径

+ (NSArray *)getAllLogFileContent;          // 获取日志内容

@end

