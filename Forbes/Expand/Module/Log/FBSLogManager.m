//
//  FBSLogManager.m
//  Forbes
//
//  Created by 周灿华 on 2019/7/20.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSLogManager.h"
#import "FBSLogCustomFormatter.h"

@implementation FBSLogManager

+ (void)setup:(BOOL)tty asl:(BOOL)asl file:(BOOL)file {
    
    if (tty) {
        //        [[DDTTYLogger sharedInstance] setColorsEnabled:YES];           // 启用颜色区分
        //        [[DDTTYLogger sharedInstance] setForegroundColor:DDMakeColor(255, 0, 0) backgroundColor:nil forFlag:DDLogFlagError];
        //        [[DDTTYLogger sharedInstance] setForegroundColor:DDMakeColor(105, 200, 80) backgroundColor:nil forFlag:DDLogFlagInfo];
        //        [[DDTTYLogger sharedInstance] setForegroundColor:DDMakeColor(100, 100, 200) backgroundColor:nil forFlag:DDLogFlagDebug];
        
        
        
        [DDTTYLogger sharedInstance].logFormatter = [[FBSLogCustomFormatter alloc] init]; // 自定义日志
        [DDLog addLogger:[DDTTYLogger sharedInstance]]; // TTY = Xcode console
        
        //        iOS 10 以后使用系统新的打印方式
        //    [DDLog addLogger:[DDOSLogger sharedInstance]];
    }
    
    if (asl) {
        [DDLog addLogger:[DDASLLogger sharedInstance]]; // ASL = Apple System Logs
    }
    
    
    if (file) {
        
        //放在沙盒directory目下,是一个共享目录,可以导出日志文件
        NSString *baseDir = kDocumentPath;
        NSString *logsDirectory = [baseDir stringByAppendingPathComponent:@"Logs"];
        
        DDLogFileManagerDefault *defaultLogFileManager = [[DDLogFileManagerDefault alloc] initWithLogsDirectory:logsDirectory];
        
        DDFileLogger *fileLogger = [[DDFileLogger alloc] initWithLogFileManager:defaultLogFileManager];// File Logger
        fileLogger.rollingFrequency = 60 * 60 * 24;// 刷新频率为24小时
        fileLogger.logFileManager.maximumNumberOfLogFiles = 7; // 最大文件数量为7个
        fileLogger.maximumFileSize = 1024 * 1024 * 2;      //最大日志文件大小
        
        [DDLog addLogger:fileLogger];
    }
    
}



/**
 获取日志路径(文件名bundleid+空格+日期)
 */
+ (NSArray *)getAllLogFilePath{
    NSString *baseDir = kDocumentPath;
    NSString *logPath = [baseDir stringByAppendingPathComponent:@"Logs"];
    NSFileManager *fileManger = [NSFileManager defaultManager];
    NSError *error = nil;
    NSArray *fileArray = [fileManger contentsOfDirectoryAtPath:logPath error:&error];
    NSMutableArray *result = [NSMutableArray array];
    [fileArray enumerateObjectsUsingBlock:^(NSString *filePath, NSUInteger idx, BOOL * _Nonnull stop) {
        if([filePath hasPrefix:[NSBundle mainBundle].bundleIdentifier]){
            NSString *logFilePath = [logPath stringByAppendingPathComponent:filePath];
            [result addObject:logFilePath];
        }
    }];
    return result;
}
/**
 获取日志内容
 */
+ (NSArray *)getAllLogFileContent{
    NSMutableArray *result = [NSMutableArray array];
    NSArray *logfilePaths = [self getAllLogFilePath];
    [logfilePaths enumerateObjectsUsingBlock:^(NSString *filePath, NSUInteger idx, BOOL * _Nonnull stop) {
        NSData *data = [NSData dataWithContentsOfFile:filePath];
        NSString *content = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        [result addObject:content];
    }];
    return result;
}


@end
