//
//  FBSUserModel.h
//  Forbes
//
//  Created by 周灿华 on 2019/9/8.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FBSUserModel : NSObject
@property (nonatomic, copy) NSString *useId;
@property (nonatomic, copy) NSString *account;
@property (nonatomic, copy) NSString *email;
@property (nonatomic, copy) NSString *tel;
@property (nonatomic, copy) NSString *nickname;
@property (nonatomic, copy) NSString *file_id;
@property (nonatomic, copy) NSString *remarks;
@property (nonatomic, copy) NSString *password;
@property (nonatomic, copy) NSString *descripe;
@property (nonatomic, copy) NSString *remember_token;
@property (nonatomic, copy) NSString *dep_id;
@property (nonatomic, copy) NSString *role_id;
@property (nonatomic, assign) NSInteger created_at;
@property (nonatomic, assign) NSInteger updated_at;
@property (nonatomic, copy) NSString *token;

@end

