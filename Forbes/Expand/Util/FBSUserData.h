//
//  FBSUserData.h
//  Forbes
//
//  Created by 周灿华 on 2019/9/8.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FBSUserData : NSObject
@property (nonatomic, copy, readonly) NSString *useId;
@property (nonatomic, copy, readonly) NSString *account;
@property (nonatomic, copy, readonly) NSString *email;
@property (nonatomic, copy, readonly) NSString *tel;
@property (nonatomic, copy, readonly) NSString *nickname;
@property (nonatomic, copy, readonly) NSString *file_id;
@property (nonatomic, copy, readonly) NSString *remarks;
@property (nonatomic, copy, readonly) NSString *password;
@property (nonatomic, copy, readonly) NSString *descripe;
@property (nonatomic, copy, readonly) NSString *remember_token;
@property (nonatomic, copy, readonly) NSString *dep_id;
@property (nonatomic, copy, readonly) NSString *role_id;
@property (nonatomic, assign, readonly) NSInteger created_at;
@property (nonatomic, assign, readonly) NSInteger updated_at;
@property (nonatomic, copy, readonly) NSString *token;
@property (nonatomic, assign, readonly) BOOL isLogin;
@property(nonatomic,copy)NSString * alertID;

+ (instancetype)sharedData;

///保存用户数据
- (void)saveUserData:(NSDictionary *)dict;

///删除用户数据
- (void)deleteUserData;

///获取并保存最新用户数据
- (void)fetchNewestUserData;

///获取用户数据字典
- (NSDictionary *)userDataDic;


@end

