//
//  FBSUserData.m
//  Forbes
//
//  Created by 周灿华 on 2019/9/8.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSUserData.h"
#import "FBSUserModel.h"
#import "FBSMineInfoAPI.h"

#define UserDataFile [NSString stringWithFormat:@"%@/userData.plist",kDocumentPath]

@interface FBSUserData ()
@property (nonatomic, strong) FBSUserModel *userModel;
@property (nonatomic, strong) FBSMineInfoAPI *useInfoAPI;
@property (nonatomic, copy) NSString *token;
@property (nonatomic, copy) NSString *remember_token;
@property (nonatomic, copy) NSString *password;
@end

@implementation FBSUserData

static FBSUserData *instance_;

+ (instancetype)sharedData {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance_ = [[FBSUserData alloc] init];
    });
    return instance_;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (void)commonInit {
    NSDictionary *dataDic = [NSDictionary dictionaryWithContentsOfFile:UserDataFile];
    self.userModel = [FBSUserModel mj_objectWithKeyValues:dataDic];
}


- (void)saveUserData:(NSDictionary *)dict {
    NSDictionary *dataDic = dict[@"data"];
    
    if (dataDic && [dataDic isKindOfClass:[NSDictionary class]]) {
        // 把value 为NULL的值转化为"",不然保存到文件会失败
        
        NSMutableDictionary *tempDic = [dataDic mutableCopy];
        [dataDic enumerateKeysAndObjectsUsingBlock:^(NSString * _Nonnull key, NSString *   _Nonnull obj, BOOL * _Nonnull stop) {
            if (obj && [obj isKindOfClass:[NSNull class]]) {
                [tempDic setValue:@"" forKey:key];
            }
        }];
        
        //获取用户信息接口没返回以下字段
        if (![tempDic valueForKey:@"token"]) {
            [tempDic setValue:self.token ?:@"" forKey:@"token"];
        }
        
        if (![tempDic valueForKey:@"remember_token"]) {
            [tempDic setValue:self.remember_token ?:@"" forKey:@"remember_token"];
        }
        
        if (![tempDic valueForKey:@"password"]) {
            [tempDic setValue:self.password ?:@"" forKey:@"password"];
        }
        
        BOOL flag = [tempDic writeToFile:UserDataFile atomically:YES];
        FBSLog(@"写入用户数据到本地 : %@",flag ? @"成功" : @"失败");
        FBSLog(@"本地用户数据 : %@",tempDic);
        
        self.userModel = [FBSUserModel mj_objectWithKeyValues:tempDic];
    }
}


- (void)deleteUserData {
    NSError *error;
    [[NSFileManager defaultManager] removeItemAtPath:UserDataFile error:&error];
    if (error) {
        FBSLog(@"删除用户数据失败: %@",error.localizedDescription);
    } else {
        FBSLog(@"删除用户数据成功");
        self.userModel = nil;
        self.token = nil;
        self.remember_token = nil;
        self.password = nil;
        
        //模拟登录退出请求
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            //发布退出登录通知
            [[NSNotificationCenter defaultCenter] postNotificationName:FBSLogoutSuccessNotication object:nil];

        });
    }
}


- (void)fetchNewestUserData {
    if (![self isLogin]) {
        return ;
    }
    
    WEAKSELF
    [self.useInfoAPI startWithSuccess:^(YBNetworkResponse * _Nonnull response) {
        FBSLog(@"获取最新用户信息成功 : %@",response.responseObject);
        [weakSelf saveUserData:response.responseObject];
        
    } failure:^(YBNetworkResponse * _Nonnull response) {
        FBSLog(@"获取最新用户信息失败 : %@",response.error.localizedDescription);
        
    }];
}



- (NSDictionary *)userDataDic {
    return [NSDictionary dictionaryWithContentsOfFile:UserDataFile];
}

#pragma mark - getter

- (NSString *)useId {
    return self.userModel.useId ?:@"";
}

- (NSString *)account {
    return self.userModel.account ?:@"";
}

- (NSString *)email {
    return self.userModel.email ?:@"";
}

- (NSString *)tel {
    return self.userModel.tel ?:@"";
}


- (NSString *)nickname {
    return self.userModel.nickname ?:@"";
}

- (NSString *)file_id {
    return self.userModel.file_id ?:@"";
}


- (NSString *)remarks {
    return self.userModel.remarks ?:@"";
}

- (NSString *)password {
    return self.userModel.password ?:@"";
}


- (NSString *)descripe {
    return self.userModel.descripe ?:@"";
}


- (NSString *)remember_token {
    return self.userModel.remember_token ?:@"";
}


- (NSString *)dep_id {
    return self.userModel.dep_id ?:@"";
}

- (NSString *)role_id {
    return self.userModel.role_id ?:@"";
}

- (NSInteger)created_at {
    return self.userModel.created_at ?: 0;
}

- (NSInteger)updated_at {
    return self.userModel.updated_at ?: 0;
}


- (NSString *)token {
    return self.userModel.token ?:@"";
}


- (BOOL)isLogin {
    if ([self token].length) {
        return YES;
    } else {
        return NO;
    }
}


- (FBSMineInfoAPI *)useInfoAPI {
    if (!_useInfoAPI) {
        _useInfoAPI  = [[FBSMineInfoAPI alloc] init];
        _useInfoAPI.requestURI = [NSString stringWithFormat:@"user/%@",self.useId];
    }
    return _useInfoAPI;
}


- (void)setUserModel:(FBSUserModel *)userModel {
    _userModel = userModel;
    
    //更新用户信息时需要用到
    self.token = self.userModel.token;
    self.remember_token = self.userModel.remember_token;
    self.password = self.userModel.password;
}

@end
