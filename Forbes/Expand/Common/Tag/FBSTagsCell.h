//
//  FBSTagsCell.h
//  Forbes
//
//  Created by 周灿华 on 2019/8/15.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSCell.h"
#import "FBSTagsLayout.h"

@interface FBSTagsItem : FBSItem
@property (nonatomic, strong) NSArray * tags;
@property (nonatomic, strong) NSMutableArray  *selectedTags;

@end


@class FBSTagAttribute;
@interface FBSTagsCell : FBSCell

@property (nonatomic,strong) NSArray *tags;//传入的标签数组 字符串数组
@property (nonatomic,strong) NSMutableArray *selectedTags; //选择的标签数组

@property (nonatomic,strong) FBSTagAttribute *tagAttribute;//按钮样式对象
@property (nonatomic,assign) BOOL isMultiSelect;//是否可以多选 默认:NO 单选
@property (nonatomic,copy) NSString *key;//搜索关键词

//刷新界面
- (void)reloadData;

@end
