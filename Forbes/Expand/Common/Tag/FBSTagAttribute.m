//
//  FBSTagAttribute.m
//  Forbes
//
//  Created by 周灿华 on 2019/8/15.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSTagAttribute.h"

@implementation FBSTagAttribute

- (instancetype)init
{
    self = [super init];
    if (self) {
        UIColor *normalColor = Hexcolor(0xF6F7F9);
        UIColor *normalBackgroundColor = Hexcolor(0xF6F7F9);
        UIColor *selectedBackgroundColor = Hexcolor(0xF6F7F9);
        
        _borderWidth = 1.0f;
        _normalBorderColor = normalColor;
        _selectedBorderColor = MainThemeColor;
        _cornerRadius = 0.0;
        _normalBackgroundColor = normalBackgroundColor;
        _selectedBackgroundColor = selectedBackgroundColor;
        _titleSize = 14;
        _normalTextColor = Hexcolor(0x333333);
        _selectedTextColor = _selectedBorderColor;
        _keyColor = [UIColor redColor];
        _tagSpace = 20;
    }
    return self;
}


@end
