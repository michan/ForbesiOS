//
//  FBSTagCollectionViewCell.m
//  Forbes
//
//  Created by 周灿华 on 2019/8/15.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSTagCollectionViewCell.h"

@implementation FBSTagCollectionViewCell

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        _titleLabel = [[UILabel alloc] initWithFrame:self.bounds];
        _titleLabel.textAlignment = NSTextAlignmentCenter;
        [self.contentView addSubview:_titleLabel];
    }
    
    return self;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    self.titleLabel.frame = self.bounds;
}

- (void)prepareForReuse
{
    [super prepareForReuse];
    
    self.titleLabel.text = @"";
}




@end
