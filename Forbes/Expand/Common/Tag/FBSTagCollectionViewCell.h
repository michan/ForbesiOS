//
//  FBSTagCollectionViewCell.h
//  Forbes
//
//  Created by 周灿华 on 2019/8/15.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FBSTagCollectionViewCell : UICollectionViewCell
@property (nonatomic,strong) UILabel *titleLabel;
@end

NS_ASSUME_NONNULL_END
