//
//  FBSTagAttribute.h
//  Forbes
//
//  Created by 周灿华 on 2019/8/15.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FBSTagAttribute : NSObject
@property (nonatomic,assign) CGFloat borderWidth;//标签边框宽度
@property (nonatomic,strong) UIColor *normalBorderColor;//标签默认边框颜色
@property (nonatomic,strong) UIColor *selectedBorderColor;//标签选中边框颜色
@property (nonatomic,assign) CGFloat cornerRadius;//标签圆角大小
@property (nonatomic,strong) UIColor *normalBackgroundColor;//标签默认背景颜色
@property (nonatomic,strong) UIColor *selectedBackgroundColor;//标签选中背景颜色
@property (nonatomic,assign) CGFloat titleSize;//标签字体大小
@property (nonatomic,strong) UIColor *normalTextColor;//标签默认字体颜色
@property (nonatomic,strong) UIColor *selectedTextColor;//标签选中字体颜色
@property (nonatomic,strong) UIColor *keyColor;//搜索关键词颜色
@property (nonatomic,assign) CGFloat tagSpace;//标签内部左右间距(标题距离边框2边的距
@end
