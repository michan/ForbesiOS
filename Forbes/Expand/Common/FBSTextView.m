//
//  FBSTextView.m
//  Forbes
//
//  Created by 周灿华 on 2019/8/14.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSTextView.h"

@interface FBSTextView ()<UITextViewDelegate>

@property (nonatomic, strong) UILabel *placeholderLabel;

@end


@implementation FBSTextView

- (instancetype)initWithFrame:(CGRect)frame{
    
    if (self = [super initWithFrame:frame]) {
        
        [self initialize];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder{
    
    if (self = [super initWithCoder:aDecoder]) {
        
        [self initialize];
    }
    return self;
}

- (void)initialize{
    //设置默认值
    self.maxLength = 0;
    
    //设置基本属性
    self.textColor = [UIColor colorWithRed:51/255.0 green:51/255.0 blue:51/255.0 alpha:1.0];
    self.font = [UIFont systemFontOfSize:14];
    self.backgroundColor = [UIColor whiteColor];
    UIEdgeInsets inset = self.textContainerInset;
    //    inset.right = RealValue(13);
    self.textContainerInset = inset;
    
    //设置代理
    self.delegate = self;
}


#pragma mark - textViewDelegate
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    
    //如果用户点击了return
    if([text isEqualToString:@"\n"]){
        if (_realDelegate && [_realDelegate respondsToSelector:@selector(limitedTextViewShouldReturn:)]) {
            return [_realDelegate limitedTextViewShouldReturn:textView];
        }
        return NO;
    }
    
    //长度限制操作
    NSString *str = [NSString stringWithFormat:@"%@%@", textView.text, text];
    
    if (str.length > self.maxLength && self.maxLength){
        
        NSRange rangeIndex = [str rangeOfComposedCharacterSequenceAtIndex:self.maxLength];
        
        if (rangeIndex.length == 1){//字数超限
            textView.text = [str substringToIndex:self.maxLength];
            //记录输入的字数
            self.inputLength = textView.text.length;
            if (_realDelegate && [_realDelegate respondsToSelector:@selector(limitedTextViewDidChange:)]) {
                [_realDelegate limitedTextViewDidChange:textView];
            }
            
        }else{
            NSRange rangeRange = [str rangeOfComposedCharacterSequencesForRange:NSMakeRange(0, self.maxLength)];
            textView.text = [str substringWithRange:rangeRange];
        }
        return NO;
    }
    return YES;
}

- (void)textViewDidChange:(UITextView *)textView{
    
    if (textView.text.length > self.maxLength && self.maxLength){
        textView.text = [textView.text substringToIndex:self.maxLength];
    }
    //记录输入的字数
    self.inputLength = textView.text.length;
    
    if (_realDelegate && [_realDelegate respondsToSelector:@selector(limitedTextViewDidChange:)]) {
        [_realDelegate limitedTextViewDidChange:textView];
    }
}

- (void)textViewDidEndEditing:(UITextView *)textView{
    if (_realDelegate && [_realDelegate respondsToSelector:@selector(limitedTextViewDidEndEditing:)]) {
        [_realDelegate limitedTextViewDidEndEditing:textView];
    }
}




- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    for (NSString *key in self.class.observingKeys) {
        @try {
            [self removeObserver:self forKeyPath:key];
        }
        @catch (NSException *exception) {
            // Do nothing
        }
    }
}


+ (UIColor *)defaultPlaceholderColor {
    static UIColor *color = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        UITextField *textField = [[UITextField alloc] init];
        textField.placeholder = @" ";
//        color = [textField valueForKeyPath:@"_placeholderLabel.textColor"];
        color = Hexcolor(0xCBCCCC);
    });
    return color;
}

+ (NSArray *)observingKeys {
    return @[@"attributedText",
             @"bounds",
             @"font",
             @"frame",
             @"text",
             @"textAlignment",
             @"textContainerInset"
             ];
}


- (UILabel *)placeholderLabel {
    if (!_placeholderLabel) {
        NSAttributedString *originalText = self.attributedText;
        self.text = @" "; // lazily set font of `UITextView`.
        self.attributedText = originalText;
        
        _placeholderLabel = [[UILabel alloc] init];
        //        _placeholderLabel.textColor = [self.class defaultPlaceholderColor];
        
        
        //        _placeholderLabel.textColor=  COLOR_RGB(136,136,136);
        _placeholderLabel.numberOfLines = 0;
        _placeholderLabel.userInteractionEnabled = NO;
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(updatePlaceholderLabel)
                                                     name:UITextViewTextDidChangeNotification
                                                   object:self];
        
        for (NSString *key in self.class.observingKeys) {
            [self addObserver:self forKeyPath:key options:NSKeyValueObservingOptionNew context:nil];
        }
    }
    return _placeholderLabel;
}



#pragma mark `placeholder`

- (NSString *)placeholder {
    return self.placeholderLabel.text;
}

- (void)setPlaceholder:(NSString *)placeholder {
    self.placeholderLabel.text = placeholder;
    [self updatePlaceholderLabel];
}

- (NSAttributedString *)attributedPlaceholder {
    return self.placeholderLabel.attributedText;
}

- (void)setAttributedPlaceholder:(NSAttributedString *)attributedPlaceholder {
    self.placeholderLabel.attributedText = attributedPlaceholder;
    [self updatePlaceholderLabel];
}

#pragma mark `placeholderColor`

- (UIColor *)placeholderColor {
    return self.placeholderLabel.textColor;
}

- (void)setPlaceholderColor:(UIColor *)placeholderColor {
    self.placeholderLabel.textColor = placeholderColor;
}


#pragma mark - KVO

- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context {
    [self updatePlaceholderLabel];
}


#pragma mark - Update

- (void)updatePlaceholderLabel {
    if (self.text.length) {
        [self.placeholderLabel removeFromSuperview];
        return;
    }
    
    [self insertSubview:self.placeholderLabel atIndex:0];
    
    //设置这两个属性会 placeholderLabel 的属性文本会失效
    //    self.placeholderLabel.font = self.font;
    self.placeholderLabel.textAlignment = self.textAlignment;
    
    // `NSTextContainer` is available since iOS 7
    CGFloat lineFragmentPadding;
    UIEdgeInsets textContainerInset;
    
#pragma deploymate push "ignored-api-availability"
    // iOS 7+
    if (NSFoundationVersionNumber > NSFoundationVersionNumber_iOS_6_1) {
        lineFragmentPadding = self.textContainer.lineFragmentPadding;
        textContainerInset = self.textContainerInset;
    }
#pragma deploymate pop
    
    // iOS 6
    else {
        lineFragmentPadding = 5;
        textContainerInset = UIEdgeInsetsMake(8, 0, 8, 0);
    }
    
    CGFloat x = lineFragmentPadding + textContainerInset.left;
    CGFloat y = textContainerInset.top;
    CGFloat width = CGRectGetWidth(self.bounds) - x - lineFragmentPadding - textContainerInset.right;
    CGFloat height = [self.placeholderLabel sizeThatFits:CGSizeMake(width, 0)].height;
    self.placeholderLabel.frame = CGRectMake(x, y, width, height);
}


@end

