//
//  FBSGroupHeadCell.h
//  Forbes
//
//  Created by 周灿华 on 2019/9/25.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSCell.h"

@interface FBSGroupHeadItem : FBSItem
@property (nonatomic, strong) NSString *title;
@property (nonatomic, assign) BOOL isShowRight;
@end



@interface FBSGroupHeadCell : FBSCell

@end
