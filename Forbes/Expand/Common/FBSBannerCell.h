//
//  FBSBannerCell.h
//  Forbes
//
//  Created by 周灿华 on 2019/7/28.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSCell.h"

@interface FBSBannerInfo : NSObject
@property (nonatomic, copy) NSString *ID;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *author;
@property (nonatomic, copy) NSString *time;
@property (nonatomic, copy) NSString *imgUrl;
@property (nonatomic, assign) BOOL isAd;
@end



@interface FBSBannerItem : FBSItem
@property (nonatomic, strong) NSArray<FBSBannerInfo *> *datas;
@end


@interface FBSBannerCell : FBSCell

@end

