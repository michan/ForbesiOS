//
//  FBSChannelHeader.h
//  Forbes
//
//  Created by 周灿华 on 2019/7/31.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FBSChannelHeader : UICollectionReusableView

@property (nonatomic, copy) NSString *title;

@property (nonatomic, copy) NSString *subTitle;

@property (nonatomic, assign) BOOL showEdit;

@property (nonatomic, assign) BOOL isEdit;

@property (nonatomic, copy) void(^editActionBlock)(BOOL isEdit);

@end
