
//
//  FBSChannelCell.m
//  Forbes
//
//  Created by 周灿华 on 2019/7/31.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSChannelCell.h"

@interface FBSChannelCell ()

@property (nonatomic, strong) UILabel *textLabel;

@property (nonatomic, strong) UIImageView *deleteImg;

@property (nonatomic, strong) CAShapeLayer *borderLayer;

@end

@implementation FBSChannelCell

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self initUI];
    }
    return self;
}


- (void)initUI {
    self.userInteractionEnabled = true;
    self.backgroundColor = [self backgroundColor];
    
    self.deleteImg = [[UIImageView alloc] init];
    self.deleteImg.image = kImageWithName(@"common_del");
    
    CGFloat cellW = CGRectGetWidth(self.frame);
    self.deleteImg.frame = CGRectMake(cellW - 13, -3, 16, 16);
    [self addSubview:self.deleteImg];
    
    self.textLabel = [UILabel new];
    self.textLabel.frame = self.bounds;
    self.textLabel.textAlignment = NSTextAlignmentCenter;
    self.textLabel.textColor = [self textColor];
    self.textLabel.adjustsFontSizeToFitWidth = true;
    self.textLabel.userInteractionEnabled = true;
    [self addSubview:self.textLabel];
    
    [self addBorderLayer];
}

- (void)addBorderLayer{
    self.borderLayer = [CAShapeLayer layer];
    self.borderLayer.bounds = self.bounds;
    self.borderLayer.position = CGPointMake(CGRectGetMidX(self.bounds), CGRectGetMidY(self.bounds));
    self.borderLayer.path = [UIBezierPath bezierPathWithRoundedRect:self.borderLayer.bounds cornerRadius:self.layer.cornerRadius].CGPath;
    self.borderLayer.lineWidth = 1;
    self.borderLayer.lineDashPattern = @[@5, @3];
    self.borderLayer.fillColor = [UIColor clearColor].CGColor;
    self.borderLayer.strokeColor = [self backgroundColor].CGColor;
    [self.layer addSublayer:self.borderLayer];
    self.borderLayer.hidden = true;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    self.textLabel.frame = self.bounds;
}

#pragma mark -
#pragma mark 配置方法

- (UIColor*)backgroundColor{
    return [UIColor colorWithRed:241/255.0f green:241/255.0f blue:241/255.0f alpha:1];
}

- (UIColor *)textColor{
    return [UIColor colorWithRed:40/255.0f green:40/255.0f blue:40/255.0f alpha:1];
}

-(UIColor *)lightTextColor{
    return MainThemeColor;
}

#pragma mark -
#pragma mark Setter

-(void)setTitle:(NSString *)title
{
    _title = title;
    self.textLabel.text = title;
}

-(void)setIsMoving:(BOOL)isMoving
{
    _isMoving = isMoving;
    if (_isMoving) {
        self.backgroundColor = [UIColor clearColor];
        self.borderLayer.hidden = false;
        self.deleteImg.hidden = YES;
    }else{
        self.backgroundColor = [self backgroundColor];
        self.borderLayer.hidden = true;
        self.deleteImg.hidden = NO;
    }
}

-(void)setIsFixed:(BOOL)isFixed{
    _isFixed = isFixed;
    if (isFixed) {
        self.textLabel.textColor = [self lightTextColor];
        self.layer.borderColor = MainThemeColor.CGColor;
        self.layer.borderWidth = 1.0;
        self.backgroundColor = [UIColor clearColor];
    }else{
        self.textLabel.textColor = [self textColor];
        self.layer.borderWidth = 0.0;
        self.backgroundColor = [self backgroundColor];
    }
}


- (void)setIsEdit:(BOOL)isEdit {
    _isEdit = isEdit;
    self.deleteImg.hidden = !isEdit;
}

@end
