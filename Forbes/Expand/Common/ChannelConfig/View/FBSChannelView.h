//
//  FBSChannelView.h
//  Forbes
//
//  Created by 周灿华 on 2019/7/31.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import <UIKit/UIKit.h>

@class FBSChannelView;
@protocol FBSChannelViewDelegate <NSObject>
- (void)channelView:(FBSChannelView *)channel clickAtChannelTitle:(NSString *)channelTitle;
- (void)channelViewClickToSave:(FBSChannelView *)channel;
@end


@interface FBSChannelView : UIView

@property (nonatomic, weak) id<FBSChannelViewDelegate> delegete;

@property (nonatomic, assign) NSInteger fixItemCount; //固定的item个数, 默认为1
@property (nonatomic, strong) NSMutableArray *enabledTitles;

@property (nonatomic, strong) NSMutableArray *disabledTitles;

- (void)reloadData;

@end

