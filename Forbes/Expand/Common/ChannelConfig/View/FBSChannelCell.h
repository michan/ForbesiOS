//
//  FBSChannelCell.h
//  Forbes
//
//  Created by 周灿华 on 2019/7/31.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FBSChannelCell : UICollectionViewCell
//标题
@property (nonatomic, copy) NSString *title;

//是否正在移动状态
@property (nonatomic, assign) BOOL isMoving;

//是否是编辑状态
@property (nonatomic, assign) BOOL isEdit;

//是否被固定
@property (nonatomic, assign) BOOL isFixed;

@end

