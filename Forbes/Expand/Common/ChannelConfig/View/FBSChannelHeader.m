//
//  FBSChannelHeader.m
//  Forbes
//
//  Created by 周灿华 on 2019/7/31.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSChannelHeader.h"

@interface FBSChannelHeader ()
@property (nonatomic, strong) UILabel *titleL;
@property (nonatomic, strong) UILabel *subtitleL;
@property (nonatomic, strong) UIButton *editBtn;
@end


@implementation FBSChannelHeader

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self buildUI];
    }
    return self;
}

- (void)buildUI {
    [self addSubview:self.titleL];
    [self addSubview:self.subtitleL];
    [self addSubview:self.editBtn];
    self.editBtn.hidden = YES;
    
    [self.titleL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.centerY.mas_equalTo(0);
    }];
    
    [self.subtitleL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.titleL.mas_right).offset(10);
        make.centerY.mas_equalTo(0);
    }];
    
    [self.editBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-15);
        make.centerY.mas_equalTo(0);
        make.width.mas_equalTo(64);
        make.height.mas_equalTo(24);
    }];
    
}

- (void)editAction:(UIButton *)sender {
    sender.selected = !sender.isSelected;
    if (self.editActionBlock) {
        self.editActionBlock(sender.isSelected);
    }
}

#pragma mark - setter

- (void)setIsEdit:(BOOL)isEdit {
    _isEdit = isEdit;
    self.editBtn.selected = isEdit;
}

- (void)setTitle:(NSString *)title {
    _title = title;
    self.titleL.text = title;
}

- (void)setSubTitle:(NSString *)subTitle {
    _subTitle = subTitle;
    self.subtitleL.text = subTitle;
}

- (void)setShowEdit:(BOOL)showEdit {
    _showEdit = showEdit;
    self.editBtn.hidden = !showEdit;
}

#pragma mark - property

- (UILabel *)titleL {
    if (!_titleL) {
        _titleL = [[UILabel alloc] init];
        _titleL.font = [UIFont systemFontOfSize:15];
        _titleL.textColor = Hexcolor(0x333333);
    }
    return _titleL;
}


- (UILabel *)subtitleL {
    if (!_subtitleL) {
        _subtitleL = [[UILabel alloc] init];
        _subtitleL.font = [UIFont systemFontOfSize:12];
        _subtitleL.textColor = Hexcolor(0x999999);
    }
    return _subtitleL;
}


- (UIButton *)editBtn {
    if (!_editBtn) {
        _editBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _editBtn.titleLabel.font = [UIFont systemFontOfSize:13];
        [_editBtn setTitle:@"编辑" forState:UIControlStateNormal];
        [_editBtn setTitle:@"完成" forState:UIControlStateSelected];
        [_editBtn setTitleColor:MainThemeColor forState:UIControlStateNormal];
        _editBtn.layer.cornerRadius = 12;
        _editBtn.layer.borderWidth = 1.0;
        _editBtn.layer.borderColor = MainThemeColor.CGColor;
        [_editBtn addTarget:self action:@selector(editAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _editBtn;
}


@end
