//
//  FBSChannelConfigController.h
//  Forbes
//
//  Created by 周灿华 on 2019/10/1.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSViewController.h"
#import "FBSChannelView.h"

@class FBSChannelConfigController;
@protocol FBSChannelConfigControllerDelegate <NSObject>
@required
- (NSArray *)enabledTitles:(FBSChannelConfigController *)channel;
- (NSArray *)disabledTitles:(FBSChannelConfigController *)channel;

- (void)channelViewController:(FBSChannelConfigController *)channel clickAtChannelTitle:(NSString *)channelTitle;
- (void)channelViewControllerClickToSave:(FBSChannelConfigController *)channel
                            enableTitles:(NSArray *)enableTitles
                           disableTitles:(NSArray *)disableTitles;
@end


@interface FBSChannelConfigController : FBSViewController
@property (nonatomic, strong, readonly) FBSChannelView *channelView;
@property (nonatomic, assign) NSInteger fixItemCount; //固定的item个数, 默认为1
@property (nonatomic, weak) id <FBSChannelConfigControllerDelegate> delegete;
@end

