//
//  FBSChannelModel.h
//  Forbes
//
//  Created by 周灿华 on 2019/10/12.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSBaseModel.h"

///频道信息
@class FBSChannelInfo;
@interface FBSChannelModel : FBSBaseModel
@property (nonatomic, strong) NSArray<FBSChannelInfo *> *data;
@end

@interface FBSChannelInfo : NSObject
@property (nonatomic, strong) NSString *ID;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *type;
@property (nonatomic, strong) NSString *channel_id;
@property (nonatomic, strong) NSString *channel_type;
@property (nonatomic, strong) NSString *path;
@property (nonatomic, strong) NSString *code;
// 视频接口分类
@property (nonatomic, strong) NSString *category;


//自定义数据
@property (nonatomic, strong) NSString *enable;  //是否展示
@property (nonatomic, strong) NSString *order;   //序号
@end
