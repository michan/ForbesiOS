//
//  FBSChannelModel.m
//  Forbes
//
//  Created by 周灿华 on 2019/10/12.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSChannelModel.h"

@implementation FBSChannelModel

+ (NSDictionary *)mj_objectClassInArray {
    return @{
             @"data" : @"FBSChannelInfo"
             };
}

@end


@implementation FBSChannelInfo

+ (NSDictionary *)mj_replacedKeyFromPropertyName {
    return @{
             @"ID" : @"id"
             };
}

@end
