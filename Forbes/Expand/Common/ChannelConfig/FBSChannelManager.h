//
//  FBSChannelManager.h
//  Forbes
//
//  Created by 周灿华 on 2019/9/21.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import <Foundation/Foundation.h>

#define KeyNewsChannelId    @"100000"   //自定义要闻频道id
#define BrandvoiceChannelId @"200000"   //自定义品牌之声频道id

@interface FBSChannelManager : NSObject

+ (instancetype)sharedManager;

- (void)fetchChannels:(void(^)(NSArray<NSString *> *enableTitles,NSArray<NSString *> *disableTitles))complete;

///更新频道数据
- (void)updateChannels:(NSArray *)enableTitles
         disableTitles:(NSArray *)disableTitles;

///根据标题获取频道id
- (NSString *)channelIdWithTitle:(NSString *)title;
@end

