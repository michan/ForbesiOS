//
//  FBSChannelConfigController.m
//  Forbes
//
//  Created by 周灿华 on 2019/10/1.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSChannelConfigController.h"

@interface FBSChannelConfigController ()<FBSChannelViewDelegate>
@property (nonatomic, strong) FBSChannelView *channelView;
@end

@implementation FBSChannelConfigController

- (instancetype)init {
    self = [super init];
    if (self) {
        self.fixItemCount = 1;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"全部频道";
    self.navigationBar.backImage = [CommonTool imageWithImage:kImageWithName(@"common_close") scaledToSize:CGSizeMake(15, 15)];
    [self buildChannelView];
}

- (void)buildChannelView {
    [self.view addSubview:self.channelView];
    [self.channelView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.navigationBar.mas_bottom);
        make.left.bottom.right.mas_equalTo(0);
    }];
    
    self.channelView.enabledTitles = [[self getEnabledTitles] mutableCopy];
    
    self.channelView.disabledTitles = [[self getDisabledTitles] mutableCopy];

}


- (void)onTouchNavigationBarBackButton {
    [self dismissViewControllerAnimated:YES completion:NULL];
}



- (NSArray *)getEnabledTitles {
    NSArray *enabledTitles = @[];
    if (self.delegete && [self.delegete respondsToSelector:@selector(enabledTitles:)]) {
        enabledTitles = [self.delegete enabledTitles:self];
        if (!enabledTitles.count) {
            enabledTitles = @[];
        }
    }
    return enabledTitles;
}

- (NSArray *)getDisabledTitles {
    NSArray *disabledTitles = @[];
    if (self.delegete && [self.delegete respondsToSelector:@selector(disabledTitles:)]) {
        disabledTitles = [self.delegete disabledTitles:self];
        if (!disabledTitles.count) {
            disabledTitles = @[];
        }
    }
    return disabledTitles;
}


- (void)setFixItemCount:(NSInteger)fixItemCount  {
    if (fixItemCount < 0) {
        fixItemCount = 0;
    }
    _fixItemCount = fixItemCount;
    self.channelView.fixItemCount = fixItemCount;
}

#pragma mark - FBSChannelViewDelegate

- (void)channelView:(FBSChannelView *)channel clickAtChannelTitle:(NSString *)channelTitle {
    if (self.delegete && [self.delegete respondsToSelector:@selector(channelViewController:clickAtChannelTitle:)]) {
        [self.delegete channelViewController:self clickAtChannelTitle:channelTitle];
    }
}

- (void)channelViewClickToSave:(FBSChannelView *)channel {
    if (self.delegete && [self.delegete respondsToSelector:@selector(channelViewControllerClickToSave:enableTitles:disableTitles:)]) {
        [self.delegete channelViewControllerClickToSave:self
                                          enableTitles:[channel.enabledTitles copy]
                                         disableTitles:[channel.disabledTitles copy]];
    }
}

#pragma mark - property

- (FBSChannelView *)channelView {
    if (!_channelView) {
        _channelView = [[FBSChannelView alloc] initWithFrame:[UIScreen mainScreen].bounds];
        _channelView.backgroundColor = [UIColor whiteColor];
        _channelView.delegete = self;
    }
    return _channelView;
}


@end
