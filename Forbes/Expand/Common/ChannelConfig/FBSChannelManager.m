//
//  FBSChannelManager.m
//  Forbes
//
//  Created by 周灿华 on 2019/9/21.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSChannelManager.h"
#import "FBSChannelAPI.h"
#import "FBSChannelModel.h"

#define AllChannelFile [NSString stringWithFormat:@"%@/allChannel.plist",kDocumentPath]
#define EnableTitlesFile [NSString stringWithFormat:@"%@/enableTitles.plist",kDocumentPath]


@interface FBSChannelManager ()
@property (nonatomic, strong) FBSChannelAPI *channelAPI;
@property (nonatomic, strong) FBSChannelModel *allChannelModel;
@property (nonatomic, strong) NSArray<NSString *> *enableTitles;
@property (nonatomic, strong) NSArray<NSString *> *disableTitles;

@property (nonatomic, strong) void(^complete)(NSArray<NSString *> *enableTitles,NSArray<NSString *> *disableTitles);
@end

@implementation FBSChannelManager

static FBSChannelManager *instance_;

+ (instancetype)sharedManager {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance_ = [[FBSChannelManager alloc] init];
    });
    return instance_;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (void)commonInit {
    NSDictionary *allChannelDic = [NSDictionary dictionaryWithContentsOfFile:AllChannelFile];
    self.allChannelModel = [FBSChannelModel mj_objectWithKeyValues:allChannelDic];
}


- (void)saveChannelData:(NSDictionary *)jsonDic {
    if (!jsonDic.count) {
        return ;
    }
    
    NSMutableDictionary *tempJsonDic = [jsonDic mutableCopy];
    NSMutableArray *tempDataM = [NSMutableArray array];
    NSArray *dataArr = [jsonDic[@"data"] mutableCopy];
    
    for (NSDictionary *subDic in dataArr) {
        if ([subDic isKindOfClass:[NSDictionary class]]) {
            // 把value 为NULL的值转化为"",不然保存到文件会失败
            NSMutableDictionary *tempDic = [subDic mutableCopy];
            [subDic enumerateKeysAndObjectsUsingBlock:^(NSString * _Nonnull key, NSString *   _Nonnull obj, BOOL * _Nonnull stop) {
                if (obj && [obj isKindOfClass:[NSNull class]]) {
                    [tempDic setValue:@"" forKey:key];
                }
            }];
            
            [tempDataM addObject:tempDic];
        }
    }
    
    tempJsonDic[@"data"] = [tempDataM copy];
    
    BOOL flag = [tempJsonDic writeToFile:AllChannelFile atomically:YES];
    FBSLog(@"写入频道数据到本地 : %@",flag ? @"成功" : @"失败");
    FBSLog(@"本地频道数据 : %@",tempJsonDic);
    
    if (!self.enableTitles.count) {
        //第一次保存显示的频道
        NSArray *enableTitles = [tempDataM valueForKey:@"title"];
        [enableTitles writeToFile:EnableTitlesFile atomically:YES];
    }
}


#pragma mark - public method

- (void)fetchChannels:(void(^)(NSArray<NSString *> *enableTitles,NSArray<NSString *> *disableTitles))complete {
    self.complete = complete;
    
    if (self.allChannelModel.data.count) {
        self.complete(self.enableTitles, self.disableTitles);
        self.complete = nil;
    }
    
    //获取最新的频道数据
    WEAKSELF
    [self.channelAPI startWithSuccess:^(YBNetworkResponse * _Nonnull response) {
        FBSLog(@"获取所有频道数据 : %@",response.responseObject);
        
        FBSChannelModel *model = [FBSChannelModel mj_objectWithKeyValues:response.responseObject];
        NSMutableArray *dataM = [model.data mutableCopy];
        
        //添加要闻频道
        FBSChannelInfo *keyNewsChannelInfo = [[FBSChannelInfo alloc] init];
        keyNewsChannelInfo.ID = @"";
        keyNewsChannelInfo.title = @"要闻";
        keyNewsChannelInfo.type = @"";
        keyNewsChannelInfo.channel_id = KeyNewsChannelId;
        keyNewsChannelInfo.channel_type = @"";
        keyNewsChannelInfo.path = @"";
        keyNewsChannelInfo.code = @"";
        [dataM insertObject:keyNewsChannelInfo atIndex:0];
        
        //过滤 榜单、活动
        for (FBSChannelInfo *info in model.data) {
            if ([info.channel_type isEqualToString:@"2"] ||
                [info.channel_type isEqualToString:@"3"]) {
                [dataM removeObject:info];
                continue;
            }
            
            if ([info.channel_type isEqualToString:@"5"]) { //设置品牌之声频道ID
                info.channel_id = BrandvoiceChannelId;
            }
        }
        
        model.data = [dataM copy];
        weakSelf.allChannelModel = model;
        
        if (model.success) {
            //保存频道数据
            [weakSelf saveChannelData:model.mj_keyValues];
        }
        
        if (weakSelf.complete) {
            weakSelf.complete(weakSelf.enableTitles, weakSelf.disableTitles);
        }
        
    } failure:^(YBNetworkResponse * _Nonnull response) {
        if (weakSelf.complete) {
            weakSelf.complete(weakSelf.enableTitles, weakSelf.disableTitles);
        }
        
    }];
}


- (void)updateChannels:(NSArray *)enableTitles disableTitles:(NSArray *)disableTitles {
    if (enableTitles.count) {
        [enableTitles writeToFile:EnableTitlesFile atomically:YES];
    }
}

- (NSString *)channelIdWithTitle:(NSString *)title {
    if (!title.length) {
        return @"";
    }
    
    NSString *channelId = @"";
    for (FBSChannelInfo *info in self.allChannelModel.data) {
        if ([title isEqualToString:info.title]) {
            channelId = info.channel_id;
            break ;
        }
    }
    return channelId;
}

#pragma mark - propety

- (FBSChannelAPI *)channelAPI {
    if (!_channelAPI) {
        _channelAPI  = [[FBSChannelAPI alloc] init];
    }
    return _channelAPI;
}


- (NSArray<NSString *> *)enableTitles {
    return [NSArray arrayWithContentsOfFile:EnableTitlesFile];
}

- (NSArray<NSString *> *)disableTitles {
    NSMutableArray *disableTitles = [NSMutableArray array];
    for (FBSChannelInfo *info in self.allChannelModel.data) {
        if (![self.enableTitles containsObject:info.title]) {
            [disableTitles addObject:info.title];
        }
    }
    return [disableTitles copy];
}


@end
