//
//  FBSTextView.h
//  Forbes
//
//  Created by 周灿华 on 2019/8/14.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol FBSTextViewDelegate <NSObject>

@optional


/**
 textView 键盘return事件监听
 
 @param textView textView
 */
-(BOOL)limitedTextViewShouldReturn:(UITextView *)textView;


/**
 textView内容改变实时监听
 
 @param textView textView
 */
- (void)limitedTextViewDidChange:(UITextView *)textView;


/**
 textView end editing
 
 @param textView textView
 */
- (void)limitedTextViewDidEndEditing:(UITextView *)textView;

@end


@interface FBSTextView : UITextView


@property (nonatomic, weak) id <FBSTextViewDelegate> realDelegate;

/** 允许输入的最大长度 默认 0不限制 */
@property (nonatomic, assign)   NSInteger maxLength;
/** 输入内容长度 */
@property (nonatomic, assign)   NSInteger inputLength;
@property (nonatomic, strong)   NSString *placeholder;
@property (nonatomic, strong)   NSAttributedString *attributedPlaceholder;
@property (nonatomic, strong)   UIColor *placeholderColor;

+ (UIColor *)defaultPlaceholderColor;


@end
