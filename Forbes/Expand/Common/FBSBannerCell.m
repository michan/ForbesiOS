
//
//  FBSBannerCell.m
//  Forbes
//
//  Created by 周灿华 on 2019/7/28.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSBannerCell.h"
#import <TYCyclePagerView.h>
#import <TYPageControl.h>

@implementation FBSBannerInfo

@end


@implementation FBSBannerItem

- (instancetype)init {
    self = [super init];
    if (self) {
        self.bottomLineHidden = YES;
        self.cellHeight = WScale(194);
    }
    return self;
}
@end


@interface FBSBannerInfoCell : UICollectionViewCell
@property (nonatomic, strong) FBSBannerInfo *info;

@property (nonatomic, strong) UIImageView *bgImgV;
@property (nonatomic, strong) UILabel *titleL;
@property (nonatomic, strong) UILabel *authorL;
@property (nonatomic, strong) UILabel *timeL;
@property (nonatomic, strong) UIView *maskView;

@end


@implementation FBSBannerInfoCell

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.bgImgV.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5];
        self.bgImgV.contentMode = UIViewContentModeScaleAspectFill;
        self.bgImgV.layer.masksToBounds = YES;
        
        [self.contentView addSubview:self.bgImgV];
        [self.contentView addSubview:self.titleL];
        [self.contentView addSubview:self.authorL];
        [self.contentView addSubview:self.timeL];
        [self.bgImgV addSubview:self.maskView];
        
        [self.bgImgV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.mas_offset(0);
        }];
        
        [self.authorL mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(WScale(13));
            make.height.mas_equalTo(WScale(11));
            make.bottom.mas_equalTo(-WScale(10));
        }];
        
        
        [self.timeL mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.authorL.mas_right).offset(WScale(5));
            make.height.mas_equalTo(WScale(11));
            make.centerY.mas_equalTo(self.authorL);
        }];

        [self.titleL mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.mas_equalTo(self.authorL.mas_top).offset(-WScale(10));
            make.left.mas_equalTo(self.authorL);
            make.right.mas_equalTo(-WScale(10));
        }];
        
        
        [self.maskView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.mas_offset(0);
        }];

        
    }
    return self;
}

- (void)setInfo:(FBSBannerInfo *)info {
    _info = info;
    self.titleL.text = info.title;
    self.authorL.text = info.author;
    self.timeL.text = info.time;

    if (info.imgUrl.length) {
        [self.bgImgV sd_setImageWithURL:[NSURL URLWithString:info.imgUrl]];
    }
}

#pragma mark - prperty

- (UIImageView *)bgImgV {
    if (!_bgImgV) {
        _bgImgV  = [[UIImageView alloc] init];
    }
    return _bgImgV;
}


- (UILabel *)titleL {
    if (!_titleL) {
        _titleL  = [[UILabel alloc] init];
        _titleL.textColor = [UIColor whiteColor];
        _titleL.text = @"-";
        _titleL.font = kLabelFontSize18;
        _titleL.numberOfLines = 2;
    }
    return _titleL;
}

- (UILabel *)authorL {
    if (!_authorL) {
        _authorL  = [[UILabel alloc] init];
        _authorL.textColor = [UIColor whiteColor];
        _authorL.text = @"-";
        _authorL.font = kLabelFontSize11;
    }
    return _authorL;
}



- (UILabel *)timeL {
    if (!_timeL) {
        _timeL  = [[UILabel alloc] init];
        _timeL.textColor = [UIColor whiteColor];
        _timeL.text = @"-";
        _timeL.font = kLabelFontSize11;
    }
    return _timeL;
}

- (UIView *)maskView {
    if (!_maskView) {
        _maskView  = [[UIView alloc] init];
        _maskView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.3];
    }
    return _maskView;
}


@end




@interface FBSBannerCell ()<TYCyclePagerViewDataSource,TYCyclePagerViewDelegate>
{
    FBSBannerItem *_currentItem;
}
@property (nonatomic, strong) TYCyclePagerView *pagerView;
@property (nonatomic, strong) TYPageControl *pageControl;

@end


@implementation FBSBannerCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self.contentView addSubview:self.pagerView];
//        [self.pagerView addSubview:self.pageControl];
        
        [self.pagerView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.mas_equalTo(0);
        }];
        
        
//        [self.pageControl mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.left.bottom.right.mas_equalTo(0);
//            make.height.mas_equalTo(WScale(26));
//        }];
        
        
    }
    return self;
}


- (void)setItem:(FBSBannerItem *)item {
    if (![item isKindOfClass:[FBSBannerItem class]]) {
        return ;
    }
    
    [super setItem:item];
    _currentItem = item;

    self.pagerView.isInfiniteLoop = item.datas.count > 1;
    [self.pagerView reloadData];
//    self.pageControl.numberOfPages = item.datas.count;
}



#pragma mark - TYCyclePagerViewDataSource

- (NSInteger)numberOfItemsInPagerView:(TYCyclePagerView *)pageView {
    return _currentItem.datas.count;
}

- (UICollectionViewCell *)pagerView:(TYCyclePagerView *)pagerView cellForItemAtIndex:(NSInteger)index {
    FBSBannerInfoCell *cell = [pagerView dequeueReusableCellWithReuseIdentifier:@"cellId" forIndex:index];
    cell.info = [_currentItem.datas objectAtIndex:index];
    return cell;
}

- (TYCyclePagerViewLayout *)layoutForPagerView:(TYCyclePagerView *)pageView {
    TYCyclePagerViewLayout *layout = [[TYCyclePagerViewLayout alloc]init];
//    layout.itemSize = CGSizeMake(CGRectGetWidth(pageView.frame)*0.8, CGRectGetHeight(pageView.frame)*0.8);
    layout.itemSize = CGSizeMake(CGRectGetWidth(pageView.frame)*0.9, CGRectGetHeight(pageView.frame));
    layout.itemSpacing = 8;
    //layout.minimumAlpha = 0.3;
    layout.itemHorizontalCenter = YES;
    return layout;
}

- (void)pagerView:(TYCyclePagerView *)pageView didScrollFromIndex:(NSInteger)fromIndex toIndex:(NSInteger)toIndex {
    _pageControl.currentPage = toIndex;
    //[_pageControl setCurrentPage:newIndex animate:YES];
//    NSLog(@"%ld ->  %ld",fromIndex,toIndex);
}

- (void)pagerView:(TYCyclePagerView *)pageView didSelectedItemCell:(__kindof UICollectionViewCell *)cell atIndex:(NSInteger)index {
    if (self.delegate && [self.delegate respondsToSelector:@selector(fbsCell:didClickButtonAtIndex:)]) {
        [self.delegate fbsCell:self didClickButtonAtIndex:index];
    }
}


#pragma mark - property

- (TYCyclePagerView *)pagerView {
    if (!_pagerView) {
        _pagerView = [[TYCyclePagerView alloc]init];
        _pagerView.isInfiniteLoop = YES;
//        _pagerView.autoScrollInterval = 3.0;
        _pagerView.dataSource = self;
        _pagerView.delegate = self;
        [_pagerView registerClass:[FBSBannerInfoCell class] forCellWithReuseIdentifier:@"cellId"];
    }
    return _pagerView;
}


- (TYPageControl *)pageControl {
    if (!_pageControl) {
        _pageControl = [[TYPageControl alloc]init];
        //_pageControl.numberOfPages = _datas.count;
        _pageControl.currentPageIndicatorSize = CGSizeMake(6, 6);
        _pageControl.pageIndicatorSize = CGSizeMake(6, 6);
        _pageControl.currentPageIndicatorTintColor = [UIColor redColor];
        _pageControl.pageIndicatorTintColor = [UIColor grayColor];
        //    pageControl.pageIndicatorImage = [UIImage imageNamed:@"Dot"];
        //    pageControl.currentPageIndicatorImage = [UIImage imageNamed:@"DotSelected"];
        //    pageControl.contentInset = UIEdgeInsetsMake(0, 20, 0, 20);
        //    pageControl.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
        //    pageControl.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
        //    [pageControl addTarget:self action:@selector(pageControlValueChangeAction:) forControlEvents:UIControlEventValueChanged];
    }
    return _pageControl;
}




@end
