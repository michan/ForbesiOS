//
//  FBSEditView.h
//  Forbes
//
//  Created by 周灿华 on 2019/8/18.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import <UIKit/UIKit.h>

#define EditViewH (WScale(50) + kDiffTabBarH)

@interface FBSEditView : UIView

@property (nonatomic, strong) UIButton *allBtn;
@property (nonatomic, strong) UIButton *deleteBtn;
@property (nonatomic, strong) UIView *middleLine;
@property (nonatomic, assign, readonly) BOOL isSelectAll;
@property (nonatomic, strong) void(^selectBlock)(BOOL isSelectAll);
@property (nonatomic, strong) void(^deleteBlock)(void);

///设置全选按钮的状态
- (void)setAllButonStatus:(BOOL)isSelectAll;

@end

