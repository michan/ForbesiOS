//
//  FBSSuccessCell.h
//  Forbes
//
//  Created by 周灿华 on 2019/8/11.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSCell.h"

@interface FBSSuccessItem : FBSItem
@property (nonatomic, strong) NSString *icon;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *tip;
@end



@interface FBSSuccessCell : FBSCell

@end
