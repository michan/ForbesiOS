//
//  FBSEditView.m
//  Forbes
//
//  Created by 周灿华 on 2019/8/18.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSEditView.h"

@interface FBSEditView ()
@property (nonatomic, strong) UIView *topView;
@end

@implementation FBSEditView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self addSubview:self.topView];
        [self.topView addSubview:self.allBtn];
        [self.topView addSubview:self.deleteBtn];
        [self.topView addSubview:self.middleLine];
        
        [self.topView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.left.right.mas_equalTo(0);
            make.height.mas_equalTo(WScale(50));
        }];
        
        
        [self.allBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.left.bottom.mas_equalTo(0);
            make.width.mas_equalTo(self).multipliedBy(0.5);
        }];
        
        [self.deleteBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.right.bottom.mas_equalTo(0);
            make.width.mas_equalTo(self).multipliedBy(0.5);
        }];
        
        [self.middleLine mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(WScale(10));
            make.bottom.mas_equalTo(-WScale(10));
            make.width.mas_equalTo(1);
            make.centerX.mas_equalTo(0);
        }];
    }
    return self;
}

- (void)clickAction:(UIButton *)sender {
    if (sender.tag == 0) {
        sender.selected = !sender.isSelected;
        if (self.selectBlock) {
            self.selectBlock(sender.isSelected);
        }
        
    } else {
        if (self.deleteBlock) {
            self.deleteBlock();
        }
    }
}

- (BOOL)isSelectAll {
    return self.allBtn.isSelected;
}


- (void)setAllButonStatus:(BOOL)isSelectAll {
    self.allBtn.selected = isSelectAll;
}


#pragma mark - property

- (UIView *)topView {
    if (!_topView) {
        _topView = [[UIView alloc] init];
        _topView.backgroundColor = [UIColor clearColor];
    }
    return _topView;
}

- (UIButton *)allBtn {
    if (!_allBtn) {
        _allBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _allBtn.titleLabel.font = kLabelFontSize16;
        _allBtn.tag = 0;
        [_allBtn setTitleColor:RGB(74,74,74) forState:UIControlStateNormal];
        [_allBtn setTitle:@"全选" forState:UIControlStateNormal];
        [_allBtn setTitle:@"取消全选" forState:UIControlStateSelected];
        [_allBtn setImage:kImageWithName(@"bottom_editall") forState:UIControlStateNormal];
        _allBtn.titleEdgeInsets = UIEdgeInsetsMake(0, 5, 0, 0);
        _allBtn.imageEdgeInsets = UIEdgeInsetsMake(0, -5, 0, 0);
        [_allBtn addTarget:self action:@selector(clickAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _allBtn;
}


- (UIButton *)deleteBtn {
    if (!_deleteBtn) {
        _deleteBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _deleteBtn.titleLabel.font = kLabelFontSize16;
        _deleteBtn.tag = 1;
        [_deleteBtn setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
        [_deleteBtn setTitle:@"删除" forState:UIControlStateNormal];
        [_deleteBtn addTarget:self action:@selector(clickAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _deleteBtn;
}


- (UIView *)middleLine {
    if (!_middleLine) {
        _middleLine = [[UIView alloc] init];
        _middleLine.backgroundColor = RGB(196,196,196);
    }
    return _middleLine;
}

@end
