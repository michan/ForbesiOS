


//
//  FBSSpaceCell.m
//  Forbes
//
//  Created by 周灿华 on 2019/7/27.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSSpaceCell.h"

@implementation FBSSpaceItem

- (instancetype)init {
    self = [super init];
    if (self) {
        self.bottomLineHidden = YES;
        self.backgroundColor = RGB(243,247,248);
        self.cellHeight = WScale(10);
    }
    return self;
}

@end

@implementation FBSSpaceCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
    }
    return self;
}


- (void)setItem:(FBSSpaceItem *)item {
    if (![item isKindOfClass:[FBSSpaceItem class]]) {
        return ;
    }
    
    [super setItem:item];
}

@end
