//
//  FBSPlusView.h
//  Forbes
//
//  Created by 周灿华 on 2019/7/28.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FBSPlusView : UIView

@property (nonatomic, strong) void(^plusBlock)(void);

@end


NS_ASSUME_NONNULL_END
