
//
//  FBSPlusView.m
//  Forbes
//
//  Created by 周灿华 on 2019/7/28.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSPlusView.h"

@interface FBSPlusView ()
@property (nonatomic, strong) UIButton *plusBtn;

@end

@implementation FBSPlusView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.1];
        [self addSubview:self.plusBtn];
        
        [self.plusBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.mas_equalTo(0);
        }];
    }
    return self;
}


#pragma mark - private method

- (void)plusAction {
    if (self.plusBlock) {
        self.plusBlock();
    }
}

#pragma mark - property

- (UIButton *)plusBtn {
    if (!_plusBtn) {
        _plusBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_plusBtn setBackgroundColor:[UIColor clearColor]];
        [_plusBtn setImage:kImageWithName(@"common_plus") forState:UIControlStateNormal];
        [_plusBtn addTarget:self action:@selector(plusAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _plusBtn;
}


@end
