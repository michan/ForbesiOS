//
//  FBSButtonCell.m
//  Forbes
//
//  Created by 周灿华 on 2019/8/10.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSButtonCell.h"

@implementation FBSButtonItem

- (instancetype)init {
    self = [super init];
    if (self) {
        self.buttonTextColor = COLOR_TitleBlack;
        self.buttonBgColor = Hexcolor(0xD8D8D8);
    }
    return self;
}

@end




@interface FBSButtonCell ()
@property (nonatomic, strong) UIButton *button;

@end

@implementation FBSButtonCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        [self.contentView addSubview:self.button];
        [self.button mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(WScale(15));
            make.right.mas_equalTo(-WScale(15));
            make.height.mas_equalTo(WScale(40));
            make.bottom.mas_equalTo(-WScale(15) - kDiffTabBarH);
        }];
    }
    return self;
}


- (void)setItem:(FBSButtonItem *)item {
    if (![item isKindOfClass:[FBSButtonItem class]]) {
        return ;
    }
    
    [super setItem:item];
    
    [self.button setBackgroundColor:item.buttonBgColor];
    [self.button setTitleColor:item.buttonTextColor forState:UIControlStateNormal];
    [self.button setTitle:item.buttonText forState:UIControlStateNormal];
}


#pragma mark - action

- (void)clickButton {
    if (self.delegate && [self.delegate respondsToSelector:@selector(fbsCell:didClickButtonAtIndex:)]) {
        [self.delegate fbsCell:self didClickButtonAtIndex:0];
    }
}


#pragma mark - property

- (UIButton *)button {
    if (!_button) {
        _button  = [UIButton buttonWithType:UIButtonTypeCustom];
        _button.backgroundColor = Hexcolor(0xD8D8D8);
        _button.titleLabel.font = kLabelFontSize15;
        [_button setTitleColor:COLOR_TitleBlack forState:UIControlStateNormal];
        [_button addTarget:self action:@selector(clickButton) forControlEvents:UIControlEventTouchUpInside];
    }
    return _button;
    
}


@end
