//
//  FBSButtonCell.h
//  Forbes
//
//  Created by 周灿华 on 2019/8/10.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSCell.h"

@interface FBSButtonItem : FBSItem
@property (nonatomic, copy) NSString *buttonText;
@property (nonatomic, strong) UIColor *buttonTextColor;
@property (nonatomic, strong) UIColor *buttonBgColor;
@end


@interface FBSButtonCell : FBSCell

@end

