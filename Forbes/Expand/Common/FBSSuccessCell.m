
//
//  FBSSuccessCell.m
//  Forbes
//
//  Created by 周灿华 on 2019/8/11.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSSuccessCell.h"

#define EdgeMargin WScale(15)
#define ImageH     WScale(200)

@implementation FBSSuccessItem

- (instancetype)init {
    self = [super init];
    if (self) {
        self.icon = @"contribute_success";
        self.bottomLineHidden = YES;
    }
    return self;
}

@end


@interface FBSSuccessCell ()
@property (nonatomic, strong) UIImageView *iconV;
@property (nonatomic, strong) UILabel *titleL;
@property (nonatomic, strong) UILabel *contentL;
@end

@implementation FBSSuccessCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self.contentView addSubview:self.iconV];
        [self.contentView addSubview:self.titleL];
        [self.contentView addSubview:self.contentL];
        
        [self.iconV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(WScale(78));
            make.centerX.mas_equalTo(0);
            make.width.mas_equalTo(WScale(76));
            make.width.mas_equalTo(WScale(76));
        }];
        
        [self.titleL mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.iconV.mas_bottom).offset(WScale(15));
            make.centerX.mas_equalTo(0);
            make.height.mas_equalTo(WScale(18));
        }];

        [self.contentL mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.titleL.mas_bottom).offset(WScale(15));
            make.centerX.mas_equalTo(0);
            make.height.mas_equalTo(WScale(13));
        }];
 
    }
    
    return self;
}



- (void)setItem:(FBSSuccessItem *)item {
    if (![item isKindOfClass:[FBSSuccessItem class]]) {
        return ;
    }
    
    [super setItem:item];
    self.iconV.image = kImageWithName(item.icon);
    self.titleL.text = item.title;
    self.contentL.text = item.tip;
}

#pragma mark - property

- (UIImageView *)iconV {
    if (!_iconV) {
        _iconV  = [[UIImageView alloc] init];
    }
    return _iconV;
}


- (UILabel *)titleL {
    if (!_titleL) {
        _titleL  = [[UILabel alloc] init];
        _titleL.textColor = MainThemeColor;
        _titleL.font = kLabelBoldFontSize(18);
        _titleL.text = @"";
    }
    return _titleL;
}


- (UILabel *)contentL {
    if (!_contentL) {
        _contentL  = [[UILabel alloc] init];
        _contentL.textColor = Hexcolor(0x999999);
        _contentL.font = kLabelFontSize13;
        _contentL.text = @"";
    }
    return _contentL;
}

@end
