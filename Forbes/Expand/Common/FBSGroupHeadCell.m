//
//  FBSGroupHeadCell.m
//  Forbes
//
//  Created by 周灿华 on 2019/9/25.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSGroupHeadCell.h"

@implementation FBSGroupHeadItem

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.backgroundColor = Hexcolor(0xF6F7F9);
        self.cellHeight = WScale(50);
    }
    return self;
}

@end


@interface FBSGroupHeadCell ()
@property (nonatomic, strong) UIView *wrapView;
@property (nonatomic, strong) UILabel *titleL;
@property (nonatomic, strong) UIImageView *imgV;

@property (nonatomic, strong) UILabel *rightLabel;
@property (nonatomic, strong) UIButton *baoming;
@end

@implementation FBSGroupHeadCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self.contentView addSubview:self.wrapView];
        [self.wrapView addSubview:self.titleL];
        [self.wrapView addSubview:self.imgV];
        [self.wrapView addSubview:self.baoming];
        [self.wrapView addSubview:self.rightLabel];
        [self.wrapView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.mas_offset(0);
            make.height.mas_equalTo(WScale(40));
            make.bottom.mas_equalTo(self.bottomLine.mas_top);
        }];

        [self.imgV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.mas_equalTo(0);
            make.width.mas_equalTo(self.titleL.mas_width).multipliedBy(1.2);
            make.left.mas_equalTo(10);
            make.height.mas_equalTo(2);
        }];
        
        [self.titleL mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.mas_equalTo(self.imgV.mas_top).offset(-WScale(10));
            make.centerX.mas_equalTo(self.imgV);
            make.height.mas_equalTo(WScale(15));
        }];
        
        [self.rightLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.imgV.left).offset(WScale(312));
            make.centerY.mas_equalTo(self.titleL);
            make.height.mas_equalTo(WScale(20));
            make.width.mas_equalTo(WScale(36));

        }];
        [self.baoming mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.imgV.left).offset(WScale(348));
            make.centerY.mas_equalTo(self.titleL);
            make.height.mas_equalTo(WScale(20));
            make.width.mas_equalTo(WScale(20));
            
        }];
    }
    
    return self;
}



- (void)setItem:(FBSGroupHeadItem *)item {
    if (![item isKindOfClass:[FBSGroupHeadItem class]]) {
        return ;
    }
    
    [super setItem:item];
    self.titleL.text = item.title;
    self.imgV.hidden = item.isShowRight;
    self.baoming.hidden = !item.isShowRight;
    self.rightLabel.hidden = !item.isShowRight;


}

#pragma mark - property

- (UILabel *)rightLabel
{
    if (!_rightLabel) {
        _rightLabel = [[UILabel alloc] init];
        _rightLabel.textColor = Hexcolor(0x999999);
        _rightLabel.font = kLabelFontSize10;
        _rightLabel.text = @"换一组";
        _rightLabel.hidden = YES;
    }
    return _rightLabel;
}
- (UIButton *)baoming{
    if (!_baoming) {
        _baoming = [UIButton buttonWithType:UIButtonTypeCustom];
        _baoming.hidden = YES;
        [_baoming setImage:[UIImage imageNamed:@"Path"] forState:UIControlStateNormal];
        [_baoming addTarget:self action:@selector(clickTo) forControlEvents:UIControlEventTouchUpInside];
    }
    return _baoming;
}
- (void)clickTo{
    if ([self.delegate respondsToSelector:@selector(fbsCell:didClickButtonAtIndex:)]) {
        [self.delegate fbsCell:self didClickButtonAtIndex:0];
    }
}
- (UIView *)wrapView {
    if (!_wrapView) {
        _wrapView  = [[UIView alloc] init];
        _wrapView.backgroundColor = [UIColor whiteColor];
    }
    return _wrapView;
}


- (UILabel *)titleL {
    if (!_titleL) {
        _titleL  = [[UILabel alloc] init];
        _titleL.textColor = Hexcolor(0x333333);
        _titleL.font = kLabelBoldFontSize(15);
        _titleL.text = @"";
    }
    return _titleL;
}


- (UIImageView *)imgV {
    if (!_imgV) {
        _imgV  = [[UIImageView alloc] init];
        _imgV.backgroundColor = MainThemeColor;
        _imgV.layer.cornerRadius = 1.0;
//        _imgV.image = kImageWithName(@"home_seperator");
    }
    return _imgV;
}


@end
