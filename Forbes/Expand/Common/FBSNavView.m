//
//  FBSNavView.m
//  Forbes
//
//  Created by 周灿华 on 2019/9/25.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSNavView.h"

@interface FBSNavView ()
@property (nonatomic, strong) UILabel *searchL;
@end

@implementation FBSNavView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = Hexcolor(0x191919);
        UIImageView *logoView = [[UIImageView alloc] init];
        logoView.image = kImageWithName(@"home_logo");
        [self addSubview:logoView];
        
        [logoView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_offset(15);
            make.bottom.mas_offset(-10);
            make.width.mas_equalTo(60);
            make.height.mas_equalTo(20);
        }];
        
        
        UIButton *liveBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [liveBtn setImage:kImageWithName(@"home_icon_zhibo_nor") forState:UIControlStateNormal];
        [liveBtn addTarget:self action:@selector(clickLive) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:liveBtn];
//        liveBtn.hidden = YES;
        
        [liveBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_offset(-15);
            make.centerY.mas_equalTo(logoView);
//            make.width.mas_equalTo(8);
            
            make.width.mas_equalTo(60);
            make.height.mas_equalTo(20);
        }];
        
        UIControl *searchControl = [[UIControl alloc] init];
        searchControl.backgroundColor = Hexcolor(0x333333);
        searchControl.layer.cornerRadius = 5;
        [self addSubview:searchControl];
        [searchControl addTarget:self action:@selector(clickSearch) forControlEvents:UIControlEventTouchUpInside];
        
        [searchControl mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(logoView.mas_right).offset(10);
            make.right.mas_equalTo(liveBtn.mas_left).offset(-10);
            make.bottom.mas_equalTo(-5);
            make.height.mas_equalTo(30);
        }];
        
        
        
        UIImageView *searchImg = [[UIImageView alloc] init];
        searchImg.image = kImageWithName(@"home_search_icon_nor");
        [searchControl addSubview:searchImg];
        
        [searchImg mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_offset(7);
            make.centerY.mas_offset(0);
            make.width.mas_equalTo(16);
            make.height.mas_equalTo(16);
        }];

        
        self.searchL = [[UILabel alloc] init];
        self.searchL.textColor = [[UIColor whiteColor] colorWithAlphaComponent:0.6];
        self.searchL.font = [UIFont systemFontOfSize:14];
        [searchControl addSubview:self.searchL];
        
        [self.searchL mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(searchImg.mas_right).offset(8);
            make.centerY.mas_equalTo(0);
        }];
        


    }
    return self;
}



- (void)clickLive {
    if (self.liveAction) {
        self.liveAction();
    }
}


- (void)clickSearch {
    if (self.searchAction) {
        self.searchAction();
    }
}


- (void)setPlaceholder:(NSString *)placeholder {
    _placeholder = placeholder;
    self.searchL.text = placeholder;
}

@end
