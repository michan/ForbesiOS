//
//  FBSNavView.h
//  Forbes
//
//  Created by 周灿华 on 2019/9/25.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FBSNavView : UIView
@property (nonatomic, copy) NSString *placeholder;
@property (nonatomic, strong) void(^liveAction)(void);
@property (nonatomic, strong) void(^searchAction)(void);
@end

NS_ASSUME_NONNULL_END
