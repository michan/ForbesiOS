//
//  CommonTool.h
//  Forbes
//
//  Created by 周灿华 on 2019/7/20.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 获取当前app版本号
 */
CC_STATIC_INLINE NSString* getAppCurrentVersion()
{
    static NSString *version = nil;
    if (version)
    {
        return version;
    }
    version = [[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"] copy];
    if (version)
    {
        return version;
    }
    return @"unknow";
}

CC_STATIC_INLINE NSString* getAppCurrentBundleID()
{
    static NSString *bundleID = nil;
    if (bundleID)
    {
        return bundleID;
    }
    bundleID = [[[NSBundle mainBundle] bundleIdentifier] copy];
    if (bundleID)
    {
        return bundleID;
    }
    return @"unknow";
}


CC_STATIC_INLINE NSString * safetyString(NSString *ori, NSString *placeholder)
{
    if(![ori isKindOfClass:[NSString class]]) {
        return (placeholder && placeholder.length) ? placeholder : @"";
    }
    
    if(ori && ori.length) {
        return ori;
    } else {
        return (placeholder && placeholder.length) ? placeholder : @"";
    }
}



@interface CommonTool : NSObject

+ (BOOL)isPhoneFilter:(NSString *)mobile;

+ (UIImage *)createImageWithColor:(UIColor *)color;

+ (UIImage*)imageWithImage:(UIImage*)image scaledToSize:(CGSize)newSize;


#pragma mark 生成image
+ (UIImage *)makeImageWithView:(UIView *)view withSize:(CGSize)size;

//生成专题图片
+ (UIImage *)makeTopicImage;

@end
