//
//  HLWJShareSDKManager.h
//  ShareSDKDemo
//
//  Created by 江霆锋 on 2017/12/12.
//  Copyright © 2017年 tingfeng. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ShareSDK/ShareSDK.h>

@interface HLWJShareSDKManager : NSObject

/** 单例 */
+(instancetype)sharedInstance;

/** 启动时调用 */
-(void)didFinishLaunchingWithOptions:(NSDictionary*)launchOptions;

/**
 分享菜单弹窗 点击事件

 @param title 标题
 @param iconArray 分享图标 可以传数组 传nil为默认HLWJ图标
 @param content 内容
 @param url url
 @param success 成功handler
 @param fail 失败handler
 */
-(void)shareWithTitle:(NSString *)title
            iconArray:(NSArray <UIImage *>*)iconArray
              content:(NSString *)content
                  url:(NSString *)url
       successHandler:(void(^)(void))success
          failHandler:(void(^)(void))fail;

/**
 分享事件 - 无UI
 
 @param title 标题
 @param iconArray 分享图标 可以传数组 传nil为默认HLWJ图标
 @param content 内容
 @param url url
 @param platformType 平台类型
 /--SSDKPlatformSubTypeWechatSession微信好友 SSDKPlatformSubTypeWechatTimeline微信朋友圈--/
 @param success 成功handler
 @param fail 失败handler
 */
-(void)shareWithOutMenuTitle:(NSString *)title
            iconArray:(NSArray <UIImage *>*)iconArray
              content:(NSString *)content
                  url:(NSString *)url
                platformType:(enum SSDKPlatformType)platformType
       successHandler:(void(^)(void))success
          failHandler:(void(^)(void))fail;

/**
 分享到小程序 - 无UI弹窗
 
 @param title 标题,必填
 @param path 小程序路径,必填
 @param thumbImage 缩略图,必填
 @param userName 小程序id
 @param webpageUrl 网址（6.5.6以下版本微信会自动转化为分享链接 必填）
 @param success 成功handler
 @param fail 失败handler
 */
-(void)shareMiniProgramWithTitle:(NSString *)title
                            path:(NSString*)path
                      thumbImage:(id)thumbImage
                        userName:(NSString*)userName
                      webpageUrl:(NSString*)webpageUrl
                  successHandler:(void(^)(void))success
                     failHandler:(void(^)(void))fail;

/**
 第三方登录 - 微信
 
 @param success 成功回调
 @param fail 失败回调
 @param cancel 取消回调
 */
- (void)loginWXWithSuccessHandler:(void(^)(SSDKUser *user))success
                      failHandler:(void(^)(NSString *errorTip))fail
                    cancelHandler:(void(^)(void))cancel;


/**
 取消第三方登录授权
 写死取消微信授权了  以后再改成传平台号吧
 SSDKPlatformTypeWechat
 */
- (void)cancelAuthorizeWX;

@end
