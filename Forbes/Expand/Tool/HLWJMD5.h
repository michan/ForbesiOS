//
//  HLWJMD5.h
//  HLWJ
//
//  Created by 江霆锋 on 2017/10/24.
//  Copyright © 2017年 tingfeng. All rights reserved.
//

#import <Foundation/Foundation.h>
@interface HLWJMD5 : NSObject

/**
 32位小写
 */
+(NSString *)MD5ForLower32Bate:(NSString *)str;

/**
 32位大写
 */
+(NSString *)MD5ForUpper32Bate:(NSString *)str;

/**
 16位小写
 */
+(NSString *)MD5ForUpper16Bate:(NSString *)str;

/**
 16位大写
 */
+(NSString *)MD5ForLower16Bate:(NSString *)str;
@end
