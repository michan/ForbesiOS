//
//  DGAlertView.h
//  iOS_TongFuBao_Mall
//
//  Created by MC on 2018/8/31.
//  Copyright © 2018年 Tongfubao. All rights reserved.
//

#import <UIKit/UIKit.h>
//typedef  void (^SelectButtonIndex)(NSInteger selectIndex);

@interface DGAlertView : UIView
@property(nonatomic,assign) void(^SelectButtonIndex)(NSInteger selectButtonIndex);

-(void)initWithTitle:(NSString*)title Message:(NSString*)message CancelButtonTitle:(NSString*)cancelButtonTitle OKButtonTitles:(NSString*)oKButtonTitles ButtonIndex:(void(^)(NSInteger selectButtonIndex))SelectButtonIndex;

@end
