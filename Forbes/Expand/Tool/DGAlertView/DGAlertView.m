//
//  DGAlertView.m
//  iOS_TongFuBao_Mall
//
//  Created by MC on 2018/8/31.
//  Copyright © 2018年 Tongfubao. All rights reserved.
//

#import "DGAlertView.h"
#import "MCIucencyView.h"
#import "AppDelegate.h"
@interface DGAlertView ()<UITextViewDelegate>
{
    MCIucencyView *_bgView;

    UITextView * _TextView;
    
    
    
}
@end

@implementation DGAlertView

-(void)initWithTitle:(NSString*)title Message:(NSString*)message CancelButtonTitle:(NSString*)cancelButtonTitle OKButtonTitles:(NSString*)oKButtonTitles ButtonIndex:(void(^)(NSInteger selectButtonIndex))SelectButtonIndex {
  
    
  [super initWithFrame:CGRectMake(0, 0, Main_Screen_Width,Main_Screen_Height)];
    
    if (self) {
        self.userInteractionEnabled = YES;

        self.SelectButtonIndex =SelectButtonIndex;
        
        _bgView = [[MCIucencyView alloc]initWithFrame:CGRectMake(0, 0, Main_Screen_Width, Main_Screen_Height)];
        [_bgView setBgViewColor:[UIColor blackColor]];
        [_bgView setBgViewAlpha:.5];
        [self addSubview:_bgView];

        
        
        
        
        
        
        CGFloat bx = 130/2;
        CGFloat bw = Main_Screen_Width - bx*2;
        CGFloat bh = 0;
        CGFloat by = 0;

        UIView * bgview = [[UIView alloc]initWithFrame:CGRectMake(bx, by, bw, bh)];
        
        bgview.backgroundColor = [UIColor whiteColor];
        ViewRadius(bgview, 5);
        
        [_bgView addSubview:bgview];
        
        
        
        
        
        
        
        CGFloat th = 0;
        CGFloat ty = 20;
        CGFloat tx = 5;
        CGFloat tw = bw - 10;
        
        
        if (title.length) {
            th = 40;
            UILabel * titleLbl =[[UILabel alloc]initWithFrame:CGRectMake(tx, ty, tw, th)];
            titleLbl.text = title;
            titleLbl.font = [UIFont boldSystemFontOfSize:15];
            titleLbl.textColor = [UIColor blackColor];
            titleLbl.textAlignment = NSTextAlignmentCenter;
            [bgview addSubview:titleLbl];
            
        }
        else
        {
            ty = 40;
        }
        
        CGFloat sy = ty + th;
        CGFloat sx = 10;
        CGFloat sw = bw - sx*2;
        
       
        CGFloat sh = [self heightforString:message andWidth:sw fontSize:14] + 10;
//
        CGFloat maxh =  Main_Screen_Height - 80-(20 +40 +20 + 50);
        
        
        if (sh>maxh) {
            sh = maxh;
        }
        
        if (!message.length) {
            sh = 0;
            
        }
        else
        {
            _TextView =[[UITextView alloc]initWithFrame:CGRectMake(sx, sy, sw, sh)];
            _TextView.font = [UIFont systemFontOfSize:14];
//            _TextView.backgroundColor = [UIColor yellowColor];
            _TextView.textColor = [UIColor grayColor];
            _TextView.textAlignment = NSTextAlignmentCenter;
            [bgview addSubview:_TextView];
//            _TextView.userInteractionEnabled = NO;
            _TextView.delegate = self;
            _TextView.text =message ;

        }
        
        CGFloat bty = sy + sh + 20;
        if (!title.length) {
            bty = sy + sh + 40;
        }
        CGFloat btx = 0;
        CGFloat btw = bw/2;
        CGFloat bth = 50;

        bh =bty +bth;
        by = (Main_Screen_Height -bh)/2;
       
        
        bgview .mj_y =by;
        bgview .mj_h =bh;
        
        [self show];
        
        
        if (!oKButtonTitles.length && cancelButtonTitle.length) {
            btw = bw;
            
            UIButton *backBtn = [[UIButton alloc]initWithFrame:CGRectMake(btx, bty, btw, bth)];
            backBtn.backgroundColor = [UIColor blackColor];
            [backBtn setTitleColor:[UIColor whiteColor] forState:0];
            
            backBtn.titleLabel.font = [UIFont boldSystemFontOfSize:14];
            [backBtn setTitle:cancelButtonTitle forState:0];
            [bgview addSubview:backBtn];
            backBtn.tag = 9109890;
            [backBtn addTarget:self action:@selector(actionSelectButtonIndex:) forControlEvents:1<<6];
            return;
        }
        if (!cancelButtonTitle.length && oKButtonTitles.length) {
            btw = bw;
            
            UIButton *okBtn = [[UIButton alloc]initWithFrame:CGRectMake(btx, bty, btw, bth)];
            okBtn.backgroundColor = [UIColor blackColor];
            okBtn.titleLabel.font = [UIFont boldSystemFontOfSize:14];
            [okBtn setTitle:oKButtonTitles forState:0];
            [okBtn setTitleColor:[UIColor whiteColor] forState:0];
            [bgview addSubview:okBtn];
            okBtn.tag = 9109891;
            [okBtn addTarget:self action:@selector(actionSelectButtonIndex:) forControlEvents:1<<6];

            return;
        }
        if (cancelButtonTitle.length && oKButtonTitles.length) {
            UIButton *backBtn = [[UIButton alloc]initWithFrame:CGRectMake(btx, bty, btw, bth)];
            [backBtn setTitleColor:[UIColor grayColor] forState:0];
            backBtn.titleLabel.font = [UIFont boldSystemFontOfSize:14];
            [backBtn setTitle:cancelButtonTitle forState:0];
            
            backBtn.backgroundColor = [UIColor whiteColor];
            [bgview addSubview:backBtn];
            backBtn.tag = 9109890;
            [backBtn addTarget:self action:@selector(actionSelectButtonIndex:) forControlEvents:1<<6];

            btx = bw/2;
            
            UIButton *okBtn = [[UIButton alloc]initWithFrame:CGRectMake(btx, bty, btw, bth)];
            okBtn.backgroundColor = [UIColor blackColor];
            [okBtn setTitleColor:[UIColor whiteColor] forState:0];
            okBtn.titleLabel.font = [UIFont boldSystemFontOfSize:14];
            [okBtn setTitle:oKButtonTitles forState:0];
            [bgview addSubview:okBtn];
            okBtn.tag = 9109891;
            [okBtn addTarget:self action:@selector(actionSelectButtonIndex:) forControlEvents:1<<6];

            
        }
        
        
        
        

    }

}
- (CGFloat) heightforString:(NSString *)value andHeight:(CGFloat)height fontSize:(CGFloat)fontSize{
    if (!value.length) {
        value=@" ";
    }

    return [value boundingRectWithSize:CGSizeMake(100000, height) options:NSStringDrawingUsesLineFragmentOrigin attributes:[NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:fontSize] forKey:NSFontAttributeName] context:nil].size.width;
    
}
- (CGFloat) heightforString:(NSString *)value andWidth:(CGFloat)width fontSize:(CGFloat)fontSize{
    if (!value.length) {
        value=@" ";
    }
    return [value boundingRectWithSize:CGSizeMake(width, 1000000) options:NSStringDrawingUsesLineFragmentOrigin attributes:[NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:fontSize] forKey:NSFontAttributeName] context:nil].size.height;
    
}
-(void)show
{
    AppDelegate *app = (AppDelegate *)[UIApplication sharedApplication].delegate;
    
    [app.window addSubview:self];

}
-(void)actionSelectButtonIndex:(UIButton*)btn{
    
    NSInteger index =btn.tag -9109890;
    if (self.SelectButtonIndex) {
        self.SelectButtonIndex(index);
        [self removeFromSuperview];
    }
    
}
-(void)setSelectButtonIndex:(void (^)(NSInteger))SelectButtonIndex{
    
    objc_setAssociatedObject(self, @selector(SelectButtonIndex), SelectButtonIndex, OBJC_ASSOCIATION_COPY_NONATOMIC);
    
}
- (void(^)(NSInteger selectButtonIndex))SelectButtonIndex

{
    
    return objc_getAssociatedObject(self, @selector(SelectButtonIndex));
    
}
-(BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    if (textView == _TextView) {
        return NO;

    }
    return YES;

}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
