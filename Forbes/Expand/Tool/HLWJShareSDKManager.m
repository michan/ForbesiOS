//
//  HLWJShareSDKManager.m
//  ShareSDKDemo
//
//  Created by 江霆锋 on 2017/12/12.
//  Copyright © 2017年 tingfeng. All rights reserved.
//

#import "HLWJShareSDKManager.h"
#import <ShareSDKConnector/ShareSDKConnector.h>
#import <ShareSDKUI/ShareSDK+SSUI.h>
// 自定义分享菜单栏需要导入的头文件
#import <ShareSDKUI/SSUIShareActionSheetStyle.h>
#import <ShareSDKUI/SSUIEditorViewStyle.h>
//微信SDK头文件
#import "WXApi.h"
#import <TencentOpenAPI/QQApiInterface.h>
#import <TencentOpenAPI/TencentOAuth.h>
#import "WeiboSDK.h"

#define WXAppId     @"wx6f7215b4fa63caf0"
#define WXAppSecret @"2e5e39b9d4b6e15d8d534695c6174c73"
#define AppName     @"福布斯中文版"


@implementation HLWJShareSDKManager
#pragma mark - public
+(instancetype)sharedInstance
{
    static HLWJShareSDKManager *manager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[[self class] alloc] init];
    });
    return manager;
}

- (void)didFinishLaunchingWithOptions:(NSDictionary*)launchOptions{
//    /**初始化ShareSDK应用
//     @param activePlatforms
//     使用的分享平台集合
//     @param importHandler (onImport)
//     导入回调处理，当某个平台的功能需要依赖原平台提供的SDK支持时，需要在此方法中对原平台SDK进行导入操作
//     @param configurationHandler (onConfiguration)
//     配置回调处理，在此方法中根据设置的platformType来填充应用配置信息
//     */
//    [ShareSDK registerActivePlatforms:@[
//                                        @(SSDKPlatformTypeSinaWeibo),
////                                        @(SSDKPlatformTypeMail),
////                                        @(SSDKPlatformTypeSMS),
////                                        @(SSDKPlatformTypeCopy),
////                                        @(SSDKPlatformTypeWechat),//微信总平台  会带收藏
//                                        @(SSDKPlatformSubTypeWechatSession),
//                                        @(SSDKPlatformSubTypeWechatTimeline),
////                                        @(SSDKPlatformTypeQQ),
//                                        ]
//                             onImport:^(SSDKPlatformType platformType)
//     {
//         switch (platformType)
//         {
//             case SSDKPlatformTypeWechat:
//                 [ShareSDKConnector connectWeChat:[WXApi class]];
//                 break;
//             case SSDKPlatformTypeQQ:
//                 [ShareSDKConnector connectQQ:[QQApiInterface class] tencentOAuthClass:[TencentOAuth class]];
//                 break;
//             case SSDKPlatformTypeSinaWeibo:
//                 [ShareSDKConnector connectWeibo:[WeiboSDK class]];
//                 break;
////             case SSDKPlatformTypeRenren:
////                 [ShareSDKConnector connectRenren:[RennClient class]];
////                 break;
//             default:
//                 break;
//         }
//     }
//                      onConfiguration:^(SSDKPlatformType platformType, NSMutableDictionary *appInfo)
//     {
//
//         switch (platformType)
//         {
//             case SSDKPlatformTypeSinaWeibo:
//                 //设置新浪微博应用信息,其中authType设置为使用SSO＋Web形式授权
////                 [appInfo SSDKSetupSinaWeiboByAppKey:@"568898243"
////                                           appSecret:@"38a4f8204cc784f81f9f0daaf31e02e3"
////                                         redirectUri:@"http://www.sharesdk.cn"
////                                            authType:SSDKAuthTypeBoth];
//                 break;
//             case SSDKPlatformTypeWechat:
//                 [appInfo SSDKSetupWeChatByAppId:WXAppId
//                                       appSecret:WXAppSecret];
//                 break;
//             case SSDKPlatformTypeQQ:
////                 [appInfo SSDKSetupQQByAppId:@"100371282"
////                                      appKey:@"aed9b0303e3ed1e27bae87c33761161d"
////                                    authType:SSDKAuthTypeBoth];
//                 break;
//
//            default:
//                   break;
//            }
//    }];
    
    [ShareSDK registPlatforms:^(SSDKRegister *platformsRegister) {
        //QQ
        [platformsRegister setupQQWithAppId:@"101807124" appkey:@"7bd702f781abad0883456f9f74605327"];
        
        //        //微信 （4.3.3以下版本初始化）
        //        [platformsRegister setupWeChatWithAppId:@"wx617c77c82218ea2c" appSecret:@"c7253e5289986cf4c4c74d1ccc185fb1"];
    NSString * ss=    [WXApi getApiVersion];
        
        
        //更新到4.3.3或者以上版本，微信初始化需要使用以下初始化
        [platformsRegister setupWeChatWithAppId:WXAppId appSecret:WXAppSecret universalLink:@"https://forbeschina.app/"];
        
        //新浪
        [platformsRegister setupSinaWeiboWithAppkey:@"1480309785" appSecret:@"cd8cece395471b3ceaf114e5afaec1ce" redirectUrl:@"http://open.weibo.com/apps/1480309785/privilege/oauth"];
    }];
    
    
}

-(void)shareMiniProgramWithTitle:(NSString *)title
                            path:(NSString *)path
                      thumbImage:(id)thumbImage
                        userName:(NSString *)userName
                      webpageUrl:(NSString *)webpageUrl
                  successHandler:(void (^)(void))success
                     failHandler:(void (^)(void))fail
{
    [self setupShareSDKUIStyle];
    NSMutableDictionary *shareParams = [NSMutableDictionary dictionary];
    int miniProgramType = 0;
//    if (CurrentEnvironment == EnvironmentType_Development) {
//        miniProgramType = 2;
//    }else{
//        miniProgramType = 0;
//    }
    
    [shareParams SSDKSetupWeChatMiniProgramShareParamsByTitle:title description:@"" webpageUrl:[NSURL URLWithString:webpageUrl?webpageUrl:@"www.baicu.com"] path:path thumbImage:thumbImage hdThumbImage:nil userName:thumbImage withShareTicket:YES miniProgramType:miniProgramType forPlatformSubType:SSDKPlatformSubTypeWechatSession];
    
    
    [ShareSDK share:SSDKPlatformSubTypeWechatSession parameters:shareParams onStateChanged:^(SSDKResponseState state, NSDictionary *userData, SSDKContentEntity *contentEntity, NSError *error) {
        switch (state) {
            case SSDKResponseStateSuccess:
            {
                if (success) {
                    success();
                }
                break;
            }
                break;
            case SSDKResponseStateFail:
            case SSDKResponseStateCancel:
            {
                if (fail) {
                    fail();
                }
                
            }
                break;
            default:
                break;
                
        }
        
        
        
    }];
}

-(void)shareWithTitle:(NSString *)title
           iconArray:(NSArray <UIImage *>*)iconArray
              content:(NSString *)content
                  url:(NSString *)url
       successHandler:(void(^)(void))success
          failHandler:(void(^)(void))fail{
    
    
    
    [self setupShareSDKUIStyle];
    //1、创建分享参数
    if (!iconArray) {
        iconArray = @[[UIImage imageNamed:@"applogo"]];
    }
    if (iconArray) {
        
        
        
        
        NSMutableDictionary *shareParams = [NSMutableDictionary dictionary];
      
        [shareParams SSDKSetupShareParamsByText:content
                                         images:iconArray
                                            url:[NSURL URLWithString:url]
                                          title:title
                                           type:SSDKContentTypeAuto];
        
     
        //2、分享（可以弹出我们的分享菜单和编辑界面）
        [ShareSDK showShareActionSheet:nil 
                                 items:nil
                           shareParams:shareParams
                   onShareStateChanged:^(SSDKResponseState state, SSDKPlatformType platformType, NSDictionary *userData, SSDKContentEntity *contentEntity, NSError *error, BOOL end) {
                       
                       switch (state) {
//                           case SSDKResponseStateBegin:{
//                               if (platformType == SSDKPlatformSubTypeWechatSession) {
//                                   [shareParams SSDKSetupWeChatMiniProgramShareParamsByTitle:title description:@"" webpageUrl:[NSURL URLWithString:url] path:content thumbImage:[iconArray firstObject] userName:GH_HLWJ withShareTicket:YES miniProgramType:0 forPlatformSubType:SSDKPlatformSubTypeWechatSession];
//                               }else if (platformType == SSDKPlatformSubTypeWechatTimeline)
//                               {
//                                   [shareParams SSDKSetupShareParamsByText:content
//                                                                    images:iconArray
//                                                                       url:[NSURL URLWithString:url]
//                                                                     title:title
//                                                                      type:SSDKContentTypeAuto];
//
//                               }
//                               break;
//                           }
                           case SSDKResponseStateSuccess:
                           {
                               if (platformType == SSDKPlatformSubTypeWechatFav) {
//                                   [HLWJToast showWithMessageByShake:@"收藏成功"];
                               }else{
                                   if (success) {
                                       success();
                                   }
                               }
                               
                           }
                           case SSDKResponseStateFail:
                           {
                               if (platformType == SSDKPlatformSubTypeWechatFav) {
//                                   [HLWJToast showWithMessageByShake:@"收藏失败"];
                               }else{
                                   if (fail) {
                                       fail();
                                   }
                               }
                               break;
                           }
                           default:
                               break;
                       }
            }];
    }
}

-(void)shareWithOutMenuTitle:(NSString *)title
                   iconArray:(NSArray <UIImage *>*)iconArray
                     content:(NSString *)content
                         url:(NSString *)url
                platformType:(enum SSDKPlatformType)platformType
              successHandler:(void(^)(void))success
                 failHandler:(void(^)(void))fail{
    
    if (!iconArray) {
        iconArray = @[[UIImage imageNamed:@"login_logo_smarthome"]];
    }
    if (iconArray) {
        
        NSMutableDictionary *shareParams = [NSMutableDictionary dictionary];
        [shareParams SSDKSetupShareParamsByText:content
                                         images:iconArray
                                            url:[NSURL URLWithString:url]
                                          title:title
                                           type:SSDKContentTypeAuto];
        
        [ShareSDK share:platformType parameters:shareParams onStateChanged:^(SSDKResponseState state, NSDictionary *userData, SSDKContentEntity *contentEntity, NSError *error) {
            switch (state) {
                    case SSDKResponseStateSuccess:
                {
                    if (success) {
                        success();
                    }
                    
                    break;
                }
                    case SSDKResponseStateFail:
                {
                    if (fail) {
                        fail();
                    }
                    break;
                }
                default:
                    break;
            }
        }];
    }
}
-(void)loginWXWithSuccessHandler:(void(^)(SSDKUser *user))success
                     failHandler:(void(^)(NSString *errorTip))fail
                   cancelHandler:(void(^)(void))cancel{
    
    [ShareSDK authorize:SSDKPlatformTypeWechat settings:nil onStateChanged:^(SSDKResponseState state, SSDKUser *user, NSError *error) {
        switch (state) {
            case SSDKResponseStateSuccess:
                if (success) {
                    success(user);
                }
                break;
            case SSDKResponseStateFail:
                if (fail) {
                    fail(error.localizedDescription);
                }
                break;
            case SSDKResponseStateCancel:
                if (cancel) {
                    cancel();
                }
                break;
            default:
                break;
        }
    }]; 
}

- (void)cancelAuthorizeWX{
    
    if ([ShareSDK hasAuthorized:SSDKPlatformTypeWechat]) {
        [ShareSDK cancelAuthorize:SSDKPlatformTypeWechat];
    }
}
#pragma mark - private
-(void)setupShareSDKUIStyle{
    // 设置分享菜单的背景颜色
    [SSUIShareActionSheetStyle setActionSheetBackgroundColor:[Hexcolor(0x000000) colorWithAlphaComponent:0.5]];
    // 设置分享菜单颜色
    [SSUIShareActionSheetStyle setActionSheetColor:Hexcolor(0xffffff)];
    // 设置分享菜单－取消按钮背景颜色
    //    [SSUIShareActionSheetStyle setCancelButtonBackgroundColor:[UIColor colorWithRed:21.0/255.0 green:21.0/255.0 blue:21.0/255.0 alpha:1.0]];
    // 设置分享菜单－取消按钮的文本颜色
    //    [SSUIShareActionSheetStyle setCancelButtonLabelColor:[UIColor blackColor]];
    //设置分享菜单－社交平台文本颜色
    [SSUIShareActionSheetStyle setItemNameColor:Hexcolor(969696)];
    // 设置分享菜单－社交平台文本字体
    [SSUIShareActionSheetStyle setItemNameFont:[UIFont systemFontOfSize:12]];
    //设置分享编辑界面的导航栏颜色
    [SSUIEditorViewStyle setiPhoneNavigationBarBackgroundColor:[UIColor clearColor]];
    //设置编辑界面标题颜色
    [SSUIEditorViewStyle setTitleColor:[UIColor clearColor]];
    //设置取消发布标签文本颜色
    [SSUIEditorViewStyle setCancelButtonLabelColor:[UIColor blueColor]];
    [SSUIEditorViewStyle setShareButtonLabelColor:[UIColor blueColor]];
    //设置分享编辑界面状态栏风格
    [SSUIEditorViewStyle setStatusBarStyle:UIStatusBarStyleLightContent];
    //设置简单分享菜单样式
    [SSUIShareActionSheetStyle setShareActionSheetStyle:ShareActionSheetStyleSimple];
    
    [SSUIShareActionSheetStyle isCancelButtomHidden:YES];
}

@end
