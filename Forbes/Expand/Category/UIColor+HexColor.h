//
//  UIColor+HexColor.h
//  WYJ
//
//  Created by coderchou on 2018/4/10.
//  Copyright © 2018年 gdunicom. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (HexColor)

+ (UIColor *)hexColor:(NSString *)hexStr;



@end
