//
//  NSString+Ext.h
//  Forbes
//
//  Created by 周灿华 on 2019/9/8.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSString (Ext)

+ (BOOL)validateContactNumber:(NSString *)mobileNum;

+ (BOOL)validateEmail:(NSString *)email;

+ (BOOL)validatePassword:(NSString *)pwd;
///手机号脱敏
+ (NSString *)filterPhone:(NSString *)phone;

+ (NSString *)getTimestamp:(NSString*)mStr formatter:(NSString *)formatter;

//列表文章时间显示
+ (NSString *)publishedTimeFormatWithTimeStamp:(NSString *)timeStamp;

@end

NS_ASSUME_NONNULL_END
