//
//  UIView+FBSLayer.m
//  Forbes
//
//  Created by 赵志辉 on 2019/9/26.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import "UIView+FBSLayer.h"

@implementation UIView (FBSLayer)

- (void)drawLineOfDashByCAShapeLayer:(UIView *)lineView lineLength:(int)lineLength lineSpacing:(int)lineSpacing lineColor:(UIColor *)lineColor lineDirection:(BOOL)isHorizonal {
    [lineView.layer removeAllSublayers];
    CAShapeLayer *shapeLayer = [CAShapeLayer layer];
    
    [shapeLayer setBounds:lineView.bounds];
    
    if (isHorizonal) {
        
        [shapeLayer setPosition:CGPointMake(CGRectGetWidth(lineView.frame) / 2, CGRectGetHeight(lineView.frame))];
        
    } else{
        [shapeLayer setPosition:CGPointMake(CGRectGetWidth(lineView.frame) / 2, CGRectGetHeight(lineView.frame)/2)];
    }
    
    [shapeLayer setFillColor:[UIColor clearColor].CGColor];
    //  设置虚线颜色为blackColor
    [shapeLayer setStrokeColor:lineColor.CGColor];
    //  设置虚线宽度
    if (isHorizonal) {
        [shapeLayer setLineWidth:WScale(1)];
    } else {
        
        [shapeLayer setLineWidth:WScale(1)];
    }
    [shapeLayer setLineJoin:kCALineJoinRound];
    //  设置线宽，线间距
    [shapeLayer setLineDashPattern:[NSArray arrayWithObjects:[NSNumber numberWithInt:lineLength], [NSNumber numberWithInt:lineSpacing], nil]];
    //  设置路径
    CGMutablePathRef path = CGPathCreateMutable();
    
    if (isHorizonal) {
        CGPathMoveToPoint(path, NULL, 0, 0 + CGRectGetHeight(lineView.frame)/2);
        CGPathAddLineToPoint(path, NULL,CGRectGetWidth(lineView.frame), 0 + CGRectGetHeight(lineView.frame)/2);
    } else {
        CGPathMoveToPoint(path, NULL, 0 + CGRectGetWidth(lineView.frame)/2, 0);
        CGPathAddLineToPoint(path, NULL, 0 + CGRectGetHeight(lineView.frame)/2, CGRectGetHeight(lineView.frame));
    }
    
    [shapeLayer setPath:path];
    CGPathRelease(path);
    //  把绘制好的虚线添加上来
    [lineView.layer addSublayer:shapeLayer];
}
@end
