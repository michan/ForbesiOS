//
//  UITableView+Refresh.m
//  Forbes
//
//  Created by 赵志辉 on 2019/9/18.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import "FBSTableViewController+Refresh.h"


@implementation FBSTableViewController (Refresh)

#pragma mark --关联对象----------------
- (void)setPageCount:(NSNumber *)pageCount
{
    objc_setAssociatedObject(self, @selector(setPageCount:), pageCount, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    self.viewModel.limit = [pageCount integerValue];
}

- (NSNumber *)pageCount
{
    return objc_getAssociatedObject(self, @selector(setPageCount:));
}
- (void)setPage:(NSNumber *)page
{
    objc_setAssociatedObject(self, @selector(setPage:), page, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    self.viewModel.page = [page integerValue];
}

- (NSNumber *)page
{
    return objc_getAssociatedObject(self, @selector(setPage:));
}



- (void)setFooterConfigBlock:(void (^)(NSInteger))footerConfigBlock
{
    objc_setAssociatedObject(self, @selector(setFooterConfigBlock:), footerConfigBlock, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

- (void (^)(NSInteger))footerConfigBlock
{
    return objc_getAssociatedObject(self, @selector(setFooterConfigBlock:));
}


#pragma mark --header刷新----------------

- (void)normalHeaderRefreshingActionBlock:(RefreshActionBlock)refreshBlock
{
    //如果外部调用的时候不设置每页的数据条目数量pageCount。这里设置一个默认值。
    self.pageCount = [self.pageCount integerValue] > 0 ? self.pageCount : @(20);
    WEAKSELF
    //将外部的block给关联对象赋值，如果外部需要内部进行footer和header结束刷新的处理的话，可以传block进来。
    self.footerConfigBlock = ^(NSInteger newCount) {
        [weakSelf handleEndRefreshing:newCount];
    };
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        self.page = @(1);
        refreshBlock(weakSelf.footerConfigBlock);
    }];
    [self beginRefreshing];
}
#pragma mark --footer刷新----------------
- (void)backNormalFooterRefreshingActionBlock:(RefreshActionBlock)footerRefreshBlock
{
    //同理设置一个默认的每页数据条目
    self.pageCount = [self.pageCount integerValue] > 0 ? self.pageCount : @(20);
    WEAKSELF
    self.footerConfigBlock = ^(NSInteger newCount) {
        [weakSelf handleEndRefreshing:newCount];
    };
    
    self.tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        self.page = [NSNumber numberWithInteger:[self.page integerValue] + 1];
        footerRefreshBlock(weakSelf.footerConfigBlock);
    }];
}

- (void)handleEndRefreshing:(NSInteger)count
{
    if (self.tableView.mj_header.isRefreshing) {
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
    }
    
    if (count < [self.pageCount integerValue]) {
        [self.tableView.mj_footer endRefreshingWithNoMoreData];
    }else{
        [self.tableView.mj_footer endRefreshing];
    }
}


#pragma mark --结束刷新-----
//如果外部想自己处理结束刷新，可以为tabelView添加的其它结束刷新的方法。
- (void)beginRefreshing
{
    if (!self.tableView.mj_header) return;
    [self.tableView.mj_header beginRefreshing];
}

- (void)endRefrehing
{
    if (self.tableView.mj_header) {
        [self.tableView.mj_header endRefreshing];
    }
    if (self.tableView.mj_footer) {
        [self.tableView.mj_footer endRefreshing];
    }
}
//- (void)scrollViewDidScroll:(UIScrollView*)scrollView {
//
//    CGFloat sectionHeaderHeight = WScale(61);
//
//    if(scrollView.contentOffset.y<=sectionHeaderHeight&&scrollView.contentOffset.y>=0) {
//
//        scrollView.contentInset=UIEdgeInsetsMake(-scrollView.contentOffset.y,0,0,0);
//
//    }else if(scrollView.contentOffset.y>=sectionHeaderHeight) {
//
//        scrollView.contentInset=UIEdgeInsetsMake(-sectionHeaderHeight,0,0,0);
//
//    }
//
//}
@end

//- (void)configTableView
//{
//    WeakSelf
//    [self.tableView normalHeaderRefreshingActionBlock:^(FooterConfigBlock  _Nonnull footerConfig) {
//        weakSelf.page = 1;
//        //上拉刷新触发请求第一页数据。
//        [weakSelf requestData:^(NSInteger count) {
//            weakSelf.dataCount = count;
//            [weakSelf.tableView reloadData];
//            //请求成功后将数据的条数回调给tableView，让tableVie自己处理footer的设置
//            footerConfig(count);
//        } failCallBack:^{
//            [weakSelf.tableView endRefrehing];
//        }];
//    }];
//
//    [self.tableView backNormalFooterRefreshingActionBlock:^(FooterConfigBlock  _Nonnull footerConfig) {
//        [weakSelf requestData:^(NSInteger count) {
//            weakSelf.dataCount += count;
//            [weakSelf.tableView reloadData];
//            //请求成功后将数据的条数回调给tableView，让tableVie自己处理footer的设置
//            footerConfig(count);
//        } failCallBack:^{
//            [weakSelf.tableView endRefrehing];
//        }];
//    }];
//
//    [self.tableView beginRefreshing];
//}

//- (void)requestData:(void(^)(NSInteger count))successCallBack failCallBack:(void(^)(void))failCallBack
//{
//    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//
//        if (self.page > 5) {
//            successCallBack(0);
//        }else{
//            successCallBack(20);
//        }
//        self.page += 1;
//    });
//}

