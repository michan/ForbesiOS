//
//  UITableView+Refresh.h
//  Forbes
//
//  Created by 赵志辉 on 2019/9/18.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
typedef void(^FooterConfigBlock)(NSInteger newDataCount);
typedef void(^RefreshActionBlock)(FooterConfigBlock footerConfig);

@interface FBSTableViewController (Refresh)

/**
 每页的数据条数
 */
@property (nonatomic,strong)NSNumber *pageCount;

/**
 多少页
 */
@property (nonatomic,strong)NSNumber *page;

/**
 设置footer的回调
 */
@property (nonatomic,copy)void(^footerConfigBlock)(NSInteger newCount);

/**
 header的刷新
 
 @param refreshBlock 刷新的回调，回调block里有个"FooterConfigBlock"的参数，外部调用的时候可以将请求下拉的数据的条数传进来，让方法里边对footer进行设置。eg：如果下拉刷新都没有数据的话，就可以直接将不要footer。
 */
- (void)normalHeaderRefreshingActionBlock:(RefreshActionBlock)refreshBlock;

/**
 footer的刷新加载更多
 
 @param footerRefreshBlock 加载更多的回调。回调block里有个"FooterConfigBlock"的参数，外部调用的时候可以将请求下拉的数据的条数传进来，让方法里边对footer进行设置。eg：如果上拉加载返回的数据小于每页的设置条目数量，则认为数据已经加载完毕，可以将footer设置为没有更多数据的状态。
 */
- (void)backNormalFooterRefreshingActionBlock:(RefreshActionBlock)footerRefreshBlock;



@end

NS_ASSUME_NONNULL_END
