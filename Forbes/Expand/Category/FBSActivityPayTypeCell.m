//
//  FBSActivityPayTypeCell.m
//  Forbes
//
//  Created by 赵志辉 on 2019/9/26.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import "FBSActivityPayTypeCell.h"
#import "NSArray+Sudoku.h"

@interface FBSActivityPayTypeCell()

@property (nonatomic, strong) UILabel *contenL;
@property (nonatomic, strong) NSArray *imaArray;

@end
@implementation FBSActivityPayTypeCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        
        [self.contentView addSubview:self.contenL]; //免费票
        [self.contenL mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(WScale(17));
            make.top.mas_equalTo(self).offset(WScale(15));
            make.height.mas_equalTo(WScale(25));
            make.right.mas_equalTo(WScale(-17));
            
        }];
        // 创建一个装载九宫格的容器
        UIView *containerView = [[UIView alloc] init];
        [self.contentView addSubview:containerView];
        containerView.backgroundColor = [UIColor whiteColor];
        
        // 给该容器添加布局代码
        [containerView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self);
            make.top.equalTo(self.contenL.mas_bottom);
            make.right.equalTo(self);
            make.bottom.equalTo(self);
        }];
        
        // 为该容器添加宫格View
        for (int i = 0; i < self.imaArray.count; i++) {
            UIButton *bu = [UIButton buttonWithType:UIButtonTypeCustom];
            [containerView addSubview:bu];
            [bu setBackgroundImage:[UIImage imageNamed:self.imaArray[i]] forState:UIControlStateNormal];
            [bu setBackgroundImage:[UIImage imageNamed:self.imaArray[i]] forState:UIControlStateHighlighted];
            [bu addTarget:self action:@selector(clickPay:) forControlEvents:UIControlEventTouchUpInside];
            bu.tag = i * 10000;
        }
        // 执行九宫格布局
        [containerView.subviews mas_distributeSudokuViewsWithFixedItemWidth:WScale(78) fixedItemHeight:WScale(40) warpCount:4 topSpacing:WScale(10) bottomSpacing:WScale(26) leadSpacing:WScale(12) tailSpacing:WScale(12)];
        
    }
    
    return self;
}
- (void)clickPay:(UIButton *)sender{
    if ([self.delegate respondsToSelector:@selector(fbsCell:didClickButtonAtIndex:)]) {
        NSInteger tag =  sender.tag / 10000;
        [self.delegate fbsCell:self didClickButtonAtIndex:tag];
    }
}
- (UILabel *)contenL {
    if (!_contenL) {
        _contenL  = [[UILabel alloc] init];
        _contenL.textColor = Hexcolor(0x333333);
        _contenL.font = kLabelFontSize18;
        _contenL.text = @"付款方式";
        
    }
    return _contenL;
}
- (NSArray *)imaArray{
    if (!_imaArray) {
        _imaArray = @[@"微信支付",@"银联",@"mastercard",@"paypal",@"visa",@"支付宝"];
        
    }
    return _imaArray;
}

- (void)setItem:(FBSItem *)item{
    [super setItem:item];
}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
