//
//  NSMutableAttributedString+FBSAttributedString.h
//  Forbes
//
//  Created by 赵志辉 on 2019/9/27.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSMutableAttributedString (FBSAttributedString)

+ (NSMutableAttributedString *)setTextAttributedString:(NSString *)actionStr attributes:(NSDictionary *)dic space:(CGFloat)space range:(NSRange)range;
@end

NS_ASSUME_NONNULL_END
