//
//  UIImage+EXT.h
//  LgConferenceLive
//
//  Created by Lancoo_Mac on 2019/7/18.
//  Copyright © 2019 Lancoo_Mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (EXT)

+ (UIImage *)imageWithColor:(UIColor *)color;

@end

