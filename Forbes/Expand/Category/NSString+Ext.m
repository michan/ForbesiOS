//
//  NSString+Ext.m
//  Forbes
//
//  Created by 周灿华 on 2019/9/8.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "NSString+Ext.h"

@implementation NSString (Ext)

/**
 验证码手机号
 
 @param mobileNum 手机号
 @return YES 通过 NO 不通过
 */
+ (BOOL)validateContactNumber:(NSString *)mobileNum
{
    /**
     * 手机号码:
     * 13[0-9], 14[5,7], 15[0, 1, 2, 3, 5, 6, 7, 8, 9], 16[6], 17[5, 6, 7, 8], 18[0-9], 170[0-9], 19[89]
     * 移动号段: 134,135,136,137,138,139,150,151,152,157,158,159,182,183,184,187,188,147,178,1705,198
     * 联通号段: 130,131,132,155,156,185,186,145,175,176,1709,166
     * 电信号段: 133,153,180,181,189,177,1700,199
     */
    NSString *MOBILE = @"^1(3[0-9]|4[57]|5[0-35-9]|6[6]|7[05-8]|8[0-9]|9[89])\\d{8}$";
    
    NSString *CM = @"(^1(3[4-9]|4[7]|5[0-27-9]|7[8]|8[2-478]|9[8])\\d{8}$)|(^1705\\d{7}$)";
    
    NSString *CU = @"(^1(3[0-2]|4[5]|5[56]|66|7[56]|8[56])\\d{8}$)|(^1709\\d{7}$)";
    
    NSString *CT = @"(^1(33|53|77|8[019]|99)\\d{8}$)|(^1700\\d{7}$)";
    
    /**
     * 大陆地区固话及小灵通
     * 区号：010,020,021,022,023,024,025,027,028,029
     * 号码：七位或八位
     */
    // NSString * PHS = @"^0(10|2[0-5789]|\\d{3})\\d{7,8}$";
    
    NSPredicate *regextestmobile = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", MOBILE];
    NSPredicate *regextestcm = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CM];
    NSPredicate *regextestcu = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CU];
    NSPredicate *regextestct = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CT];
    // NSPredicate *regextestPHS = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", PHS];
    
    if(([regextestmobile evaluateWithObject:mobileNum] == YES)
       || ([regextestcm evaluateWithObject:mobileNum] == YES)
       || ([regextestct evaluateWithObject:mobileNum] == YES)
       || ([regextestcu evaluateWithObject:mobileNum] == YES)) {
        return YES;
    } else {
        return NO;
    }
}


+ (BOOL)validateEmail:(NSString *)email {
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}



+ (BOOL)validatePassword:(NSString *)pwd {
    NSString *pwdRegex = @"^[A-Za-z0-9]+$";
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",pwdRegex];
    return [pred evaluateWithObject:pwd];

}


+ (NSString *)filterPhone:(NSString *)phone {
    if (phone.length < 7) {
        return phone;
    }
    return [phone stringByReplacingCharactersInRange:NSMakeRange(3, 4) withString:@"****"];

}


//将时间戳转换为时间 （以毫秒为单位）
//参数:时间戳
//返回值格式:2019-04-19 10:33:35.886
+ (NSString *)getTimestamp:(NSString*)mStr formatter:(NSString *)formatter{
    
    NSTimeInterval interval    =[mStr doubleValue];
    
    NSDate *date              = [NSDate dateWithTimeIntervalSince1970:interval];
    
    NSDateFormatter *forma = [[NSDateFormatter alloc] init];
    
    [forma setDateFormat:formatter];
    
    //[formatter setTimeZone:[NSTimeZone timeZoneWithName:@"Asia/Beijing"]];
    
    NSString *dateString      = [forma stringFromDate: date];
    
    NSLog(@"时间戳对应的时间是:%@",dateString);
    
    return dateString;
    
}



+ (NSString *)publishedTimeFormatWithTimeStamp:(NSString *)timeStamp {
    if (timeStamp.length == 0) {
        return timeStamp;
    }
    
    NSString *trimStr = [timeStamp stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    if (trimStr.length == 0) {
        return timeStamp;
    }
    
    NSDateFormatter *fmt = [[NSDateFormatter alloc] init];
//    fmt.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    fmt.dateFormat = @"yyyy-MM-dd";
    
    NSTimeInterval interval = [trimStr doubleValue];
    NSDate *publishedDate = [NSDate dateWithTimeIntervalSince1970:interval];
    NSDate *nowDate = [NSDate date];
    
    if (publishedDate.isThisYear) { // 今年
        if (publishedDate.isToday) { // 今天
            
            NSCalendar *calendar = [NSCalendar currentCalendar];
            NSCalendarUnit unit = NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond;
            
            NSDateComponents *cmps = [calendar components:unit fromDate:publishedDate toDate:nowDate options:0];
            
            if (cmps.hour >= 1) { // 时间差距 >= 1小时
                return [NSString stringWithFormat:@"%zd小时前", cmps.hour];
            } else if (cmps.minute >= 1) { // 1小时 > 时间差距 >= 1分钟
                return [NSString stringWithFormat:@"%zd分钟前", cmps.minute];
            } else { // 1分钟 > 时间差距
                return @"刚刚";
            }
//        } else if (publishedDate.isYesterday) { // 昨天
//            fmt.dateFormat = @"昨天 HH:mm:ss";
//            return [fmt stringFromDate:publishedDate];
        } else { // 其他
//            fmt.dateFormat = @"MM-dd HH:mm:ss";
            fmt.dateFormat = @"MM-dd";
            return [fmt stringFromDate:publishedDate];
        }
    } else { // 非今年
        return [fmt stringFromDate:publishedDate];
    }
}



@end

