//
//  NSMutableAttributedString+FBSAttributedString.m
//  Forbes
//
//  Created by 赵志辉 on 2019/9/27.
//  Copyright © 2019 周灿华. All rights reserved.
//

#import "NSMutableAttributedString+FBSAttributedString.h"

@implementation NSMutableAttributedString (FBSAttributedString)

+ (NSMutableAttributedString *)setTextAttributedString:(NSString *)actionStr attributes:(NSDictionary *)dic space:(CGFloat)space range:(NSRange)range {
    //设置整段字符串的颜色
    NSNumber *number = [NSNumber numberWithFloat:space];
    NSMutableAttributedString *text = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@",actionStr] attributes:dic];
    [text addAttribute:(id)kCTKernAttributeName value:number range:range];
    return text;
}
@end
