//
//  FBSCell.m
//  Forbes
//
//  Created by 周灿华 on 2019/7/20.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSCell.h"

@implementation FBSCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        _bottomLine = [[UIView alloc] init];
        [self.contentView addSubview:_bottomLine];
        
        _bottomLine.backgroundColor = Hexcolor(0xdddddd);
        [_bottomLine mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(@0);
            make.right.equalTo(@0);
            make.bottom.equalTo(@0);
            make.height.equalTo(@(OnePixel));
        }];
        _bottomLine.hidden = YES;
    }
    return self;
}


- (void)setItem:(FBSItem *)item {
    
    if (!UIEdgeInsetsEqualToEdgeInsets(item.bottomLineInsets, _item.bottomLineInsets)) {
        [self.bottomLine mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(@(item.bottomLineInsets.left));
            make.right.equalTo(@(-item.bottomLineInsets.right));
            make.bottom.equalTo(@0);
            make.height.equalTo(@(OnePixel));
        }];
    }
    
    
    _item = item;
    self.bottomLine.hidden = item.bottomLineHidden;
    
    
    //设置cell背景色
    if (item.backgroundColor) {
        self.contentView.backgroundColor = item.backgroundColor;
    } else {
        self.contentView.backgroundColor = [UIColor whiteColor];
    }
    
    
}


- (void)layoutSubviews {
    [super layoutSubviews];
}


@end
