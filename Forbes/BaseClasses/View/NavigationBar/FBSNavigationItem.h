//
//  FBSNavigationItem.h
//  Forbes
//
//  Created by 周灿华 on 2019/7/20.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FBSNavigationItem : NSObject

@property (nonatomic, strong) NSString *title;

@property (nonatomic, strong) NSString *highlightTitle;

@property (nonatomic, strong) NSDictionary *titleAttributes;

@property (nonatomic, strong) UIImage *image;

@property (nonatomic, strong) UIImage *highlightImage;

@property (nonatomic, copy) NSString *imageUrl;

@property (nonatomic, copy) NSString *highlightImageUrl;

@property (nonatomic, assign) BOOL isHide;

@property (nonatomic, assign) BOOL isEnable;

@property (nonatomic, assign) NSInteger tag;

@property (nonatomic, copy) void(^action)(FBSNavigationItem *item);

@property (nonatomic, assign) NSInteger bage;

@end

