//
//  FBSNavigationItem.m
//  Forbes
//
//  Created by 周灿华 on 2019/7/20.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSNavigationItem.h"

@implementation FBSNavigationItem

- (instancetype)init {
    self = [super init];
    if (self) {
        self.isHide = NO;
        self.isEnable = YES;
        self.bage = -1;
        self.tag = 0;
    }
    return self;
}


@end
