//
//  FBSNavigationBar.h
//  Forbes
//
//  Created by 周灿华 on 2019/7/20.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FBSNavigationItem.h"

@interface FBSNavigationBar : UIView

@property (nonatomic, readonly) UIView *contentView;

@property (nonatomic, readonly) UILabel *titleLabel;

@property (nonatomic, strong) UIView *titleView;

@property (nonatomic, readonly) UIButton *backButton;

@property (nonatomic ,strong) UIView *bottomLine; 
/**
 * 目前rightBarButtonItems只支持1个
 *
 * 如需自定义view add进NavigationBar 设置约束即可
 */
@property (nonatomic, strong) NSArray<FBSNavigationItem *> *rightBarButtonItems;

@property (nonatomic, strong) UIImage *backImage;

- (void)addBackButtonAction:(SEL)backAction target:(id)target;


/**
 修改contentView 透明度, titleLabel, titleView 也会改变透明度

 @param alpha 透明度
 */
- (void)changeContentViewAlpha:(CGFloat)alpha;


#pragma mark -在应用启动时候设置导航栏默认值
/**
 *  默认bar背景颜色
 *
 *  @param color 颜色
 */
+ (void)setDefaultBarBackgroundColor:(UIColor *)color;

/**
 *  titleLabel默认的参数
 *
 *  @param attributes 配置，其中NSFontAttributeName和NSForegroundColorAttributeName有效
 */
+ (void)setDefaultTitleAttributes:(NSDictionary *)attributes;

/**
 *  返回按钮默认的图片
 *
 *  @param image 图片
 */
+ (void)setDefaultBackButtonImage:(UIImage *)image;
/**
 *  barButtonItem默认参数
 *
 *  @param attributes 配置，其中NSFontAttributeName和NSForegroundColorAttributeName有效
 */
+ (void)setDefaultBarButtonItemAttributes:(NSDictionary *)attributes;

@end

