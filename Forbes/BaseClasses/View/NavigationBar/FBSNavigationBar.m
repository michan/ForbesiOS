//
//  FBSNavigationBar.m
//  Forbes
//
//  Created by 周灿华 on 2019/7/20.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSNavigationBar.h"

static UIColor *defaultColor = nil;
static NSDictionary *defaultTitleAttributes = nil;  
static UIImage *defaultBackImage = nil;
static NSDictionary *defaultItemAttributes = nil;

@interface FBSNavigationBar ()
@property (nonatomic, copy) NSArray *rightButtons;
@property (nonatomic, strong) NSMutableDictionary *callbackDictionary;
@end



@implementation FBSNavigationBar

- (id) init {
    self = [super init];
    if (self)
    {
        self.backgroundColor = [UIColor clearColor];
        
        _contentView = [[UIView alloc] init];
        [self addSubview:_contentView];
        
        [_contentView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.mas_equalTo(0);
        }];
        
        
        _titleLabel = ({
            UILabel *aLabel = [[UILabel alloc] init];
            aLabel;
        });
        [self addSubview:_titleLabel];
        
        [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            
            make.centerY.mas_equalTo(self).offset(kStatusBarH * 0.5);
            make.centerX.mas_equalTo(self).offset(0);
            make.width.mas_lessThanOrEqualTo(260);
            
        }];
        
        _backButton = ({
            UIButton *btn = [[UIButton alloc] init];
            btn;
        });
        [self addSubview:_backButton];
        
        [_backButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self).offset(0);
            make.centerY.mas_equalTo(self.titleLabel);
            make.height.width.mas_equalTo(44);
        }];
        
        _bottomLine = ({
            UIView *aView = [[UIView alloc]init];
            aView.backgroundColor = [UIColor hexColor:@"f2f2f2"];
            aView;
        });
        [self addSubview:_bottomLine];
        [_bottomLine mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.mas_equalTo(0);
            make.bottom.mas_equalTo(0);
            make.height.mas_equalTo(1);
        }];
        
        //初始化默认状态
        if (defaultTitleAttributes)
        {
            UIFont *font = [defaultTitleAttributes objectForKey:NSFontAttributeName];
            if (font) {
                _titleLabel.font = font;
            }
            
            UIColor *color = [defaultTitleAttributes objectForKey:NSForegroundColorAttributeName];
            if (color) {
                _titleLabel.textColor = color;
            }
        }
        
        if (defaultColor) {
            self.contentView.backgroundColor = defaultColor;
        }
        
        if (defaultBackImage)
        {
            [_backButton setImage:defaultBackImage forState:UIControlStateNormal];
        }
    }
    return self;
}

- (void)setBackImage:(UIImage *)backImage
{
    _backImage = backImage;
    
    [_backButton setImage:backImage forState:UIControlStateNormal];
    
}

- (void) setTitleView:(UIView *)titleView
{
    if (_titleView == titleView) {
        return;
    }
    [_titleView removeFromSuperview];
    _titleView = titleView;
    if (titleView) {
        _titleLabel.hidden = YES;
    }else
    {
        _titleLabel.hidden = NO;
    }
    [self addSubview:titleView];
    
    [titleView mas_makeConstraints:^(MASConstraintMaker *make) {
        //        make.centerY.mas_equalTo(self).offset(11);
        make.centerY.mas_equalTo(self.titleLabel);
        make.centerX.mas_equalTo(self).offset(0);
        make.width.mas_lessThanOrEqualTo(160);
    }];
}

- (void)addBackButtonAction:(SEL)backAction target:(id)target {
    if (!target || !backAction) {
        return;
    }
    
    [self.backButton addTarget:target action:backAction forControlEvents:UIControlEventTouchUpInside];
}


- (void)changeContentViewAlpha:(CGFloat)alpha {
    if (alpha < 0 || alpha > 1.0) {
        alpha = 1.0;
    }
    self.contentView.alpha = alpha;
    self.titleLabel.alpha = alpha;
    if (self.titleView) {
        self.titleView.alpha = alpha;
    }
}


- (void) setRightBarButtonItems:(NSArray<FBSNavigationItem *> *)rightBarButtonItems
{
    if (_rightBarButtonItems == rightBarButtonItems) {
        return;
    }
    
    _rightBarButtonItems = rightBarButtonItems;
    for (UIView *aView in _rightButtons) {
        [aView removeFromSuperview];
    }
    
    NSInteger maxCount = MIN(2, rightBarButtonItems.count);
    
    if (maxCount > 0) {
        
        FBSNavigationItem *aItem = [rightBarButtonItems objectAtIndex:0];
        UIButton *aBtn = [self createBarButton:aItem];
        [self addSubview:aBtn];
        
        if (aItem.title) {
            CGFloat cw = floorf([aItem.title sizeWithAttributes:@{NSFontAttributeName : aBtn.titleLabel.font}].width) ;
            
            [aBtn mas_makeConstraints:^(MASConstraintMaker *make) {
                
                make.width.mas_equalTo(@(cw + 15));
                make.centerY.mas_equalTo(self.titleLabel);
                make.height.mas_equalTo(44);
                make.right.mas_equalTo(0);
            }];
            
            //
            //            [RACObserve(aItem, title) subscribeNext:^(NSString *title) {
            //                [aBtn setTitle:title forState:UIControlStateNormal];
            //
            //                CGFloat cw = floorf([title sizeWithAttributes:@{NSFontAttributeName : aBtn.titleLabel.font}].width) ;
            //                [aBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
            //
            //                    @strongify(self);
            //
            //                    make.width.mas_equalTo(@(cw + 15));
            //                    make.centerY.mas_equalTo(self.titleLabel);
            //                    make.height.mas_equalTo(44);
            //                    make.right.mas_equalTo(@0);
            //                }];
            //
            //            }];
            
            aBtn.titleEdgeInsets = UIEdgeInsetsMake(0, -9, 0, 9);
            
            //            [RACObserve(aItem, titleAttributes) subscribeNext:^(NSDictionary *titleAttributes) {
            //                UIFont *font = [titleAttributes objectForKey:NSFontAttributeName];
            //                UIColor *color = [titleAttributes objectForKey:NSForegroundColorAttributeName];
            //                aBtn.titleLabel.font = font ? font : [defaultItemAttributes objectForKey:NSFontAttributeName];
            //                [aBtn setTitleColor:color ? color : [defaultItemAttributes objectForKey:NSForegroundColorAttributeName] forState:UIControlStateNormal];
            //
            //                CGFloat cw = floorf([[aBtn titleForState:UIControlStateNormal] sizeWithAttributes:@{NSFontAttributeName : aBtn.titleLabel.font}].width) ;
            //                [aBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
            //
            //                    @strongify(self);
            //
            //                    make.width.mas_equalTo(@(cw + 15));
            //                    make.centerY.mas_equalTo(self.titleLabel);
            //                    make.height.mas_equalTo(44);
            //                    make.right.mas_equalTo(@0);
            //                }];
            //            }];
                    }else
                    {
                        [aBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            
                            make.width.mas_equalTo(@(aItem.image.size.width + 20));
                            make.centerY.mas_equalTo(self.titleLabel);
                            make.height.mas_equalTo(44);
                            make.right.mas_equalTo(-10);
                        }];
            //            [RACObserve(aItem, image) subscribeNext:^(UIImage *image) {
            //
            //                [aBtn setImage:image forState:UIControlStateNormal];
            //
            //                [aBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
            //
            //                    @strongify(self);
            //
            //                    make.width.mas_equalTo(@(image.size.width + 15));
            //                    make.centerY.mas_equalTo(self.titleLabel);
            //                    make.height.mas_equalTo(44);
            //                    make.right.mas_equalTo(@0);
            //                }];
            //            }];
            //
            //            aBtn.imageEdgeInsets = UIEdgeInsetsMake(0, -9, 0, 9);
        }
        _rightButtons = @[aBtn];
    }
}

- (UIButton *)createBarButton:(FBSNavigationItem *)item
{
    UIButton *btn = [[UIButton alloc] init];
    btn.tag = item.tag;
    
    if (defaultItemAttributes) {
        UIFont *font = [defaultItemAttributes objectForKey:NSFontAttributeName];
        if (font) {
            btn.titleLabel.font = font;
        }
        
        UIColor *color = [defaultItemAttributes objectForKey:NSForegroundColorAttributeName];
        if (color) {
            [btn setTitleColor:color forState:UIControlStateNormal];
        }
    }
    
    if (item.title){
        [btn setTitle:item.title forState:UIControlStateNormal];
        
        
        if (item.titleAttributes) {
            UIFont *font = [item.titleAttributes objectForKey:NSFontAttributeName];
            if (font) {
                btn.titleLabel.font = font;
            }
            
            UIColor *color = [item.titleAttributes objectForKey:NSForegroundColorAttributeName];
            if (color) {
                [btn setTitleColor:color forState:UIControlStateNormal];
            }
        }
        
    }else
    {
        [btn setImage:[CommonTool imageWithImage:item.image  scaledToSize:CGSizeMake(21, 5)] forState:UIControlStateNormal];
    }
    
    
    [item addObserver:self forKeyPath:@"isHide" options:NSKeyValueObservingOptionNew context:NULL];
    
    [item addObserver:self forKeyPath:@"isEnable" options:NSKeyValueObservingOptionNew context:NULL];
    
    
    if (item.action) {
        [self.callbackDictionary setObject:[item.action copy] forKey:@(item.tag)];
        
        if ([btn isKindOfClass:[UIButton class]]) {
            [btn addTarget:self action:@selector(buttonClick:) forControlEvents:UIControlEventTouchUpInside];
        }
    }
    
    return btn;
}



- (void)observeValueForKeyPath:(nullable NSString *)keyPath ofObject:(nullable id)object change:(nullable NSDictionary<NSKeyValueChangeKey, id> *)change context:(nullable void *)context {
    if ([keyPath isEqualToString:@"isHide"]) {
        
    } else if ([keyPath isEqualToString:@"isEnable"]) {
        
        
    }
}

- (void)buttonClick:(UIButton *)button {
    void(^action)(FBSNavigationItem *item) = [self.callbackDictionary objectForKey:@(button.tag)];
    
    if (action) {
        FBSNavigationItem *item  = [self.rightBarButtonItems objectAtIndex:button.tag];
        action(item);
    }
}



- (void)dealloc {
    for (FBSNavigationItem *item in self.rightBarButtonItems) {
        [item removeObserver:self forKeyPath:@"isHide"];
        [item removeObserver:self forKeyPath:@"isEnable"];
    }
}


+ (void) setDefaultBarBackgroundColor:(UIColor *)color
{
    defaultColor = color;
}

+ (void) setDefaultTitleAttributes:(NSDictionary *)attributes
{
    defaultTitleAttributes = attributes;
}

+ (void) setDefaultBackButtonImage:(UIImage *)image
{
    defaultBackImage = image;
}

+ (void) setDefaultBarButtonItemAttributes:(NSDictionary *)attributes
{
    defaultItemAttributes = attributes;
}

//点击事件传递到下一层级
- (UIView*)hitTest:(CGPoint)point withEvent:(UIEvent *)event{
    UIView *hitView = [super hitTest:point withEvent:event];
    if(hitView == self.contentView && IPHONE_X){
        return nil;
    }
    return hitView;
}



- (NSMutableDictionary *)callbackDictionary
{
    if (nil == _callbackDictionary) {
        _callbackDictionary = [NSMutableDictionary dictionaryWithCapacity:5];
    }
    return _callbackDictionary ;
}

@end


