//
//  FBSItem.h
//  Forbes
//
//  Created by 周灿华 on 2019/7/20.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FBSBaseModel.h"

@interface FBSItem : NSObject

@property (nonatomic, assign) CGFloat cellHeight;

@property (nonatomic, assign) UIEdgeInsets bottomLineInsets;

@property (nonatomic, assign) BOOL bottomLineHidden;

@property (nonatomic, strong) UIColor *backgroundColor;

@property (nonatomic, assign) NSInteger tag;

///cell复用标识
+ (NSString *)reuseIdentifier;

+ (instancetype)item;

///如果需要一些特殊的高度计算,子类重写该方法
- (CGFloat)calculateCellHeight;


/**
 子类重载方法

 @param model 子类model
 */
- (void)ctreatItemData:(FBSBaseModel *)model;

@end


