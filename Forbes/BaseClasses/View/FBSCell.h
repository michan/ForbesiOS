//
//  FBSCell.h
//  Forbes
//
//  Created by 周灿华 on 2019/7/20.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import <UIKit/UIKit.h>

@class FBSCell;

@protocol FBSCellDelegate <NSObject>
@optional

///cell 点击事件
- (void)fbsCell:(FBSCell *)cell didSelectRowAtIndexPath:(NSIndexPath *)indexPath;

///cell 上面的按钮被点击
- (void)fbsCell:(FBSCell *)cell  didClickButtonAtIndex:(NSInteger)index;

@end


@interface FBSCell : UITableViewCell

@property (nonatomic, strong) FBSItem *item;

@property (nonatomic, weak)   id<FBSCellDelegate> delegate;

@property (nonatomic, strong) UIView *bottomLine;

@end

