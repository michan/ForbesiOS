//
//  FBSViewModel.h
//  Forbes
//
//  Created by 周灿华 on 2019/7/20.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 该类负责网络请求,处理网络请求返回的数据,提供viewController展示所需的数据
 */
@interface FBSViewModel : NSObject

@end

