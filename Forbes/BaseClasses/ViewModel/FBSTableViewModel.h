//
//  FBSTableViewModel.h
//  Forbes
//
//  Created by 周灿华 on 2019/7/20.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSViewModel.h"
#import "FBSItem.h"

@interface FBSTableViewModel : FBSViewModel

/** 请求数据的页数  */
@property (nonatomic, assign) NSInteger page;
/** 请求数据的每页多少条 */
@property (nonatomic, assign) NSInteger limit;

/** tableView item数组 */
@property (nonatomic, strong) NSMutableArray * _Nonnull items;
/** tableView 分组的数量 */
@property (nonatomic, readonly) NSInteger numberOfSections;

/**
 *  获取tableView在某一个分组中的元素数量
 *
 *  @param section 分组的index
 *
 *  @return 分组中元素的数量
 */
- (NSInteger)numberOfRowsInSection:(NSInteger)section;

/**
 *  获取在特定位置的tableView元素
 *
 *  @param row     所在行
 *  @param section 所在分组
 *
 *  @return 特定位置的tableView元素
 */
- (nullable FBSItem *)itemAtRow:(NSInteger)row inSection:(NSInteger)section;

/**
 *  获得tag为指定值的第一个item
 *
 *  @param tag 指定的标识值
 *
 *  @return tag为指定标识值的第一个item
 */
- (nullable FBSItem *)itemWithTag:(NSInteger)tag;

/**
 *  获取item所在的indexPath
 *
 *  @param item tableView元素
 *
 *  @return 指定item所在indexPath,没找到时返回nil
 */
- (nullable NSIndexPath *)indexPathForItem:(nullable FBSItem *)item;

/**
 *  获取item所在的行在其分组内的index
 *
 *  @param item tableView元素
 *
 *  @return 指定item在其分组内的row index,不存在时返回NSNotFound
 */
- (NSUInteger)rowForFormItem:(nullable FBSItem *)item;


///生成item数据,子类重写该方法
- (void)makeItems;

@end

