//
//  FBSTableViewModel.m
//  Forbes
//
//  Created by 周灿华 on 2019/7/20.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSTableViewModel.h"

@interface FBSTableViewModel ()


@end


@implementation FBSTableViewModel

- (instancetype)init {
    self = [super init];
    if (self) {
        
    }
    return self;
}

- (BOOL)isMultipleSection {
    return [self.items.firstObject isKindOfClass:[NSArray class]];
}

- (NSInteger)numberOfSections {
    // 如果是多个数组组成，则当成是多个section
    if ([self isMultipleSection]) {
        return self.items.count;
    } else {
        return 1;
    }
}

- (NSInteger)numberOfRowsInSection:(NSInteger)section {
    // 如果是由多个数组组成，则为多个section，返回每个section的数量;
    if ([self isMultipleSection]) {
        return [self.items[section] count];
    } else {
        // 只有一个分组，返回该分组的元素数量
        return self.items.count;
    }
}

- (FBSItem *)itemWithTag:(NSInteger)tag {
    for (NSInteger section = 0; section < [self numberOfSections]; section++) {
        NSInteger rowCount = [self numberOfRowsInSection:section];
        for (NSInteger row = 0; row < rowCount; row++) {
            FBSItem *item = [self itemAtRow:row inSection:section];
            if (item.tag == tag) {
                return item;
            }
        }
    }
    
    return nil;
}

- (FBSItem *)itemAtRow:(NSInteger)row inSection:(NSInteger)section {
    // 如果是有分组，则按分组的方式返回
    if ([self isMultipleSection]) {
        return self.items[section][row];
    } else {
        return self.items[row];
    }
    
    return nil;
}

- (NSUInteger)rowForFormItem:(FBSItem *)item {
    return [self indexPathForItem:item].row;
}

- (NSIndexPath *)indexPathForItem:(FBSItem *)item {
    for (int section = 0; section < [self numberOfSections]; section++) {
        for (int row = 0; row < [self numberOfRowsInSection:section]; row++) {
            if ([self itemAtRow:row inSection:section]==item) {
                return [NSIndexPath indexPathForRow:row inSection:section];
            }
        }
    }
    
    return nil;
}



- (void)makeItems {
    
}

#pragma mark - property

- (NSMutableArray *)items {
    if (!_items) {
        _items = [NSMutableArray array];
    }
    return _items;
}

@end
