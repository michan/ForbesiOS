//
//  FBSViewController.h
//  Forbes
//
//  Created by 周灿华 on 2019/7/20.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIView+Toast.h"
#import "FBSUserData.h"

@interface FBSViewController : UIViewController

@property (nonatomic, assign, readonly) BOOL isLogin;

@property (nonatomic, strong) FBSViewModel *viewModel;
///设置是否支持全屏返回
@property (nonatomic, assign) BOOL interactivePopDisabled;
///全屏返回手势的触发边界值
@property (nonatomic, assign) CGFloat interactivePopDistanceToLeftEdge;
///设置自定义导航栏是否隐藏
@property (nonatomic, assign) BOOL navigationBarHidden;
///设置自定义导航栏返回按钮是否隐藏
@property (nonatomic, assign) BOOL navigationBackButtonHidden;

@property (nonatomic, strong, readonly) FBSNavigationBar *navigationBar;
///opop之前应该做的 申请
@property (nonatomic, strong) void(^popViewControllerBlock)(void);

///点击了导航栏返回的按钮,子类可以重写该方法完成一些特殊的逻辑
- (void)onTouchNavigationBarBackButton;

///跳转登录
- (void)pushToLoginController;

///登录成功回调
- (void)loginSuccess;

///登出成功回调
- (void)logoutSuccess;

@end




typedef NS_ENUM(NSUInteger, ToastPosition) {
    ToastPositionTop,
    ToastPositionCenter,
    ToastPositionBottom
};

@interface FBSViewController (Toast)

- (void)makeToast:(NSString *)message;

- (void)makeToast:(NSString *)message duration:(NSTimeInterval)duration position:(ToastPosition)position;

@end



@interface FBSViewController (HUD)

//默认遮盖整个屏幕
- (void)showHUD;

- (void)showHUD:(BOOL)lock;

- (void)hideHUD;

@end
