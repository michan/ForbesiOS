//
//  FBSTableViewController.h
//  Forbes
//
//  Created by 周灿华 on 2019/7/20.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSViewController.h"

@interface FBSTableViewController : FBSViewController
<UITableViewDataSource,UITableViewDelegate,FBSCellDelegate>

@property (nonatomic, strong) FBSTableViewModel *viewModel;

@property (nonatomic, strong, readonly) FBSTableView *tableView;

@property (nonatomic, strong) NSDictionary<NSString *, Class> *registerDictionary;


//cell事件处理, 子类需要重写这两个方法
- (void)fbsCell:(FBSCell *)cell didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
- (void)fbsCell:(FBSCell *)cell didClickButtonAtIndex:(NSInteger)index;
@end
