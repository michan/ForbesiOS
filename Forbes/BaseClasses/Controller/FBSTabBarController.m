
//
//  FBSTabBarController.m
//  Forbes
//
//  Created by 周灿华 on 2019/7/24.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSTabBarController.h"
#import "FBSHomeViewController.h"
#import "FBSRankViewController.h"
#import "FBSVideoViewController.h"
#import "FBSActivityViewController.h"
#import "FBSMineViewController.h"

@implementation FBSTabBarController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[UITabBar appearance] setTranslucent:NO];
    
   UITabBarItem *appearance = [UITabBarItem appearance];
    [appearance setTitleTextAttributes:@{NSForegroundColorAttributeName : Hexcolor(0x666666)} forState:UIControlStateNormal];
    [appearance setTitleTextAttributes:@{NSForegroundColorAttributeName : MainThemeColor} forState:UIControlStateSelected];
    
    if (@available(iOS 13.0, *)) {
        self.tabBar.tintColor = MainThemeColor;
    }

    FBSHomeViewController *homeVc = [[FBSHomeViewController alloc] init];
    [homeVc setupChannels];  //加载频道
    homeVc.title = @"首页";
    homeVc.tabBarItem.image = kImageWithName(@"tar_homeunsel");
    homeVc.tabBarItem.selectedImage = kImageWithName(@"tab_homesel");
    FBSNavigationController *homeNav = [[FBSNavigationController alloc] initWithRootViewController:homeVc];
    
    
    FBSRankViewController *rankVc = [[FBSRankViewController alloc] init];
    rankVc.title = @"榜单";
    rankVc.tabBarItem.image = kImageWithName(@"tab_rankunsel");
    rankVc.tabBarItem.selectedImage = kImageWithName(@"tab_ranksel");
    FBSNavigationController *rankNav = [[FBSNavigationController alloc] initWithRootViewController:rankVc];
    
    
    FBSVideoViewController *videoVc = [[FBSVideoViewController alloc] init];
    [videoVc setupChannels];  //加载频道
    videoVc.title = @"视频";
    videoVc.tabBarItem.image = kImageWithName(@"tab_videounsel");
    videoVc.tabBarItem.selectedImage = kImageWithName(@"tab_videosel");
    FBSNavigationController *videoNav = [[FBSNavigationController alloc] initWithRootViewController:videoVc];
    
    FBSActivityViewController *activityVc = [[FBSActivityViewController alloc] init];
    activityVc.title = @"活动";
    activityVc.tabBarItem.image = kImageWithName(@"tab_activityunsel");
    activityVc.tabBarItem.selectedImage = kImageWithName(@"tab_activitysel");
    FBSNavigationController *activityNav = [[FBSNavigationController alloc] initWithRootViewController:activityVc];
    
    FBSMineViewController *mineVc = [[FBSMineViewController alloc] init];
    mineVc.title = @"我的";
    mineVc.tabBarItem.image = kImageWithName(@"tab_mineunsel");
    mineVc.tabBarItem.selectedImage = kImageWithName(@"tab_minesel");
    FBSNavigationController *mineNav = [[FBSNavigationController alloc] initWithRootViewController:mineVc];
    
    [self setViewControllers:@[
                               homeNav,
                               rankNav,
                               videoNav,
                               activityNav,
                               mineNav
                               ]];
}


- (BOOL)prefersStatusBarHidden {
    return NO;
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

@end
