//
//  FBSNavigationController.h
//  Forbes
//
//  Created by 周灿华 on 2019/7/20.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FBSNavigationController : UINavigationController

///支持全屏放回手势
@property (nonatomic, strong, readonly) UIPanGestureRecognizer *fullscreenPopGestureRecognizer;

@end

NS_ASSUME_NONNULL_END
