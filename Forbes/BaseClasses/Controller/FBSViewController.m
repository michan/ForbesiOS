//
//  FBSViewController.m
//  Forbes
//
//  Created by 周灿华 on 2019/7/20.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSViewController.h"
#import "FBSLoginViewController.h"
#import "MBProgressHUD+JDragon.h"

@interface FBSViewController ()
@property (nonatomic, strong) FBSNavigationBar *navigationBar;
@end

@implementation FBSViewController
@dynamic viewModel;  //不要生产ivar getter setter

#pragma mark - life cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.view.backgroundColor = COLOR_CommomBG;
    [self createNavigationBar];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loginSuccess) name:FBSLoginSuccessNotication object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(logoutSuccess) name:FBSLogoutSuccessNotication object:nil];
}


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationBar.hidden = self.navigationBarHidden;
    self.navigationBar.backButton.hidden = self.navigationBackButtonHidden;
    
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
}



- (void)setTitle:(NSString *)title{
    self.navigationBar.titleLabel.text = title;
    [super setTitle:title];
}


- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleDefault;
}


- (void)dealloc {
    
    //移除监听
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    FBSLog(@"%@ 控制器销毁",NSStringFromClass([self class]));
}

#pragma mark - public method

- (void)createNavigationBar {
    if (self.title) {
        self.navigationBar.titleLabel.text = self.title;
    }
    
    [self.view addSubview:self.navigationBar];
    
    [self.navigationBar addBackButtonAction:@selector(onTouchNavigationBarBackButton) target:self];
    
    //    [self.navigationBar addBackButtonActionSignal:[RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
    //
    //        @strongify(self);
    //        if (self.topNavigationBackBarHandler) {
    //            self.topNavigationBackBarHandler();
    //            [self.navigationController popViewControllerAnimated:YES];
    //        }
    //
    //        [subscriber sendCompleted];
    //        return nil;
    //    }]];
    
    
    [self.navigationBar mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.mas_equalTo(0);
        make.height.mas_equalTo(kNavTopMargin);
    }];
    
    
}


- (void)onTouchNavigationBarBackButton {
    if (self.popViewControllerBlock) {
        self.popViewControllerBlock();
    }
    
    [self.navigationController popViewControllerAnimated:YES];
}



- (void)pushToLoginController {
    FBSLoginViewController *loginVC = [[FBSLoginViewController alloc] init];
    [self.navigationController pushViewController:loginVC animated:YES];
}


- (void)showTopNavigationBar {
    CGRect aRec = self.navigationBar.frame;
    aRec.origin.y = -aRec.size.height;
    self.navigationBar.frame = aRec;
    
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"
    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationNone];
#pragma clang diagnostic pop
    aRec.origin.y = 0;
    [UIView animateWithDuration:0.6 animations:^{
        self.navigationBar.frame = aRec;
    } completion:^(BOOL finished) {
        
    }];
}

- (void)hideTopNavigationBar {
    CGRect aRec = self.navigationBar.frame;
    aRec.origin.y = -aRec.size.height;
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"
    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationSlide];
#pragma clang diagnostic pop
    
    [UIView animateWithDuration:0.3 animations:^{
        
        self.navigationBar.frame = aRec;
    } completion:^(BOOL finished) {
        
    }];
}

- (void)loginSuccess {
    
}


- (void)logoutSuccess {
    
}

#pragma mark - propert

- (FBSNavigationBar *)navigationBar {
    if (!_navigationBar) {
        _navigationBar = [[FBSNavigationBar alloc] init];
    }
    return _navigationBar;
}


- (BOOL)isLogin {
    return [FBSUserData sharedData].isLogin;
}

@end





@implementation FBSViewController (Toast)

- (void)makeToast:(NSString *)message {
    [self.toastShowView makeToast:message duration:1.5 position:CSToastPositionCenter style:nil];
}

- (void)makeToast:(NSString *)message duration:(NSTimeInterval)duration position:(ToastPosition)position {
    NSString const *pos = @"";
    
    switch (position) {
        case ToastPositionTop:
            pos = CSToastPositionTop;
            break;
            
        case ToastPositionCenter:
            pos = CSToastPositionCenter;
            break;

        case ToastPositionBottom:
            pos = CSToastPositionBottom;
            break;

            
        default:
            break;
    }

    [self.toastShowView makeToast:message duration:duration position:pos style:nil];
}


- (UIView *)toastShowView {
    if (self.navigationController) {
        return self.navigationController.view;
        
    } else {
        return KEYWINDOW;
    }
}

@end




@implementation FBSViewController (HUD)

- (void)showHUD {
    [self showHUD:YES];
}

- (void)showHUD:(BOOL)lock {
    dispatch_async(dispatch_get_main_queue(), ^{
        if (lock) {
            //默认锁住15秒,可调用 hideHUD 提前取消
            [MBProgressHUD showActivityMessageInWindow:nil timer:15];
            
        } else {
            [MBProgressHUD showActivityMessageInView:nil];
        }
        
    });
}

- (void)hideHUD {
    dispatch_async(dispatch_get_main_queue(), ^{
        [MBProgressHUD hideHUD];
    });

}

@end





