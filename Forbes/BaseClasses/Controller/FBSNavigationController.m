//
//  FBSNavigationController.m
//  Forbes
//
//  Created by 周灿华 on 2019/7/20.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSNavigationController.h"
#import <WMScrollView.h>

@interface FBSNavigationController ()<UINavigationControllerDelegate,UIGestureRecognizerDelegate>

@property (nonatomic, strong) UIPanGestureRecognizer *fullscreenPopGestureRecognizer;
@end


@implementation FBSNavigationController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.interactivePopGestureRecognizer.view addGestureRecognizer:self.fullscreenPopGestureRecognizer];
    
    //设置手势处理target
    NSArray *internalTargets = [self.interactivePopGestureRecognizer valueForKey:@"targets"];
    id internalTarget = [internalTargets.firstObject valueForKey:@"target"];
    SEL internalAction = NSSelectorFromString(@"handleNavigationTransition:");
    [self.fullscreenPopGestureRecognizer addTarget:internalTarget action:internalAction];
    
    //让系统的 interactivePopGestureRecognizer 失效
    self.interactivePopGestureRecognizer.enabled = NO;
    
    [self setNavigationBarHidden:YES animated:NO];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - override

- (void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated {
    
    if (self.childViewControllers.count > 0) {
        viewController.hidesBottomBarWhenPushed = YES;
    }
    [super pushViewController:viewController animated:animated];
    


}



#pragma mark - UIGestureRecognizerDelegate

- (BOOL)gestureRecognizerShouldBegin:(UIPanGestureRecognizer *)gestureRecognizer {
    //跟控制器,不能触发手势
    if (self.viewControllers.count <= 1) {
        return NO;
    }
    
    UIViewController *topViewController = self.viewControllers.lastObject;
    
    if ([topViewController isKindOfClass:[FBSViewController class]]) {
        FBSViewController *baseViewcontroller  = (FBSViewController *)topViewController;
        
        if (baseViewcontroller.interactivePopDisabled) { //子控制器关闭全屏手势
            return NO;
        }
        
        //左滑边界值校验
        CGPoint beginningLocation = [gestureRecognizer locationInView:gestureRecognizer.view];
        CGFloat maxAllowedInitialDistance = baseViewcontroller.interactivePopDistanceToLeftEdge;
        
        if (maxAllowedInitialDistance > 0 && beginningLocation.x > maxAllowedInitialDistance) {
            return NO;
            
        }
        
    }
    
    // 如果push、pop动画正在执行（私有属性）
    if ([[self valueForKey:@"_isTransitioning"] boolValue]) {
        return NO;
    }
    
    // 确保从左向右滑动方向
    CGPoint translation = [gestureRecognizer translationInView:gestureRecognizer.view];
    BOOL isLeftToRight = [UIApplication sharedApplication].userInterfaceLayoutDirection == UIUserInterfaceLayoutDirectionLeftToRight;
    CGFloat multiplier = isLeftToRight ? 1 : - 1;
    if ((translation.x * multiplier) <= 0) {
        return NO;
    }
    
    return YES;
}


- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    
    //解决全屏返回手势与WMScrollView的冲突
    if ([otherGestureRecognizer.view isKindOfClass:[WMScrollView class]]) {
        WMScrollView *scrollView = (WMScrollView *)otherGestureRecognizer.view;
        
        if (scrollView.contentOffset.x  <= 0) {
            return YES;
        }
    }
    
    return NO;
}



- (UIPanGestureRecognizer *)fullscreenPopGestureRecognizer {
    if (!_fullscreenPopGestureRecognizer) {
        _fullscreenPopGestureRecognizer = [[UIPanGestureRecognizer alloc] init];
        _fullscreenPopGestureRecognizer.maximumNumberOfTouches = 1;
        _fullscreenPopGestureRecognizer.delegate = self;
        
    }
    return _fullscreenPopGestureRecognizer;
}

@end

