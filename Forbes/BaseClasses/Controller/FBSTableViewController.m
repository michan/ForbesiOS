//
//  FBSTableViewController.m
//  Forbes
//
//  Created by 周灿华 on 2019/7/20.
//  Copyright © 2019年 周灿华. All rights reserved.
//

#import "FBSTableViewController.h"

@interface FBSTableViewController ()
@property (nonatomic, strong) FBSTableView *tableView;
@end

@implementation FBSTableViewController
@dynamic viewModel;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.registerDictionary = [[NSDictionary alloc] init];
    self.tableView.estimatedRowHeight = 44;
    self.tableView.frame = self.view.bounds;
    [self.view insertSubview:self.tableView belowSubview:self.navigationBar];
    
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(0);
    }];
    self.tableView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
    
    [self.viewModel makeItems];
}


#pragma mark - method

- (void)setRegisterDictionary:(NSDictionary *)registerDictionary {
    _registerDictionary = registerDictionary;
    
    if (registerDictionary && registerDictionary.allKeys) {
        [registerDictionary enumerateKeysAndObjectsUsingBlock:^(id  _Nonnull key, id  _Nonnull obj, BOOL * _Nonnull stop) {
            if ([key isKindOfClass:[NSString class]]) {
                [self.tableView registerClass:obj forCellReuseIdentifier:key];
            }
            
        }];
    }
}



- (void)setNavigationBarHidden:(BOOL)navigationBarHidden {
    [super setNavigationBarHidden:navigationBarHidden];
    
    if (navigationBarHidden) {
        self.tableView.contentInset = UIEdgeInsetsZero;
        self.tableView.scrollIndicatorInsets = UIEdgeInsetsZero;
    } else {
        self.tableView.contentInset = UIEdgeInsetsMake(kNavTopMargin, 0, 0, 0);
        self.tableView.scrollIndicatorInsets = self.tableView.contentInset;
    }
    
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    NSAssert(self.viewModel != nil && [self.viewModel isKindOfClass:[FBSTableViewModel class]], @"FBSTableViewController  viewModel属性不能为空，且必须为FBSTableViewModel子类！");
    
    return self.viewModel.numberOfSections;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    NSAssert(self.viewModel != nil && [self.viewModel isKindOfClass:[FBSTableViewModel class]], @"FBSTableViewController  viewModel属性不能为空，且必须为FBSTableViewModel子类！");
    
    return [self.viewModel numberOfRowsInSection:section];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSAssert(self.viewModel != nil && [self.viewModel isKindOfClass:[FBSTableViewModel class]], @"FBSTableViewController  viewModel属性不能为空，且必须为FBSTableViewModel子类！");
    
    FBSItem *item  = [self.viewModel itemAtRow:indexPath.row inSection:indexPath.section];
    FBSCell *cell;
    if ([[item class] reuseIdentifier].length) {
        cell = [self.tableView dequeueReusableCellWithIdentifier:[[item class] reuseIdentifier] forIndexPath:indexPath];
        cell.item = item;
        cell.delegate = self;
    }
    return cell;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSAssert(self.viewModel != nil && [self.viewModel isKindOfClass:[FBSTableViewModel class]], @"FBSTableViewController  viewModel属性不能为空，且必须为FBSTableViewModel子类！");
    
    FBSItem *item  = [self.viewModel itemAtRow:indexPath.row inSection:indexPath.section];
    return item.cellHeight;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSAssert(self.viewModel != nil && [self.viewModel isKindOfClass:[FBSTableViewModel class]], @"FBSTableViewController  viewModel属性不能为空，且必须为FBSTableViewModel子类！");
    
    FBSCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
    
    if ([self respondsToSelector:@selector(fbsCell:didSelectRowAtIndexPath:)]) {
        [self fbsCell:cell didSelectRowAtIndexPath:indexPath];
    }
}


#pragma mark - FBSCellDelegate

- (void)fbsCell:(FBSCell *)cell didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    //    FBSLog(@"点击了 %@, indexPath %@",cell,indexPath);
}

- (void)fbsCell:(FBSCell *)cell didClickButtonAtIndex:(NSInteger)index {
    
}

#pragma mark - preperty
- (FBSTableView *)tableView {
    if (!_tableView) {
        FBSTableView *aView = [[FBSTableView alloc] initWithFrame:CGRectZero];
        aView.separatorStyle = UITableViewCellSeparatorStyleNone;
        aView.contentInset = UIEdgeInsetsMake(kNavTopMargin, 0, 0, 0);
        aView.scrollIndicatorInsets = aView.contentInset;
        aView.backgroundColor = COLOR_CommomBG;
        aView.dataSource = self;
        aView.delegate = self;
        _tableView = aView;
    }
    return _tableView;
}

@end
